﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SunatServ;
using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Structs;
using System.Linq;

namespace Sunat.Servicios
{
    public interface ISunatSoap
    {
        Task<response> Consultar(string numDocumento);
        Task<DataTablesStructs.ReturnedData<consultarNumRucResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string numDocumento);
    }
    public class SunatSoap : ISunatSoap
    {
        public SunatSoap()
        {

        }
        public async Task<response> Consultar(string numDocumento)
        {
            consultarNumRucRequest consulta = new consultarNumRucRequest();
            consulta.name = new numDeRuc()
            {
                NumeroRuc = numDocumento,
            };
            JcfSoftSOAPPortTypeClient wsCliente = new JcfSoftSOAPPortTypeClient();
            try
            {
                //var response = await wsCliente.consultarNumRucAsync(consulta);
                //return ""; //response.name;
                return new response();
            }
            catch (Exception)
            {
                return new response();
            }
        }
        public async Task<DataTablesStructs.ReturnedData<consultarNumRucResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string numDocumento)
        {
            /*JcfSoftSOAPPortTypeClient servicio = new JcfSoftSOAPPortTypeClient();
            consultarNumRucRequest entidad = new consultarNumRucRequest();
            entidad.name = new numDeRuc()
            {
                NumeroRuc = "20563379465" //numDocumento;

            };//numDocumento;
            consultarNumRucResponse entidadResponse = new consultarNumRucResponse();
            entidadResponse = await servicio.consultarNumRucAsync(entidad);*/

            consultarNumRucRequest consulta = new consultarNumRucRequest();
            consulta.name = new numDeRuc()
            {
                NumeroRuc = numDocumento,
            };
            JcfSoftSOAPPortTypeClient wsCliente = new JcfSoftSOAPPortTypeClient();
            List<consultarNumRucResponse> lista = new List<consultarNumRucResponse>();
            try
            {
                var response = await wsCliente.consultarNumRucAsync(consulta); //await wsCliente.consultarNumRucAsync(consulta);
                lista.Add(response);
            }
            catch (Exception ex)
            {
                return new DataTablesStructs.ReturnedData<consultarNumRucResponse>
                {
                    Data = lista,
                    DrawCounter = parameters.DrawCounter,
                    RecordsFiltered = 0,
                    RecordsTotal = 0
                };
            }

            var recordsFiltered = lista.Count();
            var data = lista
                .ToList();
            var recordsTotal = data.Count;
            return new DataTablesStructs.ReturnedData<consultarNumRucResponse>
            {
                Data = data,
                DrawCounter = parameters.DrawCounter,
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };
        }
    }
}
