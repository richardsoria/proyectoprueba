﻿using BAMBAS.AutenticacionAD.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.AutenticacionAD.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly string _identityUrl;
        private readonly string _autenticacionUrl;


        public HomeController(ILogger<HomeController> logger,
             IConfiguration configuration)
        {
            _logger = logger;
            _identityUrl = configuration.GetValue<string>("IdentityUrl");
            _autenticacionUrl = configuration.GetValue<string>("AutenticacionUrl");
        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var client = new HttpClient())
                {
                    var email = new MailAddress(User.Identity.Name);
                    var user = email.ToString();
                    var content = new StringContent(
                        JsonSerializer.Serialize(new LoginViewModel
                        {
                            Email = email.ToString(),
                            IsActiveDirectory = true,
                            Password = "123",   
                        }),
                        Encoding.UTF8,
                        "application/json"
                    );
                    var request = await client.PostAsync(_identityUrl + "v1/identity/authentication", content);
                    if (request.IsSuccessStatusCode)
                    {
                        var result = JsonSerializer.Deserialize<IdentityAccess>(
                          await request.Content.ReadAsStringAsync(),
                          new JsonSerializerOptions
                          {
                              PropertyNameCaseInsensitive = true
                          });
                        /////
                        return Redirect($"{_autenticacionUrl}Conexion?at={result.AccessToken}");
                    }
                    else
                    {
                        ///
                        var codigoerror = 1;
                        var msj = await request.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(msj))
                        {
                            var errorresult = JsonSerializer.Deserialize<RespuestaError>(
                                msj,
                                new JsonSerializerOptions
                                {
                                    PropertyNameCaseInsensitive = true
                                }
                            );

                            if (errorresult.CodEstado == -4 || errorresult.CodEstado == -5)
                            {
                                codigoerror = 2;
                            }
                            return Redirect($"{_autenticacionUrl}ErrorAD?cd={codigoerror}&ad={errorresult.Nombre}");
                        }
                        return Redirect($"{_autenticacionUrl}ErrorAD?ad=error&cd={codigoerror}");
                    }
                }
            }
            return RedirectToAction("SignIn", "Account", new { Area = "MicrosoftIdentity" });
        }
        public IActionResult Logout()
        {
            return RedirectToAction("SignOut", "Account", new { Area = "MicrosoftIdentity" });
        }
    }
}
