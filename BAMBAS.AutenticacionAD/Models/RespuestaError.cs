﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.AutenticacionAD.Models
{
    public class RespuestaError
    {
        public int CodEstado { get; set; }
        public string Nombre { get; set; }
    }
}
