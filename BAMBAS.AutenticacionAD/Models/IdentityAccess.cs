﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.AutenticacionAD.Models
{
    public class IdentityAccess
    {
        public bool Succeeded { get; set; }
        public string AccessToken { get; set; }
        public string ErrorMessage { get; set; }
        public bool CambiarContrasena { get; set; }
    }
}
