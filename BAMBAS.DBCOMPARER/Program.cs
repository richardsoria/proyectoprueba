﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace BAMBAS.DBCOMPARER
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando!");

            var sgs = Assembly.Load("BAMBAS.SGS.Modelos");
            var per = Assembly.Load("BAMBAS.PER.Modelos");
            var tro = Assembly.Load("BAMBAS.TRO.Modelos");
            var sgsTypes = sgs.GetTypes();
            var perTypes = per.GetTypes();

            var todosTipos = sgsTypes.Union(perTypes);
            foreach (Type type in todosTipos)
            {
                if (type.CustomAttributes.Any(x => x.AttributeType.Name == "BaseDatosAttribute"))
                {
                    var item = type.CustomAttributes.FirstOrDefault(x => x.AttributeType.Name == "BaseDatosAttribute");
                    var entidadBD = new EntidadBD(item.NamedArguments,type.Name);
                    //
                    using (var conn = new SqlConnection("Server=sql5091.site4now.net;Initial Catalog=DB_A6AED0_CEPHEID;Persist Security Info=False;User ID=DB_A6AED0_CEPHEID_admin;Password=Cepheid123;MultipleActiveResultSets=true;Encrypt=True;TrustServerCertificate=True;Connection Timeout=300;"))
                    {
                        conn.Open();
                        var parametros = new DynamicParameters();
                        parametros.Add("@tabla", $"{entidadBD.esquema}.{entidadBD.nombreTabla}", dbType: DbType.String);
                        var reader = conn.ExecuteReader("[SEG].[USP_ObtenerRegistroTabla]", parametros, commandType: CommandType.StoredProcedure);
                        //ver propiedades de la clase que viene de la dll
                        var properties = type.GetProperties().Where(x => x.CanWrite).Select(x => x.Name).ToList();
                        try
                        {
                            var columns = new List<string>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                columns.Add(reader.GetName(i));
                            }
                            columns = columns.OrderBy(x => x).ToList();
                            properties = properties.OrderBy(x => x).ToList();
                            if (columns.SequenceEqual(properties))
                            {
                                //si son iguales
                                Console.BackgroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine($"{entidadBD.esquema}.{entidadBD.nombreTabla}: el modelo es igual al de bd");
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.DarkYellow;
                                Console.WriteLine($"{entidadBD.esquema}.{entidadBD.nombreTabla}: revisar {entidadBD.nombreGenerico} --> el modelo NO es igual al de bd");
                                var enlaprimeraynoenlasegunda = columns.Where(x => !properties.Contains(x)).ToList();
                                var enlasegundaynoenlaprimera = properties.Where(x => !columns.Contains(x)).ToList();
                                Console.BackgroundColor = ConsoleColor.Black;
                                if (enlaprimeraynoenlasegunda != null && enlaprimeraynoenlasegunda.Count()>0)
                                    Console.WriteLine($"\t\tAl modelo le falta -> {string.Join(", ", enlaprimeraynoenlasegunda)}");
                                if (enlasegundaynoenlaprimera != null && enlasegundaynoenlaprimera.Count() > 0)
                                    Console.WriteLine($"\t\tBD no tiene -> {string.Join(", ", enlasegundaynoenlaprimera)}");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine($"{entidadBD.esquema}.{entidadBD.nombreTabla}: Ocurrió un error con esta tabla");
                            Console.WriteLine($"{ex.Message}");
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ReadLine();
        }
    }
}
