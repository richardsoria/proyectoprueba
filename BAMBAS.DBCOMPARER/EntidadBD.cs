﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BAMBAS.DBCOMPARER
{
    public class EntidadBD
    {
        public EntidadBD(IEnumerable<CustomAttributeNamedArgument> argumentos, string nombreGenerico)
        {
            nombreTabla = argumentos.FirstOrDefault(x => x.MemberName == nameof(nombreTabla)).TypedValue.Value.ToString();
            esquema = argumentos.FirstOrDefault(x => x.MemberName == nameof(esquema)).TypedValue.Value.ToString();
            this.nombreGenerico = nombreGenerico;
        }
        public string nombreGenerico { get; set; }
        public string nombreTabla { get; set; }
        public string esquema { get; set; }
    }
}
