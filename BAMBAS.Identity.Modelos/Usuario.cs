﻿using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Identity.Modelos
{
    /// <summary>
    /// SGA EMP 08
    /// </summary>
    public class Usuario : EntidadAuditoria
    {
        public int IDPRSNA { get; set; }
        public string CUSRO { get; set; }   
        public string UHMLGDO { get; set; }   
        public string NYAPLLDS { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NTLFNO { get; set; }
        public string CLVE { get; set; }
        public string FVCLVE { get; set; }
        public bool FBLQUO { get; set; }
        public bool FRZRCMBOCLVE { get; set; }
        public int INTNTS { get; set; }
    }
    public class SucursalUsuario 
    {
        public string IDUSRO { get; set; }
        public string IDSCRSL { get; set; }
        public string NSCRSL { get; set; }

        public string ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; } = "A";
        public DateTime? FESTDO { get; set; }
    }
}
