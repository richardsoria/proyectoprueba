﻿//using ENTITIES.Models.Auth;
using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.Proxies.Seguridad;
using Microsoft.Extensions.DependencyInjection;

namespace BAMBAS.PER.Api.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<ISelect2Service, Select2Service>();
            //
            serviceCollection.AddTransient<IConsultasCorreo, ConsultasCorreo>();
            serviceCollection.AddTransient<IConsultasDireccion, ConsultasDireccion>();
            serviceCollection.AddTransient<IConsultasDocumento, ConsultasDocumento>();
            serviceCollection.AddTransient<IConsultasPersona, ConsultasPersona>();
            serviceCollection.AddTransient<IConsultasTelefono, ConsultasTelefono>();
            serviceCollection.AddTransient<IConsultasArchivo, ConsultasArchivo>();
            serviceCollection.AddTransient<IConsultasTrabajador, ConsultasTrabajador>();
            serviceCollection.AddHttpClient<ISeguridadProxy,SeguridadProxy>();
        }
    }
}
