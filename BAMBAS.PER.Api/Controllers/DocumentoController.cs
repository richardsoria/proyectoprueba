﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Documento;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("documento")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DocumentoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasDocumento _consultasDocumento;
        public DocumentoController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasDocumento consultasDocumento)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasDocumento = consultasDocumento;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idprsna, string id)
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasDocumento.ObtenerDataTable(parameters, idprsna, id);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(int idprsna, int id)
        {
            var datos = await _consultasDocumento.ListarDocumento(idprsna, id);
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string idprsna, string id)
        {
            var datos = await _consultasDocumento.ObtenerDocumento(idprsna, id);
            return Ok(datos);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoDocumentoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoDocumentoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoDocumentoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }
    }
}
