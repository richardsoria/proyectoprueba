﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Persona;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BAMBAS.PER.Servicio.Proxies.Seguridad;
using BAMBAS.PER.Servicio.Consultas.Objetos;
using BAMBAS.CORE.Structs;
using BAMBAS.PER.Modelos;
using BAMBAS.PER.Servicio.Proxies.Seguridad.Comandos;
using BAMBAS.PER.Servicio.Proxies;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Helpers;
using System.Text.RegularExpressions;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("persona")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PersonaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasPersona _consultasPersona;
        private readonly IMediator _mediator;
        private readonly ISeguridadProxy _seguridadProxy;
        public PersonaController(
            IDataTableService dataTableService,
            IConsultasPersona consultasPersona,
            IMediator mediator,
            ISeguridadProxy seguridadProxy)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasPersona = consultasPersona;
            _seguridadProxy = seguridadProxy;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado, bool? soloContratistas = null, bool? soloTrabajadores = null)
        {
            var parameters = _dataTableService.GetSentParameters();
            var resp = await _consultasPersona.ObtenerDataTable(parameters, gdtprsna, tipoDocumento, numeroDocumento, datos, fechaDesde, fechaHasta, estado,soloContratistas,soloTrabajadores);
            return Ok(resp);
        }
        [HttpGet("obtener-colaboradores")]
        public async Task<IActionResult> ObtenerColaboradoresDataTable(string idprsna)
        {
            var parameters = _dataTableService.GetSentParameters();
            var resp = await _consultasPersona.ObtenerColaboradoresDataTable(parameters, idprsna);
            return Ok(resp);
        }

        [HttpGet("listadoPersonaSinUsuario")]
        public async Task<IActionResult> listadoPersonaSinUsuario(string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado, bool contratista)
        {
            var parameters = _dataTableService.GetSentParameters();
            var listadopersonas = await _consultasPersona.ListarPersonas(id, gdtprsna, tipoDocumento, numeroDocumento, datos, fechaDesde, fechaHasta, estado);

            var listadousuarios = await _seguridadProxy.ListarUsuarios();

            var personas = listadopersonas.Where(x => !listadousuarios.Any(y => y.IDPRSNA == x.ID.ToString()) && !string.IsNullOrEmpty(x.NDCMNTO) && !string.IsNullOrEmpty(x.GDDCMNTO)).ToList();

            foreach (var item in personas)
            {
                item.FVNCMNTO = DateTime.Now.AddDays(30).ToLocalDateFormat();//ESTO DEBERIA ESTAR POR UN PARAMETRO
            Verificar:

                if (listadousuarios.Any(x => Regex.Replace(x.CUSRO, @"[0-9]", "") == item.PUSUARIO))
                {

                    item.PUSUARIO = item.PUSUARIO + (listadousuarios.Count(x => Regex.Replace(x.CUSRO, @"[0-9]", "") == item.PUSUARIO)).ToString();
                    goto Verificar;
                }
            }

            var retorno = personas.ConvertirTabla(parameters);

            return Ok(retorno);
        }


        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string id)
        {
            var empresas = await _consultasPersona.ObtenerDatosPersonas(id);
            return Ok(empresas);
        }
        [HttpGet("obtenerprincipal")]
        public async Task<IActionResult> ObtenerPrincipal(string id)
        {
            var personas = await _consultasPersona.ObtenerDatosPersonasPrincipal(id);
            return Ok(personas);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(string id, string tPersona, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado, bool contratista)
        {
            var empresas = await _consultasPersona.ListarPersonas(id, tPersona, tipoDocumento, numeroDocumento, datos, fechaDesde, fechaHasta, estado);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoPersonaInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoPersonaActualizar entidad)
        {

            var result = await _mediator.Send(entidad);

            if (!result.EsSatisfactoria)
            {
                return Ok(result);
            }

            string NIDENT = entidad.ID.ToString();

            var result2 = await ActualizarPersonaUsuarioAsync(NIDENT);

            if (result.EsSatisfactoria && result2.EsSatisfactoria && result.CodEstado == 2)
                return Ok(result);

            return Ok(result2);
        }

        private async Task<RespuestaConsulta> ActualizarPersonaUsuarioAsync(string NIDENT)
        {
            string NMBSAPELLS;
            var DTDCNL = await _consultasPersona.ObtenerDatosPersonasPrincipal(NIDENT);

            if (DTDCNL.RSCL == "")
            {
                NMBSAPELLS = DTDCNL.PNMBRE + ' ' + DTDCNL.SNMBRE + ' ' + DTDCNL.APTRNO + ' ' + DTDCNL.AMTRNO;
            }
            else
            {
                NMBSAPELLS = DTDCNL.RSCL;
            }


            var usuariopersona = new ComandoPersonaUsuarioActualizar
            {
                IDPRSNA = NIDENT,
                NYAPLLDS = NMBSAPELLS,
                GDDCMNTO = DTDCNL.GDDCMNTO,
                NDCMNTO = DTDCNL.NDCMNTO,
                TLFNO = DTDCNL.NTLFNO,
                UEDCN = User.GetUserCode()
            };

            var result2 = await _seguridadProxy.ActualizarPersonaUsuario(usuariopersona);

            return result2;
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoPersonaEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpGet("listahomonimo")]
        public async Task<IActionResult> listaHomonimo(string gddcmnto, string ndcmnto, string amtrno, string aptrno, string pnmbre, string snmbre)
        {
            var parameters = _dataTableService.GetSentParameters();
            var resp = await _consultasPersona.listarHomonimo(parameters, gddcmnto, ndcmnto, amtrno, aptrno, pnmbre, snmbre);
            return Ok(resp);
        }
    }
}
