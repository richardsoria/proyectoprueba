﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Trabajador;
using BAMBAS.PER.Servicio.Proxies;
using BAMBAS.PER.Servicio.Proxies.Seguridad;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("trabajador")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TrabajadorController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasTrabajador _consultasTrabajador;
        private readonly ISelect2Service _select2Service;
        public TrabajadorController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasTrabajador consultasTrabajador,
            ISelect2Service select2Service)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasTrabajador = consultasTrabajador;
            _select2Service = select2Service;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasTrabajador.ObtenerDataTable(parameters);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar()
        {
            var datos = await _consultasTrabajador.ListarTrabajador();
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string id)
        {
            var datos = await _consultasTrabajador.ObtenerTrabajador(id);
            return Ok(datos);
        }
        [HttpGet("listadoPersonalTareo")]
        public async Task<IActionResult> ListadoPersonalTareo (string csap, string datos, int idcntrtsta, int bambas)
        {
            var parameters = _dataTableService.GetSentParameters();
            var listadopersonal = await _consultasTrabajador.ListadoPersonalTareo(csap, datos, idcntrtsta, bambas);
            var retorno = listadopersonal.ConvertirTabla(parameters);
            return Ok(retorno);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoTrabajadorInsertar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoTrabajadorActualizar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoTrabajadorEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }
        ///DEJEN DE COMENTAR ESTO POR FAVOR.-.. ATTE: WMBG
        [HttpGet("select-trabajador")]
        public async Task<IActionResult> ListarTrabajadores()
        {
            var param = _select2Service.GetRequestParameters();
            var retorno = await _consultasTrabajador.SelectTrabajador(param);
            return Ok(retorno);
        }
        [HttpGet("obtenerContratista")]
        public async Task<IActionResult> ObtenerContratista()
        {
            var contratista = await _consultasTrabajador.ObtenerContratista();
            return Ok(contratista);
        }
    }
}
