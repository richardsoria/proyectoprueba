﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Correo;
using BAMBAS.PER.Servicio.Proxies;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("correo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CorreoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasCorreo _consultasCorreo;
        public CorreoController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasCorreo consultasCorreo)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasCorreo = consultasCorreo;
        }

        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idprsna, string id)
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasCorreo.ObtenerDataTable(parameters, idprsna, id);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(string idprsna, string id)
        {
            var datos = await _consultasCorreo.ListarCorreo(idprsna, id);
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string idprsna, string id)
        {
            var datos = await _consultasCorreo.ObtenerCorreo(idprsna, id);
            return Ok(datos);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoCorreoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoCorreoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);            
            return Ok(result);            
        }


        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoCorreoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }
    }
}
