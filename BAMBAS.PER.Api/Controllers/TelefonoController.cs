﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Telefono;
using BAMBAS.PER.Servicio.Proxies;
using BAMBAS.PER.Servicio.Proxies.Seguridad;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("telefono")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TelefonoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasPersona _consultasPersona;
        private readonly IMediator _mediator;
        private readonly IConsultasTelefono _consultasTelefono;
        private readonly ISeguridadProxy _seguridadProxy;
        public TelefonoController(
            IDataTableService dataTableService,
            IConsultasPersona consultasPersona,
            IMediator mediator,
            IConsultasTelefono consultasTelefono,
            ISeguridadProxy seguridadProxy)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasTelefono = consultasTelefono;
            _consultasPersona = consultasPersona;
            _seguridadProxy = seguridadProxy;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idprsna, string id)
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasTelefono.ObtenerDataTable(parameters, idprsna, id);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(int idprsna, string id)
        {
            var datos = await _consultasTelefono.ListarTelefono(idprsna, id);
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string idprsna, string id)
        {
            var datos = await _consultasTelefono.ObtenerTelefono(idprsna, id);
            return Ok(datos);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoTelefonoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoTelefonoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);

            if (!result.EsSatisfactoria)
            {
                return Ok(result);
            }

            string ident = entidad.ID.ToString();

            string idPer = entidad.IDPRSNA;

            try
            {
                var result2 = await ActualizarTelefonoPersonaUsuarioAsync(ident, idPer);

                return Ok(result2);
            }
            catch (Exception)
            {
                return Ok(result);
            }
        }

        private async Task<RespuestaConsulta> ActualizarTelefonoPersonaUsuarioAsync(string ident, string idPer)
        {
            var DTDCNL = await _consultasPersona.ObtenerDatosPersonasPrincipal(idPer);
            string NMBSAPELLS = DTDCNL.PNMBRE + ' ' + DTDCNL.SNMBRE + ' ' + DTDCNL.APTRNO + ' ' + DTDCNL.AMTRNO;

            var usuariopersona = new ComandoPersonaUsuarioActualizar
            {
                IDPRSNA = idPer,
                NYAPLLDS = NMBSAPELLS,
                GDDCMNTO = DTDCNL.GDDCMNTO,
                NDCMNTO = DTDCNL.NDCMNTO,
                TLFNO = DTDCNL.NTLFNO,
                UEDCN = User.GetUserCode()

            };

            var result2 = await _seguridadProxy.ActualizarPersonaUsuario(usuariopersona);

            return result2;

            //var DTDCNL = await _consultasTelefono.ObtenerTelefono(ident, idPer);
            //if (DTDCNL != null)
            //{
            //    var usuariopersona = new ComandoPersonaUsuarioActualizar
            //    {
            //        IDPRSNA = ident,
            //        TLFNO = DTDCNL.NTLFNO,
            //        UEDCN = User.GetUserCode()

            //    };
            //        var result2 = await _mediator.Send(usuariopersona);

            //    return result2;
            //}
            //return new RespuestaConsulta
            //{
            //    CodEstado=-1,
            //    Nombre="NO PASO!!!"
            //};
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoTelefonoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }
    }
}
