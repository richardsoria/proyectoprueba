﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Archivo;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("archivo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ArchivoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasArchivo _consultasArchivo;
        public ArchivoController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasArchivo consultasArchivo)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasArchivo = consultasArchivo;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idprsna)
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasArchivo.ObtenerDataTable(parameters, idprsna);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(string idprsna, string id)
        {
            var datos = await _consultasArchivo.ListarArchivo(idprsna, id);
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string idprsna, string id)
        {
            var datos = await _consultasArchivo.ObtenerArchivo(idprsna, id);
            return Ok(datos);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoArchivoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoArchivoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
