﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Consultas;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Direccion;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.Api.Controllers
{
    [ApiController]
    [Route("direccion")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DireccionController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasDireccion _consultasDireccion;
        public DireccionController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasDireccion consultasDireccion)
        {
            _consultasDireccion = consultasDireccion;
            _dataTableService = dataTableService;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idprsna, string id)
        {
            var parameters = _dataTableService.GetSentParameters();
            var datos = await _consultasDireccion.ObtenerDataTable(parameters, idprsna, id);
            return Ok(datos);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(string idprsna, string id)
        {
            var datos = await _consultasDireccion.ListarDireccion(idprsna, id);
            return Ok(datos);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string idprsna, string id)
        {
            var datos = await _consultasDireccion.ObtenerDireccion(idprsna, id);
            return Ok(datos);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoDireccionInsertar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoDireccionActualizar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoDireccionEliminar entidad)
        {
            var result = await _mediator.Send(entidad);

            return Ok(result);
        }
    }
}
