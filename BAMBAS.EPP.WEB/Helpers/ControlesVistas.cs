﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.EPP.WEB.Helpers
{
    public static class ControlesVistas
    {
        public const string VistaProyeccion = "PROYECCION";
        public const string VistaMovimientoAlmacen = "MOVIMIENTOALMACEN";
        public const string VistaCodigoBarras = "CODIGOBARRAS";

        public static class Proyeccion
        {
            public const string AgregarProyeccion = "AGREGARPROYECCION";
            public const string EditarProyeccion = "EDITARPROYECCION";
            public const string EliminarProyeccion = "ELIMINARPROYECCION";

        }
        public static class MovimientoAlmacen
        {
            public const string AgregarMovimientoAlmacen = "AGREGARMOVIMIENTOALMACEN";
            public const string EditarMovimientoAlmacen = "EDITARMOVIMIENTOALMACEN";
            public const string EliminarMovimientoAlmacen = "ELIMINARMOVIMIENTOALMACEN";

        }
    }


}
