﻿using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.EPP;
using BAMBAS.Negocios.General.Seguridad;
using Microsoft.Extensions.DependencyInjection;

namespace BAMBAS.EPP.WEB.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddRepository(this IServiceCollection serviceCollection)
        {
        }
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            //Base
            //serviceCollection.AddTransient<IEmailService, EmailService>();
            //serviceCollection.AddTransient<IEmailTemplateService, EmailTemplateService>();
            serviceCollection.AddTransient<ISelect2Service, Select2Service>();
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IPaginationService, PaginationService>();
            serviceCollection.AddTransient<IViewRenderService, ViewRenderService>();

            //serviceCollection.AddTransient<ICloudStorageService, CloudStorageService>(); 

            //seguridad
            serviceCollection.AddHttpClient<IPerfilObjetoProxy, PerfilObjetoProxy>();
            serviceCollection.AddHttpClient<IGrupoDatoProxy, GrupoDatoProxy>();
            serviceCollection.AddHttpClient<IParametroProxy, ParametroProxy>();
            serviceCollection.AddHttpClient<IUbigeoProxy, UbigeoProxy>();
            serviceCollection.AddHttpClient<IUsuarioProxy, UsuarioProxy>();
            serviceCollection.AddHttpClient<IAreaProxy, AreaProxy>();
            serviceCollection.AddHttpClient<ISuperintendenciaProxy, SuperintendenciaProxy>();
            serviceCollection.AddHttpClient<IGerenciaProxy, GerenciaProxy>();
            serviceCollection.AddHttpClient<IGrupoCargoProxy, GrupoCargoProxy>();
            serviceCollection.AddHttpClient<IFamiliaProxy, FamiliaProxy>();
            serviceCollection.AddHttpClient<ICargoProxy, CargoProxy>();

            serviceCollection.AddHttpClient<IArticuloProxy, ArticuloProxy>();

            //
            serviceCollection.AddHttpClient<IProyeccionProxy, ProyeccionProxy>();
            serviceCollection.AddHttpClient<IMovimientoAlmacenProxy, MovimientoAlmacenProxy>();
            serviceCollection.AddHttpClient<ICodigoBarrasProxy, CodigoBarrasProxy>();

        }
    }
}
