﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Models;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.EPP.WEB.Models;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Autocomplete;
using BAMBAS.Negocios.Modelos.EPP;
using BAMBAS.Negocios.General.Seguridad;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.EPP.WEB.Controllers
{
    public class GeneralController : Controller
    {
        private readonly IGrupoDatoProxy _grupoDatoProxy;
        private readonly IUbigeoProxy _ubigeoProxy;
        private readonly IParametroProxy _parametroProxy;
        private readonly IAreaProxy _areaProxy;
        private readonly ISuperintendenciaProxy _superintendenciaProxy;
        private readonly IGerenciaProxy _gerenciaProxy;
        private readonly IGrupoCargoProxy _grupoCargoProxy;
        private readonly IFamiliaProxy _familiaProxy;
        private readonly ICargoProxy _cargoProxy;
        private readonly IArticuloProxy _ArticulosProxy;
        private readonly IDataTableService _dataTableService;
        private readonly IMemoryCache _cache;
        public GeneralController(
            IGrupoDatoProxy grupoDatoProxy,
            IUbigeoProxy ubigeoProxy,
            IParametroProxy parametroProxy,
            IAreaProxy areaProxy,
            ISuperintendenciaProxy superintendenciaProxy,
            IGerenciaProxy gerenciaProxy,
            IGrupoCargoProxy grupoCargoProxy,
            IFamiliaProxy familiaProxy,
            ICargoProxy cargoProxy,
            IArticuloProxy articulosProxy,
            IDataTableService dataTableService,
            IMemoryCache cache
            )
        {
            _grupoDatoProxy = grupoDatoProxy;
            _ubigeoProxy = ubigeoProxy;
            _parametroProxy = parametroProxy;
            _areaProxy = areaProxy;
            _superintendenciaProxy = superintendenciaProxy;
            _gerenciaProxy = gerenciaProxy;
            _grupoCargoProxy = grupoCargoProxy;
            _familiaProxy = familiaProxy;
            _cargoProxy = cargoProxy;
            _ArticulosProxy = articulosProxy;
            _dataTableService = dataTableService;
            _cache = cache;
        }
        [HttpGet("obtener-datos-usuario-logueado")]
        public IActionResult ObtenerLogueado()
        {
            var audit = new AuditoriaDto();
            audit.UEDCN = User.GetUserCode();
            audit.UCRCN = User.GetUserCode();
            audit.FCRCN = DateTime.Now;
            audit.FEDCN = DateTime.Now;
            audit.FESTDO = DateTime.Now;
            return Ok(audit);
        } 
        [HttpPost("guardar-sucursal-seleccionada")]
        public IActionResult GuardarSucursal(string id)
        {
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromDays(1));

            // Save data in cache.
            _cache.Set("ScrslSelec", id, cacheEntryOptions);
            return Ok();
        }
        [HttpGet("obtener-departamentos")]
        public async Task<IActionResult> ObtenerDepartamentos(string cps)
        {
            var result = await _ubigeoProxy.ObtenerDepartamentos(cps);
            return Ok(result);
        }
        [HttpGet("obtener-provincias")]
        public async Task<IActionResult> ObtenerProvincias(string codigoDpto, string cps)
        {
            var result = await _ubigeoProxy.ObtenerProvincias(codigoDpto, cps);
            return Ok(result);
        }
        [HttpGet("obtener-distritos")]
        public async Task<IActionResult> ObtenerDistritos(string codigoProvincia, string cps)
        {
            var result = await _ubigeoProxy.ObtenerDistrito(codigoProvincia, cps);
            return Ok(result);
        }

        [HttpGet("obtenercatalogoGD")]
        public async Task<IActionResult> ObtenerCatalogo()
        {
            var result = await _grupoDatoProxy.ObtenerCatalogos();
            return Ok(result);
        }

        [HttpGet("obtener-paises")]
        public async Task<IActionResult> ObtenerPaises()
        {
            var result = await _ubigeoProxy.ObtenerPaises();
            return Ok(result);
        }

        [HttpGet("obtener-parametro-por-abreviacion")]
        public async Task<IActionResult> ObtenerParametroPorAbreviacion(string aprmtro)
        {
            var retorno = await _parametroProxy.ObtenerParametroPorAbreviacion(aprmtro);
            return Ok(retorno); ;
        }

        [HttpGet("obtener-areas")]
        public async Task<IActionResult> ObtenerAreas(int idSuperintendencia)
        {
            var areas = await _areaProxy.ObtenerActivas(idSuperintendencia);
            return Ok(areas);
        }
        [HttpGet("obtener-superintendencias")]
        public async Task<IActionResult> ObtenerSuperintendencias(int idGerencia)
        {
            var superintendencias = await _superintendenciaProxy.ObtenerActivas(idGerencia);
            return Ok(superintendencias);
        }
        [HttpGet("obtener-gerencias")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var gerencia = await _gerenciaProxy.ObtenerActivas();
            return Ok(gerencia);
        }
        [HttpGet("obtener-grupo-cargo")]
        public async Task<IActionResult> ObtenerGrupoCargo()
        {
            var gerencia = await _grupoCargoProxy.ObtenerActivos();
            return Ok(gerencia);
        }
        [HttpGet("obtener-cargos")]
        public async Task<IActionResult> ObtenerCargo(int idGrupoCargo)
        {
            var gerencia = await _cargoProxy.ObtenerActivos(idGrupoCargo);
            return Ok(gerencia);
        }
        [HttpGet("obtener-articulos")]
        public async Task<IActionResult> ObtenerArticulos()
        {
            var articulos = await _ArticulosProxy.ObtenerActivas();
            return Ok(articulos);
        }
        [HttpGet("get-Familiasactivas")]
        public async Task<IActionResult> ObtenerDataTableActivas()
        {
            var parameters = _dataTableService.GetSentParameters();
            var articulo = await _familiaProxy.ObtenerDataTable(parameters,0,true);
            return Ok(articulo);
        }
        [HttpGet("searchfamilias")]
        public async Task<IActionResult> searchfamilias(string term)
        {
            var familia = await _familiaProxy.ObtenerAutocomplete(term);
            var lista = new List<AutocompleteDto>();

            foreach (var item in familia)
            {
                var model = new AutocompleteDto()
                {
                    value = item.DSCRPCN.ToString(),
                    label = item.DSCRPCN.ToString(),
                    value2 = (item.ID.ToString() + " - " + item.DSCRPCN.ToString()),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }

    }
}
