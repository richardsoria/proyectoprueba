﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.EPP.Proyeccion;
using BAMBAS.Negocios.EPP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.EPP.WEB.Controllers
{
    [Route("codigobarras")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     

    public class CodigoBarrasController : Controller
    {
        private readonly IProyeccionProxy _proyeccionProxy;
        private readonly IDataTableService _dataTableService;
        public CodigoBarrasController(
            IDataTableService dataTableService,
           IProyeccionProxy proyeccionProxy)
        {
            _dataTableService = dataTableService;
            _proyeccionProxy = proyeccionProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

    }
}
