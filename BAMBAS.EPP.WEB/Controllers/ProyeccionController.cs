﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.EPP.Proyeccion;
using BAMBAS.Negocios.EPP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.EPP.WEB.Controllers
{
    [Route("proyeccion")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     

    public class ProyeccionController : Controller
    {
        private readonly IProyeccionProxy _proyeccionProxy;
        private readonly IDataTableService _dataTableService;
        public ProyeccionController(
            IDataTableService dataTableService,
           IProyeccionProxy proyeccionProxy)
        {
            _dataTableService = dataTableService;
            _proyeccionProxy = proyeccionProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("agregar")]
        [HttpGet("editar/{id}")]
        public IActionResult Datos(int? id = null)
        {
            return View(id);
        }
        [HttpGet("realvsproyectado/{id}")]
        public IActionResult Real_vs_Proyectado(int? id = null)
        {
            return View(id);
        }
        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(string dscrpcn, string anio)
        {
            var parameters = _dataTableService.GetSentParameters();
            var grupocargo = await _proyeccionProxy.ObtenerDataTable(parameters, dscrpcn, anio);
            return Ok(grupocargo);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ProyeccionDto proyeccion)
        {
            proyeccion.UCRCN = User.GetUserCode();
            var ret = await _proyeccionProxy.Insertar(proyeccion);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok(ret.CodEstado);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var retorno = await _proyeccionProxy.Obtener(id);
            return Ok(retorno); ;
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ProyeccionDto proyeccion)
        {
            proyeccion.UEDCN = User.GetUserCode();
            var ret = await _proyeccionProxy.Actualizar(proyeccion);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar-aprobado")]
        public async Task<IActionResult> ActualizarAprobado(ProyeccionDto proyeccion)
        {
            proyeccion.UEDCN = User.GetUserCode();
            proyeccion.APRBDOX = User.GetUserCode();
            var ret = await _proyeccionProxy.ActualizarAprobado(proyeccion);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new ProyeccionDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _proyeccionProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("eliminar-detalle")]
        public async Task<IActionResult> EliminarDetalle(int id)
        {
            var entidad = new ProyeccionDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _proyeccionProxy.EliminarDetalle(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }

    }
}
