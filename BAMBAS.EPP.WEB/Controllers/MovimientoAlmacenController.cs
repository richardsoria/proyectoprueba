﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.EPP.MovimientoAlmacen;
using BAMBAS.Negocios.EPP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace BAMBAS.EPP.WEB.Controllers
{
    [Route("movimientoalmacen")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class MovimientoAlmacenController : Controller
    {
        private readonly IMovimientoAlmacenProxy _movimientoalmacenProxy;
        private readonly IDataTableService _dataTableService;
        private object _stockProxy;

        public MovimientoAlmacenController(
            IDataTableService dataTableService,
            IMovimientoAlmacenProxy movimientoalmacenProxy)
        {
            _dataTableService = dataTableService;
            _movimientoalmacenProxy = movimientoalmacenProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("agregar")]
        [HttpGet("editar/{id}")]
        public IActionResult Datos(int? id = null)
        {
            return View(id);
        }

    }
}
