﻿var InicializarMovimientoAlmacen = function () {
    var $tablamovimientoalmacen= $("#tabla_movimientoalmacen");
    var $btnMovimientoNew = $("#btnMovimientoNew");
    var $selectAlmacen = $(".select-almcn");
    var $selectTipoMovimiento = $(".select-tmvmnto");
    var $accesoAgregarMovimientoAlmacen = $("#accesoAgregarMovimientoAlmacen");
    var $accesoEditarMovimientoAlmacen = $("#accesoEditarMovimientoAlmacen");
    var $accesoEliminarMovimientoAlmacen = $("#accesoEliminarMovimientoAlmacen");
    var $btnBuscar = $('#btnBuscar');

    var validacionControles = {
        init: function () {
            if ($accesoAgregarMovimientoAlmacen.val() == "False") {
                $btnMovimientoNew.remove();
            }
        }
    };

    var selects = {
        init: function () {
            $selectTipoMovimiento.LlenarSelectGD("GDTMVMNTO");
        }
    };

    var tablaMovimientoAlmacen = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/movimientoalmacen/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idalmcn = $selectAlmacen.val();
                    data.gdtmvmnto = $selectTipoMovimiento.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Almacén",
                    className: "text-center",
                    data: "anio",
                    orderable: false
                },
                {
                    title: "Tipo de Movimiento",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Fecha Movimiento",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false
                },
                {
                    title: "U. Entrega",
                    className: "text-right",
                    data: "imprte",
                    orderable: false,
                    render: function (data) {
                        return data.toFixed(2)
                    }
                },
                {
                    title: "U. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "uedcn",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '10%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarMovimientoAlmacen.val() == "True") {
                            tpm += `<button title="Editar" data-id="${data.id}" class="btn btn-info btn-xs btn-editar"><span><i class="la la-edit"></i></button>`;                            
                        }

                        if ($accesoEliminarMovimientoAlmacen.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaMovimientoAlmacen.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                RedirectWithSubfolder(`/movimientoalmacen/editar/${id}`);
            });
            tablaMovimientoAlmacen.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/proyeccion/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaMovimientoAlmacen.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaMovimientoAlmacen.objeto = $tablamovimientoalmacen.DataTable(tablaMovimientoAlmacen.opciones);
            tablaMovimientoAlmacen.eventos();
        }
    };

    var eventosIncrustados = {
        botonBuscar: function () {
            $btnBuscar.on("click", function () {        
                tablaMovimientoAlmacen.reload();
            })
        },
        init: function () {
            eventosIncrustados.botonBuscar();
        }
    };

    return {
        init: function () {
            selects.init();
            validacionControles.init();
            tablaMovimientoAlmacen.inicializador();
            eventosIncrustados.init();    
        }
    };
}();

$(() => {
    InicializarMovimientoAlmacen.init();
})

