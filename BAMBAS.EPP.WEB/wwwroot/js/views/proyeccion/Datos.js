﻿var InicializarProyeccionDatos = function () {
    var $FormularioProyeccion = $("#Proyeccion_form");
    var $IdProyeccion = $("#IdProyeccion");
    var $selectEstados = $(".select-estados");
    var $selectTipoProyeccion = $(".select-tppryccn");
    var $selectMoneda = $(".select-moneda");
    var $selectMesInicio = $(".select-msinco");
    var $selectMesFin = $(".select-msfn");

    var $tablaFamilias = $("#tabla_familias");
    var $modalFamilias = $("#modal_familias");
    var $txtanio = $("#txtanio");
    var $txtimporte = $("#txtimporte");
    var $DivFechaIni = $("#DivFechaIni");
    var $DivFechaFin = $("#DivFechaFin");
    var $DivBusqueda = $("#DivBusqueda");
    var $txtBusqueda = $("#txtBusqueda");
    var $ValtxtBusqueda = $("#ValtxtBusqueda");
    var $dataecontrada = $("#dataecontrada");
    var $btnAgregarFamilias = $('#btnAgregarFamilias');
    var $btnAprobar = $("#btnAprobar");
    var $btnBuscar = $("#btnBuscar");
    var $btnAgregar = $("#btnAgregar");
    var $btnLimpiar = $("#btnLimpiar");
    var $btnGuardar = $("#btnGuardar");
    var $DivtxtAprobado = $("#DivtxtAprobado");
    var $DivbtnAprobado = $("#DivbtnAprobado");
    var arrayfamilias = [];

    var tablafamilias = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/get-Familiasactivas",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        if (arrayfamilias.length > 0) {
                            var CheckI = arrayfamilias.find(f => f.IDFAMILIA == data.id);
                            if (CheckI) {
                                tpm += `<input data-desc="${data.dscrpcn}" data-idfamilia="${data.id}" type="checkbox" class="chksbf" title="Seleccionar SubFamilia"  checked = "checked" >`;
                            } else {
                                tpm += `<input data-desc="${data.dscrpcn}" data-idfamilia="${data.id}" type="checkbox" class="chksbf" title="Seleccionar SubFamilia" >`;
                            }
                        } else {
                            var CheckMemory = tablafamilias.memory_familia.find(x => x.IDFAMILIA == data.id);
                            if (CheckMemory) {
                                tpm += `<input data-desc="${data.dscrpcn}" data-idfamilia="${data.id}" type="checkbox" class="chksbf" title="Seleccionar SubFamilia"  checked = "${CheckMemory.CHECKFAMILIA}" >`;
                            } else {
                                tpm += `<input data-desc="${data.dscrpcn}" data-idfamilia="${data.id}" type="checkbox" class="chksbf" title="Seleccionar SubFamilia" >`;
                            }
                        }

                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        memory_familia: [],
        eventos: function () {
            tablafamilias.objeto.on("change", ".chksbf", function () {

                var checkfamilia = $(this).is(':checked');
                var idfamilia = $(this).data("idfamilia");
                var descr = $(this).data("desc");


                var delete_familia = false;
                if (!checkfamilia) {
                    delete_familia = true;
                }
                var new_familia = true;
                for (let i = 0; i < tablafamilias.memory_familia.length; i++) {
                    if (tablafamilias.memory_familia[i].IDFAMILIA == idfamilia) {
                        new_familia = false;
                        if (delete_familia) {
                            tablafamilias.memory_familia.splice(i, 1);
                        } else {
                            tablafamilias.memory_familia[i].CHECKFAMILIA = checkfamilia;
                        }
                    }
                }
                if (new_familia) {
                    tablafamilias.memory_familia.push({
                        IDFAMILIA: idfamilia,
                        CHECKFAMILIA: checkfamilia,
                        DESCR: descr,
                    });
                }

                /*             console.log(tablafamilias.memory_familia);*/
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablafamilias.objeto = $tablaFamilias.DataTable(tablafamilias.opciones);
            tablafamilias.eventos();
        }
    };

    var modalFamilia = {
        eventos: {
            onHide: function () {
                $modalFamilias.on('hidden.bs.modal', function () {
                    modalFamilia.eventos.reset();
                })
            },
            onShow: function () {
                $modalFamilias.on('shown.bs.modal', function () {
                    tablafamilias.memory_familia = [];
                })
            },
            reset: function () {
                tablafamilias.memory_familia = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        tipoproyeccion: function () {
            $selectTipoProyeccion.on("change", function () {
                var id = $(this).val();
                var fecha = new Date();
                var anio = fecha.getFullYear();
                switch (id) {
                    case '':
                        $txtanio.val("");
                        datepickers.init();
                        $DivFechaIni.hide();
                        $DivFechaFin.hide();
                    case '1':
                        $txtanio.val("");
                        datepickers.init();
                        $DivFechaIni.hide();
                        $DivFechaFin.hide();
                        break;
                    case '2':
                        $txtanio.val(anio);
                        $txtanio.datepicker("destroy")
                        $DivFechaIni.show();
                        $DivFechaFin.show();
                        $(".select-msinco option[value='1']").hide();
                        break;
                    default:
                        $txtanio.val(anio);
                        $txtanio.datepicker("destroy")
                        $DivFechaIni.show();
                        $DivFechaFin.show();
                        $(".select-msinco option[value='1']").hide();
                        break;
                }
            });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            $selectTipoProyeccion.LlenarSelectGD("GDTPRYCCN");
            $selectMoneda.LlenarSelectGD("GDTMNDA");
            $selectMesInicio.LlenarSelectGD("GDMSS");
            $selectMesFin.LlenarSelectGD("GDMSS");
            if ($selectTipoProyeccion.val() != 2) {
                $DivFechaIni.hide();
                $DivFechaFin.hide();
            }
            $selectMoneda.val(1);
            $selectMesFin.val(12);
            this.tipoproyeccion();
        }
    };

    var Solodecimales = {
        init: function () {
            var value_befort = "";
            $txtimporte.on("keypress", function (evt) {
                value_befort_key = $txtimporte.val();
                var re = /^[0-9]*(\.[0-9]{0,2})?$/;
                var OK = re.exec(value_befort_key);
                if (!OK) {
                    return false;
                } else {
                    value_befort = value_befort_key;
                    return true;
                }
            });
            $txtimporte.on("keyup", function (evt) {
                var value_after = $txtimporte.val();
                var re = /^[0-9]*(\.[0-9]{0,2})?$/;
                var OK = re.exec(value_after);
                if (!OK) {
                    value_befort = value_befort;
                    $txtimporte.val(value_befort);
                    return false;
                } else {
                    value_befort = value_after;
                    return true;
                }
            });
            $FormularioProyeccion.find("[name='TXTPERSONAL']").on("keyup", function (evt) {
                var personalmaximo = parseInt($FormularioProyeccion.find("[name='TXTPERSONAL']").val());
                $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                    var idfamilia = $(this).children('td').find("[class='idfamilia']").val();
                    var per = ".personal-" + idfamilia;
                    var personal = 0;
                    personal = $("#tabla_Proyeccionadd").find(per).val();
                    if (isNaN(personal)) { personal = 0 };
                    if (parseInt(personal) > parseInt(personalmaximo)) {
                        $("#tabla_Proyeccionadd").find(per).addClass("personalproyectado");
                        $("#tabla_Proyeccionadd").find(per).attr('style', 'border :solid 2px red; color: red');
                    } else {
                        $("#tabla_Proyeccionadd").find(per).removeClass("personalproyectado");
                        $("#tabla_Proyeccionadd").find(per).attr('style', 'border :');
                    }
                })
            });
        }
    };

    var datepickers = {
        anio: function () {
            $txtanio.datepicker({
                format: "yyyy",
                minViewMode: "years"
            });
        },
        init: function () {
            this.anio();
        }
    };

    var eventosIncrustados = {
        changePersonalProyectado: function () {
            $(document).on('keyup', '.per', function () {
                var personalmaximo = $FormularioProyeccion.find("[name='TXTPERSONAL']").val();
                var idfamilia = $(this).data("idfamilia");
                var per = ".personal-" + idfamilia;
                var personal = 0;
                personal = $("#tabla_Proyeccionadd").find(per).val();
                if (isNaN(personal)) { personal = 0 };

                if (parseInt(personal) > parseInt(personalmaximo)) {
                    $("#tabla_Proyeccionadd").find(per).addClass("personalproyectado");
                    $("#tabla_Proyeccionadd").find(per).attr('style', 'border :solid 2px red; color: red');
                } else {
                    $("#tabla_Proyeccionadd").find(per).removeClass("personalproyectado");
                    $("#tabla_Proyeccionadd").find(per).attr('style', 'border :');
                }
            });
        },
        editarCantidadFila: function () {
            $(document).on('click', '.btn-editar-tabla', function () {
                //
                var idfamilia = $(this).data("id");
                var btn = $(this);
                var hasClass = $(this).hasClass("btn-info");
                var per = ".personal-" + idfamilia;
                var cant = ".cantidad-" + idfamilia;
                var ent = ".entregas-" + idfamilia;
                var cos = ".costo-" + idfamilia;
                var costotal = ".costototal-" + idfamilia;
                if (hasClass) {
                    $("#tabla_Proyeccionadd").find(per).attr("disabled", false);
                    $("#tabla_Proyeccionadd").find(cant).attr("disabled", false);
                    $("#tabla_Proyeccionadd").find(ent).attr("disabled", false);
                    $("#tabla_Proyeccionadd").find(cos).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anteriorper = $("#tabla_Proyeccionadd").find(per).data("anterior");
                    var anteriorcant = $("#tabla_Proyeccionadd").find(cant).data("anterior");
                    var anteriorent = $("#tabla_Proyeccionadd").find(ent).data("anterior");
                    var anteriorcos = $("#tabla_Proyeccionadd").find(cos).data("anterior");
                    var anteriorcostototal = $("#tabla_Proyeccionadd").find(costotal).data("anterior");
                    $("#tabla_Proyeccionadd").find(per).val(anteriorper);
                    $("#tabla_Proyeccionadd").find(cant).val(anteriorcant);
                    $("#tabla_Proyeccionadd").find(ent).val(anteriorent);
                    $("#tabla_Proyeccionadd").find(cos).val(anteriorcos);
                    $("#tabla_Proyeccionadd").find(costotal).val(anteriorcostototal);
                    $("#tabla_Proyeccionadd").find(per).attr("disabled", true);
                    $("#tabla_Proyeccionadd").find(cant).attr("disabled", true);
                    $("#tabla_Proyeccionadd").find(ent).attr("disabled", true);
                    $("#tabla_Proyeccionadd").find(cos).attr("disabled", true);
                    $("#tabla_Proyeccionadd").find(per).removeClass("personalproyectado");
                    $("#tabla_Proyeccionadd").find(per).attr('style', 'border :');
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
                eventosIncrustados.ImporteTotal();
                //
            });
        },
        CalcularIndex: function () {
            var hayfilas = $("#tabla_Proyeccionadd tbody tr").length > 0;
            if (!hayfilas) {
                $('#tabla_Proyeccionadd tbody').html('<tr><td></td><td colspan="7" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
            $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                var trs = index + 1;
                $(this).children('td').eq(0).text(trs);
            })
        },
        EliminarFila: function () {
            $(document).on('click', '.BorrarFilaSubMenu', function (event) {
                event.preventDefault();
                var fila = $(this).closest('tr');
                var iddetalle = $(this).attr("id");
                let arrayid = iddetalle.split('_');
                var id = arrayid[1];

                if ($IdProyeccion.val()) {
                    if (id > 0) {
                        Swal.fire({
                            title: "¿Quiere modificar el estado del registro?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Aceptar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: "/eliminar-detalle",
                                    type: "POST",
                                    data: {
                                        id: id
                                    }
                                }).done(function () {
                                    Swal.fire({
                                        icon: "success",
                                        allowOutsideClick: false,
                                        title: "Éxito",
                                        text: "Registro modificado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then((result) => {
                                        fila.remove();
                                        eventosIncrustados.CalcularIndex();
                                        eventosIncrustados.ImporteTotal();
                                    });
                                }).fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al modificar el registro.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                });
                            }
                        });
                    } else {
                        fila.remove();
                    }

                } else {
                    fila.remove();
                }
                eventosIncrustados.CalcularIndex();
                eventosIncrustados.ImporteTotal();
            });
        },
        CostoTotal: function () {
            $(document).on('keyup', '.calc', function () {
                var idfamilia = $(this).data("idfamilia");
                var per = ".personal-" + idfamilia;
                var cant = ".cantidad-" + idfamilia;
                var ent = ".entregas-" + idfamilia;
                var cos = ".costo-" + idfamilia;
                var costotal = ".costototal-" + idfamilia;

                var personal = 0;
                var cantidad = 0;
                var entregas = 0;
                var costo = 0;
                var costototal = 0;

                personal = $("#tabla_Proyeccionadd").find(per).val();
                if (isNaN(personal)) { personal = 0 };
                cantidad = $("#tabla_Proyeccionadd").find(cant).val();
                if (isNaN(cantidad)) { cantidad = 0 };
                entregas = $("#tabla_Proyeccionadd").find(ent).val();
                if (isNaN(entregas)) { entregas = 0 };
                costo = $("#tabla_Proyeccionadd").find(cos).val();
                if (isNaN(costo)) { costo = 0 };

                costototal = personal * cantidad * entregas * costo;

                $("#tabla_Proyeccionadd").find(costotal).val(costototal.toFixed(2));

                eventosIncrustados.ImporteTotal();
            });       

        },
        ImporteTotal: function () {
            var importetotal = 0;
            $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                costototal = $(this).find(".cstototal").val();
                if (isNaN(costototal)) { costototal = 0 };
                importetotal = (parseFloat(importetotal) + parseFloat(costototal)).toFixed(2);
                if (isNaN(importetotal)) { importetotal = 0 };
            })
            $FormularioProyeccion.find("[name='TXTIMPORTE']").val(importetotal);
        },
        botonAgregarFamilias: function () {
            $btnAgregarFamilias.on("click", function () {
                for (var i = 0; i < tablafamilias.memory_familia.length; i++) {
                    var fam_in_table = false;
                    $("#tabla_Proyeccionadd tbody tr").each(function (index) {


                        var idfamilia = $(this).children('td').find("[class='idfamilia']").val();
                        if (tablafamilias.memory_familia[i].IDFAMILIA == idfamilia) {
                            fam_in_table = true;
                        }
                    })
                    if (!fam_in_table) {
                        var trs = i + 1
                        eventosIncrustados.formatoFilaTabla(trs, 0, tablafamilias.memory_familia[i].IDFAMILIA, tablafamilias.memory_familia[i].DESCR, "", "", "", "", "", false, false);
                    }
                }
                tablafamilias.memory_familia = [];
                $modalFamilias.modal('hide');
                eventosIncrustados.CalcularIndex();
            })

        },
        botonBuscar: function () {
            $btnBuscar.on("click", function () {
                tablafamilias.memory_familia = [];
                tablafamilias.reload();

                $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                    var idfamilia = $(this).children('td').find("[class='idfamilia']").val();
                    if (idfamilia) {
                        var CheckII = arrayfamilias.find(x => x.IDFAMILIA == idfamilia);
                        if (CheckII) {
                        } else {
                            arrayfamilias.push({
                                IDFAMILIA: idfamilia,
                            });
                        }
                    }
                })
                //console.log(arrayfamilias);
            })
        },
        botonAprobar: function () {
            $btnAprobar.on("click", function () {
                if ($IdProyeccion.val()) {
                    Swal.fire({
                        title: "¿Está seguro que desea aprobar la proyección?",
                        icon: "warning",
                        text: "Una vez aprobado no se podrá hacer cambios.",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                        cancelButtonText: "Cancelar"
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: "/proyeccion/actualizar-aprobado",
                                type: "POST",
                                data: {
                                    id: $IdProyeccion.val(),
                                }
                            }).done(function () {
                                Swal.fire({
                                    icon: "success",
                                    allowOutsideClick: false,
                                    title: "Éxito",
                                    text: "Registro modificado satisfactoriamente.",
                                    confirmButtonText: "Aceptar"
                                }).then((result) => {
                                    $('#tabla_Proyeccionadd tbody tr').remove();
                                    obtenerDatos.init();
                                });
                            }).fail(function (e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Error al modificar el registro.",
                                    text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                    confirmButtonText: "Aceptar"
                                });
                            });
                        }
                    });
                }
            })
        },
        botonGrabar: function () {
            $btnGuardar.on("click", function () {

                var error = $("#tabla_Proyeccionadd tbody tr").find('.personalproyectado');
                if (error.length) {
                    var mensaje = "";
                    if (error.length == 1) {
                        mensaje = "En la tabla Subfamilias, existe " + error.length + " registro que supera al personal proyectado.";
                    } else {
                        mensaje = "En la tabla Subfamilias, existen " + error.length + " registros que superan al personal proyectado.";
                    }
                    Swal.fire({
                        title: "Hay inconsistencias",
                        icon: "warning",
                        text: mensaje,
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                var fila = $("#tabla_Proyeccionadd tbody").find('.noData');
                if (fila.length) {
                    Swal.fire({
                        title: "No se puede crear la Proyección sin Subfamilias asociadas.",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                if ($FormularioProyeccion.find("[name='TXTIMPORTE']").val() <= 0) {
                    Swal.fire({
                        title: "No se puede crear la Proyección con un Importe Proyectado igual 0",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                if ($FormularioProyeccion.valid()) {
                    $btnGuardar.attr("disabled", true);
                    var familias = [];
                    $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                        id = $(this).find("[class='idfamilia']").val();
                        per = $(this).find(".per").val();
                        cant = $(this).find(".cant").val();
                        ent = $(this).find(".ent").val();
                        csto = $(this).find(".csto").val();
                        familias.push(
                            {

                                IDFAMILIA: id,
                                PERSONAL: per,
                                CANTIDAD: cant,
                                ENTREGAS: ent,
                                COSTO: csto,
                            }
                        );
                    })
                    //console.log(JSON.stringify(familias));
                    var formData = new FormData();
                    formData.append("FAMILIAS", JSON.stringify(familias));
                    formData.append("DSCRPCN", $FormularioProyeccion.find("[name='PRYCCN']").val());
                    formData.append("ANIO", $FormularioProyeccion.find("[name='ANIO']").val());
                    formData.append("GDTPRYCCN", $FormularioProyeccion.find("[name='TPPRYCCN']").val());
                    formData.append("GDTMNDA", $FormularioProyeccion.find("[name='GDTMNDA']").val());
                    formData.append("IMPRTE", $FormularioProyeccion.find("[name='TXTIMPORTE']").val());
                    formData.append("PRSNLPRYCTDO", $FormularioProyeccion.find("[name='TXTPERSONAL']").val());
                    formData.append("APRBDOX", $FormularioProyeccion.find("[name='APBDOX']").val());

                    if ($selectTipoProyeccion.val() == 1) {
                        formData.append("MSINCO", 1);
                        formData.append("MSFN", 12);
                    } else {
                        formData.append("MSINCO", $FormularioProyeccion.find("[name='MSINCO']").val());
                        formData.append("MSFN", $FormularioProyeccion.find("[name='MSFN']").val());
                    }

                    formData.append("GDESTDO", $FormularioProyeccion.find("[name='GDESTDO']").val());
                    //console.log(formData);
                    $.ajax({
                        url: "/proyeccion/insertar",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                RedirectWithSubfolder(`/proyeccion/editar/${e}`);
                                eventosIncrustados.CalcularIndex();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                        });


                    $btnGuardar.attr("disabled", false);
                }

            })
        },
        botonAgregar: function () {
            $btnAgregar.on("click", function () {
                var txtbusqueda = $txtBusqueda.val();
                if (txtbusqueda == "") {
                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    $("#txtBusqueda").addClass('error');
                    $ValtxtBusqueda.after('<label id="txtBusqueda-error" class="error VtxtBusqueda">Ingrese Familia </label>');
                    return;
                } else {
                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    var dataencontrada = $dataecontrada.val()
                    Arrayiddesc = dataencontrada.split('-');  //  SEPARA EL ID FAMILIA - DESCRIPCIÓN
                    var id = Arrayiddesc[0].trim();
                    var desc = Arrayiddesc[1].toString();
                    var flag = false;

                    $("#tabla_Proyeccionadd tbody tr").each(function (index) {
                        var idfamilia = $(this).children('td').find("[class='idfamilia']").val();
                        if (id == idfamilia) {
                            flag = true
                        }
                    });

                    if (flag) {
                        $txtBusqueda.val("");
                        $dataecontrada.val("");
                        $txtBusqueda.attr("disabled", false);
                        $txtBusqueda.focus();
                        eventosIncrustados.CalcularIndex();
                        $("#txtBusqueda").removeClass('error');
                        $(".VtxtBusqueda").remove();
                        return;
                    }

                    var trs = $("#tabla_Proyeccionadd tbody tr").length + 1;
                    eventosIncrustados.formatoFilaTabla(trs, 0, id.trim(), desc.trim(), "", "", "", "", "", false, false);

                    $txtBusqueda.val("");
                    $dataecontrada.val("");
                    $txtBusqueda.attr("disabled", false);
                    $txtBusqueda.focus();
                }
                eventosIncrustados.CalcularIndex();
                eventosIncrustados.ImporteTotal();
            })

        },
        botonLimpiar: function () {
            $btnLimpiar.on("click", function () {
                $txtBusqueda.val("");
                $dataecontrada.val("");
                $txtBusqueda.attr("disabled", false);
                $txtBusqueda.focus();
            })
        },
        validarbusqueda: function () {
            $txtBusqueda.on("keyup", function () {
                var txtBusqueda = $txtBusqueda.val();
                if (txtBusqueda != "") {
                    $txtBusqueda.removeClass('error');
                    $(".VtxtBusqueda").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
        },
        formatoFilaTabla: function (trs, id, idfamilia, desc, personal, cantidad, entregas, costo, costotal, obteniendo = false, FlagEstado = false) {
            var $filanodatos = $("#tabla_Proyeccionadd tbody").find('.noData').parent();
            if ($filanodatos) {
                $filanodatos.remove();
            }
            //FUSION DE LAS 3
            var htmlTags = "<tr>" +
                '<td class="text-center"><label>' + trs + '</label></td>' +
                '<td class="text-left"><label>' + desc + '</label>' +
                '<input class="iddetalle" type="hidden" value="' + id + '">' +
                '<input class="idfamilia" type="hidden" value="' + idfamilia + '">' +
                '</td > ' +
                '<td class=""><input data-idfamilia="' + idfamilia + '" class="per personal-' + idfamilia + '    text-uppercase solonumeros calc text-right form-control form-control-sm" data-anterior="' + personal + '" value="' + personal + '" maxlength="8" /></td>' +
                '<td class=""><input data-idfamilia="' + idfamilia + '"  class="cant cantidad-' + idfamilia + '  text-uppercase solonumeros calc text-right form-control form-control-sm" data-anterior="' + cantidad + '" value="' + cantidad + '" maxlength="8" /></td>' +
                '<td class=""><input data-idfamilia="' + idfamilia + '"  class="ent entregas-' + idfamilia + ' text-uppercase solonumeros calc text-right form-control form-control-sm" data-anterior="' + entregas + '" value="' + entregas + '" maxlength="8" /></td>' +
                '<td class=""><input data-idfamilia="' + idfamilia + '"  class="csto costo-' + idfamilia + '  text-uppercase decimales calc text-right form-control form-control-sm" data-anterior="' + costo + '" value="' + costo + '" maxlength="8" /></td>' +
                '<td class=""><input  class="cstototal costototal-' + idfamilia + '  text-uppercase text-right form-control form-control-sm" disabled="disabled" form-control-sm"  data-anterior="' + costotal + '" value="' + costotal + '"/></td>' +
                '<td class="text-center">';
            if (!FlagEstado) {
                htmlTags += '<button type="button" title="Editar" data-id="' + idfamilia + '" class="btn btn-info btn-xs btn-editar-tabla "><span><i class="la la-edit"></i></button> ';
                if (obteniendo) {
                    htmlTags += ' <button id="delete_' + id + '" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                } else {
                    htmlTags += ' <button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                }
            } else {
                htmlTags += '';
            }
            htmlTags += '</td>' +
                '</tr>';
            $('#tabla_Proyeccionadd tbody').append(htmlTags);
        },
        init: function () {
            eventosIncrustados.CalcularIndex();
            eventosIncrustados.EliminarFila(); 
            eventosIncrustados.botonBuscar();
            eventosIncrustados.changePersonalProyectado();
            eventosIncrustados.botonAprobar();
            eventosIncrustados.botonAgregar();
            eventosIncrustados.botonAgregarFamilias();
            eventosIncrustados.botonLimpiar();
            eventosIncrustados.botonGrabar();
            eventosIncrustados.validarbusqueda();
            eventosIncrustados.editarCantidadFila();
            eventosIncrustados.CostoTotal();
            eventosIncrustados.ImporteTotal();
        }
    }

    var autocomplete = {
        init: function () {
            $txtBusqueda.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/searchfamilias",
                        dataType: "json",
                        data: {
                            term: request.term,
                        },
                        success: function (data) {
                            //console.log(data);
                            if (data.length == 1) {
                                $txtBusqueda.val(data[0].label);
                                $dataecontrada.val(data[0].value2);
                                $txtBusqueda.attr("disabled", true);
                            } else {
                                response(data);
                            }
                        },
                        error: function (xhr, status, error) {
                            alert(xhr + " " + status + " " + error);
                        },
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    $txtBusqueda.val(ui.item.label);
                    $dataecontrada.val(ui.item.value2);
                    $txtBusqueda.attr("disabled", true);
                    $("#txtObs").focus();

                    return false;
                }
            });
        }
    }

    var obtenerDatos = {
        obtenerDetalle: function (FlagEstado, Familias) {
            data = JSON.parse(Familias);
            if (data.length) {
                $('#tabla_articuloadd tbody').html('');
                for (var i = 0; i < data.length; i++) {
                    var trs = i + 1;
                    var costotal = data[i].PRSNL * data[i].CNTDD * data[i].ENTRGSANIO * data[i].CSTO
                    eventosIncrustados.formatoFilaTabla(trs, data[i].ID, data[i].IDFMLA, data[i].SBFMLA, data[i].PRSNL, data[i].CNTDD, data[i].ENTRGSANIO, data[i].CSTO, costotal, true, FlagEstado);
                }
            } else {
                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
            eventosIncrustados.CostoTotal();
        },
        init: function () {
            var FlagEstado = false
            $.get("/proyeccion/obtener", { id: $IdProyeccion.val() })
                .done(function (data) {
                    //console.log(JSON.stringify(data));
                    $FormularioProyeccion.find("[name='PRYCCN']").val(data.dscrpcn);
                    $FormularioProyeccion.find("[name='ANIO']").val(data.anio);

                    $FormularioProyeccion.find("[name='TPPRYCCN']").val(data.gdtpryccn);

                    if (data.gdtpryccn == 1) {
                        $DivFechaIni.hide();
                        $DivFechaFin.hide();
                    } else {
                        $DivFechaIni.show();
                        $DivFechaFin.show();
                    }

                    $FormularioProyeccion.find("[name='GDTMNDA']").val(data.gdtmnda);
                    $FormularioProyeccion.find("[name='TXTIMPORTE']").val(data.imprte.toFixed(2));
                    $FormularioProyeccion.find("[name='TXTPERSONAL']").val(data.prsnlpryctdo);
                    $FormularioProyeccion.find("[name='MSINCO']").val(data.msinco);
                    $FormularioProyeccion.find("[name='MSFN']").val(data.msfn);
                    $FormularioProyeccion.AgregarCamposAuditoria(data);

                    if (data.aprbdox) {
                        $FormularioProyeccion.find("[name='APBDOX']").val(data.aprbdox);
                        $DivtxtAprobado.show();
                        $DivbtnAprobado.hide();
                        $FormularioProyeccion.find(":input").attr("disabled", true);
                        $("#flstabla").attr("disabled", true);
                        $FormularioProyeccion.find("[name='ANIO']").attr("style", "background: #e9ecef");
                        data.gdestdo = 'I';
                    } else {
                        $DivtxtAprobado.hide();
                        $DivbtnAprobado.show();
                        $btnAprobar.attr("disabled", false);
                    }

                    if (data.gdestdo == 'I') {
                        FlagEstado = true;
                        $DivBusqueda.remove();
                        $btnGuardar.remove();
                        $FormularioProyeccion.find(":input").attr("disabled", true);
                        $("#flstabla").attr("disabled", true);
                        $FormularioProyeccion.find("[name='ANIO']").attr("style", "background: #e9ecef");
                    }

                    obtenerDatos.obtenerDetalle(FlagEstado, data.familias);
                });
        }
    };

    return {
        init: function () {
            selects.init();
            datepickers.init();
            Solodecimales.init();
            eventosIncrustados.init();
            autocomplete.init();
            modalFamilia.init();
            tablafamilias.inicializador();
            if ($IdProyeccion.val()) {
                obtenerDatos.init();
            } else {
                $FormularioProyeccion.AgregarCamposDefectoAuditoria();
                $FormularioProyeccion.DeshabilitarCamposAuditoria();
                $DivbtnAprobado.remove();
                $('#tabla_articuloadd tbody').html('');
                $('#tabla_articuloadd tbody').html('<tr class="noData"><td></td><td colspan="7" class="text-center" >No hay datos disponibles</td></tr>');
            }

        }
    };
}();

$(() => {
    InicializarProyeccionDatos.init();
})

