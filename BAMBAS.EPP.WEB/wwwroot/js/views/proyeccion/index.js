﻿var InicializarProyeccion = function () {
    var $tablaProyeccion = $("#tabla_proyeccion");
    var $btnProyeccionNew = $("#btnProyeccionNew");
    var $accesoAgregarProyeccion = $("#accesoAgregarProyeccion");
    var $accesoEditarProyeccion = $("#accesoEditarProyeccion");
    var $accesoEliminarProyeccion = $("#accesoEliminarProyeccion");
    var $txtanio = $("#txtanio");
    var $txtProyeccion = $("#txtProyeccion");
    var $btnBuscar = $('#btnBuscar');

    var validacionControles = {
        init: function () {
            if ($accesoAgregarProyeccion.val() == "False") {
                $btnProyeccionNew.remove();
            }
        }
    };

    var tablaProyeccion = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/proyeccion/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.dscrpcn = $txtProyeccion.val();
                    data.anio = $txtanio.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Año",
                    className: "text-center",
                    data: "anio",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Moneda",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false
                },
                {
                    title: "Importe",
                    className: "text-right",
                    data: "imprte",
                    orderable: false,
                    render: function (data) {
                        return data.toFixed(2)
                    }
                },
                {
                    title: "U. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "uedcn",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Real vs Proyectado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<button title="Cambiar estado" data-id="${data.id}" class="btn btn-warning btn-xs btn-real"><span><i class="la la-chart-bar"></i><span></span></span></button>`;
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarProyeccion.val() == "True") {
                            tpm += `<button title="Editar" data-id="${data.id}" class="btn btn-info btn-xs btn-editar"><span><i class="la la-edit"></i></button>`;                            
                        }

                        if ($accesoEliminarProyeccion.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaProyeccion.objeto.on("click", ".btn-real", function () {
                var id = $(this).data("id");
                RedirectWithSubfolder(`/proyeccion/realvsproyectado/${id}`);
            });
            tablaProyeccion.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                RedirectWithSubfolder(`/proyeccion/editar/${id}`);
            });
            tablaProyeccion.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/proyeccion/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaProyeccion.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaProyeccion.objeto = $tablaProyeccion.DataTable(tablaProyeccion.opciones);
            tablaProyeccion.eventos();
        }
    };

    var datepickers = {
        anio: function () {
            $txtanio.datepicker({
                format: "yyyy",
                minViewMode: "years"
            });
        },
        init: function () {
            this.anio();
        }
    };

    var eventosIncrustados = {
        botonBuscar: function () {
            $btnBuscar.on("click", function () {        
                tablaProyeccion.reload();
            })
        },
        init: function () {
            eventosIncrustados.botonBuscar();
        }
    };


    return {
        init: function () {
            tablaProyeccion.inicializador();
            validacionControles.init();
            datepickers.init();
            eventosIncrustados.init();
        }
    };
}();

$(() => {
    InicializarProyeccion.init();
})

