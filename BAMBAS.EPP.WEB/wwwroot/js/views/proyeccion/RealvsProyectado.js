﻿var InicializarRealvsProyectado = function () {
    var $FormularioRealvsProyectado = $("#RealvsProyectado_form");
    var $IdProyeccion = $("#IdProyeccion");
    var $selectEstados = $(".select-estados");
    var $selectMoneda = $(".select-moneda");
    var $lblMeses = $("#lblMeses");
    var $btnDetalle = $("#btnDetalle");
    var $btnDescargar = $("#btnDescargar");
    var $DivbtnDescarga = $("#DivbtnDescarga");

    var DETALLE = [];
    var MesesArray = new Array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
            $selectMoneda.LlenarSelectGD("GDTMNDA");
        }
    };

    var eventosIncrustados = {
        botonDescargar: function () {
            $btnDescargar.on("click", function () {
                var namefile = $FormularioRealvsProyectado.find("[name='PRYCCN']").val() + '.xlsx';
                var name = $FormularioRealvsProyectado.find("[name='PRYCCN']").val();
                var wb = XLSX.utils.book_new();
                wb.Props = {
                    Title: name,
                    Subject: name,
                    Author: "CEPHEID SAC",
                    CreatedDate: new Date()
                };
                wb.SheetNames.push(name);
                var ws = XLSX.utils.table_to_sheet(document.getElementById('tabla_Detalle'));

                wb.Sheets[name] = ws;
                var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), namefile);

                //var wb = XLSX.utils.table_to_book(document.getElementById('tabla_Detalle'), { sheet: "Sheet JS" });
                //var wbout = XLSX.write(wb, { bookType: 'xlsx', rgb: "FFFFAA00" , type: 'binary' });
                //function s2ab(s) {
                //    var buf = new ArrayBuffer(s.length);
                //    var view = new Uint8Array(buf);
                //    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                //    return buf;
                //}
                //saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), 'test.xlsx');
            });
        },
        botonDetalle: function () {
            $btnDetalle.on("click", function () {
                var hasClass = $(this).hasClass("btn-info");
                if (hasClass) {
                    $btnDetalle.removeClass("btn-info");
                    $btnDetalle.find("i").removeClass("la-angle-double-down");
                    $btnDetalle.addClass("btn-secondary ");
                    $btnDetalle.find("i").addClass("la-angle-double-up");
                    $btnDetalle.find("span").text("Ocultar Detalle");
                    $DivbtnDescarga.show();
                    eventosIncrustados.formatoTabla();

                } else {
                    $btnDetalle.removeClass("btn-secondary ");
                    $btnDetalle.find("i").removeClass("la-angle-double-up");
                    $btnDetalle.addClass("btn-info");
                    $btnDetalle.find("i").addClass("la-angle-double-down");
                    $btnDetalle.find("span").text("Ver Detalle");
                    $DivbtnDescarga.hide();
                    $('#tabla_Detalle thead tr').remove();
                    $('#tabla_Detalle tbody tr').remove();
                }
            });
        },
        formatoTabla: function () {

            var mesactual = new Date().getMonth() + 1;

            var htmlHead = "<tr>" +
                '<th rowspan="3" width="2%" class="text-center"></th>' +
                '<th rowspan = "3" class="align-middle" > SubFamilia</th >';
            for (let i = 0; i < mesactual; i++) {
                htmlHead += '<th colspan="4" class="text-center">' + MesesArray[i].toString() + '</th>';
            }
            htmlHead += '</tr > ';
            htmlHead += "<tr>";
            for (let i = 0; i < mesactual; i++) {
                htmlHead +=
                    '<th colspan="2" class="text-center">Cantidad</th>' +
                    '<th colspan="2" class="text-center">Costo</th>';
            }
            htmlHead += '</tr > ';
            htmlHead += '</tr > ';
            htmlHead += "<tr>";
            for (let i = 0; i < mesactual; i++) {
                htmlHead +=
                    '<th class="text-center">Proy</th>' +
                    '<th class="text-center">Real</th>' +
                    '<th class="text-center">Proy</th>' +
                    '<th class="text-center">Real</th>';
            }
            htmlHead += '</tr > ';
            $('#tabla_Detalle thead').append(htmlHead);

            var htmlTags = "<tr>";
            for (let i = 0; i < DETALLE.length; i++) {
                var trs = i + 1;
                htmlTags +=
                    '<td class="text-center"><label>' + trs + '</label></td>' +
                    '<td class="text-center"><label>' + DETALLE[i].SBFMLA + '</label></td>';
                for (let j = 0; j < mesactual; j++) {
                    htmlTags +=
                        '<td class="text-center"><label>' + (DETALLE[i].CNTDD / 12).toFixed(2) + '</label></td>' +
                        '<td class="text-center"><label>' + "0" + '</label></td>' +
                    '<td class="text-center"><label>' + (DETALLE[i].CSTO / 12).toFixed(2)  + '</label></td>' +
                        '<td class="text-center"><label>' + "0" + '</label></td>';
                }
            }
            htmlTags += '</tr > ';

            $('#tabla_Detalle tbody').append(htmlTags);
        },
        init: function () {
            eventosIncrustados.botonDetalle();
            eventosIncrustados.botonDescargar();
        }
    };

    var obtenerDatos = {
        init: function () {
            $.get("/proyeccion/obtener", { id: $IdProyeccion.val() })
                .done(function (data) {

                    dtlle = JSON.parse(data.familias);
                    if (dtlle.length) {
                        for (var i = 0; i < dtlle.length; i++) {
                            DETALLE.push({
                                SBFMLA: dtlle[i].SBFMLA,
                                CNTDD: dtlle[i].PRSNL * dtlle[i].CNTDD * dtlle[i].ENTRGSANIO,
                                CSTO: dtlle[i].PRSNL * dtlle[i].CNTDD * dtlle[i].ENTRGSANIO * dtlle[i].CSTO,
                            });
                        }
                    }

                    $FormularioRealvsProyectado.find("[name='PRYCCN']").val(data.dscrpcn);
                    $FormularioRealvsProyectado.find("[name='ANIO']").val(data.anio);
                    $FormularioRealvsProyectado.find("[name='GDTMNDA']").val(data.gdtmnda);
                    $FormularioRealvsProyectado.find("[name='TXTIMPORTE']").val(data.imprte);
                    $FormularioRealvsProyectado.find("[name='TXTPERSONAL']").val(data.prsnlpryctdo);
                    $FormularioRealvsProyectado.find("[name='APBDOX']").val(data.aprbdox);

                    $FormularioRealvsProyectado.AgregarCamposAuditoria(data);
                    $FormularioRealvsProyectado.find(":input").attr("disabled", true);
                    $btnDetalle.attr("disabled", false);
                    $('#btnDescargar').attr("disabled", false);

                    var mesactual = new Date().getMonth();
                    var valor = (data.imprte / 12) * (mesactual + 1);

                    $lblMeses.text(MesesArray[parseInt(data.msinco) - 1].toString() + " - " + MesesArray[mesactual].toString());

                    $("#sliderProy").ionRangeSlider({
                        skin: "big",
                        min: 0,
                        max: data.imprte,
                        from: valor,
                        grid_num: 1,
                        grid: true,
                        from_fixed: true,
                        prefix: "S/ ",
                    });

                    $('#sliderReal').ionRangeSlider({
                        skin: "big",
                        min: 0,
                        grid_num: 1,
                        grid: false,
                        hide_min_max: true,
                        from_fixed: true,
                        prefix: "S/ ",
                    });



                });
        }
    };

    return {
        init: function () {
            $DivbtnDescarga.hide();
            selects.init();
            obtenerDatos.init();
            eventosIncrustados.init();
        }
    };
}();

$(() => {
    InicializarRealvsProyectado.init();
})

