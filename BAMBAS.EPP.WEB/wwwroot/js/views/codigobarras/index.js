﻿var InicializarProyeccion = function () {
    var $accesoImprimirCodigoBarras = $("#accesoImprimirCodigoBarras");
    var $selectEstados = $(".select-estados");
    var $txtanio = $("#txtanio");
    var $txtProyeccion = $("#txtProyeccion");
    var $btnImprimir = $('#btnImprimir');

    var validacionControles = {
        init: function () {
            if ($accesoImprimirCodigoBarras.val() == "False") {
                $btnImprimir.remove();
            }
        }
    };

    var eventosIncrustados = {
        botonImprimir: function () {
            $btnImprimir.on("click", function () {

            })
        },
        init: function () {
            eventosIncrustados.botonImprimir();
        }
    };


    return {
        init: function () {
            validacionControles.init();
            eventosIncrustados.init();
        }
    };
}();

$(() => {
    InicializarProyeccion.init();
})

