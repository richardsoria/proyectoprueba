﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Guardia;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface IGuardiaProxy
    {
        Task<List<GuardiaDto>> ObtenerActivas();
        Task<GuardiaDto> ObtenerPorId(int id);         
    }
    public class GuardiaProxy : IGuardiaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GuardiaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
    
        public async Task<List<GuardiaDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}guardia/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GuardiaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
     
        public async Task<GuardiaDto> ObtenerPorId(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}guardia/obtenerXid?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<GuardiaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
