﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Familia;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface IFamiliaProxy
    {
        Task<List<FamiliaDto>> ObtenerActivas(int idFamilia);
        Task<List<FamiliaDto>> ObtenerAutocomplete(string term);
        Task<DataTablesStructs.ReturnedData<FamiliaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false);
    }
    public class FamiliaProxy : IFamiliaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public FamiliaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<FamiliaDto>> ObtenerActivas(int idFamilia)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-activas?idFamilia={idFamilia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<FamiliaDto>> ObtenerAutocomplete(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/ObtenerAutocomplete?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<FamiliaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-tabla?idFamilia={idFamilia}&soloHijos={soloHijos}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<FamiliaDto>(-2);
        }
    }
}
