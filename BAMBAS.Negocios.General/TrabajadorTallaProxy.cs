﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.TrabajadorTalla;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface ITrabajadorTallaProxy
    {
        Task<DataTablesStructs.ReturnedData<TrabajadorTallaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador);
        Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagtlla, int? idgrupocargo);
        Task<List<TrabajadorTallaDto>> ObtenerTodas(int idTrabajador);
        Task<RespuestaConsulta> Actualizar(ActualizarTallaDto TrabajadorTalla);
        Task<RespuestaConsulta> EliminarDetalleTalla(ActualizarTallaDto TrabajadorTalla);
        Task<List<TrabajadorTallaDto>> ListarDetalleTalla(int idTrabajador);

    }
    public class TrabajadorTallaProxy : ITrabajadorTallaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public TrabajadorTallaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<TrabajadorTallaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}TrabajadorTalla/obtener-tabla?idTrabajador={idTrabajador}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<TrabajadorTallaDto>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagtlla, int? idgrupocargo)
        {
            var url = $"{_apiUrls.SeguridadUrl}TrabajadorTalla/obtener-tabla-articulos?flagtlla={flagtlla}&idgrupocargo={idgrupocargo}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloDto>(-2);
        }
        public async Task<List<TrabajadorTallaDto>> ListarDetalleTalla(int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}TrabajadorTalla/listar-detalle-lista?idTrabajador={idTrabajador}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorTallaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<TrabajadorTallaDto>> ObtenerTodas(int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}TrabajadorTalla/obtener-todas?idTrabajador={idTrabajador}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorTallaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        
        public async Task<RespuestaConsulta> Actualizar(ActualizarTallaDto TrabajadorTalla)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(TrabajadorTalla),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}TrabajadorTalla/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> EliminarDetalleTalla(ActualizarTallaDto TrabajadorTalla)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(TrabajadorTalla),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}TrabajadorTalla/eliminar-detalle-talla", content);
            return request.Respuesta(-2, nameof(EliminarDetalleTalla));
        }

    }
}

