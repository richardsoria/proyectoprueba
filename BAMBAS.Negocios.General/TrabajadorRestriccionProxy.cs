﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.TrabajadorRestriccion;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface ITrabajadorRestriccionProxy
    {
        Task<DataTablesStructs.ReturnedData<TrabajadorRestriccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador);
        Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagrestrccion, int? idgrupocargo);
        Task<List<TrabajadorRestriccionDto>> ObtenerTodas(int idTrabajador);
        Task<RespuestaConsulta> Actualizar(ActualizarRestriccionDto TrabajadorRestriccion);
        Task<RespuestaConsulta> EliminarDetalleRestricciones(ActualizarRestriccionDto TrabajadorRestriccion);
        Task<List<TrabajadorRestriccionDto>> ListarDetalleRestriccion(int idTrabajador);

    }
    public class TrabajadorRestriccionProxy : ITrabajadorRestriccionProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public TrabajadorRestriccionProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<TrabajadorRestriccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}restricciones/obtener-tabla?idTrabajador={idTrabajador}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<TrabajadorRestriccionDto>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagrestrccion, int? idgrupocargo)
        {
            var url = $"{_apiUrls.SeguridadUrl}restricciones/obtener-tabla-articulos?flagrestrccion={flagrestrccion}&idgrupocargo={idgrupocargo}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloDto>(-2);
        }
        public async Task<List<TrabajadorRestriccionDto>> ListarDetalleRestriccion(int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}restricciones/listar-detalle-lista?idTrabajador={idTrabajador}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorRestriccionDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<TrabajadorRestriccionDto>> ObtenerTodas(int idTrabajador)
        {
            var url = $"{_apiUrls.SeguridadUrl}restricciones/obtener-todas?idTrabajador={idTrabajador}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorRestriccionDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        
        public async Task<RespuestaConsulta> Actualizar(ActualizarRestriccionDto TrabajadorRestriccion)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(TrabajadorRestriccion),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}restricciones/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> EliminarDetalleRestricciones(ActualizarRestriccionDto TrabajadorRestriccion)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(TrabajadorRestriccion),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}restricciones/eliminar-detalle-restricciones", content);
            return request.Respuesta(-2, nameof(EliminarDetalleRestricciones));
        }
    }
}

