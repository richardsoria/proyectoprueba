﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{

    public interface IArticuloProxy
    {
        Task<List<ArticuloDto>> ObtenerActivas();
        Task<List<ArticuloDto>> ObtenerAutocomplete(string term, bool flagKit);
        Task<List<ArticuloDto>> ObtenerAutocompleteTalla(string term);
        Task<List<ArticuloDto>> ObtenerAutocompleteRestricciones(string term);
    }
    public class ArticuloProxy : IArticuloProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public ArticuloProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<ArticuloDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<List<ArticuloDto>> ObtenerAutocomplete(string term, bool flagKit)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocomplete?term={term}&flagKit={flagKit}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<ArticuloDto>> ObtenerAutocompleteTalla(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocompleteTalla?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<ArticuloDto>> ObtenerAutocompleteRestricciones(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocompleteRestricciones?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
