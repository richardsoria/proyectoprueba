﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoDato;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface IGrupoDatoProxy
    {
        Task<List<CatalogoGrupoDatoCustom>> ObtenerCatalogos();
    }
    public class GrupoDatoProxy : IGrupoDatoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GrupoDatoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<CatalogoGrupoDatoCustom>> ObtenerCatalogos()
        {

            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}grupo-dato/obtener-catalogo");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<CatalogoGrupoDatoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
