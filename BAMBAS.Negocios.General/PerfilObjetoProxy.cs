﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.PerfilObjeto;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Seguridad
{
    public interface IPerfilObjetoProxy
    {
        Task<List<PerfilObjetoDetalleCustom>> ListarControles(string controles, string idPerfiles);
        Task<List<PerfilObjetoCustom>> ListarModulos(string idPerfil);
        Task<List<PerfilObjetoCustom>> ListarMenu(string idModulo, string idPerfil);
    }
    public class PerfilObjetoProxy : IPerfilObjetoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PerfilObjetoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<PerfilObjetoCustom>> ListarModulos(string idPerfil)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil-objeto/listar-modulos?idPerfil={idPerfil}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<PerfilObjetoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<List<PerfilObjetoCustom>> ListarMenu(string idModulo, string idPerfil)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil-objeto/listar-menu?idPerfil={idPerfil}&idModulo={idModulo}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<PerfilObjetoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<PerfilObjetoDetalleCustom>> ListarControles(string controles, string idPerfiles)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil-objeto/listar-controles?controles={controles}&idPerfiles={idPerfiles}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<PerfilObjetoDetalleCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
