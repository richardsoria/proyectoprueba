﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasDireccion
    {
        Task<DataTablesStructs.ReturnedData<DireccionModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id);
        Task<DireccionModel> ObtenerDireccion(string idprsna, string id);
        Task<List<DireccionModel>> ListarDireccion(string idprsna, string id);
    }
    public class ConsultasDireccion : IConsultasDireccion
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasDireccion(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<DireccionModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            var datos = await _configuracionConexionSql.EjecutarProcedimiento<DireccionModel>(ProcedimientosAlmacenados.Direccion.ListarDireccion, parametros);
            return datos.ConvertirTabla(parameters);
        }
        public async Task<List<DireccionModel>> ListarDireccion(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<DireccionModel>(ProcedimientosAlmacenados.Direccion.ListarDireccion, parametros);
        }

        public async Task<DireccionModel> ObtenerDireccion(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            parametros.Add("@IDPRSNA", idprsna);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<DireccionModel>(ProcedimientosAlmacenados.Direccion.ObtenerDireccion, parametros);
            return ret.Entidad;
        }
    }
}
