﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasDocumento
    {
        Task<List<DocumentoModel>> ListarDocumento(int idprsna, int id);
        Task<DocumentoModel> ObtenerDocumento(string idprsna, string id);
        Task<DataTablesStructs.ReturnedData<DocumentoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idprsna, string id);
        Task<string> ObtenerUltimoCRN();
    }
    public class ConsultasDocumento : IConsultasDocumento
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasDocumento(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<DataTablesStructs.ReturnedData<DocumentoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            var datos = await _configuracionConexionSql.EjecutarProcedimiento<DocumentoModel>(ProcedimientosAlmacenados.Documento.ListarDocumento, parametros);
            return datos.ConvertirTabla(parameters);
        }
        public async Task<List<DocumentoModel>> ListarDocumento(int idprsna, int id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<DocumentoModel>(ProcedimientosAlmacenados.Documento.ListarDocumento, parametros);
        }

        public async Task<DocumentoModel> ObtenerDocumento(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            parametros.Add("@IDPRSNA", idprsna);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<DocumentoModel>(ProcedimientosAlmacenados.Documento.ObtenerDocumento, parametros);
            return ret.Entidad;
        }

        public async Task<string> ObtenerUltimoCRN()
        {
            //var parametros = new DynamicParameters();
            //parametros.Add("@ID", "asdas");
            var ret = await _configuracionConexionSql.EjecutarFuncion<string>(ProcedimientosAlmacenados.Documento.ObtenerUltimoCRN);
            return ret;
        }
    }
}
