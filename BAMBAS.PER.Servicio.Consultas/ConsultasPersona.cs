﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using BAMBAS.PER.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasPersona
    {
        Task<DatosPersonaCustom> ObtenerDatosPersonas(string id);
        Task<DatosPersonaPrincipalCustom> ObtenerDatosPersonasPrincipal(string id);        
        Task<List<ListarPersonaCustom>> ListarPersonas(string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fehcaHasta, string estado);
        Task<DataTablesStructs.ReturnedData<ListarPersonaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fehcaHasta, string estado, bool? soloContratistas = null, bool? soloTrabajadores = null);
        Task<DataTablesStructs.ReturnedData<ListarPersonaCustom>> ObtenerColaboradoresDataTable(DataTablesStructs.SentParameters parameters, string idprsna);
        Task<DataTablesStructs.ReturnedData<HomonimoPersonaCustom>> listarHomonimo(DataTablesStructs.SentParameters parameters, string gddcmnto, string ndcmnto, string aptrno, string amtrno, string pnmbre, string snmbre);
    }
    public class ConsultasPersona : IConsultasPersona
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;
        public ConsultasPersona(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DatosPersonaCustom> ObtenerDatosPersonas(string id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var res = await _configuracionConexionSql.ObtenerPrimerRegistro<DatosPersonaCustom>(ProcedimientosAlmacenados.Persona.ObtenerDatosPersona, param);
            return res.Entidad;
        }
        public async Task<DatosPersonaPrincipalCustom> ObtenerDatosPersonasPrincipal(string id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var res = await _configuracionConexionSql.ObtenerPrimerRegistro<DatosPersonaPrincipalCustom>(ProcedimientosAlmacenados.Persona.ObtenerDatosPersonaPrincipal, param);
            return res.Entidad;
        }
        public async Task<List<ListarPersonaCustom>> ListarPersonas(string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fehcaHasta, string estado)
        {
            var param = new DynamicParameters();

            param.Add("@GDTPRSNA", gdtprsna);
            param.Add("@GDDCMNTO", tipoDocumento);
            param.Add("@NDCMNTO", numeroDocumento);
            param.Add("@DATOS", datos);
            param.Add("@DESDE", fechaDesde);
            param.Add("@HASTA", fehcaHasta);
            param.Add("@SOLOCONTRATISTAS", null);
            param.Add("@SOLOTRABAJADORES", null);
            param.Add("@ESTADO", estado);
            return await _configuracionConexionSql.EjecutarProcedimiento<ListarPersonaCustom>(ProcedimientosAlmacenados.Persona.ListarDatosPersona, param);
        }
        public async Task<DataTablesStructs.ReturnedData<ListarPersonaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fehcaHasta, string estado,bool? soloContratistas= null, bool? soloTrabajadores = null)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@GDTPRSNA", gdtprsna);
            parametros.Add("@GDDCMNTO", tipoDocumento);
            parametros.Add("@NDCMNTO", numeroDocumento);
            parametros.Add("@DATOS", datos);
            parametros.Add("@DESDE", fechaDesde);
            parametros.Add("@HASTA", fehcaHasta);
            parametros.Add("@SOLOCONTRATISTAS", soloContratistas);
            parametros.Add("@SOLOTRABAJADORES", soloTrabajadores);
            parametros.Add("@ESTADO", estado);
            var lista = await _configuracionConexionSql.EjecutarProcedimiento<ListarPersonaCustom>(ProcedimientosAlmacenados.Persona.ListarDatosPersona, parametros);
            return lista.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<ListarPersonaCustom>> ObtenerColaboradoresDataTable(DataTablesStructs.SentParameters parameters, string idprsna)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@DATOS", parameters.SearchValue);

            var lista = await _configuracionConexionSql.EjecutarProcedimiento<ListarPersonaCustom>(ProcedimientosAlmacenados.Persona.ListarColaboradores, parametros);
            return lista.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<HomonimoPersonaCustom>> listarHomonimo(DataTablesStructs.SentParameters parameters, string gddcmnto, string ndcmnto, string aptrno, string amtrno, string pnmbre, string snmbre)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@GDDCMNTO", gddcmnto);
            parametros.Add("@NDCMNTO", ndcmnto);  
            parametros.Add("@APTRNO", aptrno);    
            parametros.Add("@AMTRNO", amtrno);
            parametros.Add("@PNMBRE", pnmbre);
            parametros.Add("@SNMBRE", snmbre);
            var lista = await _configuracionConexionSql.EjecutarProcedimiento<HomonimoPersonaCustom>(ProcedimientosAlmacenados.Persona.listaHomonimos, parametros);
            return lista.ConvertirTabla(parameters);
        }
    }
}
