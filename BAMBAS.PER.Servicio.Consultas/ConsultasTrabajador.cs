﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAMBAS.PER.Servicio.Consultas.Objetos;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasTrabajador
    {
        Task<List<TrabajadorCustom>> ListarTrabajador();
        Task<TrabajadorCustom> ObtenerTrabajador(string id);
        Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param);
        Task<DataTablesStructs.ReturnedData<TrabajadorCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<TrabajadorCustom>> ListadoPersonalTareo(string csap, string datos, int idcntrtsta, int bambas); 
        Task<List<TrabajadorCustom>> ObtenerContratista();
    }
    public class ConsultasTrabajador : IConsultasTrabajador
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasTrabajador(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<TrabajadorCustom>> ListarTrabajador()
        {
            var parametros = new DynamicParameters();
            //parametros.Add("@IDPRSNA", idprsna);
            //parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ListarTrabajador, parametros);
        }

        public async Task<TrabajadorCustom> ObtenerTrabajador(string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ObtenerTrabajador, parametros);
            return ret.Entidad;
        }
        public async Task<DataTablesStructs.ReturnedData<TrabajadorCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@DATOS", parameters.SearchValue);
            //parametros.Add("@ID", id);
            var datos = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ListarTrabajador, parametros);
            return datos.ConvertirTabla(parameters);
        }

        public async Task<List<TrabajadorCustom>> ListadoPersonalTareo(string csap, string datos, int idcntrtsta, int bambas)
        {
            var param = new DynamicParameters();
            param.Add("@CSAP", csap);
            param.Add("@DATOS", datos);
            param.Add("@IDCNTRTSTA", idcntrtsta);
            param.Add("@BAMBAS", Convert.ToBoolean(bambas));
            return await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ListarPersonalTrabajador, param);
        }

        public async Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@DATOS", param.SearchTerm);
            var listado = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ListarTrabajador,parametros);

            if (!string.IsNullOrEmpty(param.SearchTerm))
            {
                listado = listado.Where(x => x.CSAP != null && x.CSAP.Contains(param.SearchTerm.ToUpper().Trim()) ||
                                       x.NMBRS.Contains(param.SearchTerm.ToUpper().Trim()))
                           .ToList();
            }
            return listado.ConvertirSelect2(param,
               (x => new Select2Structs.Result
               {
                   Id = x.ID,
                   Text = (string.IsNullOrEmpty(x.CSAP) ? "" : x.CSAP + " - ") + x.NMBRS
               }));
        }
        public async Task<List<TrabajadorCustom>> ObtenerContratista()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorCustom>(ProcedimientosAlmacenados.Trabajador.ListaContratista, param);
            return ret.ToList();
        }
    }
}
