﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasTelefono
    {
        Task<List<TelefonoModel>> ListarTelefono(int idprsna, string id);
        Task<TelefonoModel> ObtenerTelefono(string idprsna, string id);
        Task<DataTablesStructs.ReturnedData<TelefonoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id);
    }
    public class ConsultasTelefono : IConsultasTelefono
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasTelefono(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<TelefonoModel>> ListarTelefono(int idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<TelefonoModel>(ProcedimientosAlmacenados.Telefono.ListarTelefono, parametros);
        }

        public async Task<TelefonoModel> ObtenerTelefono(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            parametros.Add("@IDPRSNA", idprsna);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<TelefonoModel>(ProcedimientosAlmacenados.Telefono.ObtenerTelefono, parametros);
            return ret.Entidad;
        }
        public async Task<DataTablesStructs.ReturnedData<TelefonoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            var datos = await _configuracionConexionSql.EjecutarProcedimiento<TelefonoModel>(ProcedimientosAlmacenados.Telefono.ListarTelefono, parametros);
            return datos.ConvertirTabla(parameters);
        }
    }
}
