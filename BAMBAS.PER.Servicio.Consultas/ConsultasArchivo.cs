﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using BAMBAS.PER.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasArchivo
    {
        Task<List<ArchivoModel>> ListarArchivo(string idprsna, string id);
        Task<ArchivoModel> ObtenerArchivo(string idprsna, string id);
        Task<DataTablesStructs.ReturnedData<ArchivoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna);
    }
    public class ConsultasArchivo : IConsultasArchivo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasArchivo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<ArchivoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            var datos = await _configuracionConexionSql.EjecutarProcedimiento<ArchivoModel>(ProcedimientosAlmacenados.Archivo.ListarArchivo, parametros);
            return datos.ConvertirTabla(parameters);
        }
        public async Task<List<ArchivoModel>> ListarArchivo(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<ArchivoModel>(ProcedimientosAlmacenados.Archivo.ListarArchivo, parametros);
        }
        public async Task<ArchivoModel> ObtenerArchivo(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ArchivoModel>(ProcedimientosAlmacenados.Archivo.ObtenerArchivo, parametros);
            return ret.Entidad;
        }

    }
}
