﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Consultas
{
    public interface IConsultasCorreo
    {
        Task<List<CorreoModel>> ListarCorreo(string idprsna, string id);
        Task<CorreoModel> ObtenerCorreo(string idprsna, string id);
        Task<DataTablesStructs.ReturnedData<CorreoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id);
    }
    public class ConsultasCorreo: IConsultasCorreo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasCorreo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<CorreoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            var datos =  await _configuracionConexionSql.EjecutarProcedimiento<CorreoModel>(ProcedimientosAlmacenados.Correo.ListarCorreo, parametros);
            return datos.ConvertirTabla(parameters);
        }
        public async Task<List<CorreoModel>> ListarCorreo(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDPRSNA", idprsna);
            parametros.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<CorreoModel>(ProcedimientosAlmacenados.Correo.ListarCorreo, parametros);
        }

        public async Task<CorreoModel> ObtenerCorreo(string idprsna, string id)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", id);
            parametros.Add("@IDPRSNA", idprsna);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<CorreoModel>(ProcedimientosAlmacenados.Correo.ObtenerCorreo, parametros);
            return ret.Entidad;
        }
    }
}
