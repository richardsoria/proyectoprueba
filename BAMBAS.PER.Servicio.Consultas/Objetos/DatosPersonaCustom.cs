﻿using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.Consultas.Objetos
{
    public class DatosPersonaCustom : EntidadAuditoria
    {
        public string GDTPRSNA { get; set; }
        public string RSCL { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public string NCNLDD { get; set; }
        public string CPNCMNTO { get; set; }
        public string GDECVL { get; set; }
        public string GDSXO { get; set; }
        public string FTO { get; set; }
        public bool FCNTRTSTA { get; set; }
        public string FNCMNTO { get; set; }
        public string FFLLCMNTO { get; set; }
        public string CUBGEO { get; set; }
        public string GDTESCDD { get; set; }
        public string GDTETMNO { get; set; }
        public string MPEPE01 { get; set; }
        public string MPEPE02 { get; set; }
        public string MPEPE03 { get; set; }
        public string MPEPE04 { get; set; }
        public int? IDTRBJDR { get; set; }
    }
}
