﻿using BAMBAS.PER.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.Consultas.Objetos
{
    public class TrabajadorCustom : TrabajadorModel
    {
        public string GDTPRSNA { get; set; }
        public string NMBRS { get; set; }
        public string DATOS { get; set; }
        public string CNTRTSTA { get; set; }
        public string CRGO { get; set; }
        public string[] GDPRFLSTRBJDR => GDPRFLTRBJDR?.Split(",");
        public string FTO { get; set; }
        public int BAMBAS { get; set; }
    }
}
