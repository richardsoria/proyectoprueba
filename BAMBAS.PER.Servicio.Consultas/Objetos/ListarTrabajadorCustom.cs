﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.Consultas.Objetos
{
    public class ListarTrabajadorCustom
    {
        public string CSAP { get; set; }
        public string NMBRS { get; set; }
        public int? IDCNTRTSTA { get; set; }
        public bool? BAMBAS { get; set; }

    }
}
