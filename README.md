## Introduction

Proyecto Base para programar en arquitectura limpia.

# Reference links

- https://github.com/Anexsoft/asp.net-mvc-5-arquitectura-base/blob/master/README.md

## El proyecto contiene

- ASP.NET Identity
- SoftDeleted
- Auditoria de campos
- Repository pattern
- Depency injection
- Automatic parameters seteables

##

## Configurar git para inicio de trabajo
- (*)	: Master principal hace referencia al repositorio de CEPHEID.
- (**) 	: Copia del Master principal hace referencia al repositorio de un usuario.

**Nota:**
> - Los signos "[]" hacen referencia a los argumentos que necesitan los comandos, estos se omiten.

1. Realizar FORK al proyecto a trabajar desde el repositorio CEPHEID (MP) a tu cuenta git.
2. Desde tu maquina local clona la copia del proyecto desde tu repositorio FORK (CMP).
	- Usa el siguiente comando para clonar: `git clone [url-CMP]`
	- Renombra el remoto generado por el comando anterior, de origin a fork.
	- Usa el siguiente comando para renombrar: `git remote rename [nombre] [nuevo nombre]`
3. Agrega el remoto de repositorio CEPHEID (MP).
	- Usa el siguiente comando para agregar un remoto: `git remote add [nombre] [url-MP]`
4. Verifica los remotos
	- Usa el siguiente comando para listar los remotos agregados: `git remote -v`
5. Crea una nueva rama para comenzar el trabajo.
	- Usa el siguiente comando para crear la nueva rama: `git checkout -b [nombre rama]`

## **Flujo de trabajo con Git**
**Notas:** 	
> 	- Los signos "[]" hacen referencia a los argumentos que necesitan los comandos, estos se omiten.
> 	- Siempre crea una nueva rama para realizar cambios en tu master local.
> 	- Terminado los cambios realiza el flujo para fusionar tus cambios al MP(*), siempre desde la rama en la que trabajas (nunca desde la master).
> 	- Recuerda subir cambios que no contengan bugs.

**Para agregar cambios o mantenimientos**

1. Crea una nueva rama para comenzar el trabajo.
	- Usa el siguiente comando para crear la nueva rama: `git checkout -b [nombre rama]`
2. Una vez terminado los cambios en la nueva rama valida el estado en el que te encuetras.
	- Usa el siguiente comando: `git status`
3. Si existen cambios se mostraran lineas rojas en la consola.
	- Usa el siguiente comando para agregar los cambios: `git add -A`
	- Vuelve a usar el comando para verificar tu estado (1.1).
3. Los cambios agregados se mostraran de color verde, esto indica que estan listos para realizar un commit.
	- Usa el siguiente comando: `git commit -m "[mensaje para el commit]"`
4. Realiza el push a tu repositorio fork.
	- Usa el siguiente comando: `git push fork`
5. El comando anterior generará un link(URL) de la solicitud de merge(fusión).
	- Copia el link e ingresa a la solicitud merge.
	- Verifica el mensaje de commit.
	- La descripcion es opcional.
	- la asignacion de merge es opcional.
	- Verifica que este seleccionado solo la opcion o check "elminiar branch luego del merge".
	- Clic en el boton de "generar merge".
6. Se mostrara una nueva pagina, en ella acepta tu solicitud para que finalmente tus cambios se fusionen con el MP(*).
7. Ya actualizado el MP(*), vuelve tu repositorio local y cambia a la rama master.
	- Usa el siguiente comando: `git checkout master`
8. Elimina tu rama de trabajo desde la rama master
	- Usa el siguiente comando: `git branch -D [nombre_rama]`
9. Actualiza tu repositorio local desde el repositorio MP(*).
	- Usa el siguiente comando: `git pull origin master`
	- Esto traera los cambios de cada usuario que realizo un merge al MP(*).
10. Actualiza tu repositorio fork(CMP**) desde tu rama master local.
	- Usa el siguiente comando: `git push fork`
	- Esto sincronizara todos los repositorios.
11. Vuelve a iniciar desde el paso [(1).](1.)


**Notas Extras:**	
> - Siempre realiza los "push" a repositorio "fork"
> - Siempre realiza los "pull" desde el repositorio "origin"