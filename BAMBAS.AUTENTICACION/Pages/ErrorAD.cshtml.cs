﻿using BAMBAS.Autenticacion.Modelos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    public class ErrorADModel : PageModel
    {
        private readonly string _autenticacionADUrl;

      
        private readonly ILogger<ErrorADModel> _logger;
        private readonly string _identityUrl;

        [BindProperty(SupportsGet = true)]
        public string ReturnBaseUrl { get; set; }
        [BindProperty(SupportsGet = true)]
        public string ad { get; set; } = "";
        [BindProperty(SupportsGet = true)]
        public int cd { get; set; }
        public string Message { get; set; }
        public ErrorADModel(
            ILogger<ErrorADModel> logger,
            IConfiguration configuration)
        {
            _autenticacionADUrl = configuration.GetValue<string>("AutenticacionADUrl");
            _logger = logger;
            _identityUrl = configuration.GetValue<string>("IdentityUrl");
        }

        public async Task<IActionResult> OnGet()
        {
            if (!string.IsNullOrEmpty(ad))
            {
                switch (cd)
                {
                    case 1:
                        Message = "SU USUARIO NO HA SIDO CREADO EN EL SISTEMA DE GESTION DE PERSONAL";
                        break;
                    case 2:
                        Message = ad;
                        break;
                    default:
                        Message = "ACCESO DENEGADO";
                        break;
                }
                return Page();
            }

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Page();
        }
        public async Task<IActionResult> OnPostCerrarSesion()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect($"{_autenticacionADUrl}Home/Logout");
        }
    }
}
