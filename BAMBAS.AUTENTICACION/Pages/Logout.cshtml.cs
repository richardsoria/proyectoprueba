﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    public class LogoutModel : PageModel
    {
        private readonly string _autenticacionADUrl;

        public LogoutModel(IConfiguration configuration)
        {
            _autenticacionADUrl = configuration.GetValue<string>("AutenticacionADUrl");
        }

        public async Task<IActionResult> OnGet()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect($"{_autenticacionADUrl}Home/Logout");
        }
    }
}
