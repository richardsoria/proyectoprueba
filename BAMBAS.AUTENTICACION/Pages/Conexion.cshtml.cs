﻿using BAMBAS.CORE.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    public class ConexionModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string at { get; set; }

        public ConexionModel()
        {
        }

        public async Task<IActionResult> OnGet()
        {
            if (!string.IsNullOrEmpty(at))
            {
                await HttpContext.ConectarConAccessToken(at);
                return RedirectToPage($"Indice");
            }
            return RedirectToPage($"Index");
        }
    }
}
