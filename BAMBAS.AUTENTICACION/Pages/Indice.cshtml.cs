﻿using BAMBAS.Negocios.General.Seguridad;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class IndiceModel : PageModel
    {
        private readonly ILogger<IndiceModel> _logger;
        private readonly IConfiguration _configuration;
        private readonly IPerfilObjetoProxy _perfilObjetoProxy;


        [BindProperty(SupportsGet = true)]
        public string AccessToken { get; set; }


        //PERMISOS DEL PERFIL DEL USUARIO
        public bool AccesoSeguridad { get; set; }
        public bool AccesoPersona { get; set; }
        public bool AccesoTareo { get; set; }
        public bool AccesoEpp { get; set; }
        public IndiceModel(
            ILogger<IndiceModel> logger,
            IConfiguration configuration,
            IPerfilObjetoProxy perfilObjetoProxy
            )
        {
            _logger = logger;
            _configuration = configuration;
            _perfilObjetoProxy = perfilObjetoProxy;
        }

        public async Task<IActionResult> OnGet()
        {
            var tk = GetTempData();
            if (string.IsNullOrEmpty(tk))
            {
                return RedirectToPage($"Index");
            }

            var perfiles = User?.Claims?.FirstOrDefault(v => v.Type == "PRFLS")?.Value ?? "";

            var modulos = await _perfilObjetoProxy.ListarModulos(perfiles);

            AccesoSeguridad = modulos?.FirstOrDefault(x => x.ID.ToString() == CONFIG.ConfiguracionProyecto.MODULOS.Seguridad)?.CHECKEADO ?? false;
            AccesoPersona = modulos?.FirstOrDefault(x => x.ID.ToString() == CONFIG.ConfiguracionProyecto.MODULOS.Persona)?.CHECKEADO ?? false;
            AccesoTareo = modulos?.FirstOrDefault(x => x.ID.ToString() == CONFIG.ConfiguracionProyecto.MODULOS.Tareo)?.CHECKEADO ?? false;
            AccesoEpp = modulos?.FirstOrDefault(x => x.ID.ToString() == CONFIG.ConfiguracionProyecto.MODULOS.Epp)?.CHECKEADO ?? false;

            return Page();
        }
        public IActionResult OnPostSeguridad()
        {
            var SeguridadUrl = ObtenerUrl("SeguridadUrl");
            return Redirect(SeguridadUrl);
        }
        public IActionResult OnPostPersona()
        {
            var PersonaUrl = ObtenerUrl("PersonaUrl");
            return Redirect(PersonaUrl);
        }
        public IActionResult OnPostTareo()
        {
            var TareoUrl = ObtenerUrl("TareoUrl");
            return Redirect(TareoUrl);
        }
        public IActionResult OnPostEpp()
        {
            var EppUrl = ObtenerUrl("EppUrl");
            return Redirect(EppUrl);
        }
        private string ObtenerUrl(string valorAppSettings)
        {
            var tk = GetTempData();
            var rutaLoginController = $"login/connect?access_token={tk}";
            return $"{_configuration.GetValue<string>(valorAppSettings)}{rutaLoginController}";
        }
        private string GetTempData()
        {
            var tk = "";
            tk = User?.Claims?.FirstOrDefault(v => v.Type == "access_token")?.Value ?? "";
            return tk?.ToString();
        }
    }
}
