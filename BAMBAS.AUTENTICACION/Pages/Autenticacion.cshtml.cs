﻿using BAMBAS.Autenticacion.Modelos;
using BAMBAS.CORE.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class AutenticacionModel : PageModel
    {
        private readonly ILogger<AutenticacionModel> _logger;
        private readonly string _identityUrl;
        private readonly IMemoryCache _cache;

        [BindProperty(SupportsGet = true)]
        public string ReturnBaseUrl { get; set; }
        [BindProperty(SupportsGet = true)]
        public string ad { get; set; } = "";
        [BindProperty(SupportsGet = true)]
        public int cd { get; set; }
        [BindProperty]
        public LoginViewModel model { get; set; }

        public bool HasInvalidAccess { get; set; }
        public string Message { get; set; }

        public AutenticacionModel(
            ILogger<AutenticacionModel> logger,
             IMemoryCache cache,
            IConfiguration configuration)
        {
            _cache = cache;
            _logger = logger;
            _identityUrl = configuration.GetValue<string>("IdentityUrl");
        }

        public async Task<IActionResult> OnGet()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(
                    JsonSerializer.Serialize(model),    
                    Encoding.UTF8,
                    "application/json"
                );
                var request = await client.PostAsync(_identityUrl + "v1/identity/authentication", content);

                try
                {
                    if (!request.IsSuccessStatusCode)
                    {
                        _logger.LogError($"No se pudo loguear el usuario: {model.Email}");
                        var msj = await request.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(msj))
                        {
                            var errorresult = JsonSerializer.Deserialize<RespuestaError>(
                                msj,
                                new JsonSerializerOptions
                                {
                                    PropertyNameCaseInsensitive = true
                                }
                            );
                            if (errorresult.CodEstado == -4)
                            {
                                HasInvalidAccess = true;
                                Message = errorresult.Nombre ?? "ACCESO DENEGADO";
                                return Page();
                            }
                            return RedirectToPage($"CambioContrasena", new { UserName = errorresult.Nombre, ReturnBaseUrl = ReturnBaseUrl, ShowMessage = true });
                        }
                        else
                        {
                            HasInvalidAccess = true;
                            Message = $"Error en el servidor: No se pudo conectar el servicio.";
                            return Page();
                        }
                    }

                    var result = JsonSerializer.Deserialize<IdentityAccess>(
                        await request.Content.ReadAsStringAsync(),
                        new JsonSerializerOptions
                        {
                            PropertyNameCaseInsensitive = true
                        }
                    );
                    await HttpContext.ConectarConAccessToken(result.AccessToken);
                    if (string.IsNullOrEmpty(ReturnBaseUrl))
                    {
                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromDays(1));

                        // Save data in cache.
                        _cache.Set("tk_user", result.AccessToken, cacheEntryOptions);
                        //TempData["tk"] = result.AccessToken;
                        return RedirectToPage($"Indice");
                    }
                    return Redirect(ReturnBaseUrl + $"login/connect?access_token={result.AccessToken}");
                }
                catch (Exception ex)
                {
                    HasInvalidAccess = true;
                    Message = $"Error en el servidor: {ex.Message}";
                    return Page();
                }
            }
        }
    }
}
