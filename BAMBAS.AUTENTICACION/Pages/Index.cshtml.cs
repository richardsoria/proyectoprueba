﻿using BAMBAS.Autenticacion.Modelos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly string _autenticacionADUrl;
        private readonly IMemoryCache _cache;

        [BindProperty(SupportsGet = true)]
        public string ReturnBaseUrl { get; set; }


        public IndexModel(
            ILogger<IndexModel> logger,
             IMemoryCache cache,
            IConfiguration configuration)
        {
            _cache = cache;
            _logger = logger;
            _autenticacionADUrl = configuration.GetValue<string>("AutenticacionADUrl");
        }

        public async Task<IActionResult> OnGet()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Page();
        }

        public IActionResult OnPostActiveDirectory()
        {
            return Redirect($"{_autenticacionADUrl}");
        }
        public IActionResult OnPostLoginNormal()
        {
            return Redirect("login/Autenticacion");
        }
    }
}
