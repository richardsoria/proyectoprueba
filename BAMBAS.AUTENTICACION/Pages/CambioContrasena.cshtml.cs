﻿using BAMBAS.Autenticacion.Modelos;
using BAMBAS.CORE.Structs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class CambioContrasenaModel : PageModel
    {
        private readonly ILogger<CambioContrasenaModel> _logger;
        private readonly string _identityUrl;

        [BindProperty(SupportsGet = true)]
        public string ReturnBaseUrl { get; set; }
        [BindProperty(SupportsGet = true)]
        public string UserName { get; set; }
        [BindProperty(SupportsGet = true)]
        public bool ShowMessage { get; set; } = false;
        [BindProperty]
        public ChangePassViewModel model { get; set; }

        public bool HasInvalidAccess { get; set; }
        public string Message { get; set; }

        public CambioContrasenaModel(
            ILogger<CambioContrasenaModel> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            _identityUrl = configuration.GetValue<string>("IdentityUrl");
        }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            if (model.NewPassword!= model.ConfirmPassword)
            {
                HasInvalidAccess = true;
                Message = "LAS CONTRASEÑAS NO COINCIDEN";
                return Page();
            }
            if (model.NewPassword.Length < 8)
            {
                HasInvalidAccess = true;
                Message = "LA CONTRASEÑA DEBE TENER MÁS DE 8 DÍGITOS.";
                return Page();
            }
            if (model.NewPassword == model.OldPassword)
            {
                HasInvalidAccess = true;
                Message = "LA NUEVA CONTRASEÑA DEBE SER DIFERENTE A LA ANTERIOR.";
                return Page();
            }
            using (var client = new HttpClient())
            {
                model.Email = UserName;
                model.NewPassword = GenerarEncriptada(model.NewPassword);
                var content = new StringContent(
                    JsonSerializer.Serialize(model),
                    Encoding.UTF8,
                    "application/json"
                );
                var request = await client.PostAsync(_identityUrl + "v1/identity/cambio-contrasena", content);
                
                if (!request.IsSuccessStatusCode)
                {
                    _logger.LogError($"No se pudo cambiar la contraseña de: {model.Email}");
                    HasInvalidAccess = true;
                    var req = JsonSerializer.Deserialize<RespuestaConsulta>(
                          await request.Content.ReadAsStringAsync(),
                          new JsonSerializerOptions
                          {
                              PropertyNameCaseInsensitive = true
                          }
                      );
                    Message = req.Nombre;
                    return Page();
                }
                ///VERIFICAR QUE ENVIE AL LOGIN
                if (string.IsNullOrEmpty(ReturnBaseUrl))
                {
                    return RedirectToPage($"Index");
                }
                return Redirect(ReturnBaseUrl + $"login/logout");
            }
        }
        private string GenerarEncriptada(string password)
        {
            if (password != string.Empty)
            {
                //Create the salt value with a cryptographic PRNG:
                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
                //Create the Rfc2898DeriveBytes and get the hash value:
                var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
                byte[] hash = pbkdf2.GetBytes(20);
                //Combine the salt and password bytes for later use:
                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);
                //Turn the combined salt+hash into a string for storage
                return Convert.ToBase64String(hashBytes);
            }
            return password;
        }
    }
}
