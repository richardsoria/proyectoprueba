﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.Autenticacion.Modelos
{
    public class IdentityAccess
    {
        public bool Succeeded { get; set; }
        public string AccessToken { get; set; }
        public string ErrorMessage { get; set; }
        public bool CambiarContrasena { get; set; }
    }
    public class RespuestaError
    {
        public int CodEstado { get; set; }
        public string Nombre { get; set; }
    }
}
