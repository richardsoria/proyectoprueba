﻿$("#form_submit").on("click", function () {
    if ($("#email").val() != "" && $("#password").val() != "") {
        var $btn = $("#form_submit");
        $btn.attr("disabled", true);
        $btn.text('Cargando...');
        $btn.css('opacity', '0.8');
        $("#login_form").submit();
    }
});
$("#form_submit_cambio").on("click", function () {
    if ($("#oldpassword").val() != "" && $("#newpassword").val() != "" && $("#confirmpassword").val() != "") {
        var $btn = $("#form_submit_cambio");
        $btn.attr("disabled", true);
        $btn.text('Cargando...');
        $btn.css('opacity', '0.8');
        $("#login_form_cambio").submit();
    }
});
$("#btn-regresar").on("click", function () {
    var url = $(this).data("url");
    if (url) {
        //window.location.href = url;
        window.location.href = "/"
    } else {
       
    }
});
$("#email,#oldpassword").focus();

var $showModal = $("#showModal");
var $modal = $("#modal_cambiar");
if ($showModal.val() == "True") {
    $modal.modal({
        backdrop: 'static',
        keyboard: false
    })

    $modal.modal("show");
}


$("#email").bind('keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    var regex = /^[0-9a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
});

$("#passLog").on("click", function () {
    const passwordInput = document.querySelector('#password');
    if ($("#passLog").hasClass("ti-eye")) {
        $("#passLog").removeClass("ti-eye");
        $("#passLog").addClass("ti-lock");
        passwordInput.type = "text";
    } else {
        $("#passLog").removeClass("ti-lock");
        $("#passLog").addClass("ti-eye");
        passwordInput.type = "password";
    }
    
});

$("#passOld").on("click", function () {
    const passwordInput = document.querySelector('#oldpassword');
    if ($("#passOld").hasClass("ti-eye")) {
        $("#passOld").removeClass("ti-eye");
        $("#passOld").addClass("ti-lock");
        passwordInput.type = "text";
    } else {
        $("#passOld").removeClass("ti-lock");
        $("#passOld").addClass("ti-eye");
        passwordInput.type = "password";
    }
});

$("#passNew").on("click", function () {
    const passwordInput = document.querySelector('#newpassword');
    if ($("#passNew").hasClass("ti-eye")) {
        $("#passNew").removeClass("ti-eye");
        $("#passNew").addClass("ti-lock");
        passwordInput.type = "text";
    } else {
        $("#passNew").removeClass("ti-lock");
        $("#passNew").addClass("ti-eye");
        passwordInput.type = "password";
    }

});

$("#passCon").on("click", function () {
    const passwordInput = document.querySelector('#confirmpassword');
    if ($("#passCon").hasClass("ti-eye")) {
        $("#passCon").removeClass("ti-eye");
        $("#passCon").addClass("ti-lock");
        passwordInput.type = "text";
    } else {
        $("#passCon").removeClass("ti-lock");
        $("#passCon").addClass("ti-eye");
        passwordInput.type = "password";
    }

});
