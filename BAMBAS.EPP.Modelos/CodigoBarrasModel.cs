﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.EPP.Modelos
{
    /// <summary>
    /// SGA EMP 05
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP10")]
    public class CodigoBarrasModel : EntidadAuditoria
    {
        public string FAMILIAS { get; set; }
        public string DSCRPCN { get; set; }
        public string ANIO { get; set; }
        public string GDTPRYCCN { get; set; }
        public string GDTMNDA { get; set; }
        public decimal IMPRTE { get; set; }
        public int PRSNLPRYCTDO { get; set; }
        public string APRBDOX { get; set; }
        public string MSINCO { get; set; }
        public string MSFN { get; set; }
    }
}
