﻿CREATE SCHEMA [PER]
    AUTHORIZATION [dbo];









GO
GRANT UPDATE
    ON SCHEMA::[PER] TO [UPER00];


GO
GRANT SELECT
    ON SCHEMA::[PER] TO [UPER00];


GO
GRANT INSERT
    ON SCHEMA::[PER] TO [UPER00];


GO
GRANT EXECUTE
    ON SCHEMA::[PER] TO [UPER00];

