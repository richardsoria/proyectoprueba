﻿CREATE LOGIN [UPER00] WITH PASSWORD=N'UPER00$', DEFAULT_DATABASE=[BAMBAS_CORE], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
GO;
CREATE USER [UPER00] FOR LOGIN [UPER00]
    WITH DEFAULT_SCHEMA = [PER];




GO
GRANT IMPERSONATE
    ON USER::[UPER00] TO [UIDRER]
    AS [UPER00];

