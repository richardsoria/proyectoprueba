﻿CREATE PROCEDURE [dbo].[USP_KILLDORMIDAS]
AS
DECLARE @SQL VARCHAR(max)
SET @SQL = ''

SELECT  --'Kill ' + CONVERT(VARCHAR(10),  ) as spid 
		@SQL = @SQL + ' KILL ' + CAST(sdes.session_id AS VARCHAR(5)) + ' ' 
FROM sys.dm_exec_sessions AS sdes
INNER JOIN sys.dm_exec_connections AS sdec
        ON sdec.session_id = sdes.session_id

CROSS APPLY (

    SELECT DB_NAME(dbid) AS DatabaseName
        ,OBJECT_NAME(objectid) AS ObjName
        ,COALESCE((
            SELECT TEXT AS [processing-instruction(definition)]
            FROM sys.dm_exec_sql_text(sdec.most_recent_sql_handle)
            FOR XML PATH('')
                ,TYPE
            ), '') AS Query

    FROM sys.dm_exec_sql_text(sdec.most_recent_sql_handle)

) sdest
WHERE sdes.session_id <> @@SPID
  --AND sdest.DatabaseName ='FOSMAR_CORE_QC'
  and sdes.login_name Like 'U%'
  and status = 'sleeping'

--SELECT @SQL
EXEC(@SQL)