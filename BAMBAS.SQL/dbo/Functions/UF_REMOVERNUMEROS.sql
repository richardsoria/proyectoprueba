﻿CREATE FUNCTION dbo.UF_REMOVERNUMEROS(@in VARCHAR(255))
RETURNS VARCHAR(255)
AS
BEGIN
DECLARE @out VARCHAR(255)

    IF (@in IS NOT NULL)
    BEGIN
        SET @out = ''

        WHILE (@in <> '')
        BEGIN
            IF (@in NOT LIKE '[0-9]')
                SET @out = @out + SUBSTRING(@in, 1, 1)

            SET @in = SUBSTRING(@in, 2, LEN(@in) - 1)
        END
    END

    RETURN @out
END