﻿CREATE TABLE [PER].[MPEPE05] (
    [ID]        NUMERIC (15)  IDENTITY (1, 1) NOT NULL,
    [PATHARCHV] VARCHAR (MAX) NULL,
    [NMBRARCHV] VARCHAR (150) NULL,
    [FPRNCPL]   VARCHAR (1)   NULL,
    [GDESTDO]   VARCHAR (1)   NULL,
    [FESTDO]    DATETIME      NULL,
    [UCRCN]     VARCHAR (15)  NULL,
    [FCRCN]     DATETIME      NULL,
    [UEDCN]     VARCHAR (15)  NULL,
    [FEDCN]     DATETIME      NULL,
    [IDPRSNA]   NUMERIC (15)  NULL,
    [TPOARCHV]  VARCHAR (50)  NULL,
    CONSTRAINT [PK_MPEPE05] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MPEPE05_00] FOREIGN KEY ([IDPRSNA]) REFERENCES [PER].[MPEPE00] ([ID])
);




GO



GO


