﻿CREATE PROCEDURE [PER].[USP_IMPEPE01_InsertarDireccion]
	@ID					  numeric OUTPUT, 
	@GDTDRCCN             varchar(1),
	@GDTVIA               varchar(2),
	@VIA                  varchar(40),
	@NVIA                 varchar(5),
	@NINTRR               varchar(5),
	@GDTDZNA              varchar(2) null,
	@ZNA                  varchar(40) null,
	@RFRNCIA              varchar(160),
	@FPRNCPL			  varchar(1),
	@CUBGEO				  varchar(6),
	@GDESTDO              varchar(1),
	@UCRCN                varchar(15),
	@UEDCN                varchar(15),
	@IDPRSNA              numeric(15,0)
AS	
BEGIN TRY  
	BEGIN TRANSACTION 
		INSERT INTO PER.MPEPE01(
		GDTDRCCN,						
		GDTVIA, 						
		VIA,    						
		NVIA,   						
		NINTRR, 						
		GDTDZNA,						
		ZNA,    						
		RFRNCIA,						
		FPRNCPL,						
		CUBGEO,							
		GDESTDO,						
		FESTDO, 						
		UCRCN,  						
		FCRCN,  						
		UEDCN,  						
		FEDCN,  						
		IDPRSNA)						
		VALUES(	
		@GDTDRCCN,   
		@GDTVIA,    
		@VIA,        
		@NVIA,       
		@NINTRR,     
		@GDTDZNA,    
		@ZNA,        
		@RFRNCIA,    
		@FPRNCPL,	
		@CUBGEO,	
		@GDESTDO,    
		[dbo].[UF_OBT_FECHA](),   
		@UCRCN,      
		[dbo].[UF_OBT_FECHA](),    
		@UEDCN,      
		[dbo].[UF_OBT_FECHA](),  
		@IDPRSNA)

	SET @ID = SCOPE_IDENTITY() 

	IF @FPRNCPL = '1'
	BEGIN
		UPDATE PER.MPEPE01
			SET FPRNCPL = '2'
		WHERE IDPRSNA = @IDPRSNA
		AND ID != @ID;
	END;

	COMMIT TRANSACTION
	
END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'PER', @UCRCN, 'ERROR AL INTENTAR REGISTRAR LA DIRECCIÓN DE LA PERSONA', @ID OUTPUT;
END CATCH;  