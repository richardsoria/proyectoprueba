﻿CREATE PROCEDURE [PER].[USP_UMPEPE01_ActualizarDireccion]	
	@ID			numeric(15), 
	@GDTDRCCN      varchar(1),
	@GDTVIA        varchar(2),
	@VIA           varchar(40),
	@NVIA          varchar(5),
	@NINTRR        varchar(5),
	@GDTDZNA       varchar(2),
	@ZNA           varchar(40),
	@RFRNCIA       varchar(160),
	@FPRNCPL		varchar(1),
	@CUBGEO		varchar(6),
	@GDESTDO       varchar(1),
	@UEDCN         varchar(15),
	@IDPRSNA       numeric(15,0),
	@RETORNO		numeric(1) OUTPUT 
AS	
BEGIN TRY 
	BEGIN TRANSACTION
	SET @RETORNO = 0;
		UPDATE PER.MPEPE01
			SET GDTDRCCN	= @GDTDRCCN,
				GDTVIA 		= @GDTVIA,  
				VIA    		= @VIA,     
				NVIA   		= @NVIA,    
				NINTRR 		= @NINTRR,  
				GDTDZNA		= @GDTDZNA, 
				ZNA    		= @ZNA,     
				RFRNCIA		= @RFRNCIA, 
				FPRNCPL		= @FPRNCPL,
				CUBGEO		= @CUBGEO,	
				GDESTDO		= @GDESTDO, 
				FESTDO    	= CASE WHEN @GDESTDO != GDESTDO THEN [dbo].[UF_OBT_FECHA]() ELSE FESTDO END,
				UEDCN  		= @UEDCN,   
				FEDCN  		= [dbo].[UF_OBT_FECHA](),
				IDPRSNA		= @IDPRSNA
		  WHERE ID = @ID;

	IF @FPRNCPL = '1'
	BEGIN
		UPDATE PER.MPEPE01
			SET FPRNCPL = '2'
		WHERE IDPRSNA = @IDPRSNA
		AND ID != @ID;
	END;

	COMMIT TRANSACTION
	
	SET @RETORNO= 1;

END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'PER', @UEDCN, 'ERROR AL INTENTAR ACTUALIZAR LA DIRECCIÓN REGISTRADA DE LA PERSONA', @ID OUTPUT;
END CATCH;  

