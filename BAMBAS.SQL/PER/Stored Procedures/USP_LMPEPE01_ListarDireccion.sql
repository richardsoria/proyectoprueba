﻿
CREATE PROCEDURE [PER].[USP_LMPEPE01_ListarDireccion]
	@IDPRSNA			numeric(15,0),	
	@ID				numeric(15,0) 	
AS	
BEGIN TRY  
	BEGIN TRANSACTION 
		SELECT ID,
				GDTDRCCN AS IGDTDRCCN,
			   [dbo].[UF_OBT_GDTO_DT]('GDTDRCCN',1,GDTDRCCN,1) GDTDRCCN ,						
			   [dbo].[UF_OBT_GDTO_DT]('GDTVIA',1,GDTVIA,1) GDTVIA, 						
			   VIA,    						
			   ISNULL(NVIA,'')NVIA,   						
			   ISNULL(NINTRR,'') NINTRR, 					
			   ISNULL([dbo].[UF_OBT_GDTO_DT]('GDTDZNA',1,GDTDZNA,1),'') GDTDZNA,
			   ISNULL(ZNA,'') ZNA,			  						
			   ISNULL(RFRNCIA,'') RFRNCIA,						
			   FPRNCPL,						
			   [dbo].[UF_OBT_UBIGEO] (CUBGEO,1) CUBGEO,							
			   GDESTDO,						
			   FESTDO, 						
			   UCRCN,  						
			   FCRCN,  						
			   UEDCN,  						
			   FEDCN,  						
			   IDPRSNA						
		FROM PER.MPEPE01
		WHERE IDPRSNA = ISNULL(@IDPRSNA,IDPRSNA)
		AND ID = ISNULL(@ID,ID)
		ORDER BY FPRNCPL 
	COMMIT TRANSACTION
END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'PER','LISTAR', @ID OUTPUT;
END CATCH;