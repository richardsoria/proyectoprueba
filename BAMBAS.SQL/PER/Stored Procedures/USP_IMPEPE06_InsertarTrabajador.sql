﻿CREATE PROCEDURE [PER].[USP_IMPEPE06_InsertarTrabajador]
	@ID			NUMERIC OUTPUT,
	@IDPRSNA		NUMERIC(15),
	@CSAP			VARCHAR(10),
	@GDTTRBJDR		VARCHAR(1),
	@IDCNTRTSTA     NUMERIC(15),
	@FINGRSO		DATE,
	@FBJA			DATE,
	@MBJA			VARCHAR(100),
	@GDSDE		VARCHAR(1),
	@IDGRNCA		NUMERIC(15),
	@IDSPRINTNDNCA  NUMERIC(15),
	@IDARA			NUMERIC(15),
	@IDGRPO			NUMERIC(15),
	@IDCRGO			NUMERIC(15),
	@IDPSCN			NUMERIC(15),
	@IDCNTROCSTO	NUMERIC(15),
	@IDUNDDORGNZCNL NUMERIC(15),
	@GDGRPOPS		VARCHAR(1),
	@GDNVL			VARCHAR(1),
	@GDDLGCNATRZDA	VARCHAR(1),
	@GDPRFLTRBJDR	VARCHAR(30),
	@FTRBJORMTO		BIT,
	@GDTGRDA		VARCHAR(1),
	@FTITLR			BIT,
	@GDLGRALJMNTO	VARCHAR(1),
	@GDUBCCNALJMNTO VARCHAR(1),
	@GDTALJMNTO		VARCHAR(1),
	@MDLO			VARCHAR(100),
	@PSO			VARCHAR(50),
	@CMA			VARCHAR(50),
	--
	@GDESTDO		VARCHAR(1),
	@UCRCN			VARCHAR(15)
AS	
BEGIN TRY  
	BEGIN TRANSACTION
		INSERT INTO PER.MPEPE06(
								IDPRSNA							
								,CSAP			
								,GDTTRBJDR		
								,IDCNTRTSTA     
								,FINGRSO		
								,FBJA			
								,MBJA			
								,GDSDE	
								,IDGRNCA		
								,IDSPRINTNDNCA  
								,IDARA			
								,IDGRPO			
								,IDCRGO			
								,IDPSCN			
								,IDCNTROCSTO	
								,IDUNDDORGNZCNL 
								,GDGRPOPS	
								,GDDLGCNATRZDA
								,GDNVL			
								,GDPRFLTRBJDR	
								,FTRBJORMTO		
								,GDTGRDA		
								,FTITLR			
								,GDLGRALJMNTO	
								,GDUBCCNALJMNTO 
								,GDTALJMNTO		
								,MDLO			
								,PSO			
								,CMA,		
								--
								GDESTDO,
								FESTDO,                  
								UCRCN,           
								FCRCN,                   
								UEDCN,           
								FEDCN)
		VALUES(
				@IDPRSNA		
				,@CSAP			
				,@GDTTRBJDR		
				,@IDCNTRTSTA     
				,@FINGRSO		
				,@FBJA			
				,@MBJA			
				,@GDSDE		
				,@IDGRNCA		
				,@IDSPRINTNDNCA  
				,@IDARA			
				,@IDGRPO			
				,@IDCRGO			
				,@IDPSCN			
				,@IDCNTROCSTO	
				,@IDUNDDORGNZCNL 
				,@GDGRPOPS		
				,@GDDLGCNATRZDA
				,@GDNVL			
				,@GDPRFLTRBJDR	
				,@FTRBJORMTO		
				,@GDTGRDA		
				,@FTITLR			
				,@GDLGRALJMNTO	
				,@GDUBCCNALJMNTO 
				,@GDTALJMNTO		
				,@MDLO			
				,@PSO			
				,@CMA,			
				--
				@GDESTDO,
				[dbo].[UF_OBT_FECHA](),                  
				@UCRCN,         
				[dbo].[UF_OBT_FECHA](),                   
				@UCRCN,          
				[dbo].[UF_OBT_FECHA]())
	COMMIT TRANSACTION
	SET @ID = SCOPE_IDENTITY() 
END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'PER', @UCRCN, 'ERROR AL INTENTAR REGISTRAR EL TRABAJADOR', @ID OUTPUT;	
END CATCH;


--select * from SEG.MSGEMP02 order by edtme desc