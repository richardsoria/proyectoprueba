﻿CREATE PROCEDURE [PER].[USP_OMPEPE01_ObtenerDireccion]
	@IDPRSNA			numeric(15,0),	
	@ID				numeric(15,0) 	
AS	
BEGIN TRY  
	BEGIN TRANSACTION 
		SELECT ID,
			   GDTDRCCN ,						
			   GDTVIA, 						
			   VIA,    						
			   NVIA,   						
			   NINTRR, 						
			   GDTDZNA,						
			   ZNA,    		  						
			   RFRNCIA,						
			   FPRNCPL,						
			   CUBGEO,							
			   GDESTDO,						
			   FESTDO, 						
			   UCRCN,  						
			   FCRCN,  						
			   UEDCN,  						
			   FEDCN,  						
			   IDPRSNA						
		FROM PER.MPEPE01
		WHERE IDPRSNA = ISNULL(@IDPRSNA,IDPRSNA)
		AND ID = ISNULL(@ID,ID)
	COMMIT TRANSACTION
END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'PER','OBTENER', @ID OUTPUT;
END CATCH;
GO