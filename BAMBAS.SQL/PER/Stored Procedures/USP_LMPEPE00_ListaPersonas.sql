﻿CREATE PROCEDURE [PER].[USP_LMPEPE00_ListaPersonas]
	@GDTPRSNA	NUMERIC(1),
	@GDDCMNTO	VARCHAR(1),
	@NDCMNTO	VARCHAR(20),
	@DATOS		VARCHAR(100),
	@DESDE		VARCHAR(10),
	@HASTA		VARCHAR(10),
	@SOLOCONTRATISTAS bit null,
	@ESTADO		VARCHAR(1)
AS
	
	BEGIN TRY  
			DECLARE @sql nvarchar(MAX);
			DECLARE @sqlUnion nvarchar(MAX);
			DECLARE @cFPRNCPL VARCHAR(1) = '1';

			SET @sql = 'SELECT P00.ID, 
							   [dbo].[UF_OBT_GDTO_DT](''GDDCMNTO'',1,P04.GDDCMNTO,2) GDDCMNTO, 
							   P04.NDCMNTO,
							   (CASE WHEN P00.GDTPRSNA = ''2''
									 THEN P00.RSCL 
									 ELSE CONCAT(P00.PNMBRE,'' '' , P00.SNMBRE ,'' '' , P00.APTRNO ,'' '' ,P00.AMTRNO) 
								END
								) AS DATOS,
								P00.PNMBRE,
								P00.SNMBRE,
								P00.APTRNO,
								P00.AMTRNO,
								P00.FCNTRTSTA,
								[dbo].[UF_OBT_GDTO_DT](''GDTPRSNA'',1,P00.GDTPRSNA,2) TPRSNA,
								P00.GDTPRSNA,
								CONCAT(LEFT(P00.PNMBRE,1),replace(P00.APTRNO,'' '','''')) PUSUARIO,
								P00.FCRCN, 
								CONVERT(VARCHAR, P00.FEDCN, 5) + '' '' +  CONVERT(VARCHAR, P00.FEDCN, 14) as FEDCN,
								P00.FEDCN AS FEDICION,
								P00.GDESTDO,
								ISNULL((SELECT TOP 1 * FROM PER.MPEPE01 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' FOR XML PATH(''MPEPE01'')),'''') MPEPE01,
								ISNULL((SELECT TOP 1 CONCAT(GDTTLFNO,''*'', NTLFNO) FROM PER.MPEPE02 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' ),'''') MPEPE02,
								ISNULL((SELECT TOP 1 * FROM PER.MPEPE03 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' FOR XML PATH(''MPEPE03'')),'''') MPEPE03,
								ISNULL((SELECT TOP 1 CONCAT(GDDCMNTO,''*'',NDCMNTO)  FROM PER.MPEPE04 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A''),'''') MPEPE04
						FROM PER.MPEPE00 P00
						LEFT JOIN PER.MPEPE04 P04
							ON P00.ID = P04.IDPRSNA WHERE 0 = 0'
					IF (@GDDCMNTO = '' OR @GDDCMNTO IS NULL) AND (@NDCMNTO = '' OR @NDCMNTO IS NULL)
						BEGIN
						SET @sqlUnion =
							' UNION 
							 SELECT P00.ID, 
								   NULL GDDCMNTO, 
								   NULL NDCMNTO,
								   (CASE WHEN P00.GDTPRSNA = ''2''
										 THEN P00.RSCL 
										 ELSE CONCAT(P00.PNMBRE,'' '' , P00.SNMBRE ,'' '' , P00.APTRNO ,'' '' ,P00.AMTRNO) 
									END
									) AS DATOS,
									P00.PNMBRE,
									P00.SNMBRE,
									P00.APTRNO,
									P00.AMTRNO,
									P00.FCNTRTSTA,
									[dbo].[UF_OBT_GDTO_DT](''GDTPRSNA'',1,P00.GDTPRSNA,2) TPRSNA,	
									P00.GDTPRSNA,
									CONCAT(LEFT(P00.PNMBRE,1),replace(P00.APTRNO,'' '','''')) PUSUARIO,
									P00.FCRCN, 
									CONVERT(VARCHAR, P00.FEDCN, 5) + '' '' +  CONVERT(VARCHAR, P00.FEDCN, 14) as FEDCN,
									P00.FEDCN AS FEDICION,
									P00.GDESTDO,
									ISNULL((SELECT TOP 1 * FROM PER.MPEPE01 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' FOR XML PATH(''MPEPE01'')),'''') MPEPE01,
									ISNULL((SELECT TOP 1 CONCAT(GDTTLFNO,''*'', NTLFNO) FROM PER.MPEPE02 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' ),'''') MPEPE02,
									ISNULL((SELECT TOP 1 * FROM PER.MPEPE03 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A'' FOR XML PATH(''MPEPE03'')),'''') MPEPE03,
									ISNULL((SELECT TOP 1 CONCAT(GDDCMNTO,''*'',NDCMNTO)  FROM PER.MPEPE04 WHERE IDPRSNA= P00.ID AND FPRNCPL = ''1'' AND GDESTDO =''A''),'''') MPEPE04
							FROM PER.MPEPE00 P00
							WHERE NOT EXISTS (
											SELECT 1 
											FROM PER.MPEPE04 P04
											WHERE P04.IDPRSNA = P00.ID
											AND P04.GDESTDO =''A''
										) '
						END	
				---CONDICIONES
				IF @GDTPRSNA IS NOT NULL
					BEGIN
						IF @GDTPRSNA IN (1,2)
							BEGIN
								SET @sql = @sql + CHAR(13) +  ' AND P00.GDTPRSNA = ' + convert(varchar(1), @GDTPRSNA);
								SET @sqlUnion= @sqlUnion+ CHAR(13) +  ' AND P00.GDTPRSNA = ' + convert(varchar(1), @GDTPRSNA);
							END;
						ELSE
							BEGIN
								SET @sql = @sql + CHAR(13) +  ' AND P00.GDTPRSNA = P00.GDTPRSNA'
								SET @sqlUnion = @sqlUnion + CHAR(13) +  ' AND P00.GDTPRSNA = P00.GDTPRSNA'
							END;
					END;

				IF @GDDCMNTO != '' OR @GDDCMNTO IS NOT NULL
						BEGIN
							SET @sql = @sql + CHAR(13) + CHAR(10)  + ' AND P04.GDDCMNTO = ' +  CHAR(39) + @GDDCMNTO  +  CHAR(39)
							SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + ' AND P04.GDDCMNTO = ' +  CHAR(39) + @GDDCMNTO  +  CHAR(39)
						END;

				IF @NDCMNTO != '' OR @NDCMNTO IS NOT NULL					
						BEGIN
							SET @sql = @sql + CHAR(13) + CHAR(10)  + ' AND P04.NDCMNTO = ' +  CHAR(39) + @NDCMNTO  +  CHAR(39)
							SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + ' AND P04.NDCMNTO = ' +  CHAR(39) + @NDCMNTO  +  CHAR(39)
						END;

				IF @DATOS != '' OR @DATOS IS NOT NULL
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  +
						' AND (
								TRIM(P00.APTRNO) = ' + CHAR(39) + @DATOS + CHAR(39) +
								' OR (TRIM(P00.AMTRNO) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(P00.PNMBRE) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(P00.SNMBRE) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(CONCAT(TRIM(P00.PNMBRE), '' '', TRIM(P00.SNMBRE) , '' '', TRIM(P00.APTRNO) , '' '',TRIM(P00.AMTRNO))) LIKE 
								CONCAT(''%'', ' +  CHAR(39) + @DATOS +  CHAR(39) + ', ''%''))
								OR TRIM(P00.RSCL) = ' +  CHAR(39) + @DATOS +  CHAR(39) +
							')'
							SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  +
						' AND (
								TRIM(P00.APTRNO) = ' + CHAR(39) + @DATOS + CHAR(39) +
								' OR (TRIM(P00.AMTRNO) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(P00.PNMBRE) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(P00.SNMBRE) = ' + CHAR(39) + @DATOS + CHAR(39) + ')
								OR (TRIM(CONCAT(TRIM(P00.PNMBRE), '' '', TRIM(P00.SNMBRE) , '' '', TRIM(P00.APTRNO) , '' '',TRIM(P00.AMTRNO))) LIKE 
								CONCAT(''%'', ' +  CHAR(39) + @DATOS +  CHAR(39) + ', ''%''))
								OR TRIM(P00.RSCL) = ' +  CHAR(39) + @DATOS +  CHAR(39) +
							')'
					END;

				IF (@DESDE != '' OR @DESDE IS NOT NULL) AND (@HASTA != '' OR @HASTA IS NOT NULL)
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  + 
						' AND (
								CONVERT(VARCHAR,P00.FCRCN,103) >= CONVERT(VARCHAR,  ' +  CHAR(39) + @DESDE +  CHAR(39) + ', 103) 
								AND CONVERT(VARCHAR,P00.FCRCN,103) <= CONVERT(VARCHAR, ' +  CHAR(39) + @HASTA+  CHAR(39) + ', 103)
							)'
							SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + 
						' AND (
								CONVERT(VARCHAR,P00.FCRCN,103) >= CONVERT(VARCHAR,  ' +  CHAR(39) + @DESDE +  CHAR(39) + ', 103) 
								AND CONVERT(VARCHAR,P00.FCRCN,103) <= CONVERT(VARCHAR, ' +  CHAR(39) + @HASTA+  CHAR(39) + ', 103)
							)'
					END;

				IF @ESTADO != '' OR @ESTADO IS NOT NULL
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  + ' AND P00.GDESTDO = ' +  CHAR(39) + @ESTADO + CHAR(39)
						SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + ' AND P00.GDESTDO = ' +  CHAR(39) + @ESTADO + CHAR(39)
					END;	
			
				IF(@SOLOCONTRATISTAS IS NOT NULL AND @SOLOCONTRATISTAS = 1)
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  + ' AND P00.FCNTRTSTA = ' +  CHAR(39) + CONVERT(VARCHAR, @SOLOCONTRATISTAS) + CHAR(39)
						SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + ' AND P00.FCNTRTSTA = ' +  CHAR(39) + CONVERT(VARCHAR, @SOLOCONTRATISTAS) + CHAR(39)
					END;
				IF @GDDCMNTO IS NULL AND @NDCMNTO IS NULL 
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  + ' AND P04.FPRNCPL = ' +  CHAR(39) + @cFPRNCPL  + CHAR(39) + ' AND P04.GDESTDO = ''A'''
						--SET @sqlUnion = @sqlUnion + CHAR(13) + CHAR(10)  + ' AND P04.FPRNCPL = ' +  CHAR(39) + @cFPRNCPL  + CHAR(39)
					END;
				IF (@GDDCMNTO = '' OR @GDDCMNTO IS NULL) AND (@NDCMNTO = '' OR @NDCMNTO IS NULL)
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10)  + @sqlUnion;
					END;
					ELSE
					BEGIN
						SET @sql = @sql + CHAR(13) + CHAR(10);
					END;

					SET @sql = @sql + CHAR(13) + CHAR(10) + 'ORDER BY FEDICION DESC';
				PRINT @sql;

			EXEC sp_executesql @sql

	END TRY  
	BEGIN CATCH  

		PRINT 'Error No.: ' + ltrim(str(error_number()))
		PRINT 'Description: ' + error_message()
	
	END CATCH;  
