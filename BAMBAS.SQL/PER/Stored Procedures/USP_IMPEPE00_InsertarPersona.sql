﻿CREATE PROCEDURE [PER].[USP_IMPEPE00_InsertarPersona]	
	@ID					NUMERIC(15) OUTPUT , 
	@GDTPRSNA			VARCHAR(1),
	@FCNTRTSTA			BIT,
	@RSCL				VARCHAR(150),
	@APTRNO				VARCHAR(50) ,
	@AMTRNO				VARCHAR(50) ,
	@ACSDA				VARCHAR(50) ,
	@PNMBRE				VARCHAR(30) ,
	@SNMBRE				VARCHAR(30) ,
	@NCNLDD				VARCHAR(3)  ,
    @CPNCMNTO			VARCHAR(3)  ,
	@GDECVL				VARCHAR(1)  ,
	@GDSXO				VARCHAR(1)  ,
	@FNCMNTO			DATETIME,
	@FFLLCMNTO			DATETIME NULL,
	@CUBGEO				VARCHAR(6)  ,
	@GDTESCDD			VARCHAR(3)  ,
	@GDDCMNTO			VARCHAR(1) NULL,
	@NDCMNTO			VARCHAR(20) NULL,
	@GDTETMNO			VARCHAR(3)  ,
	@FTO				VARCHAR(MAX)  ,
	@GDESTDO			VARCHAR(1)  ,
	@UCRCN				VARCHAR(15) ,
	@UEDCN				VARCHAR(15) 
AS	
	DECLARE @Existe NUMERIC(15) = 0;
	IF @GDTPRSNA IS NOT NULL AND @GDTPRSNA = '2' --PERSONA JURIDICA
	BEGIN
		SELECT @Existe = PER.UF_OBT_TIP_DCMNTO(@GDDCMNTO, @NDCMNTO, @ID)

		IF @Existe > 0 
			BEGIN
				RAISERROR('EL TIPO Y NÚMERO DE DOCUMENTO A INGRESAR YA ESTÁ REGISTRADO, VALIDAR!',15,217);
				RETURN;
			END;
	END
BEGIN TRY  
	BEGIN TRANSACTION
		INSERT INTO PER.MPEPE00(
								GDTPRSNA,
								FCNTRTSTA,
								RSCL,			
								APTRNO , 		
								AMTRNO,			
								ACSDA ,			
								PNMBRE,			
								SNMBRE,			
								NCNLDD ,     
								CPNCMNTO,
								GDECVL ,  		
								GDSXO ,			
								FNCMNTO,		
								FFLLCMNTO , 	
								CUBGEO,
								GDTESCDD,
								GDTETMNO,
								FTO,
								GDESTDO,		
								FESTDO,			
								UCRCN  , 		
								FCRCN,   		
								UEDCN,			
								FEDCN)			
		VALUES(
				@GDTPRSNA,
				@FCNTRTSTA,
				@RSCL,		
				@APTRNO,	
				@AMTRNO,	
				@ACSDA,		
				@PNMBRE,	
				@SNMBRE,	            
				@NCNLDD,	
				@CPNCMNTO,
				@GDECVL,	
				@GDSXO,		
				@FNCMNTO,
				@FFLLCMNTO,
				@CUBGEO,
				@GDTESCDD,
				@GDTETMNO,
				@FTO,
				@GDESTDO,
				[dbo].[UF_OBT_FECHA](),
				@UCRCN,	
				[dbo].[UF_OBT_FECHA](),
				@UEDCN,
				[dbo].[UF_OBT_FECHA]())		 
		SET @ID = SCOPE_IDENTITY() 

		IF  @GDTPRSNA IS NOT NULL AND @GDTPRSNA = '2' AND @Existe = 0 --crear documento si es una persona juridica
			BEGIN
				DECLARE	@IDDOC numeric(15, 0);
					INSERT INTO PER.MPEPE04(
									GDDCMNTO,
									NDCMNTO,
									FINSCRPCN,
									FVNCMNTO,
									FPRNCPL,
									GDESTDO,
									FESTDO,
									UCRCN,
									FCRCN,
									UEDCN,
									FEDCN,
									IDPRSNA)
						VALUES( @GDDCMNTO,          
								@NDCMNTO,    
								NULL,  
								NULL,   
								'1',	
								@GDESTDO,
								[dbo].[UF_OBT_FECHA](),
								@UCRCN,      
								[dbo].[UF_OBT_FECHA](),
								@UEDCN,      
								[dbo].[UF_OBT_FECHA](),
								@ID)
			END;

	COMMIT TRANSACTION
END TRY  
BEGIN CATCH  
	ROLLBACK TRANSACTION	
		EXECUTE dbo.USP_IMSGEMP02 'PER', @UCRCN, 'ERROR AL INTENTAR REGISTRAR DATOS DE LA PERSONA', @ID OUTPUT;
END CATCH;