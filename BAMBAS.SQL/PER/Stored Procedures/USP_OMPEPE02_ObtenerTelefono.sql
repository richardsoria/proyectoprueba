﻿CREATE PROCEDURE [PER].[USP_OMPEPE02_ObtenerTelefono]
	@IDPRSNA			varchar(15),--numeric(15,0),	
	@ID					varchar(15)--numeric(15,0) 	
AS	
	BEGIN
		SELECT ID,
			   GDTTLFNO,
			   CUBGEO,
			   NTLFNO,
			   OBSRVCN,
			   FPRNCPL,
			   GDESTDO,
			   FESTDO,
			   UCRCN,
			   FCRCN,
			   UEDCN,
			   FEDCN,
			   IDPRSNA
		FROM PER.MPEPE02
		WHERE IDPRSNA = ISNULL(@IDPRSNA,IDPRSNA)
		AND ID = ISNULL(@ID,ID)
END
GO