﻿
CREATE FUNCTION [SEG].[UF_MSGEMP12_ValidarObjetoPorPerfiles]
(
	@IDPRFL		  varchar(100),
	@OBJTO		  varchar(100)
)
RETURNS bit
AS
BEGIN
	DECLARE @Retorno bit
	SELECT @Retorno = (CASE WHEN  EXISTS 
							(SELECT NOBJTO FROM 
								(SELECT IDOBJTO 
									FROM SEG.MSGEMP12 EMP12
									WHERE EMP12.IDPRFL in (SELECT VALUE FROM STRING_SPLIT(@IDPRFL, ',') WHERE VALUE != '')
									AND TPO in (2,3)
									AND GDESTDO='A') 
							AS A
							inner join SEG.MSGEMP06 emp06 
							on emp06.ID = A.IDOBJTO
							WHERE emp06.NOBJTO =@OBJTO
						) 
						THEN 1
						ELSE 0
						END)
	RETURN @Retorno;
END;