﻿
CREATE FUNCTION [SEG].[UF_EXSTE_RSTR] 
(
	@DSCRPCN	VARCHAR(150),
	@ID			NUMERIC NULL
)
RETURNS NUMERIC
AS
BEGIN
	DECLARE @Cantidad NUMERIC(15)
	
	IF @ID IS NULL 
	BEGIN
		SELECT @Cantidad = COUNT(0)
		FROM SEG.MSGEMP17
		WHERE DSCRPCN = @DSCRPCN
	END
	ELSE
	BEGIN
		SELECT @Cantidad = COUNT(0)
		FROM SEG.MSGEMP17
		WHERE DSCRPCN = @DSCRPCN
		AND ID != @ID
	END

	RETURN @Cantidad;

END;