﻿CREATE TABLE [SEG].[MSGEMP12] (
    [ID]      NUMERIC (15) IDENTITY (1, 1) NOT NULL,
    [IDPRFL]  NUMERIC (15) NOT NULL,
    [IDOBJTO] NUMERIC (15) NOT NULL,
    [GDESTDO] VARCHAR (1)  NOT NULL,
    [FESTDO]  DATETIME     NOT NULL,
    [UCRCN]   VARCHAR (15) NOT NULL,
    [FCRCN]   DATETIME     NOT NULL,
    [UEDCN]   VARCHAR (15) NOT NULL,
    [FEDCN]   DATETIME     NOT NULL,
    [TPO]     INT          NULL,
    CONSTRAINT [PK_MSGEMP12] PRIMARY KEY CLUSTERED ([ID] ASC, [IDPRFL] ASC, [IDOBJTO] ASC),
    CONSTRAINT [FK_MSGEMP12_00] FOREIGN KEY ([IDPRFL]) REFERENCES [SEG].[MSGEMP09] ([ID])
);
GO

CREATE NONCLUSTERED INDEX [IDX_MSGEMP12_00]
    ON [SEG].[MSGEMP12]([ID] ASC);
GO

CREATE NONCLUSTERED INDEX [IDX_MSGEMP12_01]
    ON [SEG].[MSGEMP12]([GDESTDO] ASC);
GO