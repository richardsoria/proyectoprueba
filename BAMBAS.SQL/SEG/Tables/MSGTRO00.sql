﻿CREATE TABLE [SEG].[MSGTRO00] (
    [ID]         NUMERIC (15)  IDENTITY (1, 1) NOT NULL,
    [GDTRNO]     VARCHAR (1)   NULL,
    [IDGRDIA]    NUMERIC (15)  NULL,
    [IDRSTER]    NUMERIC (15)  NULL,
    [IDSPRVSR]   NUMERIC (15)  NULL,
    [IDGRPO]     NUMERIC (15)  NULL,
    [IDPRSNA]    NUMERIC (15)  NULL,
    [FCHTRO]     DATETIME      NULL,
    [FINCTRO]    DATETIME      NULL,
    [INDTRO]     BIT           NULL,
    [OBSRVCNS]   VARCHAR (150) NULL,
    [FCRCN]      DATETIME      NULL,
    [UCRCN]      VARCHAR (20)  NULL,
    [FEDCN]      DATETIME      NULL,
    [UEDCN]      VARCHAR (20)  NULL,
    [IDCMBGRDIA] NUMERIC (15)  NULL,
    [IDUAPRBCN]  NUMERIC (15)  NULL,
    [INDBMBS]    BIT           NULL,
    [IDCNTRTSTA] NUMERIC (15)  NULL,
    CONSTRAINT [PK_MSGTRO00] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID CONTRATISTA', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDCNTRTSTA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IND BAMBAS', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'INDBMBS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID USUARIO APROBACION', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDUAPRBCN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID CAMBIO GUARDIA', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDCMBGRDIA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'USUARIO EDICION', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'UEDCN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FECHA EDICION', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'FEDCN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'USUARIO CREACION', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'UCRCN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FECHA CREACION', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'FCRCN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OBSERVACIONES', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'OBSRVCNS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IND TAREO', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'INDTRO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FECHA INICIO DE TAREO', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'FINCTRO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FECHA TAREO', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'FCHTRO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID PERSONA', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDPRSNA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID GRUPO', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDGRPO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID SUPERVISOR', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDSPRVSR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID ROSTER', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDRSTER';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID GUARDIA', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'IDGRDIA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GRUPO DATO TURNO', @level0type = N'SCHEMA', @level0name = N'SEG', @level1type = N'TABLE', @level1name = N'MSGTRO00', @level2type = N'COLUMN', @level2name = N'GDTRNO';

