﻿CREATE TABLE [SEG].[MSGEMP20] (
    [ID]          NUMERIC (15)  IDENTITY (1, 1) NOT NULL,
    [DSCRPCN]     VARCHAR (150) NULL,
    [IDRSTR]      NUMERIC (15)  NOT NULL,
    [FINCO]       DATE          NULL,
    [FFN]         DATE          NULL,
    [GDTRNO]      VARCHAR (1)   NULL,
    [UCRCN]       VARCHAR (20)  NULL,
    [GDESTDO]     VARCHAR (1)   NULL,
    [HRAINCODA]   VARCHAR (7)   NULL,
    [HRAINCONCHE] VARCHAR (7)   NULL,
    [HRAFNDA]     VARCHAR (7)   NULL,
    [HRAFNNCHE]   VARCHAR (7)   NULL,
    [DSGRDA]      INT           NULL,
    [NCHSGRDA]    INT           NULL,
    [DSDSCNSO]    INT           NULL,
    [FESTDO]      DATETIME      NULL,
    [FCRCN]       DATETIME      NULL,
    [UEDCN]       VARCHAR (20)  NULL,
    [FEDCN]       DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC, [IDRSTR] ASC),
    FOREIGN KEY ([IDRSTR]) REFERENCES [SEG].[MSGEMP17] ([ID])
);

