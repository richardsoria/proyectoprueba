﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP00_ListarTodasEmpresas]
AS
BEGIN
Select  [ID],
        [NEMPRSA],
        [RUC],
        [dbo].[UF_OBT_UBIGEO] (UBGO,1) UBGO,	
        [DRCCN],
        [GDESTDO],
        [FESTDO],
        [UCRCN],
        [FCRCN],
        [UEDCN],
        [FEDCN]
	  FROM [SEG].MSGEMP00 
END