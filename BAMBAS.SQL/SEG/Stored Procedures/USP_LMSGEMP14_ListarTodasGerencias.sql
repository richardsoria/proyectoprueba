﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP14_ListarTodasGerencias]
AS
BEGIN
Select  [ID],
        [DSCRPCN],
        [GDESTDO],
        [FESTDO],
        [UCRCN],
        [FCRCN],
        [UEDCN],
        [FEDCN]
	  FROM [SEG].MSGEMP14 
END