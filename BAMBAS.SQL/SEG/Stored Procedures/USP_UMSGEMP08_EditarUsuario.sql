﻿CREATE PROCEDURE [SEG].[USP_UMSGEMP08_EditarUsuario]
	@ID		  NUMERIC(15), 
	@CUSRO	  VARCHAR(15),
	@CLVE	  VARCHAR(50),
	@FVCLVE   DATETIME,
	@FBLQUO   VARCHAR(1),
	@FRZRCMBOCLVE bit,
	@GDESTDO  VARCHAR(1),
	@UEDCN    VARCHAR(15),
	@RETORNO	NUMERIC(1) OUTPUT
AS
BEGIN
	BEGIN TRY  
		SET @RETORNO = 0
		BEGIN TRANSACTION
				UPDATE SEG.MSGEMP08 
				SET
				CUSRO = @CUSRO, 
				INTNTS =  CASE WHEN @FBLQUO = '0' THEN 0 ELSE INTNTS END,
				CLVE = @CLVE,
				FVCLVE = @FVCLVE,
				FBLQUO = @FBLQUO,
				FRZRCMBOCLVE = @FRZRCMBOCLVE,
				GDESTDO = @GDESTDO,
				FESTDO = CASE WHEN GDESTDO = @GDESTDO THEN FESTDO ELSE [dbo].[UF_OBT_FECHA]() END,
				FEDCN = [dbo].[UF_OBT_FECHA](), 
				UEDCN = @UEDCN
				WHERE ID = @ID  
		COMMIT TRANSACTION
		SET @RETORNO = 1 
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRANSACTION
		EXECUTE dbo.USP_IMSGEMP02 'SEG', @UEDCN, 'ERROR AL INTENTAR MODIFICAR EL REGISTRO DEL USUARIO', @ID OUTPUT;
	END CATCH;  
END
GO