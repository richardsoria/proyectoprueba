﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP10_ListarSucursalUsuario]
@IDSCRSL numeric(15,0)
AS
DECLARE @ACTIVO VARCHAR(1);
DECLARE @INACTIVO VARCHAR(1);
SET @ACTIVO = 'A'
SET @INACTIVO = 'I'
BEGIN
SELECT emp08.ID, emp08.NYAPLLDS,emp08.CUSRO,
(
	CASE WHEN  EXISTS 
		(SELECT * 
		 FROM SEG.MSGEMP10 EMP10 
		 WHERE EMP10.IDUSRO=emp08.ID 
		 AND EMP10.IDSCRSL = @IDSCRSL
		 AND EMP10.GDESTDO != @INACTIVO
		 ) 
	THEN 1
	ELSE 0
	END
) AS CHECKEADO,
(
	SELECT EMP10.FEDCN 
	FROM SEG.MSGEMP10 EMP10 
	WHERE EMP10.IDUSRO=emp08.ID 
	AND EMP10.IDSCRSL = @IDSCRSL
) AS FEDCN
FROM SEG.MSGEMP08 emp08
END