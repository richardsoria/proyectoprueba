﻿CREATE PROCEDURE [SEG].[USP_UMSGEMP16_ActualizarArea]
	@ID			INT, 
	@IDGRNCA	NUMERIC(15), 
	@IDSPRINTNDNCA	NUMERIC(15), 
	@DSCRPCN	VARCHAR(150),
	@GDESTDO	VARCHAR(1),
	@UEDCN		VARCHAR(20),
	@RETORNO	NUMERIC(1) OUTPUT 
AS
	DECLARE @ACTIVO VARCHAR(1) = 'A'
	DECLARE @INACTIVO VARCHAR(1) = 'I'
BEGIN
DECLARE @Existe NUMERIC(15) 
	SELECT @Existe = SEG.UF_EXSTE_ARA(@DSCRPCN,@IDSPRINTNDNCA,@ID)

	IF @Existe > 0 
		BEGIN
			RAISERROR('EL ÁREA A INGRESAR YA ESTÁ REGISTRADA EN LA SUPERINTENDENCIA SELECCIONADA, VALIDAR!',15,217);
			RETURN;
		END;
	BEGIN TRY  
		BEGIN TRANSACTION
			SET @RETORNO = 0;
			UPDATE SEG.MSGEMP16
			SET
			IDGRNCA = @IDGRNCA,
			IDSPRINTNDNCA = @IDSPRINTNDNCA,
			DSCRPCN	=	@DSCRPCN, 
			GDESTDO	=	@GDESTDO, 
			FESTDO  =	CASE WHEN @GDESTDO != GDESTDO THEN [dbo].[UF_OBT_FECHA]() ELSE FESTDO END,
			UEDCN  	=	@UEDCN,   
			FEDCN  	=	[dbo].[UF_OBT_FECHA]()
			WHERE ID = @ID  
			----
			COMMIT TRANSACTION
			SET @RETORNO= 1;
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRANSACTION
		EXECUTE DBO.USP_IMSGEMP02 'SEG', @UEDCN, 'ERROR AL INTENTAR MODIFICAR EL REGISTRO DEL AREA', @ID OUTPUT;
	END CATCH;  
END

select * from seg.MSGEMP02 order by edtme desc