﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP20_ListarTodasGuardias]
AS
BEGIN
Select  [ID],
        [DSCRPCN],
		[IDRSTR],
		[GDTRNO],
        [GDESTDO],
        [FESTDO],
        [UCRCN],
        [FCRCN],
        [UEDCN],
        [FEDCN]
	  FROM [SEG].MSGEMP20 
END