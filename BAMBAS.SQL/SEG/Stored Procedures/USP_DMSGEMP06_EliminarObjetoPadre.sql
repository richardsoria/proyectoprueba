﻿CREATE PROCEDURE [SEG].[USP_DMSGEMP06_EliminarObjetoPadre]
	@ID		NUMERIC(15),	
	@UEDCN		VARCHAR(15),
	@RETORNO	NUMERIC(1) OUTPUT
AS
	DECLARE @ACTIVO VARCHAR(1) = 'A'
	DECLARE @INACTIVO VARCHAR(1) = 'I'
	SET @RETORNO = 0
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION 
			UPDATE SEG.MSGEMP06
			SET GDESTDO		= CASE WHEN GDESTDO=@INACTIVO THEN @ACTIVO ELSE @INACTIVO END,	
				FESTDO    	= [dbo].[UF_OBT_FECHA](),
				UEDCN		= @UEDCN,
				FEDCN		= [dbo].[UF_OBT_FECHA]()
			WHERE ID = @ID;
			--desactivar OBJETOS DETALLES
			IF((SELECT GDESTDO FROM SEG.MSGEMP06 WHERE ID = @ID)=@INACTIVO)
			BEGIN
				UPDATE SEG.MSGEMP07
				SET GDESTDO		= @INACTIVO,	
					FESTDO    	= [dbo].[UF_OBT_FECHA](),
					UEDCN		= @UEDCN,
					FEDCN		= [dbo].[UF_OBT_FECHA]()
				WHERE [IDOBJTO] = @ID;
			END
			---
		COMMIT TRANSACTION
		SET @RETORNO = 1 
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRANSACTION
			EXECUTE dbo.USP_IMSGEMP02 'SEG', @UEDCN, 'ERROR AL INTENTAR MODIFICAR EL REGISTRO DEL OBJETO PADRE', @ID OUTPUT;
	END CATCH;  
END