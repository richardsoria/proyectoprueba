﻿CREATE PROCEDURE [SEG].[USP_OMSGEMP03_ObtenerParametroPorAbreviacion]
	@APRMTRO	VARCHAR(50)
AS
BEGIN
		SELECT	ID,
				DPRMTRO,
				APRMTRO,
				VLR1,
				VLR2,
				GDESTDO,
				FESTDO,
				UCRCN,
				FCRCN,
				UEDCN,
				FEDCN 
		FROM SEG.MSGEMP03 
		WHERE APRMTRO = @APRMTRO
END