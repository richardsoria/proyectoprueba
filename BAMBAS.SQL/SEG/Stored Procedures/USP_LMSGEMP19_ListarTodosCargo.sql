﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP19_ListarTodosCargo]
@IDGRPO INT null
AS 
BEGIN
Select  emp19.[ID],
		emp19.[IDGRPO],
		emp18.[DSCRPCN] AS GRPO,-----eeeeeeeeeeeee
        emp19.[DSCRPCN],
        emp19.[GDESTDO],
        emp19.[FESTDO],
        emp19.[UCRCN],
        emp19.[FCRCN],
        emp19.[UEDCN],
        emp19.[FEDCN]
	  FROM [SEG].MSGEMP19 emp19
	  INNER JOIN [SEG].MSGEMP18 emp18
	  ON emp19.IDGRPO = emp18.ID
	  WHERE [IDGRPO] = ISNULL(@IDGRPO,IDGRPO)
END