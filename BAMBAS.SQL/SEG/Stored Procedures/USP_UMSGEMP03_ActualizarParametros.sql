﻿CREATE PROCEDURE [SEG].[USP_UMSGEMP03_ActualizarParametros]
	@ID			NUMERIC(15),
	@DPRMTRO	VARCHAR(150),
	@APRMTRO	VARCHAR(50),
	@VLR1		VARCHAR(250),
	@VLR2		VARCHAR(250),
	@GDESTDO	VARCHAR(1), 
	@UEDCN		VARCHAR(15),
	@RETORNO	NUMERIC(1) OUTPUT
AS
BEGIN
	BEGIN TRY
		SET @RETORNO = 0
		BEGIN TRANSACTION 
			UPDATE SEG.MSGEMP03
			SET DPRMTRO = @DPRMTRO,		
				APRMTRO = @APRMTRO,
				VLR1 = @VLR1,		
				VLR2 = @VLR2,		
				GDESTDO = @GDESTDO,	
				FESTDO = CASE WHEN @GDESTDO != GDESTDO THEN [dbo].[UF_OBT_FECHA]() ELSE FESTDO END,
				UEDCN = @UEDCN,
				FEDCN = [dbo].[UF_OBT_FECHA]()
			WHERE ID = @ID;
		COMMIT TRANSACTION
		SET @RETORNO = 1 
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRANSACTION
			--EXECUTE SEG.USP_IMSGEMP02 'SEG',@UEDCN, @ID OUTPUT;
			EXECUTE dbo.USP_IMSGEMP02 'SEG', @UEDCN, 'ERROR AL INTENTAR MODIFICAR EL REGISTRO DE PARAMETROS', @ID OUTPUT;
	END CATCH;  
END