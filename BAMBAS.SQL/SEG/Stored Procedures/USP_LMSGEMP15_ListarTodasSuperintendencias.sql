﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP15_ListarTodasSuperintendencias]
@IDGRNCA NUMERIC(15) NULL
AS
BEGIN
Select  emp15.[ID],
		emp15.[IDGRNCA],
		emp14.DSCRPCN AS GRNCA,
        emp15.[DSCRPCN],
        emp15.[GDESTDO],
        emp15.[FESTDO],
        emp15.[UCRCN],
        emp15.[FCRCN],
        emp15.[UEDCN],
        emp15.[FEDCN]
	  FROM [SEG].MSGEMP15 emp15
	  INNER JOIN [SEG].MSGEMP14 emp14
	  ON emp14.ID = emp15.IDGRNCA
	  WHERE [IDGRNCA] = ISNULL(@IDGRNCA,IDGRNCA)
END