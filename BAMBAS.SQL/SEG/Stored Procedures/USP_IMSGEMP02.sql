﻿
CREATE PROCEDURE [SEG].[USP_IMSGEMP02]
	@EESQMA			varchar (10),   
	@UCRCN			varchar (15),
	@ErrG			varchar (250),
	@ID			    numeric OUTPUT 
AS


	BEGIN TRAN TErrr;
		INSERT INTO SEG.MSGEMP02
								(
									UNME,    
									EESQMA,	
									ENMBR,	
									ESVRT,	
									ESTTE,	
									EPRCDRE,	
									ERRORLNE,
									EMSSGE,
									UCRCN,
									EDTME	
								)	
		VALUES
		(  
		   SUSER_SNAME(),
		   @EESQMA,
		   ERROR_NUMBER(),
		   ERROR_SEVERITY(),
		   ERROR_STATE(),		   
		   ERROR_PROCEDURE(),
		   ERROR_LINE(),
		   ERROR_MESSAGE(),
		   @UCRCN,
		   GETDATE()
		);
	SET @ID = Scope_identity();
	COMMIT TRAN TErrr;

	RAISERROR (@ErrG, -- Message text.  
               16, -- Severity.  
               1 -- State.  
               );

GO
GRANT EXECUTE
    ON OBJECT::[SEG].[USP_IMSGEMP02] TO [UPER00]
    AS [dbo];

