﻿
CREATE PROCEDURE [SEG].[USP_OMSGEMP08_ObtenerUsuarioXid] 
	@ID int
AS
BEGIN
	SELECT [ID]
      ,[IDPRSNA]
      ,[CUSRO]
      ,[NYAPLLDS]
      ,[TDCMNTO]
      ,[NDCMNTO]
      ,[NTLFNO]
      ,[CLVE]
      ,Convert(varchar, FVCLVE, 103) as FVCLVE
      ,[FBLQUO]
      ,[GDESTDO]
      ,[FESTDO]
      ,[UCRCN]
      ,[FCRCN]
      ,[UEDCN]
      ,[FEDCN]
      ,[INTNTS]
      ,[FRZRCMBOCLVE]
  FROM [SEG].[MSGEMP08] 
	where ID = @ID;
END