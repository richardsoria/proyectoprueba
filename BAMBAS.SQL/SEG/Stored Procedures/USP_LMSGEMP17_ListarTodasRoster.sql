﻿CREATE PROCEDURE [SEG].[USP_LMSGEMP17_ListarTodasRoster]
AS
BEGIN
Select  [ID],
        [DSCRPCN],
		[DTRBJDS],
		[DDSCNSO],
        [GDESTDO],
        [FESTDO],
        [UCRCN],
        [FCRCN],
        [UEDCN],
        [FEDCN]
	  FROM [SEG].MSGEMP17 
END