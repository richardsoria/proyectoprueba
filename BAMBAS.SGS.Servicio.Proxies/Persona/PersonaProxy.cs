﻿using BAMBAS.SGS.Servicio.Proxies.Persona.Comandos;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Proxies.Persona
{
    public interface IPersonaProxy
    {
        Task<ComandoPersonaPrincipalConsulta> ObtenerPrincipal(int id);
    }
    public class PersonaProxy : IPersonaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PersonaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<int> InsertarPersona(ComandoPersonaInsertar command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync(_apiUrls.PersonaUrl + "persona/insertar", content);

            request.EnsureSuccessStatusCode();

            //leer el cdigo que devuelve persona
            var idResultado = await request.Content.ReadAsStringAsync();
            return Convert.ToInt32(idResultado);
        }
        public async Task<ComandoPersonaPrincipalConsulta> ObtenerPrincipal(int id)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/obtenerprincipal?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ComandoPersonaPrincipalConsulta>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
