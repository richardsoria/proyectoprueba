﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Proxies.Persona.Comandos
{
    public class ComandoPersonaInsertar
    {
        public string GDPRSNA { get; set; }
        public string RSCL { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public string NCNLDD { get; set; }
        public string GDECVL { get; set; }
        public string GDSXO { get; set; }
        public string FNCMNTO { get; set; }
        public string FFLLCMNTO { get; set; }
        public string CUBGEO { get; set; }

        //auitoria
        public int ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; }
        public DateTime? FESTDO { get; set; }
    }
}
