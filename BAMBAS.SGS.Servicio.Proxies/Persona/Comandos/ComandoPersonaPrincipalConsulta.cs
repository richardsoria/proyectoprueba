﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Proxies.Persona.Comandos
{
    public class ComandoPersonaPrincipalConsulta
    {
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public bool FCNTRTSTA { get; set; }

        //Telefono Principal
        public string GDTTLFNO { get; set; }
        public string NTLFNO { get; set; }

        //Documento Principal
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }

        //Correo Principal
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }

        //Dirección Principal
        public string GDTDRCCN { get; set; }

        public string DRCCN { get; set; }
    }
}
