﻿namespace SuSaludServ
{


    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(Name = "consulta-afiliadosPort", Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas", ConfigurationName = "SuSaludServ.consultaafiliadosPort")]
    public interface consultaafiliadosPort
    {

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        System.Threading.Tasks.Task<SuSaludServ.ConsultaResponse1> ConsultaAsync(SuSaludServ.ConsultaRequest1 request);
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas")]
    public partial class ConsultaRequest
    {

        private string idInstitucionField;

        private string tiDocumentoField;

        private string nuDocumentoField;

        private string doConsultanteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string idInstitucion
        {
            get
            {
                return this.idInstitucionField;
            }
            set
            {
                this.idInstitucionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string tiDocumento
        {
            get
            {
                return this.tiDocumentoField;
            }
            set
            {
                this.tiDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string nuDocumento
        {
            get
            {
                return this.nuDocumentoField;
            }
            set
            {
                this.nuDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string doConsultante
        {
            get
            {
                return this.doConsultanteField;
            }
            set
            {
                this.doConsultanteField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas")]
    public partial class ConsultaResponse
    {

        private string coErrorField;

        private string tiDocumentoField;

        private string nuDocumentoField;

        private string apPaternoField;

        private string apMaternoField;

        private string apCasadaField;

        private string noPersonaField;

        private string deSexoField;

        private string feNacimientoField;

        private string deUbigeoField;

        private string coContinenteField;

        private string deContinenteField;

        private string coPaisField;

        private string dePaisField;

        private string coDepartamentoField;

        private string deDepartamentoField;

        private string coProvinciaField;

        private string deProvinciaField;

        private string coDistritoField;

        private string deDistritoField;

        private string inFallecimientoField;

        private string feFallecimientoField;

        private string coPaisEmisorField;

        private AfiliacionesAfiliacion[] afiliacionesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string coError
        {
            get
            {
                return this.coErrorField;
            }
            set
            {
                this.coErrorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string tiDocumento
        {
            get
            {
                return this.tiDocumentoField;
            }
            set
            {
                this.tiDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string nuDocumento
        {
            get
            {
                return this.nuDocumentoField;
            }
            set
            {
                this.nuDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string apPaterno
        {
            get
            {
                return this.apPaternoField;
            }
            set
            {
                this.apPaternoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string apMaterno
        {
            get
            {
                return this.apMaternoField;
            }
            set
            {
                this.apMaternoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string apCasada
        {
            get
            {
                return this.apCasadaField;
            }
            set
            {
                this.apCasadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string noPersona
        {
            get
            {
                return this.noPersonaField;
            }
            set
            {
                this.noPersonaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string deSexo
        {
            get
            {
                return this.deSexoField;
            }
            set
            {
                this.deSexoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string feNacimiento
        {
            get
            {
                return this.feNacimientoField;
            }
            set
            {
                this.feNacimientoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string deUbigeo
        {
            get
            {
                return this.deUbigeoField;
            }
            set
            {
                this.deUbigeoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string coContinente
        {
            get
            {
                return this.coContinenteField;
            }
            set
            {
                this.coContinenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string deContinente
        {
            get
            {
                return this.deContinenteField;
            }
            set
            {
                this.deContinenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string coPais
        {
            get
            {
                return this.coPaisField;
            }
            set
            {
                this.coPaisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string dePais
        {
            get
            {
                return this.dePaisField;
            }
            set
            {
                this.dePaisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string coDepartamento
        {
            get
            {
                return this.coDepartamentoField;
            }
            set
            {
                this.coDepartamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string deDepartamento
        {
            get
            {
                return this.deDepartamentoField;
            }
            set
            {
                this.deDepartamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string coProvincia
        {
            get
            {
                return this.coProvinciaField;
            }
            set
            {
                this.coProvinciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string deProvincia
        {
            get
            {
                return this.deProvinciaField;
            }
            set
            {
                this.deProvinciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string coDistrito
        {
            get
            {
                return this.coDistritoField;
            }
            set
            {
                this.coDistritoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string deDistrito
        {
            get
            {
                return this.deDistritoField;
            }
            set
            {
                this.deDistritoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string inFallecimiento
        {
            get
            {
                return this.inFallecimientoField;
            }
            set
            {
                this.inFallecimientoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string feFallecimiento
        {
            get
            {
                return this.feFallecimientoField;
            }
            set
            {
                this.feFallecimientoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string coPaisEmisor
        {
            get
            {
                return this.coPaisEmisorField;
            }
            set
            {
                this.coPaisEmisorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 23)]
        [System.Xml.Serialization.XmlArrayItemAttribute("afiliacion", IsNullable = false)]
        public AfiliacionesAfiliacion[] Afiliaciones
        {
            get
            {
                return this.afiliacionesField;
            }
            set
            {
                this.afiliacionesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas")]
    public partial class AfiliacionesAfiliacion
    {

        private string coAfiliacionField;

        private string tiPlanSaludField;

        private string dePlanSaludField;

        private string coIafasField;

        private string tiIafasField;

        private string deIafasField;

        private string coParentescoField;

        private string coPaisTitularField;

        private string tiDocTitularField;

        private string nuDocTitularField;

        private string feIniAfiliacionField;

        private string feFinAfiliacionField;

        private string feFinCoberturaField;

        private string feActIafasField;

        private string feActSunasaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string coAfiliacion
        {
            get
            {
                return this.coAfiliacionField;
            }
            set
            {
                this.coAfiliacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string tiPlanSalud
        {
            get
            {
                return this.tiPlanSaludField;
            }
            set
            {
                this.tiPlanSaludField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string dePlanSalud
        {
            get
            {
                return this.dePlanSaludField;
            }
            set
            {
                this.dePlanSaludField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string coIafas
        {
            get
            {
                return this.coIafasField;
            }
            set
            {
                this.coIafasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string tiIafas
        {
            get
            {
                return this.tiIafasField;
            }
            set
            {
                this.tiIafasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string deIafas
        {
            get
            {
                return this.deIafasField;
            }
            set
            {
                this.deIafasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string coParentesco
        {
            get
            {
                return this.coParentescoField;
            }
            set
            {
                this.coParentescoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string coPaisTitular
        {
            get
            {
                return this.coPaisTitularField;
            }
            set
            {
                this.coPaisTitularField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string tiDocTitular
        {
            get
            {
                return this.tiDocTitularField;
            }
            set
            {
                this.tiDocTitularField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string nuDocTitular
        {
            get
            {
                return this.nuDocTitularField;
            }
            set
            {
                this.nuDocTitularField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string feIniAfiliacion
        {
            get
            {
                return this.feIniAfiliacionField;
            }
            set
            {
                this.feIniAfiliacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string feFinAfiliacion
        {
            get
            {
                return this.feFinAfiliacionField;
            }
            set
            {
                this.feFinAfiliacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string feFinCobertura
        {
            get
            {
                return this.feFinCoberturaField;
            }
            set
            {
                this.feFinCoberturaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string feActIafas
        {
            get
            {
                return this.feActIafasField;
            }
            set
            {
                this.feActIafasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string feActSunasa
        {
            get
            {
                return this.feActSunasaField;
            }
            set
            {
                this.feActSunasaField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class ConsultaRequest1
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas", Order = 0)]
        public SuSaludServ.ConsultaRequest ConsultaRequest;

        public ConsultaRequest1()
        {
        }

        public ConsultaRequest1(SuSaludServ.ConsultaRequest ConsultaRequest)
        {
            this.ConsultaRequest = ConsultaRequest;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class ConsultaResponse1
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.susalud.gob.pe/ws/consulta/afiliados/schemas", Order = 0)]
        public SuSaludServ.ConsultaResponse ConsultaResponse;

        public ConsultaResponse1()
        {
        }

        public ConsultaResponse1(SuSaludServ.ConsultaResponse ConsultaResponse)
        {
            this.ConsultaResponse = ConsultaResponse;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface consultaafiliadosPortChannel : SuSaludServ.consultaafiliadosPort, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class consultaafiliadosPortClient : System.ServiceModel.ClientBase<SuSaludServ.consultaafiliadosPort>, SuSaludServ.consultaafiliadosPort
    {

        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);

        public consultaafiliadosPortClient() :
                base(consultaafiliadosPortClient.GetDefaultBinding(), consultaafiliadosPortClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.consulta_afiliadosPortSoap11.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public consultaafiliadosPortClient(EndpointConfiguration endpointConfiguration) :
                base(consultaafiliadosPortClient.GetBindingForEndpoint(endpointConfiguration), consultaafiliadosPortClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public consultaafiliadosPortClient(EndpointConfiguration endpointConfiguration, string remoteAddress) :
                base(consultaafiliadosPortClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public consultaafiliadosPortClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) :
                base(consultaafiliadosPortClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public consultaafiliadosPortClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<SuSaludServ.ConsultaResponse1> SuSaludServ.consultaafiliadosPort.ConsultaAsync(SuSaludServ.ConsultaRequest1 request)
        {
            return base.Channel.ConsultaAsync(request);
        }

        public System.Threading.Tasks.Task<SuSaludServ.ConsultaResponse1> ConsultaAsync(SuSaludServ.ConsultaRequest ConsultaRequest)
        {
            SuSaludServ.ConsultaRequest1 inValue = new SuSaludServ.ConsultaRequest1();
            inValue.ConsultaRequest = ConsultaRequest;
            return ((SuSaludServ.consultaafiliadosPort)(this)).ConsultaAsync(inValue);
        }

        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }

        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }

        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.consulta_afiliadosPortSoap11))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.consulta_afiliadosPortSoap11))
            {
                return new System.ServiceModel.EndpointAddress("http://app9.susalud.gob.pe:8080/ws-consulta-afiliados/services/");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }

        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return consultaafiliadosPortClient.GetBindingForEndpoint(EndpointConfiguration.consulta_afiliadosPortSoap11);
        }

        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return consultaafiliadosPortClient.GetEndpointAddress(EndpointConfiguration.consulta_afiliadosPortSoap11);
        }

        public enum EndpointConfiguration
        {

            consulta_afiliadosPortSoap11,
        }
    }
}
