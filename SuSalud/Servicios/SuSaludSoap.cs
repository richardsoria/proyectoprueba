﻿using BAMBAS.CORE.Structs;
using Microsoft.AspNetCore.Mvc;
using SuSaludServ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;
using BAMBAS.CORE.Helpers;

namespace SuSalud.Servicios
{
    public interface ISuSaludSoap
    {
        Task<string> Consultar(string _tiDocumento, string _nuDocumento);
        Task<DataTablesStructs.ReturnedData<ConsultaResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string tipoDocumento, string numDocumento);
    }
    public class SuSaludSoap: ISuSaludSoap
    {
        public SuSaludSoap()
        {

        }
        public async Task<string> Consultar(string _tiDocumento, string _nuDocumento)
        {
            ConsultaRequest wsSusalud_var_entrada = new ConsultaRequest();
            consultaafiliadosPortClient wsSusalud_metodo = new consultaafiliadosPortClient();
            wsSusalud_var_entrada.idInstitucion = "20100121809";
            wsSusalud_var_entrada.doConsultante = "09382732";
            wsSusalud_var_entrada.nuDocumento = _nuDocumento;
            wsSusalud_var_entrada.tiDocumento = _tiDocumento;
            ConsultaResponse1 wsSusalud_var_obtener = new ConsultaResponse1();
            string strJson = "";
            try
            {
                wsSusalud_var_obtener = await wsSusalud_metodo.ConsultaAsync(wsSusalud_var_entrada);
                if (wsSusalud_var_obtener.ConsultaResponse.coError == "0000")
                    strJson = JsonSerializer.Serialize(wsSusalud_var_obtener.ConsultaResponse);
                else
                    strJson = "";
                return strJson;
            }
            catch 
            {
                wsSusalud_var_obtener.ConsultaResponse = new ConsultaResponse();                
                return strJson = JsonSerializer.Serialize(wsSusalud_var_obtener.ConsultaResponse);
            }
        }
        public async Task<DataTablesStructs.ReturnedData<ConsultaResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string tipoDocumento, string numDocumento)
        {

            ConsultaRequest wsSusalud_var_entrada = new ConsultaRequest();
            consultaafiliadosPortClient wsSusalud_metodo = new consultaafiliadosPortClient();
            wsSusalud_var_entrada.idInstitucion = "20100121809";
            wsSusalud_var_entrada.doConsultante = "09382732";
            wsSusalud_var_entrada.tiDocumento = tipoDocumento;
            wsSusalud_var_entrada.nuDocumento = numDocumento;
            List<ConsultaResponse> lista = new List<ConsultaResponse>();
            ConsultaResponse1 wsSusalud_var_obtener = new ConsultaResponse1();

            try
            {
                wsSusalud_var_obtener = await wsSusalud_metodo.ConsultaAsync(wsSusalud_var_entrada);
                wsSusalud_var_obtener.ConsultaResponse.Afiliaciones = null;
                wsSusalud_var_obtener.ConsultaResponse.feNacimiento = ConvertHelpers.DatepickerToDatetime(wsSusalud_var_obtener.ConsultaResponse.feNacimiento).ToString();
                lista.Add(wsSusalud_var_obtener.ConsultaResponse);
            }
            catch (Exception)
            {
                return new DataTablesStructs.ReturnedData<ConsultaResponse>
                {
                    Data = lista,
                    DrawCounter = parameters.DrawCounter,
                    RecordsFiltered = 0,
                    RecordsTotal = 0
                };
            }         
                
            var recordsFiltered = lista.Count();
            var data = lista
                .ToList();
            var recordsTotal = data.Count;
            return new DataTablesStructs.ReturnedData<ConsultaResponse>
            {
                Data = data,
                DrawCounter = parameters.DrawCounter,
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };
        }
    }
}
