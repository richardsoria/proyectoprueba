﻿jQuery.extend(jQuery.validator.messages, {
    required: "Campo requerido.",
    maxlength: jQuery.validator.format("Por favor, no ingresar más de {0} caracteres."),
    max: jQuery.validator.format("Por favor, ingresar un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, ingresar un valor mayor o igual a {0}."),
    equalTo: jQuery.validator.format("Por favor, escriba lo mismo."),
    email: jQuery.validator.format("Debes ingresar un correo electrónico válido."),
    digits: "Por favor solo ingresar dígitos",
    number: "Por favor ingresar un número válido.",
    minlength: "Por favor ingresar al menos {0} carácteres.",
});


$.validator.addMethod("alfanumerico", function (value, element) {
    return (/^[0-9a-zA-Z\sáéíóúñÑÁÉÍÓÚüÜ ]+$/).test(value);

}, "Por favor ingresar caracteres alfanumericos.");

$.validator.addMethod("longitudexacta", function (value, element, param) {
    return this.optional(element) || value.length == param;
}, $.validator.format("Por favor ingresar {0} caracteres."));

$.validator.addMethod("sololetras", function (value, element) {
    return (/^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ]+$/).test(value);
    // --or leave a space here ^^
}, "Por favor ingresar solo letras.");

//$.validator.addMethod("sololetrasyespacios", function (value, element) {
//    return (/^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$/).test(value);
//}, "Por favor ingresar solo letras y espacios.");

$.validator.addMethod("solonumeros", function (value, element) {
    return (/^[0-9]+$/).test(value);
}, "Por favor ingresar solo números.");


function validarEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

$(":input").attr("autocomplete", "off");


function mayorFechaActual(fecAdd, factual) {
    var valFecha = false;
    var fechaCon = fecAdd;//fecha Consultada   
    var fCon = fechaCon.split("/");
    var fConAnnio = fCon[2];
    var fConMes = fCon[1];
    var fConDia = fCon[0];


    var annioActual;
    var mesActual;
    var diaActual;
    if (factual) {
        var actualSplit = factual.split("/");
        annioActual = actualSplit[2];
        mesActual = actualSplit[1];
        diaActual = actualSplit[0];
    } else {
        factual = new Date();//fecha Hoy
        annioActual = parseInt(factual.getFullYear());
        mesActual = parseInt(factual.getMonth()) + 1;
        diaActual = parseInt(factual.getDate());
    }

    var annioServ = annioActual - fConAnnio;
    var mesServ = mesActual - fConMes;

    if (mesServ < 0 || (mesServ === 0 && diaActual < fConDia)) {
        annioServ--;
    }
    if (annioServ < 0) {
        valFecha = true;
    }
    return valFecha;
}

$.extend($.fn.dataTable.defaults, {
    searching: true,
    dom: "<'top'if>rt<'bottom'pl><'clear'>",
    language: {
        "sProcessing": "<div class='m-blockui' style='display: inline; background: none; box-shadow: none;'><span>Cargando datos...</span><span><div class='m-loader  m-loader--brand m-loader--lg'></div></span></div>",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando _START_ - _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 - 0 de 0 registros",
        "sInfoFiltered": "(filtrado de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "pagingType": "simple_numbers",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "<i class='la la-angle-double-left'></i>",
            "sLast": "<i class='la la-angle-double-right'></i>",
            "sNext": "<i class='la la-angle-right'></i>",
            "sPrevious": "<i class='la la-angle-left'></i>"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    lengthMenu: [10, 25, 50],
    lengthChange: true,
    orderMulti: false,
    pagingType: "full_numbers",
    processing: true,
    responsive: true,
    serverSide: true,
    info: true,
    order: [],
    filter: true,
    pageLength: 10,
    paging: true,
    fixedColumns: true,
    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull, a, a) {
        // Bold the grade for all 'A' grade browsers
        var row = nRow._DT_RowIndex;

        var elementoTabla = $(this);
        var tabla = $("#" + elementoTabla[0].id).DataTable();
        var table = tabla;
        var info = table.page.info();
        nRow.cells[0].innerHTML = (info.page * info.length) + (row + 1);
    },
    columnDefs: [
        {
            targets: "_all",
            className: 'text-center'
        }
    ]
});

$('.formatoFechas').bind('keypress', function (event) {
    var thisVal = $(this).val();
    var regex = new RegExp("^[0-9/]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

    var leng = thisVal.length;
    var reg = new RegExp(/^(((0[13578]|1[02])\-(0[1-9]|[12]\d|3[01])\-((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\-(0[1-9]|[12]\d|30)\-((19|[2-9]\d)\d{2}))|(02\-(0[1-9]|1\d|2[0-8])\-((19|[2-9]\d)\d{2}))|(02\-29\-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g);
    if (leng == 10) {
        var isValid = reg.test(thisVal);
        return isValid;
    }
});

function FuncionAlfanumerico(e) {
    var keyCode = e.keyCode || e.which;
    var regex = /^[0-9a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
}

$(".alfanumerico").keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
    return FuncionAlfanumerico(e);
});
$(document).on('keypress', '.alfaNumCharEsp', function (event) {
    var regex = new RegExp("^[A-Za-z0-9\-_()., \sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    return true;
});
var value_befort = "";
$(document).on('keypress', '.decimales', function (event) {
    var value_befort_key = $(this).val();
    var re = /^[0-9]*(\.[0-9]{0,2})?$/;
    var OK = re.exec(value_befort_key);
    if (!OK) {
        return false;
    } else {
        value_befort = value_befort_key;
        return true;
    }
});
$(document).on('keyup', '.decimales', function (event) {
    var value_after = $(this).val();
    var re = /^[0-9]*(\.[0-9]{0,2})?$/;
    var OK = re.exec(value_after);
    if (!OK) {
        value_befort = value_befort;
        $(this).val(value_befort);
        return false;
    } else {
        value_befort = value_after;
        return true;
    }
});
$(".alfaNumCharMail").keypress(function (e) {//LETRAS NUMEROS CARACTERES ESPECIFICOS
    var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9\-_@.]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
});

$(".sololetras").bind('keypress', function (event) {//LETRAS
    var regex = new RegExp("^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$(".sololetrasyespacios").bind('keypress', function (event) {//LETRAS Y ESPACIOS
    var regex = new RegExp("^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

function FuncionSoloNumeros(event) {
    var regex = new RegExp("^[0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    return true;
}

$(".solonumeros").bind('keypress', function (event) {//NUMEROS
    return FuncionSoloNumeros(event)
});

$('.text-uppercase').on("change", function () {
    $(this).val($(this).val().trim());
});

$(".text-uppercase").blur("keypress", function () {
    $input = $(this);
    $input.val($input.val().toUpperCase());
});

$(".numeroCaracteres").on('keyup', function () {//CANTIDAD DE CARACTERES USADOS
    var limit = 1000;
    $("#txtDireccionEmp").attr('maxlength', limit);
    var init = $(this).val().length;

    if (init < limit) {
        init++;
        $('.caracteres').text(init + "/1000");
    }
});

jQuery.fn.extend({
    agregarLoading: function () {
        this.html(
            `<div class="loaderbody">
                <div class="loader-div">
                    <div class="loader-wheel"></div>
                    <div class="loader-text"></div>
                </div>
            </div>`
        );
    },
    addLoader: function () {
        this.attr("disabled", true);
        return this.append('<span id="btn-loader" class="spinner-border spinner-border-sm"></span>');
    },
    removeLoader: function () {
        this.attr("disabled", false);
        return this.find("#btn-loader").remove();
    },
    AgregarCamposDefectoAuditoria: function () {
        var $this = this;
        $.ajax({
            url: `/obtener-datos-usuario-logueado`,
            type: "Get"
        })
            .done(function (result) {
                $this.find("[name='ID']").val("");
                $this.find("[name='UEDCN']").val(result.uedcn);
                $this.find("[name='UCRCN']").val(result.ucrcn);
                $this.find("[name='GDESTDO']").val("A");
                $this.find("[name='cFCRCN']").val(result.cFCRCN);
                $this.find("[name='cFEDCN']").val(result.cFEDCN);
                $this.find("[name='cFESTDO']").val(result.cFESTDO);
            });
    },
    AgregarCamposAuditoria: function (result) {
        this.find("[name='ID']").val(result.id);
        this.find("[name='ID-d']").val(result.id);
        this.find("[name='UEDCN']").val(result.uedcn);
        this.find("[name='UCRCN']").val(result.ucrcn);
        this.find("[name='GDESTDO']").val(result.gdestdo);
        this.find("[name='cFCRCN']").val(result.cFCRCN);
        this.find("[name='cFEDCN']").val(result.cFEDCN);
        this.find("[name='cFESTDO']").val(result.cFESTDO);
    },
    DeshabilitarCamposAuditoria: function () {
        this.find("[name='ID-d']").attr("disabled", true);
        this.find("[name='UEDCN']").attr("disabled", true);
        this.find("[name='UCRCN']").attr("disabled", true);
        this.find("[name='cFCRCN']").attr("disabled", true);
        this.find("[name='cFEDCN']").attr("disabled", true);
        this.find("[name='cFESTDO']").attr("disabled", true);
    },
    LlenarSelectEstados: function () {
        var $this = this;
        $this.LlenarSelectGD("GDESTDO");
    },
    LlenarSelectGrupoDatos: function (valorgd) {
        var $this = this;
        $.ajax({
            url: `/agrupadatos`,
            type: "Get",
            data: { gddescripcion: valorgd }
        })
            .done(function (result) {
                $this.append($("<option />").val('').text("Seleccione"));
                $.each(result, function () {
                    $this.append($("<option />").val(this.vlr1).text(this.nombre));
                });
            });
    },
    LlenarSelectGD: function (cGrupoDto, value, text) {
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
            return n.cgdto === cGrupoDto;
        });
        $this.append($("<option />").val('').text("Seleccione"));
        $.each(datos, function (key, item) {
            $this.append($("<option />").val(item[value]).text(item[text]));
        });
    },
    LlenarSelectGDOrdenado: function (cGrupoDto, value, text) {
        function comparando(detalleA, detalleB) {
            let comparison = 0;
            if (detalleA > detalleB) {
                comparison = 1;
            } else if (detalleA < detalleB) {
                comparison = -1;
            }
            return comparison;
        }
        function compare(a, b) {
            ///////////////////COMPARAR POR abreviacion
            const detalleA = a.agddtlle.toUpperCase();
            const detalleB = b.agddtlle.toUpperCase();
            return comparando(detalleA, detalleB);
        }
        function compareDesc(a, b) {
            ///////////////////COMPARAR POR DESCRIPCION
            const detalleA = a.dgddtlle.toUpperCase();
            const detalleB = b.dgddtlle.toUpperCase();
            return comparando(detalleA, detalleB);
        }
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
            return n.cgdto === cGrupoDto;
        });
        var newdatos = (text == "agddtlle") ? datos.sort(compare) : datos.sort(compareDesc);
        $this.append($("<option />").val('').text("Seleccione"));
        $.each(newdatos, function (key, item) {
            $this.append($("<option />").val(item[value]).text(item[text]));
        });
    },
    LlenarSelectGDFiltrado: function (cGrupoDto, noAgregar, agregar, value, text) {
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        var nuevoNoAgregar = [];
        var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
            if (agregar != undefined && agregar != null) {
                nuevoNoAgregar = noAgregar.filter(function (e) {
                    return e != agregar
                });
            } else {
                nuevoNoAgregar = noAgregar;
            }
            return n.cgdto === cGrupoDto && jQuery.inArray(n.vlR1, nuevoNoAgregar) == -1;
        });
        $this.append($("<option />").val('').text("Seleccione"));
        $.each(datos, function (key, item) {
            $this.append($("<option />").val(item[value]).text(item[text]));
        });
    },
    LlenarSelectGrupoDato: function (cGrupoDto, value, text) {
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        $.get("obtenerGrupoDatos", { cgdto: cGrupoDto })
            .done(function (data) {
                $this.append($("<option />").val('').text("Seleccione"));
                $.each(data, function (key, item) {
                    $this.append($("<option />").val(item[value]).text(item[text]));
                });
            });
    },
    LlenarSelectPais: function () {
        var $this = this;
        var value = "cps";
        var text = "dprvnca";
        var datos = $(JSON.parse(localStorage.getItem('paises')));
        /*.filter(function (i, n) {
        return n.cgdto === cGrupoDto;
    });*/
        $this.append($("<option />").val('').text("Seleccione"));
        $.each(datos, function (key, item) {
            $this.append($("<option />").val(item[value]).text(item[text]));
        });
    },
    maxlengthDocumento: function (options) {
        this.on("change", function () {
            let config = options;
            let objAfectado = document.getElementById(config.obj);
            objAfectado.value = '';
            objAfectado.disabled = false;
            var vlor = this.value;
            var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
                return n.cgdto === "GDDCMNTO" && n.vlR1 == vlor;
            });
            if (datos[0]) {
                objAfectado.minLength = 0;
                objAfectado.maxLength = 0;
                objAfectado.maxLength = datos[0].vlR2;
                objAfectado.minLength = datos[0].vlR2;
            }

            if (vlor == 3) {//SI es pasaporte permitir letras
                objAfectado.classList.add("alfanumerico");
                objAfectado.classList.remove("solonumeros");
                $("#" + config.obj).unbind('keypress');
                $("#" + config.obj).keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
                    return FuncionAlfanumerico(e);
                });
            } else {
                objAfectado.classList.add("solonumeros");
                objAfectado.classList.remove("alfanumerico");
                $("#" + config.obj).unbind('keypress');
                $("#" + config.obj).keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
                    return FuncionSoloNumeros(e);
                });
            }
        });
    }

});


function parseXML(xmlString) {
    var parser = new DOMParser();
    // Parse a simple Invalid XML source to get namespace of <parsererror>:
    var docError = parser.parseFromString('INVALID', 'text/xml');
    var parsererrorNS = docError.getElementsByTagName("parsererror")[0].namespaceURI;
    // Parse xmlString:
    // (XMLDocument object)
    var doc = parser.parseFromString(xmlString, 'text/xml');
    if (doc.getElementsByTagNameNS(parsererrorNS, 'parsererror').length > 0) {
        throw new Error('Error parsing XML');
    }
    return doc;
}

function sXMLToJson(cadena) {
    var json = xmlToJson.parse(cadena);
    //console.log(json);
    return json;
};

function isObjEmpty(obj) {
    return Object.keys(obj).length === 0;
}

$('.collapsed').on('click', function () {
    $this = $(this);

    if ($this.hasClass("mostrarA")) {
        this.nextElementSibling.classList.remove('show');
        this.classList.remove('mostrarA')
    } else {
        this.nextElementSibling.classList.add('show');
        this.classList.add('mostrarA')
    }
});

function ObtenerEstado(data) {
    var tpm;
    if (data.gdestdo == "A") {
        tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
    }
    else {
        tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
    }
    return tpm;
}

$.ajaxSetup({
    beforeSend: function (jqXHR, settings) {
        var url = settings.url;
        var carpetaPublicacion = "/per-web";
        if (!url.includes(carpetaPublicacion)) {
            settings.url = carpetaPublicacion + url;
        }
    },
});
//REDIRECCIONES 
function RedirectWithSubfolder(url) {
    var carpetaPublicacion = "/per-web";
    if (!url.includes(carpetaPublicacion)) {
        window.location.href = carpetaPublicacion + url;
    } else {
        window.location.replace(url)
    }
}

$(window).keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});

var cargarCatalogoGD = () => {
    $.get("/per-web/obtenercatalogoGD")
        .done(function (data) {
            var datos = JSON.stringify(data);
            localStorage.setItem("datosCatalogoGD", datos);
        });
}
cargarCatalogoGD();