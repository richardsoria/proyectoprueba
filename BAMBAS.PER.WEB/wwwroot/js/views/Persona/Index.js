﻿var InicializarPersona = function () {        

    //DATOS PARA LA BUSQUEDA
    $cboTPersona_NJ = $("#cboTPersona_NJ");
    $cboTipoDocumento = $("#cboTipoDocumento");
    $txtDocumento = $("#txtDocumento");
    $cboEstado = $(".select-estados");
    $cboEstadoListado = $(".select-estado-listado");
    $txtNombreRazon = $("#txtNombreRazon");

    //BOTON
    $btnBuscarPersona = $("#btnBuscarPersona");


    var $tablaPersona = $("#tabla_persona");
    var $accesoEditarPersona = $("#accesoEditarPersona");
    var $accesoEliminarPersona = $("#accesoEliminarPersona");
    
    var tablaPersona = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/listar-persona",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.p_sTPersona = $cboTPersona_NJ.val();
                    data.p_sTDocumento = $cboTipoDocumento.val();
                    data.p_sNDocumento = $txtDocumento.val();
                    data.p_sEstado = $cboEstadoListado.val();
                    data.p_sDatos = $txtNombreRazon.val();
                }
            },
            columns: [
                {
                    title: "Nro.",
                    className: "text-center",
                    data: null,
                    //width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Persona",
                    data: "tprsna",
                    //width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    //width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto",
                    //width: '10%',
                    orderable: false
                },
                {
                    title: "Nombres y apellidos /Razón",
                    data: "datos",
                    //width: '40%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN",
                    //width: '15%',
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn",
                    //width: '10%',
                    orderable: false,
                },
                {
                    title: "Estado",
                    data: null,
                    //width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    classname: "text-center",
                    data: null,
                    orderable: false,
                    //width: '10%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarPersona.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarPersona.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }                
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaPersona.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/Persona/EliminarPersonas",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).them((result) => {
                                tablaPersona.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaPersona.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                var url = `/Persona/editar/${id}`;
                RedirectWithSubfolder(url);
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        init: function () {
            tablaPersona.objecto = $tablaPersona.DataTable(tablaPersona.opciones);
            tablaPersona.eventos();
        }
    };

    var cargarCombos = {
        init: function () {
            $cboTPersona_NJ.LlenarSelectGD("GDTPRSNA");  
            $cboTipoDocumento.LlenarSelectGD("GDDCMNTO");
            $cboEstado.LlenarSelectGD("GDESTDO");
        }       
    }

    var eventosIncrustadosPersona = {
        botonNuevoPersona: function () {
            $btnNuevoPersona.on("click", function () {
                funcionesPersona.NuevoPersona();
            });
        },
        botonBuscar: function () {
            $btnBuscarPersona.on("click", function () {
                tablaPersona.reload();
            })
        },
        init: function () {
            eventosIncrustadosPersona.botonBuscar();
        }
    }

    return {
        inicializar: function () {
            cargarCombos.init();
            eventosIncrustadosPersona.init();
            tablaPersona.init();
        }
    };
}();

$(() => {
    InicializarPersona.inicializar();
})

