
var initBusquedaPersona = function () {
    //#region Persona
    $FormularioPersona = $("#FormularioPersona");
    $txtNombreRazon = $("#txtNombreRazon");
    $tabla_persona = $("#tabla_persona");
    $btnNuevoPersona = $("#btnNuevoPersona");
    $btnBuscarPersona = $("#btnBuscarPersona");
    $cnt_fDesde = $('.input-group.fecDesde');
    $cnt_fHasta = $('.input-group.fecHasta');
    $txtDesde = $("#txtDesde");
    $txtHasta = $("#txtHasta");
    $cboEstado = $(".select-estados");
    $cboEstadoListado = $(".select-estado-listado");
    $cboEstadoFormulario = $(".select-estado-formulario");
    $cboTipoDocumento = $("#cboTipoDocumento");
    $cboTPersona_NJ = $("#cboTPersona_NJ");
    $txtDocumento = $("#txtDocumento");
    $btnGrabarPersona = $("#btnGrabarPersona");
    $btnCreateWorker = $("#btnCreateWorker");
    $tab_DatosGenerales = $("#DatosGenerales-tab");
    $tab_Direccion = $("#Direccion-tab");
    $tab_Lista = $("#Lista-tab");
    $tab_Telefono = $("#Telefono-tab");
    $tab_CuentaCorreo = $("#CuentaCorreo-tab");
    $tab_Documento = $("#Documento-tab");
    $tab_Archivos = $("#Archivos-tab");
    $tab_Colaboradores = $("#Colaboradores-tab");

    $lblTDocumento = $("#lblTDocumento");
    $txtnuDocumento = $("#txtnuDocumento");
    $lblnuDocumento = $("#lblnuDocumento");
    $cboTPersonaNJ = $("#cboTPersonaNJ");
    $txtRSocial = $("#txtRSocial");
    $txtAPaterno = $("#txtAPaterno");
    $txtAMaterno = $("#txtAMaterno");
    $txtACasada = $("#txtACasada");
    $txtPNombre = $("#txtPNombre");
    $txtSNombre = $("#txtSNombre");
    $cboNacionalidad = $("#cboNacionalidad");
    $cboPDepartamento = $("#cboPDepartamento");
    $cboPProvincia = $("#cboPProvincia");
    $cboPDistrito = $("#cboPDistrito");
    $cboECivil = $("#cboECivil");
    $cboSexo = $("#cboSexo");
    $txtFNacimiento = $("#txtFNacimiento");
    $txtFFallecimiento = $("#txtFFallecimiento");
    $txtUCreacion = $("#txtUCreacion");
    $hfCodPersona = $("#hfCodPersona");
    $btnBuscarSUSALUD = $("#btnBuscarSUSALUD");
    $btnConsultaSusalud = $("#btnConsultaSusalud");
    $cnt_pNatural = $(".pNatural");
    $cnt_pNaturalAgregando = $(".pNaturalAgregando");
    $cnt_pJuridica = $(".pJuridica");
    $cboPJTDocumento = $("#cboPJTDocumento");
    $cboPNTDocumento = $("#cboPNTDocumento");
    $txtPJNDocumento = $("#txtPJNDocumento");
    $txtPNDocumento = $("#txtPNDocumento");
    $cboPJTSociedad = $("#cboPJTSociedad");
    $cboPJTEmpresa = $("#cboPJTEmpresa");
    $txtPJFNacimiento = $("#txtPJFNacimiento");
    $rdpPersList = $('#rdpPersList');

    $cboSSLDTDocumento = $('#cboSSLD-TDocumento');
    $txtSSLDNDocumento = $('#txtSSLD-NDocumento');
    $dtPersonaSusalud = $("#dtPersonaSusalud");
    $mdlSusalud = $("#mdlSusalud");

    $PaisNac = $("#PaisNac");
    $chkPaisNac = $PaisNac.find("input[type=checkbox]");
    $cboPaisNac = $("#cboPaisNac");

    //#endregion 
    //#region ------------DOCUMENTO ---------------------
    $btnNuevoDocumento = $("#btnNuevoDocumento");
    $hfCodPersona = $("#hfCodPersona");
    $hfCodDocumento = $("#hfCodDocumento");
    $tblDocumento = $("#tabla_documento");
    $mdlDocumento = $("#mdlDocumento");
    $modalTituloDocum = $mdlDocumento.find(".modal-title");

    $FormularioDocumento = $("#FormularioDocumento");
    $btnGrabarDocumento = $("#btnGrabarDocumento");
    $cboDocumento = $("#cboDocumento");
    $txtNDocumento = $("#txtNDocumento");
    $txtFInscripcion = $("#txtFInscripcion");
    $txtFVncmt = $("#txtFVncmt");
    $cboPrincipalDocumento = $("#cboPrincipalDocumento");
    //$cboEstado = $(".select-estados");
    $txtFecha = $("#txtFecha");
    $txtUCreacion = $("#txtUCreacion");
    $txtFCreacion = $("#txtFCreacion");
    $txtUEdicion = $("#txtUEdicion");
    $txtFCreacion = $("#txtFCreacion");
    $hfaction = $("#hfaction");
    //#endregion ------------DOCUMENTO ---------------------
    //#region ----------- CORREO----------------------
    $btnNuevoCorreo = $("#btnNuevoCorreo");
    //$hfCodPersona = $("#hfCodPersona");
    $hfCodCorreo = $("#hfCodCorreo");
    $tblCorreo = $("#tabla_correo");
    $mdlCorreo = $("#mdlCorreo");
    $modalTituloCorreo = $mdlCorreo.find(".modal-title");
    $FormularioCorreo = $("#FormularioCorreo");
    $btnGrabarCorreo = $("#btnGrabarCorreo");
    $cboTCorreo = $("#cboTCorreo");
    $txtCorreo = $("#txtCorreo");
    $cboPrincipalCorreo = $("#cboPrincipalCorreo");
    $cboEstado = $(".select-estados");
    $txtFEstado = $("#txtFEstado");
    $txtUCreacion = $("#txtUCreacion");
    $txtFCreacion = $("#txtFCreacion");
    $txtUEdicion = $("#txtUEdicion");
    $txtFCreacion = $("#txtFCreacion");
    $txtFEdicion = $("#txtFEdicion");
    $hfactionCorreo = $("#hfactionCorreo");
    //#endregion ----------- CORREO----------------------
    //#region ---------Direccion--------------------
    $btnNuevoDireccion = $("#btnNuevoDireccion");
    //$hfCodPersona = $("#hfCodPersona");
    $hfCodDireccion = $("#hfCodDireccion");
    $tblDireccion = $("#tabla_direccion");
    $mdlDireccion = $("#mdlDireccion");
    $modalTituloDireccion = $mdlDireccion.find(".modal-title");

    $FormularioDireccion = $("#FormularioDireccion");
    $btnGrabarDireccion = $("#btnGrabarDireccion");
    $cboTDireccion = $("#cboTDireccion");
    $cboTVia = $("#cboTVia");
    $txtVia = $("#txtVia");
    $cboTipoZona = $("#cboTipoZona");
    $txtNroZona = $("#txtNroZona");
    $txtNroVia = $("#txtNroVia");
    //$txtVia = $("#txtVia");
    $txtInterior = $("#txtInterior");
    $txtReferencia = $("#txtReferencia");
    $txtFPrincipal = $("#txtFPrincipal");
    $cboPrincipalDireccion = $("#cboPrincipalDireccion");
    $hfactionDireccion = $("#hfactionDireccion");
    $cboDepartamentoDirec = $("#cboDepartamentoDirec");
    $cboProvinciaDirec = $("#cboProvinciaDirec");
    $cboDistritoDirec = $("#cboDistritoDirec");

    //#endregion ---------Direccion--------------------
    //#region ------telefono----------------
    $btnNuevoTelefono = $("#btnNuevoTelefono");
    $hfCodPersona = $("#hfCodPersona");
    $hfCodTelefono = $("#hfCodTelefono");
    $tbltelefono = $("#tabla_telefono");
    $mdltelefono = $("#mdlTelefono");
    $modalTituloTelefono = $mdltelefono.find(".modal-title");
    $cboPrincipalTelefono = $("#cboPrincipalTelefono");
    $FormularioTelefono = $("#FormularioTelefono")
    $btnGrabarTelefono = $("#btnGrabarTelefono");
    $cboTTelefono = $("#cboTTelefono");
    $cboDepartamentoTlfno = $("#cboDepartamentoTlfno");
    $txtTelefono = $("#txtTelefono");
    $txtObservacion = $("#txtObservacion");
    $hfactionTelefono = $("#hfactionTelefono");
    //#endregion ------telefono----------------
    //#region --------------- Archivo ------------------
    $btnNuevoArchivo = $('#btnNuevoArchivo');
    $dtArchivo = $('#dtArchivo');
    $mdlArchivo = $('#mdlArchivo');
    $mdlArchivoLabel = $('#mdlArchivoLabel');
    $frmArchivo = $("#frmArchivo");
    //$txtIDArchivo = $('#txtIDArchivo');
    $btnGuardarArchivo = $("#btnGuardarArchivo");
    //#endregion --------------- Archivo ------------------
    //#region Homonimos
    $mdlHomonimos = $("#mdlHomonimos");
    $dtHomonimo = $("#dtHomonimo");
    //#endregion
    //#region sunat
    $btnConsultaSunat = $("#btnConsultaSunat");
    $cboTDocumentoSUNAT = $("#cboSUNAT-TDocumento");
    $txtNDocumentoSUNAT = $("#txtSUNAT-NDocumento");
    $mdlSunat = $("#mdlSunat");
    $dtPersonaJuridica = $("#dtPersonaJuridica");
    $btnBuscarSunat = $("#btnBuscarSunat");
    //#endregion
    //#region Colaboradores 
    $dtColaboradores = $('#dtColaboradores');
    //#endregion

    //#region --------entidades--------

    var EntidadPersona = {
        gdtprsna: "",
        rscl: "",
        aptrno: "",
        amtrno: "",
        acsda: "",
        pnmbre: "",
        snmbre: "",
        ncnldd: "",
        cpncmnto: "",
        gdecvl: "",
        gdsxo: "",
        fncmnto: "",
        ffllcmnto: "",
        cubgeo: "",
        gdestdo: "",
        ucrcn: "",
        uedcn: "",
        cboTDcmnto: "",
        txtnDcmnto: "",
        idprsna: "",
        fprncpl: "",
        finscrpcn: "",
        fvncmnto: "",
        gddcmnto: "",
        ndcmnto: "",
    }

    var EntidadDocumento = {
        id: "",
        gddcmnto: "",
        ndcmnto: "",
        finscrpcn: "",
        fvncmnto: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
        festdo: "",
    }

    var EntidadCorreo = {
        id: "",
        gdtcrreo: "",
        ccrreo: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
    }

    var EntidadDireccion = {
        id: "",
        gdtdrccn: "",
        gdtvia: "",
        via: "",
        nvia: "",
        nintrr: "",
        gdtdzna: "",
        zna: "",
        rfrncia: "",
        fprncpl: "",
        cubgeo: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: ""
    }

    var EntidadTelefono = {
        gdttlfno: "",
        cubgeo: "",
        ntlfno: "",
        obsrvcn: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
    }

    var EntidadSusalud = {
        afiliaciones: "",
        apCasada: "",
        apMaterno: "",
        apPaterno: "",
        coContinente: "",
        coDepartamento: "",
        coDistrito: "",
        coError: "",
        coPais: "",
        coPaisEmisor: "",
        coProvincia: "",
        deContinente: "",
        deDepartamento: "",
        deDistrito: "",
        dePais: "",
        deProvincia: "",
        deSexo: "",
        deUbigeo: "",
        feFallecimiento: "",
        feNacimiento: "",
        inFallecimiento: "",
        noPersona: "",
        nuDocumento: "",
        tiDocumento: ""
    }

    var DatosPersona = {
        DatosNatural: function () {
            EntidadPersona.aptrno = $txtAPaterno.val();
            EntidadPersona.amtrno = $txtAMaterno.val();
            EntidadPersona.acsda = $txtACasada.val();
            EntidadPersona.pnmbre = $txtPNombre.val();
            EntidadPersona.snmbre = $txtSNombre.val();
            EntidadPersona.ncnldd = $cboNacionalidad.val();
            EntidadPersona.cpncmnto = $chkPaisNac.is(':checked') == true ? $cboPaisNac.val() : $cboNacionalidad.val();
            EntidadPersona.gdecvl = $cboECivil.val();
            EntidadPersona.gdsxo = $cboSexo.val();
            EntidadPersona.fncmnto = $txtFNacimiento.val();
            EntidadPersona.ffllcmnto = $txtFFallecimiento.val();
            EntidadPersona.cubgeo = $cboPDistrito.val();
        },
        DatosJuridica: function () {
            EntidadPersona.rscl = $txtRSocial.val();
            EntidadPersona.fncmnto = $txtPJFNacimiento.val();
            EntidadPersona.gdtescdd = $cboPJTSociedad.val();
            EntidadPersona.gdtetmno = $cboPJTEmpresa.val();
            EntidadPersona.cubgeo = "150101";
        }
    }

    var EntidadArchivo = {
        id: '',
        idprsna: '',
        dscrpcn: '',
        nmbre: '',
    }

    var EntidadSunat = {
        "resultado": "",
        "nombre_razon_social": "",
        "direccion": "",
        "estado_contribuyente": "",
        "condicion_domicilio": "",
        "ubigeo": "",
        "tipo_via": "",
        "nombre_via": "",
        "codigo_zona": "",
        "tipo_zona": "",
        "numero": "",
        "interior": "",
        "lote": "",
        "departamento": "",
        "manzana": "",
        "kilometro": ""
    }

    //#endregion --------entidades--------

    //#region ------------CONTROLES ---------------------
    var $accesoAgregarPersona = $("#accesoAgregarPersona");
    var $accesoEditarPersona = $("#accesoEditarPersona");
    var $accesoEliminarPersona = $("#accesoEliminarPersona");

    var $accesoAgregarDocumento = $("#accesoAgregarDocumento");
    var $accesoEditarDocumento = $("#accesoEditarDocumento");
    var $accesoEliminarDocumento = $("#accesoEliminarDocumento");

    var $accesoAgregarDireccion = $("#accesoAgregarDireccion");
    var $accesoEditarDireccion = $("#accesoEditarDireccion");
    var $accesoEliminarDireccion = $("#accesoEliminarDireccion");

    var $accesoAgregarTelefono = $("#accesoAgregarTelefono");
    var $accesoEditarTelefono = $("#accesoEditarTelefono");
    var $accesoEliminarTelefono = $("#accesoEliminarTelefono");

    var $accesoAgregarCorreo = $("#accesoAgregarCorreo");
    var $accesoEditarCorreo = $("#accesoEditarCorreo");
    var $accesoEliminarCorreo = $("#accesoEliminarCorreo");

    var $accesoAgregarArchivo = $("#accesoAgregarArchivo");
    var $accesoEliminarArchivo = $("#accesoEliminarArchivo");

    //#endregion ------------CONTROLES ---------------------

    var validacionControles = {
        init: function () {
            if ($accesoAgregarPersona.val() == "False") {
                $btnNuevoPersona.hide();
            }
            if ($accesoAgregarDocumento.val() == "False") {
                $btnNuevoDocumento.hide();
            }
            if ($accesoAgregarDireccion.val() == "False") {
                $btnNuevoDireccion.hide();
            }
            if ($accesoAgregarTelefono.val() == "False") {
                $btnNuevoTelefono.hide();
            }
            if ($accesoAgregarCorreo.val() == "False") {
                $btnNuevoCorreo.hide();
            }
            if ($accesoAgregarArchivo.val() == "False") {
                $btnNuevoArchivo.hide();
            }
        }
    };

    var configDTSusalud = {
        objeto: null,
        opciones: {
            bAutoWidth: false,
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/lista-susalud",
                timeout: 60000,
                type: "GET",
                data: function (data) {
                    //delete data.columns;
                    data.tiDocumento = $cboSSLDTDocumento.val();
                    data.nuDocumento = $txtSSLDNDocumento.val();
                },
            },
            paging: false,
            ordering: false,
            columns: [
                {
                    data: null,
                    title: "Nombres y Apellidos",
                    class: "d-none",
                    render: function (data) {
                        tpm = data.noPersona + ' ' + data.apPaterno + ' ' + data.apMaterno;
                        return tpm;
                    }
                },
                {
                    data: "apPaterno",
                    title: "Apellido Paterno",
                    class: "text-center"
                },
                {
                    data: "apMaterno",
                    title: "Apellido Materno",
                    class: "text-center"
                },
                {
                    data: "noPersona",
                    title: "Nombres",
                    class: "text-center"
                }, {
                    data: "feNacimiento",
                    title: "Fecha Nacimiento",
                    class: "text-center"
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        eventos: {
            selectRow: function () {
                $dtPersonaSusalud.on('click', 'tbody tr', function () {
                    var data = configDTSusalud.objeto.row(this).data();
                    EntidadSusalud = new Object();
                    EntidadSusalud = data;
                    if (typeof (EntidadSusalud) != 'undefined') {
                        funcionesPersona.obtenerPersonaSusalud(EntidadSusalud);
                    }
                })
            },
            init: function () {
                configDTSusalud.eventos.selectRow();
            }
        },
        reload: function () {
            if (configDTSusalud.objeto == null || configDTSusalud.objeto == "" || configDTSusalud.objeto == undefined) {
                configDTSusalud.objeto = $dtPersonaSusalud.DataTable(configDTSusalud.opciones);
                configDTSusalud.eventos.init();
            }
            else {
                configDTSusalud.objeto.ajax.reload();
            }
        }
    }       //falta

    var configDTSunat = {
        objeto: null,
        opciones: {
            bAutoWidth: false,
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/lista-sunat",
                type: "GET",
                data: function (data) {
                    //delete data.columns;
                    //data.tiDocumento = $cboSSLDTDocumento.val();
                    data.nuDocumento = $txtNDocumentoSUNAT.val();
                },
            },
            paging: false,
            ordering: false,
            columns: [
                {
                    data: "nombre_razon_social",
                    title: "Razón  Social",
                    class: "text-center"
                },
                {
                    data: "estado_contribuyente",
                    title: "Estado Contribuyente",
                    class: "text-center"
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        eventos: {
            selectRow: function () {
                $dtPersonaJuridica.on('click', 'tbody tr', function () {
                    var data = configDTSunat.objeto.row(this).data();
                    EntidadSusalud = new Object();
                    EntidadSusalud = data;
                    if (typeof (EntidadSunat) != 'undefined') {
                        funcionesPersona.obtenerPersonaSunat(EntidadSunat);
                    }
                })
            },
            init: function () {
                configDTSunat.eventos.selectRow();
            }
        },
        reload: function () {
            if (configDTSunat.objeto == null || configDTSunat.objeto == "" || configDTSunat.objeto == undefined) {
                configDTSunat.objeto = $dtPersonaJuridica.DataTable(configDTSunat.opciones);
                configDTSunat.eventos.init();
            }
            else {
                configDTSunat.objeto.ajax.reload();
            }
        }
    }         //falta

    var configDTPersona = {
        objecto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/listar-persona",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.p_sDatos = $txtNombreRazon.val();
                    data.p_sDesde = $txtDesde.val();
                    data.p_sHasta = $txtHasta.val();
                    data.p_sTPersona = $cboTPersona_NJ.val();
                    data.p_sTDocumento = $cboTipoDocumento.val();
                    data.p_sNDocumento = $txtDocumento.val();
                    data.p_sEstado = $cboEstadoListado.val();
                    data.p_bContratista = false;
                }
            },
            columns: [
                {
                    title: "Tipo Persona",
                    data: "tprsna",
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto", width: '15%',
                    orderable: false
                },
                {
                    title: "Nombres y apellidos /Razón",
                    data: "datos", width: '39%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "F. Edición",
                    data: "fedcn", width: '15%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null, width: '5%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null, width: '15%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarPersona.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }

                        if ($accesoEliminarPersona.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTPersona.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesPersona.eliminarPersona(id)
                })
            },
            editar: function () {
                configDTPersona.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $hfCodPersona.val(id);
                    //$tab_DatosGenerales.click();
                    funcionesPersona.obtenerPersona(id);
                })
            },
            init: function () {
                configDTPersona.eventos.eliminar();
                configDTPersona.eventos.editar();
            }

        },
        reload: function () {
            configDTPersona.objecto.ajax.reload();
        },
        init: function () {
            configDTPersona.objecto = $tabla_persona.DataTable(configDTPersona.opciones);
            configDTPersona.eventos.init();
        }
    };

    var configDTDocumento = {
        objecto: null,
        tiposDocumentos: [],
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Documento/listarDocumento",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "N° Documento",
                    data: "ndcmnto",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "F. Inscripción",
                    data: "finscrpcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "Fec. Vencimiento",
                    data: "fvncmnto",
                    width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTDocumento.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        configDTDocumento.tiposDocumentos.push(data.igddcmnto);

                        var tpm = "";
                        if ($accesoEditarDocumento.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlDocumento" data-doc="${data.igddcmnto}" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarDocumento.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTDocumento.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesDocumento.eliminarDocumento(id);
                })
            },
            editar: function () {
                configDTDocumento.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    var doc = $(this).data("doc");
                    $modalTituloDocum.text("Editar Documento");
                    $hfaction.val("E");
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGDFiltrado("GDDCMNTO", configDTDocumento.tiposDocumentos, doc);
                    funcionesDocumento.obtenerDocumento(id);
                })
            },
            init: function () {
                configDTDocumento.eventos.eliminar();
                configDTDocumento.eventos.editar();
            }
        },
        reload: function () {
            configDTDocumento.tiposDocumentos = [];
            if (configDTDocumento.objecto == null || configDTDocumento.objecto == "" || configDTDocumento == undefined) {
                configDTDocumento.objecto = $tblDocumento.DataTable(configDTDocumento.opciones);
                configDTDocumento.eventos.init();
            }
            else {
                configDTDocumento.objecto.ajax.reload();
            }
        },
    };

    var configDTCorreo = {
        objecto: null,
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Correo/listarCorreo",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "Tipo Correo",
                    data: "gdtcrreo",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Correo",
                    data: "ccrreo",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTCorreo.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarCorreo.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlCorreo" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarCorreo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        } return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTCorreo.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesCorreo.eliminarCorreo(id);
                })
            },
            editar: function () {
                configDTCorreo.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTituloCorreo.text("Editar Correo");
                    $hfactionCorreo.val("E");
                    funcionesCorreo.obtenerCorreo(id);
                })
            },
            init: function () {
                configDTCorreo.eventos.eliminar();
                configDTCorreo.eventos.editar();
            }
        },
        reload: function () {
            if (configDTCorreo.objecto == null || configDTCorreo.objecto == "" || configDTCorreo.objecto == undefined) {
                configDTCorreo.objecto = $tblCorreo.DataTable(configDTCorreo.opciones);
                configDTCorreo.eventos.init();

            } else { configDTCorreo.objecto.ajax.reload(); }
        },
    };

    var configDTDireccion = {
        objecto: null,
        tienePrincipal: false,
        tipoDirecciones: [],
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Direccion/listarDireccion",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "Tipo Dirección",
                    data: "gdtdrccn",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTDireccion.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Dirección",
                    data: null,
                    orderable: false,
                    width: '20%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<div class="row text-left">
                                        <div class="col-12 text-break">${data.gdtvia + " " + (data.via ? data.via + " " : "") + (data.nvia ? "# " + data.nvia + " - " : "") + (data.gdtdzna ? data.gdtdzna + " " : "") + (data.zna ? data.zna + " - " : "") + (data.nintrr ? "INTERIOR " + data.nintrr + " - " : "") + data.rfrncia}</div>
                                        <div class="col-12 font-weight-bold">${data.cubgeo}</div>
                                    </div>`;
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '90px',
                    class: "text-center",
                    render: function (data) {
                        configDTDireccion.tipoDirecciones.push(data.igdtdrccn);

                        var tpm = "";
                        if ($accesoEditarDireccion.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlDireccion" data-dir="${data.igdtdrccn}" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarDireccion.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTDireccion.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesDireccion.eliminarDireccion(id);
                })
            },
            editar: function () {
                configDTDireccion.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    var dir = $(this).data("dir");
                    $modalTituloDireccion.text("Editar Dirección");
                    $hfactionDireccion.val("E");
                    $cboTDireccion.empty();
                    $cboTDireccion.LlenarSelectGDFiltrado("GDTDRCCN", configDTDireccion.tipoDirecciones, dir);
                    if ($cboTPersonaNJ.val() == 1) {
                        $cboTDireccion.children('option[value="3"]').hide();
                    } else {
                        $cboTDireccion.children('option[value="3"]').show();
                        $cboTDireccion.val("3").trigger("change")
                    }
                    funcionesDireccion.obtenerDireccion(id);
                })
            },
            init: function () {
                configDTDireccion.eventos.eliminar();
                configDTDireccion.eventos.editar();
            }
        },
        reload: function () {
            configDTDireccion.tipoDirecciones = [];
            if (configDTDireccion.objecto == null || configDTDireccion.objecto == "" || configDTDireccion.objecto == undefined) {
                configDTDireccion.objecto = $tblDireccion.DataTable(configDTDireccion.opciones);
                configDTDireccion.eventos.init()
            } else { configDTDireccion.objecto.ajax.reload(); }
        },

    };

    var configDTTelefono = {
        objecto: null,
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Telefono/listarTelefono",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "Tipo Telefono",
                    data: "gdttlfno",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Departamento",
                    data: "cubgeo",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Teléfono",
                    data: "ntlfno",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Observación",
                    data: "obsrvcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTTelefono.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "F. Estado",
                    data: "festdo", width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarTelefono.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlTelefono" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarTelefono.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTTelefono.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesTelefono.eliminarTelefono(id);
                })
            },
            editar: function () {
                configDTTelefono.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTituloTelefono.text("Editar Telefono");
                    $hfactionTelefono.val("E");
                    funcionesTelefono.obtenerTelefono(id);
                })
            },
            init: function () {
                configDTTelefono.eventos.eliminar();
                configDTTelefono.eventos.editar();
            }
        },
        reload: function () {
            if (configDTTelefono.objecto == null || configDTTelefono.objecto == "" || configDTTelefono.objecto == undefined) {
                configDTTelefono.objecto = $tbltelefono.DataTable(configDTTelefono.opciones);
                configDTTelefono.eventos.init();
            } else { configDTTelefono.objecto.ajax.reload(); }
        },

    };

    var configDtArchivo = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/archivo/listar",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    //data.id = "";
                }
            },
            columns: [
                {
                    title: "Nombre",
                    data: "nmbre",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Descripción",
                    data: "dscrpcn",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "U. Creación",
                    data: "ucrcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Creación",
                    data: "cFCRCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEliminarArchivo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-trash"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDtArchivo.objeto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesArchivo.eliminar(id);
                })
            },
            init: function () {
                configDtArchivo.eventos.eliminar();
            }
        },
        reload: function () {
            if (configDtArchivo.objeto == null || configDtArchivo.objeto == "" || configDtArchivo.objeto == undefined) {
                configDtArchivo.objeto = $dtArchivo.DataTable(configDtArchivo.opciones);
                configDtArchivo.eventos.init();
            } else { configDtArchivo.objeto.ajax.reload(); }
        }
    }

    var configDtColaboradores = {
        objeto: null,
        opciones: {
            filter: true,
            ajax: {
                dataType: "JSON",
                url: "/persona/listar-colaboradores",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    //data.id = "";
                }
            },
            columns: [
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "Documento",
                    data: "ndcmnto",
                    width: '70%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Nombres y Apellidos",
                    data: "datos",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "U. Edición",
                    data: "uedcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        reload: function () {
            if (configDtColaboradores.objeto == null || configDtColaboradores.objeto == "" || configDtColaboradores.objeto == undefined) {
                configDtColaboradores.objeto = $dtColaboradores.DataTable(configDtColaboradores.opciones);
                /*                configDtColaboradores.eventos.init();*/
            } else {
                configDtColaboradores.objeto.ajax.reload();
            }
        }
    }

    var configDtHomonimos = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/lista-homonimos",
                type: "POST",
                data: function (data) {
                    delete data.columns;
                    data.entidad = {
                        amtrno: $txtAMaterno.val().trim(),
                        aptrno: $txtAPaterno.val().trim(),
                        pnmbre: $txtPNombre.val().trim(),
                        snmbre: $txtSNombre.val().trim(),
                    }
                }
            },
            columns: [
                {
                    title: "ID",
                    data: "id",
                    width: '5%',
                    orderable: true,
                    className: "d-none"
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '5%',
                    orderable: true,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto",
                    width: '5%',
                    orderable: true,
                    className: "text-center"
                },
                {
                    title: "Nombres y Apellidos",
                    data: "datos",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center d-none",
                    render: function (data) {
                        var tpm = "";

                        tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlArchivo" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;

                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                        //tpm += `<button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            selectRow: function () {
                $dtHomonimo.on('click', 'tbody tr', function () {
                    var data = configDtHomonimos.objeto.row(this).data();
                    EntidadSusalud = new Object();
                    EntidadSusalud = data;
                    if (data.id > 0) {
                        Swal.fire({
                            title: "¿Quiere cargar los datos del registro?",
                            icon: "info",
                            showCancelButton: true,
                            confirmButtonText: "Si, cargar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $hfCodPersona.val(data.id);
                                funcionesPersona.obtenerPersona(data.id);
                                $mdlHomonimos.modal('hide');

                            }
                        });
                    }
                })
            },
            count: function () {
                configDtHomonimos.objeto.on('draw', function () {
                    var info = configDtHomonimos.objeto.page.info();
                    if (info.recordsTotal > 0) {
                        $mdlHomonimos.modal('show')
                    }
                })
            },
            init: function () {
                configDtHomonimos.eventos.selectRow();
                configDtHomonimos.eventos.count();
            }
        },
        reload: function () {
            if (configDtHomonimos.objeto == null || configDtHomonimos.objeto == "" || configDtHomonimos.objeto == undefined) {
                configDtHomonimos.objeto = $dtHomonimo.DataTable(configDtHomonimos.opciones);
                configDtHomonimos.eventos.init();
            } else { configDtHomonimos.objeto.ajax.reload(); }
        }
    }      //falta

    var configModalDocumento = {
        form: {
            objeto: $FormularioDocumento.validate({
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    cboDocumento: {
                        required: true
                    },
                    txtNDocumento: {
                        required: true
                    },
                    txtFInscripcion: {
                        required: false
                    },
                    txtFVncmt: {
                        required: false
                    },
                    cboPrincipalDocumento: {
                        required: true
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalDocumento.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlDocumento.on("hidden.bs.modal", function () {
                    configModalDocumento.eventos.reset();
                    $FormularioDocumento.find(".msg").text("");
                })
            },
            onShow: function () {
                $mdlDocumento.on("shown.bs.modal", function () {
                    if ($hfaction.val() == "N") {
                        $FormularioDocumento.AgregarCamposDefectoAuditoria();
                        $FormularioDocumento.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalDocumento.form.eventos.reset();
                $FormularioDocumento.trigger("reset");
                $FormularioDocumento.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalCorreo = {
        form: {
            objeto: $FormularioCorreo.validate({
                rules: {
                    cboTCorreo: {
                        required: true,
                    },
                    txtCorreo: {
                        required: true,
                    },
                    cboPrincipalCorreo: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalCorreo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlCorreo.on("hidden.bs.modal", function () {
                    configModalCorreo.eventos.reset();
                })
            },
            onShow: function () {
                $mdlCorreo.on("shown.bs.modal", function () {
                    if ($hfactionCorreo.val() == "N") {
                        $FormularioCorreo.AgregarCamposDefectoAuditoria();
                        $FormularioCorreo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalCorreo.form.eventos.reset();
                $FormularioCorreo.trigger("reset");
                $FormularioCorreo.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalDireccion = {
        form: {
            objeto: $FormularioDireccion.validate({
                rules: {
                    cboTDireccion: {
                        required: true,
                    },
                    cboTVia: {
                        required: true,
                    },
                    txtNroVia: {
                        required: true,
                    },
                    cboTipoZona: {
                        required: false,
                    },
                    cboPrincipalDireccion: {
                        required: true,
                    },
                    Zona: {
                        required: false,
                    },
                    cboDepartamentoDirec: {
                        required: true,
                    },
                    cboProvinciaDirec: {
                        required: true,
                    },
                    cubgeo: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalDireccion.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlDireccion.on("hidden.bs.modal", function () {
                    configModalDireccion.eventos.reset();
                })
            },
            onShow: function () {
                $mdlDireccion.on("shown.bs.modal", function () {
                    if ($hfactionDireccion.val() == "N") {
                        $FormularioDireccion.AgregarCamposDefectoAuditoria();
                        $FormularioDireccion.DeshabilitarCamposAuditoria();
                        $cboProvinciaDirec.val('').text('Seleccione').attr('disabled', true);
                        $cboDistritoDirec.val('').text('Seleccione').attr('disabled', true);
                    }
                })
            },
            reset: function () {
                configModalDireccion.form.eventos.reset();
                $FormularioDireccion.trigger("reset");
                $FormularioDireccion.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalTelefono = {
        form: {
            objeto: $FormularioTelefono.validate({
                rules: {
                    cboTTelefono: {
                        required: true,
                    },
                    txtTelefono: {
                        required: true,
                    },
                    txtNroVia: {
                        required: true,
                    },
                    cboPrincipalTelefono: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {

                    configModalTelefono.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdltelefono.on("hidden.bs.modal", function () {
                    configModalTelefono.eventos.reset();
                })
            },
            onShow: function () {
                $mdltelefono.on("shown.bs.modal", function () {
                    if ($hfactionTelefono.val() == "N") {
                        $FormularioTelefono.AgregarCamposDefectoAuditoria();
                        $FormularioTelefono.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalTelefono.form.eventos.reset();
                $FormularioTelefono.trigger("reset");
                $FormularioTelefono.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configFormPersona = {
        objeto: $FormularioPersona.validate({
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                /*cboTPersonaNJ: {
                    required: true,
                },*/
                // txtRSocial: {
                    // required: true,
                // },
                // txtAPaterno: {
                    // required: true,
                    // maxlength: 22
                // },
                // txtAMaterno: {
                    // required: false,
                // },
                // txtPNombre: {
                    // required: true,
                // },
                // txtSNombre: {
                    // required: false,
                // },
                // cboNacionalidad: {
                    // required: true,
                // },
                // cboECivil: {
                    // required: true,
                // },
                // cboSexo: {
                    // required: true,
                // },
                // txtFNacimiento: {
                    // required: true,
                // },
                // cboPDepartamento: {
                    // required: true,
                // },
                // cboPProvincia: {
                    // required: true,
                // },
                // cboDistrito: {
                    // required: true,
                // },
                /*cboTDocumento: {
                    required: true,
                },*/
                // cboPJTDocumento: {
                    // required: true,
                // },
                // txtPJNDocumento: {
                    // required: true,
                    // minlength: 11
                // },
                // cboPJTSociedad: {
                    // required: true,
                // },
                // cboPJTEmpresa: {
                    // required: false,
                // },
                // txtPJFNacimiento: {
                    // required: false,
                // },
            }
        }),
        homonimiaPN: {
            options: {
                class: 'homo',
                elemcontent: $FormularioPersona
            },
            init: function () {
                eventosIncrustadosPersona.Homonimia(configFormPersona.homonimiaPN.options);
            }
        },
        eventos: {
            reset: function () {
                configFormPersona.objeto.resetForm();
            }
        },
    }

    var configModalArchivo = {
        form: {
            objeto: $frmArchivo.validate({
                rules: {

                }
            }),
            eventos: {
                reset: function () {
                    configModalArchivo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlArchivo.on("hidden.bs.modal", function () {
                    configModalArchivo.eventos.reset();
                })
            },
            onShow: function () {
                $mdlArchivo.on("shown.bs.modal", function () {
                    if ($hfaction.val() == "N") {
                        $frmArchivo.AgregarCamposDefectoAuditoria();
                        $frmArchivo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                //configModalArchivo.form.eventos.reset();
                $frmArchivo.trigger("reset");
                $frmArchivo.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configDatePickers = {
        opcStandar: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        opcRangoDesdeHasta: {
            inputs: $('.fRangoPers'),
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        init: function () {

            //DP LISTAR PERSONA
            $rdpPersList.datepicker(configDatePickers.opcRangoDesdeHasta);

            //DP PERSONA
            $txtFNacimiento.datepicker(configDatePickers.opcStandar);
            $txtFFallecimiento.datepicker(configDatePickers.opcStandar);
            $txtPJFNacimiento.datepicker(configDatePickers.opcStandar);

            //DP DOCUMENTO
            $txtFInscripcion.datepicker(configDatePickers.opcStandar);
            $txtFVncmt.datepicker(configDatePickers.opcStandar);
        }
    }

    var configCboTipoDocumentos = {
        optDocumento: {
            obj: $txtNDocumento[0].id,
        },
        optDocSusalud: {
            obj: $txtSSLDNDocumento[0].id,
        },
        optDocPj: {
            obj: $txtPJNDocumento[0].id,
        },
        optDocLstPersona: {
            obj: $txtDocumento[0].id,
        },
        init: function () {
            $cboDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocumento);
            $cboSSLDTDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocSusalud);
            $cboPJTDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocPj);
            $cboTipoDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocLstPersona);
        }
    }

    var configUbigeos = {
        f_nacionalidad: function (obj) {
            var Pais = $(obj).val();
            var chkPais = $chkPaisNac;
            var optUbigeos;
            $cboPDistrito.children('option[value=""]').text("Seleccione").val("");
            $cboPDistrito.prop("disabled", false);
            if (chkPais.is(":checked")) {
                optUbigeos = configUbigeos.ubigeoPersona.optPaisNacimiento;
            }
            else {
                $PaisNac.addClass("d-none");
                $cboPaisNac.val(Pais);
                optUbigeos = configUbigeos.ubigeoPersona.optionNacionalidad;
            }
            if (Pais != 51) {
                $cboPDistrito.parent('div').find('span').text('Estado');
                $cboPDepartamento.parent('div').addClass('d-none');
                $cboPProvincia.parent('div').addClass('d-none');
                optUbigeos.codPais = Pais;
                optUbigeos.codProvincia = '';
                LlenarSelectDistrito(optUbigeos);
            }
            else {
                if (!chkPais.is(":checked")) {
                    $cboPaisNac.attr('disabled', true);
                }
                $PaisNac.removeClass('d-none');
                $cboPDistrito.parent('div').find('span').text('Distrito');
                $cboPDepartamento.parent('div').removeClass('d-none');
                $cboPProvincia.parent('div').removeClass('d-none');

                optUbigeos.codPais = '';
                optUbigeos.refresh = true;
                cargarUbigeo(optUbigeos);//2
            }
        },
        f_inicioUbigeo: function () {
            $PaisNac.addClass("d-none");
            $cboPDistrito.parent('div').find('span').text('Distrito');
            $cboPDistrito.append($("<option />").val('').text("Seleccione"));
            $cboPDistrito.val("");
            $cboPDistrito.attr("disabled", true);
            $cboPDepartamento.parent('div').removeClass('d-none');
            $cboPProvincia.append($("<option />").val('').text("Seleccione"));
            $cboPProvincia.val("");
            $cboPProvincia.attr("disabled", true);
            $cboPProvincia.parent('div').removeClass('d-none');
        },
        ubigeoPersona: {
            optPaisNacimiento: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '',
                controles: {
                    departamento: $cboPDepartamento,
                    provincia: $cboPProvincia,
                    distrito: $cboPDistrito,
                    nacionalidad: $cboPaisNac
                }
            },
            optionNacionalidad: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '',
                controles: {
                    departamento: $cboPDepartamento,
                    provincia: $cboPProvincia,
                    distrito: $cboPDistrito,
                    nacionalidad: $cboNacionalidad
                },
            },
            cargarUbigeosPersona: function (datosUbigeo) {
                var opt = {}
                opt = configUbigeos.ubigeoPersona.optionNacionalidad
                if (datosUbigeo.ncnldd == '51') {
                    opt = null;
                    if (datosUbigeo.cpncmnto == '51') {
                        opt = configUbigeos.ubigeoPersona.optionNacionalidad;
                        opt.codPais = datosUbigeo.ncnldd == '51' ? '169' : datosUbigeo.ncnldd;;
                    } else {
                        opt = configUbigeos.ubigeoPersona.optPaisNacimiento;
                        opt.codPais = datosUbigeo.cpncmnto;
                    }
                } else {
                    opt.codPais = datosUbigeo.cpncmnto == '51' ? '169' : datosUbigeo.cpncmnto;
                }
                opt.codUbigeo = datosUbigeo.cubgeo
                cargarUbigeos(opt);
                //#region funcionalidad mostrar elementos Ubigeo
                if (datosUbigeo.ncnldd == '51') {
                    $PaisNac.removeClass('d-none');
                    if (datosUbigeo.cpncmnto == '51') {
                        $chkPaisNac.prop('checked', false);
                        $cboPaisNac.attr('disabled', true);
                        $cboPDistrito.parent('div').find('span').text('Distrito');
                        $cboPDepartamento.parent('div').removeClass('d-none');
                        $cboPProvincia.parent('div').removeClass('d-none');
                    }
                    else {
                        $chkPaisNac.prop('checked', true);
                        $cboPaisNac.attr('disabled', false);
                        $cboPDistrito.parent('div').find('span').text('Estado');
                        $cboPDepartamento.parent('div').addClass('d-none');
                        $cboPProvincia.parent('div').addClass('d-none');
                    }
                }
                else {
                    $PaisNac.addClass('d-none');
                    $cboPDistrito.parent('div').find('span').text('Estado');
                    $cboPDepartamento.parent('div').addClass('d-none');
                    $cboPProvincia.parent('div').addClass('d-none');
                }
                $cboNacionalidad.attr("disabled", false);
                //#endregion
            },
            init: function () {
                var opt = {};
                opt = configUbigeos.ubigeoPersona.optionNacionalidad;
                opt.codPais = '169';
                opt.refresh = true;
                //cargarUbigeo(opt);///1
                //configUbigeos.f_nacionalidad($cboNacionalidad)
                configUbigeos.f_inicioUbigeo();
            }
        },
        ubigeoDireccion: {
            optUbigeo: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '169',
                controles: {
                    departamento: $cboDepartamentoDirec,
                    provincia: $cboProvinciaDirec,
                    distrito: $cboDistritoDirec,
                    nacionalidad: ''
                }
            },
            cargarUbigeo: function (datosUbigeo) {
                var opt = {};
                opt = configUbigeos.ubigeoDireccion.optUbigeo;
                opt.codUbigeo = datosUbigeo.cubgeo;
                cargarUbigeos(opt);
            },
            init: function () {
                var opt = {};
                opt = configUbigeos.ubigeoDireccion.optUbigeo;
                opt.refresh = true;
                cargarUbigeo(opt);
            }
        },
        init: function () {

        }
    }

    var funcionesPersona = {
        initTipoPersona: function (valor) {
            if (!$hfCodPersona.val()) {
                $FormularioPersona.trigger("reset");
                //$FormularioPersona.AgregarCamposDefectoAuditoria();
                $cboTPersonaNJ.val(valor);
            }
            if (valor == 1) {
                $cnt_pNatural.show();
                $cnt_pJuridica.hide();
                configFormPersona.eventos.reset();
                $tab_Colaboradores.addClass("d-none");
                $btnConsultaSusalud.show();
                $btnConsultaSunat.hide();
                $btnCreateWorker.hide();
            } else {
                $cnt_pNatural.hide();
                $cnt_pJuridica.show();
                configFormPersona.eventos.reset();
                $tab_Colaboradores.removeClass("d-none");
                $btnConsultaSusalud.hide();
                $btnConsultaSunat.hide();
                $cboPJTDocumento.val('8').trigger("change");
                $cboPJTDocumento.attr('disabled', true);
                $btnCreateWorker.hide();
            }
        },
        NuevoPersona: function () {
            $tab_Documento.removeClass("disabled");
            $tab_Direccion.addClass("disabled");
            $tab_CuentaCorreo.addClass("disabled");
            $tab_Telefono.addClass("disabled");
            $tab_Archivos.addClass("disabled");
            $tab_Colaboradores.addClass("disabled");
            $tab_DatosGenerales.removeClass("disabled");
            $FormularioPersona.trigger("reset");
            $FormularioPersona.AgregarCamposDefectoAuditoria();
            $FormularioPersona.DeshabilitarCamposAuditoria();
            $cboTPersonaNJ.attr("disabled", false);
            $txtnuDocumento.show();
            $btnConsultaSusalud.show();
            $btnCreateWorker.hide();
            $lblTDocumento.show();
            $lblnuDocumento.show();
            $tab_DatosGenerales.click();
            $tab_Documento.click();
            $cboTPersonaNJ.val(1);
            funcionesPersona.initTipoPersona($cboTPersonaNJ.val());
            $cnt_pNaturalAgregando.hide();
            configDTDocumento.reload();
            funcionesPersona.validarNuevoBotones();

            configUbigeos.ubigeoPersona.init();
            $cboNacionalidad.val('51').trigger("change");
            $cboPDistrito.attr("disabled", true);
            $cboPProvincia.attr("disabled", true);
        },
        obtenerPersona: function (id) {
            funcionesPersona.obtenerDatosPersona(id);
            $cboTPersonaNJ.attr("disabled", true);
            $tab_Direccion.removeClass("disabled");
            $tab_Telefono.removeClass("disabled");
            $tab_CuentaCorreo.removeClass("disabled");
            $tab_Direccion.removeClass("disabled");
            $tab_Documento.removeClass("disabled");
            $tab_Archivos.removeClass("disabled");
            $tab_Colaboradores.removeClass("disabled");

        },
        guardarPersona: function () {
            if ($FormularioPersona.valid()) {
                if (!$hfCodPersona.val() || $hfCodPersona.val() == 0) {
                    funcionesPersona.insertarPersona();
                    $tab_Direccion.removeClass("disabled");
                    $tab_Telefono.removeClass("disabled");
                    $tab_CuentaCorreo.removeClass("disabled");
                    $tab_Direccion.removeClass("disabled");
                    $tab_Documento.removeClass("disabled");
                    $tab_DatosGenerales.removeClass("disabled");
                    $tab_DatosGenerales.click();
                } else {
                    funcionesPersona.actualizarPersona();
                }
            }

        },
        insertarPersona: function () {//INSERTAR PERSONA
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadPersona = new Object();
                EntidadPersona.gdtprsna = $cboTPersonaNJ.val();
                EntidadPersona.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();

                var fini = "", ffin = "";
                var valorBool = false, flag = false, valorBool2 = false;
                var factual = new Date();
                if ($cboTPersonaNJ.val() == "1") {
                    DatosPersona.DatosNatural();
                    fini = $FormularioPersona.find("[name='txtFNacimiento']").val().split("/");
                } else {
                    DatosPersona.DatosJuridica();
                    fini = $FormularioPersona.find("[name='txtPJFNacimiento']").val().split("/");
                }
                //FECHA INICIAL MAYOR O IGUAL A LA FECHA ACTUAL
                var fecinic = new Date(fini[2], fini[1] - 1, fini[0]);
                valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fecinic, factual);
                if (valorBool2) {
                    $FormularioPersona.find(".msg0").text("La fecha seleccionada es mayor a la fecha actual");
                    if ($cboTPersonaNJ.val() == "1") {
                        $FormularioPersona.find("[name='txtFNacimiento']").addClass('error');
                    } else {
                        $FormularioPersona.find("[name='txtPJFNacimiento']").addClass('error');
                    }
                    flag = true;
                } else {
                    if (!$FormularioPersona.find("[name='txtFFallecimiento']").val()) {
                        valorBool2 = false;
                    } else {
                        ffin = $FormularioPersona.find("[name='txtFFallecimiento']").val().split("/");
                        var fechafin = new Date(ffin[2], ffin[1] - 1, ffin[0]);
                        // FECHA FALLECIMIENTO MAYOR O IGUAL A LA FECHA ACTUAL
                        valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fechafin, factual)
                        if (valorBool2) {
                            $FormularioPersona.find(".msg").text("La fecha de fallecimiento es mayor a la fecha actual");
                            $FormularioPersona.find("[name='txtFFallecimiento']").addClass('error');
                            flag = true;
                        } else {
                            // FECHA FALLECIMIENTO MAYOR O IGUAL A LA FECHA DE NACIMIENT0
                            valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fecinic, fechafin);
                            if (valorBool2) {
                                $FormularioPersona.find(".msg").text("La fecha es menor a la fecha de Nacimiento");
                                $FormularioPersona.find("[name='txtFFallecimiento']").addClass('error');
                                flag = true;
                            } else {
                                $FormularioPersona.find(".msg").text("");
                                $FormularioPersona.find("[name='txtFNacimiento']").removeClass('error');
                            }
                        }
                    }
                    $FormularioPersona.find(".msg0").text("");
                    $FormularioPersona.find("[name='txtFFallecimiento']").removeClass('error');
                }
                if (flag == false && valorBool2 == false) {
                    EntidadPersona.gddcmnto = $cboPJTDocumento.val();
                    EntidadPersona.ndcmnto = $txtPJNDocumento.val();
                    $.post("/Persona/insertarPersonas", EntidadPersona).done(function (idPersona) {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $hfCodPersona.val(idPersona)
                            //if ($cboTPersonaNJ.val() == "2") {
                            //funcionesPersona.insertarDniPersona(idPersona);
                            //}
                            $tab_Documento.removeClass("disabled");
                            $tab_Direccion.removeClass("disabled");
                            $tab_Telefono.removeClass("disabled");
                            $tab_CuentaCorreo.removeClass("disabled");
                            $tab_Archivos.removeClass("disabled");
                            $tab_Colaboradores.removeClass("disabled");
                            configDTPersona.reload();
                            configDTDireccion.reload();
                            configDTCorreo.reload();
                            configDTDocumento.reload();
                            configDTTelefono.reload();
                            funcionesPersona.obtenerPersona($hfCodPersona.val());
                        });
                        $FormularioPersona.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
                }
                $btnGrabarPersona.attr("disabled", false);
            }
        },
        actualizarPersona: function () {
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadPersona = new Object();
                EntidadPersona.id = $hfCodPersona.val();
                EntidadPersona.gdtprsna = $cboTPersonaNJ.val();
                EntidadPersona.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();
                var fini = "", ffin = "";
                var valorBool = false, flag = false, valorBool2 = false;
                var factual = new Date();

                if ($cboTPersonaNJ.val() == "1") {
                    DatosPersona.DatosNatural();
                    fini = $FormularioPersona.find("[name='txtFNacimiento']").val().split("/");
                } else {
                    DatosPersona.DatosJuridica();
                    fini = $FormularioPersona.find("[name='txtPJFNacimiento']").val().split("/");
                }

                var fecinic = new Date(fini[2], fini[1] - 1, fini[0]);
                //FECHA INICIAL MAYOR O IGUAL A LA FECHA ACTUAL
                valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fecinic, factual);
                if (valorBool2) {
                    $FormularioPersona.find(".msg0").text("La fecha seleccionada es mayor a la fecha actual");
                    if ($cboTPersonaNJ.val() == "1") {
                        $FormularioPersona.find("[name='txtFNacimiento']").addClass('error');
                    } else {
                        $FormularioPersona.find("[name='txtPJFNacimiento']").addClass('error');
                    }
                    flag = true;
                } else {
                    if (!$FormularioPersona.find("[name='txtFFallecimiento']").val()) {
                        valorBool2 = false;
                    } else {
                        ffin = $FormularioPersona.find("[name='txtFFallecimiento']").val().split("/");
                        var fechafin = new Date(ffin[2], ffin[1] - 1, ffin[0]);
                        // FECHA FALLECIMIENTO MAYOR O IGUAL A LA FECHA ACTUAL
                        valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fechafin, factual)
                        if (valorBool2) {
                            $FormularioPersona.find(".msg").text("La fecha de fallecimiento es mayor a la fecha actual");
                            $FormularioPersona.find("[name='txtFFallecimiento']").addClass('error');
                            flag = true;
                        } else {
                            // FECHA FALLECIMIENTO MAYOR O IGUAL A LA FECHA DE NACIMIENT0
                            valorBool2 = funcionFechas2.validarFechaMayor(valorBool, fecinic, fechafin);
                            if (valorBool2) {
                                $FormularioPersona.find(".msg").text("La fecha es menor a " + $FormularioPersona.find("[name='txtFNacimiento']").val());
                                $FormularioPersona.find("[name='txtFFallecimiento']").addClass('error');
                                flag = true;
                            } else {
                                $FormularioPersona.find(".msg").text("");
                                if ($cboTPersonaNJ.val() == "1") {
                                    $FormularioPersona.find("[name='txtFNacimiento']").removeClass('error');
                                } else {
                                    $FormularioPersona.find("[name='txtPJFNacimiento']").removeClass('error');
                                }
                            }
                        }
                    }
                    $FormularioPersona.find(".msg0").text("");
                    $FormularioPersona.find("[name='txtFNacimiento']").removeClass('error');
                }
                if (flag == 0 && valorBool2 == false) {
                    $.post("/Persona/actualizarPersona", EntidadPersona).done(function (msj) {
                        Swal.fire({
                            icon: msj ? "info" : "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: msj ? msj : "Se actualizo satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            configDTPersona.reload();
                            configDTDocumento.reload();
                            funcionesPersona.obtenerPersona($hfCodPersona.val());
                        });
                        $FormularioPersona.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
                }
                $btnGrabarPersona.attr("disabled", false);
            }
        },
        eliminarPersona: function (codigo) {
            var parametro = { id: codigo };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Persona/EliminarPersonas", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTPersona.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                }
            });
        },
        validartipodoc: function () {
            if ($cboTDocumento.val() == 8) {
                $txtRSocial.attr("disabled", false);
                $txtAPaterno.attr("disabled", true);
                $txtAMaterno.attr("disabled", true);
                $txtACasada.attr("disabled", true);
                $txtPNombre.attr("disabled", true);
                $txtSNombre.attr("disabled", true);
            } else {
                $txtRSocial.attr("disabled", true);
                $txtAPaterno.attr("disabled", false);
                $txtAMaterno.attr("disabled", false);
                $txtACasada.attr("disabled", false);
                $txtPNombre.attr("disabled", false);
                $txtSNombre.attr("disabled", false);
            }
        },
        ConsultarSusalud: function () {
            configDTSusalud.reload();
        },
        obtenerPersonaSusalud: function (entidad) {
            $txtAPaterno.val(entidad.apPaterno);
            $txtAMaterno.val(entidad.apMaterno);
            $txtACasada.val(entidad.apCasada);
            $txtPNombre.val(entidad.noPersona.split(" ")[0]);
            $txtSNombre.val(entidad.noPersona.split(" ")[1]);
            $cboSexo.val(entidad.deSexo);
            $txtFNacimiento.val(entidad.feNacimiento);
            $mdlSusalud.modal('hide');
        },
        obtenerPersonaSunat: function (entidad) {
            $txtRSocial.val(entidad.nombre_razon_social);
            $mdlSunat.modal('hide');
        },
        validarNuevoBotones: function () {
            if ($hfCodPersona.val() == "") {
                $btnCreateWorker.addClass('d-none');
                $btnNuevoDocumento.addClass('d-none');
                $btnNuevoTelefono.addClass('d-none');
                $btnNuevoDireccion.addClass('d-none');
                $btnNuevoCorreo.addClass('d-none');
            } else {
                if ($cboTPersonaNJ.val() == 2) {
                    $btnNuevoDocumento.addClass('d-none');
                } else {
                    $btnNuevoDocumento.removeClass('d-none');
                }
                $btnCreateWorker.removeClass('d-none');
                $btnNuevoTelefono.removeClass('d-none');
                $btnNuevoDireccion.removeClass('d-none');
                $btnNuevoCorreo.removeClass('d-none');
            }
        },
        obtenerDatosPersona: function (codigo) {
            var vpersona = { id: codigo };
            $.get("/Persona/obtenerPersona", vpersona)
                .done(function (data) {
                    EntidadPersona = data;
                    //$hfCodPersona.val(EntidadPersona.id);
                    $cboTPersonaNJ.val(EntidadPersona.gdtprsna);
                    $txtRSocial.val(EntidadPersona.rscl);
                    $txtAPaterno.val(EntidadPersona.aptrno);
                    $txtAMaterno.val(EntidadPersona.amtrno);
                    $txtACasada.val(EntidadPersona.acsda);
                    $txtPNombre.val(EntidadPersona.pnmbre);
                    $txtSNombre.val(EntidadPersona.snmbre);
                    $cboNacionalidad.val(EntidadPersona.ncnldd);
                    $cboPaisNac.val(EntidadPersona.cpncmnto);
                    $cboECivil.val(EntidadPersona.gdecvl);
                    $cboSexo.val(EntidadPersona.gdsxo);
                    $txtFNacimiento.val(EntidadPersona.fncmnto);
                    $txtFFallecimiento.val(EntidadPersona.ffllcmnto);
                    $txtPJFNacimiento.val(EntidadPersona.fncmnto);
                    $cboPJTSociedad.val(EntidadPersona.gdtescdd);
                    $cboPJTEmpresa.val(EntidadPersona.gdtetmno);
                    $cboPDistrito.val(EntidadPersona.cubgeo).trigger("change");
                    $FormularioPersona.AgregarCamposAuditoria(EntidadPersona);
                    configUbigeos.ubigeoPersona.cargarUbigeosPersona(EntidadPersona);
                    configDTPersona.reload();
                    configDTDocumento.reload();
                    configDTCorreo.reload();
                    configDTDireccion.reload();
                    configDTTelefono.reload();
                    configDtArchivo.reload();
                    funcionesPersona.initTipoPersona($cboTPersonaNJ.val());
                    $btnConsultaSusalud.hide();
                    $btnCreateWorker.show();
                    $tab_DatosGenerales.removeClass("disabled");
                    $tab_DatosGenerales.click();
                    $tab_Documento.click();
                    funcionesPersona.validarNuevoBotones();
                    var dataDocJuridica = sXMLToJson(EntidadPersona.mpepE04);
                    if (dataDocJuridica && dataDocJuridica.MPEPE04) {
                        if (EntidadPersona.gdtprsna == 2) {
                            $txtPJNDocumento.val(dataDocJuridica.MPEPE04.NDCMNTO + "");
                        } else {
                            $cboPNTDocumento.val(dataDocJuridica.MPEPE04.GDDCMNTO + "").trigger("change");
                            $txtPNDocumento.val(dataDocJuridica.MPEPE04.NDCMNTO + "");
                        }
                    }
                })
                .fail().always(function () {
                    //$FormularioPersona.find(":input").attr("disabled", false);
                    $FormularioPersona.DeshabilitarCamposAuditoria();
                });
        },
        insertarDniPersona: function (idpersona) {
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadDocumento = new Object();
                EntidadDocumento.gddcmnto = $cboPJTDocumento.val();
                EntidadDocumento.ndcmnto = $txtPJNDocumento.val();
                EntidadDocumento.finscrpcn = "";
                EntidadDocumento.fvncmnto = "";
                EntidadDocumento.fprncpl = "1";
                EntidadDocumento.idprsna = idpersona;
                EntidadDocumento.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();
                if ((!mayorFechaActual(false, $("#txtFInscripcion").val(), $("#txtFVncmt").val()))
                    || (!mayorFechaActual(false, $("#txtFInscripcion").val()))
                    || (!mayorFechaActual(false, $("#txtFVncmt").val()))) {
                    $.post("/Documento/insertarDocumento", EntidadDocumento).done(function () {
                        configDTPersona.reload();
                        configDTDocumento.reload();
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarPersona.attr("disabled", false);
                }
            }
        },
        mostrarModalHomonimia: function (valorBool) {
            if (valorBool) {
                configDtHomonimos.reload()
            }
        }
    }

    var funcionFechas2 = {
        validarFechaMayor: function (valorBool2, fecha1, fecha2) {
            fecha1.setHours(0, 0, 0, 0);
            fecha2.setHours(0, 0, 0, 0);
            if (fecha1.getTime() > fecha2.getTime() || fecha1 > fecha2) {
                valorBool2 = true;
            } else {
                valorBool2 = false;
            }
            return valorBool2;
        }
    }

    var funcionesDocumento = { //DOCUMENTOS
        insertarDocumento: function () {//INSERTAR
            if ($FormularioDocumento.valid()) {
                $btnGrabarDocumento.attr("disabled", true);
                EntidadDocumento.gddcmnto = $cboDocumento.val();
                EntidadDocumento.ndcmnto = $txtNDocumento.val();
                EntidadDocumento.finscrpcn = $txtFInscripcion.val();
                EntidadDocumento.fvncmnto = $txtFVncmt.val();
                EntidadDocumento.fprncpl = $cboPrincipalDocumento.val();
                EntidadDocumento.idprsna = $hfCodPersona.val();
                EntidadDocumento.gdestdo = $FormularioDocumento.find("[name='GDESTDO']").val();
                var band = false;
                var con1 = false;
                if ($txtFInscripcion.val()) {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual(band, $("#txtFInscripcion").val(), $("#txtFVncmt").val());
                    } else {
                        con1 = mayorFechaActual(band, $("#txtFInscripcion").val());
                    }
                } else {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual(band, $("#txtFVncmt").val());
                        if (!con1) {
                            con1 = true;
                        }
                    }
                }
                if (!con1) {
                    $.post("/Documento/insertarDocumento", EntidadDocumento).done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDocumento.modal("hide");
                            configDTDocumento.reload();
                        });
                        $FormularioDocumento.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarDocumento.attr("disabled", false);
                    $FormularioDocumento.find(".msg").text("");
                    $FormularioDocumento.find(".msg0").text("");
                }
            }
        },
        actualizarDocumento: function () {
            if ($FormularioDocumento.valid()) {
                $btnGrabarDocumento.attr("disabled", true);
                EntidadDocumento.gddcmnto = $cboDocumento.val();
                EntidadDocumento.ndcmnto = $txtNDocumento.val();
                EntidadDocumento.finscrpcn = $txtFInscripcion.val();
                EntidadDocumento.fvncmnto = $txtFVncmt.val();
                EntidadDocumento.fprncpl = $cboPrincipalDocumento.val();
                EntidadDocumento.idprsna = $hfCodPersona.val();
                EntidadDocumento.gdestdo = $FormularioDocumento.find("[name='GDESTDO']").val();
                var band = false;
                var con1 = false;
                if ($txtFInscripcion.val()) {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual(band, $("#txtFInscripcion").val(), $("#txtFVncmt").val());
                    } else {
                        con1 = mayorFechaActual(band, $("#txtFInscripcion").val());
                    }
                } else {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual(band, $("#txtFVncmt").val());
                        if (!con1) {
                            con1 = true;
                        }
                    }
                }
                if (!con1) {
                    $.post("/Documento/actualizarDocumento", EntidadDocumento).done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDocumento.modal("hide");
                            configDTDocumento.reload();
                        });
                        $FormularioDocumento.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarDocumento.attr("disabled", false);
                }
            }
        },
        obtenerDocumento: function (idDocumento) {
            var parametro = { id: idDocumento, idprsna: $hfCodPersona.val() };
            $.get("/Documento/obtenerDocumento", parametro)
                .done(function (data) {
                    EntidadDocumento = data;
                    $cboDocumento.val(EntidadDocumento.gddcmnto).trigger("change");
                    $txtNDocumento.val(EntidadDocumento.ndcmnto);
                    $txtFInscripcion.val(EntidadDocumento.finscrpcn);
                    $txtFVncmt.val(EntidadDocumento.fvncmnto);
                    $cboPrincipalDocumento.val(EntidadDocumento.fprncpl);
                    $hfCodDocumento.val(EntidadDocumento.id);
                    $FormularioDocumento.AgregarCamposAuditoria(EntidadDocumento);
                    if ($cboTPersonaNJ.val() == 2) {
                        $cboDocumento.prop('disabled', true);
                    } else {
                        $cboDocumento.prop('disabled', false);
                    }
                })
                .fail().always(function () {
                    $FormularioDocumento.DeshabilitarCamposAuditoria();
                    $cboDocumento.val(EntidadDocumento.gddcmnto).attr("disabled", true);
                });
        },
        eliminarDocumento: function (idDocumento) {
            var parametro = { id: idDocumento, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Documento/eliminarDocumento", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTDocumento.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTDocumento.reload();
                        })
                }
            });
        },
        guardarDocumento: function () {
            if ($hfaction.val() == "N") {
                funcionesDocumento.insertarDocumento();
            }
            else {
                funcionesDocumento.actualizarDocumento();
            }
        },
    }

    var funcionesCorreo = {
        insertarCorreo: function () {
            if ($FormularioCorreo.valid()) {
                $btnGrabarCorreo.attr("disabled", true);
                EntidadCorreo.gdtcrreo = $cboTCorreo.val();
                EntidadCorreo.ccrreo = $txtCorreo.val();
                EntidadCorreo.fprncpl = $cboPrincipalCorreo.val();
                EntidadCorreo.gdestdo = $FormularioCorreo.find("[name='GDESTDO']").val();
                EntidadCorreo.idprsna = $hfCodPersona.val();
                $.post("/Correo/insertarCorreo", EntidadCorreo).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlCorreo.modal("hide");
                        configDTCorreo.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDTCorreo.reload();
                });
                $btnGrabarCorreo.attr("disabled", false);
            }
        },
        actualizarCorreo: function () {
            if ($FormularioCorreo.valid()) {
                $btnGrabarCorreo.attr("disabled", true);
                EntidadCorreo.gdtcrreo = $cboTCorreo.val();
                EntidadCorreo.ccrreo = $txtCorreo.val();
                EntidadCorreo.fprncpl = $cboPrincipalCorreo.val();
                EntidadCorreo.gdestdo = $FormularioCorreo.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadCorreo.idprsna = $hfCodPersona.val();
                $.post("/Correo/actualizarCorreo", EntidadCorreo)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlCorreo.modal("hide");
                            configDTCorreo.reload();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTCorreo.reload();
                    });
                $btnGrabarCorreo.attr("disabled", false);
            }
        },
        obtenerCorreo: function (idCorreo) {
            var parametro = { id: idCorreo, idprsna: $hfCodPersona.val() };
            $.get("/Correo/obtenerCorreo", parametro)
                .done(function (data) {
                    EntidadCorreo = data;
                    $cboTCorreo.val(EntidadCorreo.gdtcrreo);
                    $txtCorreo.val(EntidadCorreo.ccrreo);
                    $cboPrincipalCorreo.val(EntidadCorreo.fprncpl);
                    $hfCodCorreo.val(EntidadCorreo.id);
                    $FormularioCorreo.AgregarCamposAuditoria(EntidadCorreo);
                })
                .fail().always(function () {
                    $FormularioCorreo.find(":input").attr("disabled", false);
                    $FormularioCorreo.DeshabilitarCamposAuditoria();
                });
        },
        eliminarCorreo: function (idCorreo) {
            var parametro = { id: idCorreo, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Correo/eliminarCorreo", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTCorreo.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTCorreo.reload();
                        })
                }
            });
        },
        guardarCorreo: function () {
            if ($hfactionCorreo.val() == "N") {
                funcionesCorreo.insertarCorreo();
            }
            else {
                funcionesCorreo.actualizarCorreo();
            }
            //$hfactionCorreo.val("");
        }
    }

    var funcionesDireccion = {
        insertarDireccion: function () {
            if ($FormularioDireccion.valid()) {
                $btnGrabarDireccion.attr("disabled", true);
                EntidadDireccion.gdtdrccn = $cboTDireccion.val();
                EntidadDireccion.gdtvia = $cboTVia.val();
                EntidadDireccion.via = $txtVia.val();
                EntidadDireccion.gdtdzna = $cboTipoZona.val();
                EntidadDireccion.nvia = $txtNroVia.val();
                EntidadDireccion.nintrr = $txtInterior.val();
                EntidadDireccion.zna = $txtNroZona.val();
                EntidadDireccion.rfrncia = $txtReferencia.val();
                EntidadDireccion.fprncpl = $cboPrincipalDireccion.val();
                EntidadDireccion.cubgeo = $cboDistritoDirec.val();
                EntidadDireccion.gdestdo = $FormularioDireccion.find("[name='GDESTDO']").val();
                EntidadDireccion.idprsna = $hfCodPersona.val();
                $.post("/Direccion/insertarDireccion", EntidadDireccion).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlDireccion.modal("hide");
                        configDTDireccion.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDTDireccion.reload();
                });
                $btnGrabarDireccion.attr("disabled", false);
            }
        },
        actualizarDireccion: function () {
            if ($FormularioDireccion.valid()) {
                $btnGrabarDireccion.attr("disabled", true);
                EntidadDireccion.id = $hfCodDireccion.val();
                EntidadDireccion.gdtdrccn = $cboTDireccion.val();
                EntidadDireccion.gdtvia = $cboTVia.val();
                EntidadDireccion.via = $txtVia.val();
                EntidadDireccion.gdtdzna = $cboTipoZona.val();
                EntidadDireccion.nvia = $txtNroVia.val();
                EntidadDireccion.nintrr = $txtInterior.val();
                EntidadDireccion.zna = $txtNroZona.val();
                EntidadDireccion.rfrncia = $txtReferencia.val();
                EntidadDireccion.fprncpl = $cboPrincipalDireccion.val();
                EntidadDireccion.cubgeo = $cboDistritoDirec.val();
                EntidadDireccion.gdestdo = $FormularioDireccion.find("[name='GDESTDO']").val();
                EntidadDireccion.idprsna = $hfCodPersona.val();
                $.post("/Direccion/actualizarDireccion", EntidadDireccion)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDireccion.modal("hide");
                            configDTDireccion.reload();

                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDireccion.reload();
                    });
                $btnGrabarDireccion.attr("disabled", false);
            }
        },
        obtenerDireccion: function (idDireccion) {
            var parametro = { id: idDireccion, idprsna: $hfCodPersona.val() };
            $.get("/Direccion/obtenerDireccion", parametro)
                .done(function (data) {
                    EntidadDireccion = data;
                    $cboTDireccion.val(EntidadDireccion.gdtdrccn);
                    $cboTVia.val(EntidadDireccion.gdtvia);
                    $txtVia.val(EntidadDireccion.via);
                    $cboTipoZona.val(EntidadDireccion.gdtdzna);
                    $txtNroVia.val(EntidadDireccion.nvia);
                    $txtInterior.val(EntidadDireccion.nintrr);
                    $txtNroZona.val(EntidadDireccion.zna);
                    $txtReferencia.val(EntidadDireccion.rfrncia);
                    $cboPrincipalDireccion.val(EntidadDireccion.fprncpl);
                    configUbigeos.ubigeoDireccion.cargarUbigeo(EntidadDireccion);
                    $hfCodDireccion.val(EntidadDireccion.id);
                    $FormularioDireccion.AgregarCamposAuditoria(EntidadDireccion);

                })
                .fail().always(function () {
                    $FormularioDireccion.find(":input").attr("disabled", false);
                    $FormularioDireccion.DeshabilitarCamposAuditoria();
                    $cboTDireccion.attr("disabled", true);
                });
        },
        eliminarDireccion: function (idDireccion) {
            var parametro = { id: idDireccion, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Direccion/eliminarDireccion", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTDireccion.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTDireccion.reload();
                        })
                }
            });
        },
        guardarDireccion: function () {
            if ($hfactionDireccion.val() == "N") {
                funcionesDireccion.insertarDireccion();
            }
            else {
                funcionesDireccion.actualizarDireccion();
            }

            //$hfactionDireccion.val("");
        },
        selectTipoZona: function () {
            if ($cboTipoZona.val() == 0) {
                $FormularioDireccion.find('.input-zona').attr('disabled', true);
            } else {
                $FormularioDireccion.find('.input-zona').attr('disabled', false);
            }
        },
        inicioModal: function () {
            $cboTipoZona.attr('disabled', false);
            funcionesDireccion.selectTipoZona();
        }
    }

    var funcionesTelefono = {
        insertarTelefono: function () {
            if ($FormularioTelefono.valid()) {
                $btnGrabarTelefono.attr("disabled", true);
                EntidadTelefono.gdttlfno = $cboTTelefono.val();
                EntidadTelefono.cubgeo = $cboDepartamentoTlfno.val();
                EntidadTelefono.ntlfno = $txtTelefono.val();
                EntidadTelefono.obsrvcn = $txtObservacion.val();
                EntidadTelefono.fprncpl = $cboPrincipalTelefono.val();//$cboPrincipal.val(); 
                EntidadTelefono.gdestdo = $FormularioTelefono.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadTelefono.idprsna = $hfCodPersona.val();
                $.post("/Telefono/insertarTelefono", EntidadTelefono)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdltelefono.modal("hide");
                            configDTTelefono.reload();
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTTelefono.reload();
                    });
                $btnGrabarTelefono.attr("disabled", false);
            }
        },
        actualizarTelefono: function () {
            if ($FormularioTelefono.valid()) {
                $btnGrabarTelefono.attr("disabled", true);
                EntidadTelefono.gdttlfno = $cboTTelefono.val();
                EntidadTelefono.cubgeo = $cboDepartamentoTlfno.val();
                EntidadTelefono.ntlfno = $txtTelefono.val();
                EntidadTelefono.obsrvcn = $txtObservacion.val();
                EntidadTelefono.fprncpl = $cboPrincipalTelefono.val();//$cboPrincipal.val();
                EntidadTelefono.gdestdo = $FormularioTelefono.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadTelefono.idprsna = $hfCodPersona.val();
                $.post("/Telefono/actualizarTelefono", EntidadTelefono)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdltelefono.modal("hide");
                            configDTTelefono.reload();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTTelefono.reload();
                    });
                $btnGrabarTelefono.attr("disabled", false);
            }
        },
        obtenerTelefono: function (idTelefono) {
            var parametro = { id: idTelefono, idprsna: $hfCodPersona.val() };
            $.get("/Telefono/obtenerTelefono", parametro)
                .done(function (data) {
                    EntidadTelefono = data;
                    $cboTTelefono.val(EntidadTelefono.gdttlfno).trigger("change");
                    $cboDepartamentoTlfno.val(EntidadTelefono.cubgeo);
                    $txtTelefono.val(EntidadTelefono.ntlfno);
                    $txtObservacion.val(EntidadTelefono.obsrvcn);
                    $cboPrincipalTelefono.val(EntidadTelefono.fprncpl);
                    $hfCodTelefono.val(EntidadTelefono.id);
                    $FormularioTelefono.AgregarCamposAuditoria(EntidadTelefono);
                })
                .fail().always(function () {
                    $FormularioTelefono.DeshabilitarCamposAuditoria();
                });
        },
        eliminarTelefono: function (idTelefono) {
            var parametro = { id: idTelefono, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Telefono/eliminarTelefono", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTTelefono.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTTelefono.reload();
                        })
                }
            });
        },
        guardarTelefono: function () {
            if ($hfactionTelefono.val() == "N") {
                funcionesTelefono.insertarTelefono();
            }
            else {
                funcionesTelefono.actualizarTelefono();
            }
            //$hfactionTelefono.val("");
        }
    }

    var funcionesArchivo = {
        insertar: function () {
            $btnGuardarArchivo.attr("disabled", true);
            EntidadArchivo = new Object();
            EntidadArchivo.idprsna = $hfCodPersona.val();
            /*  EntidadArchivo.idpn = $txtIDArchivo.val();*/
            EntidadArchivo.gdestdo = $frmArchivo.find("[name='GDESTDO']").val();
            $.post("/archivo/insertar", EntidadArchivo)
                .done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlArchivo.modal("hide");
                        configDtArchivo.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al modificar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDtArchivo.reload();
                });
            $btnGuardarArchivo.attr("disabled", false);
        },
        actualizar: function () {
            $btnGuardarArchivo.attr("disabled", true);
            EntidadArchivo = new Object();
            EntidadArchivo.id = $frmArchivo.find("[name='ID']").val();
            EntidadArchivo.idprsna = $hfCodPersona.val();
            /* EntidadArchivo.idpn = $txtIDArchivo.val();*/
            EntidadArchivo.gdestdo = $frmArchivo.find("[name='GDESTDO']").val();
            $.post("/archivo/actualizar", EntidadArchivo)
                .done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlArchivo.modal("hide");
                        configDtArchivo.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al modificar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDtArchivo.reload();
                });
            $btnGuardarArchivo.attr("disabled", false);
        },
        eliminar: function (id) {
            var parametro = { id: id };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/archivo/eliminar", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDtArchivo.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDtArchivo.reload();
                        })
                }
            });
        },
        obtener: function (id, obj) {
            var parametro = { id: id };
            $.get("/archivo/obtener", parametro)
                .done(function (data) {
                    EntidadArchivo = data;
                    //$txtIDArchivo.val(EntidadArchivo.idpn);
                    $frmArchivo.AgregarCamposAuditoria(EntidadArchivo);
                })
                .fail().always(function () {
                    $frmArchivo.find(":input").attr("disabled", false);
                    $frmArchivo.DeshabilitarCamposAuditoria();
                });
        },
        guardar: function () {
            if ($hfaction.val() == "N") {
                funcionesArchivo.insertar();
            }
            else {
                funcionesArchivo.actualizar();
            }
        }
    }

    var eventosIncrustadosPersona = {
        botonNuevoPersona: function () {
            $btnNuevoPersona.on("click", function () {
                funcionesPersona.NuevoPersona();
            });
        },
        botonBuscar: function () {
            $btnBuscarPersona.on("click", function () {
                configDTPersona.reload();
            })
        },
        botonGrabarPersona: function () {
            $btnGrabarPersona.on("click", function () {
                $(this).attr("disabled", true);
                funcionesPersona.guardarPersona();
                $(this).attr("disabled", false);
            })

        },
        CambioDoc: function () {
            $cboTDocumento.on('change', function () {
                //funcionesPersona.validartipodoc()
            });
        },
        botonConsultarSUSALUD: function () {
            $btnConsultaSusalud.on("click", function () {
                $cboSSLDTDocumento.val('');
                $txtSSLDNDocumento.val('');
                funcionesPersona.ConsultarSusalud();
            });
            $btnBuscarSUSALUD.on("click", function () {
                funcionesPersona.ConsultarSusalud();
            });
        },
        botonesConsultaSunat: function () {
            $btnConsultaSunat.on("click", function () {
                $cboTDocumentoSUNAT.val('8');
                $cboTDocumentoSUNAT.prop('disabled', true);
                $txtNDocumentoSUNAT.val('');
                configDTSunat.reload();
            });
            $btnBuscarSunat.on("click", function () {
                configDTSunat.reload();
            });
        },
        tap_listaPersona: function () {
            $tab_Lista.on("click", function () {
                $tab_Direccion.addClass("disabled");
                $tab_Telefono.addClass("disabled");
                $tab_CuentaCorreo.addClass("disabled");
                $tab_Direccion.addClass("disabled");
                $tab_Documento.addClass("disabled");
                $tab_DatosGenerales.addClass("disabled");
                $tab_Lista.removeClass("disabled");
                $hfCodPersona.val("");
                configDTDocumento.tiposDocumentos = [];
                configDTDireccion.tipoDirecciones = [];
                configDTCorreo.tienePrincipal = false;
                configDTDireccion.tienePrincipal = false;
                configDTTelefono.tienePrincipal = false;
            });

        },
        cboTipoPersona: function () {
            $cboTPersonaNJ.change(function () {
                funcionesPersona.initTipoPersona(this.value);
            });
        },
        Homonimia: function (options) {

            const defecto = {
                class: 'input-vacio',
                elemcontent: 'document',
            };
            var config = $.extend(defecto, options);

            //var elemClass = clase || "validar-vacios";
            var Elements = $(config.elemcontent).find('.' + config.class); // this.find('.' + elemClass);
            var ElemLength = $(config.elemcontent).find('.' + config.class).length;//this.find('.' + elemClass).length;
            $('.' + config.class).on("blur", function () {
                var boolVacio = false;
                for (var i = 0; i < ElemLength; i++) {
                    if (Elements[i].value) {
                        boolVacio = true;
                    } else { boolVacio = false; }
                }
                if (boolVacio) {
                    funcionesPersona.mostrarModalHomonimia(boolVacio);
                }
            });
        },
        nacionalidad: function () {
            $cboNacionalidad.on("change", function () {
                $cboPDepartamento.val("").change();
                $cboPDistrito.attr("disabled", true);
                configUbigeos.f_nacionalidad(this);
                //cargarUbigeo(opts);
            });
            $cboPaisNac.on("change", function () {
                $cboPDepartamento.val("").change();
                $cboPDistrito.attr("disabled", true);
                configUbigeos.f_nacionalidad(this);
                //cargarUbigeo(opts);
            });
            $chkPaisNac.on('change', function () {
                $cboPaisNac.attr("disabled", true);
                $cboNacionalidad.attr("disabled", false);
                if ($(this).is(':checked')) {
                    $cboPaisNac.attr("disabled", false);
                    $cboNacionalidad.attr("disabled", true);
                } else {
                    $cboPaisNac.val($cboNacionalidad.val());
                    configUbigeos.f_nacionalidad($cboNacionalidad);
                }
            });
        },
        init: function () {
            eventosIncrustadosPersona.botonNuevoPersona();
            eventosIncrustadosPersona.botonBuscar();
            eventosIncrustadosPersona.botonGrabarPersona();
            eventosIncrustadosPersona.botonConsultarSUSALUD();
            eventosIncrustadosPersona.tap_listaPersona();
            eventosIncrustadosPersona.cboTipoPersona();
            eventosIncrustadosPersona.nacionalidad();
            eventosIncrustadosPersona.botonesConsultaSunat();
            eventosIncrustadosPersona.Homonimia();
        }
    }

    var eventosIncrustadosDocumento = {
        botonGrabarDocumento: function () {
            $btnGrabarDocumento.on("click", function () {
                $(this).attr("disabled", true);
                funcionesDocumento.guardarDocumento()
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoDocumento.on("click", function () {
                $hfaction.val("N");
                if (configDTDocumento.tienePrincipal) {
                    $cboPrincipalDocumento.val("2");
                } else {
                    $cboPrincipalDocumento.val("1");
                }
                $modalTituloDocum.text("Agregar Documento")
                if ($cboTPersonaNJ.val() == 2) {
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGD("GDDCMNTO");
                    $cboDocumento.val('8').trigger("change");
                    $cboDocumento.prop('disabled', true);
                } else {
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGDFiltrado("GDDCMNTO", configDTDocumento.tiposDocumentos);
                    if (jQuery.inArray('1', configDTDocumento.tiposDocumentos) == -1) {
                        $cboDocumento.val('1').trigger("change");
                    }
                    $cboDocumento.val("").attr("disabled", false);
                    $cboDocumento.prop('disabled', false);
                }
            });
        },
        init: function () {
            eventosIncrustadosDocumento.botonGrabarDocumento();
            eventosIncrustadosDocumento.botonNuevo();
        }
    }

    var eventosIncrustadosCorreo = {
        botonGrabarCorreo: function () {
            $btnGrabarCorreo.on("click", function () {
                $(this).attr("disabled", true);
                funcionesCorreo.guardarCorreo();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoCorreo.on("click", function () {
                $hfactionCorreo.val("N");
                $modalTituloCorreo.text("Agregar Correo");
                if (configDTCorreo.tienePrincipal) {
                    $cboPrincipalCorreo.val("2");
                } else {
                    $cboPrincipalCorreo.val("1");
                }
            });
        },
        init: function () {
            eventosIncrustadosCorreo.botonGrabarCorreo();
            eventosIncrustadosCorreo.botonNuevo();
        }
    }

    var eventosIncrustadosDireccion = {
        botonGrabarDireccion: function () {
            $btnGrabarDireccion.on("click", function () {
                $(this).attr("disabled", true);
                funcionesDireccion.guardarDireccion();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoDireccion.on("click", function () {
                $hfactionDireccion.val("N"); $modalTituloDireccion.text("Agregar Dirección");
                $cboTDireccion.empty();
                $cboTDireccion.LlenarSelectGDFiltrado("GDTDRCCN", configDTDireccion.tipoDirecciones);
                if ($cboTPersonaNJ.val() == 1) {
                    $cboTDireccion.children('option[value="3"]').hide();
                } else {
                    $cboTDireccion.children('option[value="3"]').show();
                    $cboTDireccion.val("3").trigger("change")
                }
                if (configDTDireccion.tienePrincipal) {
                    $cboPrincipalDireccion.val("2");
                } else {
                    $cboPrincipalDireccion.val("1");
                }
                $cboTDireccion.val("").attr("disabled", false);
            });

        },
        onChangeTipoZona: function () {
            $cboTipoZona.on("change", function () {
                configModalDireccion.form.eventos.reset();
                funcionesDireccion.selectTipoZona();
            });
        },
        init: function () {
            eventosIncrustadosDireccion.botonGrabarDireccion();
            eventosIncrustadosDireccion.botonNuevo();
            eventosIncrustadosDireccion.onChangeTipoZona();
        }
    }

    var eventosIncrustadostelefono = {
        botonGrabarTelefono: function () {
            $btnGrabarTelefono.on("click", function () {
                $(this).attr("disabled", true);
                funcionesTelefono.guardarTelefono();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoTelefono.on("click", function () {
                $hfactionTelefono.val("N");
                $modalTituloTelefono.text("Agregar Teléfono");
                if (configDTTelefono.tienePrincipal) {
                    $cboPrincipalTelefono.val("2");
                } else {
                    $cboPrincipalTelefono.val("1");
                }
            });
        },
        cboTipoTelefono: function () {
            $cboTTelefono.on("change", function () {
                $this = $(this);
                $cboDepartamentoTlfno.val("").trigger("change");
                if ($this.val() == 1) {
                    $cboDepartamentoTlfno.removeClass("error");
                    $cboDepartamentoTlfno.attr("required", false);
                    $cboDepartamentoTlfno.attr("disabled", true);
                } else {
                    $cboDepartamentoTlfno.attr("required", true);
                    $cboDepartamentoTlfno.attr("disabled", false);
                }
            });
        },
        cboDepartamentoTelefono: function () {
            $cboDepartamentoTlfno.attr("disabled", true);
            var $ubigeo = $(JSON.parse(localStorage.getItem('ubigeo')));
            $cboDepartamentoTlfno.append($("<option />").val('').text("Cargando..."));
            $.each($ubigeo, function (key, item) {
                $cboDepartamentoTlfno.append($("<option />").val(item["cdgo"]).text(item["dscrpcn"]));
            });
            $cboDepartamentoTlfno.children('option[value=""]').text("Seleccione").val("");
            //$cboDepartamentoTlfno.attr("disabled", false);
        },
        init: function () {
            eventosIncrustadostelefono.botonGrabarTelefono();
            eventosIncrustadostelefono.botonNuevo();
            eventosIncrustadostelefono.cboTipoTelefono();
            eventosIncrustadostelefono.cboDepartamentoTelefono();
        }
    }

    var eventosIncrustadosArchivo = {
        btnNuevoArchivo: function () {
            $btnNuevoArchivo.on("click", function () {
                $hfaction.val("N");
                $mdlArchivoLabel.text("Agregar Archivo");
            });
        },
        btnGuardarArchivo: function () {
            $btnGuardarArchivo.on("click", function () {
                $(this).attr("disabled", true);
                funcionesArchivo.guardar();
                $(this).attr("disabled", false);
            });
        },
        init: function () {
            eventosIncrustadosArchivo.btnNuevoArchivo();
            eventosIncrustadosArchivo.btnGuardarArchivo();
        }
    }

    var cboEstados = {
        init: function () {
            $cboEstado.LlenarSelectGD("GDESTDO");
        }
    };

    var cargarCombos = {
        init: function () {
            $cboTipoDocumento.LlenarSelectGD("GDDCMNTO");
            $cboTPersonaNJ.LlenarSelectGD("GDTPRSNA");
            $cboTPersona_NJ.LlenarSelectGD("GDTPRSNA");
            $cboSexo.LlenarSelectGD("GDSXO", "vlR1", "dgddtlle");
            $cboECivil.LlenarSelectGD("GDECVL", "vlR1", "dgddtlle");
            $cboPJTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPNTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboNacionalidad.LlenarSelectPais();
            $cboPaisNac.LlenarSelectPais();
            $cboSSLDTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPJTSociedad.LlenarSelectGD("GDTESCDD");
            $cboPJTEmpresa.LlenarSelectGD("GDTETMNO");
            $cboTDocumentoSUNAT.LlenarSelectGD("GDDCMNTO");

            //------Documento ----------
            $cboDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPrincipalDocumento.LlenarSelectGD("GDTDOPC");

            //---------Correo----------
            $cboTCorreo.LlenarSelectGD("GDTCRREO");//GDTCRREO
            $cboPrincipalCorreo.LlenarSelectGD("GDTDOPC"); //GDTDOPC

            //--------Direccion-----------
            $cboTDireccion.LlenarSelectGD("GDTDRCCN");
            if ($cboTPersonaNJ.val() == 1) {
                $cboTDireccion.children('option[value="3"]').hide();
            } else {
                $cboTDireccion.children('option[value="3"]').show();
                $cboTDireccion.val("3").trigger("change")
            }
            $cboTVia.LlenarSelectGD("GDTVIA");
            //$cboVia.LlenarSelectGD("GDVIA");
            $cboTipoZona.LlenarSelectGD("GDTDZNA"); //GDTDZNA
            $cboPrincipalDireccion.LlenarSelectGD("GDTDOPC");

            //---------Telefono-----------
            $cboTTelefono.LlenarSelectGD("GDTTLFNO");
            $cboPrincipalTelefono.LlenarSelectGD("GDTDOPC");
        }
    };

    return {
        inicializar: function () {
            //donde se inicializa las funciones\
            configDTPersona.init();
            eventosIncrustadosPersona.init();
            cboEstados.init();
            cargarCombos.init();
            configModalDocumento.init();
            eventosIncrustadosDocumento.init();
            configModalCorreo.init();
            eventosIncrustadosCorreo.init();
            configModalDireccion.init();
            eventosIncrustadosDireccion.init();
            configModalTelefono.init();
            eventosIncrustadostelefono.init();
            configDatePickers.init();
            configModalArchivo.init();
            eventosIncrustadosArchivo.init();
            configCboTipoDocumentos.init();
            configFormPersona.homonimiaPN.init();
            configUbigeos.ubigeoPersona.init();
            configUbigeos.ubigeoDireccion.init();
            validacionControles.init();
        }
    }

}();
$(function () {
    initBusquedaPersona.inicializar();
});
