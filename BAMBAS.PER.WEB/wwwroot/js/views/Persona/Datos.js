﻿var InicializarDatosPersonas = function () {
    //#region ---------CROPPER -----------------------
    $modalCropper = $('#modalCropper');
    $mainCropper = $('#main-cropper');
    $seleccioneImagen = $("#seleccion-imagen");
    $btnUploadImage = $('#btnupload');
    $imagenSeleccionada = $("#imagen-seleccionada");
    //#region ---------PERSONA------------------------
    $txtDocumento = $("#txtDocumento");
    $FormularioPersona = $("#FormularioPersona");
    $cboTPersonaNJ = $("#cboTPersonaNJ");
    $cboPNTDocumento = $("#cboPNTDocumento");
    $txtPNDocumento = $("#txtPNDocumento");
    $checkContract = $("#checkContract");
    $txtAPaterno = $("#txtAPaterno");
    $txtAMaterno = $("#txtAMaterno");
    $txtACasada = $("#txtACasada");
    $txtPNombre = $("#txtPNombre");
    $txtSNombre = $("#txtSNombre");
    $cboECivil = $("#cboECivil");
    $cboSexo = $("#cboSexo");
    $txtFNacimiento = $("#txtFNacimiento");
    $cboNacionalidad = $("#cboNacionalidad");
    $cboPDepartamento = $("#cboPDepartamento");
    $cboPProvincia = $("#cboPProvincia");
    $cboPDistrito = $("#cboPDistrito");
    $txtRSocial = $("#txtRSocial");
    $cboPJTDocumento = $("#cboPJTDocumento");
    $txtPJNDocumento = $("#txtPJNDocumento");
    $cboPJTSociedad = $("#cboPJTSociedad");
    $cboPJTEmpresa = $("#cboPJTEmpresa");
    $txtPJFNacimiento = $("#txtPJFNacimiento");
    $PaisNac = $("#PaisNac");
    $chkPaisNac = $PaisNac.find("input[type=checkbox]");
    $cboPaisNac = $("#cboPaisNac");
    $cboEstado = $(".select-estados");
    $cboEstadoListado = $(".select-estado-listado");
    $cboEstadoFormulario = $(".select-estado-formulario");
    $btnGrabarPersona = $("#btnGrabarPersona");
    $btnCreateWorker = $("#btnCreateWorker");
    $btnVerTrabajador = $("#btnVerTrabajador");
    $tab_DatosGenerales = $("#DatosGenerales-tab");
    $tab_Documento = $("#Documento-tab");
    $tab_Direccion = $("#Direccion-tab");
    $tab_Telefono = $("#Telefono-tab");
    $tab_CuentaCorreo = $("#CuentaCorreo-tab");
    $tab_Archivos = $("#Archivos-tab");
    $tab_Colaboradores = $("#Colaboradores-tab");
    $tab_Lista = $("#Lista-tab");
    $lblTDocumento = $("#lblTDocumento");
    $txtnuDocumento = $("#txtnuDocumento");
    $lblnuDocumento = $("#lblnuDocumento");
    $hfCodPersona = $("#hfCodPersona");
    $hddIdtrbjdr = $("#hddIdtrbjdr");
    $btnBuscarSUSALUD = $("#btnBuscarSUSALUD");
    $btnConsultaSusalud = $("#btnConsultaSusalud");
    $cnt_pNatural = $(".pNatural");
    $cnt_pNaturalAgregando = $(".pNaturalAgregando");
    $cnt_pJuridica = $(".pJuridica");
    $cboSSLDTDocumento = $('#cboSSLD-TDocumento');
    $txtSSLDNDocumento = $('#txtSSLD-NDocumento');
    $mdlSusalud = $("#mdlSusalud");
    $txtPJNDocumento = $("#txtPJNDocumento");
    $cboTipoDocumento = $("#cboTipoDocumento");
    $cboTPersona_NJ = $("#cboTPersona_NJ");
    $dtHomonimo = $("#dtHomonimo");
    $mdlHomonimos = $("#mdlHomonimos");
    //#endregion -------------PERSONA----------------------

    //#region ------------DOCUMENTO ---------------------
    $btnNuevoDocumento = $("#btnNuevoDocumento");
    $tblDocumento = $("#tabla_documento");
    $hfaction = $("#hfaction");
    $hfCodDocumento = $("#hfCodDocumento");
    $mdlDocumento = $("#mdlDocumento");
    $FormularioDocumento = $("#FormularioDocumento");
    $cboDocumento = $("#cboDocumento");
    $txtNDocumento = $("#txtNDocumento");
    $cboPrincipalDocumento = $("#cboPrincipalDocumento");
    $txtFInscripcion = $("#txtFInscripcion");
    $txtFVncmt = $("#txtFVncmt");
    $btnGrabarDocumento = $("#btnGrabarDocumento");
    $modalTituloDocum = $mdlDocumento.find(".modal-title");
    $txtUCreacion = $("#txtUCreacion");
    $txtFCreacion = $("#txtFCreacion");
    $txtUEdicion = $("#txtUEdicion");
    $txtFCreacion = $("#txtFCreacion");
    //#endregion ------------DOCUMENTO ---------------------

    //#region ---------DIRECCION--------------------
    $btnNuevoDireccion = $("#btnNuevoDireccion");
    $hfCodDireccion = $("#hfCodDireccion");
    $tblDireccion = $("#tabla_direccion");
    $mdlDireccion = $("#mdlDireccion");
    $modalTituloDireccion = $mdlDireccion.find(".modal-title");
    $FormularioDireccion = $("#FormularioDireccion");
    $btnGrabarDireccion = $("#btnGrabarDireccion");
    $cboTDireccion = $("#cboTDireccion");
    $cboTVia = $("#cboTVia");
    $txtVia = $("#txtVia");
    $cboTipoZona = $("#cboTipoZona");
    $txtNroZona = $("#txtNroZona");
    $txtNroVia = $("#txtNroVia");
    $txtInterior = $("#txtInterior");
    $txtReferencia = $("#txtReferencia");
    $txtFPrincipal = $("#txtFPrincipal");
    $cboPrincipalDireccion = $("#cboPrincipalDireccion");
    $hfactionDireccion = $("#hfactionDireccion");
    $cboDepartamentoDirec = $("#cboDepartamentoDirec");
    $cboProvinciaDirec = $("#cboProvinciaDirec");
    $cboDistritoDirec = $("#cboDistritoDirec");

    //#endregion ---------DIRECCION--------------------

    //#region ------TELEFONO----------------
    $btnNuevoTelefono = $("#btnNuevoTelefono");
    $hfCodPersona = $("#hfCodPersona");
    $hfCodTelefono = $("#hfCodTelefono");
    $tbltelefono = $("#tabla_telefono");
    $mdltelefono = $("#mdlTelefono");
    $modalTituloTelefono = $mdltelefono.find(".modal-title");
    $cboPrincipalTelefono = $("#cboPrincipalTelefono");
    $FormularioTelefono = $("#FormularioTelefono")
    $btnGrabarTelefono = $("#btnGrabarTelefono");
    $cboTTelefono = $("#cboTTelefono");
    $cboDepartamentoTlfno = $("#cboDepartamentoTlfno");
    $txtTelefono = $("#txtTelefono");
    $txtObservacion = $("#txtObservacion");
    $hfactionTelefono = $("#hfactionTelefono");
    //#endregion ------TELEFONO----------------

    //#region ----------- CORREO----------------------
    $btnNuevoCorreo = $("#btnNuevoCorreo");
    $hfCodCorreo = $("#hfCodCorreo");
    $tblCorreo = $("#tabla_correo");
    $mdlCorreo = $("#mdlCorreo");
    $modalTituloCorreo = $mdlCorreo.find(".modal-title");
    $FormularioCorreo = $("#FormularioCorreo");
    $btnGrabarCorreo = $("#btnGrabarCorreo");
    $cboTCorreo = $("#cboTCorreo");
    $txtCorreo = $("#txtCorreo");
    $cboPrincipalCorreo = $("#cboPrincipalCorreo");
    $cboEstado = $(".select-estados");
    $txtFEstado = $("#txtFEstado");
    $txtUCreacion = $("#txtUCreacion");
    $txtFCreacion = $("#txtFCreacion");
    $txtUEdicion = $("#txtUEdicion");
    $txtFCreacion = $("#txtFCreacion");
    $txtFEdicion = $("#txtFEdicion");
    $hfactionCorreo = $("#hfactionCorreo");
    //#endregion ----------- CORREO----------------------

    //#region --------------- ARCHIVOS ------------------
    $txtNombArch = $("#txtNombArch");// VARIABLE PRUEBA
    $txtFileLoad = $("#txtFileLoad");
    $hfactionArchivo = $("#hfactionArchivo");
    $hfCodArchivo = $("#hfCodArchivo");
    $FormularioArchivo = $("#FormularioArchivo"); //FORMULARIO
    $btnNuevoArchivo = $('#btnNuevoArchivo');
    $dtArchivo = $('#dtArchivo');  //DATATABLE
    $mdlArchivo = $('#mdlArchivo'); //MODAL
    $mdlArchivoLabel = $('#mdlArchivoLabel'); //EVENTO INCRUSTADO MODAL
    $modalTituloArchivo = $mdlCorreo.find(".modal-title");
    $btnGuardarArchivo = $("#btnGuardarArchivo");  //BOTON
    //#endregion -------------------------------------

    //#region-----------COLABORADORES ----------- 
    $dtColaboradores = $('#dtColaboradores');
    //#endregion---------------------------------

    //#region----------SUNAT----------------------------
    $btnConsultaSunat = $("#btnConsultaSunat");
    $cboTDocumentoSUNAT = $("#cboSUNAT-TDocumento");
    $txtNDocumentoSUNAT = $("#txtSUNAT-NDocumento");
    $mdlSunat = $("#mdlSunat");
    $dtPersonaJuridica = $("#dtPersonaJuridica");
    $btnBuscarSunat = $("#btnBuscarSunat");
    //#endregion-----------------------------------------
    $dtPersonaSusalud = $("#dtPersonaSusalud");
    $mdlSusalud = $("#mdlSusalud");
    //#region ---------------entidades----------------------
    var EntidadPersona = {
        gdtprsna: "",
        rscl: "",
        aptrno: "",
        amtrno: "",
        acsda: "",
        pnmbre: "",
        snmbre: "",
        ncnldd: "",
        cpncmnto: "",
        gdecvl: "",
        gdsxo: "",
        fto: "",
        fncmnto: "",
        ffllcmnto: "",
        cubgeo: "",
        gdestdo: "",
        ucrcn: "",
        uedcn: "",
        cboTDcmnto: "",
        txtnDcmnto: "",
        idprsna: "",
        fprncpl: "",
        finscrpcn: "",
        fvncmnto: "",
        gddcmnto: "",
        ndcmnto: "",
        fcntrtsta: "",
        idtrbjdr: "",
    }

    var EntidadDocumento = {
        id: "",
        gddcmnto: "",
        ndcmnto: "",
        finscrpcn: "",
        fvncmnto: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
        festdo: "",
    }

    var EntidadCorreo = {
        id: "",
        gdtcrreo: "",
        ccrreo: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
    }

    var EntidadDireccion = {
        id: "",
        gdtdrccn: "",
        gdtvia: "",
        via: "",
        nvia: "",
        nintrr: "",
        gdtdzna: "",
        zna: "",
        rfrncia: "",
        fprncpl: "",
        cubgeo: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: ""
    }

    var EntidadTelefono = {
        gdttlfno: "",
        cubgeo: "",
        ntlfno: "",
        obsrvcn: "",
        fprncpl: "",
        idprsna: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        gdestdo: "",
        festdo: "",
    }

    var EntidadSusalud = {
        afiliaciones: "",
        apCasada: "",
        apMaterno: "",
        apPaterno: "",
        coContinente: "",
        coDepartamento: "",
        coDistrito: "",
        coError: "",
        coPais: "",
        coPaisEmisor: "",
        coProvincia: "",
        deContinente: "",
        deDepartamento: "",
        deDistrito: "",
        dePais: "",
        deProvincia: "",
        deSexo: "",
        deUbigeo: "",
        feFallecimiento: "",
        feNacimiento: "",
        inFallecimiento: "",
        noPersona: "",
        nuDocumento: "",
        tiDocumento: ""
    }

    var DatosPersona = {
        DatosNatural: function () {
            $checkContract.is(':checked') == true ? EntidadPersona.fcntrtsta = true : EntidadPersona.fcntrtsta = false;
            EntidadPersona.aptrno = $txtAPaterno.val();
            EntidadPersona.amtrno = $txtAMaterno.val();
            EntidadPersona.pnmbre = $txtPNombre.val();
            EntidadPersona.snmbre = $txtSNombre.val();
            EntidadPersona.gdecvl = $cboECivil.val();
            EntidadPersona.gdsxo = $cboSexo.val();
            EntidadPersona.fncmnto = $txtFNacimiento.val();
            EntidadPersona.ncnldd = $cboNacionalidad.val();
            EntidadPersona.cpncmnto = $chkPaisNac.is(':checked') == true ? $cboPaisNac.val() : $cboNacionalidad.val();
            EntidadPersona.cubgeo = $cboPDistrito.val();
            //EntidadPersona.txtnDcmnto = $txtDocumento.val();
            //EntidadPersona.tiDocumento = $cboPNTDocumento.val();

        },
        DatosJuridica: function () {
            EntidadPersona.rscl = $txtRSocial.val();
            EntidadPersona.gdtescdd = $cboPJTSociedad.val();
            EntidadPersona.gdtetmno = $cboPJTEmpresa.val();
            EntidadPersona.fncmnto = $txtPJFNacimiento.val();
            EntidadPersona.cubgeo = "150101";
        }
    }

    var EntidadArchivo = {
        id: "",
        nmbrarchv: "",
        patharchv: "",
        fprncpl: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        idprsna: "",
        ruta: "",
    }

    var EntidadSunat = {
        "resultado": "",
        "nombre_razon_social": "",
        "direccion": "",
        "estado_contribuyente": "",
        "condicion_domicilio": "",
        "ubigeo": "",
        "tipo_via": "",
        "nombre_via": "",
        "codigo_zona": "",
        "tipo_zona": "",
        "numero": "",
        "interior": "",
        "lote": "",
        "departamento": "",
        "manzana": "",
        "kilometro": ""
    }

    //#endregion ------------entidades----------------------

    //#region ------------CONTROLES ---------------------

    var $accesoAgregarDocumento = $("#accesoAgregarDocumento");
    var $accesoEditarDocumento = $("#accesoEditarDocumento");
    var $accesoEliminarDocumento = $("#accesoEliminarDocumento");

    var $accesoAgregarDireccion = $("#accesoAgregarDireccion");
    var $accesoEditarDireccion = $("#accesoEditarDireccion");
    var $accesoEliminarDireccion = $("#accesoEliminarDireccion");

    var $accesoAgregarTelefono = $("#accesoAgregarTelefono");
    var $accesoEditarTelefono = $("#accesoEditarTelefono");
    var $accesoEliminarTelefono = $("#accesoEliminarTelefono");

    var $accesoAgregarCorreo = $("#accesoAgregarCorreo");
    var $accesoEditarCorreo = $("#accesoEditarCorreo");
    var $accesoEliminarCorreo = $("#accesoEliminarCorreo");

    var $accesoAgregarArchivo = $("#accesoAgregarArchivo");
    var $accesoEliminarArchivo = $("#accesoEliminarArchivo");
    var $accesoDescargarArchivo = $("#accesoDescargarArchivo");

    //#endregion ---------CONTROLES ---------------------

    //#region -----------------VALIDAR CONTROLES -------------
    var validacionControles = {
        init: function () {
            if ($accesoAgregarDocumento.val() == "False") {
                $btnNuevoDocumento.hide();
            }
            if ($accesoAgregarDireccion.val() == "False") {
                $btnNuevoDireccion.hide();
            }
            if ($accesoAgregarTelefono.val() == "False") {
                $btnNuevoTelefono.hide();
            }
            if ($accesoAgregarCorreo.val() == "False") {
                $btnNuevoCorreo.hide();
            }
            if ($accesoAgregarArchivo.val() == "False") {
                $btnNuevoArchivo.hide();
            }
        }
    }
    //#endregion ---------------------------------------------

    //#region ------------------- CONFIG DT ------------------
    var configDTDocumento = {
        objecto: null,
        tiposDocumentos: [],
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Documento/listarDocumento",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '8%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "N° Documento",
                    data: "ndcmnto",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "F. Inscripción",
                    data: "finscrpcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "Fec. Vencimiento",
                    data: "fvncmnto",
                    width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '5%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTDocumento.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '5%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '1%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '5%',
                    class: "text-center",
                    render: function (data) {
                        configDTDocumento.tiposDocumentos.push(data.igddcmnto);
                        var tpm = "";
                        if ($accesoEditarDocumento.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlDocumento" data-doc="${data.igddcmnto}" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarDocumento.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTDocumento.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesDocumento.eliminarDocumento(id);
                })
            },
            editar: function () {
                configDTDocumento.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    var doc = $(this).data("doc");
                    $modalTituloDocum.text("Editar Documento");
                    $hfaction.val("E");
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGDFiltrado("GDDCMNTO", configDTDocumento.tiposDocumentos, doc);
                    funcionesDocumento.obtenerDocumento(id);
                })
            },
            init: function () {
                configDTDocumento.eventos.eliminar();
                configDTDocumento.eventos.editar();
            }
        },
        reload: function () {
            configDTDocumento.tiposDocumentos = [];
            if (configDTDocumento.objecto == null || configDTDocumento.objecto == "" || configDTDocumento == undefined) {
                configDTDocumento.objecto = $tblDocumento.DataTable(configDTDocumento.opciones);
                configDTDocumento.eventos.init();
            }
            else {
                configDTDocumento.objecto.ajax.reload();
            }
        },
    }

    var configDTDireccion = {
        objecto: null,
        tienePrincipal: false,
        tipoDirecciones: [],
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Direccion/listarDireccion",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Dirección",
                    data: "gdtdrccn",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTDireccion.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Dirección",
                    data: null,
                    orderable: false,
                    width: '20%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<div class="row text-left">
                                        <div class="col-12 text-break">${data.gdtvia + " " + (data.via ? data.via + " " : "") + (data.nvia ? "# " + data.nvia + " - " : "") + (data.gdtdzna ? data.gdtdzna + " " : "") + (data.zna ? data.zna + " - " : "") + (data.nintrr ? "INTERIOR " + data.nintrr + " - " : "") + data.rfrncia}</div>
                                        <div class="col-12 font-weight-bold">${data.cubgeo}</div>
                                    </div>`;
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '90px',
                    class: "text-center",
                    render: function (data) {
                        configDTDireccion.tipoDirecciones.push(data.igdtdrccn);

                        var tpm = "";
                        if ($accesoEditarDireccion.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlDireccion" data-dir="${data.igdtdrccn}" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarDireccion.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTDireccion.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesDireccion.eliminarDireccion(id);
                })
            },
            editar: function () {
                configDTDireccion.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    var dir = $(this).data("dir");
                    $modalTituloDireccion.text("Editar Dirección");
                    $hfactionDireccion.val("E");
                    $cboTDireccion.empty();
                    $cboTDireccion.LlenarSelectGDFiltrado("GDTDRCCN", configDTDireccion.tipoDirecciones, dir);
                    if ($cboTPersonaNJ.val() == 1) {
                        $cboTDireccion.children('option[value="3"]').hide();
                    } else {
                        $cboTDireccion.children('option[value="3"]').show();
                        $cboTDireccion.val("3").trigger("change")
                    }
                    funcionesDireccion.obtenerDireccion(id);
                })
            },
            init: function () {
                configDTDireccion.eventos.eliminar();
                configDTDireccion.eventos.editar();
            }
        },
        reload: function () {
            configDTDireccion.tipoDirecciones = [];
            if (configDTDireccion.objecto == null || configDTDireccion.objecto == "" || configDTDireccion.objecto == undefined) {
                configDTDireccion.objecto = $tblDireccion.DataTable(configDTDireccion.opciones);
                configDTDireccion.eventos.init()
            } else {
                configDTDireccion.objecto.ajax.reload();
            }
        },

    }

    var configDTTelefono = {
        objecto: null,
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Telefono/listarTelefono",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Teléfono",
                    data: "gdttlfno",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Departamento",
                    data: "cubgeo",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Teléfono",
                    data: "ntlfno",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Observación",
                    data: "obsrvcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTTelefono.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "F. Estado",
                    data: "festdo", width: '10%',
                    orderable: false,
                    class: "text-center d-none"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarTelefono.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlTelefono" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarTelefono.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTTelefono.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesTelefono.eliminarTelefono(id);
                })
            },
            editar: function () {
                configDTTelefono.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTituloTelefono.text("Editar Telefono");
                    $hfactionTelefono.val("E");
                    funcionesTelefono.obtenerTelefono(id);
                })
            },
            init: function () {
                configDTTelefono.eventos.eliminar();
                configDTTelefono.eventos.editar();
            }
        },
        reload: function () {
            if (configDTTelefono.objecto == null || configDTTelefono.objecto == "" || configDTTelefono.objecto == undefined) {
                configDTTelefono.objecto = $tbltelefono.DataTable(configDTTelefono.opciones);
                configDTTelefono.eventos.init();
            } else { configDTTelefono.objecto.ajax.reload(); }
        },

    }

    var configDTCorreo = {
        objecto: null,
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Correo/listarCorreo",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    data.id = "";
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Correo",
                    data: "gdtcrreo",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Correo",
                    data: "ccrreo",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Predeterminada",
                    data: null,
                    width: '10%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if (data.fprncpl == "1") {
                            tpm += `SI`;
                            configDTCorreo.tienePrincipal = true;
                        }
                        else {
                            tpm += `NO`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarCorreo.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlCorreo" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarCorreo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                            //tpm += ` <button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        } return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTCorreo.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesCorreo.eliminarCorreo(id);
                })
            },
            editar: function () {
                configDTCorreo.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTituloCorreo.text("Editar Correo");
                    $hfactionCorreo.val("E");
                    funcionesCorreo.obtenerCorreo(id);
                })
            },
            init: function () {
                configDTCorreo.eventos.eliminar();
                configDTCorreo.eventos.editar();
            }
        },
        reload: function () {
            if (configDTCorreo.objecto == null || configDTCorreo.objecto == "" || configDTCorreo.objecto == undefined) {
                configDTCorreo.objecto = $tblCorreo.DataTable(configDTCorreo.opciones);
                configDTCorreo.eventos.init();

            } else { configDTCorreo.objecto.ajax.reload(); }
        },
    }

    var configDtArchivo = {
        objeto: null,
        tienePrincipal: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/archivo/listarArchivo",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Nombre Archivo",
                    data: "nmbrarchv",
                    orderable: false,
                    className: "text-center",
                    width: '80%'

                },
                {
                    title: "U. Creación",
                    data: "ucrcn",
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Creación",
                    data: "cFCRCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoDescargarArchivo.val() == "True") {
                            tpm += ` <button type="button" class="btn btn-success btn-xs btn-descargar" data-id="${data.id}" title="Descargar Archivo"><span><i class="la la-download"></i></span></button>`;
                        }
                        if ($accesoEliminarArchivo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            descargar: function () {
                configDtArchivo.objeto.on("click", ".btn-descargar", function () {
                    var id = $(this).data("id");
                    funcionesArchivo.descargarArchivo(id);
                })
            },
            eliminar: function () {
                configDtArchivo.objeto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    funcionesArchivo.eliminarArchivo(id);
                })
            },
            init: function () {
                configDtArchivo.eventos.eliminar();
                configDtArchivo.eventos.descargar();
            }
        },
        reload: function () {
            if (configDtArchivo.objeto == null || configDtArchivo.objeto == "" || configDtArchivo.objeto == undefined) {
                configDtArchivo.objeto = $dtArchivo.DataTable(configDtArchivo.opciones);
                configDtArchivo.eventos.init();
            } else { configDtArchivo.objeto.ajax.reload(); }
        }
    }

    var configDtColaboradores = {
        objeto: null,
        opciones: {
            filter: true,
            ajax: {
                dataType: "JSON",
                url: "/persona/listar-colaboradores",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idprsna = $hfCodPersona.val();
                    //data.id = "";
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "Documento",
                    data: "ndcmnto",
                    width: '70%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "Nombres y Apellidos",
                    data: "datos",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "U. Edición",
                    data: "uedcn",
                    width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        reload: function () {
            if (configDtColaboradores.objeto == null || configDtColaboradores.objeto == "" || configDtColaboradores.objeto == undefined) {
                configDtColaboradores.objeto = $dtColaboradores.DataTable(configDtColaboradores.opciones);
                /*                configDtColaboradores.eventos.init();*/
            } else {
                configDtColaboradores.objeto.ajax.reload();
            }
        }
    }

    var configDTSusalud = {
        objeto: null,
        opciones: {
            bAutoWidth: false,
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/lista-susalud",
                timeout: 60000,
                type: "GET",
                data: function (data) {
                    //delete data.columns;
                    data.tiDocumento = 1;
                    data.nuDocumento = $txtSSLDNDocumento.val();
                },
            },
            paging: false,
            ordering: false,
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    data: null,
                    title: "Nombres y Apellidos",
                    class: "d-none",
                    render: function (data) {
                        tpm = data.noPersona + ' ' + data.apPaterno + ' ' + data.apMaterno;
                        return tpm;
                    }
                },
                {
                    data: "apPaterno",
                    title: "Apellido Paterno",
                    class: "text-center"
                },
                {
                    data: "apMaterno",
                    title: "Apellido Materno",
                    class: "text-center"
                },
                {
                    data: "noPersona",
                    title: "Nombres",
                    class: "text-center"
                },
                {
                    data: null,
                    title: "Fecha Nacimiento",
                    class: "text-center",
                    render: function (data) {
                        tpm = data.feNacimiento.substr(0, 10);
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        eventos: {
            selectRow: function () {
                $dtPersonaSusalud.on('click', 'tbody tr', function () {
                    var data = configDTSusalud.objeto.row(this).data();
                    EntidadSusalud = new Object();
                    EntidadSusalud = data;
                    if (typeof (EntidadSusalud) != 'undefined') {
                        funcionesPersona.obtenerPersonaSusalud(EntidadSusalud);
                    }
                })
            },
            init: function () {
                configDTSusalud.eventos.selectRow();
            }
        },
        reload: function () {
            $txtSSLDNDocumento.parent().find("label").remove();
            if ($txtSSLDNDocumento.val()) {
                $txtSSLDNDocumento.removeClass("error");
                if (configDTSusalud.objeto == null || configDTSusalud.objeto == "" || configDTSusalud.objeto == undefined) {
                    configDTSusalud.objeto = $dtPersonaSusalud.DataTable(configDTSusalud.opciones);
                    configDTSusalud.eventos.init();
                }
                else {
                    configDTSusalud.objeto.ajax.reload();
                }
            } else {
                $txtSSLDNDocumento.addClass("error");
                $txtSSLDNDocumento.after('<label id="txtSSLD-NDocumento-error" class="error">Campo requerido.</label>')
            }
        }
    }       //falta

    var configDtHomonimos = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Persona/lista-homonimos",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.gddcmnto= $cboPNTDocumento.val().trim();
                    data.ndcmnto= $txtPNDocumento.val().trim();
                    data.amtrno= $txtAMaterno.val().trim();
                    data.aptrno= $txtAPaterno.val().trim();
                    data.pnmbre= $txtPNombre.val().trim();
                    data.snmbre= $txtSNombre.val().trim();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto",
                    width: '5%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nombres y Apellidos",
                    data: "datos",
                    width: '70%',
                    orderable: false,
                    class: "text-left"
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '15%',
                    class: "text-center d-none",
                    render: function (data) {
                        var tpm = "";

                        tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlArchivo" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;

                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;

                        //tpm += `<button type="button" class="btn btn-danger btn-xs btn-delete" data-id="${data.id}" title="Desactivar"><span><i class="la la-trash"></i></span></button>`;
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            selectRow: function () {
                $dtHomonimo.on('click', 'tbody tr', function () {
                    var data = configDtHomonimos.objeto.row(this).data();
                    EntidadSusalud = new Object();
                    EntidadSusalud = data;
                    if (data.id > 0) {
                        Swal.fire({
                            title: "¿Quiere cargar los datos del registro?",
                            icon: "info",
                            showCancelButton: true,
                            confirmButtonText: "Si, cargar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $hfCodPersona.val(data.id);
                                funcionesPersona.obtenerPersona(data.id);
                                $mdlHomonimos.modal('hide');

                            }
                        });
                    }
                })
            },
            count: function () {
                configDtHomonimos.objeto.on('draw', function () {
                    var info = configDtHomonimos.objeto.page.info();
                    if (info.recordsTotal > 0) {
                        $mdlHomonimos.modal('show')
                    }
                })
            },
            init: function () {
                configDtHomonimos.eventos.selectRow();
                configDtHomonimos.eventos.count();
            }
        },
        reload: function () {
            if (configDtHomonimos.objeto == null || configDtHomonimos.objeto == "" || configDtHomonimos.objeto == undefined) {
                configDtHomonimos.objeto = $dtHomonimo.DataTable(configDtHomonimos.opciones);
                configDtHomonimos.eventos.init();
            } else { configDtHomonimos.objeto.ajax.reload(); }
        }
    }
    //#endregion -----------------------------------------------  

    //#region ------------------- CONFIG FORMULARIO ------------------
    var configFormPersona = {
        objeto: $FormularioPersona.validate({
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                cboTPersonaNJ: {
                    required: true,
                },
                cboTDocumento: {
                    required: true,
                },
                //checkContract: {
                //    required: true,
                //},
                txtAPaterno: {
                    required: true,
                    maxlength: 22
                },
                txtAMaterno: {
                    required: false,
                },
                txtPNombre: {
                    required: true,
                },
                txtSNombre: {
                    required: false,
                },
                cboECivil: {
                    required: true,
                },
                cboSexo: {
                    required: true,
                },
                txtFNacimiento: {
                    required: true,
                },
                cboNacionalidad: {
                    required: true,
                },
                cboPDepartamento: {
                    required: true,
                },
                cboPProvincia: {
                    required: true,
                },
                cboDistrito: {
                    required: true,
                },
                txtRSocial: {
                    required: true,
                },
                cboPJTDocumento: {
                    required: true,
                },
                txtPJNDocumento: {
                    required: true,
                    minlength: 11
                },
                cboPJTSociedad: {
                    required: true,
                },
                cboPJTEmpresa: {
                    required: false,
                },
                txtPJFNacimiento: {
                    required: false,
                },
            }
        }),
        homonimiaPN: {
            options: {
                class: 'homo',
                elemcontent: $FormularioPersona
            },
            init: function () {
                eventosIncrustadosPersona.Homonimia(configFormPersona.homonimiaPN.options);
            }
        },
        eventos: {
            reset: function () {
                configFormPersona.objeto.resetForm();
            }
        },
    }
    //#endregion -----------------------------------------------------

    //#region ------------------- CONFIG MODAL ------------------
    var configModalDocumento = {
        form: {
            objeto: $FormularioDocumento.validate({
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    cboDocumento: {
                        required: true
                    },
                    txtNDocumento: {
                        required: true
                    },
                    txtFInscripcion: {
                        required: false
                    },
                    txtFVncmt: {
                        required: false
                    },
                    cboPrincipalDocumento: {
                        required: true
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalDocumento.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlDocumento.on("hidden.bs.modal", function () {
                    configModalDocumento.eventos.reset();
                    $FormularioDocumento.find(".msg").text("");
                })
            },
            onShow: function () {
                $mdlDocumento.on("shown.bs.modal", function () {
                    var esPeruano = $cboNacionalidad.val() == '51';
                    if (esPeruano) {
                        $cboDocumento.children('option[value="1"]').show();
                    } else {
                        $cboDocumento.children('option[value="1"]').hide();
                    }
                    $FormularioDocumento.find(":input").attr("disabled", false);
                    if ($hfaction.val() == "N") {
                        $FormularioDocumento.AgregarCamposDefectoAuditoria();
                        $FormularioDocumento.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalDocumento.form.eventos.reset();
                $FormularioDocumento.trigger("reset");
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalDireccion = {
        form: {
            objeto: $FormularioDireccion.validate({
                rules: {
                    cboTDireccion: {
                        required: true,
                    },
                    cboTVia: {
                        required: true,
                    },
                    txtNroVia: {
                        required: true,
                    },
                    cboTipoZona: {
                        required: false,
                    },
                    cboPrincipalDireccion: {
                        required: true,
                    },
                    Zona: {
                        required: false,
                    },
                    cboDepartamentoDirec: {
                        required: true,
                    },
                    cboProvinciaDirec: {
                        required: true,
                    },
                    cubgeo: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalDireccion.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlDireccion.on("hidden.bs.modal", function () {
                    configModalDireccion.eventos.reset();
                })
            },
            onShow: function () {
                $mdlDireccion.on("shown.bs.modal", function () {
                    $FormularioDireccion.find(":input").attr("disabled", false);
                    if ($hfactionDireccion.val() == "N") {
                        $FormularioDireccion.AgregarCamposDefectoAuditoria();
                        $FormularioDireccion.DeshabilitarCamposAuditoria();
                        $cboProvinciaDirec.val('').text('Seleccione').attr('disabled', true);
                        $cboDistritoDirec.val('').text('Seleccione').attr('disabled', true);
                    }
                })
            },
            reset: function () {
                configModalDireccion.form.eventos.reset();
                $FormularioDireccion.trigger("reset");
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalTelefono = {
        form: {
            objeto: $FormularioTelefono.validate({
                rules: {
                    cboTTelefono: {
                        required: true,
                    },
                    txtTelefono: {
                        required: true,
                    },
                    txtNroVia: {
                        required: true,
                    },
                    cboPrincipalTelefono: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {

                    configModalTelefono.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdltelefono.on("hidden.bs.modal", function () {
                    configModalTelefono.eventos.reset();
                })
            },
            onShow: function () {
                $mdltelefono.on("shown.bs.modal", function () {
                    $FormularioTelefono.find(":input").attr("disabled", false);
                    if ($hfactionTelefono.val() == "N") {
                        $FormularioTelefono.AgregarCamposDefectoAuditoria();
                        $FormularioTelefono.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalTelefono.form.eventos.reset();
                $FormularioTelefono.trigger("reset");
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalCorreo = {
        form: {
            objeto: $FormularioCorreo.validate({
                rules: {
                    cboTCorreo: {
                        required: true,
                    },
                    txtCorreo: {
                        required: true,
                    },
                    cboPrincipalCorreo: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalCorreo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlCorreo.on("hidden.bs.modal", function () {
                    configModalCorreo.eventos.reset();
                })
            },
            onShow: function () {
                $mdlCorreo.on("shown.bs.modal", function () {
                    $FormularioCorreo.find(":input").attr("disabled", false);
                    if ($hfactionCorreo.val() == "N") {
                        $FormularioCorreo.AgregarCamposDefectoAuditoria();
                        $FormularioCorreo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalCorreo.form.eventos.reset();
                $FormularioCorreo.trigger("reset");
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }

    var configModalArchivo = {
        form: {
            objeto: $FormularioArchivo.validate({
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    nmbrarchv: {
                        required: true,
                    },
                    patharchv: {
                        required: true,
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalArchivo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlArchivo.on("hidden.bs.modal", function () {
                    configModalArchivo.eventos.reset();
                })
            },
            onShow: function () {
                $mdlArchivo.on("shown.bs.modal", function () {
                    $FormularioArchivo.find(":input").attr("disabled", false);
                    if ($hfactionArchivo.val() == "N") {
                        $FormularioArchivo.AgregarCamposDefectoAuditoria();
                        $FormularioArchivo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalArchivo.form.eventos.reset();
                $FormularioArchivo.trigger("reset");
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }
    //#endregion -----------------------------------------------------

    var configDatePickers = {
        opcStandar: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        validarMenorEdad: function (fecNac) {
            var factual = new Date();//fecha Hoy
            var annioActual = parseInt(factual.getFullYear());
            var mesActual = parseInt(factual.getMonth()) + 1;
            var diaActual = parseInt(factual.getDate());
            var hace18 = new Date(annioActual - 18, mesActual - 1, diaActual);
            var hace18str = hace18.getDate() + "/" + hace18.getMonth() + "/" + hace18.getFullYear();

            return mayorFechaActual(fecNac,hace18str);
        },
        fechaNacimiento: function () {
            $txtFNacimiento.on("change", function () {
                var fecNac = $(this).val();
                $txtFNacimiento.removeClass("error");
                $txtFNacimiento.parent().find('.error').remove();
                if (mayorFechaActual(fecNac)) {
                    $txtFNacimiento.addClass("error");
                    $txtFNacimiento.parent().append('<label id="txtFNacimiento-error" class="error" for="txtFNacimiento">La fecha no puede ser mayor a la de hoy.</label>');
                    return;
                }

                if (configDatePickers.validarMenorEdad(fecNac)) {
                    $txtFNacimiento.addClass("error");
                    $txtFNacimiento.parent().append('<label id="txtFNacimiento-error" class="error" for="txtFNacimiento">El trabajador no puede ser menor de edad.</label>');
                    return;
                }
            });
        },
        fechaInicioAct: function () {
            $txtPJFNacimiento.on("change", function () {
                var fecNac = $(this).val();
                $txtPJFNacimiento.removeClass("error");
                $txtPJFNacimiento.parent().find('.error').remove();
                if (mayorFechaActual(fecNac)) {
                    $txtPJFNacimiento.addClass("error");
                    $txtPJFNacimiento.parent().append('<label id="txtPJFNacimiento-error" class="error" for="txtPJFNacimiento">La fecha no puede ser mayor a la de hoy.</label>');
                    return;
                }
            });
        },
        init: function () {
            //DP PERSONA
            $txtFNacimiento.datepicker(configDatePickers.opcStandar);
            $txtPJFNacimiento.datepicker(configDatePickers.opcStandar);
            $txtFInscripcion.datepicker(configDatePickers.opcStandar);
            $txtFVncmt.datepicker(configDatePickers.opcStandar);

            this.fechaNacimiento();
            this.fechaInicioAct();
        }
    }

    var configCboTipoDocumentos = {
        optDocumento: {
            obj: $txtNDocumento[0].id,
        },
        optDocSusalud: {
            obj: $txtSSLDNDocumento[0].id,
        },
        optDocPj: {
            obj: $txtPJNDocumento[0].id,
        },
        optDocLstPersona: {
            obj: $txtPNDocumento[0].id,
        },
        init: function () {
            $cboDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocumento);
            $cboSSLDTDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocSusalud);
            $cboPJTDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocPj);
            $cboPNTDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocLstPersona);
        }
    }

    var configUbigeos = {
        f_nacionalidad: function (obj) {
            var Pais = $(obj).val();
            var chkPais = $chkPaisNac;
            var optUbigeos;
            $cboPDistrito.children('option[value=""]').text("Seleccione").val("");
            $cboPDistrito.prop("disabled", false);
            if (chkPais.is(":checked")) {
                optUbigeos = configUbigeos.ubigeoPersona.optPaisNacimiento;
            }
            else {
                $PaisNac.addClass("d-none");
                $cboPaisNac.val(Pais);
                optUbigeos = configUbigeos.ubigeoPersona.optionNacionalidad;
            }
            if (Pais != 51) {
                $cboPDistrito.parent('div').find('.distrito-label').text('Estado');
                $cboPDepartamento.parent('div').addClass('d-none');
                $cboPProvincia.parent('div').addClass('d-none');
                $cboPNTDocumento.children('option[value="1"]').hide();
                optUbigeos.codPais = Pais;
                optUbigeos.codProvincia = '';
                LlenarSelectDistrito(optUbigeos);
            }
            else {
                $cboPaisNac.attr('disabled', false);
                $chkPaisNac.attr('disabled', false);
                $cboPNTDocumento.children('option[value="1"]').show();
                if (!chkPais.is(":checked")) {
                    $cboPaisNac.attr('disabled', true);
                    $cboPaisNac.attr('disabled', true);
                }
                $PaisNac.removeClass('d-none');
                $cboPDistrito.parent('div').find('.distrito-label').text('Distrito');
                $cboPDepartamento.parent('div').removeClass('d-none');
                $cboPProvincia.parent('div').removeClass('d-none');

                optUbigeos.codPais = '';
                optUbigeos.refresh = true;
                cargarUbigeo(optUbigeos);//2
            }
        },
        f_inicioUbigeo: function () {
            $PaisNac.addClass("d-none");
            $cboPDistrito.parent('div').find('.distrito-label').text('Distrito');
            $cboPDistrito.append($("<option />").val('').text("Seleccione"));
            $cboPDistrito.val("");
            $cboPDistrito.attr("disabled", true);
            $cboPDepartamento.parent('div').removeClass('d-none');
            $cboPProvincia.append($("<option />").val('').text("Seleccione"));
            $cboPProvincia.val("");
            $cboPProvincia.attr("disabled", true);
            $cboPProvincia.parent('div').removeClass('d-none');
        },
        ubigeoPersona: {
            optPaisNacimiento: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '',
                controles: {
                    departamento: $cboPDepartamento,
                    provincia: $cboPProvincia,
                    distrito: $cboPDistrito,
                    nacionalidad: $cboPaisNac
                }
            },
            optionNacionalidad: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '',
                controles: {
                    departamento: $cboPDepartamento,
                    provincia: $cboPProvincia,
                    distrito: $cboPDistrito,
                    nacionalidad: $cboNacionalidad
                },
            },
            cargarUbigeosPersona: function (datosUbigeo) {
                var opt = {}
                opt = configUbigeos.ubigeoPersona.optionNacionalidad
                if (datosUbigeo.ncnldd == '51') {
                    opt = null;
                    if (datosUbigeo.cpncmnto == '51') {
                        opt = configUbigeos.ubigeoPersona.optionNacionalidad;
                        opt.codPais = datosUbigeo.ncnldd == '51' ? '169' : datosUbigeo.ncnldd;;
                    } else {
                        opt = configUbigeos.ubigeoPersona.optPaisNacimiento;
                        opt.codPais = datosUbigeo.cpncmnto;
                    }
                } else {
                    opt.codPais = datosUbigeo.cpncmnto == '51' ? '169' : datosUbigeo.cpncmnto;
                }
                opt.codUbigeo = datosUbigeo.cubgeo
                cargarUbigeos(opt);
                //#region funcionalidad mostrar elementos Ubigeo
                if (datosUbigeo.ncnldd == '51') {
                    $PaisNac.removeClass('d-none');
                    if (datosUbigeo.cpncmnto == '51') {
                        $chkPaisNac.prop('checked', false);
                        $cboPaisNac.attr('disabled', true);
                        $cboPDistrito.parent('div').find('.distrito-label').text('Distrito');
                        $cboPDepartamento.parent('div').removeClass('d-none');
                        $cboPProvincia.parent('div').removeClass('d-none');
                    }
                    else {
                        $chkPaisNac.prop('checked', true);
                        $cboPaisNac.attr('disabled', false);
                        $cboPDistrito.parent('div').find('.distrito-label').text('Estado');
                        $cboPDepartamento.parent('div').addClass('d-none');
                        $cboPProvincia.parent('div').addClass('d-none');
                    }
                }
                else {
                    $PaisNac.addClass('d-none');
                    $cboPDistrito.parent('div').find('.distrito-label').text('Estado');
                    $cboPDepartamento.parent('div').addClass('d-none');
                    $cboPProvincia.parent('div').addClass('d-none');
                }
                $cboNacionalidad.attr("disabled", false);
                
                //#endregion
            },
            init: function () {
                var opt = {};
                opt = configUbigeos.ubigeoPersona.optionNacionalidad;
                opt.codPais = '169';
                opt.refresh = true;
                //cargarUbigeo(opt);///1
                //configUbigeos.f_nacionalidad($cboNacionalidad)
                configUbigeos.f_inicioUbigeo();
            }
        },
        ubigeoDireccion: {
            optUbigeo: {
                codDepartamento: '',
                codProvincia: '',
                codUbigeo: '',
                codPais: '169',
                controles: {
                    departamento: $cboDepartamentoDirec,
                    provincia: $cboProvinciaDirec,
                    distrito: $cboDistritoDirec,
                    nacionalidad: '',
                }
            },
            cargarUbigeo: function (datosUbigeo) {
                var opt = {};
                opt = configUbigeos.ubigeoDireccion.optUbigeo;
                opt.codUbigeo = datosUbigeo.cubgeo;
                cargarUbigeos(opt);
            },
            init: function () {
                var opt = {};
                opt = configUbigeos.ubigeoDireccion.optUbigeo;
                opt.refresh = true;
                cargarUbigeo(opt);
            }
        },
        init: function () {

        }
    }

    var funcionesPersona = {
        initTipoPersona: function (valor) {
            if (!$hfCodPersona.val()) {
                $FormularioPersona.trigger("reset");
                $cboTPersonaNJ.val(valor);
            }
            if (valor == 1) {
                $cnt_pNatural.show();
                $cnt_pJuridica.hide();
                configFormPersona.eventos.reset();
                $tab_Colaboradores.addClass("d-none");
                $btnConsultaSusalud.show();
                $btnCreateWorker.show();
                $cnt_pNaturalAgregando.show();
            } else {
                $cnt_pNatural.hide();
                $cnt_pJuridica.show();
                configFormPersona.eventos.reset();
                $tab_Colaboradores.removeClass("d-none");
                $btnConsultaSusalud.hide();
                $cboPJTDocumento.val('8').change();
                $cboPJTDocumento.attr('disabled', true);
                $btnCreateWorker.hide();
            }

        },
        NuevoPersona: function () {
            $tab_Documento.removeClass("disabled");
            $tab_Direccion.addClass("disabled");
            $tab_Telefono.addClass("disabled");
            $tab_CuentaCorreo.addClass("disabled");
            $tab_Archivos.addClass("disabled");
            $tab_Colaboradores.addClass("disabled");
            $tab_DatosGenerales.removeClass("disabled");
            $FormularioPersona.trigger("reset");
            $cboTPersonaNJ.attr("disabled", false);
            $txtnuDocumento.show();
            $btnConsultaSusalud.show();
            $btnCreateWorker.show();
            $btnVerTrabajador.hide();
            $lblTDocumento.show();
            $lblnuDocumento.show();
            $tab_DatosGenerales.click();
            $tab_Documento.click();
            $cboTPersonaNJ.val(1);
            funcionesPersona.initTipoPersona($cboTPersonaNJ.val()); //NO PASA EL VALOR
            //$cnt_pNaturalAgregando.hide();
            configDTDocumento.reload();
            funcionesPersona.validarNuevoBotones();
            $FormularioPersona.AgregarCamposDefectoAuditoria();
            $FormularioPersona.DeshabilitarCamposAuditoria();
            configUbigeos.ubigeoPersona.init();
            $cboNacionalidad.val('51').trigger("change");
            $cboPDistrito.attr("disabled", true);
            $cboPProvincia.attr("disabled", true);
        },
        obtenerPersona: function (id) {
            funcionesPersona.obtenerDatosPersona(id);
            $cboTPersonaNJ.attr("disabled", true);
            $tab_Documento.removeClass("disabled");
            $tab_Direccion.removeClass("disabled");
            $tab_Telefono.removeClass("disabled");
            $tab_CuentaCorreo.removeClass("disabled");
            $tab_Archivos.removeClass("disabled");
            $tab_Colaboradores.removeClass("disabled");
        },
        guardarPersona: function () {
            if ($FormularioPersona.valid()) {
                if (!$hfCodPersona.val()) {
                    funcionesPersona.insertarPersona();
                } else {
                    funcionesPersona.actualizarPersona();
                }
            }
        },
        insertarPersona: function () {
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadPersona = new Object();
                EntidadPersona.gdtprsna = $cboTPersonaNJ.val();
                EntidadPersona.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();
                var fecNac = "";
                if ($cboTPersonaNJ.val() == "1") {
                    DatosPersona.DatosNatural();
                    fecNac = $txtFNacimiento.val();
                } else {
                    DatosPersona.DatosJuridica();
                    fecNac = $txtPJFNacimiento.val()
                }
                if (!mayorFechaActual(fecNac) && !configDatePickers.validarMenorEdad(fecNac)) {
                    if ($cboTPersonaNJ.val() == "1") {
                        EntidadPersona.gddcmnto = $cboPNTDocumento.val();
                        EntidadPersona.ndcmnto = $txtPNDocumento.val();
                    } else {
                        $cboPJTDocumento.attr("disabled", false);
                        EntidadPersona.gddcmnto = $cboPJTDocumento.val();
                        $cboPJTDocumento.attr("disabled", true);
                        EntidadPersona.ndcmnto = $txtPJNDocumento.val();
                    }
                    //
                    //cropper.basic.croppie('result', 'blob').then(function (blob) {
                    //    EntidadPersona.imgnprsna = blob;
                    //});
                    var llamarLlenarFormData = $.Deferred();
                    llamarLlenarFormData.resolve(llenarFormData())
                    /* var frmData = llenarFormData(frmData);*/
                    function llenarFormData() {
                        var frmData = new FormData();
                        var elementosDeEntidad = Object.getOwnPropertyNames(EntidadPersona);

                        elementosDeEntidad.forEach((item) => {
                            frmData.append(item.toUpperCase(), EntidadPersona[item]);
                        });

                        //EntidadPersona.imgnprsna = blob;
                        frmData.append("IMGNPRSNA", cropper.blob);
                        return frmData;
                    }
                    $.when(
                        llamarLlenarFormData
                    ).done(function (x) {
                        $.ajax({
                            type: 'post',
                            url: "/Persona/insertarPersonas",
                            data: x,
                            processData: false,
                            contentType: false
                        }).done(function (idPersona) {
                            //$.post("/Persona/insertarPersonas", EntidadPersona).done(function (idPersona) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Se registro satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                RedirectWithSubfolder("/Persona/editar/" + idPersona)
                            });
                            $FormularioPersona.find(".msg").text("");
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al insertar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    });
                } else {
                    $txtFNacimiento.change();
                }

                $btnGrabarPersona.attr("disabled", false);
            }
        },
        actualizarPersona: function () {
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadPersona = new Object();
                EntidadPersona.id = $hfCodPersona.val();
                EntidadPersona.gdtprsna = $cboTPersonaNJ.val();
                EntidadPersona.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();
                var fecNac = "";
                if ($cboTPersonaNJ.val() == "1") {
                    DatosPersona.DatosNatural();
                    fecNac = $txtFNacimiento.val();
                } else {
                    DatosPersona.DatosJuridica();
                    fecNac = $txtPJFNacimiento.val();
                }
                if (!mayorFechaActual(fecNac) && !configDatePickers.validarMenorEdad(fecNac)) {
                    //
                    var llamarLlenarFormData = $.Deferred();
                    llamarLlenarFormData.resolve(llenarFormData())
                    /* var frmData = llenarFormData(frmData);*/
                    function llenarFormData() {
                        var frmData = new FormData();
                        var elementosDeEntidad = Object.getOwnPropertyNames(EntidadPersona);

                        elementosDeEntidad.forEach((item) => {
                            frmData.append(item.toUpperCase(), EntidadPersona[item]);
                        });

                        //EntidadPersona.imgnprsna = blob;
                        frmData.append("IMGNPRSNA", cropper.blob);
                        return frmData;
                    }
                    $.when(
                        llamarLlenarFormData
                    ).done(function (x) {
                        $.ajax({
                            type: 'post',
                            url: "/Persona/actualizarPersona",
                            data: x,
                            processData: false,
                            contentType: false
                        }).done(function (msj) {
                            //$.post("/Persona/actualizarPersona", EntidadPersona).done(function (msj) {
                            Swal.fire({
                                icon: msj ? "info" : "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: msj ? msj : "Se actualizo satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTDocumento.reload();
                                funcionesPersona.obtenerPersona($hfCodPersona.val());
                            });
                            $FormularioPersona.find(".msg").text("");
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    });
                } else {
                    $txtFNacimiento.change();
                }
                $btnGrabarPersona.attr("disabled", false);
            }
        },
        validartipodoc: function () {
            if ($cboTDocumento.val() == 8) {
                $txtRSocial.attr("disabled", false);
                $txtAPaterno.attr("disabled", true);
                $txtAMaterno.attr("disabled", true);
                $txtACasada.attr("disabled", true);
                $txtPNombre.attr("disabled", true);
                $txtSNombre.attr("disabled", true);
            } else {
                $txtRSocial.attr("disabled", true);
                $txtAPaterno.attr("disabled", false);
                $txtAMaterno.attr("disabled", false);
                $txtACasada.attr("disabled", false);
                $txtPNombre.attr("disabled", false);
                $txtSNombre.attr("disabled", false);
            }
        },
        ConsultarSusalud: function () {
            configDTSusalud.reload();
        },
        obtenerPersonaSusalud: function (entidad) {
            $cboPNTDocumento.val(entidad.tiDocumento).change();
            $txtPNDocumento.val(entidad.nuDocumento);

            $txtAPaterno.val(entidad.apPaterno);
            $txtAMaterno.val(entidad.apMaterno);
            $txtACasada.val(entidad.apCasada);
            $txtPNombre.val(entidad.noPersona.split(" ")[0]);
            $txtSNombre.val(entidad.noPersona.split(" ")[1]);
            $cboSexo.val(entidad.deSexo);
            $txtFNacimiento.datepicker('setDate', entidad.feNacimiento.substr(0, 10));
            $txtFNacimiento.val(entidad.feNacimiento.substr(0, 10)).change();
            $mdlSusalud.modal('hide');
        },
        validarNuevoBotones: function () {
            if ($hfCodPersona.val() == "") {
                $btnCreateWorker.addClass('d-none');
                $btnNuevoDocumento.addClass('d-none');
                $btnNuevoTelefono.addClass('d-none');
                $btnNuevoDireccion.addClass('d-none');
                $btnNuevoCorreo.addClass('d-none');
                $btnNuevoArchivo.addClass('d-none');
            } else {
                if ($cboTPersonaNJ.val() == 2) {
                    $btnNuevoDocumento.addClass('d-none');
                } else {
                    $btnNuevoDocumento.removeClass('d-none');
                }
                $btnCreateWorker.removeClass('d-none');
                $btnNuevoTelefono.removeClass('d-none');
                $btnNuevoDireccion.removeClass('d-none');
                $btnNuevoCorreo.removeClass('d-none');
                $btnNuevoArchivo.removeClass('d-none');
            }
        },
        obtenerDatosPersona: function (codigo) {
            var vpersona = { id: codigo };
            $.get("/Persona/obtenerPersona", vpersona).done(function (data) {
                EntidadPersona = data;
                $FormularioPersona.find("[name='ID']").val(data.id)
                $cboTPersonaNJ.val(EntidadPersona.gdtprsna);
                $checkContract.attr("checked", EntidadPersona.fcntrtsta);
                $txtRSocial.val(EntidadPersona.rscl);
                $txtAPaterno.val(EntidadPersona.aptrno);
                $txtAMaterno.val(EntidadPersona.amtrno);
                $txtPNombre.val(EntidadPersona.pnmbre);
                $txtSNombre.val(EntidadPersona.snmbre);
                $cboNacionalidad.val(EntidadPersona.ncnldd);
                $cboPaisNac.val(EntidadPersona.cpncmnto);
                $cboECivil.val(EntidadPersona.gdecvl);
                $cboSexo.val(EntidadPersona.gdsxo);
                $txtFNacimiento.val(EntidadPersona.fncmnto);
                $txtFNacimiento.datepicker('setDate', EntidadPersona.fncmnto).change();
                $txtPJFNacimiento.val(EntidadPersona.fncmnto);
                $cboPJTSociedad.val(EntidadPersona.gdtescdd);
                $cboPJTEmpresa.val(EntidadPersona.gdtetmno);
                $txtDocumento.val(EntidadPersona.txtnDcmnto);
                $cboPNTDocumento.val(EntidadPersona.tiDocumento);
                $cboPDistrito.val(EntidadPersona.cubgeo).trigger("change");
                if (EntidadPersona.fto) {
                    var imagenpersona = document.getElementById('preview-image');
                    imagenpersona.src = "data:image/*;base64," + EntidadPersona.fto;
                }
                $FormularioPersona.AgregarCamposAuditoria(EntidadPersona);
                configUbigeos.ubigeoPersona.cargarUbigeosPersona(EntidadPersona);
                configDTDocumento.reload();
                configDTCorreo.reload();
                configDTDireccion.reload();
                configDTTelefono.reload();
                configDtArchivo.reload();
                funcionesPersona.initTipoPersona($cboTPersonaNJ.val());
                $btnConsultaSusalud.hide();
                $btnCreateWorker.show();
                $tab_Documento.removeClass("disabled");
                $tab_Direccion.removeClass("disabled");
                $tab_Telefono.removeClass("disabled");
                $tab_CuentaCorreo.removeClass("disabled");
                $tab_Archivos.removeClass("disabled");
                $tab_Colaboradores.removeClass("disabled");
                $tab_Documento.click();
                funcionesPersona.validarNuevoBotones();
                var dataDocJuridica = sXMLToJson(EntidadPersona.mpepE04);
                if (dataDocJuridica && dataDocJuridica.MPEPE04) {
                    if (EntidadPersona.gdtprsna == 2) {
                        $txtPJNDocumento.val(dataDocJuridica.MPEPE04.NDCMNTO + "");
                    } else {
                        $cboPNTDocumento.val(dataDocJuridica.MPEPE04.GDDCMNTO + "").trigger("change");
                        $txtPNDocumento.val(dataDocJuridica.MPEPE04.NDCMNTO + "");
                    }
                } else {
                    $cnt_pNaturalAgregando.hide();
                }
                if (data.idtrbjdr > 0) {
                    $hddIdtrbjdr.val(data.idtrbjdr);
                    $btnCreateWorker.hide();
                } else {
                    $hddIdtrbjdr.val(0);
                    $btnVerTrabajador.hide();
                }
                //ACA DESBLOQUEAR
                $txtAPaterno.attr('disabled', false);
                $txtAMaterno.attr('disabled', false);
                $txtPNombre.attr('disabled', false);
                $txtSNombre.attr('disabled', false);
                $cboECivil.attr('disabled', false);
                $txtFNacimiento.attr('disabled', false);
                $checkContract.attr('disabled', false);
                $cboSexo.attr('disabled', false);
                $txtPNDocumento.attr('disabled', true);


                $cboPJTSociedad.attr('disabled', false);
                $cboPJTEmpresa.attr('disabled', false);
                $txtPJFNacimiento.attr('disabled', false);

            }).fail().always(function () {
                $FormularioPersona.DeshabilitarCamposAuditoria();
            });
        },
        insertarDniPersona: function (idpersona) {
            if ($FormularioPersona.valid()) {
                $btnGrabarPersona.attr("disabled", true);
                EntidadDocumento = new Object();
                EntidadDocumento.gddcmnto = $cboPJTDocumento.val();
                EntidadDocumento.ndcmnto = $txtPJNDocumento.val();
                EntidadDocumento.finscrpcn = "";
                EntidadDocumento.fvncmnto = "";
                EntidadDocumento.fprncpl = "1";
                EntidadDocumento.idprsna = idpersona;
                EntidadDocumento.gdestdo = $FormularioPersona.find("[name='GDESTDO']").val();
                if ((!mayorFechaActual($("#txtFInscripcion").val(), $("#txtFVncmt").val()))
                    || (!mayorFechaActual($("#txtFInscripcion").val()))
                    || (!mayorFechaActual($("#txtFVncmt").val()))) {
                    $.post("/Documento/insertarDocumento", EntidadDocumento).done(function () {
                        //configDTPersona.reload();
                        configDTDocumento.reload();
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarPersona.attr("disabled", false);
                }
            }
        },
        mostrarModalHomonimia: function (valorBool) {
            if (valorBool && $cboPNTDocumento.val() && $txtPNDocumento.val()) {  //
                configDtHomonimos.reload()
            }
        },
        init: function () {
            if ($hfCodPersona.val() == null || $hfCodPersona.val() == "" || $hfCodPersona.val() == undefined) {
                funcionesPersona.NuevoPersona();
            } else {
                //BLOQUEAR INPUTS
                $('input').attr('disabled', true);
                $('select').attr('disabled', true);
                funcionesPersona.obtenerDatosPersona($hfCodPersona.val());
            }
        }
    }

    var funcionesDocumento = { //DOCUMENTOS
        insertarDocumento: function () {//INSERTAR  
            if ($FormularioDocumento.valid()) {
                $btnGrabarDocumento.attr("disabled", true);
                EntidadDocumento.gddcmnto = $cboDocumento.val();
                EntidadDocumento.ndcmnto = $txtNDocumento.val();
                EntidadDocumento.finscrpcn = $txtFInscripcion.val();
                EntidadDocumento.fvncmnto = $txtFVncmt.val();
                EntidadDocumento.fprncpl = $cboPrincipalDocumento.val();
                EntidadDocumento.idprsna = $hfCodPersona.val();
                EntidadDocumento.gdestdo = $FormularioDocumento.find("[name='GDESTDO']").val();
                var con1 = false;
                if ($txtFInscripcion.val()) {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual($("#txtFInscripcion").val(), $("#txtFVncmt").val());
                    } else {
                        con1 = mayorFechaActual($("#txtFInscripcion").val());
                    }
                } else {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual($("#txtFVncmt").val());
                        if (!con1) {
                            con1 = true;
                        }
                    }
                }
                if (!con1) {
                    $.post("/Documento/insertarDocumento", EntidadDocumento).done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDocumento.modal("hide");
                            configDTDocumento.reload();
                        });
                        $FormularioDocumento.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarDocumento.attr("disabled", false);
                    $FormularioDocumento.find(".msg").text("");
                    $FormularioDocumento.find(".msg0").text("");
                }
            }
        },
        actualizarDocumento: function () {
            if ($FormularioDocumento.valid()) {
                $btnGrabarDocumento.attr("disabled", true);
                EntidadDocumento.gddcmnto = $cboDocumento.val();
                EntidadDocumento.ndcmnto = $txtNDocumento.val();
                EntidadDocumento.finscrpcn = $txtFInscripcion.val();
                EntidadDocumento.fvncmnto = $txtFVncmt.val();
                EntidadDocumento.fprncpl = $cboPrincipalDocumento.val();
                EntidadDocumento.idprsna = $hfCodPersona.val();
                EntidadDocumento.gdestdo = $FormularioDocumento.find("[name='GDESTDO']").val();
                var con1 = false;
                if ($txtFInscripcion.val()) {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual($("#txtFInscripcion").val(), $("#txtFVncmt").val());
                    } else {
                        con1 = mayorFechaActual($("#txtFInscripcion").val());
                    }
                } else {
                    if ($txtFVncmt.val()) {
                        con1 = mayorFechaActual($("#txtFVncmt").val());
                        if (!con1) {
                            con1 = true;
                        }
                    }
                }
                if (!con1) {
                    $.post("/Documento/actualizarDocumento", EntidadDocumento).done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDocumento.modal("hide");
                            configDTDocumento.reload();
                        });
                        $FormularioDocumento.find(".msg").text("");
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDocumento.reload();
                    });
                    $btnGrabarDocumento.attr("disabled", false);
                }
            }
        },
        obtenerDocumento: function (idDocumento) {
            var parametro = { id: idDocumento, idprsna: $hfCodPersona.val() };
            $.get("/Documento/obtenerDocumento", parametro)
                .done(function (data) {
                    EntidadDocumento = data;
                    $cboDocumento.val(EntidadDocumento.gddcmnto).trigger("change");
                    $txtNDocumento.val(EntidadDocumento.ndcmnto);
                    $txtFInscripcion.val(EntidadDocumento.finscrpcn);
                    $txtFVncmt.val(EntidadDocumento.fvncmnto);
                    $cboPrincipalDocumento.val(EntidadDocumento.fprncpl);
                    $hfCodDocumento.val(EntidadDocumento.id);
                    $FormularioDocumento.AgregarCamposAuditoria(EntidadDocumento);
                    if ($cboTPersonaNJ.val() == 2) {
                        $cboDocumento.prop('disabled', true);
                    } else {
                        $cboDocumento.prop('disabled', false);
                    }

                })

                .fail().always(function () {
                    $FormularioDocumento.DeshabilitarCamposAuditoria();
                    $cboDocumento.val(EntidadDocumento.gddcmnto).attr("disabled", true);
                });

        },
        eliminarDocumento: function (idDocumento) {
            var parametro = { id: idDocumento, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Documento/eliminarDocumento", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTDocumento.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTDocumento.reload();
                        })
                }
            });
        },
        guardarDocumento: function () {
            if ($hfaction.val() == "N") {
                funcionesDocumento.insertarDocumento();
            }
            else {
                funcionesDocumento.actualizarDocumento();
            }
        },
    }

    var funcionesDireccion = {
        insertarDireccion: function () {
            if ($FormularioDireccion.valid()) {
                $btnGrabarDireccion.attr("disabled", true);
                EntidadDireccion.gdtdrccn = $cboTDireccion.val();
                EntidadDireccion.gdtvia = $cboTVia.val();
                EntidadDireccion.via = $txtVia.val();
                EntidadDireccion.gdtdzna = $cboTipoZona.val();
                EntidadDireccion.nvia = $txtNroVia.val();
                EntidadDireccion.nintrr = $txtInterior.val();
                EntidadDireccion.zna = $txtNroZona.val();
                EntidadDireccion.rfrncia = $txtReferencia.val();
                EntidadDireccion.fprncpl = $cboPrincipalDireccion.val();
                EntidadDireccion.cubgeo = $cboDistritoDirec.val();
                EntidadDireccion.gdestdo = $FormularioDireccion.find("[name='GDESTDO']").val();
                EntidadDireccion.idprsna = $hfCodPersona.val();
                $.post("/Direccion/insertarDireccion", EntidadDireccion).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlDireccion.modal("hide");
                        configDTDireccion.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDTDireccion.reload();
                });
                $btnGrabarDireccion.attr("disabled", false);
            }
        },
        actualizarDireccion: function () {
            if ($FormularioDireccion.valid()) {
                $btnGrabarDireccion.attr("disabled", true);
                EntidadDireccion.id = $hfCodDireccion.val();
                EntidadDireccion.gdtdrccn = $cboTDireccion.val();
                EntidadDireccion.gdtvia = $cboTVia.val();
                EntidadDireccion.via = $txtVia.val();
                EntidadDireccion.gdtdzna = $cboTipoZona.val();
                EntidadDireccion.nvia = $txtNroVia.val();
                EntidadDireccion.nintrr = $txtInterior.val();
                EntidadDireccion.zna = $txtNroZona.val();
                EntidadDireccion.rfrncia = $txtReferencia.val();
                EntidadDireccion.fprncpl = $cboPrincipalDireccion.val();
                EntidadDireccion.cubgeo = $cboDistritoDirec.val();
                EntidadDireccion.gdestdo = $FormularioDireccion.find("[name='GDESTDO']").val();
                EntidadDireccion.idprsna = $hfCodPersona.val();
                $.post("/Direccion/actualizarDireccion", EntidadDireccion)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlDireccion.modal("hide");
                            configDTDireccion.reload();

                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTDireccion.reload();
                    });
                $btnGrabarDireccion.attr("disabled", false);
            }
        },
        obtenerDireccion: function (idDireccion) {
            var parametro = { id: idDireccion, idprsna: $hfCodPersona.val() };
            $.get("/Direccion/obtenerDireccion", parametro)
                .done(function (data) {
                    EntidadDireccion = data;
                    $cboTDireccion.val(EntidadDireccion.gdtdrccn);
                    $cboTVia.val(EntidadDireccion.gdtvia);
                    $txtVia.val(EntidadDireccion.via);
                    $cboTipoZona.val(EntidadDireccion.gdtdzna);
                    $txtNroVia.val(EntidadDireccion.nvia);
                    $txtInterior.val(EntidadDireccion.nintrr);
                    $txtNroZona.val(EntidadDireccion.zna);
                    $txtReferencia.val(EntidadDireccion.rfrncia);
                    $cboPrincipalDireccion.val(EntidadDireccion.fprncpl);
                    configUbigeos.ubigeoDireccion.cargarUbigeo(EntidadDireccion);
                    $hfCodDireccion.val(EntidadDireccion.id);
                    $FormularioDireccion.AgregarCamposAuditoria(EntidadDireccion);

                })
                .fail().always(function () {
                    $FormularioDireccion.find(":input").attr("disabled", false);
                    $FormularioDireccion.DeshabilitarCamposAuditoria();
                    $cboTDireccion.attr("disabled", true);
                });
        },
        eliminarDireccion: function (idDireccion) {
            var parametro = { id: idDireccion, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Direccion/eliminarDireccion", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTDireccion.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTDireccion.reload();
                        })
                }
            });
        },
        guardarDireccion: function () {
            if ($hfactionDireccion.val() == "N") {
                funcionesDireccion.insertarDireccion();
            }
            else {
                funcionesDireccion.actualizarDireccion();
            }

            //$hfactionDireccion.val("");
        },
        selectTipoZona: function () {
            if ($cboTipoZona.val() == 0) {
                $FormularioDireccion.find('.input-zona').attr('disabled', true);
            } else {
                $FormularioDireccion.find('.input-zona').attr('disabled', false);
            }
        },
        inicioModal: function () {
            $cboTipoZona.attr('disabled', false);
            funcionesDireccion.selectTipoZona();
        }
    }

    var funcionesTelefono = {
        insertarTelefono: function () {
            if ($FormularioTelefono.valid()) {
                $btnGrabarTelefono.attr("disabled", true);
                EntidadTelefono.gdttlfno = $cboTTelefono.val();
                EntidadTelefono.cubgeo = $cboDepartamentoTlfno.val();
                EntidadTelefono.ntlfno = $txtTelefono.val();
                EntidadTelefono.obsrvcn = $txtObservacion.val();
                EntidadTelefono.fprncpl = $cboPrincipalTelefono.val();//$cboPrincipal.val(); 
                EntidadTelefono.gdestdo = $FormularioTelefono.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadTelefono.idprsna = $hfCodPersona.val();
                $.post("/Telefono/insertarTelefono", EntidadTelefono)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdltelefono.modal("hide");
                            configDTTelefono.reload();
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTTelefono.reload();
                    });
                $btnGrabarTelefono.attr("disabled", false);
            }
        },
        actualizarTelefono: function () {
            if ($FormularioTelefono.valid()) {
                $btnGrabarTelefono.attr("disabled", true);
                EntidadTelefono.gdttlfno = $cboTTelefono.val();
                EntidadTelefono.cubgeo = $cboDepartamentoTlfno.val();
                EntidadTelefono.ntlfno = $txtTelefono.val();
                EntidadTelefono.obsrvcn = $txtObservacion.val();
                EntidadTelefono.fprncpl = $cboPrincipalTelefono.val();//$cboPrincipal.val();
                EntidadTelefono.gdestdo = $FormularioTelefono.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadTelefono.idprsna = $hfCodPersona.val();
                $.post("/Telefono/actualizarTelefono", EntidadTelefono)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdltelefono.modal("hide");
                            configDTTelefono.reload();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTTelefono.reload();
                    });
                $btnGrabarTelefono.attr("disabled", false);
            }
        },
        obtenerTelefono: function (idTelefono) {
            var parametro = { id: idTelefono, idprsna: $hfCodPersona.val() };
            $.get("/Telefono/obtenerTelefono", parametro)
                .done(function (data) {
                    EntidadTelefono = data;
                    $cboTTelefono.val(EntidadTelefono.gdttlfno).trigger("change");
                    $cboDepartamentoTlfno.val(EntidadTelefono.cubgeo);
                    $txtTelefono.val(EntidadTelefono.ntlfno);
                    $txtObservacion.val(EntidadTelefono.obsrvcn);
                    $cboPrincipalTelefono.val(EntidadTelefono.fprncpl);
                    $hfCodTelefono.val(EntidadTelefono.id);
                    $FormularioTelefono.AgregarCamposAuditoria(EntidadTelefono);
                })
                .fail().always(function () {
                    $FormularioTelefono.DeshabilitarCamposAuditoria();
                });
        },
        eliminarTelefono: function (idTelefono) {
            var parametro = { id: idTelefono, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Telefono/eliminarTelefono", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTTelefono.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTTelefono.reload();
                        })
                }
            });
        },
        guardarTelefono: function () {
            if ($hfactionTelefono.val() == "N") {
                funcionesTelefono.insertarTelefono();
            }
            else {
                funcionesTelefono.actualizarTelefono();
            }
            //$hfactionTelefono.val("");
        }
    }

    var funcionesCorreo = {
        insertarCorreo: function () {
            if ($FormularioCorreo.valid()) {
                $btnGrabarCorreo.attr("disabled", true);
                EntidadCorreo.gdtcrreo = $cboTCorreo.val();
                EntidadCorreo.ccrreo = $txtCorreo.val();
                EntidadCorreo.fprncpl = $cboPrincipalCorreo.val();
                EntidadCorreo.gdestdo = $FormularioCorreo.find("[name='GDESTDO']").val();
                EntidadCorreo.idprsna = $hfCodPersona.val();
                $.post("/Correo/insertarCorreo", EntidadCorreo).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registro satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlCorreo.modal("hide");
                        configDTCorreo.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDTCorreo.reload();
                });
                $btnGrabarCorreo.attr("disabled", false);
            }
        },
        actualizarCorreo: function () {
            if ($FormularioCorreo.valid()) {
                $btnGrabarCorreo.attr("disabled", true);
                EntidadCorreo.gdtcrreo = $cboTCorreo.val();
                EntidadCorreo.ccrreo = $txtCorreo.val();
                EntidadCorreo.fprncpl = $cboPrincipalCorreo.val();
                EntidadCorreo.gdestdo = $FormularioCorreo.find("[name='GDESTDO']").val();//$cboEstado.val();
                EntidadCorreo.idprsna = $hfCodPersona.val();
                $.post("/Correo/actualizarCorreo", EntidadCorreo)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            $mdlCorreo.modal("hide");
                            configDTCorreo.reload();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                        configDTCorreo.reload();
                    });
                $btnGrabarCorreo.attr("disabled", false);
            }
        },
        obtenerCorreo: function (idCorreo) {
            var parametro = { id: idCorreo, idprsna: $hfCodPersona.val() };
            $.get("/Correo/obtenerCorreo", parametro)
                .done(function (data) {
                    EntidadCorreo = data;
                    $cboTCorreo.val(EntidadCorreo.gdtcrreo);
                    $txtCorreo.val(EntidadCorreo.ccrreo);
                    $cboPrincipalCorreo.val(EntidadCorreo.fprncpl);
                    $hfCodCorreo.val(EntidadCorreo.id);
                    $FormularioCorreo.AgregarCamposAuditoria(EntidadCorreo);
                })
                .fail().always(function () {
                    $FormularioCorreo.find(":input").attr("disabled", false);
                    $FormularioCorreo.DeshabilitarCamposAuditoria();
                });
        },
        eliminarCorreo: function (idCorreo) {
            var parametro = { id: idCorreo, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Correo/eliminarCorreo", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTCorreo.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDTCorreo.reload();
                        })
                }
            });
        },
        guardarCorreo: function () {
            if ($hfactionCorreo.val() == "N") {
                funcionesCorreo.insertarCorreo();
            }
            else {
                funcionesCorreo.actualizarCorreo();
            }
            //$hfactionCorreo.val("");
        }
    }

    var funcionesArchivo = {
        descargarArchivo: function (id) {
            RedirectWithSubfolder(`/archivo/descargar?id=${id}&idprsna=${$hfCodPersona.val()}`);
            //$.get("/archivo/descargar", parametro).done(function (data) {

            //}).fail().always(function () {
            //});
        },
        eliminarArchivo: function (id) {
            var parametro = { id: id, idprsna: $hfCodPersona.val() };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/archivo/eliminar", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDtArchivo.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                            configDtArchivo.reload();
                        })
                }
            });
        },
        insertarArchivo: function () {
            if ($FormularioArchivo.valid()) {
                $btnGuardarArchivo.attr("disabled", true);
                //EntidadArchivo.nmbrarchv = $txtNombArch.val();
                //EntidadArchivo.ruta = $txtFileLoad.val();
                //EntidadArchivo.gdestdo = $FormularioArchivo.find("[name='GDESTDO']").val();
                //EntidadArchivo.idprsna = $hfCodPersona.val();
                $FormularioArchivo.find("[name='IDPRSNA']").val($hfCodPersona.val());
                var formElement = document.getElementById("FormularioArchivo");
                var fdata = new FormData(formElement);

                var fileInput = $FormularioArchivo.find("[name='Archivo']")[0];
                var file = fileInput.files[0];
                fdata.append("ARCHVO", file);
                $.ajax({
                    type: 'post',
                    url: "/archivo/insertarArchivo",
                    data: fdata,
                    processData: false,
                    contentType: false
                }).done(function () {
                    //$.post("/archivo/insertarArchivo", EntidadArchivo).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registró satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $mdlArchivo.modal("hide");
                        configDtArchivo.reload();
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                    configDtArchivo.reload();
                });
                $btnGuardarArchivo.attr("disabled", false);
            }
        },
        obtenerArchivo: function (idArchivo) {
            var parametro = { id: idArchivo, idprsna: $hfCodPersona.val() };
            $.get("/archivo/obtenerArchivo", parametro).done(function (data) {
                EntidadArchivo = data;
                $txtNombArch.val(EntidadArchivo.nmbrarchv);
                $txtFileLoad.val(EntidadArchivo.patharchv);
                $hfCodArchivo.val(EntidadArchivo.id);
                $FormularioArchivo.AgregarCamposAuditoria(EntidadArchivo);
            }).fail().always(function () {
                $FormularioArchivo.find(":input").attr("disabled", false);
                $FormularioArchivo.DeshabilitarCamposAuditoria();
            });
        }
    }

    var eventosIncrustadosPersona = {
        botonVerTrabajador: function () {
            $btnVerTrabajador.on("click", function () {
                var url = `/trabajador/editar/${$hddIdtrbjdr.val()}`;
                RedirectWithSubfolder(url);
            });
        },
        botonGrabarPersona: function () {
            $btnGrabarPersona.on("click", function () {
                $(this).attr("disabled", true);

                funcionesPersona.guardarPersona();
                $(this).attr("disabled", false);
            });
        },
        botonConsultarSUSALUD: function () {
            $btnBuscarSUSALUD.on("click", function () {
                funcionesPersona.ConsultarSusalud();
            });
            $btnConsultaSusalud.on("click", function () {
                $cboSSLDTDocumento.val(1).change().attr("disabled", true)
                //funcionesPersona.ConsultarSusalud();
            });
        },
        tap_listaPersona: function () {
            $tab_Lista.on("click", function () {
                $hfCodPersona.val("");
                $tab_Documento.addClass("disabled");
                $tab_Direccion.addClass("disabled");
                $tab_Telefono.addClass("disabled");
                $tab_CuentaCorreo.addClass("disabled");
                $tab_Archivos.addClass("disabled");
                $tab_Colaboradores.addClass("disabled");
                $tab_DatosGenerales.addClass("disabled");
                $tab_Lista.removeClass("disabled");
                configDTDocumento.tiposDocumentos = [];
                configDTDireccion.tipoDirecciones = [];
                configDTCorreo.tienePrincipal = false;
                configDTDireccion.tienePrincipal = false;
                configDTTelefono.tienePrincipal = false;
            });
        },
        cboTipoPersona: function () {
            $cboTPersonaNJ.change(function () {
                funcionesPersona.initTipoPersona(this.value);
                $FormularioPersona.AgregarCamposDefectoAuditoria();
                $FormularioPersona.DeshabilitarCamposAuditoria();
            });
        },
        Homonimia: function (options) {

            const defecto = {
                class: 'input-vacio',
                elemcontent: 'document',
            };
            var config = $.extend(defecto, options);

            var Elements = $(config.elemcontent).find('.' + config.class); // this.find('.' + elemClass);
            var ElemLength = $(config.elemcontent).find('.' + config.class).length;//this.find('.' + elemClass).length;
            $('.' + config.class).on("blur", function () {
                var boolVacio = false;
                for (var i = 0; i < ElemLength; i++) {
                    if (Elements[i].value) {
                        boolVacio = true;
                        break;
                    } else {
                        boolVacio = false;
                    }
                }
                if (boolVacio) {
                    funcionesPersona.mostrarModalHomonimia(boolVacio);
                }
            });
        },
        nacionalidad: function () {
            $cboNacionalidad.on("change", function () {
                $cboPDepartamento.val("").change();
                $cboPDistrito.attr("disabled", true);
                configUbigeos.f_nacionalidad(this);
            });
            $cboPaisNac.on("change", function () {
                $cboPDepartamento.val("").change();
                $cboPDistrito.attr("disabled", true);
                configUbigeos.f_nacionalidad(this);
                //cargarUbigeo(opts);
            });
            $chkPaisNac.on('change', function () {
                $cboPaisNac.attr("disabled", true);
                $cboNacionalidad.attr("disabled", false);
                if ($(this).is(':checked')) {
                    $cboPaisNac.attr("disabled", false);
                    $cboNacionalidad.attr("disabled", true);
                } else {
                    $cboPaisNac.val($cboNacionalidad.val());
                    configUbigeos.f_nacionalidad($cboNacionalidad);
                }
            });
        },
        init: function () {
            eventosIncrustadosPersona.botonVerTrabajador();
            eventosIncrustadosPersona.botonGrabarPersona();
            eventosIncrustadosPersona.botonConsultarSUSALUD();
            eventosIncrustadosPersona.tap_listaPersona();
            eventosIncrustadosPersona.cboTipoPersona();
            eventosIncrustadosPersona.nacionalidad();
        }
    }

    var eventosIncrustadosDocumento = {
        botonGrabarDocumento: function () {
            $btnGrabarDocumento.on("click", function () {
                $(this).attr("disabled", false);
                funcionesDocumento.guardarDocumento()
                $(this).attr("disabled", true);
            });
        },
        botonNuevo: function () {
            $btnNuevoDocumento.on("click", function () {
                $hfaction.val("N");
                if (configDTDocumento.tienePrincipal) {
                    $cboPrincipalDocumento.val("2");
                } else {
                    $cboPrincipalDocumento.val("1");
                }
                $modalTituloDocum.text("Agregar Documento")
                if ($cboTPersonaNJ.val() == 2) {
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGD("GDDCMNTO");
                    $cboDocumento.val('8').trigger("change");
                    $cboDocumento.prop('disabled', true);
                } else {
                    $cboDocumento.empty();
                    $cboDocumento.LlenarSelectGDFiltrado("GDDCMNTO", configDTDocumento.tiposDocumentos);
                    if (jQuery.inArray('1', configDTDocumento.tiposDocumentos) == -1) {
                        $cboDocumento.val('1').trigger("change");
                    }
                    $cboDocumento.val("").attr("disabled", false);
                    $cboDocumento.prop('disabled', false);
                }
            });
        },
        init: function () {
            eventosIncrustadosDocumento.botonNuevo();
            eventosIncrustadosDocumento.botonGrabarDocumento();
        }
    }

    var eventosIncrustadosDireccion = {
        botonGrabarDireccion: function () {
            $btnGrabarDireccion.on("click", function () {
                $(this).attr("disabled", true);
                funcionesDireccion.guardarDireccion();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoDireccion.on("click", function () {
                $hfactionDireccion.val("N"); $modalTituloDireccion.text("Agregar Dirección");
                $cboTDireccion.empty();
                $cboTDireccion.LlenarSelectGDFiltrado("GDTDRCCN", configDTDireccion.tipoDirecciones);
                if ($cboTPersonaNJ.val() == 1) {
                    $cboTDireccion.children('option[value="3"]').hide();
                } else {
                    $cboTDireccion.children('option[value="3"]').show();
                    $cboTDireccion.val("3").trigger("change")
                }
                if (configDTDireccion.tienePrincipal) {
                    $cboPrincipalDireccion.val("2");
                } else {
                    $cboPrincipalDireccion.val("1");
                }
                $cboTDireccion.val("").attr("disabled", false);
            });

        },
        onChangeTipoZona: function () {
            $cboTipoZona.on("change", function () {
                configModalDireccion.form.eventos.reset();
                funcionesDireccion.selectTipoZona();
            });
        },
        init: function () {
            eventosIncrustadosDireccion.botonGrabarDireccion();
            eventosIncrustadosDireccion.botonNuevo();
            eventosIncrustadosDireccion.onChangeTipoZona();
        }
    }

    var eventosIncrustadostelefono = {
        botonGrabarTelefono: function () {
            $btnGrabarTelefono.on("click", function () {
                $(this).attr("disabled", true);
                funcionesTelefono.guardarTelefono();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoTelefono.on("click", function () {
                $hfactionTelefono.val("N");
                $modalTituloTelefono.text("Agregar Teléfono");
                if (configDTTelefono.tienePrincipal) {
                    $cboPrincipalTelefono.val("2");
                } else {
                    $cboPrincipalTelefono.val("1");
                }
            });
        },
        cboTipoTelefono: function () {
            $cboTTelefono.on("change", function () {
                $this = $(this);
                $cboDepartamentoTlfno.val("").trigger("change");
                if ($this.val() == 1) {
                    $cboDepartamentoTlfno.removeClass("error");
                    $cboDepartamentoTlfno.attr("required", false);
                    $cboDepartamentoTlfno.attr("disabled", true);
                } else {
                    $cboDepartamentoTlfno.attr("required", true);
                    $cboDepartamentoTlfno.attr("disabled", false);
                }
            });
        },
        cboDepartamentoTelefono: function () {
            $cboDepartamentoTlfno.attr("disabled", true);
            var $ubigeo = $(JSON.parse(localStorage.getItem('ubigeo')));
            $cboDepartamentoTlfno.append($("<option />").val('').text("Cargando..."));
            $.each($ubigeo, function (key, item) {
                $cboDepartamentoTlfno.append($("<option />").val(item["cdgo"]).text(item["dscrpcn"]));
            });
            $cboDepartamentoTlfno.children('option[value=""]').text("Seleccione").val("");
            //$cboDepartamentoTlfno.attr("disabled", false);
        },
        init: function () {
            eventosIncrustadostelefono.botonGrabarTelefono();
            eventosIncrustadostelefono.botonNuevo();
            eventosIncrustadostelefono.cboTipoTelefono();
            eventosIncrustadostelefono.cboDepartamentoTelefono();
        }
    }

    var eventosIncrustadosCorreo = {
        botonGrabarCorreo: function () {
            $btnGrabarCorreo.on("click", function () {
                $(this).attr("disabled", true);
                funcionesCorreo.guardarCorreo();
                $(this).attr("disabled", false);
            });
        },
        botonNuevo: function () {
            $btnNuevoCorreo.on("click", function () {
                $hfactionCorreo.val("N");
                $modalTituloCorreo.text("Agregar Correo");
                if (configDTCorreo.tienePrincipal) {
                    $cboPrincipalCorreo.val("2");
                } else {
                    $cboPrincipalCorreo.val("1");
                }
            });
        },
        init: function () {
            eventosIncrustadosCorreo.botonGrabarCorreo();
            eventosIncrustadosCorreo.botonNuevo();
        }
    }

    var eventosIncrustadosArchivo = {
        botonNuevo: function () {
            $btnNuevoArchivo.on("click", function () {
                $hfactionArchivo.val("N");
                $modalTituloArchivo.text("Agregar Archivo");
            });
        },
        btnGuardarArchivo: function () {
            $btnGuardarArchivo.on("click", function () {
                $(this).attr("disabled", true);
                funcionesArchivo.insertarArchivo();
                $(this).attr("disabled", false);
            });
        },
        init: function () {
            eventosIncrustadosArchivo.botonNuevo();
            eventosIncrustadosArchivo.btnGuardarArchivo();
        }
    }

    var cboEstados = {
        init: function () {
            $cboEstado.LlenarSelectGD("GDESTDO");
        }
    };

    var cargarCombos = {
        init: function () {
            $cboTipoDocumento.LlenarSelectGD("GDDCMNTO");
            $cboTPersonaNJ.LlenarSelectGD("GDTPRSNA");
            $cboTPersona_NJ.LlenarSelectGD("GDTPRSNA");
            $cboSexo.LlenarSelectGD("GDSXO", "vlR1", "dgddtlle");
            $cboECivil.LlenarSelectGD("GDECVL", "vlR1", "dgddtlle");
            $cboPJTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPNTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboNacionalidad.LlenarSelectPais();
            $cboPaisNac.LlenarSelectPais();
            $cboSSLDTDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPJTSociedad.LlenarSelectGD("GDTESCDD");
            $cboPJTEmpresa.LlenarSelectGD("GDTETMNO");
            $cboTDocumentoSUNAT.LlenarSelectGD("GDDCMNTO");

            //------Documento ----------
            $cboDocumento.LlenarSelectGD("GDDCMNTO");
            $cboPrincipalDocumento.LlenarSelectGD("GDTDOPC");

            //---------Correo----------
            $cboTCorreo.LlenarSelectGD("GDTCRREO");//GDTCRREO
            $cboPrincipalCorreo.LlenarSelectGD("GDTDOPC"); //GDTDOPC

            //--------Direccion-----------
            $cboTDireccion.LlenarSelectGD("GDTDRCCN");
            if ($cboTPersonaNJ.val() == 1) {
                $cboTDireccion.children('option[value="3"]').hide();
            } else {
                $cboTDireccion.children('option[value="3"]').show();
                $cboTDireccion.val("3").trigger("change")
            }
            $cboTVia.LlenarSelectGD("GDTVIA");
            $cboTipoZona.LlenarSelectGD("GDTDZNA"); //GDTDZNA
            $cboPrincipalDireccion.LlenarSelectGD("GDTDOPC");

            //---------Telefono-----------
            $cboTTelefono.LlenarSelectGD("GDTTLFNO");
            $cboPrincipalTelefono.LlenarSelectGD("GDTDOPC");
        }
    };

    var cropper = {
        blob: null,
        basic: null,
        uploadImage: function () {
            // Upload button to Post Cropped Image to Store.
            $btnUploadImage.on('click', function () {
                cropper.basic.croppie('result', 'blob').then(function (blob) {
                    var output = document.getElementById('preview-image');
                    output.src = window.URL.createObjectURL(blob);
                    cropper.blob = blob;
                    //var tipoImagen = blob.type.split("/")[1];
                    //let file = new File([blob], new Date().getTime() +"."+ tipoImagen);
                    //var imagenseleccionada = document.getElementById('imagen-seleccionada');
                    //imagenseleccionada.value = file;
                });
                $modalCropper.modal("hide");
            });
        },
        readFile: function (input) {
            //Reading the contents of the specified Blob or File
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $mainCropper.croppie('bind', {
                        url: e.target.result
                    });
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        eventoChange: function () {
            // Change Event to Read file content from File input
            $seleccioneImagen.on('change', function () {
                cropper.readFile(this);
            });
        },
        init: function () {
            //initialize Croppie
            cropper.basic = $mainCropper.croppie
                ({
                    viewport: { width: 250, height: 250 },
                    boundary: { width: 300, height: 300 },
                    showZoomer: true,
                    url: '../../images/300.png',
                    format: 'png' //'jpeg'|'png'|'webp'
                });

            cropper.eventoChange();
            cropper.uploadImage();
        }
    }

    return {
        inicializar: function () {
            //donde se inicializa las funciones
            configDatePickers.init();
            cargarCombos.init();
            eventosIncrustadosPersona.init();
            configUbigeos.ubigeoPersona.init();
            cboEstados.init();
            funcionesPersona.init();
            configFormPersona.homonimiaPN.init();
            validacionControles.init();
            configModalDocumento.init();
            eventosIncrustadosDocumento.init();
            configModalCorreo.init();
            eventosIncrustadosCorreo.init();
            configModalDireccion.init();
            eventosIncrustadosDireccion.init();
            configModalTelefono.init();
            eventosIncrustadostelefono.init();
            configModalArchivo.init();
            eventosIncrustadosArchivo.init();
            configCboTipoDocumentos.init();
            configUbigeos.ubigeoDireccion.init();

            cropper.init();
        }
    }
}();
$(function () {
    InicializarDatosPersonas.inicializar();
});