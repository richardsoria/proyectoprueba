﻿var InicializarDatosTrabajador = function () {
    var $idTrabajador = $("#IdTrabajador").val();
    var $idPersona = $("#IdPersona").val();
    var $selectEstados = $(".select-estados");
    var $selectTipoPersona = $(".select-tipopersona");
    var $selectTipoTrabajador = $(".select-tipotrabajador");
    var $selectSedeContratacion = $(".select-sede-contratacion");
    var $selectGerencia = $(".select-gerencia");
    var $selectSuperintendencia = $(".select-superintendencia");
    var $selectArea = $(".select-area");
    var $selectPosicion = $(".select-posicion");
    var $selectCentroCosto = $(".select-centro-costo");
    var $selectUnidadOrganizacional = $(".select-unidad-organizacional");
    var $selectGrupo = $(".select-grupo");
    var $selectNivel = $(".select-nivel");
    var $selectDelegacionAutorizada = $(".select-delegacion-autorizada");
    var $selectPerfil = $(".select-perfil");
    var $selectTrabajoRemoto = $(".select-trabajo-remoto");
    var $selectTipoGuardia = $(".select-tipo-guardia");
    var $selectTitular = $(".select-titular");
    var $selectUbicacionAlojamiento = $(".select-ubicacion-alojamiento");
    var $selectLugarAlojamiento = $(".select-lugar-alojamiento");
    var $selectTipoAlojamiento = $(".select-tipo-alojamiento");
    var $selectGrupoCargo = $(".select-grupo-cargo");
    var $selectCargo = $(".select-cargo");
    $btnVerTrabajador = $("#btnVerTrabajador");
    var $btnBuscarPersona = $(".buscar-persona");
    var $btnContratista = $(".buscar-contratistas");
    var $inputContratista = $(".input-contratista");
    var $inputMotivo = $(".input-motivo");
    var $inputFechas = $(".input-fechas");
    var $inputTrabajadorNatural = $(".trabajador-natural");

    var $formTrabajador = $("#trabajador_form");
    /*****LISTA PERSONA (HACER GENERICO)******/
    var $modalBuscarPersona = $("#modal_buscar_persona");
    var $txtNombreRazon = $("#txtNombreRazon");
    var $txtNumDocumento = $("#txtNumDocumento");
    var $cbo_TipoDocumento = $("#cbo_TipoDocumento");
    var $cboTPersonaNJ = $("#cboTPersonaNJ");
    var $btnBuscarPersonaPopup = $("#btnBuscarPersonaPopup");
    var $btnaAgregarPersonaPopup = $("#btnaAgregarPersonaPopup");
    var $tabla_Persona = $("#tabla_Persona");

    var tablaPersona = {
        objeto: null,
        soloContratistas: false,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/persona/listar-persona",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.p_sDatos = $txtNombreRazon.val();
                    data.p_sTPersona = $cboTPersonaNJ.val();
                    data.p_sTDocumento = $cbo_TipoDocumento.val();
                    data.p_sNDocumento = $txtNumDocumento.val();
                    data.p_sEstado = 'A';
                    data.soloContratistas = tablaPersona.soloContratistas;
                    data.soloTrabajadores = true;
                },
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Persona",
                    data: "tprsna",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Nombres y apellidos /Razón",
                    data: "datos",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '2%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto",
                    width: '2%',
                    orderable: false,
                    className: "text-center"
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eventosPersona: function (data) {
                $formTrabajador.find("[name='GDTPRSNA']").val(data.gdtprsna);
                $formTrabajador.find("[name='IDPRSNA']").val(data.id);

                if (data.gdtprsna == "2") {//PERSONA JURIDICA
                    $formTrabajador.find("[name='NMBREPRSNA']").val(data.datos);
                    ConfigTrabajador.ocultarCamposTrabajadorNatural();
                    $formTrabajador.find("[name='GDTTRBJDR']").parent().hide();
                    $inputContratista.hide();
                    $formTrabajador.find("[name='GDTTRBJDR']").val("2").change();
                    $formTrabajador.find("[name='IDCNTRTSTA']").val(data.id);
                } else {//PERSONA NATURAL
                    var nombreCompleto = data.pnmbre + " " + (data.snmbre ? data.snmbre + " " : "") + data.aptrno + " " + (data.amtrno ? data.amtrno : "");
                    $formTrabajador.find("[name='NMBREPRSNA']").val(nombreCompleto);

                    if (data.fcntrtsta) {
                        ConfigTrabajador.ocultarCamposTrabajadorNatural();
                        $formTrabajador.find("[name='GDTTRBJDR']").parent().hide();
                        $inputContratista.hide();
                        $formTrabajador.find("[name='GDTTRBJDR']").val("2").change();
                        $formTrabajador.find("[name='IDCNTRTSTA']").val(data.id);
                    } else {
                        ConfigTrabajador.mostrarCamposTrabajadorNatural();
                        $inputContratista.show();
                        $formTrabajador.find("[name='GDTTRBJDR']").val("1").change();
                        $formTrabajador.find("[name='GDTTRBJDR']").parent().show();
                    }
                }
                ConfigTrabajador.habilitarCampos();
                $selectSuperintendencia.attr("disabled", true);
                $selectArea.attr("disabled", true);
                $selectCargo.attr("disabled", true);
                $modalBuscarPersona.modal("hide");
            },
            eventosContratistas: function (data) {
                $formTrabajador.find("[name='IDCNTRTSTA']").val(data.id);
                if (data.gdtprsna == "2") {
                    $formTrabajador.find("[name='CNTRTSTA']").val(data.datos);
                } else {
                    var nombreCompleto = data.pnmbre + " " + (data.snmbre ? data.snmbre : "") + data.aptrno + " " + (data.amtrno ? data.amtrno : "");
                    $formTrabajador.find("[name='CNTRTSTA']").val(nombreCompleto);
                }

                $modalBuscarPersona.modal("hide");
            },
            init: function () {
                tablaPersona.objeto.on("click", "tr", function () {
                    var data = tablaPersona.objeto.row(this).data();
                    if (tablaPersona.soloContratistas) {
                        tablaPersona.eventos.eventosContratistas(data);
                    } else {
                        tablaPersona.eventos.eventosPersona(data);
                    }
                });
            }
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            if (tablaPersona.objeto == null || tablaPersona.objeto == "" || tablaPersona.objeto == undefined) {
                tablaPersona.objeto = $tabla_Persona.DataTable(tablaPersona.opciones);
                tablaPersona.eventos.init();
            }
            else {
                tablaPersona.reload();
            }
        }
    };
    /*****LISTA PERSONA (HACER GENERICO)******/

    var obtenerDatosTrabajador = {
        init: function () {
            $.get("/trabajador/obtener", { id: $idTrabajador })
                .done(function (data) {
                    console.log(JSON.stringify(data));
                    $formTrabajador.find("[name='GDTPRSNA']").val(data.gdtprsna);
                    $formTrabajador.find("[name='NMBREPRSNA']").val(data.nmbrs);
                    $formTrabajador.find("[name='CSAP']").val(data.csap);
                    $formTrabajador.find("[name='GDTTRBJDR']").val(data.gdttrbjdr).change();
                    if (data.gdttrbjdr == 2) {
                        $inputContratista.removeClass("d-none");
                        $formTrabajador.find("[name='IDCNTRTSTA']").val(data.idcntrtsta);
                        $formTrabajador.find("[name='CNTRTSTA']").val(data.cntrtsta);
                    } else {
                        $inputContratista.addClass("d-none");
                    }
                    if (data.idcntrtsta == data.idprsna)//SI SON IGUALES ES PORQUE LA PERSONA A EDITAR ES EL MISMO CONTRATISTA
                    {
                        ConfigTrabajador.ocultarCamposTrabajadorNatural();
                        $formTrabajador.find("[name='IDCNTRTSTA']").val(data.idcntrtsta);
                    } else {
                        $formTrabajador.find("[name='FINGRSO']").val(data.fingrso);
                        $formTrabajador.find("[name='FINGRSO']").datepicker('setDate', data.fingrso);
                        $formTrabajador.find("[name='FBJA']").val(data.fbja);
                        $formTrabajador.find("[name='MBJA']").val(data.mbja);
                        $formTrabajador.find("[name='GDSDE']").val(data.gdsde);
                        $formTrabajador.find("[name='GDGRPOPS']").val(data.gdgrpops);
                        $formTrabajador.find("[name='GDNVL']").val(data.gdnvl);
                        $formTrabajador.find("[name='GDDLGCNATRZDA']").val(data.gddlgcnatrzda);
                        $formTrabajador.find("[name='FTRBJORMTO']").val(data.ftrbjormto);
                        $formTrabajador.find("[name='FTITLR']").val(data.ftitlr);
                        $formTrabajador.find("[name='GDPRFLSTRBJDR[]']").val(data.gdprfltrbjdr.split(",")).trigger('change');
                        $formTrabajador.find("[name='GDTGRDA']").val(data.gdtgrda);
                        $formTrabajador.find("[name='GDLGRALJMNTO']").val(data.gdlgraljmnto);
                        $formTrabajador.find("[name='GDUBCCNALJMNTO']").val(data.gdubccnaljmnto);
                        $formTrabajador.find("[name='GDTALJMNTO']").val(data.gdtaljmnto);
                        $formTrabajador.find("[name='MDLO']").val(data.mdlo);
                        $formTrabajador.find("[name='PSO']").val(data.pso);
                        $formTrabajador.find("[name='CMA']").val(data.cma);
                    }

                    if (data.fto) {
                        var imagenpersona = document.getElementById('preview-image');
                        imagenpersona.src = "data:image/*;base64," + data.fto;
                    }

                    $formTrabajador.find("[name='FINGRSO']").val(data.fingrso);
                    $formTrabajador.find("[name='FINGRSO']").datepicker('setDate', data.fingrso);
                    $formTrabajador.find("[name='FBJA']").val(data.fbja);
                    $formTrabajador.find("[name='MBJA']").val(data.mbja);
                    ///////////////////////////////////////////////////////////////////////
                    selects.gerencia(data.idgrnca, data.idsprintndnca, data.idara);
                    ///////////////////////////////////////////////////////////////////////
                    selects.grupoCargo(data.idgrpo, data.idcrgo);
                    ///////////////////////////////////////////////////////////////////////
                    selects.posicion(data.idpscn);
                    selects.centroCosto(data.idcntrocsto);
                    selects.unidadOrganizacional(data.idunddorgnzcnl);
                    ///////////////////////////////////////////////////////////////////////
                    $idPersona = data.idprsna;
                    $formTrabajador.AgregarCamposAuditoria(data);
                    setTimeout(function () {
                        ConfigTrabajador.habilitarCampos();
                    }, 2000)
                });
        }
    };

    var obtenerDatosPersona = {
        init: function () {
            $.get("/Persona/obtenerPersona", { id: $idPersona })
                .done(function (data) {
                    if (data.fto) {
                        var imagenpersona = document.getElementById('preview-image');
                        imagenpersona.src = "data:image/*;base64," + data.fto;
                    }
                    $formTrabajador.find("[name='GDTPRSNA']").val(data.gdtprsna);
                    if (data.gdtprsna == 2) {
                        $formTrabajador.find("[name='NMBREPRSNA']").val(data.rscl);
                        ConfigTrabajador.ocultarCamposTrabajadorNatural();
                        $formTrabajador.find("[name='GDTTRBJDR']").parent().hide();
                        $inputContratista.hide();
                        $formTrabajador.find("[name='GDTTRBJDR']").val("2").change();
                        $formTrabajador.find("[name='IDCNTRTSTA']").val(data.id);
                    } else {
                        var nombreCompleto = data.pnmbre + " " + (data.snmbre ? data.snmbre + " " : "") + data.aptrno + " " + (data.amtrno ? data.amtrno : "");
                        $formTrabajador.find("[name='NMBREPRSNA']").val(nombreCompleto);
                        if (data.fcntrtsta) {
                            ConfigTrabajador.ocultarCamposTrabajadorNatural();
                            $formTrabajador.find("[name='GDTTRBJDR']").parent().hide();
                            $inputContratista.hide();
                            $formTrabajador.find("[name='GDTTRBJDR']").val("2").change();
                            $formTrabajador.find("[name='IDCNTRTSTA']").val(data.id);
                        } else {
                            ConfigTrabajador.mostrarCamposTrabajadorNatural();
                            $inputContratista.show();
                            $formTrabajador.find("[name='GDTTRBJDR']").val("1").change();
                            $formTrabajador.find("[name='GDTTRBJDR']").parent().show();
                        }
                    }
                    ConfigTrabajador.habilitarCampos();
                    $selectSuperintendencia.attr("disabled", true);
                    $selectArea.attr("disabled", true);
                    $selectCargo.attr("disabled", true);
                });
        }
    }

    var ConfigTrabajador = {
        form: {
            objeto: $formTrabajador.validate({
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass("select-perfil")) {
                        element.parent().append(error);
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    //CLVE: {
                    //    required: true,
                    //    minlength: 8
                    //}
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formTrabajador.find(":input").attr("disabled", true);

                    if (!$formTrabajador.find("[name='ID']").val()) {
                        url = `/trabajador/insertar`;
                    } else {
                        url = `/trabajador/actualizar`
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                RedirectWithSubfolder("/trabajador");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $formTrabajador.find(":input").attr("disabled", false);
                            ConfigTrabajador.deshabilitarCampos();
                            $formTrabajador.DeshabilitarCamposAuditoria();
                        });
                }
            }),
            eventos: {
                reset: function () {
                    ConfigTrabajador.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            reset: function () {
                ConfigTrabajador.form.eventos.reset();
                $formTrabajador.trigger("reset");
            }
        },
        ocultarCamposTrabajadorNatural: function () {
            $formTrabajador.find("[name='GDTTRBJDR']").parent().hide();
            $formTrabajador.find("[name='GDTTRBJDR']").attr("required", false);
            $formTrabajador.find("[name='GDSDE']").parent().hide();
            $formTrabajador.find("[name='GDSDE']").attr("required", false);
            $formTrabajador.find("[name='CSAP']").parent().hide();
            $formTrabajador.find("[name='CSAP']").attr("required", false);
            $inputTrabajadorNatural.hide();
            $inputTrabajadorNatural.find("input").val("");
            $inputTrabajadorNatural.find("input").attr("required", false);
            $inputTrabajadorNatural.find("select").val("");
            $inputTrabajadorNatural.find("select").attr("required", false);
        },
        mostrarCamposTrabajadorNatural: function () {
            $formTrabajador.find("[name='GDTTRBJDR']").parent().show();
            $formTrabajador.find("[name='GDTTRBJDR']").attr("required", true);
            $formTrabajador.find("[name='GDSDE']").parent().show();
            $formTrabajador.find("[name='GDSDE']").attr("required", true);
            $formTrabajador.find("[name='CSAP']").parent().show();
            $formTrabajador.find("[name='CSAP']").attr("required", true);

            $inputTrabajadorNatural.show();
            $inputTrabajadorNatural.find("input").attr("required", true);
            $inputTrabajadorNatural.find("select").attr("required", true);
            this.valoresPorDefecto();
        },
        valoresPorDefecto: function () {
            $formTrabajador.find("[name='GDTTRBJDR']").val("1");
            $formTrabajador.find("[name='FTRBJORMTO']").val("2");
            $formTrabajador.find("[name='FTITLR']").val("1");
        },
        deshabilitarCampos: function () {
            $formTrabajador.find("[name='GDTPRSNA']").attr("disabled", true);
            $formTrabajador.find("[name='NMBREPRSNA']").attr("disabled", true);
            $formTrabajador.find("[name='CNTRTSTA']").attr("readonly", true);
            //$formTrabajador.find("[name='FBJA']").attr("disabled", true);
            $formTrabajador.find("[name='MBJA']").attr("disabled", true);
        },
        habilitarCampos: function () {
            $formTrabajador.find("input").attr("disabled", false);
            $formTrabajador.find("select").attr("disabled", false);

            if (!$idTrabajador) {
                this.valoresPorDefecto();
            }

            //NO HABILITAR ESTOS
            this.deshabilitarCampos();
            //auditoria

            $formTrabajador.DeshabilitarCamposAuditoria();
        },
        init: function () {
            $formTrabajador.find("input").attr("disabled", true);
            $formTrabajador.find("select").attr("disabled", true);
            if (!$idTrabajador) {
                $formTrabajador.AgregarCamposDefectoAuditoria();
            }
        }
    };

    var eventosIncrustados = {
        btnVerPersona: function () {
            $btnVerTrabajador.on("click", function () {
                var url = `/Persona/editar/${$idPersona}`;
                RedirectWithSubfolder(url);
            });
        },
        btnBuscarPersonaModal: function () {
            $btnBuscarPersonaPopup.click(function () {
                tablaPersona.inicializador();
            });
            $btnBuscarPersona.on("click", function () {
                tablaPersona.soloContratistas = false;
                tablaPersona.inicializador();
            });
        },
        btnBuscarContratista: function () {
            $btnContratista.on("click", function () {
                tablaPersona.soloContratistas = true;
                $modalBuscarPersona.modal("show");
                tablaPersona.inicializador();
            });
        },
        init: function () {
            this.btnVerPersona();
            this.btnBuscarContratista();
            this.btnBuscarPersonaModal();
        }
    }

    var datepickers = {
        opciones: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        fechaIngreso: function () {
            $formTrabajador.find("[name='FINGRSO']").datepicker(datepickers.opciones);
            $formTrabajador.find("[name='FINGRSO']").on("change", function () {
                var valor = $(this).val();
                var FechaIngreso = $formTrabajador.find("[name='FINGRSO']").datepicker("getDate");
                var FechaBaja = $formTrabajador.find("[name='FBJA']").datepicker("getDate");

                if (valor) {

                } else {
                    $formTrabajador.find("[name='FINGRSO']").attr("required", false);
                    $formTrabajador.find("[name='FINGRSO']").removeClass('error');
                    $(".VFINGRSO").remove();
                }

                if (FechaBaja) {
                    var Dias = (FechaBaja - FechaIngreso) / (1000 * 60 * 60 * 24);
                    if (Math.round(Dias) < 0) {
                        $formTrabajador.find("[name='FINGRSO']").addClass('error');
                        $formTrabajador.find("[name='ValFINGRSO']").after('<label id="FINGRSO-error" class="error VFINGRSO" for="FINGRSO">La fecha de Ingreso no puede ser mayor a la de Baja</label>');

                    }
                }
            });
        },
        fechaBaja: function () {
            var contador = 0;
            $formTrabajador.find("[name='FBJA']").datepicker(datepickers.opciones);
            $formTrabajador.find("[name='FBJA']").on("change", function () {
                var valor = $(this).val();

                if (valor) {
                    $formTrabajador.find("[name='MBJA']").attr("required", true);
                    var FechaIngreso = $formTrabajador.find("[name='FINGRSO']").datepicker("getDate");
                    var FechaBaja = $formTrabajador.find("[name='FBJA']").datepicker("getDate");
                    if (FechaIngreso) {
                        var Dias = (FechaBaja - FechaIngreso) / (1000 * 60 * 60 * 24);
                        if (Math.round(Dias) < 0) {
                            $formTrabajador.find("[name='FBJA']").addClass('error');
                            $formTrabajador.find("[name='ValFBJA']").after('<label id="FBJA-error" class="error VFBJA" for="FBJA">La fecha Baja no puede ser menor a la de Ingreso</label>');
                        }
                    }

                    if (contador == 0) {
                        $formTrabajador.find("[name='MBJA']").addClass('error');
                        $formTrabajador.find("[name='MBJA']").after('<label id="MBJA-error" class="error VMBJA" for="MBJA">Ingrese Motivo de Baja</label>');
                        contador = 1;
                    }
                } else {
                    $formTrabajador.find("[name='FBJA']").attr("required", false);
                    $formTrabajador.find("[name='FBJA']").removeClass('error');
                    $(".VFBJA").remove();
                    contador = 0;
                    $formTrabajador.find("[name='MBJA']").attr("required", false);
                    $formTrabajador.find("[name='MBJA']").removeClass('error');
                    $(".VMBJA").remove();
                }
            });
        },
        init: function () {
            this.fechaIngreso();
            this.fechaBaja();
        }
    };

    var selects = {
        gerencia: function (idGerencia, idSuperintendencia, idArea) {
            $selectGerencia.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-gerencias")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectGerencia.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectGerencia.children('option[value=""]').text("Seleccione");
                    if (idGerencia) {
                        $selectGerencia.val(idGerencia)
                        selects.superintendencia(idGerencia, idSuperintendencia, idArea)
                    }
                });
            $selectGerencia.on("change", function () {
                selects.superintendencia();
                selects.area();
            });
        },
        superintendencia: function (idGerencia, idSuperintendencia, idArea) {
            $selectSuperintendencia.attr("disabled", true);
            $selectSuperintendencia.empty();
            $selectSuperintendencia.append($("<option />").val('').text("Cargando..."));
            if ($selectGerencia.val()) {
                $.get("/obtener-superintendencias", { idGerencia: (idGerencia ? idGerencia : $selectGerencia.val()) })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $selectSuperintendencia.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                        });
                        $selectSuperintendencia.attr("disabled", false);
                        if (idSuperintendencia) {
                            $selectSuperintendencia.val(idSuperintendencia);
                            selects.area(idSuperintendencia, idArea);
                        }
                    });
            } else {
                $selectSuperintendencia.val("").change();
            }
            $selectSuperintendencia.children('option[value=""]').text("Seleccione");
            $selectSuperintendencia.unbind('change');
            $selectSuperintendencia.on("change", function () {
                selects.area();
            });
        },
        area: function (idSuperintendencia, idArea) {
            $selectArea.attr("disabled", true);
            $selectArea.empty();
            $selectArea.append($("<option />").val('').text("Cargando..."));
            if ($selectSuperintendencia.val()) {
                $.get("/obtener-areas", { idSuperintendencia: (idSuperintendencia ? idSuperintendencia : $selectSuperintendencia.val()) })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $selectArea.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                        });
                        $selectArea.attr("disabled", false);
                        if (idArea) {
                            $selectArea.val(idArea)
                        }
                    });
            } else {
                $selectArea.val("");
            }
            $selectArea.children('option[value=""]').text("Seleccione");
        },
        posicion: function (idPosicion) {
            $selectPosicion.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-posicion")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectPosicion.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectPosicion.children('option[value=""]').text("Seleccione");
                    if (idPosicion) {
                        $selectPosicion.val(idPosicion);
                    }
                });
        },
        centroCosto: function (idCentro) {
            $selectCentroCosto.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-centro-costo")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectCentroCosto.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectCentroCosto.children('option[value=""]').text("Seleccione");
                    if (idCentro) {
                        $selectCentroCosto.val(idCentro);
                    }
                });
        },
        unidadOrganizacional: function (idUnidad) {
            $selectUnidadOrganizacional.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-unidad-organizacional")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectUnidadOrganizacional.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectUnidadOrganizacional.children('option[value=""]').text("Seleccione");
                    if (idUnidad) {
                        $selectUnidadOrganizacional.val(idUnidad);
                    }
                });
        },
        grupoCargo: function (idGrupo, idCargo) {
            $selectGrupoCargo.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-grupo-cargo")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectGrupoCargo.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectGrupoCargo.children('option[value=""]').text("Seleccione");
                    if (idGrupo) {
                        $selectGrupoCargo.val(idGrupo)
                        selects.cargo(idGrupo, idCargo)
                    }
                });
            $selectGrupoCargo.on("change", function () {
                selects.cargo();
            });
        },
        cargo: function (idGrupo, idCargo) {
            $selectCargo.attr("disabled", true);
            $selectCargo.empty();
            $selectCargo.append($("<option />").val('').text("Cargando..."));
            if ($selectGrupoCargo.val()) {
                $.get("/obtener-cargos", { idGrupoCargo: (idGrupo ? idGrupo : $selectGrupoCargo.val()) })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $selectCargo.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                        });
                        $selectCargo.attr("disabled", false);
                        if (idCargo) {
                            $selectCargo.val(idCargo)
                        }
                    });
            } else {
                $selectCargo.val("");
            }
            $selectCargo.children('option[value=""]').text("Seleccione");
        },
        tipoTrabajador: function () {
            $selectTipoTrabajador.on("change", function () {
                var id = $(this).val();
                if (id != 2) {//MMG
                    //OCULTAR CAMPO DE CONTRATISTA
                    $inputContratista.addClass("d-none");
                    $formTrabajador.find("[name='IDCNTRTSTA']").val("");
                    $formTrabajador.find("[name='CNTRTSTA']").val("");
                    $formTrabajador.find("[name='CNTRTSTA']").attr("required", false);
                    $inputMotivo.removeClass("col-lg-3").addClass("col-lg-4");
                    $inputFechas.removeClass("col-lg-2").addClass("col-lg-3");
                } else {//CONTRATISTA
                    //MOSTRAR CAMPO DE CONTRATISTA
                    $formTrabajador.find("[name='CNTRTSTA']").attr("required", true);
                    $inputContratista.removeClass("d-none");
                    $inputMotivo.addClass("col-lg-3").removeClass("col-lg-4");
                    $inputFechas.addClass("col-lg-2").removeClass("col-lg-3");
                }
            });
        },
        perfilTrabajador: function () {
            $selectPerfil.LlenarSelectGD("GDPRFLTRBJDR");
            $selectPerfil.select2({
                language: {
                    noResults: function () {
                        return "No hay resultados";
                    },
                },
                width: 'resolve',
                minimumInputLength: -1,
                placeholder: 'Seleccione perfiles',
                allowClear: false
            });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            $cbo_TipoDocumento.LlenarSelectGD("GDDCMNTO");
            $selectTipoPersona.LlenarSelectGD("GDTPRSNA");
            $selectTipoTrabajador.LlenarSelectGD("GDTTRBJDR");
            $selectSedeContratacion.LlenarSelectGD("GDSDE");
            $selectGrupo.LlenarSelectGD("GDGRPOPS");
            $selectNivel.LlenarSelectGDOrdenado("GDNVL");
            $selectDelegacionAutorizada.LlenarSelectGD("GDDLGCNATRZDA");
            /* $selectPerfil.LlenarSelectGD("GDPRFLTRBJDR");*/
            $selectTrabajoRemoto.LlenarSelectGD("GDTDOPC");
            $selectTipoGuardia.LlenarSelectGD("GDTGRDA");
            $selectTitular.LlenarSelectGD("GDTDOPC");
            $selectLugarAlojamiento.LlenarSelectGD("GDLGRALJMNTO");
            $selectUbicacionAlojamiento.LlenarSelectGD("GDUBCCNALJMNTO");
            $selectTipoAlojamiento.LlenarSelectGD("GDTALJMNTO");

            this.tipoTrabajador();
            this.perfilTrabajador();
            if (!$idTrabajador) {
                this.gerencia();
                this.superintendencia();
                this.area();
                this.grupoCargo();
                this.cargo();
                this.posicion();
                this.centroCosto();
                this.unidadOrganizacional();
                $btnVerTrabajador.hide();
            }
        }
    };

    return {
        init: function () {
            selects.init();
            eventosIncrustados.init();
            datepickers.init();
            ConfigTrabajador.init();

            if ($idTrabajador) {
                obtenerDatosTrabajador.init();
            }
            if ($idPersona) {
                obtenerDatosPersona.init();
            }
        }
    };
}();

$(() => {
    InicializarDatosTrabajador.init();
})

