﻿
var InicializarTalla = function () {
    var $idtrbjdr = $("#IDTRBJDR");
    var $Divtlls = $("#Divtlls");
    var $DivRstrc = $("#DivRstrc");
    var $lblTitulo = $("#lblTitulo");
    var $lblTrabajador = $("#lblTrabajador");
    var $DSCRPCN = $("#DSCRPCN");
    var $selectTalla = $(".select-talla");
    var $tablaArticulo = $("#tabla_articulo");
    var $modalArticulo = $("#modal_Articulo");
    var $idgrupocargo = 0;
    var $grupocargo = "";
    var $DivBusqueda = $("#DivBusqueda");
    var $txtBusqueda = $("#txtBusqueda");
    var $ValtxtBusqueda = $("#ValtxtBusqueda");
    var $ValCNTDD = $("#ValCNTDD");
    var $dataecontrada = $("#dataecontrada");
    var $btnAgregarArticulos = $('#btnAgregarArticulos');
    var $btnKitBuscar = $("#btnKitBuscar");
    var $btnAgregar = $("#btnKitAgregar");
    var $btnKitLimpiar = $("#btnKitLimpiar");
    var $btnGuardar = $("#btnGrabarTalla");

    var tablaArticulo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/obtener-articulos-talla",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.flagtlla = $('.chkMnjtlls').is(':checked');
                    data.idgrupocargo = $idgrupocargo;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Unid. Med.",
                    className: "text-center",
                    data: "gdunddmdda",
                    orderable: false
                },
                {
                    title: "Talla",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        var tallaMemory = tablaArticulo.memory_art.find(x => x.CSAP == data.csap);
                        if (tallaMemory) {
                            tpm += `<select data-csap="${data.csap}" style="width:100%" data-dscrpcn="${data.dscrpcn}" data-gdunddmdda="${data.gdunddmdda}" data-idartclo="${data.id}" class="input-group-text select2"></select>`;
                        } else {
                            tpm += `<select data-csap="${data.csap}" style="width:100%" data-dscrpcn="${data.dscrpcn}" data-gdunddmdda="${data.gdunddmdda}" data-idartclo="${data.id}" class="input-group-text select2"></select>`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        memory_art: [],
        eventos: function () {
            tablaArticulo.objeto.on('draw.dt', function () {
                $('.select2').LlenarSelectGD("GDTLLA");
                if (tablaArticulo.memory_art.length > 0) {
                    $('.select2').each(function (index) {
                        var csaptabla = $(this).data("csap");
                        for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                            if (tablaArticulo.memory_art[i].CSAP == csaptabla) {
                                $(this).val(tablaArticulo.memory_art[i].TLLA);
                            }
                        }
                    });
                }

                $('.select2').select2(selects.opciones);
            });
            tablaArticulo.objeto.on("change", ".select2", function () {
                var csap = $(this).data("csap");
                var descr = $(this).data("dscrpcn");
                var und = $(this).data("gdunddmdda");
                var tlla = $(this).val();
                var idart = $(this).data("idartclo");

                var delete_articulo = false;
                if (tlla == '') {
                    delete_articulo = true;
                }
                var new_articulo = true;
                for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                    if (tablaArticulo.memory_art[i].CSAP == csap) {
                        new_articulo = false;
                        if (delete_articulo) {
                            tablaArticulo.memory_art.splice(i, 1);
                        } else {
                            tablaArticulo.memory_art[i].TLLA = tlla;
                        }
                    }
                }
                if (new_articulo) {
                    tablaArticulo.memory_art.push({
                        CSAP: csap,
                        DESCR: descr,
                        UND: und,
                        TLLA: tlla,
                        IDART: idart,
                    });
                }
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
            tablaArticulo.eventos();
        }
    };

    var modalArticulo = {
        eventos: {
            onHide: function () {
                $modalArticulo.on('hidden.bs.modal', function () {
                    modalArticulo.eventos.reset();
                })
            },
            onShow: function () {
                $modalArticulo.on('shown.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                })
            },
            reset: function () {
                tablaArticulo.memory_art = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var eventosIncrustados = {
        editarCantidadFila: function () {
            $(document).on('click', '.btn-editar-tabla', function (event) {
                //
                var id = $(this).data("id");
                var btn = $(this);
                var hasClass = $(this).hasClass("btn-info");
                var input = ".select-tabla-talla-" + id;
                if (hasClass) {
                    $("#tabla_articuloadd").find(input).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anterior = $("#tabla_articuloadd").find(input).data("anterior");
                    $("#tabla_articuloadd").find(input).val(anterior).change();
                    $("#tabla_articuloadd").find(input).attr("disabled", true);
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
                //
            });
        },
        CheckTalla: function () {
            $('.chkMnjtlls').on('change', function () {
                if ($('.chkMnjtlls').is(':checked')) {
                    $lblTitulo.text("ARTÍCULOS QUE MANEJAN TALLA");
                } else {
                    $lblTitulo.text("ARTÍCULOS POR GRUPO CARGO " + $grupocargo);
                }
                tablaArticulo.reload();
            });
        },
        CalcularIndex: function () {
            var hayfilas = $("#tabla_articuloadd tbody tr").length > 0;
            if (!hayfilas) {
                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
            $("#tabla_articuloadd tbody tr").each(function (index) {
                var trs = index + 1;
                $(this).children('td').eq(0).text(trs);
            })
        },
        EliminarFila: function () {
            $(document).on('click', '.BorrarFilaSubMenu', function (event) {
                event.preventDefault();
                var fila = $(this).closest('tr');
                var idkitdetalle = $(this).attr("id");
                let arrayid = idkitdetalle.split('_');
                var id = arrayid[1];

                if ($idtrbjdr.val()) {
                    if (id > 0) {
                        Swal.fire({
                            title: "¿Quiere modificar el estado del registro?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Aceptar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: "/eliminar-detalle-talla",
                                    type: "POST",
                                    data: {
                                        id: id
                                    }
                                }).done(function () {
                                    Swal.fire({
                                        icon: "success",
                                        allowOutsideClick: false,
                                        title: "Éxito",
                                        text: "Registro modificado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then((result) => {
                                        fila.remove();
                                        eventosIncrustados.CalcularIndex();
                                    });
                                }).fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al modificar el registro.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                });
                            }
                        });
                    } else {
                        fila.remove();
                    }

                } else {
                    fila.remove();
                }
                eventosIncrustados.CalcularIndex();
            });
        },
        botonAgregarArticulo: function () {
            $btnAgregarArticulos.on("click", function () {
                for (var i = 0; i < tablaArticulo.memory_art.length; i++) {
                    var art_in_table = false;
                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').find("[class='csap']").text();
                        if (tablaArticulo.memory_art[i].CSAP == csaptabla.trim()) {
                            art_in_table = true;
                            $(this).children('td').find("[class='.select-tabla-talla']").val(tablaArticulo.memory_art[i].TLLA);
                        }
                    })
                    if (!art_in_table) {
                        var trs = i + 1
                        eventosIncrustados.formatoFilaTabla(trs, 0, tablaArticulo.memory_art[i].IDART, tablaArticulo.memory_art[i].CSAP, tablaArticulo.memory_art[i].DESCR, tablaArticulo.memory_art[i].UND, tablaArticulo.memory_art[i].TLLA, false, false);
                    }
                }
                tablaArticulo.memory_art = [];
                $modalArticulo.modal('hide');
                eventosIncrustados.CalcularIndex();
            })

        },
        botonBuscar: function () {
            $btnKitBuscar.on("click", function () {
                tablaArticulo.memory_art = [];
                tablaArticulo.reload();
            })
        },
        botonGrabar: function () {
            $btnGuardar.on("click", function () {

                var fila = $("#tabla_articuloadd tbody tr").length;

                if (fila == 0) {
                    Swal.fire({
                        title: "No se puede crear el Tallas Por Trabajdor sin artículos asociados.",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                $btnGuardar.attr("disabled", true);
                var articulos = [];
                $("#tabla_articuloadd tbody tr").each(function (index) {
                    var tlla = $(this).find(".select-tabla-talla").val();
                    var id = $(this).find("[class='idartclo']").val();
                    articulos.push(
                        {
                            TLLATRBJDR: tlla,
                            ARTCLO: id,
                        }
                    );
                })
                //console.log(JSON.stringify(articulos));
                var formData = new FormData();
                formData.append("TLLADTO", JSON.stringify(articulos));
                formData.append("IDTRBJDR", $idtrbjdr.val());
                //console.log(formData);
                $.ajax({
                    url: "/actualizar-trabajadortalla",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false
                })
                    .done(function (e) {
                        Swal.fire({
                            title: "Éxito",
                            icon: "success",
                            allowOutsideClick: false,
                            text: "Guardado satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then(function () {
                            //RedirectWithSubfolder(`/trabajador/tallas/${$idtrbjdr.val()}`);
                            obtenerDatos.obtenerDetalle();

                            eventosIncrustados.CalcularIndex();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al guardar los datos.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    })
                    .always(function () {
                    });


                $btnGuardar.attr("disabled", false);

            })
        },
        botonAgregar: function () {
            $btnAgregar.on("click", function () {
                debugger;

                var talla = $("#txtTalla").val();
                var txtbusqueda = $txtBusqueda.val();

                if (talla == "" && txtbusqueda != "") {
                    $("#txtTalla").removeClass('error');
                    $("#prueba").removeClass('error');
                    $(".VCNTDDA").remove();

                    $("#txtTalla").addClass('error');
                    $("#prueba").addClass('error');
                    $ValCNTDD.after('<label id="CNTDD-error" class="error VCNTDDA" >Ingrese Talla</label>');

                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    return;
                }

                if (talla != "" && txtbusqueda == "") {
                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    $("#txtBusqueda").addClass('error');
                    $ValtxtBusqueda.after('<label id="txtBusqueda-error" class="error VtxtBusqueda">Ingrese Descripción </label>');

                    $("#txtTalla").removeClass('error');
                    $("#prueba").removeClass('error');
                    $(".VCNTDDA").remove();
                    return;
                }

                if (talla == "" && txtbusqueda == "") {
                    $("#txtTalla").removeClass('error');
                    $("#prueba").removeClass('error');
                    $(".VCNTDDA").remove();

                    $("#txtTalla").addClass('error');
                    $("#prueba").addClass('error');
                    $ValCNTDD.after('<label id="CNTDD-error" class="error VCNTDDA" >Ingrese Talla</label>');

                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    $("#txtBusqueda").addClass('error');
                    $ValtxtBusqueda.after('<label id="txtBusqueda-error" class="error VtxtBusqueda">Ingrese Descripción </label>');

                    return;
                }

                if (talla != "" && txtbusqueda != "") {
                    $("#txtTalla").removeClass('error');
                    $(".VCNTDDA").remove();
                    $(".VtxtBusqueda").removeClass();
                    $txtBusqueda.remove('error');

                    //  VIENE CODIGO SAP - DESCRIPCION
                    var dataencontrada = $dataecontrada.val()           //  VIENE EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA

                    Arraycsapdesc = txtbusqueda.split('-'); // SEPARA  CODIGO SAP - DESCRIPCION
                    var csap = Arraycsapdesc[0].toString();
                    var desc = Arraycsapdesc[1].toString();

                    Arrayidgduni = dataencontrada.split('-');  //  SEPARA EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA
                    var id = Arrayidgduni[0].toString();
                    var und = Arrayidgduni[1].toString();
                    var flag = false;

                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').eq(1).text()
                        if (csap.trim() == csaptabla.trim()) {
                            $(this).children('td').find("[class='select-tabla-talla']").val(talla);
                            flag = true
                        }
                    })

                    if (flag) {
                        $txtBusqueda.val("");
                        $("#txtTalla").val("");
                        $dataecontrada.val("");
                        $txtBusqueda.attr("disabled", false);
                        $txtBusqueda.focus();
                        eventosIncrustados.CalcularIndex();

                        $("#txtTalla").removeClass('error');
                        $("#prueba").removeClass('error');
                        $(".VCNTDDA").remove();

                        $("#txtBusqueda").removeClass('error');
                        $(".VtxtBusqueda").remove();
                        return;
                    }

                    //$('#divtabla').css("visibility", "visible ");
                    var trs = $("#tabla_articuloadd tbody tr").length + 1;

                    eventosIncrustados.formatoFilaTabla(trs, 0, id.trim(), csap.trim(), desc.trim(), und.trim(), talla.trim(), false, false);
                    $txtBusqueda.val("");
                    $("#txtTalla").val("");
                    $dataecontrada.val("");
                    $txtBusqueda.attr("disabled", false);
                    $txtBusqueda.focus();
                }
                eventosIncrustados.CalcularIndex();
            })

        },
        botonLimpiar: function () {
            $btnKitLimpiar.on("click", function () {
                $txtBusqueda.val("");
                $dataecontrada.val("");
                $txtBusqueda.attr("disabled", false);
                $txtBusqueda.focus();
            })
        },
        solonumero: function () {
            $("#txtTalla").on("keyup", function () {
                var talla = $("#txtTalla").val();
                if (talla != "") {
                    $("#txtTalla").removeClass('error');
                    $("#prueba").removeClass('error');
                    $(".VCNTDDA").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
            $txtBusqueda.on("keyup", function () {
                var txtBusqueda = $txtBusqueda.val();
                if (txtBusqueda != "") {
                    $txtBusqueda.removeClass('error');
                    $(".VtxtBusqueda").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
        },
        formatoFilaTabla: function (trs, id, idarticulo, csap, desc, unidadmedida, talla, obteniendo = false, FlagEstado = false) {
            var $filanodatos = $("#tabla_articuloadd tbody").find('.noData').parent();
            if ($filanodatos) {
                $filanodatos.remove();
            }

            //FUSION DE LAS 3
            var htmlTags = "<tr>" +
                '<td class="text-center"><label>' + trs + '</label></td>' +
                '<td class="text-center"><label class="csap">' + csap + '</label>' +
                '<input class="idkitdetalle" type="hidden" value="' + id + '">' +
                '<input class="idartclo" type="hidden" value="' + idarticulo + '">' +

                '</td > ' +
                '<td class="text-left"><label>' + desc + '</label></td>' +
                '<td class="text-center"><label>' + unidadmedida + '</label></td>' +
                ///
                '<td class="text-center"><select style="width:200px" data-anterior="' + talla + '" class="form-control form-control-sm select-tabla-talla tlla select-tabla-talla-' + idarticulo + '"></select></td>' +
                //'<td class=""><input class="cntdd cntdd-' + idarticulo + ' text-center form-control form-control-sm" disabled="disabled" data-anterior="' + cant + '" type="number" min="0" value="' + cant + '"/><input class="csto" type="hidden" value="' + costo + '"></td>' +
                '<td class="text-center">';
            if (!FlagEstado) {
                htmlTags += '<button type="button" title="Editar" data-id="' + idarticulo + '" class="btn btn-info btn-xs btn-editar-tabla "><span><i class="la la-edit"></i></button> ';
                //
                if (obteniendo) {
                    htmlTags += ' <button id="delete_' + id + '" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                } else {
                    htmlTags += ' <button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                }
            } else {
                htmlTags += '';
            }
            htmlTags += '</td>' +
                '</tr>';
            $('#tabla_articuloadd tbody').append(htmlTags);

            $(".select-tabla-talla-" + idarticulo).LlenarSelectGD("GDTLLA");
            $(".select-tabla-talla-" + idarticulo).val(talla);
            var opt = selects.opciones;
            opt.disabled = true;
            $(".select-tabla-talla-" + idarticulo).select2(opt);
        },

        init: function () {
            eventosIncrustados.CheckTalla();
            eventosIncrustados.CalcularIndex();
            eventosIncrustados.EliminarFila();
            eventosIncrustados.botonBuscar();
            eventosIncrustados.botonAgregar();
            eventosIncrustados.botonAgregarArticulo();
            eventosIncrustados.botonLimpiar();
            eventosIncrustados.botonGrabar();
            eventosIncrustados.solonumero();
            eventosIncrustados.editarCantidadFila();
        }
    }

    var autocomplete = {
        init: function () {
            $txtBusqueda.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/searchcontalla",
                        dataType: "json",
                        data: {
                            term: request.term,
                            flagKit: false,
                        },
                        success: function (data) {
                            //console.log(data);
                            if (data.length == 1) {
                                $txtBusqueda.val(data[0].label);
                                $dataecontrada.val(data[0].value2);
                                $txtBusqueda.attr("disabled", true);
                                $("#txtTalla").focus();
                            } else {
                                response(data);
                            }
                        },
                        error: function (xhr, status, error) {
                            alert(xhr + " " + status + " " + error);
                        },
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    $txtBusqueda.val(ui.item.label);
                    $dataecontrada.val(ui.item.value2);
                    $txtBusqueda.attr("disabled", true);
                    $("#txtTalla").focus();

                    return false;
                }
            });
        }
    }

    var obtenerDatos = {
        obtenerDetalle: function (FlagEstado) {
            $.get("/obtener-tallas-lista", { idTrabajador: $idtrbjdr.val() })
                .done(function (data) {
                    if (data.length) {
                        $('#tabla_articuloadd tbody').html('');
                        for (var i = 0; i < data.length; i++) {
                            var trs = i + 1;
                            eventosIncrustados.formatoFilaTabla(trs, data[i].id, data[i].idartclo, data[i].csap, data[i].dscrpcnartclo, data[i].gdunddmdda, data[i].tlla, true, FlagEstado);
                        }
                    } else {
                        $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
                    }
                });
        },
        init: function () {
            var FlagEstado = false
            $.get("/trabajador/obtener", { id: $idtrbjdr.val() })
                .done(function (data) {
                    //console.log(data);
                    $DSCRPCN.val(data.nmbrs);
                    $lblTrabajador.text(data.nmbrs);
                    $idgrupocargo = data.idgrpo;
                    $grupocargo = data.grpo;
                    $lblTitulo.text("ARTÍCULOS POR GRUPO CARGO " + $grupocargo);
                    $
                    if (data.gdestdo == 'I') {
                        $DivBusqueda.remove();
                        $btnGuardar.remove();
                        FlagEstado = true;
                    }

                    obtenerDatos.obtenerDetalle(FlagEstado);
                });
        }
    };

    var selects = {
        opciones: {
            disabled:false,
            language: {
                noResults: function () {
                    return "No hay resultados";
                },
                searching: function () {
                    return "Buscando..";
                },
                loadingMore: function () {
                    return 'Cargando más resultados..';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;
                    var message = 'Por favor ingresar ' + remainingChars + ' o más caracteres';

                    return message;
                },
            },
        },
        init: function () {
            $selectTalla.LlenarSelectGD("GDTLLA");
        }
    };

    var widget = {
        config: function () {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);
                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";

                    this.input = $("<input id='txtTalla'>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip()

                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function () {
                    var input = this.input,
                        wasOpen = false;

                    $("<a id='prueba'>")
                        .attr("tabIndex", -1)
                        .attr("title", "Ver todo")
                        //.tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .on("mousedown", function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .on("click", function () {
                            input.trigger("focus");

                            // Close if already visible
                            if (wasOpen) {
                                return;
                            }

                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                },

                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },

                _removeIfInvalid: function (event, ui) {

                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if (valid) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                        .val("")
                        .attr("title", "La talla '" + value + "' no existe.")
                        .tooltip("open");
                    this.element.val("");
                    this._delay(function () {
                        this.input.tooltip("close").attr("title", "");
                    }, 2500);
                    this.input.autocomplete("instance").term = "";
                },

                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        },
        init: function () {
            this.config();
            $("#combobox").combobox();
            /* $(".select-talla-grilla").combobox();*/
        }
    }

    return {
        init: function () {
            $DivRstrc.remove();
            selects.init();
            tablaArticulo.inicializador();
            modalArticulo.init();
            autocomplete.init();
            eventosIncrustados.init();
            obtenerDatos.init();
            widget.init();

            $('#tabla_articuloadd tbody').html('');
            $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
        }
    };
}();

$(() => {
    InicializarTalla.init();
})

