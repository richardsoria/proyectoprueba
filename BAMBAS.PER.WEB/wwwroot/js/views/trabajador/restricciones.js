﻿
var InicializarTalla = function () {
    var $idtrbjdr = $("#IDTRBJDR");
    var $Divtlls = $("#Divtlls");
    var $DivRstrc = $("#DivRstrc");
    var $lblTitulo = $("#lblTitulo");
    var $lblTrabajador = $("#lblTrabajador");
    var $DSCRPCN = $("#DSCRPCN");
    var $tablaArticulo = $("#tabla_articulo");
    var $modalArticulo = $("#modal_Articulo");
    var $idgrupocargo = 0;
    var $grupocargo = "";
    var $DivBusqueda = $("#DivBusqueda");
    var $txtBusqueda = $("#txtBusqueda");
    var $ValtxtBusqueda = $("#ValtxtBusqueda");
    var $ValCNTDD = $("#ValCNTDD");
    var $dataecontrada = $("#dataecontrada");
    var $btnAgregarArticulos = $('#btnAgregarArticulos');
    var $btnKitBuscar = $("#btnKitBuscar");
    var $btnAgregar = $("#btnKitAgregar");
    var $btnKitLimpiar = $("#btnKitLimpiar");
    var $btnGuardar = $("#btnGrabarTalla");

    var tablaArticulo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/obtener-articulos-restricciones",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.flagrestrccion = $('.chkRstrc').is(':checked');
                    data.idgrupocargo = $idgrupocargo;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Unid. Med.",
                    className: "text-center",
                    data: "gdunddmdda",
                    orderable: false
                },
                {
                    title: "Observación",
                    className: "text-center",
                    data: null,
                    width: '20%',
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        var ObsMemory = tablaArticulo.memory_art.find(x => x.CSAP == data.csap);
                        if (ObsMemory) {
                            tpm += ` <input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-idartclo="${data.id}" type="text" maxlength="150" class="form-control text-uppercase form-control-sm obs alfaNumCharEsp" value="${ObsMemory.OBS}">`;
                        } else {
                            tpm += `<input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-idartclo="${data.id}" type="text" maxlength="150" class="form-control text-uppercase form-control-sm obs alfaNumCharEsp" >`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        memory_art: [],
        eventos: function () {
            tablaArticulo.objeto.on("keyup", ".obs", function () {
                var csap = $(this).data("csap");
                var descr = $(this).data("dscrpcn");
                var und = $(this).data("gdunddmdda");
                var obs = $(this).val();
                var idart = $(this).data("idartclo");

                var delete_articulo = false;
                if (obs == '') {
                    delete_articulo = true;
                }
                var new_articulo = true;
                for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                    if (tablaArticulo.memory_art[i].CSAP == csap) {
                        new_articulo = false;
                        if (delete_articulo) {
                            tablaArticulo.memory_art.splice(i, 1);
                        } else {
                            tablaArticulo.memory_art[i].OBS = obs.toUpperCase();
                        }
                    }
                }
                if (new_articulo) {
                    tablaArticulo.memory_art.push({
                        CSAP: csap,
                        DESCR: descr,
                        UND: und,
                        OBS: obs,
                        IDART: idart,
                    });
                }
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
            tablaArticulo.eventos();
        }
    };

    var modalArticulo = {
        eventos: {
            onHide: function () {
                $modalArticulo.on('hidden.bs.modal', function () {
                    modalArticulo.eventos.reset();
                })
            },
            onShow: function () {
                $modalArticulo.on('shown.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                })
            },
            reset: function () {
                tablaArticulo.memory_art = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var eventosIncrustados = {
        editarCantidadFila: function () {
            $(document).on('click', '.btn-editar-tabla', function (event) {
                //
                var id = $(this).data("id");
                var btn = $(this);
                var hasClass = $(this).hasClass("btn-info");
                var input = ".observaciones-" + id;
                if (hasClass) {
                    $("#tabla_articuloadd").find(input).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anterior = $("#tabla_articuloadd").find(input).data("anterior");
                    $("#tabla_articuloadd").find(input).val(anterior);
                    $("#tabla_articuloadd").find(input).attr("disabled", true);
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
                //
            });
        },
        CheckTalla: function () {
            $('.chkRstrc').on('change', function () {
                if ($('.chkRstrc').is(':checked')) {
                    $lblTitulo.text("ARTÍCULOS QUE MANEJAN RESTRICCIONES");
                } else {
                    $lblTitulo.text("ARTÍCULOS POR GRUPO CARGO " + $grupocargo);
                }
                tablaArticulo.reload();
            });
        },
        CalcularIndex: function () {
            var hayfilas = $("#tabla_articuloadd tbody tr").length > 0;
            if (!hayfilas) {
                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
            $("#tabla_articuloadd tbody tr").each(function (index) {
                var trs = index + 1;
                $(this).children('td').eq(0).text(trs);
            })
        },
        EliminarFila: function () {
            
            $(document).on('click', '.BorrarFilaSubMenu', function (event) {
                event.preventDefault();
                var fila = $(this).closest('tr');
                var idkitdetalle = $(this).attr("id");
                let arrayid = idkitdetalle.split('_');
                var id = arrayid[1];

                if ($idtrbjdr.val()) {
                    if (id > 0) {
                        Swal.fire({
                            title: "¿Quiere modificar el estado del registro?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Aceptar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: "/eliminar-detalle-restricciones",
                                    type: "POST",
                                    data: {
                                        id: id
                                    }
                                }).done(function () {
                                    Swal.fire({
                                        icon: "success",
                                        allowOutsideClick: false,
                                        title: "Éxito",
                                        text: "Registro modificado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then((result) => {
                                        fila.remove();
                                        eventosIncrustados.CalcularIndex();
                                    });
                                }).fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al modificar el registro.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                });
                            }
                        });
                    } else {
                        fila.remove();
                    }

                } else {
                    fila.remove();
                }
                eventosIncrustados.CalcularIndex();
            });
        },
        botonAgregarArticulo: function () {
            $btnAgregarArticulos.on("click", function () {
                for (var i = 0; i < tablaArticulo.memory_art.length; i++) {
                    var art_in_table = false;
                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').find("[class='csap']").text();
                        if (tablaArticulo.memory_art[i].CSAP == csaptabla.trim()) {
                            art_in_table = true;
                            $(this).children('td').find("[class='obs']").val(tablaArticulo.memory_art[i].OBS);
                        }
                    })
                    if (!art_in_table) {
                        var trs = i + 1
                        eventosIncrustados.formatoFilaTabla(trs, 0, tablaArticulo.memory_art[i].IDART, tablaArticulo.memory_art[i].CSAP, tablaArticulo.memory_art[i].DESCR, tablaArticulo.memory_art[i].UND, tablaArticulo.memory_art[i].OBS, false, false);

                        //var htmlTags = "<tr>" +
                        //    '<td class="text-center"><label>' + trs + '</label></td>' +
                        //    '<td class="text-center"><label class="csap">' + tablaArticulo.memory_art[i].CSAP + '</label></td>' +
                        //    '<td class="text-left"><label>' + tablaArticulo.memory_art[i].DESCR + '</label></td>' +
                        //    '<td class="text-center"><label>' + tablaArticulo.memory_art[i].UND + '</label></td>' +
                        //    '<td class="text-left"><label class="obs">' + tablaArticulo.memory_art[i].OBS + '</label><input class="idartclo" type="hidden" value="' + tablaArticulo.memory_art[i].IDART + '"></td>' +
                        //    '<td class="text-center">' +
                        //    '<button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>' +
                        //    '</td>' +
                        //    '</tr>';
                    }
                }
                tablaArticulo.memory_art = [];
                $modalArticulo.modal('hide');
                eventosIncrustados.CalcularIndex();
            })

        },
        botonBuscar: function () {
            $btnKitBuscar.on("click", function () {
                tablaArticulo.memory_art = [];
                tablaArticulo.reload();
            })
        },
        botonGrabar: function () {
            $btnGuardar.on("click", function () {

                var fila = $("#tabla_articuloadd tbody tr").length;

                if (fila == 0) {
                    Swal.fire({
                        title: "No se puede crear el Restricciones Por Trabajador sin artículos asociados.",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                $btnGuardar.attr("disabled", true);
                var articulos = [];
                $("#tabla_articuloadd tbody tr").each(function (index) {

                    obs = $(this).find(".obs").val();
                    id = $(this).find("[class='idartclo']").val();
                    articulos.push(
                        {
                            TLLATRBJDR: obs,
                            ARTCLO: id,
                        }
                    );
                })
                //console.log(JSON.stringify(articulos));
                var formData = new FormData();
                formData.append("TLLADTO", JSON.stringify(articulos));
                formData.append("IDTRBJDR", $idtrbjdr.val());
                //console.log(formData);
                $.ajax({
                    url: "/actualizar-trabajadorrestricciones",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false
                })
                    .done(function (e) {
                        Swal.fire({
                            title: "Éxito",
                            icon: "success",
                            allowOutsideClick: false,
                            text: "Guardado satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then(function () {
                            //window.location.href = `/trabajador/restricciones/${$idtrbjdr.val()}`;
                            obtenerDatos.obtenerDetalle();

                            eventosIncrustados.CalcularIndex();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al guardar los datos.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    })
                    .always(function () {
                    });


                $btnGuardar.attr("disabled", false);

            })
        },
        botonAgregar: function () {
            $btnAgregar.on("click", function () {

                var obs = $("#txtObs").val();
                var txtbusqueda = $txtBusqueda.val();

                if (obs == "" && txtbusqueda != "") {
                    $("#txtObs").removeClass('error');
                    $(".VCNTDDA").remove();

                    $("#txtObs").addClass('error');
                    $ValCNTDD.after('<label id="CNTDD-error" class="error VCNTDDA" >Ingrese Observación</label>');

                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    return;
                }

                if (obs != "" && txtbusqueda == "") {
                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    $("#txtBusqueda").addClass('error');
                    $ValtxtBusqueda.after('<label id="txtBusqueda-error" class="error VtxtBusqueda">Ingrese Descripción </label>');

                    $("#txtObs").removeClass('error');
                    $(".VCNTDDA").remove();
                    return;
                }

                if (obs == "" && txtbusqueda == "") {
                    $("#txtObs").removeClass('error');
                    $(".VCNTDDA").remove();

                    $("#txtObs").addClass('error');
                    $ValCNTDD.after('<label id="CNTDD-error" class="error VCNTDDA" >Ingrese Observación</label>');

                    $("#txtBusqueda").removeClass('error');
                    $(".VtxtBusqueda").remove();

                    $("#txtBusqueda").addClass('error');
                    $ValtxtBusqueda.after('<label id="txtBusqueda-error" class="error VtxtBusqueda">Ingrese Descripción </label>');

                    return;
                }

                if (obs != "" && txtbusqueda != "") {
                    $("#txtObs").removeClass('error');
                    $(".VCNTDDA").remove();
                    $(".VtxtBusqueda").removeClass();
                    $txtBusqueda.remove('error');
                    //  VIENE CODIGO SAP - DESCRIPCION
                    var dataencontrada = $dataecontrada.val()           //  VIENE EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA

                    Arraycsapdesc = txtbusqueda.split('-'); // SEPARA  CODIGO SAP - DESCRIPCION
                    var csap = Arraycsapdesc[0].toString();
                    var desc = Arraycsapdesc[1].toString();

                    Arrayidgduni = dataencontrada.split('-');  //  SEPARA EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA
                    var id = Arrayidgduni[0].toString();
                    var und = Arrayidgduni[1].toString();
                    var flag = false;

                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').eq(1).text()
                        if (csap.trim() == csaptabla.trim()) {
                            $(this).children('td').find("[class='obs']").text(obs);
                            flag = true
                        }
                    })

                    if (flag) {
                        $txtBusqueda.val("");
                        $("#txtObs").val("");
                        $dataecontrada.val("");
                        $txtBusqueda.attr("disabled", false);
                        $txtBusqueda.focus();
                        eventosIncrustados.CalcularIndex();

                        $("#txtObs").removeClass('error');
                        $(".VCNTDDA").remove();

                        $("#txtBusqueda").removeClass('error');
                        $(".VtxtBusqueda").remove();
                        return;
                    }

                    //$('#divtabla').css("visibility", "visible ");
                    var trs = $("#tabla_articuloadd tbody tr").length + 1;
                    //var htmlTags = "<tr>" +
                    //    '<td class="text-center"><label>' + trs + '</label></td>' +
                    //    '<td class="text-center"><label class="csap">' + csap.trim() + '</label></td>' +
                    //    '<td class="text-left"><label>' + desc.trim() + '</label></td>' +
                    //    '<td class="text-center"><label>' + und.trim() + '</label></td>' +
                    //    '<td class="text-left"><label class="obs">' + obs.trim() + '</label><input class="idartclo" type="hidden" value="' + id.trim() + '"></td>' +
                    //    '<td class="text-center">' +
                    //    '<button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>' +
                    //    '</td>' +
                    //    '</tr>';
                    //$('#tabla_articuloadd tbody').append(htmlTags);
                    eventosIncrustados.formatoFilaTabla(trs, 0, id.trim(), csap.trim(), desc.trim(), und.trim(), obs.trim(), false, false);

                    $txtBusqueda.val("");
                    $("#txtObs").val("");
                    $dataecontrada.val("");
                    $txtBusqueda.attr("disabled", false);
                    $txtBusqueda.focus();
                }
                eventosIncrustados.CalcularIndex();
            })

        },
        botonLimpiar: function () {
            $btnKitLimpiar.on("click", function () {
                $txtBusqueda.val("");
                $dataecontrada.val("");
                $txtBusqueda.attr("disabled", false);
                $txtBusqueda.focus();
            })
        },
        solonumero: function () {
            $("#txtObs").on("keyup", function () {
                var obs = $("#txtObs").val();
                if (obs != "") {
                    $("#txtObs").removeClass('error');
                    $(".VCNTDDA").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
            $txtBusqueda.on("keyup", function () {
                var txtBusqueda = $txtBusqueda.val();
                if (txtBusqueda != "") {
                    $txtBusqueda.removeClass('error');
                    $(".VtxtBusqueda").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
        },
        formatoFilaTabla: function (trs, id, idarticulo, csap, desc, unidadmedida, observaciones, obteniendo = false, FlagEstado = false) {
            var $filanodatos = $("#tabla_articuloadd tbody").find('.noData').parent();
            if ($filanodatos) {
                $filanodatos.remove();
            }

            //FUSION DE LAS 3
            var htmlTags = "<tr>" +
                '<td class="text-center"><label>' + trs + '</label></td>' +
                '<td class="text-center"><label class="csap">' + csap + '</label>' +
                '<input class="idkitdetalle" type="hidden" value="' + id + '">' +
                '<input class="idartclo" type="hidden" value="' + idarticulo + '">' +

                '</td > ' +
                '<td class="text-left"><label>' + desc + '</label></td>' +
                '<td class="text-center"><label>' + unidadmedida + '</label></td>' +
                ///
                '<td class=""><input class="obs observaciones-' + idarticulo + ' text-uppercase alfaNumCharEsp  text-center form-control form-control-sm" disabled="disabled" data-anterior="' + observaciones + '" value="' + observaciones + '"/></td>' +
                '<td class="text-center">';
            if (!FlagEstado) {
                htmlTags += '<button type="button" title="Editar" data-id="' + idarticulo + '" class="btn btn-info btn-xs btn-editar-tabla "><span><i class="la la-edit"></i></button> ';
                //
                if (obteniendo) {
                    htmlTags += ' <button id="delete_' + id + '" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                } else {
                    htmlTags += ' <button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                }
            } else {
                htmlTags += '';
            }
            htmlTags += '</td>' +
                '</tr>';
            $('#tabla_articuloadd tbody').append(htmlTags);
        },
        init: function () {
            eventosIncrustados.CheckTalla();
            eventosIncrustados.CalcularIndex();
            eventosIncrustados.EliminarFila();
            eventosIncrustados.botonBuscar();
            eventosIncrustados.botonAgregar();
            eventosIncrustados.botonAgregarArticulo();
            eventosIncrustados.botonLimpiar();
            eventosIncrustados.botonGrabar();
            eventosIncrustados.solonumero();
            eventosIncrustados.editarCantidadFila();
        }
    }

    var autocomplete = {
        init: function () {
            $txtBusqueda.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/searchconrestricciones",
                        dataType: "json",
                        data: {
                            term: request.term,
                            flagKit: false,
                        },
                        success: function (data) {
                            //console.log(data);
                            if (data.length == 1) {
                                $txtBusqueda.val(data[0].label);
                                $dataecontrada.val(data[0].value2);
                                $txtBusqueda.attr("disabled", true);
                                $("#txtObs").focus();
                            } else {
                                response(data);
                            }
                        },
                        error: function (xhr, status, error) {
                            alert(xhr + " " + status + " " + error);
                        },
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    $txtBusqueda.val(ui.item.label);
                    $dataecontrada.val(ui.item.value2);
                    $txtBusqueda.attr("disabled", true);
                    $("#txtObs").focus();

                    return false;
                }
            });
        }
    }

    var obtenerDatos = {
        obtenerDetalle: function (FlagEstado) {
            $.get("/obtener-restricciones-lista", { idTrabajador: $idtrbjdr.val() })
                .done(function (data) {
                    if (data.length) {
                        $('#tabla_articuloadd tbody').html('');
                        for (var i = 0; i < data.length; i++) {
                            var trs = i + 1;
                            eventosIncrustados.formatoFilaTabla(trs, data[i].id, data[i].idartclo, data[i].csap, data[i].dscrpcnartclo, data[i].gdunddmdda, data[i].obsrvcn, true, FlagEstado);
                        }
                    } else {
                        $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
                    }
                });
        },
        init: function () {
            var FlagEstado = false
            $.get("/trabajador/obtener", { id: $idtrbjdr.val() })
                .done(function (data) {
                    //console.log(data);
                    $DSCRPCN.val(data.nmbrs);
                    $lblTrabajador.text(data.nmbrs);
                    $idgrupocargo = data.idgrpo;
                    $grupocargo = data.grpo;
                    $lblTitulo.text("ARTÍCULOS POR GRUPO CARGO " + $grupocargo);

                    if (data.gdestdo == 'I') {
                        $DivBusqueda.remove();
                        $btnGuardar.remove();
                        FlagEstado = true;
                    }

                    obtenerDatos.obtenerDetalle(FlagEstado);
                });
        }
    };

    return {
        init: function () {
            $Divtlls.remove();
            tablaArticulo.inicializador();
            modalArticulo.init();
            autocomplete.init();
            eventosIncrustados.init();
            obtenerDatos.init();

            $('#tabla_articuloadd tbody').html('');
            $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
        }
    };
}();

$(() => {
    InicializarTalla.init();
})