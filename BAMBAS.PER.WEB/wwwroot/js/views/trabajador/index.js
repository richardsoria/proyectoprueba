﻿var InicializarTrabajador = function () {
    var $tablaTrabajador = $("#tabla_trabajadores");

    var $accesoAgregarTrabajador = $("#accesoAgregarTrabajador");
    var $accesoEditarTrabajador = $("#accesoEditarTrabajador");
    var $accesoEliminarTrabajador = $("#accesoEliminarTrabajador");

    var $accesoTalla = $("#accesoTalla");

    var $btnTrabajador = $("#btnTrabajador");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarTrabajador.val() == "False") {
                $btnTrabajador.remove();
            }
        }
    };

    var tablaTrabajador = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/trabajador/get",
                type: "GET",
                data: function (data) {
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Nombres y Apellidos / Razón social",
                    className: "text-left",
                    data: "nmbrs",
                    orderable: false
                },
                {
                    title: "Contratista",
                    className: "text-left",
                    data: "cntrtsta",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Rest.",
                    data: null,
                    width: '5%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        //if ($accesoTalla.val() == "True") {
                        tpm += `<button type="button" class="btn btn-warning btn-xs btn-restricciones" data-id="${data.id}" title="Restricciones"><span><i class="la la-exclamation-circle"></i></span></button>`;
                        //}
                        return tpm;
                    }
                },
                {
                    title: "Talla",
                    data: null,
                    width: '5%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoTalla.val() == "True") {
                            tpm += `<button type="button" class="btn btn-secondary btn-xs btn-tallas" data-id="${data.id}" title="Talla"><span><i class="la la-gear"></i></span></button>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '10%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarTrabajador.val() == "True") {
                            tpm += ` <button type="button" class="btn btn-info btn-xs btn-editar" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarTrabajador.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaTrabajador.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/trabajador/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaTrabajador.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaTrabajador.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                var url = `/trabajador/editar/${id}`;
                RedirectWithSubfolder(url);
            });

            tablaTrabajador.objeto.on("click", ".btn-tallas", function () {
                var id = $(this).data("id");
                var url = `/trabajador/tallas/${id}`;
                RedirectWithSubfolder(url);
            });

            tablaTrabajador.objeto.on("click", ".btn-restricciones", function () {
                var id = $(this).data("id");
                var url = `/trabajador/restricciones/${id}`;
                RedirectWithSubfolder(url);
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaTrabajador.objeto = $tablaTrabajador.DataTable(tablaTrabajador.opciones);
            tablaTrabajador.eventos();
        }
    };

    var selects = {
        init: function () {

        }
    };
    return {
        init: function () {
            selects.init();
            tablaTrabajador.inicializador();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarTrabajador.init();
})

