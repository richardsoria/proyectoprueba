using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.WEB.Filters;
using BAMBAS.Negocios.Persona;
using BAMBAS.Negocios.General.Seguridad;
using BAMBAS.Negocios.Sunat;
using BAMBAS.Negocios.SuSalud;
//using BAMBAS.Negocios.Sunat;
//using BAMBAS.Negocios.SuSalud;
using Microsoft.Extensions.DependencyInjection;
using SuSalud.Servicios;
using Sunat.Servicios;

namespace BAMBAS.PER.WEB.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddRepository(this IServiceCollection serviceCollection)
        {
        }
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            //Base
            //serviceCollection.AddTransient<IEmailService, EmailService>();
            //serviceCollection.AddTransient<IEmailTemplateService, EmailTemplateService>();
            serviceCollection.AddTransient<ISelect2Service, Select2Service>();
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IPaginationService, PaginationService>();
            serviceCollection.AddTransient<IViewRenderService, ViewRenderService>();
            

            //serviceCollection.AddTransient<ICloudStorageService, CloudStorageService>(); 

            //Proxies
            //persona
            serviceCollection.AddHttpClient<ICorreoProxy, CorreoProxy>();
            serviceCollection.AddHttpClient<IDireccionProxy, DireccionProxy>();
            serviceCollection.AddHttpClient<IDocumentoProxy, DocumentoProxy>();
            serviceCollection.AddHttpClient<IPersonaProxy, PersonaProxy>();
            serviceCollection.AddHttpClient<ITelefonoProxy, TelefonoProxy>();
            serviceCollection.AddHttpClient<IArchivoProxy, ArchivoProxy>();
            serviceCollection.AddHttpClient<ITrabajadorProxy, TrabajadorProxy>();

            //seguridad
            serviceCollection.AddHttpClient<IPerfilObjetoProxy, PerfilObjetoProxy>();
            serviceCollection.AddHttpClient<IGrupoDatoProxy, GrupoDatoProxy>();
            serviceCollection.AddHttpClient<IParametroProxy, ParametroProxy>();
            serviceCollection.AddHttpClient<IUbigeoProxy, UbigeoProxy>();
            serviceCollection.AddHttpClient<IUsuarioProxy, UsuarioProxy>();
            serviceCollection.AddHttpClient<IAreaProxy, AreaProxy>();
            serviceCollection.AddHttpClient<ISuperintendenciaProxy, SuperintendenciaProxy>();
            serviceCollection.AddHttpClient<IGerenciaProxy, GerenciaProxy>();
            serviceCollection.AddHttpClient<IGrupoCargoProxy, GrupoCargoProxy>();
            serviceCollection.AddHttpClient<ICargoProxy, CargoProxy>();

            serviceCollection.AddHttpClient<IPosicionProxy, PosicionProxy>();
            serviceCollection.AddHttpClient<ICentroCostoProxy, CentroCostoProxy>();
            serviceCollection.AddHttpClient<IUnidadOrganizacionalProxy, UnidadOrganizacionalProxy>();
            serviceCollection.AddHttpClient<IArticuloProxy, ArticuloProxy>();
            serviceCollection.AddHttpClient<ITrabajadorTallaProxy, TrabajadorTallaProxy>();
            serviceCollection.AddHttpClient<ITrabajadorRestriccionProxy, TrabajadorRestriccionProxy>();


            //susalud
            serviceCollection.AddTransient<ISuSaludSoap, SuSaludSoap>();
            serviceCollection.AddTransient<ISuSaludProxy, SuSaludProxy>();

            //sunat
            serviceCollection.AddTransient<ISunatSoap, SunatSoap>();
            serviceCollection.AddTransient<ISunatProxy, SunatProxy>();

            serviceCollection.AddScoped<AuthLogin>();

        }
    }
}
