﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.PER.WEB.Helpers
{
    public static class ControlesVistas
    {
        public const string VistaPersona = "PERSONA";
        public const string VistaTrabajador = "TRABAJADOR";
        public static class Persona
        {
            public const string AgregarPersona = "AGRGRPRSNA";
            public const string EditarPersona = "EDTRPRSNA";
            public const string EliminarPersona = "ELMNRPRSNA";

            public const string AgregarDocumento = "AGRGRDCMNTO";
            public const string EditarDocumento = "EDTRDCMNTO";
            public const string EliminarDocumento = "ELMNRDCMNTO";

            public const string AgregarDireccion = "AGRGRDRCCN";
            public const string EditarDireccion = "EDTRDRCCN";
            public const string EliminarDireccion = "ELMNRDRCCN";

            public const string AgregarTelefono = "AGRGRTLFNO";
            public const string EditarTelefono = "EDTRTLFNO";
            public const string EliminarTelefono = "ELMNRTLFNO";

            public const string AgregarCorreo = "AGRGRCRRO";
            public const string EditarCorreo = "EDTRCRRO";
            public const string EliminarCorreo = "ELMNRCRRO";

            public const string AgregarArchivo = "AGRGRARCHVO";
            public const string EliminarArchivo = "ELMNRARCHVO";
            public const string DescargarArchivo = "DSCRGRARCHVO";
        }

        public static class Trabajador
        {
            public const string AgregarTrabajador = "AGRGRTRBJDR";
            public const string EditarTrabajador = "EDTRTRBJDR";
            public const string EliminarTrabajador = "ELMNRTRBJDR";

            public const string TallaTrabajador = "TLLATRBJDR";
        }
    }
}
