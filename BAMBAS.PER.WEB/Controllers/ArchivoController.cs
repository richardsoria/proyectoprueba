﻿using System.Threading.Tasks;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Persona.Archivo;
using BAMBAS.Negocios.Persona;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BAMBAS.PER.WEB.Filters;
using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace BAMBAS.PER.WEB.Controllers
{
    [Route("archivo")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class ArchivoController : Controller
    {
        private readonly IDataTableService _dataTableService;
        private readonly IArchivoProxy _archivoproxy;

        public ArchivoController(
            IDataTableService datatableservice,
            IArchivoProxy archivoproxy
            )
        {
            _archivoproxy = archivoproxy;
            _dataTableService = datatableservice;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("insertarArchivo")]
        public async Task<IActionResult> InsertarArchivo(ArchivoDtoCustom entidad)
        {
            entidad.PATHARCHV = FileToByteArray(entidad.ARCHVO);
            entidad.TPOARCHV = entidad.ARCHVO.ContentType;
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();
            var ret = await _archivoproxy.Insertar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("listarArchivo")]
        public async Task<IActionResult> ListarArchivo(string idprsna)
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _archivoproxy.ObtenerDataTable(parameters, idprsna);
            return Ok(retorno); ;
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(string idprsna, string id)
        {
            var retono = await _archivoproxy.Listar(idprsna, id);
            return Ok(retono);
        }

        [HttpGet("obtenerArchivo")]
        public async Task<IActionResult> ObtenerArchivo(string idprsna, string id)
        {
            var retorno = await _archivoproxy.Obtener(idprsna, id);
            return Ok(retorno); ;
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarArchivo(string idprsna, int id)
        {
            ArchivoDto entidad = new ArchivoDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var ret = await _archivoproxy.Eliminar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }


        [HttpGet("descargar")]
        public async Task<IActionResult> DescargarArchivo(string idprsna, string id)
        {
            var retorno = await _archivoproxy.Obtener(idprsna, id);
            var bytes = Convert.FromBase64String(retorno.PATHARCHV);
            return File(bytes, retorno.TPOARCHV);
        }

        private string FileToByteArray(IFormFile archivo)
        {
            ////byte[] archivoByte = System.IO.File.ReadAllBytes(fileName);
            var bytes = ReadToEnd(archivo.OpenReadStream());
            string archivoBase64 = Convert.ToBase64String(bytes);
            return archivoBase64;
        }
        private static byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
