﻿using BAMBAS.CORE.Extensions;  //
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Persona.Persona;  //
using BAMBAS.Negocios.Persona;
using BAMBAS.Negocios.Sunat;
using BAMBAS.Negocios.SuSalud;
using Microsoft.AspNetCore.Authorization;
using BAMBAS.PER.WEB.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Http;
using System;

namespace BAMBAS.PER.WEB.Controllers
{
    [Route("Persona")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]
    public class PersonaController : Controller

    {
        private readonly IPersonaProxy _personaProxy;
        private readonly IDataTableService _dataTableService;
        private readonly ISuSaludProxy _suSaludProxy;
        private readonly ISunatProxy _sunatproxy;
        //CONSTRUCTOR
        public PersonaController(IDataTableService dataTableService, 
            IPersonaProxy personaProxy, 
            ISuSaludProxy suSaludProxy, 
            ISunatProxy sunatproxy)
        {
            _personaProxy = personaProxy;
            _dataTableService = dataTableService;
            _suSaludProxy = suSaludProxy;
            _sunatproxy = sunatproxy;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("listar-persona")]
        public async Task<IActionResult> listarPersona(string p_sTPersona, string p_sTDocumento, string p_sNDocumento, string p_sDatos, string p_sEstado, bool soloContratistas, bool soloTrabajadores)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _personaProxy.ObtenerDataTable(parametrosDT, p_sTPersona, p_sTDocumento, p_sNDocumento, p_sDatos, p_sEstado, soloContratistas, soloTrabajadores);
            return Ok(retorno);
        }

        [HttpGet("listar-colaboradores")]
        public async Task<IActionResult> ListarColaboradores(string idprsna)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _personaProxy.ObtenerColaboradoresDataTable(parametrosDT, idprsna);
            return Ok(retorno);
        }

        [HttpGet("listar")]
        public async Task<IActionResult> listar(string tPersona, string p_sTDocumento, string p_sNDocumento, string p_sDatos, string p_sDesde, string p_sHasta, string p_sEstado, bool p_bContratista)
        {
            var retorno = await _personaProxy.Listar(tPersona, p_sTDocumento, p_sNDocumento, p_sDatos, p_sDesde, p_sHasta, p_sEstado, p_bContratista);
            return Ok(retorno);
        }

        [HttpGet("agregar")]
        [HttpGet("editar/{id}")]
        public IActionResult AddPersona(int? id = null)
        {
            return View(id);
        }

        [HttpPost("insertarPersonas")]
        public async Task<IActionResult> insertarPersona(PersonaConImagenDto entidad)
        {
            if (entidad.IMGNPRSNA != null)
            {
                entidad.FTO = FileToByteArray(entidad.IMGNPRSNA);
            }
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();

            var ret = await _personaProxy.Insertar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok(ret.CodEstado);
        }
        private string FileToByteArray(IFormFile archivo)
        {
            ////byte[] archivoByte = System.IO.File.ReadAllBytes(fileName);
            var bytes = ReadToEnd(archivo.OpenReadStream());
            string archivoBase64 = Convert.ToBase64String(bytes);
            return archivoBase64;
        }
        private static byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        [HttpGet("obtenerPersona")]
        public async Task<IActionResult> ObtenerPersona(int id)
        {
            var retorno = await _personaProxy.Obtener(id);
            return Ok(retorno);
        }

        [HttpPost("actualizarPersona")]
        public async Task<IActionResult> ActualizarPersona(PersonaConImagenDto entidad)
        {
            if (entidad.IMGNPRSNA != null)
            {
                entidad.FTO = FileToByteArray(entidad.IMGNPRSNA);
            }
            entidad.UEDCN = User.GetUserCode();
            var ret = await _personaProxy.Actualizar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }

        [HttpPost("EliminarPersonas")]
        public async Task<IActionResult> EliminarPersonas(int id)
        {
            PersonaDto eliminarPersona = new PersonaDto();
            eliminarPersona.ID = id;
            eliminarPersona.UEDCN = User.GetUserCode();
            var ret = await _personaProxy.Eliminar(eliminarPersona);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }

        [HttpGet("lista-homonimos")]
        public async Task<IActionResult> listaHomonimos(string gddcmnto, string ndcmnto, string amtrno, string aptrno, string pnmbre, string snmbre)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _personaProxy.listaHomonimos(parametrosDT, gddcmnto, ndcmnto, amtrno, aptrno, pnmbre, snmbre);
            return Ok(retorno);
        }

        [HttpGet("consultarsusalud")]
        public async Task<IActionResult> ConsultarSuSalud(string tiDocumento, string nuDocumento)
        {
            var retorno = await _suSaludProxy.ConsultarSuSalud(tiDocumento, nuDocumento);
            return Ok(retorno);
        }

        [HttpGet("lista-susalud")]
        public async Task<IActionResult> listaSuSalud(string tiDocumento, string nuDocumento)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _suSaludProxy.ObtenerDataTable(parametrosDT, tiDocumento, nuDocumento);
            return Ok(retorno);
        }

        [HttpGet("lista-sunat")]
        public async Task<IActionResult> listaSunat(string nuDocumento)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _sunatproxy.ObtenerDataTable(parametrosDT, nuDocumento);
            return Ok(retorno);
        }
    }
}