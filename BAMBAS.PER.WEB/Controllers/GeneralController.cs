﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Models;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.WEB.Models;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.TrabajadorTalla;
using BAMBAS.Negocios.Modelos.Seguridad.TrabajadorRestriccion;
using BAMBAS.Negocios.Modelos.Seguridad.Autocomplete;
using BAMBAS.Negocios.General.Seguridad;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.PER.WEB.Controllers
{
    public class GeneralController : Controller
    {
        private readonly IGrupoDatoProxy _grupoDatoProxy;
        private readonly IUbigeoProxy _ubigeoProxy;
        private readonly IParametroProxy _parametroProxy;
        private readonly IAreaProxy _areaProxy;
        private readonly ISuperintendenciaProxy _superintendenciaProxy;
        private readonly IGerenciaProxy _gerenciaProxy;
        private readonly IGrupoCargoProxy _grupoCargoProxy;
        private readonly ICargoProxy _cargoProxy;
        private readonly IPosicionProxy _posicionProxy;
        private readonly ICentroCostoProxy _centroCostoProxy;
        private readonly IUnidadOrganizacionalProxy _unidadOrganizacionalProxy;
        private readonly IArticuloProxy _ArticulosProxy;
        private readonly ITrabajadorTallaProxy _TrabajadorTallaProxy;
        private readonly ITrabajadorRestriccionProxy _TrabajadorRestriccionProxy;
        private readonly IDataTableService _dataTableService;
        private readonly IMemoryCache _cache;
        public GeneralController(
            IGrupoDatoProxy grupoDatoProxy,
            IUbigeoProxy ubigeoProxy,
            IParametroProxy parametroProxy,
            IAreaProxy areaProxy,
            ISuperintendenciaProxy superintendenciaProxy,
            IGerenciaProxy gerenciaProxy,
            IGrupoCargoProxy grupoCargoProxy,
            ICargoProxy cargoProxy,
            IPosicionProxy posicionProxy,
            ICentroCostoProxy centroCostoProxy,
            IUnidadOrganizacionalProxy unidadOrganizacionalProxy,
            IArticuloProxy articulosProxy,
            ITrabajadorTallaProxy trabajadorTallaProxy,
            ITrabajadorRestriccionProxy trabajadorRestriccionProxy,
            IDataTableService dataTableService,
            IMemoryCache cache
            )
        {
            _grupoDatoProxy = grupoDatoProxy;
            _ubigeoProxy = ubigeoProxy;
            _parametroProxy = parametroProxy;
            _areaProxy = areaProxy;
            _superintendenciaProxy = superintendenciaProxy;
            _gerenciaProxy = gerenciaProxy;
            _grupoCargoProxy = grupoCargoProxy;
            _cargoProxy = cargoProxy;
            _posicionProxy = posicionProxy;
            _centroCostoProxy = centroCostoProxy;
            _unidadOrganizacionalProxy = unidadOrganizacionalProxy;
            _ArticulosProxy = articulosProxy;
            _TrabajadorTallaProxy = trabajadorTallaProxy;
            _TrabajadorRestriccionProxy = trabajadorRestriccionProxy;
            _dataTableService = dataTableService;
            _cache = cache;
        }
        [HttpGet("obtener-datos-usuario-logueado")]
        public IActionResult ObtenerLogueado()
        {
            var audit = new AuditoriaDto();
            audit.UEDCN = User.GetUserCode();
            audit.UCRCN = User.GetUserCode();
            audit.FCRCN = DateTime.Now;
            audit.FEDCN = DateTime.Now;
            audit.FESTDO = DateTime.Now;
            return Ok(audit);
        }
        [HttpGet("obtener-sucursales-usuario-logueado")]
        public IActionResult ObtenerSucursalesLogueado()
        {
            var suc = User?.ObtenerSucursales() ?? "";
            var sucursales = string.IsNullOrEmpty(suc) ? new List<SucursalUsuarioInfo>() : JsonSerializer.Deserialize<List<SucursalUsuarioInfo>>(suc);
            return Ok(sucursales);
        }
        [HttpPost("guardar-sucursal-seleccionada")]
        public IActionResult GuardarSucursal(string id)
        {
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromDays(1));

            // Save data in cache.
            _cache.Set("ScrslSelec", id, cacheEntryOptions);
            return Ok();
        }
        [HttpGet("obtener-departamentos")]
        public async Task<IActionResult> ObtenerDepartamentos(string cps)
        {
            var result = await _ubigeoProxy.ObtenerDepartamentos(cps);
            return Ok(result);
        }
        [HttpGet("obtener-provincias")]
        public async Task<IActionResult> ObtenerProvincias(string codigoDpto, string cps)
        {
            var result = await _ubigeoProxy.ObtenerProvincias(codigoDpto, cps);
            return Ok(result);
        }
        [HttpGet("obtener-distritos")]
        public async Task<IActionResult> ObtenerDistritos(string codigoProvincia, string cps)
        {
            var result = await _ubigeoProxy.ObtenerDistrito(codigoProvincia, cps);
            return Ok(result);
        }

        [HttpGet("obtenercatalogoGD")]
        public async Task<IActionResult> ObtenerCatalogo()
        {
            var result = await _grupoDatoProxy.ObtenerCatalogos();
            return Ok(result);
        }

        [HttpGet("obtener-paises")]
        public async Task<IActionResult> ObtenerPaises()
        {
            var result = await _ubigeoProxy.ObtenerPaises();
            return Ok(result);
        }

        [HttpGet("obtener-parametro-por-abreviacion")]
        public async Task<IActionResult> ObtenerParametroPorAbreviacion(string aprmtro)
        {
            var retorno = await _parametroProxy.ObtenerParametroPorAbreviacion(aprmtro);
            return Ok(retorno); ;
        }

        [HttpGet("obtener-areas")]
        public async Task<IActionResult> ObtenerAreas(int idSuperintendencia)
        {
            var areas = await _areaProxy.ObtenerActivas(idSuperintendencia);
            return Ok(areas);
        }
        [HttpGet("obtener-superintendencias")]
        public async Task<IActionResult> ObtenerSuperintendencias(int idGerencia)
        {
            var superintendencias = await _superintendenciaProxy.ObtenerActivas(idGerencia);
            return Ok(superintendencias);
        }
        [HttpGet("obtener-gerencias")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var gerencia = await _gerenciaProxy.ObtenerActivas();
            return Ok(gerencia);
        }
        [HttpGet("obtener-grupo-cargo")]
        public async Task<IActionResult> ObtenerGrupoCargo()
        {
            var gerencia = await _grupoCargoProxy.ObtenerActivos();
            return Ok(gerencia);
        }
        [HttpGet("obtener-cargos")]
        public async Task<IActionResult> ObtenerCargo(int idGrupoCargo)
        {
            var gerencia = await _cargoProxy.ObtenerActivos(idGrupoCargo);
            return Ok(gerencia);
        }
        [HttpGet("obtener-posicion")]
        public async Task<IActionResult> ObtenerPosicion()
        {
            var gerencia = await _posicionProxy.ObtenerActivos();
            return Ok(gerencia);
        }
        [HttpGet("obtener-centro-costo")]
        public async Task<IActionResult> ObtenerCentroCosto()
        {
            var gerencia = await _centroCostoProxy.ObtenerActivos();
            return Ok(gerencia);
        }
        [HttpGet("obtener-unidad-organizacional")]
        public async Task<IActionResult> ObtenerUnidadOrganizacional()
        {
            var gerencia = await _unidadOrganizacionalProxy.ObtenerActivos();
            return Ok(gerencia);
        }
        [HttpGet("obtener-articulos")]
        public async Task<IActionResult> ObtenerArticulos()
        {
            var articulos = await _ArticulosProxy.ObtenerActivas();
            return Ok(articulos);
        }

        [HttpGet("obtener-tallas-lista")]
        public async Task<IActionResult> ListarDetalleTalla(int idTrabajador)
        {
            var retorno = await _TrabajadorTallaProxy.ListarDetalleTalla(idTrabajador);
            return Ok(retorno); ;
        }
        [HttpGet("obtener-tallas")]
        public async Task<IActionResult> ObtenerDataTable(int idTrabajador)
        {
            var parameters = _dataTableService.GetSentParameters();
            var Trabajador = await _TrabajadorTallaProxy.ObtenerDataTable(parameters, idTrabajador);
            return Ok(Trabajador);
        }
        [HttpGet("searchcontalla")]
        public async Task<IActionResult> searchcontalla(string term)
        {
            var articulo = await _ArticulosProxy.ObtenerAutocompleteTalla(term);
            var lista = new List<AutocompleteDto>();

            foreach (var item in articulo)
            {
                var model = new AutocompleteDto()
                {
                    value = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    label = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    value2 = (item.ID.ToString() + " - " + item.GDUNDDMDDA.ToString()),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }
        [HttpGet("searchconrestricciones")]
        public async Task<IActionResult> searchconrestricciones(string term)
        {
            var articulo = await _ArticulosProxy.ObtenerAutocompleteRestricciones(term);
            var lista = new List<AutocompleteDto>();

            foreach (var item in articulo)
            {
                var model = new AutocompleteDto()
                {
                    value = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    label = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    value2 = (item.ID.ToString() + " - " + item.GDUNDDMDDA.ToString()),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }

        [HttpGet("obtener-articulos-talla")]
        public async Task<IActionResult> ObtenerDataTableArticulos(bool flagtlla, int? idgrupocargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var Trabajador = await _TrabajadorTallaProxy.ObtenerDataTableArticulos(parameters, flagtlla, idgrupocargo);
            return Ok(Trabajador);
        }
        [HttpPost("actualizar-trabajadortalla")]
        public async Task<IActionResult> ActualizarDatos(ActualizarTallaDto TrabajadorTalla)
        {

            TrabajadorTalla.UEDCN = User.GetUserCode();

            var result = await _TrabajadorTallaProxy.Actualizar(TrabajadorTalla);
            if (!result.EsSatisfactoria)
                return BadRequest(result.Mensaje);
            return Ok();
        }

        [HttpGet("obtener-articulos-restricciones")]
        public async Task<IActionResult> ObtenerDataTableRestricciones(bool flagrestrccion, int? idgrupocargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var Trabajador = await _TrabajadorRestriccionProxy.ObtenerDataTableArticulos(parameters, flagrestrccion, idgrupocargo);
            return Ok(Trabajador);
        }

        [HttpGet("obtener-restricciones-lista")]
        public async Task<IActionResult> ListarDetalleRestricciones(int idTrabajador)
        {
            var retorno = await _TrabajadorRestriccionProxy.ListarDetalleRestriccion(idTrabajador);
            return Ok(retorno); ;
        }
        [HttpPost("eliminar-detalle-talla")]
        public async Task<IActionResult> EliminarDetalleTalla(int id)
        {
            var entidad = new ActualizarTallaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _TrabajadorTallaProxy.EliminarDetalleTalla(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar-trabajadorrestricciones")]
        public async Task<IActionResult> ActualizarDatosRestricciones(ActualizarRestriccionDto TrabajadorRestriccion)
        {

            TrabajadorRestriccion.UEDCN = User.GetUserCode();

            var result = await _TrabajadorRestriccionProxy.Actualizar(TrabajadorRestriccion);
            if (!result.EsSatisfactoria)
                return BadRequest(result.Mensaje);
            return Ok();
        }
        [HttpPost("eliminar-detalle-restricciones")]
        public async Task<IActionResult> EliminarDetalleRestricciones(int id)
        {
            var entidad = new ActualizarRestriccionDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _TrabajadorRestriccionProxy.EliminarDetalleRestricciones(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
