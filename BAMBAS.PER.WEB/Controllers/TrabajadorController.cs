﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Persona.Trabajador;
using BAMBAS.Negocios.Persona;
using Microsoft.AspNetCore.Authorization;
using BAMBAS.PER.WEB.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.PER.WEB.Controllers
{
    [Route("trabajador")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]
    public class TrabajadorController : Controller
    {
        private readonly ITrabajadorProxy _trabajadorProxy;
        private readonly IDataTableService _dataTableService;
        private readonly ISelect2Service _select2Service;

        public TrabajadorController(ITrabajadorProxy trabajadorProxy, IDataTableService datatableservice, ISelect2Service select2Service)

        {
            _trabajadorProxy = trabajadorProxy;
            _dataTableService = datatableservice;
            _select2Service = select2Service;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet("agregar")]
        [HttpGet("agregar/{idprsna}")]
        [HttpGet("editar/{id}")]
        public IActionResult Datos(int? id = null)
        {
            return View(id);
        }

        [HttpGet("tallas/{id}")]
        public IActionResult Tallas(int id)
        {
            return View(id);
        }

        [HttpGet("restricciones/{id}")]
        public IActionResult Restricciones(int id)
        {
            return View(id);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(TrabajadorDto entidad)
        {
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();
            var ret = await _trabajadorProxy.Insertar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("get")]
        public async Task<IActionResult> Listar()
        {
            var parametros = _dataTableService.GetSentParameters();
            var retorno = await _trabajadorProxy.ObtenerDataTable(parametros);
            return Ok(retorno);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var retorno = await _trabajadorProxy.Obtener(id.ToString());
            return Ok(retorno);
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(TrabajadorDto entidad)
        {
            entidad.UEDCN = User.GetUserCode();
            var ret = await _trabajadorProxy.Actualizar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            TrabajadorDto entidad = new TrabajadorDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var ret = await _trabajadorProxy.Eliminar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("select-trabajador")]
        public async Task<IActionResult> ListarTrabajadores()
        {
            var param = _select2Service.GetRequestParameters();
            var retorno = await _trabajadorProxy.SelectTrabajador(param);
            return Ok(retorno);
        }
    }
}
