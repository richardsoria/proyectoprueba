﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoDetalle;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
using System.Data;

namespace BAMBAS.SGS.Servicio.ControladorEventos.ObjetoDetalle
{
    public class ControladorEventosObjetoDetalleInsertar : IRequestHandler<ComandoObjetoDetalleInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosObjetoDetalleInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoObjetoDetalleInsertar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@IDOBJTO", entidad.IDOBJTO);
            param.Add("@NOBJTO", entidad.NOBJTO);
            param.Add("@DOBJTO", entidad.DOBJTO);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@GDESTDO", entidad.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.ObjetoDetalle.Insertar,"ID", param);
        }
    }
}
