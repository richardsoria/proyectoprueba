﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Parametro;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Parametro
{
    public class ControladorEventosParametroInsertar : IRequestHandler<ComandoParametroInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosParametroInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoParametroInsertar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@DPRMTRO", entidad.DPRMTRO);
            param.Add("@APRMTRO", entidad.APRMTRO);
            param.Add("@VLR1", entidad.VLR1);
            param.Add("@VLR2", entidad.VLR2);
            param.Add("@GDESTDO", entidad.GDESTDO);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.ParametroEmpresa.InsertarParametros, "ID", param);
        }
    }
}
