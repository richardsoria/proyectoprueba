﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Empresa;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Empresa
{
    public class ControladorEventosEmpresaActualizar : IRequestHandler<ComandoEmpresaActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosEmpresaActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoEmpresaActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@DRCCN", empresa.DRCCN);
            param.Add("@UBGO", empresa.UBGO);
            param.Add("@RUC", empresa.RUC);
            param.Add("@NEMPRSA", empresa.NEMPRSA);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Empresa.EditarEmpresa, "RETORNO", param);
        }
    }
}
