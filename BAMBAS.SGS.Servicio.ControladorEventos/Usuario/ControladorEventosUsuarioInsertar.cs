﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Usuario;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Usuario
{
    public class ControladorEventosUsuarioInsertar : IRequestHandler<ComandoUsuarioInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;
        public ControladorEventosUsuarioInsertar(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoUsuarioInsertar usuario, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", value: 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@IDPRSNA", usuario.IDPRSNA);
            param.Add("@CUSRO", usuario.CUSRO);
            param.Add("@NYAPLLDS", usuario.NYAPLLDS);
            param.Add("@TDCMNTO", usuario.TDCMNTO);
            param.Add("@NDCMNTO", usuario.NDCMNTO);
            param.Add("@NTLFNO", usuario.NTLFNO);
            param.Add("@CLVE", usuario.CLVE);
            param.Add("@FVCLVE", ConvertHelpers.DatepickerToDatetime(usuario.FVCLVE));
            param.Add("@FBLQUO", usuario.FBLQUO);
            param.Add("@FRZRCMBOCLVE", usuario.FRZRCMBOCLVE);
            param.Add("@FCNTRTSTA", usuario.FCNTRTSTA);
            param.Add("@GDESTDO", usuario.GDESTDO);
            param.Add("@UCRCN", usuario.UCRCN);
            param.Add("@UEDCN", usuario.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.InsertarUsuario, "ID", param);
        }
    }
}
