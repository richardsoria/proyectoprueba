﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Usuario;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Usuario
{
    public class ControladorEventosUsuarioActualizar : IRequestHandler<ComandoUsuarioActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosUsuarioActualizar(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoUsuarioActualizar usuario, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", usuario.ID);
            param.Add("@CUSRO", usuario.CUSRO);
            param.Add("@CLVE", usuario.CLVE);
            param.Add("@FVCLVE", ConvertHelpers.DatepickerToDatetime(usuario.FVCLVE));           
            param.Add("@FBLQUO", usuario.FBLQUO);
            param.Add("@FRZRCMBOCLVE", usuario.FRZRCMBOCLVE);
            param.Add("@GDESTDO", usuario.GDESTDO);
            param.Add("@UEDCN", usuario.UEDCN);
            param.Add("@RETORNO", value: 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.EditarUsuario, "RETORNO", param);
        }
    }
}

