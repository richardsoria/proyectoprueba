﻿using BAMBAS.DATABASE.Configuraciones;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Helpers;
using MediatR;
using BAMBAS.CORE.Structs;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Usuario;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Usuario
{
    public class ControladorEventosPersonaUsuarioActualizar : IRequestHandler<ComandoPersonaUsuarioActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosPersonaUsuarioActualizar(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoPersonaUsuarioActualizar entidad, CancellationToken cancellationToken)
        {
            var parametros = new Dapper.DynamicParameters();
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@NYAPLLDS", entidad.NYAPLLDS);
            parametros.Add("@TDCMNTO", entidad.GDDCMNTO);
            parametros.Add("@NDCMNTO", entidad.NDCMNTO);
            parametros.Add("@NTLFNO", entidad.TLFNO);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.ActualizarPersonaUsuario, "RETORNO", parametros);
        }
    }
}
