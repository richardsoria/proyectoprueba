﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.TrabajadorRestriccion;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;


namespace BAMBAS.SGS.Servicio.ControladorEventos.TrabajadorRestriccion
{
    public class ControladorEventosTrabajadorRestriccionActualizar : IRequestHandler<ComandoTrabajadorRestriccionActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTrabajadorRestriccionActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTrabajadorRestriccionActualizar talla, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ARTCLS", talla.TLLADTO);
            param.Add("@UEDCN", talla.UEDCN);
            param.Add("@IDTRBJDR", talla.IDTRBJDR);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);


            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.TrabajadorRestriccion.ActualizarTrabajadorRestriccion, "RETORNO", param);
        }
    }
}
