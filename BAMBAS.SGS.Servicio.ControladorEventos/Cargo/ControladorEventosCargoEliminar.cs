﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Cargo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Cargo
{
    public class ControladorEventosCargoEliminar : IRequestHandler<ComandoCargoEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosCargoEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoCargoEliminar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Cargo.Eliminar, "RETORNO", parametros);
        }
    }
}
