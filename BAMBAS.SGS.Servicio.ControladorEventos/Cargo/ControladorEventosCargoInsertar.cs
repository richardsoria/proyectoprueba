﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Cargo;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Cargo
{
    public class ControladorEventosCargoInsertar : IRequestHandler<ComandoCargoInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosCargoInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoCargoInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@IDGRPO", empresa.IDGRPO);
            param.Add("@UCRCN", empresa.UCRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Cargo.Insertar, "ID", param);
        }
    }
}
