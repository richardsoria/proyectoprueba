﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoPadre;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.ObjetoPadre
{
    public class ControladorEventosObjetoPadreActualizar : IRequestHandler<ComandoObjetoPadreActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosObjetoPadreActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoObjetoPadreActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@FORDN", empresa.FORDN);
            param.Add("@IDMDLO", empresa.IDMDLO);
            param.Add("@NOBJTO", empresa.NOBJTO);
            param.Add("@DOBJTO", empresa.DOBJTO);
            param.Add("@URL", empresa.URL);
            param.Add("@IDOBJTOPDRE", empresa.IDOBJTOPDRE);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.ObjetoPadre.Editar, "RETORNO", param);
        }
    }
}
