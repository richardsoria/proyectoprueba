﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoPadre;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.ObjetoPadre
{
    public class ControladorEventosObjetoPadreEliminar : IRequestHandler<ComandoObjetoPadreEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosObjetoPadreEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoObjetoPadreEliminar entidad, CancellationToken cancellationToken)
        {
            var objeto = new DynamicParameters();
            objeto.Add("@ID", entidad.ID);
            objeto.Add("@UEDCN", entidad.UEDCN);
            objeto.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.ObjetoPadre.Eliminar, "RETORNO", objeto);
        }
    }
}
