﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoPadre;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
using System.Data;

namespace BAMBAS.SGS.Servicio.ControladorEventos.ObjetoPadre
{
    public class ControladorEventosObjetoPadreInsertar : IRequestHandler<ComandoObjetoPadreInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosObjetoPadreInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoObjetoPadreInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID, dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@UCRCN", empresa.UCRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@FORDN", empresa.FORDN);
            param.Add("@IDMDLO", empresa.IDMDLO);
            param.Add("@DOBJTO", empresa.DOBJTO);
            param.Add("@URL", empresa.URL);
            param.Add("@IDOBJTOPDRE", empresa.IDOBJTOPDRE);
            param.Add("@NOBJTO", empresa.NOBJTO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.ObjetoPadre.Insertar,"ID", param);
        }
    }
}
