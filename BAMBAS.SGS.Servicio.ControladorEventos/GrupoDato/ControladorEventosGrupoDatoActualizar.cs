﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDato;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoDato
{
    public class ControladorEventosGrupoDatoActualizar : IRequestHandler<ComandoGrupoDatoActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoDatoActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoDatoActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@CGDTO", empresa.CGDTO);
            param.Add("@DGDTO", empresa.DGDTO);
            param.Add("@AGDTO", empresa.AGDTO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoDatoEmpresa.EditarGrupoDato, "RETORNO", param);
        }
    }
}
