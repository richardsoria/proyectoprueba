﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDato;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoDato
{
    public class ControladorEventosGrupoDatoEliminar : IRequestHandler<ComandoGrupoDatoEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoDatoEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoDatoEliminar entidad, CancellationToken cancellationToken)
        {
            var datos = new DynamicParameters();
            datos.Add("@ID", entidad.ID);
            datos.Add("@UEDCN", entidad.UEDCN);
            datos.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoDatoEmpresa.EliminarGrupoDato, "RETORNO", datos);
        }
    }
}
