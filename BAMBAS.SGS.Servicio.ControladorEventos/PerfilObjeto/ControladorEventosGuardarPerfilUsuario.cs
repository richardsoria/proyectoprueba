﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilObjeto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.PerfilObjeto
{
    public class ControladorEventosGuardarPerfilObjeto : IRequestHandler<ComandoGuardarPerfilObjeto, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGuardarPerfilObjeto(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGuardarPerfilObjeto entidad, CancellationToken cancellationToken)
        {
            var perfil = new DynamicParameters();
            perfil.Add("@ID", value: 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            perfil.Add("@IDPRFL", entidad.IdPerfil);
            perfil.Add("@MDLOSASGNDS", string.Join(",", entidad.ModulosAsignados));
            perfil.Add("@MDLOSNOASGNDS", string.Join(",", entidad.ModulosNoAsignados));
            perfil.Add("@PDRSASGNDS", string.Join(",", entidad.ObjPadreAsignados));
            perfil.Add("@PDRSNOASGNDS", string.Join(",", entidad.ObjPadreNoAsignados));
            perfil.Add("@HJOSASGNDS", string.Join(",", entidad.ObjHijoAsignados));
            perfil.Add("@HJOSNOASGNDS", string.Join(",", entidad.ObjHijoNoAsignados));
            perfil.Add("@DTLLSASGNDS", string.Join(",", entidad.DetalleAsignados));
            perfil.Add("@DTLLSNOASGNDS", string.Join(",", entidad.DetalleNoAsignados));
            perfil.Add("@UCRCN", entidad.UCRCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.PerfilObjeto.GuardarPerfilesObjeto, "ID", perfil);
        }
    }
}
