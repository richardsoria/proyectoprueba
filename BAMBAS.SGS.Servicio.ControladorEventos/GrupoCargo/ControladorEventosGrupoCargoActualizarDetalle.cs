﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoCargo;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoCargo
{
    public class ControladorEventosGrupoCargoActualizarDetalle : IRequestHandler<ComandoGrupoCargoActualizarDetalle, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoCargoActualizarDetalle(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoCargoActualizarDetalle empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@ARTCLS", empresa.ARTCLS);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoCargo.EditarDetalle, "RETORNO", param);
        }
    }
}
