﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Perfil;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Perfil
{
    public class ControladorEventosPerfilInsertar : IRequestHandler<ComandoPerfilInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosPerfilInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoPerfilInsertar entidad, CancellationToken cancellationToken)
        {
            var perfiles = new DynamicParameters();
            perfiles.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            perfiles.Add("@DPRFL", entidad.DPRFL);
            perfiles.Add("@GDESTDO", entidad.GDESTDO);
            perfiles.Add("@UCRCN", entidad.UCRCN);
            perfiles.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Perfil.InsertarPerfil, "ID", perfiles);
        }
    }
}
