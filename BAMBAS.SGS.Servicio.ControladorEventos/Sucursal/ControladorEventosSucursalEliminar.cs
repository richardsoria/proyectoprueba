﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Sucursal;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Sucursal
{
    public class ControladorEventosSucursalEliminar : IRequestHandler<ComandoSucursalEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosSucursalEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoSucursalEliminar entidad, CancellationToken cancellationToken)
        {
            var DatoDetalle = new DynamicParameters();
            DatoDetalle.Add("@ID", entidad.ID);
            DatoDetalle.Add("@UEDCN", entidad.UEDCN);
            DatoDetalle.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Sucursal.EliminarSucuesal, "RETORNO", DatoDetalle);
        }
    }
}
