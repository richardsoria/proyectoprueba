﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Sucursal;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Sucursal
{
    public class ControladorEventosSucursalActualizar : IRequestHandler<ComandoSucursalActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosSucursalActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoSucursalActualizar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@NSCRSL", entidad.NSCRSL);
            param.Add("@DSCRSL", entidad.DSCRSL);
            param.Add("@UBGO", entidad.UBGO);
            param.Add("@ID", entidad.ID);
            param.Add("@UEDCN", entidad.UEDCN);
            param.Add("@GDESTDO", entidad.GDESTDO);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Sucursal.EditarSucursal, "RETORNO", param);
        }
    }
}
