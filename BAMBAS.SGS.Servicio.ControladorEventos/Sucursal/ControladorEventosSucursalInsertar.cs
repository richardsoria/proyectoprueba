﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Sucursal;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Sucursal
{
    public class ControladorEventosSucursalInsertar : IRequestHandler<ComandoSucursalInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosSucursalInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoSucursalInsertar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@IDEMPRSA", entidad.IDEMPRSA);
            param.Add("@NSCRSL", entidad.NSCRSL);
            param.Add("@UBGO", entidad.UBGO);
            param.Add("@DSCRSL", entidad.DSCRSL);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@GDESTDO", entidad.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Sucursal.InsertarSucursal, "ID", param);
        }
    }
}
