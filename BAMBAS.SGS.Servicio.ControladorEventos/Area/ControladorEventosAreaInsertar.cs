﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Area;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Area
{
    public class ControladorEventosAreaInsertar : IRequestHandler<ComandoAreaInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosAreaInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoAreaInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@IDGRNCA", empresa.IDGRNCA);
            param.Add("@IDSPRINTNDNCA", empresa.IDSPRINTNDNCA);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@UCRCN", empresa.UCRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Area.Insertar, "ID", param);
        }
    }
}
