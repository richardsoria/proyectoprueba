﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Roster;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Roster
{
    public class ControladorEventosRosterInsertar : IRequestHandler<ComandoRosterInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosRosterInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoRosterInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@DTRBJDS", empresa.DTRBJDS);
            param.Add("@DDSCNSO", empresa.DDSCNSO);
            param.Add("@UCRCN", empresa.UCRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Roster.Insertar, "ID", param);
        }
    }
}
