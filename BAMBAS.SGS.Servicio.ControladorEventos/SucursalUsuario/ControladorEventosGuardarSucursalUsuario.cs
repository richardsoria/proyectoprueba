﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.SucursalUsuario;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.SucursalUsuario
{
    public class ControladorEventosGuardarSucursalUsuario : IRequestHandler<ComandoGuardarSucursalUsuario, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGuardarSucursalUsuario(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGuardarSucursalUsuario entidad, CancellationToken cancellationToken)
        {
            var perfil = new DynamicParameters();
            perfil.Add("@UCRCN", entidad.UCRCN);
            perfil.Add("@ASGNDS", string.Join(",",entidad.Asignados));
            perfil.Add("@NOASGNDS", string.Join(",", entidad.NoAsignados));
            perfil.Add("@IDSCRSL", entidad.IdSucursal);
            perfil.Add("@ID",value:0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.SucursalUsuario.GuardarSucursalUsuario, "ID", perfil);
        }
    }
}
