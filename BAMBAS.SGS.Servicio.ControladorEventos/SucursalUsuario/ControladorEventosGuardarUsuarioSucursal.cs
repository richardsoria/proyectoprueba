﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.SucursalUsuario;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.SucursalUsuario
{
    public class ControladorEventosGuardarUsuarioSucursal : IRequestHandler<ComandoGuardarUsuarioSucursal, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGuardarUsuarioSucursal(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGuardarUsuarioSucursal entidad, CancellationToken cancellationToken)
        {
            var perfil = new DynamicParameters();
            perfil.Add("@UCRCN", entidad.UCRCN);
            perfil.Add("@ASGNDS", string.Join(",",entidad.Asignadas));
            perfil.Add("@NOASGNDS", string.Join(",", entidad.NoAsignadas));
            perfil.Add("@IDUSRO", entidad.IdUsuario);
            perfil.Add("@ID",value:0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.SucursalUsuario.GuardarUsuarioSucursal, "ID", perfil);
        }
    }
}
