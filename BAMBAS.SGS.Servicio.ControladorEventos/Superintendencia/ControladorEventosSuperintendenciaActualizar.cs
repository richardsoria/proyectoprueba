﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Superintendencia;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Superintendencia
{
    public class ControladorEventosSuperintendenciaActualizar : IRequestHandler<ComandoSuperintendenciaActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosSuperintendenciaActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoSuperintendenciaActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@IDGRNCA", empresa.IDGRNCA);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Superintendencia.Editar, "RETORNO", param);
        }
    }
}
