﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Superintendencia;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Superintendencia
{
    public class ControladorEventosSuperintendenciaInsertar : IRequestHandler<ComandoSuperintendenciaInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosSuperintendenciaInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoSuperintendenciaInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@IDGRNCA", empresa.IDGRNCA);
            param.Add("@UCRCN", empresa.UCRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Superintendencia.Insertar, "ID", param);
        }
    }
}
