﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Familia;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Familia
{
    public class ControladorEventosFamiliaInsertar : IRequestHandler<ComandoFamiliaInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosFamiliaInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoFamiliaInsertar empresa, CancellationToken cancellationToken)
        {
            
                var param = new DynamicParameters();
                param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                param.Add("@IDFMLAPDRE", empresa.IDFMLAPDRE);
                param.Add("@DSCRPCN", empresa.DSCRPCN);
                param.Add("@UCRCN", empresa.UCRCN);
                param.Add("@GDESTDO", empresa.GDESTDO);
                return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Familia.Insertar, "ID", param);
         
        }
    }
}
