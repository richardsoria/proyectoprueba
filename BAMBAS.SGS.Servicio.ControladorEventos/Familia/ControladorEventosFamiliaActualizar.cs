﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Familia;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Familia
{
    public class ControladorEventosFamiliaActualizar : IRequestHandler<ComandoFamiliaActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosFamiliaActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoFamiliaActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@IDFMLAPDRE", empresa.IDFMLAPDRE);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Familia.Editar, "RETORNO", param);
        }
    }
}
