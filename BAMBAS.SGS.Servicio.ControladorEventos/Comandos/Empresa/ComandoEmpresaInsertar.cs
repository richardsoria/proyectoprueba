﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Empresa
{
    public class ComandoEmpresaInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string NEMPRSA { get; set; }
        public string RUC { get; set; }
        public string UBGO { get; set; }
        public string DRCCN { get; set; }
    }
}
