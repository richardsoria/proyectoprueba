﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilUsuario
{
    public class ComandoGuardarPerfilPorUsuario : IRequest<RespuestaConsulta>
    {
        public string IdUsuario { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignados { get; set; }
        public List<int> NoAsignados { get; set; }
    }
}
