﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Parametro
{
    public class ComandoParametroInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DPRMTRO { get; set; }
        public string APRMTRO { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
    }
}
