﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.TrabajadorRestriccion
{
    public class ComandoTrabajadorRestriccionActualizar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string TLLADTO { get; set; }
        public int IDTRBJDR { get; set; }
    }
}
