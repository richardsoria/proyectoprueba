﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Perfil
{
    public class ComandoPerfilInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DPRFL { get; set; }
    }
}
