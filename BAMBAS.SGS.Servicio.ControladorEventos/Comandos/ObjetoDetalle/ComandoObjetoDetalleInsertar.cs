﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoDetalle
{
    public class ComandoObjetoDetalleInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDOBJTO { get; set; }
        public string NOBJTO { get; set; }
        public string DOBJTO { get; set; }
    }
}
