﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Usuario
{
    public class ComandoUsuarioInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string IDPRSNA { get; set; }
        public string CUSRO { get; set; }
        public string NYAPLLDS { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NTLFNO { get; set; }
        public string CLVE { get; set; }
        public string FVCLVE { get; set; }
        public bool FBLQUO { get; set; }
        public bool FRZRCMBOCLVE { get; set; }
        public bool FCNTRTSTA { get; set; }
    }
}
