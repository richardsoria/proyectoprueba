﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.SucursalUsuario
{
    public class ComandoGuardarUsuarioSucursal : IRequest<RespuestaConsulta>
    {
        public string IdUsuario { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignadas { get; set; }
        public List<int> NoAsignadas { get; set; }
    }
}
