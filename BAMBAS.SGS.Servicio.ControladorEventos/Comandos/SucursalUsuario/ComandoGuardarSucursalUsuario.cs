﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.SucursalUsuario
{
    public class ComandoGuardarSucursalUsuario : IRequest<RespuestaConsulta>
    {
        public string IdSucursal { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignados { get; set; }
        public List<int> NoAsignados { get; set; }
    }
}
