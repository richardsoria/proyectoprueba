﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoPadre
{
    public class ComandoObjetoPadreInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDMDLO { get; set; }
        public string NOBJTO { get; set; }
        public int FORDN { get; set; }
        public string IDOBJTOPDRE { get; set; }
        public string DOBJTO { get; set; }
        public string URL { get; set; }
    }
}
