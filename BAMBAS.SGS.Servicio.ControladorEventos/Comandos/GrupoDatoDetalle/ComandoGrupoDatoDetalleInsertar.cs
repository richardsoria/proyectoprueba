﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDatoDetalle
{
    public class ComandoGrupoDatoDetalleInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string CGDTO { get; set; }
        public string DGDDTLLE { get; set; }
        public string AGDDTLLE { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
        public string VLR3 { get; set; }
    }
}
