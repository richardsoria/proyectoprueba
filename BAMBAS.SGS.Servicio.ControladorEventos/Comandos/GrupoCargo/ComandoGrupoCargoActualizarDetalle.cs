﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoCargo
{
    public class ComandoGrupoCargoActualizarDetalle : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDGRPOCRGO { get; set; }
        public string DSCRPCN { get; set; }
        public string ARTCLS { get; set; }
        public string CSAP { get; set; }
        public int CNTDD { get; set; }
        public string GDUNDDMDDA { get; set; }
        public decimal CSTO { get; set; }
        public int DRCN { get; set; }
        public string IDARTCLO { get; set; }
        public string IDEPP { get; set; }
    }
}
