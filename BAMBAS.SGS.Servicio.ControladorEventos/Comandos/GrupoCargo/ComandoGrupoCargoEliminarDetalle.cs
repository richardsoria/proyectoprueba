﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoCargo
{
    public class ComandoGrupoCargoEliminarDetalle : IRequest<RespuestaConsulta>
    {
        public int ID { get; set; }
        public string UEDCN { get; set; }
    }
}
