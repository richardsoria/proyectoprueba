﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoCargo
{
    public class ComandoGrupoCargoInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }

    }
}
