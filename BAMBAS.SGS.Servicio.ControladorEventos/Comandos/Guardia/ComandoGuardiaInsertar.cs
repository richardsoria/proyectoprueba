﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Guardia
{
    public class ComandoGuardiaInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }
        public int IDRSTR { get; set; }
        public string FINCO { get; set; }
        public string GDTRNO { get; set; }
        public string HRAINCODA { get; set; }
        public string HRAINCONCHE { get; set; }
        public string HRAFNDA { get; set; }
        public string HRAFNNCHE { get; set; }
        public int DSGRDA { get; set; }
        public int NCHSGRDA { get; set; }
        public int DSDSCNSO { get; set; }


    }
}
