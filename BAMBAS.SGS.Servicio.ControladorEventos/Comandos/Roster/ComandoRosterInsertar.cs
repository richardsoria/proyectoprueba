﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Roster
{
    public class ComandoRosterInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }
        public int DTRBJDS { get; set; }
        public int DDSCNSO { get; set; }
    }
}
