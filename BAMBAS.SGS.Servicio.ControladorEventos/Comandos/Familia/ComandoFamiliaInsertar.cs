﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Familia
{
    public class ComandoFamiliaInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }
        public int? IDFMLAPDRE { get; set; }
    }
}
