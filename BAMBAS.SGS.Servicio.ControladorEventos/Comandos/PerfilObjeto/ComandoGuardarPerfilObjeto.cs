﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilObjeto
{
    public class ComandoGuardarPerfilObjeto: IRequest<RespuestaConsulta>
    {
        public string IdPerfil { get; set; }
        public string UCRCN { get; set; }
        public List<int> ModulosAsignados { get; set; }
        public List<int> ObjPadreAsignados { get; set; }
        public List<int> ObjHijoAsignados { get; set; }
        public List<int> DetalleAsignados { get; set; }
        public List<int> ModulosNoAsignados { get; set; }
        public List<int> ObjPadreNoAsignados { get; set; }
        public List<int> ObjHijoNoAsignados { get; set; }
        public List<int> DetalleNoAsignados { get; set; }
    }
}
