﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Almacen
{
    public class ComandoAlmacenInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }
        public string GDTALMCN { get; set; }
        public int? IDTRBJDR { get; set; }
    }
}
