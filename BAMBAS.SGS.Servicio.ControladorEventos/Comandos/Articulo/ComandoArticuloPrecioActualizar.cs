﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Articulo
{
    public class ComandoArticuloPrecioActualizar : IRequest<RespuestaConsulta>
    {
        public List<ComandoArticuloInsertar> Articulos { get; set; }
        public string UEDCN { get; set; }
    }
}
