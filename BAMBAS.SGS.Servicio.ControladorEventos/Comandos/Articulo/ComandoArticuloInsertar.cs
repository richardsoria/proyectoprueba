﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;


namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Articulo
{
    public class ComandoArticuloInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string DSCRPCN { get; set; }
        public int IDFMLA { get; set; }
        public int IDSBFMLA { get; set; }
        public string CBRRA { get; set; }
        public string CSAP { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string GDTMNDA { get; set; }
        public int DRCN { get; set; }
        public decimal CSTO { get; set; }
        public bool FKIT { get; set; }
        public bool FMNJOTLLS { get; set; }
        public bool FMNJORSTRCN { get; set; }
    }
}