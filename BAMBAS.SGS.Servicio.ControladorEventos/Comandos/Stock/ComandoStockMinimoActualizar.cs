﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock
{
    public class ComandoStockMinimoActualizar :  IRequest<RespuestaConsulta>
    {
        public string STOCKS { get; set; }
        public string UEDCN { get; set; }
        public int IDALMCN { get; set; }
    }
}
