﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock
{
    public class ComandoStockInsertar :  IRequest<RespuestaConsulta>
    {
        //public int IDALMCN { get; set; }
        //public int IDARTCLO { get; set; }
        public string TCDGO { get; set; }
        public string DSCRPCNARTCLO { get; set; }
        public string CSAP { get; set; }
        public int STCK { get; set; }
        public int STCKMNMO { get; set; }
        public decimal PRCO { get; set; }
        //public string UNDDMDDA { get; set; }
    }
}
