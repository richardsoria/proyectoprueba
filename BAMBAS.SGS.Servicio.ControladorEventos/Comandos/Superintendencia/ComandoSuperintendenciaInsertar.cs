﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Superintendencia
{
    public class ComandoSuperintendenciaInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDGRNCA { get; set; }
        public string DSCRPCN { get; set; }
    }
}
