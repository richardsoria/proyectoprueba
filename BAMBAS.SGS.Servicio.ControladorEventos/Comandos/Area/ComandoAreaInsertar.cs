﻿using BAMBAS.CORE.Structs;
using MediatR;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Area
{
    public class ComandoAreaInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public string DSCRPCN { get; set; }
    }
}
