﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Sucursal
{
    public class ComandoSucursalInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string UBGO { get; set; }
        public string DSCRSL { get; set; }
        public string NSCRSL { get; set; }


        public int IDEMPRSA { get; set; }
    }
}
