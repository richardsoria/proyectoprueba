﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDato
{
    public class ComandoGrupoDatoInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string CGDTO { get; set; }
        public string DGDTO { get; set; }
        public string AGDTO { get; set; }
    }
}
