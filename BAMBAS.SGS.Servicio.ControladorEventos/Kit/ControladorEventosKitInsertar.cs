﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Kit;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Kit
{
    public class ControladorEventosKitInsertar : IRequestHandler<ComandoKitInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosKitInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoKitInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@ARTCLS", empresa.KITS);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@IDFMLA", empresa.IDFMLA);
            param.Add("@CSAP", empresa.CSAP);
            param.Add("@GDUNDDMDDA", empresa.GDUNDDMDDA);
            param.Add("@GDTMNDA", empresa.GDTMNDA);
            param.Add("@CSTOKIT", empresa.CSTO);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UCRCN", empresa.UCRCN);          
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Kit.Insertar, "ID", param);
        }
    }
}
