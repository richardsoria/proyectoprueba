﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDatoDetalle;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoDatoDetalle
{
    public class ControladorEventosGrupoDatoDetalleEliminar : IRequestHandler<ComandoGrupoDatoDetalleEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoDatoDetalleEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoDatoDetalleEliminar entidad, CancellationToken cancellationToken)
        {
            var DatoDetalle = new DynamicParameters();
            DatoDetalle.Add("@ID", entidad.ID);
            DatoDetalle.Add("@UEDCN", entidad.UEDCN);
            DatoDetalle.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.EliminarGrupoDatoDetalle, "RETORNO", DatoDetalle);
        }
    }
}
