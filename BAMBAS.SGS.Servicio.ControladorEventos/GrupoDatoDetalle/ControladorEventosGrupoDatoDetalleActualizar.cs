﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDatoDetalle;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoDatoDetalle
{
    public class ControladorEventosGrupoDatoDetalleActualizar : IRequestHandler<ComandoGrupoDatoDetalleActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoDatoDetalleActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoDatoDetalleActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            //param.Add("@CGDTO", empresa.CGDTO);
            param.Add("@DGDDTLLE", empresa.DGDDTLLE);
            param.Add("@AGDDTLLE", empresa.AGDDTLLE);
            param.Add("@VLR1", empresa.VLR1);
            param.Add("@VLR2", empresa.VLR2);
            param.Add("@VLR3", empresa.VLR2);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.EditarGrupoDatoDetalle, "RETORNO", param);
        }
    }
}
