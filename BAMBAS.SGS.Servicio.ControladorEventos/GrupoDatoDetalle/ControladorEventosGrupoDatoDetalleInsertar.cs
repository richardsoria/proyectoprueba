﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDatoDetalle;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.GrupoDatoDetalle
{
    public class ControladorEventosGrupoDatoDetalleInsertar : IRequestHandler<ComandoGrupoDatoDetalleInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGrupoDatoDetalleInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGrupoDatoDetalleInsertar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@CGDTO", entidad.CGDTO);
            param.Add("@DGDDTLLE", entidad.DGDDTLLE);
            param.Add("@AGDDTLLE", entidad.AGDDTLLE);
            param.Add("@VLR1", entidad.VLR1);
            param.Add("@VLR2", entidad.VLR2);
            param.Add("@VLR3", entidad.VLR3);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@GDESTDO", entidad.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.InsertarGrupoDatoDetalle, "ID", param);
        }
    }
}