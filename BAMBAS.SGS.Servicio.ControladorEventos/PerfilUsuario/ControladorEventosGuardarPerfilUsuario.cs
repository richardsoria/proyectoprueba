﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilUsuario;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
namespace BAMBAS.SGS.Servicio.ControladorEventos.PerfilUsuario
{
    public class ControladorEventosGuardarPerfilUsuario : IRequestHandler<ComandoGuardarPerfilUsuario, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGuardarPerfilUsuario(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGuardarPerfilUsuario entidad, CancellationToken cancellationToken)
        {
            var perfil = new DynamicParameters();
            perfil.Add("@ID", value: 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
            perfil.Add("@UCRCN", entidad.UCRCN);
            perfil.Add("@ASGNDS", string.Join(",",entidad.Asignados));
            perfil.Add("@NOASGNDS", string.Join(",", entidad.NoAsignados));
            perfil.Add("@IDPRFL", entidad.IdPerfil);
            perfil.Add("@CHCKTTL", entidad.CheckTotal == true ? "S" : "N");
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.PerfilUsuario.GuardarPerfilesUsuarios, "ID", perfil);
        }
    }
}
