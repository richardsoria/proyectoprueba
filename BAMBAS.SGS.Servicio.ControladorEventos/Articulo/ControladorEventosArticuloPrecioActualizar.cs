﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Articulo;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Articulo
{
    public class ControladorEventosArticuloPrecioActualizar : IRequestHandler<ComandoArticuloPrecioActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosArticuloPrecioActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoArticuloPrecioActualizar articulos, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ARTCLS", JsonSerializer.Serialize(articulos.Articulos));
            param.Add("@UEDCN", articulos.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Articulo.ActualizarPrecios, "RETORNO", param);
        }
    }
}
