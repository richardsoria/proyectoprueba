﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Articulo;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Articulo
{
    public class ControladorEventosArticuloActualizar : IRequestHandler<ComandoArticuloActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosArticuloActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoArticuloActualizar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", empresa.ID);
            param.Add("@DSCRPCN", empresa.DSCRPCN);
            param.Add("@IDFMLA", empresa.IDFMLA);
            param.Add("@IDSBFMLA", empresa.IDSBFMLA);
            param.Add("@CBRRA", empresa.CBRRA);
            param.Add("@CSAP", empresa.CSAP);
            param.Add("@GDUNDDMDDA", empresa.GDUNDDMDDA);
            param.Add("@GDTMNDA", empresa.GDTMNDA);
            param.Add("@DRCN", empresa.DRCN);
            param.Add("@CSTO", empresa.CSTO);
            param.Add("@FKIT", empresa.FKIT);
            param.Add("@FMNJOTLLS", empresa.FMNJOTLLS);
            param.Add("@FMNJORSTRCN", empresa.FMNJORSTRCN);
            param.Add("@GDESTDO", empresa.GDESTDO);
            param.Add("@UEDCN", empresa.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Articulo.Editar, "RETORNO", param);
        }
    }
}
