﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.TrabajadorTalla;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;


namespace BAMBAS.SGS.Servicio.ControladorEventos.TrabajadorTalla
{
    public class ControladorEventosTrabajadorTallaActualizar : IRequestHandler<ComandoTrabajadorTallaActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTrabajadorTallaActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTrabajadorTallaActualizar talla, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ARTCLS", talla.TLLADTO);
            param.Add("@UEDCN", talla.UEDCN);
            param.Add("@IDTRBJDR", talla.IDTRBJDR);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);


            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.TrabajadorTalla.ActualizarTrabajadorTalla, "RETORNO", param);
        }
    }
}
