﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Stock
{
    public class ControladorEventosStockMinimoActualizar : IRequestHandler<ComandoStockMinimoActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosStockMinimoActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoStockMinimoActualizar stock, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ARTCLS", stock.STOCKS);
            param.Add("@UEDCN", stock.UEDCN);
            param.Add("@IDALMCN", stock.IDALMCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Stock.ActualizarStockMinimo, "RETORNO", param);
        }
    }
}
