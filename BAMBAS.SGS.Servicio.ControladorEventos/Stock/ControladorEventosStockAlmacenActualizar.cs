﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Stock
{
    public class ControladorEventosStockAlmacenActualizar : IRequestHandler<ComandoStockAlmacenActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosStockAlmacenActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoStockAlmacenActualizar stock, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ARTCLS", JsonSerializer.Serialize(stock.Stock));
            param.Add("@UEDCN", stock.UEDCN);
            param.Add("@IDALMCN", stock.IDALMCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Stock.ActualizarStock, "RETORNO", param);
        }
    }
}
