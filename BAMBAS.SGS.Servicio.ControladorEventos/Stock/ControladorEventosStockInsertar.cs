﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Stock
{
    public class ControladorEventosStockInsertar : IRequestHandler<ComandoStockInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosStockInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoStockInsertar empresa, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            //param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            //param.Add("@IDALMCN", empresa.IDALMCN);
            //param.Add("@IDARTCLO", empresa.IDARTCLO);
            //param.Add("@STCK", empresa.STCK);
            //param.Add("@STCKMNMO", empresa.STCKMNMO);
            //param.Add("@UCRCN", empresa.UCRCN);
            //param.Add("@GDESTDO", empresa.GDESTDO);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Stock.Insertar, "ID", param);
        }
    }
}
