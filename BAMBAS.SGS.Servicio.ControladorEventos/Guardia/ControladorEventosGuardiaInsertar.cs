﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Guardia;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.ControladorEventos.Guardia
{
    public class ControladorEventosGuardiaInsertar : IRequestHandler<ComandoGuardiaInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosGuardiaInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoGuardiaInsertar guardia, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@DSCRPCN", guardia.DSCRPCN);
            param.Add("@IDRSTR", guardia.IDRSTR);
            param.Add("@FINCO", guardia.FINCO);
            param.Add("@GDTRNO", guardia.GDTRNO);
            param.Add("@UCRCN", guardia.UCRCN);            
            param.Add("@GDESTDO", guardia.GDESTDO);
            param.Add("@HRAINCODA", guardia.HRAINCODA);
            param.Add("@HRAINCONCHE", guardia.HRAINCONCHE);
            param.Add("@HRAFNDA", guardia.HRAFNDA);
            param.Add("@HRAFNNCHE", guardia.HRAFNNCHE);
            param.Add("@DSGRDA", guardia.DSGRDA);
            param.Add("@NCHSGRDA", guardia.NCHSGRDA);
            param.Add("@DSDSCNSO", guardia.DSDSCNSO);

            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Guardia.Insertar, "ID", param);
        }
    }
}
