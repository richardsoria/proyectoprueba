﻿using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.EPP.Servicio.Consultas;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Api.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IConsultasMovimientoAlmacen, ConsultasMovimientoAlmacen>();
            serviceCollection.AddTransient<IConsultasProyeccion, ConsultasProyeccion>();
        }
    }
}
