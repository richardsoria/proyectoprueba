﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Api.Controllers
{
    [ApiController]
    [Route("/")]
    public class DefaultControllercs : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "Ejecutando Api EPP...";
        }
    }
}
