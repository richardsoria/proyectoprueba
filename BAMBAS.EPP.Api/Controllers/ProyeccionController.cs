﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.EPP.Servicio.Consultas;
using BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Api.Controllers
{
    [ApiController]
    [Route("proyeccion")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProyeccionController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasProyeccion _consultasProyeccion;
        private readonly IMediator _mediator;
        public ProyeccionController(
            IDataTableService dataTableService,
            IConsultasProyeccion consultasProyeccion,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasProyeccion = consultasProyeccion;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string dscrpcn, string anio)
        {
            var parameters = _dataTableService.GetSentParameters();
            var proyeccion = await _consultasProyeccion.ObtenerDataTable(parameters, dscrpcn, anio);
            return Ok(proyeccion);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoProyeccionInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasProyeccion.Obtener(id);
            return Ok(empresas);
        }
        [HttpPost("actualizar-aprobado")]
        public async Task<IActionResult> ActualizarAprobado(ComandoProyeccionActualizarAprobado entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoProyeccionActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoProyeccionEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar-detalle")]
        public async Task<IActionResult> EliminarDetalleDatos(ComandoProyeccionEliminarDetalle entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
