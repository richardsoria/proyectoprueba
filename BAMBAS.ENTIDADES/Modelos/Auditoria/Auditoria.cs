﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BAMBAS.ENTIDADES.Modelos.Auditoria
{
    public class EntidadAuditoria : IAuditoria
    {
        public int ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; } = "A";
        public DateTime? FESTDO { get; set; }
    }
    public interface IAuditoria
    {
        public int ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; }
        public DateTime? FESTDO { get; set; }
    }
}
