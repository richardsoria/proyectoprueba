﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.ENTIDADES
{

    [AttributeUsage(AttributeTargets.Class)]
    public class BaseDatosAttribute : Attribute
    {
        public string nombreTabla;
        public string esquema;
        public BaseDatosAttribute() { }
    }
}
