﻿            using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasError
    {
        Task<DataTablesStructs.ReturnedData<ErrorModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasError: IConsultasError
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasError(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<ErrorModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            //var param = new DynamicParameters();
            //param.Add("@UBGO", company.UBGO);
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<ErrorModel>(ProcedimientosAlmacenados.Error.ObtenerTodos/*, param*/);
          
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.EESQMA.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.EMSSGE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.ENMBR.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.EPRCDRE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.ERRORLNE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.ESTTE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.ESVRT.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }
            return empresas.ConvertirTabla(parameters);
        }
    }
}
