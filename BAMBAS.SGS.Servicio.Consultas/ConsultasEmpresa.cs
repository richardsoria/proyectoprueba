﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasEmpresa
    {
        Task<EmpresaModel> Obtener(int id);
        Task<List<EmpresaModel>> ObtenerActivos();
        Task<DataTablesStructs.ReturnedData<EmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasEmpresa : IConsultasEmpresa
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasEmpresa(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<EmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<EmpresaModel>(ProcedimientosAlmacenados.Empresa.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.NEMPRSA.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.RUC.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.DRCCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<EmpresaModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<EmpresaModel>(ProcedimientosAlmacenados.Empresa.ObtenerEmpresa, param);
            return ret.Entidad;
        }
        public async Task<List<EmpresaModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<EmpresaModel>(ProcedimientosAlmacenados.Empresa.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
