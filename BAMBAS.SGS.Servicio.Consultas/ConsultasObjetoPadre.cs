﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasObjetoPadre
    {
        Task<ObjetoPadreModel> Obtener(int id);
        Task<List<ObjetoPadreModel>> ObtenerTodos();
        Task<List<ObjetoPadreModel>> ObtenerActivos();
        Task<DataTablesStructs.ReturnedData<ObjetoCustom>> ObtenerPadreDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ObjetoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idobjetopadre);
    }

    public class ConsultasObjetoPadre : IConsultasObjetoPadre
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasObjetoPadre(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<List<ObjetoPadreModel>> ObtenerTodos()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<ObjetoPadreModel>(ProcedimientosAlmacenados.ObjetoPadre.ObtenerTodos);
        }

        public async Task<List<ObjetoPadreModel>> ObtenerActivos()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<ObjetoPadreModel>(ProcedimientosAlmacenados.ObjetoPadre.ObtenerActivos);
        }
        public async Task<ObjetoPadreModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ObjetoPadreModel>(ProcedimientosAlmacenados.ObjetoPadre.Obtener, param);
            return ret.Entidad;
        }

        public async Task<DataTablesStructs.ReturnedData<ObjetoCustom>> ObtenerPadreDataTable(DataTablesStructs.SentParameters parameters)
        {
            var grupodato = await ObtenerDatos(parameters);
            grupodato = grupodato.Where(x => string.IsNullOrEmpty(x.IDOBJTOPDRE)).ToList();
            return grupodato.ConvertirTabla(parameters);
        }

        public async Task<DataTablesStructs.ReturnedData<ObjetoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idobjetopadre)
        {
            var grupodato = await ObtenerDatos(parameters);
            grupodato = grupodato.Where(x => x.IDOBJTOPDRE == idobjetopadre).ToList();
            return grupodato.ConvertirTabla(parameters);
        }

        private async Task<List<ObjetoCustom>> ObtenerDatos(DataTablesStructs.SentParameters parameters)
        {
            var grupodato = await _configuracionConexionSql.EjecutarProcedimiento<ObjetoCustom>(ProcedimientosAlmacenados.ObjetoPadre.ObtenerTodos/*, param*/);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                grupodato = grupodato.Where(x => x.DOBJTO.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.NOBJTO.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.OBJTOPDRE.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return grupodato;
        }
    }
}
