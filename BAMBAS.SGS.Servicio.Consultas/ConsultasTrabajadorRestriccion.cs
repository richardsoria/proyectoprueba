﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasTrabajadorRestriccion
    {
        Task<DataTablesStructs.ReturnedData<TrabajadorRestriccionCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador);
        Task<List<TrabajadorRestriccionCustom>> ListarDetalleRestriccion(int idTrabajador);
        Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagrestrccion, int? idgrupocargo);
    }
    public class ConsultasTrabajadorRestriccion : IConsultasTrabajadorRestriccion
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasTrabajadorRestriccion(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<TrabajadorRestriccionCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador)
        {
            var param = new DynamicParameters();
            param.Add("@IDTRBJDR", idTrabajador);
            var Restriccion = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorRestriccionCustom>(ProcedimientosAlmacenados.TrabajadorRestriccion.ListarRestriccion, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                Restriccion = Restriccion.Where(x => x.DSCRPCNARTCLO.Contains(parameters.SearchValue.ToUpper()) ||
                                            x.CSAP.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return Restriccion.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagrestrccion, int? idgrupocargo)
        {
            var Restriccion = new List<ArticuloModel>();
            if (flagrestrccion)
            {
                Restriccion = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas/*, param*/);
                Restriccion = Restriccion.Where(x => x.FKIT == false && x.FMNJORSTRCN == flagrestrccion && x.GDESTDO == "A").ToList();
            }
            else
            {
                var param = new DynamicParameters();
                param.Add("@ID", idgrupocargo);
                Restriccion = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.GrupoCargo.ListarDetalle, param);
                Restriccion = Restriccion.Where(x => x.FMNJORSTRCN == true && x.GDESTDO == "A").ToList();
            }

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                Restriccion = Restriccion.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper())).ToList();
            }
            return Restriccion.ConvertirTabla(parameters);
        }
        public async Task<List<TrabajadorRestriccionCustom>> ListarDetalleRestriccion(int idTrabajador)
        {
            var param = new DynamicParameters();
            param.Add("@IDTRBJDR", idTrabajador);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorRestriccionCustom>(ProcedimientosAlmacenados.TrabajadorRestriccion.ListarRestriccion, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }  
    }
}