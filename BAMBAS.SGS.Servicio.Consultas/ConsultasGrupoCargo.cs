﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasGrupoCargo
    {
        Task<GrupoCargoModel> Obtener(int id);
        Task<List<GrupoCargoModel>> ListarDetalle(int id);
        Task<List<GrupoCargoModel>> ObtenerActivos();
        Task<List<GrupoCargoModel>> ObtenerTodas();
        Task<DataTablesStructs.ReturnedData<GrupoCargoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasGrupoCargo : IConsultasGrupoCargo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasGrupoCargo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<GrupoCargoModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
                var empresas = await _configuracionConexionSql.EjecutarProcedimiento<GrupoCargoModel>(ProcedimientosAlmacenados.GrupoCargo.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<GrupoCargoModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GrupoCargoModel>(ProcedimientosAlmacenados.GrupoCargo.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<GrupoCargoModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GrupoCargoModel>(ProcedimientosAlmacenados.GrupoCargo.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<GrupoCargoModel>> ObtenerTodas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GrupoCargoModel>(ProcedimientosAlmacenados.GrupoCargo.ObtenerTodas, param);
            return ret;
        }
        public async Task<List<GrupoCargoModel>> ListarDetalle(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GrupoCargoModel>(ProcedimientosAlmacenados.GrupoCargo.ListarDetalle, param);
            return ret;
        }
    }
}
