﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasPerfilUsuario
    {
        Task<DataTablesStructs.ReturnedData<PerfilUsuarioCustom>> Listar(DataTablesStructs.SentParameters parameters, string idPerfil);

        Task<DataTablesStructs.ReturnedData<UsuarioPerfilCustom>> ListarPorUsuario(DataTablesStructs.SentParameters parameters, string idUsuario);
    }
    public class ConsultasPerfilUsuario : IConsultasPerfilUsuario
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasPerfilUsuario(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<PerfilUsuarioCustom>> Listar(DataTablesStructs.SentParameters parameters, string idPerfil)
        {
            if (string.IsNullOrEmpty(idPerfil))
            {
                var vacia = new List<PerfilUsuarioCustom>();
                return vacia.ConvertirTabla(parameters);
            }

            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfil);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<PerfilUsuarioCustom>(ProcedimientosAlmacenados.PerfilUsuario.ListarPerfil, paramet);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                perfiles = perfiles.Where(x =>
                                        x.NYAPLLDS.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }
            return perfiles.ConvertirTabla(parameters);
        }

        public async Task<DataTablesStructs.ReturnedData<UsuarioPerfilCustom>> ListarPorUsuario(DataTablesStructs.SentParameters parameters, string idUsuario)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDUSRO", idUsuario);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<UsuarioPerfilCustom>(ProcedimientosAlmacenados.PerfilUsuario.ListarPorUsuario, paramet);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                perfiles = perfiles.Where(x =>
                                        x.DPRFL.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }
            return perfiles.ConvertirTabla(parameters);
        }
    }
}
