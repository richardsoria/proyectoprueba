﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasUsuario
    {
        Task<List<UsuarioModel>> ListadoUsuarios();
        Task<UsuarioModel> ObtenerXid(int id);
        Task<UsuarioModel> Obtener(string cusro);
        Task<DataTablesStructs.ReturnedData<UsuarioModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);

    }
    public class ConsultasUsuario : IConsultasUsuario
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasUsuario(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<UsuarioModel> Obtener(string cusro)
        {
            var param = new DynamicParameters();
            param.Add("@CUSRO", cusro);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<UsuarioModel>(ProcedimientosAlmacenados.Usuario.ObtenerUsuario, param);
            return ret.Entidad;
        }

        public async Task<UsuarioModel> ObtenerXid(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<UsuarioModel>(ProcedimientosAlmacenados.Usuario.ObtenerUsuarioPorId, param);
            return ret.Entidad;
        }

        public async Task<List<UsuarioModel>> ListadoUsuarios()
        {
            var param = new DynamicParameters();
            return await _configuracionConexionSql.EjecutarProcedimiento<UsuarioModel>(ProcedimientosAlmacenados.Usuario.ObtenerTodas);
        }

        public async Task<DataTablesStructs.ReturnedData<UsuarioModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var usuarios = await _configuracionConexionSql.EjecutarProcedimiento<UsuarioModel>(ProcedimientosAlmacenados.Usuario.ObtenerTodas/*, param*/);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                usuarios = usuarios.Where(x => x.NYAPLLDS.Contains(parameters.SearchValue.ToUpper()) || x.CUSRO.Contains(parameters.SearchValue.ToUpper())).ToList();
            }
            return usuarios.ConvertirTabla(parameters);
        }
    }
}
