﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasSuperintendencia
    {
        Task<SuperintendenciaCustom> Obtener(int id);
        Task<List<SuperintendenciaCustom>> ObtenerActivas(int idGerencia);
        Task<List<SuperintendenciaCustom>> ObtenerTodas(int idGerencia);
        Task<DataTablesStructs.ReturnedData<SuperintendenciaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia);
    }
    public class ConsultasSuperintendencia : IConsultasSuperintendencia
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasSuperintendencia(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<SuperintendenciaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRNCA", idGerencia == 0 ? null : idGerencia.ToString());
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<SuperintendenciaCustom>(ProcedimientosAlmacenados.Superintendencia.ObtenerTodas, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<SuperintendenciaCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<SuperintendenciaCustom>(ProcedimientosAlmacenados.Superintendencia.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<SuperintendenciaCustom>> ObtenerActivas(int idGerencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRNCA", idGerencia);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<SuperintendenciaCustom>(ProcedimientosAlmacenados.Superintendencia.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<SuperintendenciaCustom>> ObtenerTodas(int idGerencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRNCA", idGerencia);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<SuperintendenciaCustom>(ProcedimientosAlmacenados.Superintendencia.ObtenerTodas, param);
            return ret;
        }
    }
}
