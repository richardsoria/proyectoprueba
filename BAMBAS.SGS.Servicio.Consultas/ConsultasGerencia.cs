﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasGerencia
    {
        Task<GerenciaModel> Obtener(int id);
        Task<List<GerenciaModel>> ObtenerActivas();
        Task<List<GerenciaModel>> ObtenerTodas();
        Task<DataTablesStructs.ReturnedData<GerenciaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasGerencia : IConsultasGerencia
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasGerencia(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<GerenciaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<GerenciaModel>(ProcedimientosAlmacenados.Gerencia.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<GerenciaModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GerenciaModel>(ProcedimientosAlmacenados.Gerencia.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<GerenciaModel>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GerenciaModel>(ProcedimientosAlmacenados.Gerencia.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<GerenciaModel>> ObtenerTodas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GerenciaModel>(ProcedimientosAlmacenados.Gerencia.ObtenerTodas, param);
            return ret;
        }
    }
}
