﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasKit
    {
        Task<KitModel> Obtener(int id);
        Task<List<KitModel>> ListarDetalle(int id);
        Task<List<KitModel>> ObtenerActivas();
        Task<DataTablesStructs.ReturnedData<KitModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasKit : IConsultasKit
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasKit(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<KitModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<KitModel>(ProcedimientosAlmacenados.Kit.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas/*, param*/);

            empresas = empresas.Where(x => x.FKIT == false && x.GDESTDO == "A").ToList();
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }
            return empresas.ConvertirTabla(parameters);
        }

        public async Task<KitModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<KitModel>(ProcedimientosAlmacenados.Kit.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<KitModel>> ListarDetalle(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<KitModel>(ProcedimientosAlmacenados.Kit.ListarDetalle, param);
            return ret;
        }
        public async Task<List<KitModel>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<KitModel>(ProcedimientosAlmacenados.Kit.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }

    }
}
