﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasGrupoDatoDetalle
    {
        Task<GrupoDatoDetalleEmpresaModel> Obtener(int id);
        Task<DataTablesStructs.ReturnedData<GrupoDatoDetalleEmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string codigoGrupoDato);
        Task<List<GrupoDatoDetalleEmpresaModel>> ObtenerDatosSelect(string estados);
    }
    public class ConsultasGrupoDatoDetalle : IConsultasGrupoDatoDetalle
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasGrupoDatoDetalle(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<GrupoDatoDetalleEmpresaModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
                var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GrupoDatoDetalleEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.ObtenerGrupoDatoDetalle, param);
            return ret.Entidad;
        }

        public async Task<DataTablesStructs.ReturnedData<GrupoDatoDetalleEmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string codigoGrupoDato)
        {
            var param = new DynamicParameters();
            param.Add("@CGDTO", codigoGrupoDato);
            var grupodato = await _configuracionConexionSql.EjecutarProcedimiento<GrupoDatoDetalleEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.ObtenerTodos, param);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                grupodato = grupodato.Where(x => x.AGDDTLLE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.DGDDTLLE.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.VLR1.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.CGDTO.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }
            return grupodato.ConvertirTabla(parameters);
        }

        public async Task<List<GrupoDatoDetalleEmpresaModel>> ObtenerDatosSelect(string grupodato)
        {
            var param = new DynamicParameters();
            param.Add("@CGDTO", grupodato);
            var grupodatodet = await _configuracionConexionSql.EjecutarProcedimiento<GrupoDatoDetalleEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.ObtenerActivos, param);

            return grupodatodet;
        }
    }
}
