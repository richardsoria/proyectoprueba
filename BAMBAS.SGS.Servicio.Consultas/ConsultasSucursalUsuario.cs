﻿            using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasSucursalUsuario
    {
        Task<DataTablesStructs.ReturnedData<SucursalUsuarioCustom>> Listar(DataTablesStructs.SentParameters parameters, string idSucursal);
        Task<DataTablesStructs.ReturnedData<UsuarioSucursalCustom>> ObtenerSucursalesDataTable(DataTablesStructs.SentParameters parameters, string idEmpresa, string idUsuario);
    }
    public class ConsultasSucursalUsuario: IConsultasSucursalUsuario
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasSucursalUsuario(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<SucursalUsuarioCustom>> Listar(DataTablesStructs.SentParameters parameters,string idSucursal)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDSCRSL", idSucursal);
            var Sucursales = await _configuracionConexionSql.EjecutarProcedimiento<SucursalUsuarioCustom>(ProcedimientosAlmacenados.SucursalUsuario.ListarUsuarios,paramet);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                Sucursales = Sucursales.Where(x =>
                                        x.NYAPLLDS.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }
            return Sucursales.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<UsuarioSucursalCustom>> ObtenerSucursalesDataTable(DataTablesStructs.SentParameters parameters, string idEmpresa, string idUsuario)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDEMPRSA", idEmpresa);
            paramet.Add("@IDUSRO", idUsuario);
            var Sucursales = await _configuracionConexionSql.EjecutarProcedimiento<UsuarioSucursalCustom>(ProcedimientosAlmacenados.SucursalUsuario.ListarSucursalesUsuarios, paramet);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                Sucursales = Sucursales.Where(x =>
                                        x.NSCRSL.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }
            return Sucursales.ConvertirTabla(parameters);
        }
    }
}
