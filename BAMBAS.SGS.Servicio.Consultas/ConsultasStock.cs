﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasStock
    {
        Task<StockCustom> Obtener(int id);
        Task<List<StockCustom>> ObtenerActivas();
        Task<DataTablesStructs.ReturnedData<StockCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idalmcn);
        Task<DataTablesStructs.ReturnedData<StockModel>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<StockActualizadosCustom>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters,int idalmacen);
        Task<StockModel> ObtenerUltimaActualizacion(int idalmacen);


    }
    public class ConsultasStock : IConsultasStock
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasStock(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<StockCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idalmcn)
        {
            var param = new DynamicParameters();
            param.Add("@IDALMCN", idalmcn);
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<StockCustom>(ProcedimientosAlmacenados.Stock.ObtenerTodas, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCNARTCLO.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<StockModel>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<StockModel>(ProcedimientosAlmacenados.Stock.ObtenerTodas/*, param*/);
            empresas = empresas.Where(x => x.GDESTDO == "A").ToList();
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                //empresas = empresas.Where(x => (x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()) || x.CSAP.Contains(parameters.SearchValue.ToUpper())))
                    //.ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }
        public async Task<StockCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<StockCustom>(ProcedimientosAlmacenados.Stock.Obtener, param);
            return ret.Entidad;
        }
        
        public async Task<List<StockCustom>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<StockCustom>(ProcedimientosAlmacenados.Stock.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<DataTablesStructs.ReturnedData<StockActualizadosCustom>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters, int idalmacen)
        {
            var param = new DynamicParameters();
            param.Add("@IDALMCN", idalmacen);
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<StockActualizadosCustom>(ProcedimientosAlmacenados.Stock.ObtenerUltimosActualizados, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                //empresas = empresas.Where(x => x.STCK.Contains(parameters.SearchValue.ToUpper()))
                    //.ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<StockModel> ObtenerUltimaActualizacion(int idalmacen)
        {
            var param = new DynamicParameters();
            param.Add("@IDALMCN", idalmacen);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<StockModel>(ProcedimientosAlmacenados.Stock.ObtenerUltimaActualizacion,param);
            return ret.Entidad;
        }
    }
}
