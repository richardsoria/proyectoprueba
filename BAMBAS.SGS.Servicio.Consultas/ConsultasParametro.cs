﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasParametro
    {
        Task<DataTablesStructs.ReturnedData<ParametroEmpresaModel>> ListarParametro(DataTablesStructs.SentParameters parameters);
        Task<ParametroEmpresaModel> ObtenerParametros(ParametroEmpresaModel entidad);
        Task<ParametroEmpresaModel> ObtenerParametroPorAbreviacion(string aprmtro);
    }
    public class ConsultasParametro : IConsultasParametro
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasParametro(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<ParametroEmpresaModel>> ListarParametro(DataTablesStructs.SentParameters parameters)
        {
            var parametroempresa = await _configuracionConexionSql.EjecutarProcedimiento<ParametroEmpresaModel>(ProcedimientosAlmacenados.ParametroEmpresa.ListarParametros);

            Func<ParametroEmpresaModel, dynamic> columnaAOrdenar;
            switch (parameters.OrderColumn)
            {
                case "0":
                    columnaAOrdenar = x => x.DPRMTRO;
                    break;
                //case "1":
                //    columnaAOrdenar = x => x.DPRMTRO;
                //    break;
                default:
                    columnaAOrdenar = x => x.ID;
                    break;
            }

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                parametroempresa = parametroempresa.Where(x =>
                                        x.DPRMTRO.Contains(parameters.SearchValue.ToUpper()) ||
                                        x.APRMTRO.Contains(parameters.SearchValue.ToUpper()) ||
                                        x.VLR1.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }

            return parametroempresa.ConvertirTabla(parameters, columnaAOrdenar);
        }

        public async Task<ParametroEmpresaModel> ObtenerParametros(ParametroEmpresaModel entidad)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ParametroEmpresaModel>(ProcedimientosAlmacenados.ParametroEmpresa.ObtenerParametros, parametros);
            return ret.Entidad;
        }
        public async Task<ParametroEmpresaModel> ObtenerParametroPorAbreviacion(string aprmtro)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@APRMTRO", aprmtro);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ParametroEmpresaModel>(ProcedimientosAlmacenados.ParametroEmpresa.ObtenerParametroPorAbreviacion, parametros);
            return ret.Entidad;
        }
    }
}
