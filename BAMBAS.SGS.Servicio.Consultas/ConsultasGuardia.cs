﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasGuardia
    {
        Task<GuardiaModel> Obtener(int id);
        Task<GuardiaModel> ObtenerPorId(int id);
        Task<List<GuardiaModel>> ObtenerActivas();
        Task<DataTablesStructs.ReturnedData<GuardiaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasGuardia : IConsultasGuardia
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasGuardia(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<GuardiaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<GuardiaCustom>(ProcedimientosAlmacenados.Guardia.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<GuardiaModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GuardiaModel>(ProcedimientosAlmacenados.Guardia.Obtener, param);
            return ret.Entidad;
        }
        public async Task<GuardiaModel> ObtenerPorId(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GuardiaModel>(ProcedimientosAlmacenados.Guardia.ObtenerPorId, param);
            return ret.Entidad;
        }
        public async Task<List<GuardiaModel>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<GuardiaModel>(ProcedimientosAlmacenados.Guardia.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
