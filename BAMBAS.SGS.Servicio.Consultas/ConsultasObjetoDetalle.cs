﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasObjetoDetalle
    {
        Task<ObjetoDetalleModel> Obtener(int id);
        Task<DataTablesStructs.ReturnedData<ObjetoDetalleModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string codigoObjeto);
        Task<List<ObjetoDetalleModel>> ObtenerActivos(string idObjeto);
    }
    public class ConsultasObjetoDetalle : IConsultasObjetoDetalle
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasObjetoDetalle(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<ObjetoDetalleModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ObjetoDetalleModel>(ProcedimientosAlmacenados.ObjetoDetalle.Obtener, param);
            return ret.Entidad;
        }

        public async Task<DataTablesStructs.ReturnedData<ObjetoDetalleModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string codigoObjeto)
        {
            var param = new DynamicParameters();
            param.Add("@IDOBJTO", codigoObjeto);
            var Objeto = await _configuracionConexionSql.EjecutarProcedimiento<ObjetoDetalleModel>(ProcedimientosAlmacenados.ObjetoDetalle.ObtenerTodos, param);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                Objeto = Objeto.Where(x => x.DOBJTO.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.NOBJTO.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }
            return Objeto.ConvertirTabla(parameters);
        }

        public async Task<List<ObjetoDetalleModel>> ObtenerActivos(string idObjeto)
        {
            var param = new DynamicParameters();
            param.Add("@IDOBJTO", idObjeto);
            var Objetodet = await _configuracionConexionSql.EjecutarProcedimiento<ObjetoDetalleModel>(ProcedimientosAlmacenados.ObjetoDetalle.ObtenerActivos, param);

            return Objetodet;
        }
    }
}
