﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasPerfilObjeto
    {
        Task<List<PerfilObjetoCustom>> Listar(string idPerfil);
        Task<List<PerfilObjetoCustom>> ListarMenuLateral(string idModulo, string idPerfil);
        Task<List<PerfilObjetoCustom>> ListarModulos(string idPerfil);
        Task<bool> ValidarAcceso(string idPerfiles, string objeto);
        Task<List<PerfilObjetoDetalleCustom>> VerificarControles(string controles, string idPerfiles);
    }
    public class ConsultasPerfilObjeto : IConsultasPerfilObjeto
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasPerfilObjeto(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<PerfilObjetoCustom>> Listar(string idPerfil)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfil);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<PerfilObjetoCustom>(ProcedimientosAlmacenados.PerfilObjeto.ListarPerfil, paramet);
            return perfiles;
        }
        public async Task<List<PerfilObjetoCustom>> ListarMenuLateral(string idModulo, string idPerfil)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfil);
            paramet.Add("@IDMDLO", idModulo);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimientoRespuesta<PerfilObjetoCustom>(ProcedimientosAlmacenados.PerfilObjeto.ListarPerfilMenuLateral, paramet);
            return perfiles.Lista.Where(x => x.CHECKEADO).ToList();
        }
        public async Task<List<PerfilObjetoCustom>> ListarModulos(string idPerfil)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfil);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimientoRespuesta<PerfilObjetoCustom>(ProcedimientosAlmacenados.PerfilObjeto.ListarPerfilModulos, paramet);
            return perfiles.Lista;
        }
        public async Task<List<PerfilObjetoDetalleCustom>> VerificarControles(string controles, string idPerfiles)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfiles);
            paramet.Add("@CTRLS", controles);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimientoRespuesta<PerfilObjetoDetalleCustom>(ProcedimientosAlmacenados.PerfilObjeto.ListarControlesPorPerfil, paramet);
            return perfiles.Lista;
        }
        public async Task<bool> ValidarAcceso(string idPerfiles, string objeto)
        {
            var paramet = new DynamicParameters();
            paramet.Add("@IDPRFL", idPerfiles);
            paramet.Add("@OBJTO", objeto);
            var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<bool>(ProcedimientosAlmacenados.PerfilObjeto.ValidarObjetoPorPerfiles, "", paramet);
            return perfiles;
        }
    }
}
