﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasSucursal
    {
        Task<DataTablesStructs.ReturnedData<SucursalModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int empresaId);
        Task<SucursalModel> Obtener(int id);
    }
    public class ConsultasSucursal : IConsultasSucursal
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasSucursal(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<DataTablesStructs.ReturnedData<SucursalModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int empresaId)
        {
            var param = new DynamicParameters();
            param.Add("@IDEMPRSA", empresaId);
            var grupodato = await _configuracionConexionSql.EjecutarProcedimiento<SucursalModel>(ProcedimientosAlmacenados.Sucursal.ObtenerTodas, param);
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                grupodato = grupodato.Where(x =>
                                        x.DSCRSL.Contains(parameters.SearchValue.ToUpper()) ||
                                        x.NSCRSL.Contains(parameters.SearchValue.ToUpper()))
                                    .ToList();
            }
            return grupodato.ConvertirTabla(parameters);
        }

        public async Task<SucursalModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<SucursalModel>(ProcedimientosAlmacenados.Sucursal.ObtenerSucursal, param);
            return ret.Entidad;
        }
    }
}
