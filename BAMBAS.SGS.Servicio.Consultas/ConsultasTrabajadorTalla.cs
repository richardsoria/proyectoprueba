﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasTrabajadorTalla
    {
        Task<DataTablesStructs.ReturnedData<TrabajadorTallaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador);
        Task<List<TrabajadorTallaCustom>> ListarDetalleTalla(int idTrabajador);
        Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagtlla, int? idgrupocargo);
    }
    public class ConsultasTrabajadorTalla : IConsultasTrabajadorTalla
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasTrabajadorTalla(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<TrabajadorTallaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idTrabajador)
        {
            var param = new DynamicParameters();
            param.Add("@IDTRBJDR", idTrabajador);
            var talla = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorTallaCustom>(ProcedimientosAlmacenados.TrabajadorTalla.ListarTalla, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                talla = talla.Where(x => x.DSCRPCNARTCLO.Contains(parameters.SearchValue.ToUpper()) ||
                                            x.CSAP.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return talla.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters, bool flagtlla, int? idgrupocargo)
        {
            var talla = new List<ArticuloModel>();
            if (flagtlla)
            {
                talla = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas/*, param*/);
                talla = talla.Where(x => x.FKIT == false && x.FMNJOTLLS == flagtlla && x.GDESTDO == "A").ToList();
            }
            else
            {
                var param = new DynamicParameters();
                param.Add("@ID", idgrupocargo);
                talla = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.GrupoCargo.ListarDetalle, param);
                talla = talla.Where(x => x.FMNJOTLLS == true &&  x.GDESTDO == "A").ToList();
            }
           
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                talla = talla.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper())).ToList();
            }
            return talla.ConvertirTabla(parameters);
        }
        public async Task<List<TrabajadorTallaCustom>> ListarDetalleTalla(int idTrabajador)
        {
            var param = new DynamicParameters();
            param.Add("@IDTRBJDR", idTrabajador);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<TrabajadorTallaCustom>(ProcedimientosAlmacenados.TrabajadorTalla.ListarTalla, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }    
    }
}