﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasFamilia
    {
        Task<FamiliaCustom> Obtener(int id);
        Task<List<FamiliaCustom>> ObtenerAutocomplete(string term);
        Task<List<FamiliaCustom>> ObtenerActivas(int idFamilia);
        Task<List<FamiliaCustom>> ObtenerTodas(int idFamilia);
        Task<List<FamiliaCustom>> ObtenerActivasSubfamilia(int idFamilia, bool? soloHijos = false);
        Task<DataTablesStructs.ReturnedData<FamiliaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false);
    }
    public class ConsultasFamilia : IConsultasFamilia
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasFamilia(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<FamiliaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false)
        {
            var param = new DynamicParameters();
            param.Add("@IDFMLAPDRE", idFamilia == 0 ? null : idFamilia.ToString());
            param.Add("@SOLOHIJOS", soloHijos);
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<FamiliaCustom>(ProcedimientosAlmacenados.Familia.ObtenerTodas, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }
            return empresas.ConvertirTabla(parameters);
        }

        public async Task<FamiliaCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<FamiliaCustom>(ProcedimientosAlmacenados.Familia.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<FamiliaCustom>> ObtenerActivas(int idFamilia)
        {
            var param = new DynamicParameters();
            param.Add("@IDFMLAPDRE", idFamilia == 0 ? null : idFamilia.ToString());
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<FamiliaCustom>(ProcedimientosAlmacenados.Familia.ObtenerTodasPadre, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<FamiliaCustom>> ObtenerTodas(int idFamilia)
        {
            var param = new DynamicParameters();
            param.Add("@IDFMLAPDRE", idFamilia == 0 ? null : idFamilia.ToString());
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<FamiliaCustom>(ProcedimientosAlmacenados.Familia.ObtenerTodasPadre, param);
            return ret;
        }
        public async Task<List<FamiliaCustom>> ObtenerActivasSubfamilia(int idFamilia, bool? soloHijos = false)
        {
            var param = new DynamicParameters();
            param.Add("@IDFMLAPDRE", idFamilia == 0 ? null : idFamilia.ToString());
            param.Add("@SOLOHIJOS", soloHijos);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<FamiliaCustom>(ProcedimientosAlmacenados.Familia.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }

        public async Task<List<FamiliaCustom>> ObtenerAutocomplete(string term)
        {
            var param = new DynamicParameters();
            param.Add("@IDFMLAPDRE", 0);
            param.Add("@SOLOHIJOS", true);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<FamiliaCustom>(ProcedimientosAlmacenados.Familia.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A" && (x.DSCRPCN.Contains(term.ToUpper()))).ToList();
            return ret;
        }
    }
}
