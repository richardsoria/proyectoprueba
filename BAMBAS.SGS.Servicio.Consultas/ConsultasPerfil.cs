﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasPerfil
    {
        Task<DataTablesStructs.ReturnedData<PerfilModel>> ListarPerfil(DataTablesStructs.SentParameters parameters);
        Task<List<PerfilModel>> ListarActivos();
        Task<PerfilModel> ObtenerPerfil(PerfilModel entidad);
    }
    public class ConsultasPerfil : IConsultasPerfil
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasPerfil(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<DataTablesStructs.ReturnedData<PerfilModel>> ListarPerfil(DataTablesStructs.SentParameters parameters)
        {
            var paramet = new DynamicParameters();
            var perfiles = await ListarTodos();

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                perfiles = perfiles.Where(x => x.DPRFL.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return perfiles.ConvertirTabla(parameters);
        }
        private async Task<List<PerfilModel>> ListarTodos()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<PerfilModel>(ProcedimientosAlmacenados.Perfil.ListarPerfil);
        }
        public async Task<List<PerfilModel>> ListarActivos()
        {
            var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<PerfilModel>(ProcedimientosAlmacenados.Perfil.ListarPerfil);
            return perfiles.Where(x => x.GDESTDO == "A").ToList();
        }
        public async Task<PerfilModel> ObtenerPerfil(PerfilModel entidad)
        {
            var perfil = new DynamicParameters();
            perfil.Add("@ID", entidad.ID);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<PerfilModel>(ProcedimientosAlmacenados.Perfil.ObtenerPerfil, perfil);
            return ret.Entidad;
        }
    }
}
