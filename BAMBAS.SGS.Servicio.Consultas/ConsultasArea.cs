﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasArea
    {
        Task<AreaCustom> Obtener(int id);
        Task<List<AreaCustom>> ObtenerActivas(int idSuperintendencia);
        Task<List<AreaCustom>> ObtenerTodas(int idSuperintendencia);
        Task<DataTablesStructs.ReturnedData<AreaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia,int idSuperintendencia);
    }
    public class ConsultasArea : IConsultasArea
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasArea(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<AreaCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia,int idSuperintendencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDSPRINTNDNCA", idSuperintendencia == 0 ? null : idSuperintendencia.ToString());
            param.Add("@IDGRNCA", idGerencia == 0 ? null : idGerencia.ToString());
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<AreaCustom>(ProcedimientosAlmacenados.Area.ObtenerTodas, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<AreaCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<AreaCustom>(ProcedimientosAlmacenados.Area.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<AreaCustom>> ObtenerActivas(int idSuperintendencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDSPRINTNDNCA", idSuperintendencia == 0 ? null : idSuperintendencia.ToString());
            param.Add("@IDGRNCA", null);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<AreaCustom>(ProcedimientosAlmacenados.Area.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<AreaCustom>> ObtenerTodas(int idSuperintendencia)
        {
            var param = new DynamicParameters();
            param.Add("@IDSPRINTNDNCA", idSuperintendencia == 0 ? null : idSuperintendencia.ToString());
            param.Add("@IDGRNCA", null);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<AreaCustom>(ProcedimientosAlmacenados.Area.ObtenerTodas, param);
            return ret;
        }
    }
}
