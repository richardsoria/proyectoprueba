﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasCargo
    {
        Task<CargoCustom> Obtener(int id);
        Task<List<CargoCustom>> ObtenerActivos(int idGrupoCargo);
        Task<List<CargoCustom>> ObtenerTodas(int idGrupoCargo);
        Task<DataTablesStructs.ReturnedData<CargoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGrupoCargo);
    }
    public class ConsultasCargo : IConsultasCargo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasCargo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<CargoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGrupoCargo)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRPO", idGrupoCargo == 0 ? null : idGrupoCargo.ToString());
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<CargoCustom>(ProcedimientosAlmacenados.Cargo.ObtenerTodas,param);
            
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<CargoCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<CargoCustom>(ProcedimientosAlmacenados.Cargo.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<CargoCustom>> ObtenerActivos(int idGrupoCargo)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRPO", idGrupoCargo);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<CargoCustom>(ProcedimientosAlmacenados.Cargo.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<CargoCustom>> ObtenerTodas(int idGrupoCargo)
        {
            var param = new DynamicParameters();
            param.Add("@IDGRPO", idGrupoCargo);
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<CargoCustom>(ProcedimientosAlmacenados.Cargo.ObtenerTodas, param);
            return ret;
        }
    }
}
