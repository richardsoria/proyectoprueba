﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasAlmacen
    {
        Task<AlmacenCustom> Obtener(int id);
        Task<List<AlmacenCustom>> ObtenerActivas();
        Task<DataTablesStructs.ReturnedData<AlmacenCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string gdtalmcn);
    }
    public class ConsultasAlmacen : IConsultasAlmacen
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasAlmacen(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<AlmacenCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtalmcn)
        {
            var param = new DynamicParameters();
            param.Add("@GDTALMCN", gdtalmcn);
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<AlmacenCustom>(ProcedimientosAlmacenados.Almacen.ObtenerTodas, param);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<AlmacenCustom> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<AlmacenCustom>(ProcedimientosAlmacenados.Almacen.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<AlmacenCustom>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<AlmacenCustom>(ProcedimientosAlmacenados.Almacen.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
