﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasUbigeo
    {
        Task<List<UbigeoModel>> ObtenerTodo();
        Task<List<UbigeoCustom>> ObtenerDepartamentos(string cps);
        Task<List<UbigeoCustom>> ObtenerProvincias(string codigo, string cps);
        Task<List<UbigeoCustom>> ObtenerDistritos(string codigo, string cps);
        Task<List<PaisCustom>> ObtenerPaises();
    }
    public class ConsultasUbigeo: IConsultasUbigeo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasUbigeo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<UbigeoModel>> ObtenerTodo()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<UbigeoModel>(ProcedimientosAlmacenados.Ubigeo.ObtenerTodos);
        }
        public async Task<List<UbigeoCustom>> ObtenerDepartamentos(string cps)
        {
            var parametro = new DynamicParameters();
            parametro.Add("@CDGO", "");
            parametro.Add("@VALOR", 1);
            parametro.Add("@CPS", cps);
            return await _configuracionConexionSql.EjecutarProcedimiento<UbigeoCustom>(ProcedimientosAlmacenados.Ubigeo.Obtener, parametro);
        }
        public async Task<List<UbigeoCustom>> ObtenerProvincias(string codigo, string cps)
        {
            var parametro = new DynamicParameters();
            parametro.Add("@CDGO", codigo);
            parametro.Add("@VALOR", 2);
            parametro.Add("@CPS", cps);
            return await _configuracionConexionSql.EjecutarProcedimiento<UbigeoCustom>(ProcedimientosAlmacenados.Ubigeo.Obtener, parametro);
        }
        public async Task<List<UbigeoCustom>> ObtenerDistritos(string codigo, string cps)
        {
            var parametro = new DynamicParameters();
            parametro.Add("@CDGO", codigo);
            parametro.Add("@VALOR", 3);
            parametro.Add("@CPS", cps);
            return await _configuracionConexionSql.EjecutarProcedimiento<UbigeoCustom>(ProcedimientosAlmacenados.Ubigeo.Obtener, parametro);
        }
        public async Task<List<PaisCustom>> ObtenerPaises()
        {
            var parametro = new DynamicParameters();
            return await _configuracionConexionSql.EjecutarProcedimiento<PaisCustom>(ProcedimientosAlmacenados.Ubigeo.ObtenerPaises);
        }
    }
}
