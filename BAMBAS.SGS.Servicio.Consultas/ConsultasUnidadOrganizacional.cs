﻿using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasUnidadOrganizacional
    {
        Task<List<UnidadOrganizacionalModel>> ObtenerActivos();
    }
    public class ConsultasUnidadOrganizacional : IConsultasUnidadOrganizacional
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasUnidadOrganizacional(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<UnidadOrganizacionalModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<UnidadOrganizacionalModel>(ProcedimientosAlmacenados.UnidadOrganizacional.ListarTodosUnidadOrganizacional, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
