﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasCentroCosto
    {
        Task<List<CentroCostoModel>> ObtenerActivos();
    }
    public class ConsultasCentroCosto : IConsultasCentroCosto
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasCentroCosto(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<CentroCostoModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<CentroCostoModel>(ProcedimientosAlmacenados.CentroDeCostos.ListarTodosCentroDeCostos, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
