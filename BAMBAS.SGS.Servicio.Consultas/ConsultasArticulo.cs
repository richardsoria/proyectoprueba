﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasArticulo
    {
        Task<ArticuloModel> Obtener(int id);
        Task<List<ArticuloModel>> ObtenerAutocomplete(string term, bool flagKit);
        Task<List<ArticuloModel>> ObtenerAutocompleteTalla(string term);
        Task<List<ArticuloModel>> ObtenerAutocompleteRestricciones(string term);
        Task<List<ArticuloModel>> ObtenerActivas();

        Task<List<ArticuloModel>> ObtenerTallas();
        Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ArticuloActualizadosCustom>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters);
        Task<ArticuloModel> ObtenerUltimaActualizacion();
    
    }
    public class ConsultasArticulo : IConsultasArticulo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasArticulo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloModel>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas/*, param*/);
            empresas = empresas.Where(x => x.GDESTDO == "A").ToList();
            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => (x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()) || x.CSAP.Contains(parameters.SearchValue.ToUpper())))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<ArticuloModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ArticuloModel>(ProcedimientosAlmacenados.Articulo.Obtener, param);
            return ret.Entidad;
        }

        public async Task<List<ArticuloModel>> ObtenerAutocomplete(string term, bool flagKit)
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas, param);

            if (flagKit)
            {
                ret = ret.Where(x => x.GDESTDO == "A" && (x.DSCRPCN.Contains(term.ToUpper()) || x.CSAP.Contains(term.ToUpper()) || x.CBRRA.Contains(term.ToUpper()))).ToList();
            }
            else
            {
                ret = ret.Where(x => x.FKIT == flagKit && x.GDESTDO == "A" && (x.DSCRPCN.Contains(term.ToUpper()) || x.CSAP.Contains(term.ToUpper()) || x.CBRRA.Contains(term.ToUpper()))).ToList();
            }
           
            return ret;
        }
        public async Task<List<ArticuloModel>> ObtenerAutocompleteTalla(string term)
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas, param);

            ret = ret.Where(x => x.FMNJOTLLS == true && x.GDESTDO == "A" && (x.DSCRPCN.Contains(term.ToUpper()) || x.CSAP.Contains(term.ToUpper()) || x.CBRRA.Contains(term.ToUpper()))).ToList();

            return ret;
        }
        public async Task<List<ArticuloModel>> ObtenerAutocompleteRestricciones(string term)
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas, param);

            ret = ret.Where(x => x.FMNJORSTRCN == true && x.GDESTDO == "A" && (x.DSCRPCN.Contains(term.ToUpper()) || x.CSAP.Contains(term.ToUpper()) || x.CBRRA.Contains(term.ToUpper()))).ToList();

            return ret;
        }
        public async Task<List<ArticuloModel>> ObtenerActivas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }

        public async Task<DataTablesStructs.ReturnedData<ArticuloActualizadosCustom>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloActualizadosCustom>(ProcedimientosAlmacenados.Articulo.ObtenerUltimosActualizados/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<ArticuloModel> ObtenerUltimaActualizacion()
        {
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerUltimaActualizacion);
            return ret.Entidad;
        }
        public async Task<List<ArticuloModel>> ObtenerTallas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<ArticuloModel>(ProcedimientosAlmacenados.Articulo.ObtenerTodas, param);
            ret = ret.Where(x => x.FMNJOTLLS).ToList();
            return ret;
        }
    }
}

