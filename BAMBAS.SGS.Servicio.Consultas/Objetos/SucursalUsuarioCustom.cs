﻿using System;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class SucursalUsuarioCustom
    {
        public int ID { get; set; }
        public string CUSRO { get; set; }
        public string NYAPLLDS { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
    }
    public class UsuarioSucursalCustom
    {
        public int ID { get; set; }
        public string NSCRSL { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
    }
}
