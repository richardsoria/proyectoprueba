﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class ObjetoCustom :ObjetoPadreModel
    {
        public string OBJTOPDRE { get; set; }
        public bool TIENEHIJOS{ get; set; }
        public bool TIENEDETALLES { get; set; }
    }
}
