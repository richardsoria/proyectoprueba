﻿using System;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class PerfilUsuarioCustom
    {
        public int ID { get; set; }
        public string CUSRO { get; set; }
        public string NYAPLLDS { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
    }
}
