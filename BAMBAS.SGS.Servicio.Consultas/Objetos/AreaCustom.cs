﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class AreaCustom: AreaModel
    {
        public string GRNCA { get; set; }
        public string SPRINTNDNCA { get; set; }
    }
}
