﻿using System;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class PerfilObjetoCustom
    {
        public int ID { get; set; }
        public string IDPDRE { get; set; }
        public string NMBRE { get; set; }
        public string URL { get; set; }
        public int TIPO { get; set; }
        public int FORDN { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
    }
    public class PerfilObjetoDetalleCustom
    {
        public string IDOBJDET { get; set; }
        public bool ACCESO { get; set; }
    }
}
