﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class TrabajadorRestriccionCustom : TrabajadorRestriccionModel
    {
        public string CSAP { get; set; }
        public string DSCRPCNARTCLO { get; set; }
    }
}

