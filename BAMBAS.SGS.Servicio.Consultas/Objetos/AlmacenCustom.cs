﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class AlmacenCustom: AlmacenModel
    {
        public string TALMCN { get; set; }
        public string TRBJDR { get; set; }
    }
}
