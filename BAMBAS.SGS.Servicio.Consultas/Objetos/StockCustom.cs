﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class StockCustom : StockModel
    {
        public string DSCRPCNARTCLO { get; set; }
        public string CSAP { get; set; }
        public string UNDDMDDA { get; set; }
    }
}
