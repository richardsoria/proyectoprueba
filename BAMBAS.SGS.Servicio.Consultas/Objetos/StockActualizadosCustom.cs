﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class StockActualizadosCustom : StockModel
    {
        public decimal STCKANTRR { get; set; }
        public decimal STCKNVO { get; set; } 
        public string CSAP { get; set; }
        public string DSCRPCN { get; set; }
    }
}
