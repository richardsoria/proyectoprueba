﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class UbigeoCustom
    {
        public string CDGO { get; set; }
        public string DSCRPCN { get; set; }
    }
}
