﻿using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class UsuarioCustom : EntidadAuditoria
    {
        //Usuario
        
        public string IDPRSNA { get; set; }
        public string CUSRO { get; set; }
        public string NYAPLLDS { get; set; }      
        public string CLVE { get; set; }
        public string FVCLVE { get; set; }
        public string FBLQUO { get; set; }
        public string FCNTRTSTA { get; set; }
        public string FRZRCMBOCLVE { get; set; }

        //PErsona

        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }

        //Telefono Principal
        public string GDTTLFNO { get; set; }
        public string NTLFNO { get; set; }

        //Documento Principal
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }

        //Correo Principal
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }

        //Dirección Principal
        public string GDTDRCCN { get; set; }

        public string DRCCN { get; set; }
    }
}
