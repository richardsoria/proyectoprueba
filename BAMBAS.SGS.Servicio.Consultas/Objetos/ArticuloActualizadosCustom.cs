﻿using BAMBAS.SGS.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Servicio.Consultas.Objetos
{
    public class ArticuloActualizadosCustom :ArticuloModel
    {
        public decimal CSTOANTRR { get; set; }
        public decimal CSTONVO { get; set; }
    }
}
