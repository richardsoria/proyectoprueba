﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasRoster
    {
        Task<RosterModel> Obtener(int id);
        Task<List<RosterModel>> ObtenerActivos();
        Task<List<RosterModel>> ObtenerTodas();
        Task<DataTablesStructs.ReturnedData<RosterModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }
    public class ConsultasRoster : IConsultasRoster
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasRoster(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<DataTablesStructs.ReturnedData<RosterModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var empresas = await _configuracionConexionSql.EjecutarProcedimiento<RosterModel>(ProcedimientosAlmacenados.Roster.ObtenerTodas/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                empresas = empresas.Where(x => x.DSCRPCN.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return empresas.ConvertirTabla(parameters);
        }

        public async Task<RosterModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<RosterModel>(ProcedimientosAlmacenados.Roster.Obtener, param);
            return ret.Entidad;
        }
        public async Task<List<RosterModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<RosterModel>(ProcedimientosAlmacenados.Roster.ObtenerTodas, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
        public async Task<List<RosterModel>> ObtenerTodas()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<RosterModel>(ProcedimientosAlmacenados.Roster.ObtenerTodas, param);
            return ret;
        }
    }
}
