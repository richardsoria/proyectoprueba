﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using Dapper;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasPosicion
    {
        Task<List<PosicionModel>> ObtenerActivos();
    }
    public class ConsultasPosicion : IConsultasPosicion
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasPosicion(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<List<PosicionModel>> ObtenerActivos()
        {
            var param = new DynamicParameters();
            var ret = await _configuracionConexionSql.EjecutarProcedimiento<PosicionModel>(ProcedimientosAlmacenados.Posicion.ListarTodosPosiciones, param);
            ret = ret.Where(x => x.GDESTDO == "A").ToList();
            return ret;
        }
    }
}
