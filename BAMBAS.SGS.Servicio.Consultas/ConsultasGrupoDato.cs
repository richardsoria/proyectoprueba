﻿using BAMBAS.CORE.Extensions;
using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.SGS.Modelos;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Servicio.Consultas
{
    public interface IConsultasGrupoDato
    {
        Task<List<CatalogoGrupoDatoCustom>> ListarCatalogosGD();
        Task<GrupoDatoEmpresaModel> Obtener(int id);
        Task<List<GrupoDatoEmpresaModel>> ObtenerTodos();
        Task<List<GrupoDatoEmpresaModel>> ObtenerActivos();
        Task<DataTablesStructs.ReturnedData<GrupoDatoEmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
    }

    public class ConsultasGrupoDato : IConsultasGrupoDato
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasGrupoDato(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<List<GrupoDatoEmpresaModel>> ObtenerTodos()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<GrupoDatoEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoEmpresa.ObtenerTodos);
        }

        public async Task<List<GrupoDatoEmpresaModel>> ObtenerActivos()
        {
            return await _configuracionConexionSql.EjecutarProcedimiento<GrupoDatoEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoEmpresa.ObtenerActivos);
        }
        public async Task<GrupoDatoEmpresaModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<GrupoDatoEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoEmpresa.ObtenerGrupoDato, param);
            return ret.Entidad;
        }

        public async Task<DataTablesStructs.ReturnedData<GrupoDatoEmpresaModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var grupodato = await _configuracionConexionSql.EjecutarProcedimiento<GrupoDatoEmpresaModel>(ProcedimientosAlmacenados.GrupoDatoEmpresa.ObtenerTodos/*, param*/);

            if (!string.IsNullOrEmpty(parameters.SearchValue))
            {
                grupodato = grupodato.Where(x => x.AGDTO.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.DGDTO.Contains(parameters.SearchValue.ToUpper()) ||
                                    x.CGDTO.Contains(parameters.SearchValue.ToUpper()))
                    .ToList();
            }

            return grupodato.ConvertirTabla(parameters);
        }
        public async Task<List<CatalogoGrupoDatoCustom>> ListarCatalogosGD()
        {
            var parametros = new DynamicParameters();
            parametros.Add("@CGDTO", null);
            return await _configuracionConexionSql.EjecutarProcedimiento<CatalogoGrupoDatoCustom>(ProcedimientosAlmacenados.GrupoDatoDetalleEmpresa.ListarGrupoDatosCatalogos, parametros);
        }
    }
}
