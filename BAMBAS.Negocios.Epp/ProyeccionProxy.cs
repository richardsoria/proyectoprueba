﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.EPP.Proyeccion;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.EPP
{
    public interface IProyeccionProxy
    {
        Task<DataTablesStructs.ReturnedData<ProyeccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string dscrpcn, string anio);

        //Task<DataTablesStructs.ReturnedData<ProyeccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<ProyeccionDto> Obtener(int id);
        Task<RespuestaConsulta> Insertar(ProyeccionDto kit);
        Task<RespuestaConsulta> Eliminar(ProyeccionDto command);
        Task<RespuestaConsulta> EliminarDetalle(ProyeccionDto command);
        Task<RespuestaConsulta> Actualizar(ProyeccionDto proyeccion);
        Task<RespuestaConsulta> ActualizarAprobado(ProyeccionDto proyeccion);

    }
    public class ProyeccionProxy : IProyeccionProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public ProyeccionProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<ProyeccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string dscrpcn, string anio)
        {
            var url = $"{_apiUrls.EPPUrl}proyeccion/obtener-tabla?dscrpcn={dscrpcn}&anio={anio}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ProyeccionDto>(-3);
        }
        public async Task<RespuestaConsulta> Insertar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.EPPUrl}proyeccion/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<ProyeccionDto> Obtener(int id)
        {
            var url = $"{_apiUrls.EPPUrl}proyeccion/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ProyeccionDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Actualizar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.EPPUrl}proyeccion/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> ActualizarAprobado(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.EPPUrl}proyeccion/actualizar-aprobado", content);
            return request.Respuesta(-2, nameof(ActualizarAprobado));
        }
        public async Task<RespuestaConsulta> Eliminar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.EPPUrl}proyeccion/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<RespuestaConsulta> EliminarDetalle(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.EPPUrl}proyeccion/eliminar-detalle", content);
            return request.Respuesta(-2, nameof(EliminarDetalle));
        }

    }
}
