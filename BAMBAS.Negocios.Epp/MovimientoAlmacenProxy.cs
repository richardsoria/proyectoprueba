﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.EPP.MovimientoAlmacen;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.EPP
{
    public interface IMovimientoAlmacenProxy
    {
        Task<DataTablesStructs.ReturnedData<MovimientoAlmacenDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtalmcn);
        Task<List<MovimientoAlmacenDto>> ObtenerTabla();
    }
    public class MovimientoAlmacenProxy : IMovimientoAlmacenProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public MovimientoAlmacenProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<MovimientoAlmacenDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtalmcn)
        {
            var url = $"{_apiUrls.SeguridadUrl}movimientoalmacen/obtener-tabla?gdtalmcn={gdtalmcn}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<MovimientoAlmacenDto>(-2);
        }
        public async Task<List<MovimientoAlmacenDto>> ObtenerTabla()
        {
            var url = $"{_apiUrls.SeguridadUrl}movimientoalmacen/obtener-tabla";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<MovimientoAlmacenDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
