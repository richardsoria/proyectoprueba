﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.TRO.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Servicio.Consultas
{
    public interface IConsultasTareo
    {
        Task<DatosTareoCustom> ObtenerDatosTareo(int id);
        Task<DatosTareoCustom> ObtenerUltimoRegistro();
        Task<List<ListarTareoCustom>> ListarTareo(int id);
        Task<DataTablesStructs.ReturnedData<ListarTareoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int idgrdia, string finic, string ffin);
    }
    public class ConsultasTareo : IConsultasTareo
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;


        public ConsultasTareo(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;    
        }

        public async Task<DatosTareoCustom> ObtenerDatosTareo(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var res = await _configuracionConexionSql.ObtenerPrimerRegistro<DatosTareoCustom>(ProcedimientosAlmacenados.Tareo.ObtenerDatosTareo, param);
            return res.Entidad;
        }
        public async Task<DatosTareoCustom> ObtenerUltimoRegistro()
        {
            var param = new DynamicParameters();
            var res = await _configuracionConexionSql.ObtenerPrimerRegistro<DatosTareoCustom>(ProcedimientosAlmacenados.Tareo.ObtenerUltimoTareo, param);
            return res.Entidad;
        }
        public async Task<List<ListarTareoCustom>> ListarTareo(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<ListarTareoCustom>(ProcedimientosAlmacenados.Tareo.ListarDatosTareo, param);
        }

        public async Task<DataTablesStructs.ReturnedData<ListarTareoCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDGRNCIA", idgrnc);
            parametros.Add("@IDSPRINTNDCIA", idsprntndnc);
            parametros.Add("@IDAREA", idarea);
            parametros.Add("@IDCNTRTSTA", idcntrtsta);
            parametros.Add("@IDRSTER", idrster);
            parametros.Add("@IDGRDIA", numgrdia);
            parametros.Add("@FINCTRO", finic);
            parametros.Add("@FFINTRO", ffin);
            var lista = await _configuracionConexionSql.EjecutarProcedimiento<ListarTareoCustom>(ProcedimientosAlmacenados.Tareo.ListarDatosTareo, parametros);
            return lista.ConvertirTabla(parameters);
        }
    } 
}
