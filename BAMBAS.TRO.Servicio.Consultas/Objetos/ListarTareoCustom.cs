﻿using BAMBAS.TRO.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.TRO.Servicio.Consultas.Objetos
{
    public class ListarTareoCustom: TareoModel
    {
        public string GRNCA { get; set; }
        public string SPRINTNDNCA { get; set; }
        public string ARA { get; set; }
        public string GRDA { get; set; }
        public string GRDIA { get; set; }
        public string TRNO { get; set; }
        public string RSTR { get; set; }
    }
}
