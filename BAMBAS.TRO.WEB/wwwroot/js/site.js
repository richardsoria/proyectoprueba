jQuery.extend(jQuery.validator.messages, {
    required: "Campo requerido.",
    maxlength: jQuery.validator.format("Por favor, no ingresar más de {0} caracteres."),
    max: jQuery.validator.format("Por favor, ingresar un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, ingresar un valor mayor o igual a {0}."),
    equalTo: jQuery.validator.format("Por favor, escriba lo mismo."),
    email: jQuery.validator.format("Debes ingresar un correo electrónico válido."),
    digits: "Por favor solo ingresar dígitos",
    number: "Por favor ingresar un número válido.",
    minlength: "Por favor ingresar al menos {0} carácteres.",
});

function mayorFechaActual(fecAdd, factual) {
    var valFecha = false;
    var fechaCon = fecAdd;//fecha Consultada   
    var fCon = fechaCon.split('/');
    var fConAnnio = fCon[2];
    var fConMes = fCon[1];
    var fConDia = fCon[0];


    var annioActual;
    var mesActual;
    var diaActual;
    if (factual) {
        var actualSplit = factual.split("/");
        annioActual = actualSplit[2];
        mesActual = actualSplit[1];
        diaActual = actualSplit[0];
    } else {
        factual = new Date();//fecha Hoy
        annioActual = parseInt(factual.getFullYear());
        mesActual = parseInt(factual.getMonth()) + 1;
        diaActual = parseInt(factual.getDate());
    }

    var annioServ = annioActual - fConAnnio;
    var mesServ = mesActual - fConMes;

    if (mesServ < 0 || (mesServ === 0 && diaActual < fConDia)) {
        annioServ--;
    }
    if (annioServ < 0) {
        valFecha = true;
    }
    return valFecha;
}

$.validator.addMethod("alfanumerico", function (value, element) {
    return (/^[0-9a-zA-Z\sáéíóúñÑÁÉÍÓÚüÜ ]+$/).test(value);

}, "Por favor ingresar caracteres alfanumericos.");

$.validator.addMethod("longitudexacta", function (value, element, param) {
    return this.optional(element) || value.length == param;
}, $.validator.format("Por favor ingresar {0} caracteres."));

$.validator.addMethod("sololetras", function (value, element) {
    return (/^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ]+$/).test(value);
    // --                                    or leave a space here ^^
}, "Por favor ingresar solo letras.");

$.validator.addMethod("solonumeros", function (value, element) {
    return (/^[0-9]+$/).test(value);
}, "Por favor ingresar solo números.");

$.extend($.fn.dataTable.defaults, {
    searching: true,
    dom: "<'top'if>rt<'bottom'pl><'clear'>",
    language: {
        "sProcessing": "<div class='m-blockui' style='display: inline; background: none; box-shadow: none;'><span>Cargando datos...</span><span><div class='m-loader  m-loader--brand m-loader--lg'></div></span></div>",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando _START_ - _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 - 0 de 0 registros",
        "sInfoFiltered": "(filtrado de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "pagingType": "simple_numbers",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "<i class='la la-angle-double-left'></i>",
            "sLast": "<i class='la la-angle-double-right'></i>",
            "sNext": "<i class='la la-angle-right'></i>",
            "sPrevious": "<i class='la la-angle-left'></i>"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    lengthMenu: [10, 25, 50],
    lengthChange: true,
    orderMulti: false,
    pagingType: "full_numbers",
    processing: true,
    responsive: true,
    serverSide: true,
    info: true,
    order: [],
    filter: true,
    pageLength: 10,
    paging: true,
    fixedColumns: true,
    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull, a, a) {
        // Bold the grade for all 'A' grade browsers
        var row = nRow._DT_RowIndex;

        var elementoTabla = $(this);
        var tabla = $("#" + elementoTabla[0].id).DataTable();
        var table = tabla;
        var info = table.page.info();
        nRow.cells[0].innerHTML = (info.page * info.length) + (row + 1);
    },
    columnDefs: [
        {
            targets: "_all",
            className: 'text-center'
        }
    ]
});



//VALIDACIONES INPUTS
$(".alfaNumCharMail").keypress(function (e) {//LETRAS NUMEROS CARACTERES ESPECIFICOS
    var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9\-_@.]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
});
function FuncionAlfanumerico(e) {
    var keyCode = e.keyCode || e.which;
    var regex = /^[0-9a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
}

$(".alfanumerico").keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
    return FuncionAlfanumerico(e);
});

$(".alfaNumCharEsp").keypress(function (e) {//LETRAS NUMEROS CARACTERES ESPECIFICOS Y ESPACIO
    var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9\-_()., \sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
});

$(".alfaNumCharEsp1").keypress(function (e) {//LETRAS NUMEROS CARACTERES ESPECIFICOS Y ESPACIO
    var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9\- ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    return isValid;
});

$('.text-uppercase').on("change", function () {
    $(this).val($(this).val().trim());
});

$(".text-uppercase").blur("keypress", function () {
    $input = $(this);
    $input.val($input.val().toUpperCase());
});


$(".sololetras").bind('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z\sáéíóúñÑÁÉÍÓÚüÜ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$(".sololetrasyespacios").bind('keypress', function (event) {//LETRAS Y ESPACIOS
    var regex = new RegExp("^[a-zA-Z\sáéíóúñÑÁÉÍÓÚäëïöüÄËÏÖÜ ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
function FuncionSoloNumeros(event) {
    var regex = new RegExp("^[0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    return true;
}
function ObtenerNumeroDiaSemana(fecha) {
    var finic = fecha.split('/');
    var finic2 = `${finic[2]}/${finic[1]}/${finic[0]}` 
    var numDia = new Date(finic2).getDay(); 
    return numDia;
}
$(".solonumeros").bind('keypress', function (event) {//NUMEROS
   return  FuncionSoloNumeros(event)
});

$(".text-uppercase").bind('blur', function (event) {//MAYUSCULAS
    if (!$(this).val()) {
        return false;
    } else {
        $(this).val($(this).val().toUpperCase());
    }
});

$(".numeroCaracteres").on('keyup', function () {//CANTIDAD DE CARACTERES USADOS
    var limit = 1000;
    $("#txtDireccionEmp").attr('maxlength', limit);
    var init = $(this).val().length;

    if (init < limit) {
        init++;
        $('.caracteres').text(init + "/1000");
    }
});

$(":input").attr("autocomplete", "off");

jQuery.fn.extend({
    agregarLoading: function () {
        this.html(
            `<div class="loaderbody">
                <div class="loader-div">
                    <div class="loader-wheel"></div>
                    <div class="loader-text"></div>
                </div>
            </div>`
        );
    },

    addLoader: function () {
        this.attr("disabled", true);
        return this.append('<span id="btn-loader" class="spinner-border spinner-border-sm"></span>');
    },
    removeLoader: function () {
        this.attr("disabled", false);
        return this.find("#btn-loader").remove();
    },
    AgregarCamposDefectoAuditoria: function () {
        var $this = this;
        $.ajax({
            url: `/obtener-datos-usuario-logueado`,
            type: "Get"
        })
            .done(function (result) {
                $this.find("[name='ID']").val("");
                $this.find("[name='UEDCN']").val(result.uedcn);
                $this.find("[name='UCRCN']").val(result.ucrcn);
                $this.find("[name='GDESTDO']").val("A");
                $this.find("[name='cFCRCN']").val(result.cFCRCN);
                $this.find("[name='cFEDCN']").val(result.cFEDCN);
                $this.find("[name='cFESTDO']").val(result.cFESTDO);
            });
    },
    AgregarCamposAuditoria: function (result) {
        this.find("[name='ID']").attr("disabled", false);
        this.find("[name='ID']").val(result.id);
        this.find("[name='ID-d']").val(result.id);
        this.find("[name='UEDCN']").val(result.uedcn);
        this.find("[name='UCRCN']").val(result.ucrcn);
        this.find("[name='GDESTDO']").val(result.gdestdo);
        this.find("[name='cFCRCN']").val(result.cFCRCN);
        this.find("[name='cFEDCN']").val(result.cFEDCN);
        this.find("[name='cFESTDO']").val(result.cFESTDO);
    },
    DeshabilitarCamposAuditoria: function () {
        this.find("[name='ID']").attr("disabled", false);
        this.find("[name='ID-d']").attr("disabled", true);
        this.find("[name='UEDCN']").attr("disabled", true);
        this.find("[name='UCRCN']").attr("disabled", true);
        this.find("[name='cFCRCN']").attr("disabled", true);
        this.find("[name='cFEDCN']").attr("disabled", true);
        this.find("[name='cFESTDO']").attr("disabled", true);
    },
    LlenarSelectEstados: function () {
        var $this = this;
        $this.LlenarSelectGD("GDESTDO");
    },
    LlenarSelectTipoDocumento: function () {
        var $this = this;
        $this.LlenarSelectGD("GDDCMNTO");
    },
    LlenarSelectTipoPersona: function () {
        var $this = this;
        $this.LlenarSelectGD("GDTPRSNA");
    },
    LlenarSelectTipoTelefono: function () {
        var $this = this;
        $this.LlenarSelectGD("GDTTLFNO");
    },

    LlenarSelectInicioGuardia: function () {
        var $this = this;
        $this.LlenarSelectGD("GDTGRDA");
    },
    LlenarSelectGD: function (cGrupoDto, value, text) {
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
            return n.cgdto === cGrupoDto;
        });
        $this.append($("<option />").val('').text("Seleccione"));
        $.each(datos, function (key, item) {
            $this.append($("<option />").val(item[value]).text(item[text]));
        });
    },
    LlenarSelectGrupoDato: function (cGrupoDto, value, text) {
        var $this = this;
        value = value || "vlR1";
        text = text || "agddtlle";
        $.get("/obtenerGrupoDatos", { cgdto: cGrupoDto })
            .done(function (data) {
                $this.append($("<option />").val('').text("Seleccione"));
                $.each(data, function (key, item) {
                    $this.append($("<option />").val(item[value]).text(item[text]));
                });
            });
    },
    maxlengthDocumento: function (options) {
        this.on("change", function () {
            let config = options;
            let objAfectado = document.getElementById(config.obj);
            objAfectado.value = '';
            var vlor = this.value;
            var datos = $(JSON.parse(localStorage.getItem('datosCatalogoGD'))).filter(function (i, n) {
                return n.cgdto === "GDDCMNTO" && n.vlR1 == vlor;
            });
            if (datos[0]) {
                objAfectado.minLength = 0;
                objAfectado.maxLength = 0;
                objAfectado.maxLength = datos[0].vlR2;
                objAfectado.minLength = datos[0].vlR2;
            }

            if (vlor == 3) {//SI es pasaporte permitir letras
                objAfectado.classList.add("alfanumerico");
                objAfectado.classList.remove("solonumeros");
                $("#" + config.obj).unbind('keypress');
                $("#" + config.obj).keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
                    return FuncionAlfanumerico(e);
                });
            } else {
                objAfectado.classList.add("solonumeros");
                objAfectado.classList.remove("alfanumerico");
                $("#" + config.obj).unbind('keypress');
                $("#" + config.obj).keypress(function (e) {//LETRAS NUMEROS Y ESPACIO
                    return FuncionSoloNumeros(e);
                });
            }
        });
    }
});



function AgregarRastro(nombre, clase) {
    $(".breadcrumbs li:last-child").addClass(clase)
    $(".breadcrumbs").append(`<li class="">
            <a class="">
                <span class="">${nombre}</span>
            </a>
        </li>`);
};
function QuitarRastro(clase) {
    $(".breadcrumbs li." + clase + " ~ li").remove();
};


function SeleccionarOpcionMenu() {
    var url = window.location.pathname;
    var $opcHijoMenu = $(".opcion-hijo-menu");
    $.each($opcHijoMenu, function (index, value) {
        var $this = $(this);
        var href = $this.attr("href");
        if (href == url) {
            $this.parent().addClass("active");
            $this.parent().parent().parent().addClass("active");
        }
    });
};
SeleccionarOpcionMenu();


$('.collapsed').on('click', function () {
    $this = $(this);

    if ($this.hasClass("mostrarA")) {
        this.nextElementSibling.classList.remove('show');
        this.classList.remove('mostrarA')
    } else {
        this.nextElementSibling.classList.add('show');
        this.classList.add('mostrarA')
    }
});

function ObtenerEstado(data) {
    var tpm;
    if (data.gdestdo == "A") {
        tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
    }
    else {
        tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
    }
    return tpm;
}


$.ajaxSetup({
    beforeSend: function (jqXHR, settings) {
        var url = settings.url;
        var carpetaPublicacion = "/tro-web" ;
        if (!url.includes(carpetaPublicacion)) {
            settings.url = carpetaPublicacion + url;
        }
    },
});

//REDIRECCIONES 
function RedirectWithSubfolder(url) {
    var carpetaPublicacion = "/tro-web";
    if (!url.includes(carpetaPublicacion)) {
        window.location.href = carpetaPublicacion + url;
    } else {
        window.location.replace(url)
    }
}


$(window).keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});

var cargarCatalogoGD = () => {
    $.get("/tro-web/obtenercatalogoGD")
        .done(function (data) {
            var datos = JSON.stringify(data);
            localStorage.setItem("datosCatalogoGD", datos);
        });
}

cargarCatalogoGD();
//