﻿var InicializadorDatosTareo = function () {
    /**
     * VARIABLES CREACION NUEVO TAREO
     * */
    var $selectEstados = $(".select-estados");
    var $formTareo = $("#FormularioTareo");
    var $selGrnca = $("#IDGRNCA");
    var $selsprintd = $("#IDSPRINTNDNCA");
    var $selarea = $("#IDARA");
    var $cbogrda = $("#CBOGRDA");
    var $cborstr = $("#CBORSTR");
    var $cboturno = $("#CBOTURNO");
    var $ciclos = $("#CICLOS");
    var $fecini = $("#TXTFINI");
    var $fecfin = $("#TXTFFIN");
    var $preview = $(".vista-previa");
    var $btnAgregTareo = $(".gnrrTro");
    let xyz = '';
    var flag = true;

    /*VARIABLES ASIGNAR PERSONAL*/
    var $modalasignarpersonal = $("#modal_asignar_personal");
    var $txtCSAP = $("#txtCSAP");
    var $txtNombRazon = $("#txtNombRazon");
    var $cbo_empresa = $("#cbo_empresa");
    var $cbocntrtst = $("#cbocntrtst");
    var $chkBmbs = $("#Bmbs");
    var $chkBambas = $chkBmbs.find("input[type=checkbox]");
    var $asignarPersonal = $(".asignar-personal");
    var $btnBuscarPersonal = $("#btnBuscarPersonal");
    var $tablaPersonal = $("#tabla_Personal");
    var $btnAddPersonal = $("#btnAgregarPersonal");
    var $tblPrsnlAggdo = $("#tbl_prsnl_aggdo");

    var $IdTareo = $("#IdTareo");
    /*VARIABLES FORMULARIO ASISTENCIA TAREO*/
    var $formAsitTareo = $("#FormularioAsistenciaTareo");

    var tablaPersonal = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Tareo/listarPersonal",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.csap = $txtCSAP.val();
                    data.datos = $txtNombRazon.val();
                    data.idcntrtsta = $cbo_empresa.val();
                    $chkBambas.is(':checked') == true ? data.bambas = 1 : data.bambas = 0;
                },
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    data: "csap",
                    orderable: false,
                    className: 'text-center'
                },
                {
                    title: "Nombres y Apellidos / Razon Social",
                    data: "datos",
                    orderable: false,
                    className: 'text-center'
                },
                {
                    title: "Contratista",
                    data: "cntrtsta",
                    orderable: false,
                    className: 'text-center'
                },
                {
                    title: "Seleccionar",
                    className: 'text-center',
                    data: null,
                    orderable: false,
                    render: function (data) {
                        var tmp = "";
                        var idreg = tablaPersonal.record_set.find(x => x.CSAP == data.csap);
                        if (idreg) {
                            tmp += `   <input data-idprsna="${data.idprsna}" data-csap="${data.csap}" data-datos="${data.datos}" data-cntrtsta="${data.cntrtsta}" data-idprsnl="${data.id}" data-crgo="${data.crgo}" class="form-control form-control-sm numprsna" type="checkbox" value="${idreg.NUMPRSNA}">`;
                        } else {
                            tmp += `   <input data-idprsna="${data.idprsna}" data-csap="${data.csap}" data-datos="${data.datos}" data-cntrtsta="${data.cntrtsta}" data-idprsnl="${data.id}" data-crgo="${data.crgo}" class="form-control form-control-sm numprsna" type="checkbox">`;
                        }
                        return tmp;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        record_set: [],
        eventos: function () {
            tablaPersonal.objeto.on("click", ".numprsna", function () {
                var chkPrsnal = $(this).val();
                var csap = $(this).data("csap");
                var datos = $(this).data("datos");
                var idprsna = $(this).data("idprsna");
                var cntrtsta = $(this).data("cntrtsta");
                var crgo = $(this).data("crgo");

                var del_Personal = false;
                if (chkPrsnal == "on") {
                    del_Personal = true;
                }

                var new_Personal = true;
                for (let i = 0; i < tablaPersonal.record_set.length; i++) {
                    if (tablaPersonal.record_set[i].CSAP === csap) {
                        new_Personal = true;
                        if (del_Personal) {
                            tablaPersonal.record_set.splice(i, 1);
                        } else {
                            tablaPersonal.record_set[i].NUMPRSNA = idprsna;
                        }
                    }
                }
                if (new_Personal) {
                    tablaPersonal.record_set.push({
                        CSAP: csap,
                        DATOS: datos,
                        CNTRTSTA: cntrtsta,
                        NUMPRSNA: idprsna,
                        CRGO: crgo
                    });
                }
                console.log(tablaPersonal.record_set);
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            if (tablaPersonal.objeto == null || tablaPersonal.objeto == "" || tablaPersonal.objeto == undefined) {
                tablaPersonal.objeto = $tablaPersonal.DataTable(tablaPersonal.opciones);
                tablaPersonal.eventos();
            } else {
                tablaPersonal.reload();
            }
        }
    }

    var modalPersonalTareo = {
        eventos: {
            onHide: function () {
                $modalasignarpersonal.on('hidden.bs.modal', function () {
                    tablaPersonal.record_set = [];
                    modalPersonalTareo.eventos.reset();
                })
            },
            onShow: function () {
                $modalasignarpersonal.on('shown.bs.modal', function () {
                    tablaPersonal.record_set = [];
                })
            },
            reset: function () {
                tablaPersonal.record_set = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        gerencia: function (idGerencia, idSuperintendencia, idArea) {
            $selsprintd.attr("disabled", true);
            $selarea.attr("disabled", true);
            $selGrnca.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-gerencia").done(function (data) {
                $.each(data, function (key, item) {
                    $selGrnca.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                });
                $selGrnca.children('option[value=""]').text("Seleccione");
                if (idGerencia) {
                    $selGrnca.val(idGerencia)
                    selects.$selsprintd(idGerencia, idSuperintendencia, idArea)
                }
            });
            $selGrnca.on("change", function () {
                selects.superintendencia();
                selects.area();
            });
        },
        superintendencia: function (idGerencia, idSuperintendencia, idArea) {
            $selsprintd.empty();
            $selsprintd.append($("<option />").val('').text("Cargando..."));
            if ($selGrnca.val()) {
                $.get("/obtener-superintendencia", { idGerencia: (idGerencia ? idGerencia : $selGrnca.val()) }).done(function (data) {
                    $.each(data, function (key, item) {
                        $selsprintd.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selsprintd.attr("disabled", false);
                    if (idSuperintendencia) {
                        $selsprintd.val(idSuperintendencia);
                        selects.area(idSuperintendencia, idArea);
                    }
                });
            } else {
                $selsprintd.val("").change();
            }
            $selsprintd.children('option[value=""]').text("Seleccione");
            $selsprintd.unbind('change');
            $selsprintd.on("change", function () {
                selects.area();
            });
        },
        area: function (idSuperintendencia, idArea) {
            $selarea.empty();
            $selarea.append($("<option />").val('').text("Cargando..."));
            if ($selsprintd.val()) {
                $.get("/obtener-areas", { idSuperintendencia: (idSuperintendencia ? idSuperintendencia : $selsprintd.val()) }).done(function (data) {
                    $.each(data, function (key, item) {
                        $selarea.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selarea.attr("disabled", false);
                    if (idArea) {
                        $selarea.val(idArea)
                    }
                });
            } else {
                $selarea.val("");
            }
            $selarea.children('option[value=""]').text("Seleccione");
        },
        contratista: function (idcntrtsta) {
            $cbo_empresa.append($("<option />").val('').text("Cargando..."));
            $.get("/obtenerContratista").done(function (data) {
                $.each(data, function (key, item) {
                    $cbo_empresa.append($("<option />").val(item["idcntrtsta"]).text(item["cntrtsta"]));
                });
                $cbo_empresa.children('option[value=""]').text("Seleccione");
                if (idcntrtsta) {
                    $cbo_empresa.val(idContratista);
                }
            })
        },
        guardia: function (idGuardia) {
            $cbogrda.append($("<option/>").val('').text("Cargando..."));
            $.get("/obtener-guardias").done(function (data) {
                $.each(data, function (key, item) {
                    $cbogrda.append($("<option/>").val(item["id"]).text(item["dscrpcn"]));
                });
                $cbogrda.children('option[value=""]').text("Seleccione");
                if (idGuardia) {
                    $cbogrda.val(idGuardia);
                }
            });
        },
        ciclo: function (idciclo) {
            $ciclos.append($("<option/>").val('').text("Seleccione"));
            for (var i = 1; i < 21; i++) {
                $ciclos.append($("<option/>").val(i).text(i));
            }
            $cbogrda.children('option[value=""]').text("Seleccione");
            if (idciclo) {
                $cbogrda.val(idciclo);
            }
        },
        init: function () { 
            selects.gerencia();
            selects.contratista();
            selects.guardia();
            $selectEstados.LlenarSelectEstados();
            selects.ciclo();
        }
    };

    var formModalPrsnal = {
        init: function () {
            $chkBambas.prop('checked', true);
            $cbocntrtst.hide();
        }
    }

    var eventosIncrustados = {
        ocultarSelectCtrtst: function () {
            $chkBambas.on("click", function () {
                //$chkBambas.is(':checked') == true ?  : $cbocntrtst.show();
                if ($chkBambas.is(':checked')) {
                    $cbocntrtst.hide();
                } else {
                    $cbocntrtst.show();
                    $cbo_empresa.val("");
                }
                $cbo_empresa.children('option[value=""]').text("Seleccione");
            });
        },
        BuscarPersonal: function () {
            $asignarPersonal.on("click", function () {
                tablaPersonal.record_set = [];
                tablaPersonal.inicializador();
            })
        },
        ObtenerAttrGuardia: function () {
            $cbogrda.on("change", function () {
                var parametro = { id: $cbogrda.val() };
                $.get("/obtener-atrbt-guardias", parametro).done(function (data) {
                    $cborstr.val(data.rstr); 
                    $cboturno.val(data.trno);
                });
            })
        },
        ObtenerFechaFinal: function () {
            $ciclos.on("change", function () {
                if ($cbogrda.val()) {
                    $formTareo.find(".msgccl").text("");
                    if ($fecini.val()) {
                        $formTareo.find(".msgccl").text("");
                        var parametro = { id: $cbogrda.val() }
                        $.get("/obtener-atrbt-guardias", parametro).done(function (data) {
                            var diasT = data.dtrbjds; 
                            var diasD = data.ddscnso;
                            var ttlcclo = (diasT + diasD) * $ciclos.val();
                            var fechaFin = eventosIncrustados.sumarFecha($fecini.val(), ttlcclo);
                            $fecfin.val(fechaFin);

                            //no deberia estar haciendo esto pero espero cambiarlo (WB)
                            $("#DIASDESCANSO").val(diasD);
                            $("#DIASTRABAJADOS").val(diasT);
                            $("#TOTALDIAS").val(ttlcclo);
                        })
                    } else {
                        if ($ciclos.val() == '') {
                            $formTareo.find(".msgccl").text("");
                        } else {
                            $formTareo.find(".msgccl").text("Seleccione una fecha de inicio.");
                        }
                    }
                } else {
                    if ($ciclos.val() == '') {
                        $formTareo.find(".msgccl").text("");
                    } else {
                        $formTareo.find(".msgccl").text("Seleccione una Guardia.");
                    }
                }
            });    
        },
        FiltroPersonal: function () {
            $btnBuscarPersonal.on("click", function () {
                tablaPersonal.reload();
            });
        },
        btnAddPersonal: function () {
            $btnAddPersonal.on("click", function () {
                for (var i = 0; i < tablaPersonal.record_set.length; i++) {
                    let prsnalontbl = false;
                    $("#tbl_prsnl_aggdo tbody tr").each(function (index) {
                        var csaptbl = $(this).children('td').find("[class='csap']").text();
                        if (tablaPersonal.record_set[i].CSAP == csaptbl.trim()) {
                            prsnalontbl = true;
                            $(this).children('td').find("[class='numprsna']").text(tablaPersonal.record_set[i].NUMPRSNA);
                        }
                    })
                    if (!prsnalontbl) {
                        var trs = i + 1;
                        var htmlTags = `<tr>                                  
                            <td class="text-center headcol"><label>${trs}</label></td>
                            <td class="text-center"><label class="csap">${tablaPersonal.record_set[i].CSAP}</label></td> 
                            <td class="text-center"><label>${tablaPersonal.record_set[i].DATOS}</label></td>  
                            <td class="text-center"><label>${tablaPersonal.record_set[i].CNTRTSTA}</label></td>
                            <td class="text-center"><label>${tablaPersonal.record_set[i].CRGO}</label></td>           
                        </tr>`
                        $('#tbl_prsnl_aggdo').append(htmlTags);  
                    }
                    xyz = $cbocntrtst.val();
                }
                tablaPersonal.record_set = [];
                $modalasignarpersonal.modal('hide');
            });
        },
        VistaPrevia: function () {
            $preview.on("click", function () {
                //verificar que los campos no esten vacios
                if ($formTareo.valid()) {
                    let cantreg = $tblPrsnlAggdo.find('tbody tr').length;  //esto es para saber que hay registros
                    var ncol = $(".col-days").length;
                    if (cantreg) {
                        //OBTENER EL NUMERO DE DIAS DEL RANGO DE FECHA Y MULTIPLICARLO AL CICLO 
                        var parametro = { id: $cbogrda.val() }
                        $.get("/obtener-atrbt-guardias", parametro).done(function (data) {
                            var diasT = data.dtrbjds;
                            var diasD = data.ddscnso;
                            var sumDias = diasT + diasD
                            var totaldias = (sumDias) * $ciclos.val();
                            //ENCABEZADO
                            if (ncol > 0) {
                                $(".col-days").remove();
                                $(".col-dia").remove();
                                $(".col-nch").remove();
                                $(".col-des").remove();
                                $(".del").remove();
                            }
                           
                            $('#tbl_prsnl_aggdo').find('thead tr').each(function () {
                                var col = 4; 
                                for (var i = 0; i < totaldias; i++) {
                                    var thFec = eventosIncrustados.sumarFecha($fecini.val(), i + 1);
                                    var clmna = thFec.split('/');
                                    var dm = `${clmna[0]}/${clmna[1]}`
                                    $(this).find('th').eq(col).after(`<th class='col-days' style='text-align:center; padding-right:5px;'>${dm}</th>`);
                                    col = col + 1
                                }
                                $(this).find('th').eq(col).after(`<th class='col-days headcol2' style='text-align:center; padding-right:5px;'>Eliminar</th>`);
                            })
                            //CUERPO
                            $('#tbl_prsnl_aggdo').find('tbody tr').each(function () {
                                var estiloD = `style='text-align:center; background-color: #d39e00; color:white;'`
                                var estiloN = `style='text-align:center; background-color: #1c1f2a; color:white;'`
                                var estiloDES = `style='text-align:center; background-color: #C6C6C6;'`    
                                var col = 4;
                                var col2 = 0;
                                //CANTIDAD DE CICLOS
                                for (var i = 0; i < $ciclos.val(); i++) {
                                    //DEBO VER LOS TURNOS
                                    if (data.gdtrno == 3) {   //DIA-NOCHE
                                        //VERIFICO SI LOS DIAS DE TRABAJO SON PAR O IMPAR
                                        if (diasT % 2 == 0) {
                                            var dasT = (diasT / 2) // diasT
                                            var nchT = diasT - dasT // noches 
                                        } else {
                                            var dasT = (diasT / 2) + 0.5 // diasT
                                            var nchT = diasT - dasT // noches 
                                        }
                                        //EN ESTE PRIMER CICLO AGREGO LAS COLUMNAS DIA TRABAJADO
                                        for (var j = 0; j < dasT; j++) {
                                            $(this).find('td').eq(col).after(`<td class='col-dia' ${estiloD}>D</td>`)
                                            col = col + 1
                                            col2 = col + 1
                                        }
                                        for (var k = 0; k < nchT; k++) {
                                            $(this).find('td').eq(col).after(`<td class='col-nch' ${estiloN}>N</td>`)
                                            col = col + 1;
                                        }
                                        for (var m = 0; m < diasD; m++) {
                                            $(this).find('td').eq(col).after(`<td class='col-des' ${estiloDES}>DL</td>`)
                                            col = col + 1;
                                        }
                                    } else if (data.gdtrno == 2) {  //NOCHE
                                        for (var k = 0; k < diasT; k++) {
                                            $(this).find('td').eq(col).after(`<td class='col-nch' ${estiloN}>N</td>`)
                                            col = col + 1;
                                        }
                                        for (var m = 0; m < diasD; m++) {
                                            $(this).find('td').eq(col).after(`<td class='col-des' ${estiloDES}>DL</td>`)
                                            col = col + 1;
                                        }
                                    } else {//DIA
                                        for (var k = 0; k < diasT; k++) {
                                            $(this).find('td').eq(col).after(`<td class='col-dia' ${estiloD}>D</td>`)
                                            col = col + 1;
                                        }
                                        for (var m = 0; m < diasD; m++) {
                                            $(this).find('td').eq(col).after(`<td class='col-des' ${estiloDES}>DL</td>`)
                                            col = col + 1;
                                        }
                                    } 
                                } 
                                $(this).find('td').eq(col).after(
                                    `<td class='text-center del headcol2'>
                                        <button id='delete_0' title='Eliminar Asignación' class='btn btn-danger btn-xs borrarFilaAsignado'>
                                            <span class='la la-trash'></span>
                                        </button>
                                    </td>`
                                );
                                flag = true;   
                            })
                        });
                        $btnAgregTareo.show();
                        $formTareo.find(".msg5").text("");
                    } else {
                        if (cantreg == 0) $formTareo.find(".msg5").text("Debe asignar un personal al Tareo.");
                    }
                } else {
                    $formTareo.reset()
                }                
            });
        },
        EliminarRegistro: function () {
            $(document).on('click', '.borrarFilaAsignado', function (event) {
                event.preventDefault();
                //IDENTIFICAMOS SI HAY DATOS EN LA FILA
                let cantreg = $tblPrsnlAggdo.find('tbody tr').length;//TIENE REGISTROS
                var fila = $(this).closest('tr');
                var filaEncbz = $(".col-days");
                if (cantreg) {
                    //Swal.fire({
                    //    title: "¿Quiere modificar el estado del registro?",
                    //    icon: "warning",
                    //    showCancelButton: true,
                    //    confirmButtonText: "Aceptar",
                    //    confirmButtonClass: "btn btn-danger",
                    //    cancelButtonText: "Cancelar"
                    //}).then(function (result) {
                    //    Swal.fire({
                    //        icon: "success",
                    //        allowOutsideClick: false,
                    //        title: "Éxito",
                    //        text: "Registro modificado satisfactoriamente.",
                    //        confirmButtonText: "Aceptar"
                    //    }).then((result) => {
                            fila.remove();
                            if (cantreg == 1) {
                                $btnAgregTareo.hide();
                                filaEncbz.remove();
                            }
                    //    })
                    //});
                }                
            });
        },
        AgregarTareo: function () {
            $btnAgregTareo.on("click", function () {
                //VERIFICO QUE EL FORMULARIO ESTE LLENO
                if ($formTareo.valid()) {
                    //VERIFICO SI HAY REGISTROS
                    let cantreg = $tblPrsnlAggdo.find('tbody tr').length; //CUANTOS REGISTROS TENGO
                    if (cantreg && flag) {
                        //DATOS PARA ALMACENAR0
                        var indBambas;
                        $chkBambas.is(':checked') == true ? indBambas = true : indBambas = false;
                        var parametro = {
                            idgrnca: $selGrnca.val(),
                            idsprintndnca: $selsprintd.val(),
                            idara: $selarea.val(),
                            idgrdia: $cbogrda.val(),
                            finctro: $fecini.val(),
                            ffintro: $fecfin.val(),
                            indbmbs: indBambas,
                            idcntrtsta: $cbo_empresa.val(), 
                            gdestdo: $formTareo.find("[name='GDESTDO']").val()
                        };
                        $.post("/Tareo/insertarTareo", parametro).done(function (result) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Se registro satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                //if (result) {
                                //    RedirectWithSubfolder("/tareo/asistencia/"+id);
                                //}
                                flag = false;
                                $btnAgregTareo.attr("disabled", true);
                                //LUEGO DE GUARDAR NECESITO OBTENER ULTIMO REGISTRO GUARDADO PARA ASOCIARLO AL DETALLE
                                $.get("/Tareo/obtener-ultimo-tareo").done(function (data) {
                                    $("#tbl_prsnl_aggdo tbody tr").each(function (index) {
                                        var csaptbl = $(this).children('td').find("[class='csap']").text();
                                        var param = {
                                            idtro: data.id, //si lo trae pero el penultimo XD
                                            csap: csaptbl,
                                            gdestdo: 'A'
                                        }
                                        $.post("/Tareo/insertar-detalle-tareo", param)
                                    })
                                });
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });                            
                        })

                    } 
                }                 
            })
        },
        guardarDetalle: function () {
            var proof = $tblPrsnlAggdo.find("td:eq(3)").text();
        },
        restarFechas: function (f1, f2) {
            var aFecha1 = f1.split('/');
            var aFecha2 = f2.split('/');
            var fFecha1 = Date.UTC(aFecha1[2], aFecha1[1] - 1, aFecha1[0]);
            var fFecha2 = Date.UTC(aFecha2[2], aFecha2[1] - 1, aFecha2[0]);
            var dif = fFecha2 - fFecha1;
            var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
            return dias;
        },
        sumarFecha: function (f1, cantd) {
            var arrFec = f1.split("/")
            var annio = arrFec[2]
            var mes = arrFec[1] - 1
            var dia = arrFec[0]
            var fecha = new Date(annio, mes, dia)
            diaF = fecha.setDate(fecha.getDate() + (cantd));
            var year = new Date(diaF).getFullYear();
            var month = new Date(diaF).getMonth() + 1;
            var day = new Date(diaF).getDate()
            var dy = (day < 10) ? `0${day}` : day
            var mnth = (month < 10) ? `0${month}` : month  
            var df = `${dy}/${mnth}/${year}`
            return df;
        },
        capturarURL: function () {
            var Url = $(location).attr('hash');
            alert(Url);
        },
        init: function () {
            eventosIncrustados.ocultarSelectCtrtst();
            eventosIncrustados.BuscarPersonal();
            eventosIncrustados.ObtenerAttrGuardia();
            eventosIncrustados.ObtenerFechaFinal();
            eventosIncrustados.FiltroPersonal();
            eventosIncrustados.btnAddPersonal();
            eventosIncrustados.VistaPrevia();
            eventosIncrustados.EliminarRegistro();
            eventosIncrustados.AgregarTareo();
        }
    }

    var configDatePickers = {
        opcStandar: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighLighted: "6,0",
            autoclose: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        fechainicial: function () {
            $fecini.on("change", function () {
                var fechaInic = $(this).val();
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var valF = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
                $fecini.removeClass("error");
                $formTareo.find(".msgF0").text("");
                if (mayorFechaActual(valF, fechaInic)) {
                    $fecini.addClass("error");
                    $formTareo.find(".msgF0").text("La fecha de inicio de guardia no puede ser menor a la fecha actual.");
                    return;
                } else {                         
                    $fecini.removeClass("error");
                    $formTareo.find(".msgF0").text("");
                }
            })
        },
        fechafinal: function () {
            $fecfin.on("change", function () {
                var fechaFinal = $(this).val();
                var fechaInicial = $fecini.val();
                $fecfin.removeClass("error");
                $fecfin.parent().find('error').remove();
                if (mayorFechaActual(fechaInicial, fechaFinal)) {
                    $fecfin.addClass("error");
                    $fecfin.parent().append('<label id="TXTFFIN-error" class="error" for="TXTFFIN">La fecha final de guardia no puede ser menor a la fecha inicial.</label>');
                    return;
                }
            })
        },
        init: function () {
            $fecini.datepicker(configDatePickers.opcStandar);
            $fecfin.datepicker(configDatePickers.opcStandar);

            this.fechainicial();
            this.fechafinal();
        }
    }

    return {
        init: function () {
            $btnAgregTareo.hide();
            $formTareo.AgregarCamposDefectoAuditoria();
            $formTareo.DeshabilitarCamposAuditoria();
            selects.init();
            configDatePickers.init();
            formModalPrsnal.init();
            modalPersonalTareo.init();
            eventosIncrustados.init();
        }
    }
}();

$(function () {
    InicializadorDatosTareo.init();
})
