﻿let InicializarDatosTareo = function () {

    //DATOS PARA LA BUSQUEDA
    let $selectGerencia = $(".select-gerencia");
    let $selectSuperintendencia = $(".select-superintendencia");
    let $selectArea = $(".select-area");
    let $selectGuardia = $(".select-guardia");
    let $chkBmbs = $("#checkBambas");
    let $chkBambas = $chkBmbs.find("input[type=checkbox]");
    let $selectContratista = $(".select-contratista");
    let $selectRoster = $(".select-roster");
    //let $numGuardia = $("#numGrdia");
    let $fechaInicio = $("#txtFInic");
    let $fechaFin = $("#txtFFin");
    let $areaCntrtst = $("#areaContratista");

    let $accesoAgregarTareo = $("#accesoAgregarTareo");

    //BOTON
    //let $btnBuscarTareo = $("#btnBuscarTareo");
    let $btnTareo = $("#btnTareo");

    //TABLA
    let $tablaTareo = $("#tabla_tareo");

    let $cboGrncia = $("#cboGrncia");

    let validarControles = {
        init: function () {
            if ($accesoAgregarTareo.val() == "false") {
                $btnTareo.hide();
            }
        }
    }

    let tablaTareo = {
        objeto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/Tareo/listar-tareo",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idgrnc = $selectGerencia.val();
                    data.idsprntndnc = $selectSuperintendencia.val();
                    data.idarea = $selectArea.val();
                    data.idcntrtsta = $selectContratista.val();
                    data.idrster = $selectRoster.val();
                    data.numgrdia = $selectGuardia.val();
                    data.finic = $fechaInicio.val();
                    data.ffin = $fechaFin.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Superintendencia",
                    data: "sprintndnca",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Área",
                    data: "ara",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "MMG",
                    data: null,
                    orderable: false,
                    className: "text-center",
                    render: function (data) {
                        var tmp = "";
                        tmp += '<input class="form-control form-control-sm" type="checkbox"' + (data.indbmbs ? "checked" : "") + ' disabled="disabled">';

                        return tmp;
                    }
                },
                //{
                //    title: "Contratista",
                //    data: "idcntrtsta",
                //    orderable: false,
                //    className: "text-center"
                //},
                {
                    title: "Roster",
                    data: "rstr",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Guardia",
                    data: "grdia",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Turno",
                    data: "trno",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Fecha Inicio",
                    data: "finctro",
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Fecha Fin",
                    data: "ffintro",
                    orderable: false,
                    className: "text-center"
                },
                //{
                //    title: "Cant. Personas",
                //    data: "cntprsns",
                //    orderable: false,
                //    className: "text-center"
                //},
                {
                    title: "Resumen",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<button type="button" class="btn btn- btn-xs btn-resumen" data-id="${data.id}" title="Resumen"><span><i class="la la-eye"></i></span></button>`;

                        return tpm;
                    }
                },
                {
                    title: "Asis.",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<button type="button" class="btn btn-secondary btn-xs btn-asistencias" data-id="${data.id}" title="Asistencias"><span><i class="la la-list"></i></span></button>`;
                        return tpm;
                    }
                },
            ]
        },
        eventos: function () {
            tablaTareo.objeto.on("click", ".btn-resumen", function () {
                var id = $(this).data("id");
                var url = `/tareo/resumen/${id}`;
                RedirectWithSubfolder(url);
            });
            tablaTareo.objeto.on("click", ".btn-asistencias", function () {
                var id = $(this).data("id");
                var url = `/tareo/asistencia/${id}`;
                RedirectWithSubfolder(url);
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        init: function () {
            tablaTareo.objeto = $tablaTareo.DataTable(tablaTareo.opciones);
            this.eventos();
        }
    }

    let funcionesTareo = {
        initFormTareo: function () {
            $chkBambas.prop('checked', true);
            $areaCntrtst.hide();
        }
    }

    let configDatePickers = {
        opcStandar: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        fechaInicio: function () {
            $fechaInicio.datepicker(configDatePickers.opcStandar);
            $fechaInicio.on("change", function () {
                tablaTareo.reload();
            });
        },
        fechaFin: function () {
            $fechaFin.datepicker(configDatePickers.opcStandar);
            $fechaFin.on("change", function(){
                tablaTareo.reload();
            });
        },
        init: function () {
            this.fechaInicio();
            this.fechaFin();
        }
    }

    let eventosIncrustadosTareo = {
        ocultarSelectCtrtst: function () {
            $chkBambas.on("click", function () {
                $chkBambas.is(':checked') == true ? $areaCntrtst.hide() : $areaCntrtst.show();
                $selectContratista.val("");
            });
        },
        init: function () {
            eventosIncrustadosTareo.ocultarSelectCtrtst();
        }
    }

    //OBTENER COMBOS
    let selects = {
        gerencia: function (idGerencia, idSuperintendencia, idArea) {
            $selectSuperintendencia.attr("disabled", true);
            $selectArea.attr("disabled", true);
            $selectGerencia.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-gerencia").done(function (data) {
                $.each(data, function (key, item) {
                    $selectGerencia.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                });
                $selectGerencia.children('option[value=""]').text("Seleccione");
                if (idGerencia) {
                    $selectGerencia.val(idGerencia)
                    selects.superintendencia(idGerencia, idSuperintendencia, idArea)
                }
            });
            $selectGerencia.on("change", function () {
                selects.superintendencia();
                selects.area();
            });
        },
        superintendencia: function (idGerencia, idSuperintendencia, idArea) {
            $selectSuperintendencia.empty();
            $selectSuperintendencia.append($("<option />").val('').text("Cargando..."));
            if ($selectGerencia.val()) {
                $.get("/obtener-superintendencia", { idGerencia: (idGerencia ? idGerencia : $selectGerencia.val()) }).done(function (data) {
                    $.each(data, function (key, item) {
                        $selectSuperintendencia.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectSuperintendencia.attr("disabled", false);
                    if (idSuperintendencia) {
                        $selectSuperintendencia.val(idSuperintendencia);
                        selects.area(idSuperintendencia, idArea);
                    }
                });
            } else {
                $selectSuperintendencia.val("").change();
            }
            $selectSuperintendencia.children('option[value=""]').text("Seleccione");
            $selectSuperintendencia.unbind('change');
            $selectSuperintendencia.on("change", function () {
                selects.area();
            });
        },
        area: function (idSuperintendencia, idArea) {
            $selectArea.empty();
            $selectArea.append($("<option />").val('').text("Cargando..."));
            if ($selectSuperintendencia.val()) {
                $.get("/obtener-areas", { idSuperintendencia: (idSuperintendencia ? idSuperintendencia : $selectSuperintendencia.val()) }).done(function (data) {
                    $.each(data, function (key, item) {
                        $selectArea.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectArea.attr("disabled", false);
                    if (idArea) {
                        $selectArea.val(idArea)
                    }
                });
            } else {
                $selectArea.val("");
            }
            $selectArea.children('option[value=""]').text("Seleccione");
            tablaTareo.reload();
        },
        contratista: function (idContratista) {
            $selectContratista.append($("<option />").val('').text("Cargando..."));
            $.get("/obtenerContratista").done(function (data) {
                $.each(data, function (key, item) {
                    $selectContratista.append($("<option />").val(item["id"]).text(item["cntrtsta"]));
                });
                $selectContratista.children('option[value=""]').text("Seleccione");
                if (idContratista) {
                    $selectContratista.val(idContratista);
                }
            });
            $selectContratista.on("change", function () {
                tablaTareo.reload();
            });
        },
        guardia: function (idGuardia) {
            $selectGuardia.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-guardias").done(function (data) {
                $.each(data, function (key, item) {
                    $selectGuardia.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                });
                $selectGuardia.children('option[value=""]').text("Seleccione");
                if (idGuardia) {
                    $selectGuardia.val(idRoster);
                }
            });
            $selectGuardia.on("change", function () {
                tablaTareo.reload();
            });
        },
        roster: function (idRoster) {
            $selectRoster.append($("<option />").val('').text("Cargando..."));
            $.get("/obtener-roster").done(function (data) {
                $.each(data, function (key, item) {
                    $selectRoster.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                });
                $selectRoster.children('option[value=""]').text("Seleccione");
                if (idRoster) {
                    $selectRoster.val(idRoster);
                }
            });
            $selectRoster.on("change", function () {
                tablaTareo.reload();
            });
        },
        init: function () {
            selects.gerencia()
            selects.contratista()
            selects.roster()
            selects.guardia()
        }
    };

    return {
        inicializar: function () {
            selects.init();
            tablaTareo.init();
            funcionesTareo.initFormTareo();
            eventosIncrustadosTareo.init();
            validarControles.init();
            configDatePickers.init();
        }
    }
}();

$(() => {
    InicializarDatosTareo.inicializar();
})