﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.WEB.Helpers
{
    public static class ControlesVistas
    {
        public const string VistaTareo = "TAREO";
        public static class Tareo
        {
            public const string AgregarTareo = "AGRGRTRO";
            public const string EditarTareo = "EDTRTRO";
            public const string EliminarTareo = "ELMNRTRO";
        }
    }
}
