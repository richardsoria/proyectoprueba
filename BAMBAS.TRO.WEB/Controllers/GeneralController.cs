﻿using BAMBAS.CORE.Extensions;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.General.Seguridad;
using BAMBAS.Negocios.General.Persona;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.WEB.Controllers
{
    public class GeneralController : Controller
    {
        private readonly IGrupoDatoProxy _grupoDatoProxy;
        private readonly IUbigeoProxy _ubigeoProxy;
        private readonly IAreaProxy _areaProxy;
        private readonly ISuperintendenciaProxy _superintendenciaProxy;
        private readonly IGerenciaProxy _gerenciaProxy;
        //private readonly IEmpresaProxy _empresaProxy;
        private readonly IRosterProxy _rosterProxy;
        private readonly ITrabajadorProxy _trabajadorProxy;
        private readonly IGuardiaProxy _guardiaProxy;
        

        public GeneralController(
            IGrupoDatoProxy grupoDatoProxy, 
            IUbigeoProxy ubigeoProxy,
            IAreaProxy areaProxy,
            ISuperintendenciaProxy superintendenciaProxy,
            IGerenciaProxy gerenciaProxy,
            //IEmpresaProxy empresaProxy,
            IRosterProxy rosterProxy,
            ITrabajadorProxy trabajadorProxy,
            IGuardiaProxy guardiaProxy)
        {
            _grupoDatoProxy = grupoDatoProxy;
            _ubigeoProxy = ubigeoProxy;
            _areaProxy = areaProxy;
            _superintendenciaProxy = superintendenciaProxy;
            _gerenciaProxy = gerenciaProxy;
            //_empresaProxy = empresaProxy;
            _rosterProxy = rosterProxy;
            _trabajadorProxy = trabajadorProxy;
            _guardiaProxy = guardiaProxy;
        }
        [HttpGet("obtener-datos-usuario-logueado")]
        public IActionResult ObtenerLogueado()
        {
            var audit = new AuditoriaDto();
            audit.UEDCN = User.GetUserCode();
            audit.UCRCN = User.GetUserCode();
            audit.FCRCN = DateTime.Now;
            audit.FEDCN = DateTime.Now;
            audit.FESTDO = DateTime.Now;
            return Ok(audit);
        }
        [HttpGet("obtener-gerencia")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var gerencia = await _gerenciaProxy.ObtenerActivas();
            return Ok(gerencia);
        }
        [HttpGet("obtener-superintendencia")]
        public async Task<IActionResult> ObtenerSuperintendencias(int idGerencia)
        {
            var superintendencia = await _superintendenciaProxy.ObtenerActivas(idGerencia);
            return Ok(superintendencia);
        }
        [HttpGet("obtener-areas")]
        public async Task<IActionResult> ObtenerAreas(int idSuperintendencia)
        {
            var areas = await _areaProxy.ObtenerActivas(idSuperintendencia);
            return Ok(areas);
        }
        //[HttpGet("obtener-contratista")]
        //public async Task<IActionResult> ObtenerEmpresas()
        //{
        //    var empresas = await _empresaProxy.ObtenerActivos();
        //    return Ok(empresas);
        //}
        [HttpGet("obtener-roster")]
        public async Task<IActionResult> ObtenerRoster()
        {
            var roster = await _rosterProxy.ObtenerActivos();
            return Ok(roster);
        }
        [HttpGet("buscar-roster")]
        public async Task<IActionResult> BuscarRoster(int id)
        {
            var roster = await _rosterProxy.Obtener(id);
            return Ok(roster);
        }
        [HttpGet("obtenerContratista")]
        public async Task<IActionResult> ObtenerContratista()
        {
            var contratista = await _trabajadorProxy.ObtenerContratista();
            return Ok(contratista);
        }
        [HttpGet("obtenercatalogoGD")]
        public async Task<IActionResult> ObtenerCatalogo()
        {
            var result = await _grupoDatoProxy.ObtenerCatalogos();
            return Ok(result);
        }
        [HttpGet("obtener-guardias")]
        public async Task<IActionResult> ObtenerGuardias() {
            var result = await _guardiaProxy.ObtenerActivas();
            return Ok(result);
        }
        [HttpGet("obtener-atrbt-guardias")]
        public async Task<IActionResult> ObtenerAtributosGuardias(int id)
        {
            var result = await _guardiaProxy.ObtenerPorId(id);
            return Ok(result);
        }
    }
}
