﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Tareo.Tareo;
using BAMBAS.Negocios.Tareo;
using BAMBAS.Negocios.General.Persona;
using Microsoft.AspNetCore.Authorization;
using BAMBAS.TRO.WEB.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BAMBAS.TRO.WEB.Controllers
{
    [Route("Tareo")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]
    public class TareoController : Controller
    {
        private readonly ITareoProxy _tareoProxy;
        private readonly ITrabajadorProxy _trabajadorProxy;
        private readonly IDataTableService _dataTableService;

        //CONSTRUCTOR
        public TareoController(IDataTableService dataTableService,
            ITareoProxy tareoProxy,
            ITrabajadorProxy trabajadorProxy)
        {
            _dataTableService = dataTableService;
            _tareoProxy = tareoProxy;
            _trabajadorProxy = trabajadorProxy;
        }
        public ActionResult Index()
        {
            return View();
        }

        #region PersonaTrabajador
        [HttpGet("listarPersonal")]
        public async Task<IActionResult> listarPersonal(string csap, string datos, int idcntrtsta, int bambas)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var personal = await _trabajadorProxy.listadoPersonalTareo(parametrosDT, csap, datos, idcntrtsta, bambas);
            return Ok(personal);
        }
        #endregion

        [HttpGet("listar-tareo")]
        public async Task<IActionResult> listarTareo(int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin)
        {
            var parametrosDT = _dataTableService.GetSentParameters();
            var retorno = await _tareoProxy.ObtenerDataTable(parametrosDT, idgrnc, idsprntndnc, idarea, idcntrtsta, idrster, numgrdia, finic, ffin);
            return Ok(retorno);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> listar(int id)
        {
            var retorno = await _tareoProxy.Listar(id);
            return Ok(retorno);
        }

        [HttpGet("agregar")]
        [HttpGet("editar/{id}")]
        public IActionResult Datos(int? id = null)
        {
            return View(id);
        }

        [HttpGet("asistencia/{id}")]
        public IActionResult Asistencia(int id)
        {
            return View(id);
        }

        [HttpGet("resumen/{id}")]
        public IActionResult Resumen(int id)
        {
            return View(id);
        }

        [HttpPost("insertarTareo")]
        public async Task<IActionResult> insertarTareo(TareoDto entidad)
        {
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();

            var ret = await _tareoProxy.Insertar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok(ret.CodEstado);
        }
        [HttpPost("insertar-detalle-tareo")]
        public async Task<IActionResult> InsertarDetalleTareo(DatosPersonalTareo entidad)
        {
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();
            var ret = await _tareoProxy.InsertarDetalleTareo(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok(ret.CodEstado);
        }
        [HttpGet("obtenerTareo")]
        public async Task<IActionResult> obtenerTareo(int id)
        {
            var retorno = await _tareoProxy.Obtener(id);
            return Ok(retorno);
        }

        [HttpGet("obtener-ultimo-tareo")]
        public async Task<IActionResult> ObtenerUltimoTareo()
        {
            var retorno = await _tareoProxy.ObtenerUltimoRegistro();
            return Ok(retorno);
        }
        [HttpGet("actualizarTareo")]
        public async Task<IActionResult> actualizarTareo(TareoDto entidad)
        {
            entidad.UEDCN = User.GetUserCode();
            var ret = await _tareoProxy.Actualizar(entidad);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }

        [HttpPost("eliminarTareo")]
        public async Task<IActionResult> eliminarTareo(int id)
        {
            TareoDto eliminarTareo = new TareoDto();
            eliminarTareo.ID = id;
            eliminarTareo.UEDCN = User.GetUserCode();
            var ret = await _tareoProxy.Eliminar(eliminarTareo);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
    }
}
