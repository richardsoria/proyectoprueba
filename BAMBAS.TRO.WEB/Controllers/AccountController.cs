﻿using BAMBAS.CORE.Extensions;
using BAMBAS.TRO.WEB.Base;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.WEB.Controllers
{
    [Route("[controller]")]
    public class LoginController : ControllerCore
    {
        private readonly string _authenticationUrl;
        private readonly IMemoryCache _cache;

        public LoginController(IConfiguration configuration, IMemoryCache cache)
        {
            _authenticationUrl = configuration.GetValue<string>("AuthenticationUrl");
            _cache = cache;
        }
        [HttpGet("/acceso-denegado")]
        public IActionResult AccessDenied(string ReturnUrl = "/")
        {
            return View("AccessDenied", ReturnUrl);
        }
        [HttpGet("/no-encontrada")]
        public IActionResult NotFound(string ReturnUrl = "/")
        {
            return View("NotFound", ReturnUrl);
        }
        public IActionResult Login()
        {
            return Redirect(_authenticationUrl + $"?ReturnBaseUrl={this.Request.Scheme}://{this.Request.Host}/");
        }

        [HttpGet("/cambiar-contrasena")]
        public IActionResult CambioContrasena()
        {
            return Redirect(_authenticationUrl + $"CambioContrasena?UserName={User.GetUserCode()}&ReturnBaseUrl={this.Request.Scheme}://{this.Request.Host}/");
        }
        [HttpGet("/indice-modulos")]
        public IActionResult IndiceModulos()
        {
            return Redirect(_authenticationUrl + $"Conexion?at={User.GetUserAccessToken()}");
        }
        [HttpGet("connect")]
        public async Task<IActionResult> Connect(string access_token)
        {
            await HttpContext.ConectarConAccessToken(access_token, _cache);
            return Redirect("/");
        }
        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            var logueadoPorActiveDirectory = User.EstaAutenticadoPorActiveDirectory();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            if (logueadoPorActiveDirectory)
            {
                return Redirect($"{_authenticationUrl}Logout");
            }
            return Redirect(_authenticationUrl);
        }
    }
}