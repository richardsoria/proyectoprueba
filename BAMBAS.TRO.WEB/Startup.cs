using BAMBAS.CONFIG;
using BAMBAS.Negocios;
using BAMBAS.Negocios.Config;
using BAMBAS.TRO.WEB.Extensions;
using BAMBAS.TRO.WEB.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BAMBAS.TRO.WEB
{
    public class Startup
    {
        private readonly IWebHostEnvironment CurrentEnviroment;
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            Configuration = configuration;
            CurrentEnviroment = webHostEnvironment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region REPOSITORY / SERVICE
            services.AddRepository();
            services.AddTransientServices();
            services.AddMemoryCache();
            #endregion

            #region CONFIGURATION
            services.AddSingleton<HttpClient>();
            #endregion

            #region Auth
            services.Configure<ApiUrls>(opts => Configuration.GetSection("ApiUrls").Bind(opts));
            services.AddHttpContextAccessor();
            #endregion

            #region Error handler
            Action<MvcOptions> setupAction = (options) => { };
            setupAction = (options) => { options.Filters.Add(new ErrorHandlerAttribute()); };
            #endregion

            #region Razor Pages & MVC
            services.AddRazorPages(o => o.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute())).AddMvcOptions(setupAction).AddRazorRuntimeCompilation();
            services.AddControllersWithViews();
            #endregion

            #region Add Cookie Authentication
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options => {
                options.AccessDeniedPath = "/acceso-denegado";
                options.LoginPath = "/login";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);    //www.tutorialspoint.com/timespan-fromminutes-method-in-chash        OJO
                options.SlidingExpiration = true;
            });
            services.Configure<CookiePolicyOptions>(options => {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.ConfigureApplicationCookie(options => {
                options.LoginPath = "/login";
                options.Cookie.Name = "Cp_AUTH";
                options.AccessDeniedPath = "/acceso-denegado";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto });
            
            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = new PathString($"/{ConfiguracionProyecto.CARPETAS_DESPLIEGUE.Tareo_Web}")
            });
            app.UsePathBase($"/{ConfiguracionProyecto.CARPETAS_DESPLIEGUE.Tareo_Web}");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            app.Run(context => 
            { 
                context.Response.StatusCode = 404;
                return Task.FromResult(0);
            });
        }
    }
}
