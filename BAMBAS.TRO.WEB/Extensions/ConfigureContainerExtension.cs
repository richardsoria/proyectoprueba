﻿using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.General.Persona;
using BAMBAS.Negocios.General.Seguridad;
using BAMBAS.Negocios.Tareo;
using BAMBAS.TRO.WEB.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.WEB.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddRepository(this IServiceCollection serviceCollection)
        {
            
        }
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            //BASE
            serviceCollection.AddTransient<ISelect2Service, Select2Service>();
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IPaginationService, PaginationService>();
            serviceCollection.AddTransient<IViewRenderService, ViewRenderService>();

            //PROXY
            serviceCollection.AddHttpClient<IPerfilObjetoProxy, PerfilObjetoProxy>();
            serviceCollection.AddHttpClient<ITareoProxy, TareoProxy>();
            serviceCollection.AddHttpClient<ITrabajadorProxy, TrabajadorProxy>();   
            serviceCollection.AddScoped<AuthLogin>();

            //SEGURIDAD
            serviceCollection.AddHttpClient<IUbigeoProxy, UbigeoProxy>();
            serviceCollection.AddHttpClient<IAreaProxy, AreaProxy>();
            serviceCollection.AddHttpClient<ISuperintendenciaProxy, SuperintendenciaProxy>();
            serviceCollection.AddHttpClient<IGerenciaProxy, GerenciaProxy>();
            serviceCollection.AddHttpClient<IGrupoDatoProxy, GrupoDatoProxy>();
            //serviceCollection.AddHttpClient<IEmpresaProxy, EmpresaProxy>();
            serviceCollection.AddHttpClient<IRosterProxy, RosterProxy>();
            serviceCollection.AddHttpClient<IGuardiaProxy, GuardiaProxy>();
        }
    }
}
