﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Telefono;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface ITelefonoProxy
    {
        Task<DataTablesStructs.ReturnedData<TelefonoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id);
        Task<List<TelefonoDto>> Listar(int idprsna, string id);
        Task<TelefonoDto> Obtener(string idprsna, string id);
        Task<RespuestaConsulta> Insertar(TelefonoDto Telefono);
        Task<RespuestaConsulta> Actualizar(TelefonoDto Telefono);
        Task<RespuestaConsulta> Eliminar(TelefonoDto command);
    }
    public class TelefonoProxy : ITelefonoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public TelefonoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<TelefonoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}telefono/obtener-tabla?idprsna={idprsna}&id={id}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<TelefonoDto>(-2);
        }
        public async Task<List<TelefonoDto>> Listar(int idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}telefono/listar?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TelefonoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<TelefonoDto> Obtener(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}telefono/obtener?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<TelefonoDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(TelefonoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}telefono/insertar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(TelefonoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}telefono/actualizar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(TelefonoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}telefono/eliminar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Eliminar));
        }
    }
}
