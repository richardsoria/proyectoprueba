﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Documento;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface IDocumentoProxy
    {
        Task<DataTablesStructs.ReturnedData<ProyeccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idprsna, string id);
        Task<List<ProyeccionDto>> Listar(int idprsna, int id);
        Task<ProyeccionDto> Obtener(string idprsna, string id);
        Task<string> ObtenerUltimoCRN();
        Task<RespuestaConsulta> Insertar(ProyeccionDto Documento);
        Task<RespuestaConsulta> Actualizar(ProyeccionDto Documento);
        Task<RespuestaConsulta> Eliminar(ProyeccionDto command);
    }
    public class DocumentoProxy : IDocumentoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public DocumentoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<ProyeccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}documento/obtener-tabla?idprsna={idprsna}&id={id}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ProyeccionDto>(-2);
        }
        public async Task<List<ProyeccionDto>> Listar(int idprsna, int id)
        {
            var url = $"{_apiUrls.PersonaUrl}documento/listar?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ProyeccionDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<ProyeccionDto> Obtener(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}documento/obtener?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ProyeccionDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<string> ObtenerUltimoCRN()
        {
            var url = $"{_apiUrls.PersonaUrl}documento/obtenerUltimoCRN";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return await request.Content.ReadAsStringAsync();
        }
        public async Task<RespuestaConsulta> Insertar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}documento/insertar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}documento/actualizar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(ProyeccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}documento/eliminar", content);
            //request.EnsureSuccessStatusCode();
            // return await request.Content.ReadAsStringAsync();
            return request.Respuesta(-2, nameof(Eliminar));
        }
    }
}
