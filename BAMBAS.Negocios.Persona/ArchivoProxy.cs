﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Archivo;
using BAMBAS.Negocios.Config;
using BAMBAS.Negocios;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface IArchivoProxy
    {
        Task<DataTablesStructs.ReturnedData<ArchivoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna);
        Task<List<ArchivoDto>> Listar(string idprsna, string id);
        Task<ArchivoDto> Obtener(string idprsna, string id);
        Task<RespuestaConsulta> Insertar(ArchivoDto entidad);
        Task<RespuestaConsulta> Eliminar(ArchivoDto entidad);
    }
    public class ArchivoProxy : IArchivoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;
        public ArchivoProxy(HttpClient httpClient,IOptions<ApiUrls> apiUrls, IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);
            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<ArchivoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna)
        {
            var url = $"{_apiUrls.PersonaUrl}archivo/obtener-tabla?idprsna={idprsna}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ArchivoDto>(-2);
        }
        public async Task<List<ArchivoDto>> Listar(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}archivo/listar?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArchivoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
            );
        }
        public async Task<ArchivoDto> Obtener(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}archivo/obtener?idprsna={idprsna}&id={id}";

            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ArchivoDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(ArchivoDto entidad)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(entidad),
                Encoding.UTF8,
                "application/json"
            );
            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}archivo/insertar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Eliminar(ArchivoDto entidad)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(entidad),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}archivo/eliminar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Eliminar));
        }
        


    }
}
