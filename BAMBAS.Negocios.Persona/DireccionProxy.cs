﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Direccion;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface IDireccionProxy
    {
        Task<DataTablesStructs.ReturnedData<DireccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id);
        Task<List<DireccionDto>> Listar(string idprsna, string id);
        Task<DireccionDto> Obtener(string idprsna, string id);
        Task<RespuestaConsulta> Insertar(DireccionDto Direccion);
        Task<RespuestaConsulta> Actualizar(DireccionDto Direccion);
        Task<RespuestaConsulta> Eliminar(DireccionDto command);
    }
    public class DireccionProxy : IDireccionProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public DireccionProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<DireccionDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}direccion/obtener-tabla?idprsna={idprsna}&id={id}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<DireccionDto>(-2);
        }
        public async Task<List<DireccionDto>> Listar(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}direccion/listar?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<DireccionDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DireccionDto> Obtener(string idprsna, string id)
        {
            var url = $"{_apiUrls.PersonaUrl}direccion/obtener?idprsna={idprsna}&id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<DireccionDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(DireccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}direccion/insertar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(DireccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}direccion/actualizar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(DireccionDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}direccion/eliminar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Eliminar));
        }
    }
}
