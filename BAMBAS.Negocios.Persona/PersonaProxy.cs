﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Persona;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface IPersonaProxy
    {
        Task<DataTablesStructs.ReturnedData<ListarPersona>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string estado, bool? soloContratistas = null, bool? soloTrabajadores = null);
        Task<DataTablesStructs.ReturnedData<ListarPersona>> ObtenerColaboradoresDataTable(DataTablesStructs.SentParameters parameters, string idprsna);
        Task<List<ListarPersona>> Listar(string tPersona, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado,bool contratista);
        Task<DatosPersona> Obtener(int id);
        Task<RespuestaConsulta> Insertar(PersonaDto Persona);
        Task<RespuestaConsulta> Actualizar(PersonaDto Persona);
        Task<RespuestaConsulta> Eliminar(PersonaDto command);
        Task<string> ConsultarSuSalud(string _tiDocumento, string _nuDocumento);
        Task<DataTablesStructs.ReturnedData<ListarPersona>> listadoPersonaSinUsuario(DataTablesStructs.SentParameters parameters, string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado);
        Task<DataTablesStructs.ReturnedData<HomonimoPersona>> listaHomonimos(DataTablesStructs.SentParameters parameters, string gddcmnto, string ndcmnto, string aptrno, string amtrno, string pnmbre, string snmbre);
    }
    public class PersonaProxy : IPersonaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PersonaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<ListarPersona>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string estado,bool? soloContratistas= null, bool? soloTrabajadores = null)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/obtener-tabla?gdtprsna={gdtprsna}&tipoDocumento={tipoDocumento}&numeroDocumento={numeroDocumento}&datos={datos}&estado={estado}&soloContratistas={soloContratistas}&soloTrabajadores={soloTrabajadores}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ListarPersona>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ListarPersona>> ObtenerColaboradoresDataTable(DataTablesStructs.SentParameters parameters, string idprsna)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/obtener-colaboradores?idprsna={idprsna}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ListarPersona>(-3);
        }
        //public async Task<List<ListarPersona>> Listar(string tPersona, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado)
        //{
        //    var url = $"{_apiUrls.PersonaUrl}persona/listar?tPersona={tPersona}&tipoDocumento={tipoDocumento}&numeroDocumento={numeroDocumento}&datos={datos}&fechaDesde={fechaDesde}&fechaHasta={fechaHasta}&estado={estado}";
        //    var request = await _httpClient.GetAsync(url);
        //    request.EnsureSuccessStatusCode();

        //    return JsonSerializer.Deserialize<List<ListarPersona>>(
        //        await request.Content.ReadAsStringAsync(),
        //        new JsonSerializerOptions
        //        {
        //            PropertyNameCaseInsensitive = true
        //        }
        //    );
        //}
        public async Task<List<ListarPersona>> Listar(string tPersona, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado, bool contratista)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/listar?tPersona={tPersona}&tipoDocumento={tipoDocumento}&numeroDocumento={numeroDocumento}&datos={datos}&fechaDesde={fechaDesde}&fechaHasta={fechaHasta}&estado={estado}&contratista={contratista}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ListarPersona>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DatosPersona> Obtener(int id)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<DatosPersona>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(PersonaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}persona/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(PersonaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}persona/actualizar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(PersonaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}persona/eliminar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<DataTablesStructs.ReturnedData<ListarPersona>> listadoPersonaSinUsuario(DataTablesStructs.SentParameters parameters, string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/listadoPersonaSinUsuario?id={id}&gdtprsna={gdtprsna}&tipoDocumento={tipoDocumento}&numeroDocumento={numeroDocumento}&datos={datos}&fechaDesde={fechaDesde}&fechaHasta={fechaHasta}&estado={estado}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);

            return request.RespuestaTabla<ListarPersona>(-2);
        }
        public async Task<string> ConsultarSuSalud(string _tiDocumento, string _nuDocumento)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/consultar-susalud?tipoDocumento={_tiDocumento}&numDocumento={_nuDocumento}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<string>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<HomonimoPersona>> listaHomonimos(DataTablesStructs.SentParameters parameters, string gddcmnto, string ndcmnto, string amtrno, string aptrno, string pnmbre, string snmbre)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/listahomonimo?gddcmnto={gddcmnto}&ndcmnto={ndcmnto}&amtrno={amtrno}&aptrno={aptrno}&pnmbre={pnmbre}&snmbre={snmbre}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<HomonimoPersona>(-3);
        }
    }
}
