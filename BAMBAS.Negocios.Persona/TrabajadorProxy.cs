﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Trabajador;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Persona
{
    public interface ITrabajadorProxy
    {
        Task<DataTablesStructs.ReturnedData<TrabajadorDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<TrabajadorDto>> Listar();
        Task<TrabajadorDto> Obtener(string id);
        Task<List<TrabajadorDto>> ObtenerContratista();
        Task<RespuestaConsulta> Insertar(TrabajadorDto Trabajador);
        Task<RespuestaConsulta> Actualizar(TrabajadorDto Trabajador);
        Task<RespuestaConsulta> Eliminar(TrabajadorDto command);
        Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param);
    }
    public class TrabajadorProxy : ITrabajadorProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public TrabajadorProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<TrabajadorDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<TrabajadorDto>(-2);
        }
     
        public async Task<List<TrabajadorDto>> Listar()
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/listar";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<TrabajadorDto> Obtener(string id)
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<TrabajadorDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(TrabajadorDto command)
        {
            var content = new StringContent(
                    JsonSerializer.Serialize(command),
                    Encoding.UTF8,
                    "application/json"
                );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}trabajador/insertar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(TrabajadorDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}trabajador/actualizar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(TrabajadorDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.PersonaUrl}trabajador/eliminar", content);
            //request.EnsureSuccessStatusCode();
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param)
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/select-trabajador".AgregarParametrosSelect2(param);
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<Select2Structs.ResponseParameters>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<TrabajadorDto>> ObtenerContratista()
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/obtenerContratista";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }

}
