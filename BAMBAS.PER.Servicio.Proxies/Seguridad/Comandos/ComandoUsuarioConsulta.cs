﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.Proxies.Seguridad.Comandos
{
    public class ComandoUsuarioConsulta
    {
        public string IDPRSNA { get; set; }
        public string CUSRO { get; set; }
    }
}
