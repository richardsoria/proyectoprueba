﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.Proxies
{
    public class ComandoPersonaUsuarioActualizar : IRequest<RespuestaConsulta>
    {
        public string IDPRSNA { get; set; }
        public string NYAPLLDS { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string TLFNO { get; set; }
        public string UEDCN { get; set; }
    }
}
