﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.PER.Servicio.Proxies.Seguridad.Comandos;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.Proxies.Seguridad
{
    public interface ISeguridadProxy
    {
        Task<List<ComandoUsuarioConsulta>> ListarUsuarios();
        Task<RespuestaConsulta> ActualizarPersonaUsuario(ComandoPersonaUsuarioActualizar command);
    }
    public class SeguridadProxy : ISeguridadProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;
        public SeguridadProxy(HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<ComandoUsuarioConsulta>> ListarUsuarios()
        {
            var url = $"{_apiUrls.SeguridadUrl}usuario/listadoUsuarios";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ComandoUsuarioConsulta>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> ActualizarPersonaUsuario(ComandoPersonaUsuarioActualizar command)
        {
            
            var url = $"{_apiUrls.SeguridadUrl}usuario/actualizar-persona-usuario";
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
                );
            var request = await _httpClient.PostAsync(url, content);

            return request.Respuesta(-3, "Actualizar Persona Usuario");
          

            //hacer post hacia ms de seguridad
        }
    }
}
