﻿using BAMBAS.CORE.Structs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuSalud.Servicios;
using SuSaludServ;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.SuSalud
{
    public interface ISuSaludProxy
    {
        Task<ConsultaResponse> ConsultarSuSalud(string _tiDocumento, string _nuDocumento);
        Task<DataTablesStructs.ReturnedData<ConsultaResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string tipoDocumento, string numDocumento);

    }
    public class SuSaludProxy : ISuSaludProxy
    {
        private readonly ISuSaludSoap _suSaludSoap;
        public SuSaludProxy(
             ISuSaludSoap suSaludSoap)
        {
            _suSaludSoap = suSaludSoap;
        }
        public async Task<ConsultaResponse> ConsultarSuSalud(string _tiDocumento, string _nuDocumento)
        {
            var retorno = await _suSaludSoap.Consultar(_tiDocumento, _nuDocumento);
          
            return JsonSerializer.Deserialize<ConsultaResponse>(
                retorno,
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<DataTablesStructs.ReturnedData<ConsultaResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string _tiDocumento, string _nuDocumento)
        {
           return await _suSaludSoap.ObtenerDataTable(parameters, _tiDocumento, _nuDocumento);
        }
    }
}
