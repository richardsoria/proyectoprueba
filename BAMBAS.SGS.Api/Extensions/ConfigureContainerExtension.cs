//using ENTITIES.Models.Auth;
using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.Proxies.Persona;
using Microsoft.Extensions.DependencyInjection;

namespace BAMBAS.SGS.Api.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            //
            serviceCollection.AddTransient<IConsultasGrupoDato, ConsultasGrupoDato>();
            serviceCollection.AddTransient<IConsultasEmpresa, ConsultasEmpresa>();
            serviceCollection.AddTransient<IConsultasGrupoDatoDetalle, ConsultasGrupoDatoDetalle>();
            serviceCollection.AddTransient<IConsultasError, ConsultasError>();
            serviceCollection.AddTransient<IConsultasParametro, ConsultasParametro>();
            serviceCollection.AddTransient<IConsultasSucursal, ConsultasSucursal>();
            serviceCollection.AddTransient<IConsultasPerfil, ConsultasPerfil>();
            serviceCollection.AddTransient<IConsultasUbigeo, ConsultasUbigeo>();
            serviceCollection.AddTransient<IConsultasUsuario, ConsultasUsuario>();
            serviceCollection.AddHttpClient<IPersonaProxy, PersonaProxy>();

            serviceCollection.AddTransient<IConsultasPerfilUsuario, ConsultasPerfilUsuario>();
            serviceCollection.AddTransient<IConsultasSucursalUsuario, ConsultasSucursalUsuario>();
            serviceCollection.AddTransient<IConsultasPerfilObjeto, ConsultasPerfilObjeto>();
            serviceCollection.AddTransient<IConsultasObjetoPadre, ConsultasObjetoPadre>();
            serviceCollection.AddTransient<IConsultasObjetoDetalle, ConsultasObjetoDetalle>();

            serviceCollection.AddTransient<IConsultasGerencia, ConsultasGerencia>();
            serviceCollection.AddTransient<IConsultasSuperintendencia, ConsultasSuperintendencia>();
            serviceCollection.AddTransient<IConsultasArea, ConsultasArea>();
            serviceCollection.AddTransient<IConsultasRoster, ConsultasRoster>();
            serviceCollection.AddTransient<IConsultasGuardia, ConsultasGuardia>();
            serviceCollection.AddTransient<IConsultasGrupoCargo, ConsultasGrupoCargo>();
            serviceCollection.AddTransient<IConsultasCargo, ConsultasCargo>();
            serviceCollection.AddTransient<IConsultasAlmacen, ConsultasAlmacen>();
            serviceCollection.AddTransient<IConsultasStock, ConsultasStock>();
            serviceCollection.AddTransient<IConsultasFamilia, ConsultasFamilia>();
            serviceCollection.AddTransient<IConsultasKit, ConsultasKit>();
            serviceCollection.AddTransient<IConsultasArticulo, ConsultasArticulo>();
            serviceCollection.AddTransient<IConsultasTrabajadorTalla, ConsultasTrabajadorTalla>();

            serviceCollection.AddTransient<IConsultasPosicion, ConsultasPosicion>();
            serviceCollection.AddTransient<IConsultasCentroCosto, ConsultasCentroCosto>();
            serviceCollection.AddTransient<IConsultasUnidadOrganizacional, ConsultasUnidadOrganizacional>();

            serviceCollection.AddTransient<IConsultasTrabajadorTalla, ConsultasTrabajadorTalla>();
            serviceCollection.AddTransient<IConsultasTrabajadorRestriccion, ConsultasTrabajadorRestriccion>();

        }
    }
}
