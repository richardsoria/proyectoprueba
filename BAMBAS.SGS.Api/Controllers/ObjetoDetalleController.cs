﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoDetalle;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("objeto-detalle")]
    public class ObjetoDetalleEmpresaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasObjetoDetalle _consultasObjetoDetalle;
        public ObjetoDetalleEmpresaController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasObjetoDetalle consultasObjetoDetalle)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasObjetoDetalle = consultasObjetoDetalle;
        }

        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idObjeto)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasObjetoDetalle.ObtenerDataTable(parameters, idObjeto);
            return Ok(empresas);
        }

        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos(string idObjeto)
        {
            var empresas = await _consultasObjetoDetalle.ObtenerActivos(idObjeto);
            return Ok(empresas);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasObjetoDetalle.Obtener(id);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoObjetoDetalleInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoObjetoDetalleActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoObjetoDetalleEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
