﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDato;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("grupo-dato")]
    public class GrupoDatoEmpresaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasGrupoDato _consultasGrupoDato;
        public GrupoDatoEmpresaController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasGrupoDato consultasGrupoDato)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasGrupoDato = consultasGrupoDato;
        }
        [HttpGet("obtener-todos")]
        public async Task<IActionResult> ObtenerTodos()
        {
            var grupoDatos = await _consultasGrupoDato.ObtenerTodos();
            return Ok(grupoDatos);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var grupoDatos = await _consultasGrupoDato.ObtenerActivos();
            return Ok(grupoDatos);
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasGrupoDato.ObtenerDataTable(parameters);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasGrupoDato.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-catalogo")]
        public async Task<IActionResult> ObtenerCatalogo()
        {
            var empresas = await _consultasGrupoDato.ListarCatalogosGD();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoGrupoDatoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoGrupoDatoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoGrupoDatoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
