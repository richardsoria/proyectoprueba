﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Perfil;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilObjeto;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("perfil-objeto")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerfilObjetoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasPerfilObjeto _consultasPerfilObjeto;
        private readonly IMediator _mediator;
        public PerfilObjetoController(
            IDataTableService dataTableService,
            IConsultasPerfilObjeto consultasPerfilObjeto,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasPerfilObjeto = consultasPerfilObjeto;
            _mediator = mediator;
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Obtener(string idPerfil)
        {
            var empresas = await _consultasPerfilObjeto.Listar(idPerfil);
            return Ok(empresas);
        }
        [HttpGet("listar-menu")]
        public async Task<IActionResult> ObtenerMenu(string idModulo,string idPerfil)
        {
            var empresas = await _consultasPerfilObjeto.ListarMenuLateral(idModulo,idPerfil);
            return Ok(empresas);
        }
        [HttpGet("listar-modulos")]
        public async Task<IActionResult> ListarModulos(string idPerfil)
        {
            var empresas = await _consultasPerfilObjeto.ListarModulos(idPerfil);
            return Ok(empresas);
        }
        [HttpGet("listar-controles")]
        public async Task<IActionResult> ObtenerControles(string controles, string idPerfiles)
        {
            var empresas = await _consultasPerfilObjeto.VerificarControles(controles, idPerfiles);
            return Ok(empresas);
        }
        [HttpGet("validar-objetos")]
        public async Task<IActionResult> ValidarObjetos(string idPerfiles,string objeto)
        {
            var empresas = await _consultasPerfilObjeto.ValidarAcceso(idPerfiles,objeto);
            return Ok(empresas);
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(ComandoGuardarPerfilObjeto data)
        {
            var result = await _mediator.Send(data);
            return Ok(result);
        }
    }
}
