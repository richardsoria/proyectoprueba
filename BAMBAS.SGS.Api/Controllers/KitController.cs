﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Kit;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("Kit")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class KitController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasKit _consultasKit;
        private readonly IMediator _mediator;
        public KitController(
            IDataTableService dataTableService,
            IConsultasKit consultasKit,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasKit = consultasKit;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasKit.ObtenerDataTable(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener-tabla-articulos")]
        public async Task<IActionResult> ObtenerDataTableArticulos()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasKit.ObtenerDataTableArticulos(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasKit.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("listar-detalle")]
        public async Task<IActionResult> ListarDetalle(int id)
        {
            var empresas = await _consultasKit.ListarDetalle(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var empresas = await _consultasKit.ObtenerActivas();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoKitInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoKitActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoKitEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
