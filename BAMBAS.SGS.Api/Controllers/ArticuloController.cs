﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Articulo;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("articulo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ArticuloController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasArticulo _consultasArticulo;
        private readonly IMediator _mediator;
        public ArticuloController(
            IDataTableService dataTableService,
            IConsultasArticulo consultasArticulo,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasArticulo = consultasArticulo;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasArticulo.ObtenerDataTable(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener-tabla-activas")]
        public async Task<IActionResult> ObtenerDataTableActivas()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasArticulo.ObtenerDataTableActivas(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtenerAutocomplete")]
        public async Task<IActionResult> ObtenerAutocomplete(string term,bool flagKit)
        {
            var empresas = await _consultasArticulo.ObtenerAutocomplete(term, flagKit);
            return Ok(empresas);
        }
        [HttpGet("obtenerAutocompleteTalla")]
        public async Task<IActionResult> ObtenerAutocompleteTalla(string term)
        {
            var empresas = await _consultasArticulo.ObtenerAutocompleteTalla(term);
            return Ok(empresas);
        }
        [HttpGet("obtenerAutocompleteRestricciones")]
        public async Task<IActionResult> ObtenerAutocompleteRestricciones(string term)
        {
            var empresas = await _consultasArticulo.ObtenerAutocompleteRestricciones(term);
            return Ok(empresas);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasArticulo.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var empresas = await _consultasArticulo.ObtenerActivas();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoArticuloInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoArticuloActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoArticuloEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizacion-masiva-precios")]
        public async Task<IActionResult> ActualizacionMasivaPrecios(ComandoArticuloPrecioActualizar articulos)
        {
            var result = await _mediator.Send(articulos);
            return Ok(result);
        }

        [HttpGet("obtener-ultimos-actualizados")]
        public async Task<IActionResult> ObtenerUltimosActualizadosDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var articulo = await _consultasArticulo.ObtenerUltimosActualizadosDataTable(parameters);
            return Ok(articulo);
        }
        [HttpGet("obtener-ultima-act")]
        public async Task<IActionResult> ObtenerUltimaActualizacion()
        {
            var articulo = await _consultasArticulo.ObtenerUltimaActualizacion();
            return Ok(articulo);
        }
    }
}
