﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoDatoDetalle;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("grupo-dato-detalle")]
    public class GrupoDatoDetalleEmpresaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasGrupoDatoDetalle _consultasGrupoDatoDetalle;
        public GrupoDatoDetalleEmpresaController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasGrupoDatoDetalle consultasGrupoDatoDetalle)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasGrupoDatoDetalle = consultasGrupoDatoDetalle;
        }

        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string codigoGrupoDato)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasGrupoDatoDetalle.ObtenerDataTable(parameters,codigoGrupoDato);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasGrupoDatoDetalle.Obtener(id);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoGrupoDatoDetalleInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoGrupoDatoDetalleActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoGrupoDatoDetalleEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
