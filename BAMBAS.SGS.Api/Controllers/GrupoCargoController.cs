﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.GrupoCargo;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("grupocargo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GrupoCargoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasGrupoCargo _consultasGrupoCargo;
        private readonly IMediator _mediator;
        public GrupoCargoController(
            IDataTableService dataTableService,
            IConsultasGrupoCargo consultasGrupoCargo,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasGrupoCargo = consultasGrupoCargo;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasGrupoCargo.ObtenerDataTable(parameters);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasGrupoCargo.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var empresas = await _consultasGrupoCargo.ObtenerActivos();
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas()
        {
            var empresas = await _consultasGrupoCargo.ObtenerTodas();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoGrupoCargoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoGrupoCargoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizar-detalle")]
        public async Task<IActionResult> ActualizarDetalleDatos(ComandoGrupoCargoActualizarDetalle entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar-detalle")]
        public async Task<IActionResult> EliminarDetalleDatos(ComandoGrupoCargoEliminarDetalle entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoGrupoCargoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpGet("listar-detalle")]
        public async Task<IActionResult> ListarDetalle(int id)
        {
            var empresas = await _consultasGrupoCargo.ListarDetalle(id);
            return Ok(empresas);
        }
    }
}
