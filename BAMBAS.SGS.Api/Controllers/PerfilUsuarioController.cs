﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Perfil;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.PerfilUsuario;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("perfil-usuario")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerfilUsuarioController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasPerfilUsuario _consultasPerfilUsuario;
        private readonly IMediator _mediator;
        public PerfilUsuarioController(
            IDataTableService dataTableService,
            IConsultasPerfilUsuario consultasPerfilUsuario,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasPerfilUsuario = consultasPerfilUsuario;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idPerfil)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasPerfilUsuario.Listar(parameters, idPerfil);
            return Ok(empresas);
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(ComandoGuardarPerfilUsuario data)
        {
            var result = await _mediator.Send(data);
            return Ok(result);
        }

        [HttpGet("obtener-tabla-porusuario")]
        public async Task<IActionResult> ObtenerDataTablePorUsuario(string idUsuario)
        {
            var parameters = _dataTableService.GetSentParameters();
            var perfiles = await _consultasPerfilUsuario.ListarPorUsuario(parameters, idUsuario);
            return Ok(perfiles);
        }

        [HttpPost("guardarporusuario")]
        public async Task<IActionResult> GuardarPorUsuario(ComandoGuardarPerfilPorUsuario data)
        {
            var result = await _mediator.Send(data);
            return Ok(result);
        }
    }
}
