﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Sucursal;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.SucursalUsuario;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("sucursal-usuario")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SucursalUsuarioController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasSucursalUsuario _consultasSucursalUsuario;
        private readonly IMediator _mediator;
        public SucursalUsuarioController(
            IDataTableService dataTableService,
            IConsultasSucursalUsuario consultasSucursalUsuario,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasSucursalUsuario = consultasSucursalUsuario;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idSucursal)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasSucursalUsuario.Listar(parameters, idSucursal);
            return Ok(empresas);
        }
        [HttpGet("obtener-sucursales")]
        public async Task<IActionResult> ObtenerSucursalesDataTable(string idEmpresa,string idUsuario)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasSucursalUsuario.ObtenerSucursalesDataTable(parameters, idEmpresa, idUsuario);
            return Ok(empresas);
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(ComandoGuardarSucursalUsuario data)
        {
            var result = await _mediator.Send(data);
            return Ok(result);
        }
        [HttpPost("guardar-sucursales-usuario")]
        public async Task<IActionResult> Guardar(ComandoGuardarUsuarioSucursal data)
        {
            var result = await _mediator.Send(data);
            return Ok(result);
        }
    }
}
