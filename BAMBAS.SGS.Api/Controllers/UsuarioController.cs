﻿using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.Consultas.Objetos;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Usuario;
using BAMBAS.SGS.Servicio.Proxies.Persona;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("usuario")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UsuarioController : Controller
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasUsuario _consultasUsuario;
        private readonly IMediator _mediator;
        private readonly IPersonaProxy _personaProxy;

        public UsuarioController(
            IDataTableService dataTableService,
            IConsultasUsuario consultasUsuario,
            IMediator mediator,
            IPersonaProxy personaProxy)
        {
            _dataTableService = dataTableService;
            _consultasUsuario = consultasUsuario;
            _mediator = mediator;
            _personaProxy = personaProxy;
        }

        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var usuarios = await _consultasUsuario.ObtenerDataTable(parameters);
            return Ok(usuarios);
        }
        
        [HttpGet("listadoUsuarios")]
        public async Task<IActionResult> ListadoUsuarios()
        {
            var usuarios = await _consultasUsuario.ListadoUsuarios();
            return Ok(usuarios);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(string cusro)
        {
            var usuarios = await _consultasUsuario.Obtener(cusro);
            return Ok(usuarios);
        }

        [HttpGet("obtenerXid")]
        public async Task<IActionResult> ObtenerXid(int id)
        {
            var usuarios = await _consultasUsuario.ObtenerXid(id);
            return Ok(usuarios);
        }

        [HttpGet("obtenerPrincipal")]
        public async Task<IActionResult> ObtenerPrincipal(int id)
        {
            var usuarios = await _consultasUsuario.ObtenerXid(id);
            var Personas = await _personaProxy.ObtenerPrincipal(Convert.ToInt32(usuarios.IDPRSNA));

            var usu = new UsuarioCustom();
            usu.ID = usuarios.ID;
            usu.IDPRSNA = usuarios.IDPRSNA;
            usu.CUSRO = usuarios.CUSRO;
            usu.NYAPLLDS = usuarios.NYAPLLDS;
            usu.CLVE = usuarios.CLVE;
            usu.FVCLVE = usuarios.FVCLVE;//MMW
            //usu.FVCLVE = usuarios.FVCLVE;
            usu.FBLQUO = usuarios.FBLQUO?"1":"0";
            usu.FRZRCMBOCLVE = usuarios.FRZRCMBOCLVE ? "1" : "0";

            //Persona
            usu.APTRNO = Personas.APTRNO;
            usu.AMTRNO = Personas.AMTRNO;
            usu.ACSDA = Personas.ACSDA;
            usu.PNMBRE = Personas.PNMBRE;
            usu.SNMBRE = Personas.SNMBRE;
            usu.GDTTLFNO =  Personas.GDTTLFNO;
            usu.NTLFNO =  Personas.NTLFNO;
            usu.GDDCMNTO =  Personas.GDDCMNTO;
            usu.NDCMNTO =  Personas.NDCMNTO;
            usu.GDTCRREO =  Personas.GDTCRREO;
            usu.CCRREO = Personas.CCRREO;
            usu.GDTDRCCN = Personas.GDTDRCCN;
            usu.DRCCN = Personas.DRCCN;
            usu.FCNTRTSTA = Personas.FCNTRTSTA ? "1" : "0";

            usu.GDESTDO = usuarios.GDESTDO;
            usu.FESTDO = usuarios.FESTDO;
            usu.UEDCN = usuarios.UEDCN;
            usu.FEDCN = usuarios.FEDCN;
            usu.UCRCN = usuarios.UCRCN;
            usu.FCRCN = usuarios.FCRCN;

            return Ok(usu);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoUsuarioInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }


        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoUsuarioActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoUsuarioEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar-persona-usuario")]
        public async Task<IActionResult> ActualizarPersonaUsuario(ComandoPersonaUsuarioActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
