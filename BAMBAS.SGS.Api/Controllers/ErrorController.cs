﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Empresa;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("error")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ErrorController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasError _consultasError;
        private readonly IMediator _mediator;
        public ErrorController(
            IDataTableService dataTableService,
            IConsultasError consultasError,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasError = consultasError;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasError.ObtenerDataTable(parameters);
            return Ok(empresas);
        }
    }
}
