﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Parametro;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("parametro")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ParametroController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasParametro _consultasParametro;
        private readonly IMediator _mediator;
        public ParametroController(
            IDataTableService dataTableService,
            IConsultasParametro consultasparametro,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasParametro = consultasparametro;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasParametro.ListarParametro(parameters);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasParametro.ObtenerParametros(new Modelos.ParametroEmpresaModel { ID = id });
            return Ok(empresas);
        } 
        [HttpGet("obtener-por-abreviacion")]
        public async Task<IActionResult> ObtenerParametroPorAbreviacion(string aprmtro)
        {
            var empresas = await _consultasParametro.ObtenerParametroPorAbreviacion(aprmtro);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoParametroInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoParametroActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoParametroEliminar comando)
        {
            var result = await _mediator.Send(comando);
            return Ok(result);
        }
    }
}
