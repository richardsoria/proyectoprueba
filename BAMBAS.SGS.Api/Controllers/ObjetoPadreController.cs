﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.ObjetoPadre;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("objeto")]
    public class ObjetoPadreController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IMediator _mediator;
        private readonly IConsultasObjetoPadre _consultasObjetoPadre;
        public ObjetoPadreController(
            IDataTableService dataTableService,
            IMediator mediator,
            IConsultasObjetoPadre consultasObjetoPadre)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasObjetoPadre = consultasObjetoPadre;
        }
        [HttpGet("obtener-todos")]
        public async Task<IActionResult> ObtenerTodos()
        {
            var ObjetoPadres = await _consultasObjetoPadre.ObtenerTodos();
            return Ok(ObjetoPadres);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var ObjetoPadres = await _consultasObjetoPadre.ObtenerActivos();
            return Ok(ObjetoPadres);
        }
        [HttpGet("obtener-tabla-padre")]
        public async Task<IActionResult> ObtenerPadreDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasObjetoPadre.ObtenerPadreDataTable(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(string idobjetopadre)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasObjetoPadre.ObtenerDataTable(parameters, idobjetopadre);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasObjetoPadre.Obtener(id);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoObjetoPadreInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoObjetoPadreActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoObjetoPadreEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
