﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Area;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("area")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AreaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasArea _consultasArea;
        private readonly IMediator _mediator;
        public AreaController(
            IDataTableService dataTableService,
            IConsultasArea consultasArea,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasArea = consultasArea;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idGerencia,int idSuperintendencia)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasArea.ObtenerDataTable(parameters, idGerencia, idSuperintendencia);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasArea.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idSuperintendencia)
        {
            var empresas = await _consultasArea.ObtenerActivas(idSuperintendencia);
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idSuperintendencia)
        {
            var empresas = await _consultasArea.ObtenerTodas(idSuperintendencia);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoAreaInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoAreaActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoAreaEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
