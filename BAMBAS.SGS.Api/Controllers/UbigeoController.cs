﻿using BAMBAS.SGS.Servicio.Consultas;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("ubigeo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UbigeoController : Controller
    {
        private readonly IConsultasUbigeo _consultasUbigeo;
        public UbigeoController(IConsultasUbigeo consultasUbigeo)
        {
            _consultasUbigeo = consultasUbigeo;
        }
        [HttpGet("obtener-departamentos")]
        public async Task<IActionResult> ObtenerDepartamentos(string cps)
        {
            var Retorno = await _consultasUbigeo.ObtenerDepartamentos(cps);
            return Ok(Retorno);
        }
        [HttpGet("obtener-provincias")]
        public async Task<IActionResult> ObtenerProvincias(string codigoDpto, string cps)
        {
            var Retorno = await _consultasUbigeo.ObtenerProvincias(codigoDpto, cps);
            return Ok(Retorno);
        }
        [HttpGet("obtener-distritos")]
        public async Task<IActionResult> ObtenerDistritos(string codigoProvincia, string cps)
        {
            var Retorno = await _consultasUbigeo.ObtenerDistritos(codigoProvincia, cps);
            return Ok(Retorno);
        }
        [HttpGet("obtener-todo")]
        public async Task<IActionResult> ObtenerTodo()
        {
            var Retorno = await _consultasUbigeo.ObtenerTodo();
            return Ok(Retorno);
        }
        [HttpGet("obtener-paises")]
        public async Task<IActionResult> ObtenerPaises() {
            var Retorno = await _consultasUbigeo.ObtenerPaises();
            return Ok(Retorno);
        }
    }
}
