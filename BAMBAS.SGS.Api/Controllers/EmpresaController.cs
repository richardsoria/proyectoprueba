﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Empresa;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("empresa")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmpresaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasEmpresa _consultasEmpresa;
        private readonly IMediator _mediator;
        public EmpresaController(
            IDataTableService dataTableService,
            IConsultasEmpresa consultasEmpresa,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasEmpresa = consultasEmpresa;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasEmpresa.ObtenerDataTable(parameters);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasEmpresa.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var empresas = await _consultasEmpresa.ObtenerActivos();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoEmpresaInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoEmpresaActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoEmpresaEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
