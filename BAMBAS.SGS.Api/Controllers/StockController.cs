﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Stock;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("stock")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StockController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasStock _consultasStock;
        private readonly IMediator _mediator;
        public StockController(
            IDataTableService dataTableService,
            IConsultasStock consultasStock,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasStock = consultasStock;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idalmcn)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasStock.ObtenerDataTable(parameters, idalmcn);
            return Ok(empresas);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasStock.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var empresas = await _consultasStock.ObtenerActivas();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoStockMinimoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoStockActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoStockEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizacion-stock")]
        public async Task<IActionResult> ActualizacionMasivaStock(ComandoStockAlmacenActualizar stock)
        {
            var result = await _mediator.Send(stock);
            return Ok(result);
        }

        [HttpGet("obtener-ultimos-actualizados")]
        public async Task<IActionResult> ObtenerUltimosActualizadosDataTable(int idalmacen)
        {
            var parameters = _dataTableService.GetSentParameters();
            var stock = await _consultasStock.ObtenerUltimosActualizadosDataTable(parameters, idalmacen);
            return Ok(stock);
        }
        [HttpGet("obtener-ultima-act")]
        public async Task<IActionResult> ObtenerUltimaActualizacion(int idalmacen)
        {
            var stock = await _consultasStock.ObtenerUltimaActualizacion(idalmacen);
            return Ok(stock);
        }
    }
}
