﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.TrabajadorRestriccion;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("restricciones")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TrabajadorRestriccionController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasTrabajadorRestriccion _consultasTrabajadorRestriccion;
        private readonly IMediator _mediator;
        public TrabajadorRestriccionController(
            IDataTableService dataTableService,
            IConsultasTrabajadorRestriccion consultasTrabajadorRestriccion,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasTrabajadorRestriccion = consultasTrabajadorRestriccion;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idTrabajador)
        {
            var parameters = _dataTableService.GetSentParameters();
            var Restriccion = await _consultasTrabajadorRestriccion.ObtenerDataTable(parameters, idTrabajador);
            return Ok(Restriccion);
        }
        [HttpGet("obtener-tabla-articulos")]
        public async Task<IActionResult> ObtenerDataTableArticulos(bool flagrestrccion, int? idgrupocargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasTrabajadorRestriccion.ObtenerDataTableArticulos(parameters, flagrestrccion, idgrupocargo);
            return Ok(empresas);
        }
        [HttpGet("listar-detalle-lista")]
        public async Task<IActionResult> ListarDetalleRestriccion(int idTrabajador)
        {
            var Restriccion = await _consultasTrabajadorRestriccion.ListarDetalleRestriccion(idTrabajador);
            return Ok(Restriccion);
        }


        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoTrabajadorRestriccionActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar-detalle-restricciones")]
        public async Task<IActionResult> EliminarDetalleTalla(ComandoTrabajadorRestriccionEliminarDetalle entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

    }
}
