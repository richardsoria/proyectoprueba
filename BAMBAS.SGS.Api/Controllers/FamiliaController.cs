﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Familia;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("familia")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FamiliaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasFamilia _consultasFamilia;
        private readonly IMediator _mediator;
        public FamiliaController(
            IDataTableService dataTableService,
            IConsultasFamilia consultasFamilia,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasFamilia = consultasFamilia;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idFamilia, bool? soloHijos = false)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasFamilia.ObtenerDataTable(parameters, idFamilia,soloHijos);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasFamilia.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idFamilia)
        {
            var empresas = await _consultasFamilia.ObtenerActivas(idFamilia);
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idFamilia)
        {
            var empresas = await _consultasFamilia.ObtenerTodas(idFamilia);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas-Subfamilia")]
        public async Task<IActionResult> ObtenerActivasSubfamilia(int idFamilia,bool? soloHijos = false)
        {
            var empresas = await _consultasFamilia.ObtenerActivasSubfamilia(idFamilia,soloHijos);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoFamiliaInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoFamiliaActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoFamiliaEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpGet("obtenerAutocomplete")]
        public async Task<IActionResult> ObtenerAutocomplete(string term)
        {
            var empresas = await _consultasFamilia.ObtenerAutocomplete(term);
            return Ok(empresas);
        }
    }
}
