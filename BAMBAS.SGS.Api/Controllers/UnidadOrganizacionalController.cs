﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("unidad-organizacional")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UnidadOrganizacionalController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasUnidadOrganizacional _consultasUnidadOrganizacional;
        private readonly IMediator _mediator;
        public UnidadOrganizacionalController(
         IDataTableService dataTableService,
         IConsultasUnidadOrganizacional consultasUnidadOrganizacional,
         IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasUnidadOrganizacional = consultasUnidadOrganizacional;
            _mediator = mediator;
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var unidadOrganizacionales = await _consultasUnidadOrganizacional.ObtenerActivos();
            return Ok(unidadOrganizacionales);
        }
    }
}
