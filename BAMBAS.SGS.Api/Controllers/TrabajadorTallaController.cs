﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.TrabajadorTalla;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("TrabajadorTalla")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TrabajadorTallaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasTrabajadorTalla _consultasTrabajadorTalla;
        private readonly IMediator _mediator;
        public TrabajadorTallaController(
            IDataTableService dataTableService,
            IConsultasTrabajadorTalla consultasTrabajadorTalla,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasTrabajadorTalla = consultasTrabajadorTalla;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idTrabajador)
        {
            var parameters = _dataTableService.GetSentParameters();
            var talla = await _consultasTrabajadorTalla.ObtenerDataTable(parameters, idTrabajador);
            return Ok(talla);
        }
        [HttpGet("obtener-tabla-articulos")]
        public async Task<IActionResult> ObtenerDataTableArticulos(bool flagtlla, int? idgrupocargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasTrabajadorTalla.ObtenerDataTableArticulos(parameters, flagtlla, idgrupocargo);
            return Ok(empresas);
        }
        [HttpGet("listar-detalle-lista")]
        public async Task<IActionResult> ListarDetalleTalla(int idTrabajador)
        {
            var talla = await _consultasTrabajadorTalla.ListarDetalleTalla(idTrabajador);
            return Ok(talla);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoTrabajadorTallaActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("eliminar-detalle-talla")]
        public async Task<IActionResult> EliminarDetalleTalla(ComandoTrabajadorTallaEliminarDetalle entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }


    }
}
