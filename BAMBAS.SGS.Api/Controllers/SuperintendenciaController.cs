﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Superintendencia;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("superintendencia")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SuperintendenciaController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasSuperintendencia _consultasSuperintendencia;
        private readonly IMediator _mediator;
        public SuperintendenciaController(
            IDataTableService dataTableService,
            IConsultasSuperintendencia consultasSuperintendencia,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasSuperintendencia = consultasSuperintendencia;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idGerencia)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasSuperintendencia.ObtenerDataTable(parameters, idGerencia);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasSuperintendencia.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idGerencia)
        {
            var empresas = await _consultasSuperintendencia.ObtenerActivas(idGerencia);
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idGerencia)
        {
            var empresas = await _consultasSuperintendencia.ObtenerTodas(idGerencia);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoSuperintendenciaInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoSuperintendenciaActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoSuperintendenciaEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
