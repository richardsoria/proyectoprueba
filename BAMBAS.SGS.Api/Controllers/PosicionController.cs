﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("posicion")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PosicionController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasPosicion _consultasPosicion;
        private readonly IMediator _mediator;
        public PosicionController(
         IDataTableService dataTableService,
         IConsultasPosicion consultasPosicion,
         IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasPosicion = consultasPosicion;
            _mediator = mediator;
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var posiciones = await _consultasPosicion.ObtenerActivos();
            return Ok(posiciones);
        }
    }
}
