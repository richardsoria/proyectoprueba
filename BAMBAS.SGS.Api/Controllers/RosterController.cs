﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Roster;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("roster")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RosterController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasRoster _consultasRoster;
        private readonly IMediator _mediator;
        public RosterController(
            IDataTableService dataTableService,
            IConsultasRoster consultasRoster,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasRoster = consultasRoster;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasRoster.ObtenerDataTable(parameters);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasRoster.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var empresas = await _consultasRoster.ObtenerActivos();
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas()
        {
            var empresas = await _consultasRoster.ObtenerTodas();
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoRosterInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoRosterActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoRosterEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
