﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{
    [ApiController]
    [Route("centro-costo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CentroCostoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasCentroCosto _consultasCentroCosto;
        private readonly IMediator _mediator;
        public CentroCostoController(
         IDataTableService dataTableService,
         IConsultasCentroCosto consultasCentroCosto,
         IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasCentroCosto = consultasCentroCosto;
            _mediator = mediator;
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var centroCostos = await _consultasCentroCosto.ObtenerActivos();
            return Ok(centroCostos);
        }
    }
}
