﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.SGS.Servicio.Consultas;
using BAMBAS.SGS.Servicio.ControladorEventos.Comandos.Cargo;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.Api.Controllers
{                                                                                    
    [ApiController]
    [Route("cargo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CargoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasCargo _consultasCargo;
        private readonly IMediator _mediator;
        public CargoController(
            IDataTableService dataTableService,
            IConsultasCargo consultasCargo,
            IMediator mediator)
        {
            _dataTableService = dataTableService;
            _consultasCargo = consultasCargo;
            _mediator = mediator;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idGrupoCargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _consultasCargo.ObtenerDataTable(parameters, idGrupoCargo);
            return Ok(empresas);
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var empresas = await _consultasCargo.Obtener(id);
            return Ok(empresas);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos(int idGrupoCargo)
        {
            var empresas = await _consultasCargo.ObtenerActivos(idGrupoCargo);
            return Ok(empresas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idGrupoCargo)
        {
            var empresas = await _consultasCargo.ObtenerTodas(idGrupoCargo);
            return Ok(empresas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarDatos(ComandoCargoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> ActualizarDatos(ComandoCargoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> EliminarDatos(ComandoCargoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
