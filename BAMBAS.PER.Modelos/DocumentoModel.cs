﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.PER.Modelos
{
    [BaseDatos(esquema = "PER", nombreTabla = "MPEPE04")]
    public class DocumentoModel : EntidadAuditoria
    {
        public string IGDDCMNTO { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string FINSCRPCN { get; set; }
        public string FVNCMNTO { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
        public DocumentoModel Entidad { get; set; }
    }
}
