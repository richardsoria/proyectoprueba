﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;

namespace BAMBAS.PER.Modelos
{
    [BaseDatos(esquema = "PER", nombreTabla = "MPEPE00")]
    public class PersonaModel : EntidadAuditoria
    {
        public string GDTPRSNA { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string RSCL { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public string PUSUARIO { get; set; }
        public string NCNLDD { get; set; }
        public string GDECVL { get; set; }
        public string GDSXO { get; set; }
        public string FNCMNTO { get; set; }
        public string FFLLCMNTO { get; set; }
        public string CUBGEO { get; set; }
        public bool FCNTRTSTA { get; set; }
    }
}








