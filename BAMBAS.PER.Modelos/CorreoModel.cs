﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.PER.Modelos
{
    [BaseDatos(esquema = "PER", nombreTabla = "MPEPE03")]
    public class CorreoModel : EntidadAuditoria
    {
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
