﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;

namespace BAMBAS.PER.Modelos
{
    [BaseDatos(esquema = "PER", nombreTabla = "MPEPE02")]
    public class TelefonoModel : EntidadAuditoria
    {
        public string GDTTLFNO { get; set; }
        public string CUBGEO { get; set; }
        public string NTLFNO { get; set; }
        public string OBSRVCN { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
