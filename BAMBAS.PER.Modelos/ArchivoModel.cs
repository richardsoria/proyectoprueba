﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.PER.Modelos
{
    [BaseDatos(esquema = "PER", nombreTabla = "MPEPE05")]
    public class ArchivoModel : EntidadAuditoria
    {
        public string NMBRARCHV { get; set; }
        public string PATHARCHV { get; set; }
        public string TPOARCHV { get; set; }
        public string FPRNCPL { get; set; }
        public int IDPRSNA { get; set; }
    }
}
