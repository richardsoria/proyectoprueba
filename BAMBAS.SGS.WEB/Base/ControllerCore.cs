﻿using BAMBAS.CONFIG;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
namespace BAMBAS.SGS.WEB.Base
{
    public class ControllerCore : Microsoft.AspNetCore.Mvc.Controller
    {
        public override RedirectResult Redirect(string url)
        {
            if (url.Contains("https://") || url.Contains("http://"))
                return base.Redirect(url);
            return base.Redirect($"/{ConfiguracionProyecto.CARPETAS_DESPLIEGUE.Seguridad_Web}{url}");
        }
    }
}
