using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.General.Persona;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace BAMBAS.SGS.WEB.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddRepository(this IServiceCollection serviceCollection)
        {
        }
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            //Base
            //serviceCollection.AddTransient<IEmailService, EmailService>();
            //serviceCollection.AddTransient<IEmailTemplateService, EmailTemplateService>();
            serviceCollection.AddTransient<ISelect2Service, Select2Service>();
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IPaginationService, PaginationService>();
            serviceCollection.AddTransient<IViewRenderService, ViewRenderService>();
            //serviceCollection.AddTransient<ICloudStorageService, CloudStorageService>(); 
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);


            //proxies
            serviceCollection.AddHttpClient<IGrupoDatoProxy, GrupoDatoProxy>();
            serviceCollection.AddHttpClient<IEmpresaProxy, EmpresaProxy>();
            serviceCollection.AddHttpClient<ISucursalProxy, SucursalProxy>();
            serviceCollection.AddHttpClient<IGrupoDatoDetalleProxy, GrupoDatoDetalleProxy>();
            serviceCollection.AddHttpClient<IErrorProxy, ErrorProxy>();
            serviceCollection.AddHttpClient<IParametroProxy, ParametroProxy>();
            serviceCollection.AddHttpClient<IPerfilProxy, PerfilProxy>();
            serviceCollection.AddHttpClient<IUbigeoProxy, UbigeoProxy>();
            serviceCollection.AddHttpClient<IUsuarioProxy, UsuarioProxy>();

            serviceCollection.AddHttpClient<IPersonaProxy, PersonaProxy>();
            serviceCollection.AddHttpClient<ITrabajadorProxy, TrabajadorProxy>();
            serviceCollection.AddHttpClient<IPerfilUsuarioProxy, PerfilUsuarioProxy>();
            serviceCollection.AddHttpClient<ISucursalUsuarioProxy, SucursalUsuarioProxy>();
            serviceCollection.AddHttpClient<IPerfilObjetoProxy, PerfilObjetoProxy>();
            serviceCollection.AddHttpClient<IObjetoPadreProxy, ObjetoPadreProxy>();
            serviceCollection.AddHttpClient<IObjetoDetalleProxy, ObjetoDetalleProxy>();

            serviceCollection.AddHttpClient<IGerenciaProxy, GerenciaProxy>();
            serviceCollection.AddHttpClient<ISuperintendenciaProxy, SuperintendenciaProxy>();
            serviceCollection.AddHttpClient<IAreaProxy, AreaProxy>();
            serviceCollection.AddHttpClient<IRosterProxy, RosterProxy>(); 
            serviceCollection.AddHttpClient<IGuardiaProxy, GuardiaProxy>();
            serviceCollection.AddHttpClient<IGrupoCargoProxy, GrupoCargoProxy>();
            serviceCollection.AddHttpClient<ICargoProxy, CargoProxy>();
            serviceCollection.AddHttpClient<IFamiliaProxy, FamiliaProxy>();
            serviceCollection.AddHttpClient<IKitProxy, KitProxy>();
            serviceCollection.AddHttpClient<IAlmacenProxy, AlmacenProxy>();
            serviceCollection.AddHttpClient<IStockProxy, StockProxy>();
            serviceCollection.AddHttpClient<IArticuloProxy, ArticuloProxy>();
            serviceCollection.AddHttpClient<ITrabajadorTallaProxy, TrabajadorTallaProxy>();

            serviceCollection.AddHttpClient<IPosicionProxy, PosicionProxy>();
            serviceCollection.AddHttpClient<ICentroCostoProxy, CentroCostoProxy>();
            serviceCollection.AddHttpClient<IUnidadOrganizacionalProxy, UnidadOrganizacionalProxy>();
            

            serviceCollection.AddScoped<AuthLogin>();
        }
    }
}
