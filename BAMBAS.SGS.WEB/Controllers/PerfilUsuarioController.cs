﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.PerfilUsuario;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [ApiController]
    [Route("perfil-usuario")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class PerfilUsuarioController : Controller
    {
        private readonly IDataTableService _dataTableService;
        private readonly IPerfilUsuarioProxy _perfilUsuarioProxy;

        public PerfilUsuarioController(IDataTableService dataTableService,
            IPerfilUsuarioProxy perfilUsuarioProxy)
        {
            _dataTableService = dataTableService;
            _perfilUsuarioProxy = perfilUsuarioProxy;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("obtener-usuarios")]
        public async Task<IActionResult> ObtenerUsuarios(string idPerfil)
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _perfilUsuarioProxy.ObtenerDataTable(parameters,idPerfil);
            return Ok(retorno); ;
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(GuardarPerfilUsuarioCustom data)
        {
            data.UCRCN = User.GetUserCode();
            var retorno = await _perfilUsuarioProxy.GuardarPerfilesUsuario(data);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }

        [HttpGet("obtener-perfiles")]
        public async Task<IActionResult> ObtenerPerfilPorUsuario(string idUsuario)
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _perfilUsuarioProxy.ObtenerDataTablePorUsuario(parameters, idUsuario);
            return Ok(retorno); ;
        }

        [HttpPost("guardarporusuario")]
        public async Task<IActionResult> GuardarPorUsuario(GuardarPerfilPorUsuarioCustom data)
        {
            data.UCRCN = User.GetUserCode();
            var retorno = await _perfilUsuarioProxy.GuardarPerfilesPorUsuario(data);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
