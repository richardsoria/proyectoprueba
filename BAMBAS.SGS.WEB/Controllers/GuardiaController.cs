﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Guardia;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("guardia")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     
                                                                                 
    public class GuardiaController : Controller
    {
        private readonly IGuardiaProxy _guardiaProxy;
        private readonly IDataTableService _dataTableService;
        public GuardiaController(
            IDataTableService dataTableService,
            IGuardiaProxy guardiaProxy)
        {
            _dataTableService = dataTableService;
            _guardiaProxy = guardiaProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var guardia = await _guardiaProxy.ObtenerDataTable(parameters);
            return Ok(guardia);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var guardia = await _guardiaProxy.ObtenerActivas();
            return Ok(guardia);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(GuardiaDto guardia)
        {
            guardia.UCRCN = User.GetUserCode();
            var ret = await _guardiaProxy.Insertar(guardia);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(GuardiaDto guardia)
        {
            guardia.UEDCN = User.GetUserCode();
            var ret = await _guardiaProxy.Actualizar(guardia);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var guardia = await _guardiaProxy.Obtener(id);
            var model = new GuardiaDto
            {
                GDTRNO= guardia.GDTRNO,
                ID = guardia.ID,
                DSCRPCN = guardia.DSCRPCN,
                IDRSTR = guardia.IDRSTR,
                TRNO = guardia.TRNO,
                FINCO = guardia.FINCO,
                DSGRDA = guardia.DSGRDA,
                HRAINCODA = guardia.HRAINCODA,
                HRAFNDA = guardia.HRAFNDA,
                NCHSGRDA = guardia.NCHSGRDA,
                HRAINCONCHE = guardia.HRAINCONCHE,
                HRAFNNCHE = guardia.HRAFNNCHE,
                DDSCNSO = guardia.DDSCNSO,
                //
                GDESTDO = guardia.GDESTDO,
                FCRCN = guardia.FCRCN,
                FEDCN = guardia.FEDCN,
                FESTDO = guardia.FESTDO,
                UCRCN = guardia.UCRCN,
                UEDCN = guardia.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new GuardiaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _guardiaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
