﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.ObjetoDetalle;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("objeto-detalle")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class ObjetoDetalleController : Controller
    {
        private readonly IObjetoDetalleProxy _ObjetoDetalleProxy;
        private readonly IDataTableService _dataTableService;
        public ObjetoDetalleController(
            IDataTableService dataTableService,
            IObjetoDetalleProxy ObjetoDetalleProxy
            )
        {
            _dataTableService = dataTableService;
            _ObjetoDetalleProxy = ObjetoDetalleProxy;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerObjetoDetalleDataTable(string idObjeto)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _ObjetoDetalleProxy.ObtenerDataTable(parameters, idObjeto);
            return Ok(empresas);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarObjetoDetalle(ObjetoDetalleDto grupo)
        {
            grupo.UCRCN = User.GetUserCode();
            var retorno = await _ObjetoDetalleProxy.Insertar(grupo);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ObjetoDetalleDto grupo)
        {
            grupo.UEDCN = User.GetUserCode();
            var retorno = await _ObjetoDetalleProxy.Actualizar(grupo);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var grupo = await _ObjetoDetalleProxy.Obtener(id);
            var model = new ObjetoDetalleDto
            {
                NOBJTO = grupo.NOBJTO,
                IDOBJTO = grupo.IDOBJTO,
                DOBJTO = grupo.DOBJTO,
                //
                UEDCN = grupo.UEDCN,
                UCRCN = grupo.UCRCN,
                GDESTDO = grupo.GDESTDO,
                FCRCN = grupo.FCRCN,
                FEDCN = grupo.FEDCN,
                FESTDO = grupo.FESTDO,
                ID = grupo.ID
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var empresa = new ObjetoDetalleDto(); //await _ObjetoDetalleProxy.Obtener(id);
            empresa.ID = id;
            empresa.UEDCN = User.GetUserCode();
            var retorno = await _ObjetoDetalleProxy.Eliminar(empresa);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
