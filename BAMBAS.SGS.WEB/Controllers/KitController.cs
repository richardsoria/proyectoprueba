﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Kit;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("kit")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class KitController : Controller
    {
        private readonly IKitProxy _kitProxy;
        private readonly IDataTableService _dataTableService;
        public KitController(
            IDataTableService dataTableService,
            IKitProxy kitProxy)
        {
            _dataTableService = dataTableService;
            _kitProxy = kitProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("agregar")]
        [HttpGet("editar/{id}")]
        public IActionResult AddKit(int? id = null)
        {
            return View(id);
        }
        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var kit = await _kitProxy.ObtenerDataTable(parameters);
            return Ok(kit);
        }
        [HttpGet("get-articulos")]
        public async Task<IActionResult> ObtenerDataTableArticulos()
        {
            var parameters = _dataTableService.GetSentParameters();
            var kit = await _kitProxy.ObtenerDataTableArticulos(parameters);
            return Ok(kit);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var retorno = await _kitProxy.Obtener(id);
            return Ok(retorno); ;
        }
        [HttpGet("listar-detalle")]
        public async Task<IActionResult> ListarDetalle(int id)
        {
            var retorno = await _kitProxy.ListarDetalle(id);
            return Ok(retorno); ;
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var kit = await _kitProxy.ObtenerActivas();
            return Ok(kit);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(KitDto kit)
        {
            kit.UCRCN = User.GetUserCode();
            var ret = await _kitProxy.Insertar(kit);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok(ret.CodEstado);
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(KitDto kit)
        {
            kit.UEDCN = User.GetUserCode();
            var ret = await _kitProxy.Actualizar(kit);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
       
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new KitDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _kitProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
