﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Modelos.Seguridad.Autocomplete;
using BAMBAS.Negocios.Seguridad;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("articulo")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class ArticuloController : Controller
    {
        private readonly IArticuloProxy _articuloProxy;
        private readonly IDataTableService _dataTableService;
        public ArticuloController(
            IDataTableService dataTableService,
            IArticuloProxy articuloProxy)
        {
            _dataTableService = dataTableService;
            _articuloProxy = articuloProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("actualizar-precios")]
        public async Task<IActionResult> ActualizacionPrecios()
        {
            return View();
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(string term, bool flagKit)
        {
            var articulo = await _articuloProxy.ObtenerAutocomplete(term, flagKit);
            var lista = new List<AutocompleteDto>();

            foreach (var item in articulo)
            {
                var model = new AutocompleteDto()
                {
                    value = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    label = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    value2 = (item.ID.ToString() + " - " + item.GDUNDDMDDA + " - " + item.CSTO + " - " + item.DRCN),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }
        [HttpGet("searchcontalla")]
        public async Task<IActionResult> Searchcontalla(string term)
        {
            var articulo = await _articuloProxy.ObtenerAutocompleteTalla(term);
            var lista = new List<AutocompleteDto>();

            foreach (var item in articulo)
            {
                var model = new AutocompleteDto()
                {
                    value = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    label = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    value2 = (item.ID.ToString() + " - " + item.GDUNDDMDDA),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }
        [HttpGet("searchconrestricciones")]
        public async Task<IActionResult> Searchconrestricciones(string term)
        {
            var articulo = await _articuloProxy.ObtenerAutocompleteRestricciones(term);
            var lista = new List<AutocompleteDto>();

            foreach (var item in articulo)
            {
                var model = new AutocompleteDto()
                {
                    value = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    label = (item.CSAP + " - " + item.DSCRPCN).ToString(),
                    value2 = (item.ID.ToString() + " - " + item.GDUNDDMDDA),
                };
                lista.Add(model);
            };
            return Ok(lista);
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var articulo = await _articuloProxy.ObtenerDataTable(parameters);
            return Ok(articulo);
        }
        [HttpGet("get-activas")]
        public async Task<IActionResult> ObtenerDataTableActivas()
        {
            var parameters = _dataTableService.GetSentParameters();
            var articulo = await _articuloProxy.ObtenerDataTableActivas(parameters);
            return Ok(articulo);
        }

        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var articulo = await _articuloProxy.ObtenerActivas();
            return Ok(articulo);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ArticuloDto articulo)
        {
            articulo.UCRCN = User.GetUserCode();
            var ret = await _articuloProxy.Insertar(articulo);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ArticuloDto articulo)
        {
            articulo.UEDCN = User.GetUserCode();
            var ret = await _articuloProxy.Actualizar(articulo);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var articulo = await _articuloProxy.Obtener(id);
            var model = new ArticuloDto
            {
                DSCRPCN = articulo.DSCRPCN,
                ID = articulo.ID,
                //
                IDFMLA = articulo.IDFMLA,
                IDSBFMLA = articulo.IDSBFMLA,
                CBRRA = articulo.CBRRA,
                CSAP = articulo.CSAP,
                GDUNDDMDDA = articulo.GDUNDDMDDA,
                GDTMNDA = articulo.GDTMNDA,
                DRCN = articulo.DRCN,
                CSTO = articulo.CSTO,
                FKIT = articulo.FKIT,
                FMNJOTLLS = articulo.FMNJOTLLS,
                FMNJORSTRCN = articulo.FMNJORSTRCN,
                MNSJE = articulo.MNSJE,
                //
                GDESTDO = articulo.GDESTDO,
                FCRCN = articulo.FCRCN,
                FEDCN = articulo.FEDCN,
                FESTDO = articulo.FESTDO,
                UCRCN = articulo.UCRCN,
                UEDCN = articulo.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new ArticuloDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _articuloProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("cargar-excel")]
        public async Task<IActionResult> CargarExcel(IFormFile archivo)
        {
            #region Variable Declaration
            DataSet dsexcelRecords = new DataSet();
            IExcelDataReader reader = null;
            //HttpPostedFile Inputfile = null;
            Stream FileStream = null;
            #endregion

            #region Save Student Detail From Excel
            try
            {
                if (archivo != null)
                {
                    var Inputfile = archivo;
                    FileStream = Inputfile.OpenReadStream();

                    if (Inputfile != null && FileStream != null)
                    {
                        if (Inputfile.FileName.EndsWith(".xls"))
                            reader = ExcelReaderFactory.CreateBinaryReader(FileStream);
                        else if (Inputfile.FileName.EndsWith(".xlsx"))
                            reader = ExcelReaderFactory.CreateOpenXmlReader(FileStream);
                        else
                            return BadRequest("El formato del archivo no es válido.");

                        dsexcelRecords = reader.AsDataSet();
                        reader.Close();

                        if (dsexcelRecords != null && dsexcelRecords.Tables.Count > 0)
                        {
                            DataTable dtStudentRecords = dsexcelRecords.Tables[0];
                            var articulos = new List<ArticuloDto>();
                            for (int i = 1; i < dtStudentRecords.Rows.Count; i++)
                            {
                                var articulo = new ArticuloDto();
                                articulo.CSAP = Convert.ToString(dtStudentRecords.Rows[i][0]);
                                articulo.DSCRPCN = Convert.ToString(dtStudentRecords.Rows[i][1]);
                                articulo.CSTO = Convert.ToDecimal(dtStudentRecords.Rows[i][2]);
                                articulos.Add(articulo);
                            }
                            var entidad = new ActualizacionMasivaPreciosDto();
                            entidad.UEDCN = User.GetUserCode();
                            entidad.Articulos = articulos;
                            var retorno = await _articuloProxy.ActualizacionMasivaPrecios(entidad);
                            if (!retorno.EsSatisfactoria)
                                return BadRequest(retorno.Mensaje);
                            return Ok();
                        }
                        else
                            return BadRequest("El archivo seleccionado está vacío");
                    }
                    else
                        return BadRequest("No ha seleccionado un archivo.");
                }
                else
                    return BadRequest("No ha seleccionado un archivo.");
            }
            catch (Exception)
            {
                throw new Exception("Hay celdas con formatos incorrectos.");
            }
            #endregion
        }

        #region LOG DE ACTUALIZACION DE PRECIOS DE ARTICULOS
        [HttpGet("obtener-ultimos-atualizados")]
        public async Task<IActionResult> ObtenerUltimosActualizadosDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var articulo = await _articuloProxy.ObtenerUltimosActualizadosDataTable(parameters);
            return Ok(articulo);
        }
        [HttpGet("obtener-ultima-act")]
        public async Task<IActionResult> ObtenerUltimaActualizacion()
        {
            var articulo = await _articuloProxy.ObtenerUltimaActualizacion();
            var model = new ArticuloDto
            {
                FEDCN = articulo.FEDCN,
                UEDCN = articulo.UEDCN
            };
            return Ok(model);
        }
        #endregion
    }
}
