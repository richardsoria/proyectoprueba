﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Superintendencia;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("superintendencia")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class SuperintendenciaController : Controller
    {
        private readonly ISuperintendenciaProxy _superintendenciaProxy;
        private readonly IDataTableService _dataTableService;
        public SuperintendenciaController(
            IDataTableService dataTableService,
            ISuperintendenciaProxy superintendenciaProxy)
        {
            _dataTableService = dataTableService;
            _superintendenciaProxy = superintendenciaProxy;
        }
        [HttpGet("")]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(int idGerencia)
        {
            var parameters = _dataTableService.GetSentParameters();
            var superintendencias = await _superintendenciaProxy.ObtenerDataTable(parameters, idGerencia);
            return Ok(superintendencias);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idGerencia)
        {
            var superintendencias = await _superintendenciaProxy.ObtenerActivas(idGerencia);
            return Ok(superintendencias);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idGerencia)
        {
            var superintendencias = await _superintendenciaProxy.ObtenerTodas(idGerencia);
            return Ok(superintendencias);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(SuperintendenciaDto superintendencia)
        {
            superintendencia.UCRCN = User.GetUserCode();
            var ret = await _superintendenciaProxy.Insertar(superintendencia);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(SuperintendenciaDto superintendencia)
        {
            superintendencia.UEDCN = User.GetUserCode();
            var ret = await _superintendenciaProxy.Actualizar(superintendencia);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var superintendencia = await _superintendenciaProxy.Obtener(id);
            var model = new SuperintendenciaDto
            {
                DSCRPCN = superintendencia.DSCRPCN,
                ID = superintendencia.ID,
                IDGRNCA=superintendencia.IDGRNCA,
                //
                GDESTDO = superintendencia.GDESTDO,
                FCRCN = superintendencia.FCRCN,
                FEDCN = superintendencia.FEDCN,
                FESTDO = superintendencia.FESTDO,
                UCRCN = superintendencia.UCRCN,
                UEDCN = superintendencia.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new SuperintendenciaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _superintendenciaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
