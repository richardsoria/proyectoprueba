﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Sucursal;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("sucursal")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class SucursalController : Controller
    {
        private readonly ISucursalProxy _sucursalServ;
        private readonly IDataTableService _dataTableService;
        public SucursalController(
            IDataTableService dataTableService,
            ISucursalProxy sucursalServ)
        {
            _dataTableService = dataTableService;
            _sucursalServ = sucursalServ;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> ObtenerDataTable(int empresaId)
        {
            var parameters = _dataTableService.GetSentParameters();
            var sucursales = await _sucursalServ.ObtenerDataTable(parameters, empresaId);
            return Ok(sucursales);
        }

        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(SucursalDto sucursal)
        {
            RespuestaConsulta retorno;
            if (!sucursal.ID.HasValue)
            {
                sucursal.UCRCN = User.GetUserCode();
                retorno = await _sucursalServ.Insertar(sucursal);
            }
            else
            {
                sucursal.UEDCN = User.GetUserCode();
                retorno = await _sucursalServ.Actualizar(sucursal);
            }
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var sucursal = await _sucursalServ.Obtener(id);
            var model = new SucursalDto
            {
                ID = sucursal.ID,
                IDEMPRSA = sucursal.IDEMPRSA,
                NSCRSL = sucursal.NSCRSL,
                DSCRSL = sucursal.DSCRSL,
                UBGO = sucursal.UBGO,
                //
                GDESTDO = sucursal.GDESTDO,
                FCRCN = sucursal.FCRCN,
                FEDCN = sucursal.FEDCN,
                FESTDO = sucursal.FESTDO,
                UCRCN = sucursal.UCRCN,
                UEDCN = sucursal.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {

            var sucursal = new SucursalDto();
            sucursal.ID = id;
            sucursal.UEDCN = User.GetUserCode();
            var retorno = await _sucursalServ.Eliminar(sucursal);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
