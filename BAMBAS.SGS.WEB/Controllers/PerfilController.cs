﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Perfil;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("Perfil")]
    [Authorize]
   // [ServiceFilter(typeof(AuthLogin))]

    public class PerfilController :Controller
    {
        private readonly IPerfilProxy _perfilProxy;
        private readonly IDataTableService _dataTableService;

        public PerfilController(IPerfilProxy perfilServ, IDataTableService dataTableService)
        {
            _perfilProxy = perfilServ;
            _dataTableService = dataTableService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarPerfil")]
        public async Task<IActionResult> ListarPerfil()
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _perfilProxy.ObtenerDataTable(parameters);
            return Ok(retorno); ;
        }
        [HttpGet("ListarPerfiles")]
        public async Task<IActionResult> ListarPerfiles()
        {
            var retorno = await _perfilProxy.Listar();
            return Ok(retorno); ;
        }
        [HttpPost("ObtenerPerfil")]
        public async Task<IActionResult> ObtenerPerfil(int id)
        {
            var retorno = await _perfilProxy.Obtener(id);
            return Ok(retorno); ;
        }
        [HttpPost("InsertarPerfil")]
        public async Task<IActionResult> InsertarPerfil(PerfilDto entidad)
        {
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _perfilProxy.Insertar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("ActualizarPerfil")]
        public async Task<IActionResult> ActualizarPerfil(PerfilDto entidad)
        {
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _perfilProxy.Actualizar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("EliminarPerfil")]
        public async Task<IActionResult> EliminarPerfil(int id)
        {
            var entidad = new PerfilDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _perfilProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
