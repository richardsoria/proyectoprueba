﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoDatoDetalle;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("grupo-dato-detalle")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class GrupoDatoDetalleController : Controller
    {
        private readonly IGrupoDatoDetalleProxy _grupoDatoDetalleProxy;
        private readonly IDataTableService _dataTableService;
        public GrupoDatoDetalleController(
            IDataTableService dataTableService,
            IGrupoDatoDetalleProxy grupoDatoDetalleProxy
            )
        {
            _dataTableService = dataTableService;
            _grupoDatoDetalleProxy = grupoDatoDetalleProxy;
        }


        [HttpGet("obtener-grupodatodetalle")]
        public async Task<IActionResult> ObtenerGrupoDatoDetalleDataTable(string codigoGrupoDato)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _grupoDatoDetalleProxy.ObtenerDataTable(parameters, codigoGrupoDato);
            return Ok(empresas);
        }

        [HttpPost("guardar-grupo-dato-detalle")]
        public async Task<IActionResult> GuardarGrupoDatoDetalle(GrupoDatoDetalleDto grupo)
        {
            RespuestaConsulta retorno;
            if (!grupo.ID.HasValue)
            {
                grupo.UCRCN = User.GetUserCode();
                retorno = await _grupoDatoDetalleProxy.Insertar(grupo);
            }
            else
            {
                grupo.UEDCN = User.GetUserCode();
                retorno = await _grupoDatoDetalleProxy.Actualizar(grupo);
            }
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("editar-detalle/{id}")]
        public async Task<IActionResult> EditarDetalle(int id)
        {
            var grupo = await _grupoDatoDetalleProxy.Obtener(id);
            var model = new GrupoDatoDetalleDto
            {
                CGDTO = grupo.CGDTO,
                AGDDTLLE = grupo.AGDDTLLE,
                VLR1 = grupo.VLR1,
                VLR2 = grupo.VLR2,
                DGDDTLLE = grupo.DGDDTLLE,
                //
                UEDCN = grupo.UEDCN,
                UCRCN = grupo.UCRCN,
                GDESTDO = grupo.GDESTDO,
                FCRCN = grupo.FCRCN,
                FEDCN = grupo.FEDCN,
                FESTDO = grupo.FESTDO,
                ID = grupo.ID
            };
            return Ok(model);
        }
        [HttpPost("eliminar-detalle")]
        public async Task<IActionResult> EliminarDetalle(int id)
        {
            var empresa = new GrupoDatoDetalleDto();// _grupoDatoDetalleProxy.Obtener(id);
            empresa.ID = id;
            empresa.UEDCN = User.GetUserCode();
            var retorno = await _grupoDatoDetalleProxy.Eliminar(empresa);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
