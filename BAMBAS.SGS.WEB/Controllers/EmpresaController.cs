﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Empresa;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using BAMBAS.SGS.WEB.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("empresa")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class EmpresaController : Controller
    {
        private readonly IEmpresaProxy _empresaProxy;
        private readonly IDataTableService _dataTableService;
        public EmpresaController(
            IDataTableService dataTableService,
            IEmpresaProxy empresaProxy)
        {
            _dataTableService = dataTableService;
            _empresaProxy = empresaProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _empresaProxy.ObtenerDataTable(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var empresas = await _empresaProxy.ObtenerActivos();
            return Ok(empresas);
        }

        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(EmpresaDto empresa)
        {
            RespuestaConsulta ret;
            if (!empresa.ID.HasValue)
            {
                empresa.UCRCN = User.GetUserCode();
                ret = await _empresaProxy.Insertar(empresa);
            }
            else
            {
                empresa.UEDCN = User.GetUserCode();
                ret = await _empresaProxy.Actualizar(empresa);
            }
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var empresa = await _empresaProxy.Obtener(id);
            var model = new EmpresaDto
            {
                DRCCN = empresa.DRCCN,
                ID = empresa.ID,
                NEMPRSA = empresa.NEMPRSA,
                RUC = empresa.RUC,
                UBGO = empresa.UBGO,
                //
                GDESTDO = empresa.GDESTDO,
                FCRCN = empresa.FCRCN,
                FEDCN = empresa.FEDCN,
                FESTDO = empresa.FESTDO,
                UCRCN = empresa.UCRCN,
                UEDCN = empresa.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new EmpresaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _empresaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
