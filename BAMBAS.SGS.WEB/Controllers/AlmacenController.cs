﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Almacen;
using BAMBAS.Negocios.Modelos.Seguridad.Stock;
using BAMBAS.Negocios.Seguridad;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("almacen")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class AlmacenController : Controller
    {
        private readonly IAlmacenProxy _almacenProxy;
        private readonly IDataTableService _dataTableService;

        public AlmacenController(
            IDataTableService dataTableService,
            IAlmacenProxy almacenProxy)
        {
            _dataTableService = dataTableService;
            _almacenProxy = almacenProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("stock/{id}")]
        public async Task<IActionResult> Stock(int id)
        {
            return View(id);
        }
        [HttpGet("actualizar-stock")]
        public async Task<IActionResult> ActualizacionAlmacen(int id)
        {
            return View(id);
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(string gdtalmcn)
        {
            var parameters = _dataTableService.GetSentParameters();
            var almacen = await _almacenProxy.ObtenerDataTable(parameters, gdtalmcn);
            return Ok(almacen);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var almacen = await _almacenProxy.ObtenerActivas();
            return Ok(almacen);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(AlmacenDto almacen)
        {
            almacen.UCRCN = User.GetUserCode();
            var ret = await _almacenProxy.Insertar(almacen);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(AlmacenDto almacen)
        {
            almacen.UEDCN = User.GetUserCode();
            var ret = await _almacenProxy.Actualizar(almacen);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var almacen = await _almacenProxy.Obtener(id);
            var model = new AlmacenDto
            {
                DSCRPCN = almacen.DSCRPCN,
                GDTALMCN = almacen.GDTALMCN,
                ID = almacen.ID,
                IDTRBJDR = almacen.IDTRBJDR,
                TRBJDR = almacen.TRBJDR,
                //
                GDESTDO = almacen.GDESTDO,
                FCRCN = almacen.FCRCN,
                FEDCN = almacen.FEDCN,
                FESTDO = almacen.FESTDO,
                UCRCN = almacen.UCRCN,
                UEDCN = almacen.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new AlmacenDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _almacenProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
        

