﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Area;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("area")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class AreaController : Controller
    {
        private readonly IAreaProxy _areaProxy;
        private readonly IDataTableService _dataTableService;
        public AreaController(
            IDataTableService dataTableService,
            IAreaProxy areaProxy)
        {
            _dataTableService = dataTableService;
            _areaProxy = areaProxy;
        }
        [HttpGet("")]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(int idGerencia, int idSuperintendencia)
        {
            var parameters = _dataTableService.GetSentParameters();
            var areas = await _areaProxy.ObtenerDataTable(parameters, idGerencia, idSuperintendencia);
            return Ok(areas);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idSuperintendencia)
        {
            var areas = await _areaProxy.ObtenerActivas(idSuperintendencia);
            return Ok(areas);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idSuperintendencia)
        {
            var areas = await _areaProxy.ObtenerTodas(idSuperintendencia);
            return Ok(areas);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(AreaDto area)
        {
            area.UCRCN = User.GetUserCode();
            var ret = await _areaProxy.Insertar(area);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(AreaDto area)
        {
            area.UEDCN = User.GetUserCode();
            var ret = await _areaProxy.Actualizar(area);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var area = await _areaProxy.Obtener(id);
            var model = new AreaDto
            {
                DSCRPCN = area.DSCRPCN,
                ID = area.ID,
                IDGRNCA = area.IDGRNCA,
                IDSPRINTNDNCA = area.IDSPRINTNDNCA,
                //
                GDESTDO = area.GDESTDO,
                FCRCN = area.FCRCN,
                FEDCN = area.FEDCN,
                FESTDO = area.FESTDO,
                UCRCN = area.UCRCN,
                UEDCN = area.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new AreaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _areaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
