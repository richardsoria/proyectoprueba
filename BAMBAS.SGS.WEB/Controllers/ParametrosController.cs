﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Parametro;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("Parametros")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class ParametrosController : Controller
    {
        private readonly IParametroProxy _parametroProxy;
        private readonly IDataTableService _dataTableService;
        public ParametrosController(IParametroProxy parametroProxy,
            IDataTableService dataTableService)
        {
            _parametroProxy = parametroProxy;
            _dataTableService = dataTableService;
        }
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet("listarparametros")]
        public async Task<IActionResult> ListarParametros()
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _parametroProxy.ObtenerDataTable(parameters);
            return Ok(retorno); ;
        }
        [HttpPost("obtenerparametro")]
        public async Task<IActionResult> ObtenerParametro(int id)
        {
            var retorno = await _parametroProxy.Obtener(id);
            return Ok(retorno); ;
        }
        [HttpGet("obtener-por-abreviacion")]
        public async Task<IActionResult> ObtenerParametroPorAbreviacion(string aprmtro)
        {
            var retorno = await _parametroProxy.ObtenerParametroPorAbreviacion(aprmtro);
            return Ok(retorno); ;
        }
        [HttpPost("insertarparametro")]
        public async Task<IActionResult> InsertarParametro(ParametroDto entidad)
        {
            entidad.UCRCN = User.GetUserCode();
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _parametroProxy.Insertar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("actualizarparametro")]
        public async Task<IActionResult> ActualizarParametro(ParametroDto entidad)
        {
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _parametroProxy.Actualizar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("eliminarparametro")]
        public async Task<IActionResult> EliminarParametro(int id)
        {
            var entidad = new ParametroDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _parametroProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
