﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Gerencia;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("gerencia")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class GerenciaController : Controller
    {
        private readonly IGerenciaProxy _gerenciaProxy;
        private readonly IDataTableService _dataTableService;
        public GerenciaController(
            IDataTableService dataTableService,
            IGerenciaProxy gerenciaProxy)
        {
            _dataTableService = dataTableService;
            _gerenciaProxy = gerenciaProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var gerencia = await _gerenciaProxy.ObtenerDataTable(parameters);
            return Ok(gerencia);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas()
        {
            var gerencia = await _gerenciaProxy.ObtenerActivas();
            return Ok(gerencia);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas()
        {
            var gerencia = await _gerenciaProxy.ObtenerTodas();
            return Ok(gerencia);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(GerenciaDto gerencia)
        {
            gerencia.UCRCN = User.GetUserCode();
            var ret = await _gerenciaProxy.Insertar(gerencia);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(GerenciaDto gerencia)
        {
            gerencia.UEDCN = User.GetUserCode();
            var ret = await _gerenciaProxy.Actualizar(gerencia);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var gerencia = await _gerenciaProxy.Obtener(id);
            var model = new GerenciaDto
            {
                DSCRPCN = gerencia.DSCRPCN,
                ID = gerencia.ID,
                //
                GDESTDO = gerencia.GDESTDO,
                FCRCN = gerencia.FCRCN,
                FEDCN = gerencia.FEDCN,
                FESTDO = gerencia.FESTDO,
                UCRCN = gerencia.UCRCN,
                UEDCN = gerencia.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new GerenciaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _gerenciaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
