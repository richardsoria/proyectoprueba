﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Cargo;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("cargo")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     

    public class CargoController : Controller
    {
        private readonly  ICargoProxy _cargoProxy;
        private readonly IDataTableService _dataTableService;
        public CargoController(
            IDataTableService dataTableService,
            ICargoProxy cargoProxy)
        {
            _dataTableService = dataTableService;
            _cargoProxy = cargoProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(int idGrupoCargo)
        {
            var parameters = _dataTableService.GetSentParameters();
            var cargo = await _cargoProxy.ObtenerDataTable(parameters, idGrupoCargo);
            return Ok(cargo);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos(int idGrupoCargo )
        {
            var cargo = await _cargoProxy.ObtenerActivos(idGrupoCargo);
            return Ok(cargo);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idGrupoCargo)
        {
            var cargo = await _cargoProxy.ObtenerTodas(idGrupoCargo);
            return Ok(cargo);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(CargoDto cargo)
        {
            cargo.UCRCN = User.GetUserCode();
            var ret = await _cargoProxy.Insertar(cargo);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(CargoDto cargo)
        {
            cargo.UEDCN = User.GetUserCode();
            var ret = await _cargoProxy.Actualizar(cargo);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var cargo = await _cargoProxy.Obtener(id);
            var model = new CargoDto
            {
                DSCRPCN = cargo.DSCRPCN,
                ID = cargo.ID,
                //
                IDGRPO  = cargo.IDGRPO,
                GDESTDO = cargo.GDESTDO,
                FCRCN = cargo.FCRCN,
                FEDCN = cargo.FEDCN,
                FESTDO = cargo.FESTDO,
                UCRCN = cargo.UCRCN,
                UEDCN = cargo.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new CargoDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _cargoProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
