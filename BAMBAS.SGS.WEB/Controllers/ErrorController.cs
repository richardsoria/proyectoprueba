﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Seguridad;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("error")]
    [Authorize]
    public class ErrorController : Controller
    {
        private readonly IErrorProxy _errorProxy;
        private readonly IDataTableService _dataTableService;
        public ErrorController(
            IDataTableService dataTableService,
            IErrorProxy errorProxy)
        {
            _dataTableService = dataTableService;
            _errorProxy = errorProxy;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var errores = await _errorProxy.ObtenerDataTable(parameters);
            return Ok(errores);
        }

        [HttpGet("descargar")]
        public async Task<IActionResult> Descargar()
        {
            var path = Path.Combine(
                     Directory.GetCurrentDirectory(),
                     "Logs", "app.log");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".log", "text/plain"},
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}
