﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoDato;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using BAMBAS.SGS.WEB.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("grupo-dato")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class GrupoDatoController : Controller
    {
        private readonly IGrupoDatoProxy _grupoDatoProxy;
        private readonly IPerfilObjetoProxy _perfilObjetoProxy;
        private readonly IDataTableService _dataTableService;
        public GrupoDatoController(
            IDataTableService dataTableService,
            IGrupoDatoProxy grupoDatoProxy,
            IPerfilObjetoProxy perfilObjetoProxy
            )
        {
            _dataTableService = dataTableService;
            _grupoDatoProxy = grupoDatoProxy;
            _perfilObjetoProxy = perfilObjetoProxy;
        }
        public  IActionResult Index()
        {
            return View();
        }

        [HttpGet("obtener-grupodato")]
        public async Task<IActionResult> ObtenerGrupoDatoDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _grupoDatoProxy.ObtenerDataTable(parameters);
            return Ok(empresas);
        }

        [HttpPost("guardar-grupo-dato")]
        public async Task<IActionResult> GuardarGrupoDato(GrupoDatoEmpresaDto grupo)
        {
            RespuestaConsulta retorno;
            if (!grupo.ID.HasValue)
            {
                grupo.UCRCN = User.GetUserCode();
                retorno = await _grupoDatoProxy.Insertar(grupo);
            }
            else
            {
                grupo.UEDCN = User.GetUserCode();
                retorno = await _grupoDatoProxy.Actualizar(grupo);
            }
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var grupo = await _grupoDatoProxy.Obtener(id);
            var model = new GrupoDatoEmpresaDto
            {
                AGDTO = grupo.AGDTO,
                CGDTO = grupo.CGDTO,
                DGDTO = grupo.DGDTO,
                //
                UEDCN = grupo.UEDCN,
                UCRCN = grupo.UCRCN,
                GDESTDO = grupo.GDESTDO,
                FCRCN = grupo.FCRCN,
                FEDCN = grupo.FEDCN,
                FESTDO = grupo.FESTDO,
                ID = grupo.ID
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new GrupoDatoEmpresaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _grupoDatoProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
