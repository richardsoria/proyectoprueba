﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Familia;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("familia")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class FamiliaController : Controller
    {
        private readonly IFamiliaProxy _familiaProxy;
        private readonly IDataTableService _dataTableService;
        public FamiliaController(
            IDataTableService dataTableService,
            IFamiliaProxy familiaProxy)
        {
            _dataTableService = dataTableService;
            _familiaProxy = familiaProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("sub")]
        public async Task<IActionResult> SubFamilia()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(int idFamilia, bool? soloHijos=false)
        {
            var parameters = _dataTableService.GetSentParameters();
            var familia = await _familiaProxy.ObtenerDataTable(parameters, idFamilia, soloHijos);
            return Ok(familia);
        }
        [HttpGet("obtener-activas")]
        public async Task<IActionResult> ObtenerActivas(int idFamilia)
        {
            var familia = await _familiaProxy.ObtenerActivas(idFamilia);
            return Ok(familia);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas(int idFamilia)
        {
            var familia = await _familiaProxy.ObtenerTodas(idFamilia);
            return Ok(familia);
        }
        [HttpGet("obtener-activas-Subfamilia")]
        public async Task<IActionResult> ObtenerActivasSubfamilia(int idFamilia, bool? soloHijos = false)
        {
            var familia = await _familiaProxy.ObtenerActivasSubfamilia(idFamilia,soloHijos);
            return Ok(familia);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(FamiliaDto familia)
        {
            familia.UCRCN = User.GetUserCode();
            var ret = await _familiaProxy.Insertar(familia);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(FamiliaDto familia)
        {
            familia.UEDCN = User.GetUserCode();
            var ret = await _familiaProxy.Actualizar(familia);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var familia = await _familiaProxy.Obtener(id);
            var model = new FamiliaDto
            {
                DSCRPCN = familia.DSCRPCN,
                ID = familia.ID,
                IDFMLAPDRE = familia.IDFMLAPDRE,
                //
                GDESTDO = familia.GDESTDO,
                FCRCN = familia.FCRCN,
                FEDCN = familia.FEDCN,
                FESTDO = familia.FESTDO,
                UCRCN = familia.UCRCN,
                UEDCN = familia.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new FamiliaDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _familiaProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
