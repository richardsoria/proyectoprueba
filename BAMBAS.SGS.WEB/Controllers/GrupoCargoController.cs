﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoCargo;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("grupocargo")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     

    public class GrupoCargoController : Controller
    {
        private readonly IGrupoCargoProxy _grupocargoProxy;
        private readonly IDataTableService _dataTableService;
        public GrupoCargoController(
            IDataTableService dataTableService,
            IGrupoCargoProxy grupocargoProxy)
        {
            _dataTableService = dataTableService;
            _grupocargoProxy = grupocargoProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        [HttpGet("agregar/{id}")]
        public IActionResult EPPxGrupoCargo(int? id = null)
        {
            return View(id);
        }
        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var grupocargo = await _grupocargoProxy.ObtenerDataTable(parameters);
            return Ok(grupocargo);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var grupocargo = await _grupocargoProxy.ObtenerActivos();
            return Ok(grupocargo);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas()
        {
            var grupocargo = await _grupocargoProxy.ObtenerTodas();
            return Ok(grupocargo);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(GrupoCargoDto grupocargo)
        {
            grupocargo.UCRCN = User.GetUserCode();
            var ret = await _grupocargoProxy.Insertar(grupocargo);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar-detalle")]
        public async Task<IActionResult> ActualizarDetalle(GrupoCargoDto grupocargo)
        {
            grupocargo.UEDCN = User.GetUserCode();
            var ret = await _grupocargoProxy.ActualizarDetalle(grupocargo);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(GrupoCargoDto grupocargo)
        {
            grupocargo.UEDCN = User.GetUserCode();
            var ret = await _grupocargoProxy.Actualizar(grupocargo);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var grupocargo = await _grupocargoProxy.Obtener(id);
            var model = new GrupoCargoDto
            {
                DSCRPCN = grupocargo.DSCRPCN,
                ID = grupocargo.ID,
                //
                GDESTDO = grupocargo.GDESTDO,
                FCRCN = grupocargo.FCRCN,
                FEDCN = grupocargo.FEDCN,
                FESTDO = grupocargo.FESTDO,
                UCRCN = grupocargo.UCRCN,
                UEDCN = grupocargo.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new GrupoCargoDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _grupocargoProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("eliminar-detalle")]
        public async Task<IActionResult> EliminarDetalle(int id)
        {
            var entidad = new GrupoCargoDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _grupocargoProxy.EliminarDetalle(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("listar-detalle")]
        public async Task<IActionResult> ListarDetalle(int id)
        {
            var retorno = await _grupocargoProxy.ListarDetalle(id);
            return Ok(retorno); ;
        }
    }
}
