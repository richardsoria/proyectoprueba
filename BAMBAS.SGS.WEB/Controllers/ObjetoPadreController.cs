﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.ObjetoPadre;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("objeto")]
    [Authorize]
   // [ServiceFilter(typeof(AuthLogin))]

    public class ObjetoPadreController : Controller
    {
        private readonly IObjetoPadreProxy _ObjetoPadreProxy;
        private readonly IDataTableService _dataTableService;
        public ObjetoPadreController(
            IDataTableService dataTableService,
            IObjetoPadreProxy ObjetoPadreProxy
            )
        {
            _dataTableService = dataTableService;
            _ObjetoPadreProxy = ObjetoPadreProxy;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet("obtener-tabla-padre")]
        public async Task<IActionResult> ObtenerObjetoPadreDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _ObjetoPadreProxy.ObtenerPadreDataTable(parameters);
            return Ok(empresas);
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerObjetoDataTable(string idobjetopadre)
        {
            var parameters = _dataTableService.GetSentParameters();
            var empresas = await _ObjetoPadreProxy.ObtenerDataTable(parameters, idobjetopadre);
            return Ok(empresas);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerObjetoPadre()
        {
            var empresas = await _ObjetoPadreProxy.ObtenerActivos();
            return Ok(empresas);
        }
        [HttpGet("obtener-por-modulo")]
        public async Task<IActionResult> ObtenerObjetoPadre(int idModulo)
        {
            var empresas = await _ObjetoPadreProxy.ObtenerActivos();
            var resultado = empresas.Where(x => x.IDMDLO == idModulo).ToList();
            return Ok(resultado);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> InsertarObjetoPadre(ObjetoPadreDto grupo)
        { 
            grupo.UCRCN = User.GetUserCode();
            var retorno = await _ObjetoPadreProxy.Insertar(grupo);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ObjetoPadreDto grupo)
        {
            grupo.UEDCN = User.GetUserCode();
            var retorno = await _ObjetoPadreProxy.Actualizar(grupo);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var grupo = await _ObjetoPadreProxy.Obtener(id);
            var model = new ObjetoPadreDto
            {
                NOBJTO = grupo.NOBJTO,
                FORDN = grupo.FORDN,
                IDMDLO = grupo.IDMDLO,
                IDOBJTOPDRE=grupo.IDOBJTOPDRE,
                DOBJTO=grupo.DOBJTO,
                URL=grupo.URL,
                //
                UEDCN = grupo.UEDCN,
                UCRCN = grupo.UCRCN,
                GDESTDO = grupo.GDESTDO,
                FCRCN = grupo.FCRCN,
                FEDCN = grupo.FEDCN,
                FESTDO = grupo.FESTDO,
                ID = grupo.ID
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var empresa = new ObjetoPadreDto();//await _ObjetoPadreProxy.Obtener(id);
            empresa.ID = id;
            empresa.UEDCN = User.GetUserCode();
            var retorno = await _ObjetoPadreProxy.Eliminar(empresa);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
