﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Usuario;
using BAMBAS.Negocios.General.Persona;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("usuario")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioProxy _usuarioProxy;
        private readonly IPersonaProxy _personaProxy;
        private readonly IDataTableService _dataTableService;

        public UsuarioController(IDataTableService dataTableService,
            IUsuarioProxy usuarioProxy,
            IPersonaProxy personaProxy
            )
        {
            _dataTableService = dataTableService;
            _usuarioProxy = usuarioProxy;
            _personaProxy = personaProxy;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Persona
        [HttpGet("listarPersonas")]
        public async Task<IActionResult> listarPersonas(string gdtprsna, string tipoDocumento, string NumDocumento, string buscarNombreApellido, string desde, string hasta, string estado)
        {
            var parameters = _dataTableService.GetSentParameters();
            var personas = await _personaProxy.listadoPersonaSinUsuario(parameters, "", gdtprsna, tipoDocumento, NumDocumento, buscarNombreApellido, desde, hasta, estado);
            return Ok(personas);
        }
        #endregion

        #region Usuario

        [HttpGet("listarusuarios")]
        public async Task<IActionResult> ListarUsuarios()
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _usuarioProxy.ObtenerDataTable(parameters);
            return Ok(retorno);
        }

        [HttpPost("obtenerusuario")]
        public async Task<IActionResult> ObtenerUsuario(string cusro)
        {
            var retorno = await _usuarioProxy.Obtener(cusro);
            return Ok(retorno);
        }

        [HttpPost("obtenerusuarioXid")]
        public async Task<IActionResult> ObtenerUsuarioXid(int id)
        {
            var retorno = await _usuarioProxy.ObtenerXid(id);
            return Ok(retorno);
        }

        [HttpGet("obtenerusuarioprincipal")]
        public async Task<IActionResult> ObtenerUsuarioPrincipal(int id)
        {
            var retorno = await _usuarioProxy.ObtenerPrincipal(id);
            return Ok(retorno);
        }

        [HttpPost("insertarusuario")]
        public async Task<IActionResult> InsertarUsuario(UsuarioCustom entidad)
        {
            var usu = new UsuarioDto();
            usu.IDPRSNA = entidad.IDPRSNA;
            usu.CUSRO = entidad.CUSRO;
            //usu.UHMLGDO = entidad.UHMLGDO;
            usu.NYAPLLDS = NombreCompleto(entidad);
            usu.TDCMNTO = entidad.TDCMNTO;
            usu.NDCMNTO = entidad.NDCMNTO;
            usu.NTLFNO = entidad.NTLFNO;
            usu.CLVE = !string.IsNullOrEmpty(entidad.CLVE) ? GenerarEncriptada(entidad.CLVE): null;
            usu.FVCLVE = entidad.FVCLVE;
            usu.FBLQUO = entidad.FBLQUO == "on";
            usu.FRZRCMBOCLVE = entidad.FRZRCMBOCLVE == "on";
            usu.FCNTRTSTA = entidad.FCNTRTSTA == "on";
            usu.GDESTDO = entidad.GDESTDO;
            usu.UCRCN = User.GetUserCode();
            usu.UEDCN = User.GetUserCode();

            var retorno = await _usuarioProxy.Insertar(usu);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok(retorno);
        }

        private string NombreCompleto(UsuarioCustom entidad)
        {
            var nombre1 = string.Empty;
            var nombre2 = string.Empty;
            var nombre3 = string.Empty;
            var nombre4 = string.Empty;
            if (!string.IsNullOrEmpty(entidad.RSCL))
            {
                nombre1 = entidad.RSCL;
            }
            else
            {
                if (!string.IsNullOrEmpty(entidad.PNMBRE))
                {
                    nombre1 = entidad.PNMBRE;
                }
                if (!string.IsNullOrEmpty(entidad.SNMBRE))
                {
                    nombre2 = entidad.SNMBRE;
                }
                if (!string.IsNullOrEmpty(entidad.APTRNO))
                {
                    nombre3 = entidad.APTRNO;
                }
                if (!string.IsNullOrEmpty(entidad.AMTRNO))
                {
                    nombre4 = entidad.AMTRNO;
                }
            }

            return nombre1 + " " + nombre2 + " " + nombre3 + " " + nombre4;
        }

        [HttpPost("actualizarusuario")]
        public async Task<IActionResult> ActualizarUsuario(UsuarioCustom entidad)
        {
            var usuarioEditar = await _usuarioProxy.ObtenerXid(entidad.ID.Value);
            usuarioEditar.CUSRO = entidad.CUSRO;
            usuarioEditar.CLVE = GenerarClave(usuarioEditar.CLVE, entidad.CLVE);
            usuarioEditar.FVCLVE = entidad.FVCLVE;
            usuarioEditar.FBLQUO = entidad.FBLQUO == "on";
            usuarioEditar.FRZRCMBOCLVE = entidad.FRZRCMBOCLVE == "on";
            usuarioEditar.FCNTRTSTA = entidad.FCNTRTSTA == "on";
            usuarioEditar.GDESTDO = entidad.GDESTDO;
            usuarioEditar.UEDCN = User.GetUserCode();

            var retorno = await _usuarioProxy.Actualizar(usuarioEditar);

            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok(entidad.ID.Value);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var usuario = new UsuarioDto();//await _usuarioProxy.ObtenerXid(id);
            usuario.ID = id;
            usuario.UEDCN = User.GetUserCode();
            var retorno = await _usuarioProxy.Eliminar(usuario);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }

        private string GenerarClave(string claveobtenida, string nuevaclave)
        {
            if (claveobtenida != string.Empty)
            {
                if (nuevaclave == string.Empty || nuevaclave == null)
                {
                    return claveobtenida;
                }
                else
                {
                    return GenerarEncriptada(nuevaclave);
                }
            }
            else
            {
                return GenerarEncriptada(nuevaclave);
            }
        }
        private string GenerarEncriptada(string password)
        {
            if (password != string.Empty)
            {
                //Create the salt value with a cryptographic PRNG:
                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
                //Create the Rfc2898DeriveBytes and get the hash value:
                var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
                byte[] hash = pbkdf2.GetBytes(20);
                //Combine the salt and password bytes for later use:
                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);
                //Turn the combined salt+hash into a string for storage
                return Convert.ToBase64String(hashBytes);
            }
            return password;
        }

        #endregion
    }
}
