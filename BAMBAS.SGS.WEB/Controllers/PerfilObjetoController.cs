﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.PerfilObjeto;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Extensions;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [ApiController]
    [Route("perfil-objeto")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class PerfilObjetoController : Controller
    {
        private readonly IDataTableService _dataTableService;
        private readonly IPerfilObjetoProxy _perfilObjetoProxy;
        private readonly IViewRenderService _viewRenderService;

        public PerfilObjetoController(IDataTableService dataTableService,
            IPerfilObjetoProxy perfilObjetoProxy,
            IViewRenderService viewRenderService)
        {
            _dataTableService = dataTableService;
            _perfilObjetoProxy = perfilObjetoProxy;
            _viewRenderService = viewRenderService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("obtener-objetos")]
        public async Task<IActionResult> ObtenerObjetos(string idPerfil)
        {
            const int TIPO_MODULO = 1;
            const int TIPO_OBJETO_PADRE = 2;
            const int TIPO_OBJETO_HIJO = 3;
            const int TIPO_DETALLE = 4;

            var nodos = new List<ArbolModel>();
            if (!string.IsNullOrEmpty(idPerfil))
            {
                var perfiles = await _perfilObjetoProxy.Listar(idPerfil);
                foreach (var item in perfiles.Where(x => x.TIPO == TIPO_MODULO))
                {
                    var nodosOBjetosPadre = new List<ArbolModel>();
                    foreach (var objs in perfiles.Where(x => x.TIPO == TIPO_OBJETO_PADRE && x.IDPDRE == item.ID.ToString()))
                    {
                        var nodosOBjetosHijos = new List<ArbolModel>();
                        var listadohijos = perfiles.Where(x => x.TIPO == TIPO_OBJETO_HIJO && x.IDPDRE == objs.ID.ToString());
                        if (listadohijos != null && listadohijos.Count() > 0)
                        {
                            foreach (var hijos in listadohijos)
                            {
                                var nodosDetalles = new List<ArbolModel>();
                                foreach (var detalles in perfiles.Where(x => x.TIPO == TIPO_DETALLE && x.IDPDRE == hijos.ID.ToString()))
                                {
                                    nodosDetalles.Add(new ArbolModel
                                    {
                                        Id = detalles.ID,
                                        Texto = detalles.NMBRE,
                                        Checkeado = detalles.CHECKEADO,
                                        Tipo = TIPO_DETALLE
                                    });
                                }                             
                                nodosOBjetosHijos.Add(new ArbolModel
                                {
                                    Id = hijos.ID,
                                    Texto = hijos.NMBRE,
                                    Checkeado = hijos.CHECKEADO,
                                    Nodos = nodosDetalles,
                                    Tipo = TIPO_OBJETO_HIJO
                                });                             
                            }
                        }
                        else
                        {
                            var nodosDetalles = new List<ArbolModel>();
                            foreach (var detalles in perfiles.Where(x => x.TIPO == TIPO_DETALLE && x.IDPDRE == objs.ID.ToString()))
                            {
                                nodosDetalles.Add(new ArbolModel
                                {
                                    Id = detalles.ID,
                                    Texto = detalles.NMBRE,
                                    Checkeado = detalles.CHECKEADO,
                                    Tipo = TIPO_DETALLE
                                });
                            }
                            nodosOBjetosHijos = nodosDetalles;
                        }
                        nodosOBjetosPadre.Add(new ArbolModel
                        {
                            Id = objs.ID,
                            Texto = objs.NMBRE,
                            Checkeado = objs.CHECKEADO,
                            Nodos = nodosOBjetosHijos,
                            Tipo = TIPO_OBJETO_PADRE
                        });
                    }
                    nodos.Add(new ArbolModel
                    {
                        Id = item.ID,
                        Texto = item.NMBRE,
                        Checkeado = item.CHECKEADO,
                        Nodos = nodosOBjetosPadre,
                        Tipo = TIPO_MODULO
                    });
                }
            }

            var partialViewHtml = await _viewRenderService.RenderToStringAsync("~/Views/PerfilObjeto/Partials/_ArbolObjetos.cshtml", nodos);
            return Ok(new { partial = partialViewHtml, esAdmin = idPerfil == "1" });
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(GuardarPerfilObjetoCustom data)
        {
            data.UCRCN = User.GetUserCode();
            var retorno = await _perfilObjetoProxy.GuardarPerfilesObjeto(data);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
