﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.SucursalUsuario;
using BAMBAS.Negocios.Seguridad;
using BAMBAS.SGS.WEB.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [ApiController]
    [Route("sucursal-usuario")]
    [Authorize]
    [ServiceFilter(typeof(AuthLogin))]

    public class SucursalUsuarioController : Controller
    {
        private readonly IDataTableService _dataTableService;
        private readonly ISucursalUsuarioProxy _SucursalUsuarioProxy;

        public SucursalUsuarioController(IDataTableService dataTableService,
            ISucursalUsuarioProxy SucursalUsuarioProxy)
        {
            _dataTableService = dataTableService;
            _SucursalUsuarioProxy = SucursalUsuarioProxy;
        }

        [HttpGet("obtener-usuarios")]
        public async Task<IActionResult> ObtenerUsuarios(string idSucursal)
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _SucursalUsuarioProxy.ObtenerDataTable(parameters,idSucursal);
            return Ok(retorno); ;
        }
        [HttpGet("obtener-sucursales")]
        public async Task<IActionResult> ObtenerSucursales(string idEmpresa, string idUsuario)
        {
            var parameters = _dataTableService.GetSentParameters();
            var retorno = await _SucursalUsuarioProxy.ObtenerSucursalesDataTable(parameters, idEmpresa, idUsuario);
            return Ok(retorno); ;
        }
        [HttpPost("guardar")]
        public async Task<IActionResult> Guardar(GuardarSucursalUsuarioCustom data)
        {
            data.UCRCN = User.GetUserCode();
            var retorno = await _SucursalUsuarioProxy.GuardarSucursalesUsuario(data);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("guardar-sucursales-usuario")]
        public async Task<IActionResult> GuardarSucursales(GuardarUsuarioSucursalCustom data)
        {
            data.UCRCN = User.GetUserCode();
            var retorno = await _SucursalUsuarioProxy.GuardarSucursalesUsuario(data);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
