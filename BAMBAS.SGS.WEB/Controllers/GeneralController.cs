﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.General.Persona;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    public class GeneralController : Controller
    {
        private readonly IGrupoDatoProxy _grupoDatoProxy;
        private readonly IUbigeoProxy _ubigeoProxy;
        private readonly ISelect2Service _select2Service;
        private readonly ITrabajadorProxy _trabajadorProxy;

        public GeneralController(
            IGrupoDatoProxy grupoDatoProxy,
            IUbigeoProxy ubigeoProxy,
            ISelect2Service select2Service,
            ITrabajadorProxy trabajadorProxy

            )
        {
            _grupoDatoProxy = grupoDatoProxy;
            _ubigeoProxy = ubigeoProxy;
            _select2Service = select2Service;
            _trabajadorProxy = trabajadorProxy;
        }

        [HttpGet("obtener-datos-usuario-logueado")]
        public IActionResult ObtenerLogueado()
        {
            var audit = new AuditoriaDto();
            audit.UEDCN = User.GetUserCode();
            audit.UCRCN = User.GetUserCode();
            audit.FCRCN = DateTime.Now;
            audit.FEDCN = DateTime.Now;
            audit.FESTDO = DateTime.Now;
            return Ok(audit);
        }
        [HttpGet("obtener-departamentos")]
        public async Task<IActionResult> ObtenerDepartamentos(string cps = "169")
        {
            var result = await _ubigeoProxy.ObtenerDepartamentos(cps);
            return Ok(result);
        }
        [HttpGet("obtener-provincias")]
        public async Task<IActionResult> ObtenerProvincias(string codigoDpto, string cps="169")
        {
            var result = await _ubigeoProxy.ObtenerProvincias(codigoDpto, cps);
            return Ok(result);
        }
        [HttpGet("obtener-distritos")]
        public async Task<IActionResult> ObtenerDistritos(string codigoProvincia, string cps = "169")
        {
            var result = await _ubigeoProxy.ObtenerDistrito(codigoProvincia, cps);
            return Ok(result);
        }

        [HttpGet("obtenercatalogoGD")]
        public async Task<IActionResult> ObtenerCatalogo()
        {
            var result = await _grupoDatoProxy.ObtenerCatalogos();
            return Ok(result);
        }
        [HttpGet("select-trabajador")]
        public async Task<IActionResult> ListarTrabajadores()
        {
            var param = _select2Service.GetRequestParameters();
            var retorno = await _trabajadorProxy.SelectTrabajador(param);
            return Ok(retorno);
        }
    }
}
