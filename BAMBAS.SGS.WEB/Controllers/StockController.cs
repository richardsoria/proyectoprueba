﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Stock;
using BAMBAS.Negocios.Seguridad;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("stock")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]

    public class StockController : Controller
    {
        private readonly IStockProxy _stockProxy;
        private readonly IDataTableService _dataTableService;
        public StockController(
            IDataTableService dataTableService,
            IStockProxy stockProxy)
        {
            _dataTableService = dataTableService;
            _stockProxy = stockProxy;
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable(int idalmcn)
        {
            var parameters = _dataTableService.GetSentParameters();
            var stock = await _stockProxy.ObtenerDataTable(parameters, idalmcn);
            return Ok(stock);
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ActualizarStockMinimoDto stock)
        {
            stock.UEDCN = User.GetUserCode();
            var ret = await _stockProxy.Insertar(stock);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
      

        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new StockDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _stockProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
        [HttpPost("cargar-excel/{idalmacen}")]
        public async Task<IActionResult> CargarExcel(int idalmacen, IFormFile archivo)
        {
            #region Variable Declaration
            DataSet dsexcelRecords = new DataSet();
            IExcelDataReader reader = null;
            //HttpPostedFile Inputfile = null;
            Stream FileStream = null;
            #endregion

            #region Save Student Detail From Excel
            try
            {
                if (archivo != null)
                {
                    var Inputfile = archivo;
                    FileStream = Inputfile.OpenReadStream();

                    if (Inputfile != null && FileStream != null)
                    {
                        if (Inputfile.FileName.EndsWith(".xls"))
                            reader = ExcelReaderFactory.CreateBinaryReader(FileStream);
                        else if (Inputfile.FileName.EndsWith(".xlsx"))
                            reader = ExcelReaderFactory.CreateOpenXmlReader(FileStream);
                        else
                            return BadRequest("El formato del archivo no es válido.");

                        dsexcelRecords = reader.AsDataSet();
                        reader.Close();

                        if (dsexcelRecords != null && dsexcelRecords.Tables.Count > 0)
                        {
                            DataTable dtStudentRecords = dsexcelRecords.Tables[0];
                            var listastock = new List<StockDto>();
                            for (int i = 1; i < dtStudentRecords.Rows.Count; i++)
                            {
                                var stock = new StockDto();
                                stock.TCDGO = Convert.ToString(dtStudentRecords.Rows[i][0]);
                                stock.CSAP = Convert.ToString(dtStudentRecords.Rows[i][1]);
                                stock.DSCRPCNARTCLO = Convert.ToString(dtStudentRecords.Rows[i][2]);
                                stock.STCK = Convert.ToInt32(dtStudentRecords.Rows[i][3]);
                                stock.STCKMNMO = Convert.ToInt32(dtStudentRecords.Rows[i][4]);
                                stock.PRCO = Convert.ToDecimal(dtStudentRecords.Rows[i][5]);
                                listastock.Add(stock);
                            }
                            var entidad = new ActualizacionMasivaStockDto();
                            entidad.IDALMCN = idalmacen;
                            entidad.UEDCN = User.GetUserCode();
                            entidad.Stock = listastock;
                            var retorno = await _stockProxy.ActualizacionMasivaStock(entidad);
                            if (!retorno.EsSatisfactoria)
                                return BadRequest(retorno.Mensaje);
                            return Ok();
                        }
                        else
                            return BadRequest("El archivo seleccionado está vacío");
                    }
                    else
                        return BadRequest("No ha seleccionado un archivo.");
                }
                else
                    return BadRequest("No ha seleccionado un archivo.");
            }
            catch (Exception)
            {
                throw new Exception("Hay celdas con formatos incorrectos.   ");
            }
            #endregion
        }

        #region LOG DE ACTUALIZACION DE STOCK
        [HttpGet("obtener-ultimos-actualizados")]
        public async Task<IActionResult> ObtenerUltimosActualizadosDataTable(int idalmacen)
        {
            var parameters = _dataTableService.GetSentParameters();
            var stock = await _stockProxy.ObtenerUltimosActualizadosDataTable(parameters, idalmacen);
            return Ok(stock);
        }
        [HttpGet("obtener-ultima-act/{idalmacen}")]
        public async Task<IActionResult> ObtenerUltimaActualizacion(int idalmacen)
        {
            var stock = await _stockProxy.ObtenerUltimaActualizacion(idalmacen);
            var model = new StockDto
            {
                FEDCN = stock.FEDCN,
                UEDCN = stock.UEDCN
            };
            return Ok(model);
        }
        #endregion
    }
}

