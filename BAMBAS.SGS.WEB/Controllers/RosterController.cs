﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.Negocios.Modelos.Seguridad.Roster;
using BAMBAS.Negocios.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Controllers
{
    [Route("roster")]
    [Authorize]
    //[ServiceFilter(typeof(AuthLogin))]                                     
                                                                                 
    public class RosterController : Controller
    {
        private readonly IRosterProxy _rosterProxy;
        private readonly IDataTableService _dataTableService;
        public RosterController(
            IDataTableService dataTableService,
            IRosterProxy rosterProxy)
        {
            _dataTableService = dataTableService;
            _rosterProxy = rosterProxy;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet("get")]
        public async Task<IActionResult> ObtenerDataTable()
        {
            var parameters = _dataTableService.GetSentParameters();
            var roster = await _rosterProxy.ObtenerDataTable(parameters);
            return Ok(roster);
        }
        [HttpGet("obtener-activos")]
        public async Task<IActionResult> ObtenerActivos()
        {
            var roster = await _rosterProxy.ObtenerActivos();
            return Ok(roster);
        }
        [HttpGet("obtener-todas")]
        public async Task<IActionResult> ObtenerTodas()
        {
            var roster = await _rosterProxy.ObtenerTodas();
            return Ok(roster);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(RosterDto roster)
        {
            roster.UCRCN = User.GetUserCode();
            roster.DSCRPCN =string.IsNullOrEmpty(roster.DSCRPCN)? $"{roster.DTRBJDS}X{roster.DDSCNSO}": roster.DSCRPCN;
            var ret = await _rosterProxy.Insertar(roster);

            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(RosterDto roster)
        {
            roster.UEDCN = User.GetUserCode();
            roster.DSCRPCN = string.IsNullOrEmpty(roster.DSCRPCN) ? $"{roster.DTRBJDS}X{roster.DDSCNSO}" : roster.DSCRPCN;
            var ret = await _rosterProxy.Actualizar(roster);
            if (!ret.EsSatisfactoria)
                return BadRequest(ret.Mensaje);
            return Ok();
        }
        [HttpGet("editar/{id}")]
        public async Task<IActionResult> Editar(int id)
        {
            var roster = await _rosterProxy.Obtener(id);
            var model = new RosterDto
            {
                DSCRPCN = roster.DSCRPCN,
                ID = roster.ID,
                //
                DTRBJDS = roster.DTRBJDS,
                DDSCNSO = roster.DDSCNSO,
                GDESTDO = roster.GDESTDO,
                FCRCN = roster.FCRCN,
                FEDCN = roster.FEDCN,
                FESTDO = roster.FESTDO,
                UCRCN = roster.UCRCN,
                UEDCN = roster.UEDCN
            };
            return Ok(model);
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var entidad = new RosterDto();
            entidad.ID = id;
            entidad.UEDCN = User.GetUserCode();
            var retorno = await _rosterProxy.Eliminar(entidad);
            if (!retorno.EsSatisfactoria)
                return BadRequest(retorno.Mensaje);
            return Ok();
        }
    }
}
