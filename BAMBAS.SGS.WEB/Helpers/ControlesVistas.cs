﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB.Helpers
{
    public static class ControlesVistas
    {
        public const string VistaEmpresa = "EMPRESA";
        public const string VistaGrupoDato = "GRUPODATO";
        public const string VistaParametro = "PARAMETRO";
        public const string VistaUsuario = "USUARIO";
        public const string VistaPerfil = "PERFIL";
        public const string VistaPerfilUsuario = "PERFILUSUARIO";
        public const string VistaObjeto = "OBJETO";
        public const string VistaGerencia = "GERENCIA";
        public const string VistaGrupoCargo = "GRUPOCARGO";
        public const string VistaCargo = "CARGO";
        public const string VistaAlmacen = "ALMACEN";
        public const string VistaFamilia = "FAMILIA";
        public const string VistaSubFamilia = "SUBFAMILIA";
        public const string VistaArticulo = "ARTICULO";
        public const string VistaKit = "KIT";
        public const string VistaArea = "AREA";
        public const string VistaGuardia = "GUARDIA";
        public const string VistaRoster = "ROSTER";
        public const string VistaSuperintendencia = "SUPERINTENDENCIA";



        public static class Empresa
        {
            public const string AgregarEmpresa = "AGRGREMPRSA";
            public const string EditarEmpresa = "EDTREMPRSA";
            public const string EliminarEmpresa = "ELMNREMPRSA";
            public const string AgregarSucursal = "AGRGRSCRSL";
            public const string EditarSucursal = "EDTRSCRSL";
            public const string EliminarSucursal = "ELMNRSCRSL";
            public const string AsignarUsuarios = "ASGNRUSRS";
        }
        public static class GrupoDato
        {
            public const string AgregarGrupoDato = "AGRGRGRPODTO";
            public const string EditarGrupoDato = "EDTRGRPODTO";
            public const string EliminarGrupoDato = "ELMNRGRPODTO";
            public const string AgregarGrupoDatoDetalle = "AGRGRGRPODTODTLLE";
            public const string EditarGrupoDatoDetalle = "EDTRGRPODTODTLLE";
            public const string EliminarGrupoDatoDetalle = "ELMNRGRPODTODTLLE";
        }
        public static class Parametro
        {
            public const string AgregarParametro = "AGRGRPRMTRO";
            public const string EditarParametro = "EDTRPRMTRO";
            public const string EliminarParametro = "ELMNRPRMTRO";
        }
        public static class Usuario
        {
            public const string AgregarUsuario = "AGRGRPRMTRO";
            public const string EditarUsuario = "EDTRPRMTRO";
            public const string EliminarUsuario = "ELMNRPRMTRO";
            public const string PerfilesUsuario = "PRFLSUSRO";
        }
        public static class Perfil
        {
            public const string AgregarPerfil = "AGRGRPRFL";
            public const string EditarPerfil = "EDTRPRFL";
            public const string EliminarPerfil = "ELMNRPRFL";
        }
        public static class PerfilUsuario
        {
            public const string Guardar = "GRDRPRFLUSRO";
        }
        public static class PerfilObjeto
        {
            public const string Guardar = "GRDRPRFLOBJTO";
        }
        public static class Objeto
        {
            public const string AgregarObjeto = "AGRGROBJTO";
            public const string EditarObjeto = "EDTROBJTO";
            public const string EliminarObjeto = "ELMNROBJTO";
            public const string AgregarObjetoDetalle = "AGRGROBJTODTLLE";
            public const string EditarObjetoDetalle = "EDTROBJTODTLLE";
            public const string EliminarObjetoDetalle = "ELMNROBJTODTLLE";
        }
        public static class GrupoCargo
        {
            public const string AgregarGrupoCargo = "AGRGRGRPOCRGO";
            public const string EditarGrupoCargo = "EDTRGRPOCRGO";
            public const string EliminarGrupoCargo = "ELMNRGRPOCRGO";

        }
        public static class Almacen
        {
            public const string AgregarAlmacen = "AGRGRALMCN";
            public const string EditarAlmacen = "EDTRALMCN";
            public const string EliminarAlmacen = "ELMNRALMCN";
            public const string ActualizacionStock = "STCKALMCN";
        }
        public static class Cargo
        {
            public const string AgregarCargo = "AGRGRCRGO";
            public const string EditarCargo = "EDTRCRGO";
            public const string EliminarCargo = "ELMNRCRGO";

        }
        public static class Familia
        {
            public const string AgregarFamilia = "AGRGRFMLA";
            public const string EditarFamilia = "EDTRFMLA";
            public const string EliminarFamilia = "ELMNRFMLA";

        }
        public static class SubFamilia
        {
            public const string AgregarSubFamilia = "AGRGRSBFMLA";
            public const string EditarSubFamilia = "EDTRSBFMLA";
            public const string EliminarSubFamilia = "ELMNRSBFMLA";

        }
        public static class Articulo
        {
            public const string AgregarArticulo = "AGRGRARTCLO";
            public const string EditarArticulo = "EDTRARTCLO";
            public const string EliminarArticulo = "ELMNRARTCLO";
            public const string ActualizacionPrecios = "PRCOARTCLO";

        }
        public static class Kit
        {
            public const string AgregarKit = "AGRGRKT";
            public const string EditarKit = "EDTRKT";
            public const string EliminarKit = "ELMNRKT";

        }
        public static class Area
        {
            public const string AgregarArea = "AGRGRARA";
            public const string EditarArea = "EDTRARA";
            public const string EliminarArea = "ELMNRARA";

        }
        public static class Guardia
        {
            public const string AgregarGuardia = "AGRGRGRDA";
            public const string EliminarGuardia = "ELMNRGDRA";

        }
        public static class Roster
        {
            public const string AgregarRoster = "AGRGRRSTR";
            public const string EliminarRoster = "ELMNRRSTR";

        }
        public static class Superintendencia
        {
            public const string AgregarSuperintendencia = "AGRGRSPRNTNDNCA";
            public const string EditarSuperintendencia = "EDTRSPRNTNDNCA";
            public const string EliminarSuperintendencia = "ELMNRSPRNTNDNCA";

        }
        public static class Gerencia
        {
            public const string AgregarGerencia = "AGRGRGRNCA";
            public const string EditarGerencia = "EDTRGRNCA";
            public const string EliminarGerencia = "ELMNRGRNCA";
        }
        
    }
}
