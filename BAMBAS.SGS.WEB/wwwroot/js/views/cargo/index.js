﻿var InicializarCargo = function () {
    var $selectEstados = $(".select-estados");
    var $selectGrupoCargo = $(".select-grupocargo");
    var $filtroGrupoCargo = $(".filtro-select-grupocargo");
    //variables JQuery    //variables JQuery
    var $tablaCargo = $("#tabla_cargo");
    var $formularioCargo = $("#cargo_form");
    var $modalCargo = $("#modal_cargo");
    //

    var $accesoAgregarCargo = $("#accesoAgregarCargo");
    var $accesoEditarCargo = $("#accesoEditarCargo");
    var $accesoEliminarCargo = $("#accesoEliminarCargo");

    var $btnCargo = $("#btnCargo");

    var entidadCargo = {
        id: "",
        idgrpo: "",
        dscrpcn: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: ""
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarCargo.val() == "False") {
                $btnCargo.remove();
            }
        }
    };

    var tablaCargo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/cargo/get",
                type: "GET",
                data: function (data) {
                    data.idGrupoCargo = $selectGrupoCargo.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Grupo Cargo",
                    className: "text-center",
                    data: "grpo",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-center",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";

                        if ($accesoEditarCargo.val() == "True") {
                        tpm += `<a data-toggle="modal" href="#modal_cargo" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarCargo.val() == "True") {
                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaCargo.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/cargo/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaCargo.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaCargo.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioCargo.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/cargo/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadCargo = result;
                        $formularioCargo.find("[name='DSCRPCN']").val(entidadCargo.dscrpcn);
                        $formularioCargo.find("[name='IDGRPO']").val(entidadCargo.idgrpo).change();
                        
                
                        $formularioCargo.AgregarCamposAuditoria(entidadCargo);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioCargo.find(":input").attr("disabled", false);
                        $formularioCargo.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaCargo.objeto = $tablaCargo.DataTable(tablaCargo.opciones);
            tablaCargo.eventos();
        }
    };
    var modalCargo = {
        form: {
            objeto: $formularioCargo.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioCargo.find(":input").attr("disabled", true);

                    if (!$formularioCargo.find("[name='ID']").val()) {
                        url = `/cargo/insertar`
                    } else {
                        url = `/cargo/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaCargo.reload();
                                modalCargo.eventos.reset();
                                $modalCargo.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioCargo.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalCargo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalCargo.on('hidden.bs.modal', function () {
                    modalCargo.eventos.reset();
                    $formularioCargo.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalCargo.on('shown.bs.modal', function () {
                    if (!$formularioCargo.find("[name='ID']").val()) {
                        $formularioCargo.AgregarCamposDefectoAuditoria();
                        $formularioCargo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalCargo.form.eventos.reset();
                $formularioCargo.trigger("reset");
                $formularioCargo.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        grupocargo: function () {
            $.get("/grupocargo/obtener-todas")
                .done(function (data) {
                    $selectGrupoCargo.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectGrupoCargo.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.grupocargo();
        }
    };

    var eventosIncrustados = {
        filtroGrupoCargo: function () {
            $filtroGrupoCargo.on("change", function () {
                tablaCargo.reload();
            });
        },
        init: function () {
            this.filtroGrupoCargo();
        }
    }
    return {
        init: function () {
            selects.init();
            tablaCargo.inicializador();
            modalCargo.init();
            eventosIncrustados.init();
            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarCargo.init();
})

