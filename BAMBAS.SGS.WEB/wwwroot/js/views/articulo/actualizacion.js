﻿var InicializarArticulo = function () {
    //variables JQuery    //variables JQuery
    var $tablaArticulo = $("#tabla_articulo");
    var $ultimaActualizacion = $(".ultima-actualizacion");
    var $ultimoResponsable = $(".ultimo-responsable");
    //
    var $formularioCargaExcel = $("#formularioCargaExcel");


    var tablaArticulo = {
        objeto: null,
        opciones: {
            pageLength: 50,
            ajax: {
                dataType: "JSON",
                url: "/articulo/obtener-ultimos-atualizados",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Cód. SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Artículo",
                    className: "text-center",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Costo Anterior",
                    className: "text-center",
                    data: "cstoantrr",
                    orderable: false
                },
                {
                    title: "Costo Actualizado",
                    className: "text-center",
                    data: "cstonvo",
                    orderable: false
                },
                {
                    title: "Observaciones",
                    data: null,
                    width: '25%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "B") {
                            tpm = "Actualizado";
                        } else if (data.gdestdo == "E") {
                            tpm = "No existe el Código SAP del artículo";
                        } else if (data.gdestdo == "P") {
                            tpm = "Error de parseo del costo del artículo";
                        }
                        return tpm;
                    }
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "B") {
                            tpm = `<span><i class="fa fa-circle text-success mr-1" title="Actualizado"></i></span>`;
                        } else if (data.gdestdo == "E") {
                            tpm = `<span><i class="fa fa-circle text-danger" title="No actualizado"></i></span>`;
                        } else if (data.gdestdo == "P") {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Error de Parseo"></i></span>`;
                        }
                        return tpm;
                    }
                },
            ]
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
        }
    };
    var modalCargaExcel = {
        form: {
            objeto: $formularioCargaExcel.validate({
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    Swal.fire({
                        title: "Se actualizará el costo de los artículos",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                        cancelButtonText: "Cancelar"
                    }).then(function (result) {
                        if (result.value) {
                            var $btn = $(formElement).find("button[type='submit']");
                            $btn.attr("disabled", true);
                            var formData = new FormData(formElement);

                            var fileInput = $formularioCargaExcel.find("[name='Archivo']")[0];
                            var file = fileInput.files[0];
                            formData.append("archivo", file);

                            $.ajax({
                                url: "/articulo/cargar-excel",
                                type: "POST",
                                data: formData,
                                contentType: false,
                                processData: false
                            })
                                .done(function (e) {
                                    Swal.fire({
                                        title: "Éxito",
                                        icon: "success",
                                        allowOutsideClick: false,
                                        text: "Guardado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then(function () {
                                        tablaArticulo.reload();
                                        ultimaActualizacion.init();
                                        modalCargaExcel.eventos.reset();
                                    });
                                })
                                .fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al guardar los datos.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                })
                                .always(function () {
                                    $btn.removeLoader();
                                    $formularioCargaExcel.find(":input").attr("disabled", false);
                                });
                        }
                    });
                }
            }),
            eventos: {
                reset: function () {
                    modalCargaExcel.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            reset: function () {
                modalCargaExcel.form.eventos.reset();
                $formularioCargaExcel.trigger("reset");
                $formularioCargaExcel.find(":input").attr("disabled", false);
            }
        },
        init: function () {
        }
    }
    var ultimaActualizacion = {
        init: function () {
            $.get("/articulo/obtener-ultima-act")
                .done(function (data) {
                    $ultimaActualizacion.text(data.cFEDCN)
                    $ultimoResponsable.text(data.uedcn)
                });
        }
    }
    return {
        init: function () {
            ultimaActualizacion.init();
            tablaArticulo.inicializador();
            //validacionControles.init();
        }
    };
}();

$(() => {
    InicializarArticulo.init();
})

