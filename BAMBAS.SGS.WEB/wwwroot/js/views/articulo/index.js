﻿var InicializarArticulo = function () {
    var $selectEstados = $(".select-estados");
    var $selectFamilias = $(".select-familias");
    var $selectSubFamilias = $(".select-Subfamilias");
    var $selectMedida = $(".select-unidadmedida");
    var $selectMoneda = $(".select-moneda");
    //variables JQuery    //variables JQuery
    var $tablaArticulo = $("#tabla_articulo");
    var $formularioArticulo = $("#articulo_form");
    var $modalArticulo = $("#modal_articulo");

    var $accesoAgregarArticulo = $("#accesoAgregarArticulo");
    var $accesoEditarArticulo = $("#accesoEditarArticulo");
    var $accesoEliminarArticulo = $("#accesoEliminarArticulo");

    var $btnArticulo = $("#btnArticulo");
    var $btnCerrarModal = $("#btnCerrarModal");
    var $txtcsto = $("#txtcsto");

    var $btnActualizacionPrecios = $("#btnActualizacionPrecios");
    var $accesoActualizacionPrecios = $("#accesoActualizacionPrecios");

    var entidadArticulo = {
        id: "",
        dscrpcn: "",
        csap: "",
        fmnjotlls: "",
        idfmla: "",
        idsbfmla: "",
        gdunddmdda: "",
        gdtmnda: "",
        csto: "",
        fmnjorstrcn: "",
        cbrra: "",
        drcn: "",
        fkit: "",
        gdestdo: "",
        mnsje: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarArticulo.val() == "False") {
                $btnArticulo.remove();
            }
            if ($accesoActualizacionPrecios.val() == "False") {
                $btnActualizacionPrecios.remove();
            }
        }
    };

    var tablaArticulo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/articulo/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },

            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Und. Medida",
                    className: "text-center",
                    data: "gdunddmdda",
                    orderable: false
                },
                {
                    title: "Moneda",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false
                },
                {
                    title: "Costo",
                    className: "text-right",
                    data: "csto",
                    orderable: false,
                    render: function (data) {
                        return data.toFixed(2)
                    }
                },
                {
                    title: "U. Edición",
                    className: "text-center",
                    data: "uedcn",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '15%',
                    render: function (data) {
                        var tpm = "";

                        if ($accesoEditarArticulo.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_articulo"  data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarArticulo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-mnsje="${data.mnsje}" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ],
        },
        eventos: function () {
            tablaArticulo.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                var mnsj = $(this).data("mnsje");
                console.log(mnsj);
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    html: mnsj,
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/articulo/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaArticulo.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaArticulo.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioArticulo.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/articulo/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadArticulo = result;
                        $formularioArticulo.find("[name='DSCRPCN']").val(entidadArticulo.dscrpcn);
                        $formularioArticulo.find("[name='CSAP']").val(entidadArticulo.csap);
                        $formularioArticulo.find("[name='IDFMLA']").val(entidadArticulo.idfmla);
                        $formularioArticulo.find("[name='CINTRNO']").val(entidadArticulo.id);

                        if (entidadArticulo.idsbfmla) {
                            $.get("/familia/obtener-activas-Subfamilia?idFamilia=" + entidadArticulo.idfmla + "&soloHijos=true")
                                .done(function (data) {
                                    if (data.length == 0) {
                                        $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", true);
                                    } else {
                                        $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", false);
                                        $selectSubFamilias.empty();
                                        $selectSubFamilias.append($("<option />").val('').text("Seleccione"));
                                        $.each(data, function (key, item) {
                                            $selectSubFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                                        });

                                        if (entidadArticulo.idsbfmla) {
                                            $formularioArticulo.find("[name='IDSBFMLA']").val(entidadArticulo.idsbfmla);
                                        }
                                    }

                                });
                        }

                        $formularioArticulo.find("[name='GDUNDDMDDA']").val(entidadArticulo.gdunddmdda);
                        $formularioArticulo.find("[name='GDTMNDA']").val(entidadArticulo.gdtmnda);
                        $formularioArticulo.find("[name='CSTO']").val(entidadArticulo.csto.toFixed(2));

                        if (entidadArticulo.fmnjotlls == true) {
                            $('.chkMnjtlls').prop('checked', true)
                        } else {
                            $('.chkMnjtlls').prop('checked', false)
                        }

                        if (entidadArticulo.fmnjorstrcn == true) {
                            $('.chkRstrc').prop('checked', true)
                        } else {
                            $('.chkRstrc').prop('checked', false)
                        }

                        $formularioArticulo.find("[name='CBRRA']").val(entidadArticulo.cbrra);
                        $formularioArticulo.find("[name='DRCN']").val(entidadArticulo.drcn);


                        $formularioArticulo.AgregarCamposAuditoria(entidadArticulo);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {

                        if (entidadArticulo.fkit) {
                            $formularioArticulo.find(":input").attr("disabled", true);
                            $btnCerrarModal.attr("disabled", false);
                            $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", true);
                        } else {
                            $formularioArticulo.find(":input").attr("disabled", false);
                        }

                        /*      $formularioArticulo.find(":input").attr("disabled", false);*/
                        $formularioArticulo.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
            tablaArticulo.eventos();
        }
    };

    var modalArticulo = {
        form: {
            objeto: $formularioArticulo.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioArticulo.find(":input").attr("disabled", true);

                    if ($('.chkMnjtlls').is(':checked')) {
                        formData.append("FMNJOTLLS", true);
                    } else {
                        formData.append("FMNJOTLLS", false);
                    }

                    if ($('.chkRstrc').is(':checked')) {
                        formData.append("FMNJORSTRCN", true);
                    } else {
                        formData.append("FMNJORSTRCN", false);
                    }


                    for (var pair of formData.entries()) {
                        console.log(pair[0] + ', ' + pair[1]);
                    }
                    if (!$formularioArticulo.find("[name='ID']").val()) {
                        url = "/articulo/insertar"
                    } else {
                        url = "/articulo/actualizar"
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaArticulo.reload();
                                modalArticulo.eventos.reset();
                                $modalArticulo.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioArticulo.find(":input").attr("disabled", false);
                        });
                    $formularioArticulo.find("[name='DSCRPCN']").attr("disabled", false);
                }
            }),
            eventos: {
                reset: function () {
                    modalArticulo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalArticulo.on('hidden.bs.modal', function () {
                    modalArticulo.eventos.reset();
                    $formularioArticulo.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalArticulo.on('shown.bs.modal', function () {
                    if (!$formularioArticulo.find("[name='ID']").val()) {
                        $formularioArticulo.AgregarCamposDefectoAuditoria();
                        $formularioArticulo.DeshabilitarCamposAuditoria();
                        $selectSubFamilias.empty();
                        $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", true);
                    }
                })
            },
            reset: function () {
                modalArticulo.form.eventos.reset();
                $formularioArticulo.trigger("reset");
                $formularioArticulo.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        familias: function () {
            $.get("/familia/obtener-activas")
                .done(function (data) {
                    $selectFamilias.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
            $selectFamilias.on("change", function () {
                var $this = $(this);
                var id = $this.val();
                if (id != '') {
                    $.get("/familia/obtener-activas-Subfamilia?idFamilia=" + id + "&soloHijos=true")
                        .done(function (data) {
                            if (data.length != 0) {
                                $selectSubFamilias.empty();
                                $selectSubFamilias.append($("<option />").val('').text("Seleccione"));
                                $.each(data, function (key, item) {
                                    $selectSubFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                                });
                            } else {
                                $selectSubFamilias.empty();
                                $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", true);
                            }
                        });
                    $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", false);
                } else {
                    $selectSubFamilias.empty();
                    $formularioArticulo.find("[name='IDSBFMLA']").attr("disabled", true);
                }
            });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            $selectMedida.LlenarSelectGD("GDUNDDMDDA");
            $selectMoneda.LlenarSelectGD("GDTMNDA");
            $formularioArticulo.find("[name='GDTMNDA']").val(2);
            selects.familias();
        }
    };

    return {
        init: function () {
            selects.init();
            tablaArticulo.inicializador();
            modalArticulo.init();
            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarArticulo.init();
})

