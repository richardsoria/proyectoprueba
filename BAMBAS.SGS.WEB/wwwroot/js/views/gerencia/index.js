﻿var InicializarGerencia = function () {
    var $selectEstados = $(".select-estados");
    //variables JQuery    //variables JQuery
    var $tablaGerencia = $("#tabla_gerencia");
    var $formularioGerencia = $("#gerencia_form");
    var $modalGerencia = $("#modal_gerencia");
    //
    var $accesoAgregarGerencia = $("#accesoAgregarGerencia");
    var $accesoEditarGerencia = $("#accesoEditarGerencia");
    var $accesoEliminarGerencia = $("#accesoEliminarGerencia");

    var $btnGerencia = $("#btnGerencia");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarGerencia.val() == "False") {
                $btnGerencia.remove();
            }
        }
    };

    var entidadGerencia = {
        id: "",
        dscrpcn: "",

        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }
    var tablaGerencia = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/gerencia/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarGerencia.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_gerencia" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarGerencia.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaGerencia.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/gerencia/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaGerencia.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaGerencia.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioGerencia.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/gerencia/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadGerencia = result;
                        $formularioGerencia.find("[name='DSCRPCN']").val(entidadGerencia.dscrpcn);

                        $formularioGerencia.AgregarCamposAuditoria(entidadGerencia);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioGerencia.find(":input").attr("disabled", false);
                        $formularioGerencia.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
          
        },
        inicializador: function () {
            tablaGerencia.objeto = $tablaGerencia.DataTable(tablaGerencia.opciones);
            tablaGerencia.eventos();
        }
    };
    var modalGerencia = {
        form: {
            objeto: $formularioGerencia.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioGerencia.find(":input").attr("disabled", true);

                    if (!$formularioGerencia.find("[name='ID']").val()) {
                        url = `/gerencia/insertar`
                    } else {
                        url = `/gerencia/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGerencia.reload();
                                modalGerencia.eventos.reset();
                                $modalGerencia.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioGerencia.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalGerencia.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalGerencia.on('hidden.bs.modal', function () {
                    modalGerencia.eventos.reset();
                    $formularioGerencia.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalGerencia.on('shown.bs.modal', function () {
                    if (!$formularioGerencia.find("[name='ID']").val()) {
                        $formularioGerencia.AgregarCamposDefectoAuditoria();
                        $formularioGerencia.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalGerencia.form.eventos.reset();
                $formularioGerencia.trigger("reset");
                $formularioGerencia.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    };
    return {
        init: function () {
            selects.init();
            tablaGerencia.inicializador();
            modalGerencia.init();

            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarGerencia.init();
})

