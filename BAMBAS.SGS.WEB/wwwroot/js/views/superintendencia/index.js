﻿var InicializarSuperintendencia = function () {
    var $selectEstados = $(".select-estados");
    var $selectGerencias = $(".select-gerencias");
    var $filtroGerencias = $(".filtro-select-gerencias");
    //variables JQuery    //variables JQuery
    var $tablaSuperintendencia = $("#tabla_superintendencia");
    var $formularioSuperintendencia = $("#superintendencia_form");
    var $modalSuperintendencia = $("#modal_superintendencia");
    //
    var $accesoAgregarSuperintendencia = $("#accesoAgregarSuperintendencia");
    var $accesoEditarSuperintendencia = $("#accesoEditarSuperintendencia");
    var $accesoEliminarSuperintendencia = $("#accesoEliminarSuperintendencia");

    var $btnSuperintendencia = $("#btnSuperintendencia");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarSuperintendencia.val() == "False") {
                $btnSuperintendencia.remove();
            }
        }
    };

    var entidadSuperintendencia = {
        id: "",
        dscrpcn: "",
        idgrnca: "",

        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }
    var tablaSuperintendencia = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: `/superintendencia/get`,
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idGerencia = $selectGerencias.val();

                    return data;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Gerencia",
                    className: "text-left",
                    data: "grnca",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarSuperintendencia.val() == "True") {
                        tpm += `<a data-toggle="modal" href="#modal_superintendencia" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarSuperintendencia.val() == "True") {
                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaSuperintendencia.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/superintendencia/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaSuperintendencia.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaSuperintendencia.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioSuperintendencia.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/superintendencia/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadSuperintendencia = result;
                        $formularioSuperintendencia.find("[name='DSCRPCN']").val(entidadSuperintendencia.dscrpcn);
                        $formularioSuperintendencia.find("[name='IDGRNCA']").val(entidadSuperintendencia.idgrnca).change();


                        $formularioSuperintendencia.AgregarCamposAuditoria(entidadSuperintendencia);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioSuperintendencia.find(":input").attr("disabled", false);
                        $formularioSuperintendencia.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaSuperintendencia.objeto = $tablaSuperintendencia.DataTable(tablaSuperintendencia.opciones);
            tablaSuperintendencia.eventos();
        }
    };
    var modalSuperintendencia = {
        form: {
            objeto: $formularioSuperintendencia.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioSuperintendencia.find(":input").attr("disabled", true);

                    if (!$formularioSuperintendencia.find("[name='ID']").val()) {
                        url = `/superintendencia/insertar`;
                    } else {
                        url = `/superintendencia/actualizar`
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaSuperintendencia.reload();
                                modalSuperintendencia.eventos.reset();
                                $modalSuperintendencia.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioSuperintendencia.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalSuperintendencia.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalSuperintendencia.on('hidden.bs.modal', function () {
                    modalSuperintendencia.eventos.reset();
                    $formularioSuperintendencia.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalSuperintendencia.on('shown.bs.modal', function () {
                    if (!$formularioSuperintendencia.find("[name='ID']").val()) {
                        $formularioSuperintendencia.AgregarCamposDefectoAuditoria();
                        $formularioSuperintendencia.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalSuperintendencia.form.eventos.reset();
                $formularioSuperintendencia.trigger("reset");
                $formularioSuperintendencia.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var selects = {
            gerencias: function () {
                $.get("/gerencia/obtener-todas")
                .done(function (data) {
                     $selectGerencias.append($("<option />").val('').text("Seleccione"));
                     $.each(data, function (key, item) {
                         $selectGerencias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                     });
                });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.gerencias();
        }
    };
    var eventosIncrustados = {
        filtroGerencias: function () {
            $filtroGerencias.on("change", function () {
                tablaSuperintendencia.reload();
            });
        },
        init: function () {
            this.filtroGerencias();
        }
    }
    return {
        init: function () {
            selects.init();
            tablaSuperintendencia.inicializador();
            modalSuperintendencia.init();

            //validacionControles.init();
            eventosIncrustados.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarSuperintendencia.init();
})

