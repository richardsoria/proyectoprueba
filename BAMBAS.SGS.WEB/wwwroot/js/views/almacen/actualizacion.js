﻿var InicializarAlmacen = function () {
    //variables JQuery    //variables JQuery
    var $tablaAlmacen = $("#tabla_almacen");
    var $ultimaActualizacion = $(".ultima-actualizacion");
    var $ultimoResponsable = $(".ultimo-responsable");
    //
    var $formularioCargaExcel = $("#formularioCargaExcel");
    var $IdAlmacen = $("#IdAlmacen");
    //
    var tablaAlmacen = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/stock/obtener-ultimos-actualizados",
                type: "GET",
                data: function (data) {
                    data.idalmacen = $IdAlmacen.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function () {
                        return "";
                    }
                },
                {
                    title: "Cód. SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Artículo",
                    className: "text-center",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Stock Anterior",
                    className: "text-center",
                    data: "stckantrr",
                    orderable: false
                },
                {
                    title: "Stock Nuevo",
                    className: "text-center",
                    data: "stcknvo",
                    orderable: false
                },
                {
                    title: "Observaciones",
                    data: null,
                    width: '25%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "B") {
                            tpm = "Actualizado";
                        }
                        else {
                            tpm = "No existe el Código SAP del artículo en el almacém";
                        }
                        return tpm;
                    }
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "B") {
                            tpm = `<span><i class="fa fa-circle text-success mr-1" title="Actualizado"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="No actualizado"></i></span>`;
                        }
                        return tpm;
                    }
                },
            ]
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaAlmacen.objeto = $tablaAlmacen.DataTable(tablaAlmacen.opciones);
        }
    };
    var modalCargaExcel = {
        form: {
            objeto: $formularioCargaExcel.validate({
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    Swal.fire({
                        title: "Se actualizará el stock de los artículos",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                        cancelButtonText: "Cancelar"
                    }).then(function (result) {
                        if (result.value) {
                            var $btn = $(formElement).find("button[type='submit']");
                            $btn.attr("disabled", true);
                            var formData = new FormData(formElement);

                            var fileInput = $formularioCargaExcel.find("[name='Archivo']")[0];
                            var file = fileInput.files[0];
                            formData.append("archivo", file);

                            $.ajax({
                                url: "/stock/cargar-excel/" + $IdAlmacen.val(),
                                type: "POST",
                                data: formData,
                                contentType: false,
                                processData: false
                            })
                                .done(function (e) {
                                    Swal.fire({
                                        title: "Éxito",
                                        icon: "success",
                                        allowOutsideClick: false,
                                        text: "Guardado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then(function () {
                                        tablaAlmacen.reload();
                                        ultimaActualizacion.init();
                                        modalCargaExcel.eventos.reset();
                                    });
                                })
                                .fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al guardar los datos.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                })
                                .always(function () {
                                    $btn.removeLoader();
                                    $formularioCargaExcel.find(":input").attr("disabled", false);
                                });
                        }
                    });
                }
            }),
            eventos: {
                reset: function () {
                    modalCargaExcel.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            reset: function () {
                modalCargaExcel.form.eventos.reset();
                $formularioCargaExcel.trigger("reset");
                $formularioCargaExcel.find(":input").attr("disabled", false);
            }
        },
        init: function () {
        }
    }
    var ultimaActualizacion = {
        init: function () {
            $.get("/stock/obtener-ultima-act/" + $IdAlmacen.val())
                .done(function (data) {
                    $ultimaActualizacion.text(data.cFEDCN)
                    $ultimoResponsable.text(data.uedcn)
            });
        }
    }
    return {
        init: function () {
            ultimaActualizacion.init();
            tablaAlmacen.inicializador();
        }
    };
}();

$(() => {
    InicializarAlmacen.init();
})

