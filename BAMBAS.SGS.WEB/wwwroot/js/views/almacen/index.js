﻿var InicializarAlmacen = function () {
    var $selectEstados = $(".select-estados");
    var $selectTipoAlmacen = $(".select-tipo-almacen");
    var $selectFiltroTipoAlmacen = $(".filtro-select-tipo-almacen");
    var $selectTrabajador = $(".select-trabajador");
    //variables JQuery    //variables JQuery
    var $tablaAlmacen = $("#tabla_almacen");
    var $formularioAlmacen = $("#almacen_form");
    var $modalAlmacen = $("#modal_almacen");
    //
    var $accesoAgregarAlmacen = $("#accesoAgregarAlmacen");
    var $accesoEditarAlmacen = $("#accesoEditarAlmacen");
    var $accesoEliminarAlmacen = $("#accesoEliminarAlmacen");

    var $btnAlmacen = $("#btnAlmacen");

    var entidadAlmacen = {
        id: "",
        dscrpcn: "",
        gdtalmcn: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        idtrbjdr: "",
        trbjdr: "",
    }
    var validacionControles = {
        init: function () {
            if ($accesoAgregarAlmacen.val() == "False") {
                $btnAlmacen.remove();
            }
        }
    };

    var tablaAlmacen = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/almacen/get",
                type: "GET",
                data: function (data) {
                    data.gdtalmcn = $selectFiltroTipoAlmacen.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Almacén",
                    className: "text-center",
                    data: "talmcn",
                    orderable: false
                },
                {
                    title: "Almacén",
                    className: "text-center",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Responsable",
                    className: "text-center",
                    data: "trbjdr",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Stock",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a data-id="${data.id}" class="btn btn-xs btn-ver" title ="Ver Stock"><span><i class="la la-eye"></i></a>`;
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        //tpm += `<a data-id="${data.id}" class="btn btn-xs btn-ver" title ="Ver Stock"><span><i class="la la-eye"></i></a>`;

                        if ($accesoEditarAlmacen.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_almacen" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarAlmacen.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaAlmacen.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/almacen/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaAlmacen.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaAlmacen.objeto.on("click", ".btn-ver", function () {
                var id = $(this).data("id");
                var url = `/almacen/Stock/${id}`;
                RedirectWithSubfolder(url);
            });
            tablaAlmacen.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioAlmacen.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/almacen/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadAlmacen = result;
                        $formularioAlmacen.find("[name='DSCRPCN']").val(entidadAlmacen.dscrpcn);
                        $formularioAlmacen.find("[name='GDTALMCN']").val(entidadAlmacen.gdtalmcn);

                        var opcionNueva = {
                            id: entidadAlmacen.idtrbjdr || "",
                            text: entidadAlmacen.trbjdr
                        };
                        var newOption = new Option(opcionNueva.text, opcionNueva.id, true, true);
                        $formularioAlmacen.find("[name='IDTRBJDR']").append(newOption);

                        $formularioAlmacen.AgregarCamposAuditoria(entidadAlmacen);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioAlmacen.find(":input").attr("disabled", false);
                        $formularioAlmacen.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaAlmacen.objeto = $tablaAlmacen.DataTable(tablaAlmacen.opciones);
            tablaAlmacen.eventos();
        }
    };
    var modalAlmacen = {
        form: {
            objeto: $formularioAlmacen.validate({
                highlight: function (element, errorClass, validClass) {
                    if ($(element).hasClass("select2-hidden-accessible")) {
                        $(element).siblings(".select2-container").find(".select2-selection").addClass("error");
                    } else {
                        $(element).addClass('error');
                    }
                },
                unhighlight: function (element, errorClass, validClass) {
                    if ($(element).hasClass("select2-hidden-accessible")) {
                        $(element).siblings(".select2-container").find(".select2-selection").removeClass("error");
                    } else {
                        $(element).removeClass('error');
                    }
                },
                success: function (label, element) {
                    if ($(element).hasClass("select2-hidden-accessible")) {
                        $(element).siblings(".select2-container").find(".select2-selection").removeClass("error");
                    } else {
                        $(element).removeClass('error');
                    }
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass("select-trabajador")) {
                        element.parent().append(error);
                    } else {
                        error.insertAfter(element);
                    }
                },
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioAlmacen.find(":input").attr("disabled", true);

                    if (!$formularioAlmacen.find("[name='ID']").val()) {
                        url = `/almacen/insertar`
                    } else {
                        url = `/almacen/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaAlmacen.reload();
                                modalAlmacen.eventos.reset();
                                $modalAlmacen.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioAlmacen.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalAlmacen.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalAlmacen.on('hidden.bs.modal', function () {
                    modalAlmacen.eventos.reset();
                    $formularioAlmacen.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalAlmacen.on('shown.bs.modal', function () {
                    if (!$formularioAlmacen.find("[name='ID']").val()) {
                        $formularioAlmacen.AgregarCamposDefectoAuditoria();
                        $formularioAlmacen.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalAlmacen.form.eventos.reset();
                $formularioAlmacen.trigger("reset");
                $formularioAlmacen.find(":input").attr("disabled", false);
              //$formularioAlmacen.find("[name='IDTRBJDR']").val();
                $formularioAlmacen.find("[name='IDTRBJDR']").text("");

            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        tipoAlmacen: function () {
            $selectTipoAlmacen.LlenarSelectGD("GDTALMCN", "vlR1", "dgddtlle");
            $selectFiltroTipoAlmacen.on("change", function () {
                tablaAlmacen.reload();
            });
        },
        trabajadoralmacen: function () {
            $selectTrabajador.select2({
                ajax: {
                    url: '/select-trabajador',
                    delay: 500,
                    data: function (params) {
                        var query = {
                            page: params.page || 1,
                            term: params.term
                        };
                        return query;
                    }
                },
                language: {
                    noResults: function () {
                        return "No hay resultados";
                    },
                    searching: function () {
                        return "Buscando..";
                    },
                    loadingMore: function () {
                        return 'Cargando más resultados..';
                    },
                    inputTooShort: function (args) {
                        var remainingChars = args.minimum - args.input.length;
                        var message = 'Por favor ingresar ' + remainingChars + ' o más caracteres';

                        return message;
                    },
                },
                width: 'resolve',
                dropdownParent: $modalAlmacen,
                minimumInputLength: -1,
                placeholder: 'Seleccione Responsable',
                allowClear: false
            });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.tipoAlmacen();
            selects.trabajadoralmacen();
        }
    };
    return {
        init: function () {
            selects.init();
            tablaAlmacen.inicializador();
            modalAlmacen.init();

            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarAlmacen.init();
})

