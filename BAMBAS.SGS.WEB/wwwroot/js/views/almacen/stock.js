﻿var InicializarAlmacen = function () {
    //variables JQuery    //variables JQuery
    var $tablaAlmacen = $("#tabla_almacen");
    var $IdAlmacen = $("#IdAlmacen");
    //
    var $accesoActualizacionStock = $("#accesoActualizacionStock");
    var $btnStock = $("#btnStock");
    var $btnGrabar = $("#btnGrabar");
    var validacionControles = {
        init: function () {
            if ($accesoActualizacionStock.val() == "False") {
                $btnStock.remove();
            }
        }
    };

    var tablaAlmacen = {
        objeto: null,
        pageLength: 50,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/stock/get",
                type: "GET",
                data: function (data) {
                    data.idalmcn = $IdAlmacen.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },

                {
                    title: "Cod. SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Artículo",
                    className: "text-center",
                    data: "dscrpcnartclo",
                    orderable: false
                },
                {
                    title: "Unid. Medida",
                    className: "text-center",
                    data: "unddmdda",
                    orderable: false
                },
                {
                    title: "Stock Actual",
                    className: "text-center",
                    data: "stck",
                    orderable: false
                },
                {
                    title: "Stock Mínimo",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '15%',
                    render: function (data) {
                        var tpm = "";
                        if (data.stckmnmo) {
                            tpm += '<input data-id="' + data.idartclo + '" data-anterior="' + data.stckmnmo + '" style="width:80%" type="number" min="0" class="stock-minimo stock-minimo-' + data.idartclo + ' text-center text-uppercase solonumeros form-control form-control-sm" disabled="disabled" value="' + data.stckmnmo + '" />';
                        } else {
                            tpm += '<input data-id="' + data.idartclo + '" data-anterior="' + data.stckmnmo + '" style="width:80%" type="number" min="0" class="stock-minimo stock-minimo-' + data.idartclo + ' text-center text-uppercase solonumeros form-control form-control-sm" disabled="disabled" />';
                        }
                        return tpm;
                    }
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Editar",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return `<button data-id="${data.idartclo}" class="btn btn-info btn-xs btn-editar btn-stock-minimo-${data.idartclo}" title="Editar"><span><i class="la la-edit"></i></button>`;
                    }
                },
            ]
        },
        eventos: function () {
            tablaAlmacen.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                var hasClass = $(this).hasClass("btn-info");
                var input = ".stock-minimo-" + id;
                var btn = $tablaAlmacen.find(".btn-stock-minimo-" + id);
                if (hasClass) {
                    $tablaAlmacen.find(input).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anterior = $tablaAlmacen.find(input).data("anterior");
                    $tablaAlmacen.find(input).val(anterior);
                    $tablaAlmacen.find(input).attr("disabled", true);
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaAlmacen.objeto = $tablaAlmacen.DataTable(tablaAlmacen.opciones);
            tablaAlmacen.eventos();
        }
    };

    var validacionFunciones = {
        init: function () {
            var stocksMinimos = [];
            $btnGrabar.on("click", function () {
                $("#tabla_almacen tbody tr").each(function (index) {
                    var stock = $(this).find(".stock-minimo").val();
                    var idarticulo = $(this).find(".stock-minimo").data("id");
                    stocksMinimos.push(
                        {
                            STCKMNMO: stock,
                            IDARTCLO: idarticulo,
                        }
                    )
                    /////
                });
                var formData = new FormData();
                formData.append("STOCKS", JSON.stringify(stocksMinimos));
                formData.append("IDALMCN", $IdAlmacen.val());
                $.ajax({
                    url: "/stock/insertar",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false
                }).done(function () {
                    Swal.fire({
                        icon: "success",
                        allowOutsideClick: false,
                        title: "Éxito",
                        text: "Se registró satisfactoriamente.",
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        tablaAlmacen.reload();  
                        //funcionesPersona.obtenerPersona($hfCodPersona.val());
                    });
                }).fail(function (e) {
                    Swal.fire({
                        icon: "error",
                        title: "Error al insertar el registro.",
                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                        confirmButtonText: "Aceptar"
                    });
                });
                $btnGrabar.attr("disabled", false);
            });
        }
    }
    return {
        init: function () {
            tablaAlmacen.inicializador();
            validacionFunciones.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarAlmacen.init();
})

