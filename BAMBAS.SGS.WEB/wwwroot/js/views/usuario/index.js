﻿var InicializarUsuario = function () {
    //Tab1
    var $btnNuevo = $(".btn-nuevo");
    var $tablaUsuario = $("#tabla_Usuario");
    var $hfaction = $("#hfaction");

    //Tab2
    var $btnAtras = $(".btn-Atras");
    var $btnGuardar = $(".btn-save");
    var $btnBuscarPersona = $(".btn-BPersona");
    var $btnCerrarPopup = $("#btnCerrarPopup");
    var $ControlesPersona = $(".datoPersona");
    var $tabla_Persona = $("#tabla_Persona");

    //campos
    var $txtPNombre = $("#txtPNombre");
    var $txtSNombre = $("#txtSNombre");
    var $txtUsuario = $("#txtUsuario");
    var $txtAPaterno = $("#txtAPaterno");
    var $txtAMaterno = $("#txtAMaterno");
    var $txtClave = $("#txtClave");
    var $txtRSocial = $("#txtRSocial");
    var $tipoDocumento = $("#cboTipoDocumento");
    var $tipotelefono = $("#cboTipoTelefono");
    var $txtNDocumento = $("#txtNDocumento");
    var $txtNTelefono = $("#txtNTelefono");
    var $txtFVencimiento = $("#txtFVencimiento");
    var $chkBloqueo = $("#chkBloqueo");
    var $chkForzar = $("#chkForzar");
    var $chkContratista = $("#chkContratista");
    var $selectEstados = $(".select-estados");
    var $selectEmpresas = $(".select-empresas");

    var $formUsuario = $("#usuario_form");
    // $modalBuscarPersona = $("#modal_buscar_persona");
    var $txtNombreRazon = $("#txtNombreRazon");
    var $txtNumDocumento = $("#txtNumDocumento");
    var $cbo_TipoDocumento = $("#cbo_TipoDocumento");
    var $cboTPersonaNJ = $("#cboTPersonaNJ");
    var $btnBuscarPersonaPopup = $("#btnBuscarPersonaPopup");

    //Tab3
    var $tablaConfiguracion = $("#tabla_Configuracion");
    var $btnGuardarPerfilPorUsuario = $("#guardar-perfilPorUsuario");

    //tab4
    var $tablaSucursales = $("#tabla_sucursalUsuario");
    var $btnGuardarSucursalesPorUsuario = $("#guardar-sucursalesPorUsuario");

    //Tabs
    var $tab_usuarios = $("#listausuario-tab");
    var $tab_datousuarios = $("#datousuario-tab");
    var $tab_configuracion = $("#configuracion-tab");
    var $tab_sucursales = $("#sucursalUsuarios-tab");

    var $accesoAgregarUsuario = $("#accesoAgregarUsuario");
    var $accesoEditarUsuario = $("#accesoEditarUsuario");
    var $accesoEliminarUsuario = $("#accesoEliminarUsuario");
    var $accesoPerfilesUsuario = $("#accesoPerfilesUsuario");

    //inputs
    var $personaN = $("#personaNatural");
    var $personaJ = $("#personaJuridica");

    //
    var $divContrasena = $(".showPass");

    //Listadoa
    var entidadUsuario = {
        csda: "",
        amtrno: "",
        aptrno: "",
        cFCRCN: "",
        cFEDCN: "",
        cFESTDO: "",
        ccrreo: "",
        clve: "",
        cusro: "",
        drccn: "",
        fblquo: "",
        frzrcmboclve: "",
        fcrcn: "",
        fedcn: "",
        festdo: "",
        fvclve: "",
        gddcmnto: "",
        gdestdo: "",
        gdtcrreo: "",
        gdtdrccn: "",
        gdttlfno: "",
        id: "",
        idprsna: "",
        ndcmnto: "",
        ntlfno: "",
        nyapllds: "",
        pnmbre: "",
        snmbre: "",
        ucrcn: "",
        uedcn: ""
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarUsuario.val() == "False") {
                $btnNuevo.remove();
            }
            if ($accesoEditarUsuario.val() == "False") {
                $btnGuardar.remove();
            }
            if ($accesoPerfilesUsuario.val() == "False") {
                $btnGuardarPerfilPorUsuario.remove();
            }
        }
    };

    var tablaUsuario = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/usuario/listarusuarios",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                },
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Apellidos y Nombres / Razón Social",
                    data: "nyapllds",
                    width: '40%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Cód. Usuario",
                    data: "cusro",
                    width: '20%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarUsuario.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar"  data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarUsuario.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaUsuario.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/usuario/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaUsuario.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaUsuario.objecto.on("click", ".btn-editar", function () {
                $hfaction.val("E");
                $txtClave.prop("required", false);
                $btnBuscarPersona.hide();
                $formUsuario.find(":input").attr("disabled", true);
                var data = tablaUsuario.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.cusro, "rastrousuario");

                $tab_datousuarios.removeClass("disabled");
                $tab_datousuarios.click();



                $.ajax({
                    url: `/usuario/obtenerusuarioprincipal`,
                    data: {
                        id: $(this).data("id")
                    },
                    type: "Get"
                }).done(function (data) {
                    entidadUsuario = data;
                    $formUsuario.find("[name='ID']").val(entidadUsuario.id);
                    $txtUsuario.val(entidadUsuario.cusro);
                    $txtClave.val("");
                    $txtFVencimiento.val(entidadUsuario.fvclve);

                    $chkBloqueo.prop('checked', (entidadUsuario.fblquo == "1"));
                    $chkForzar.prop('checked', (entidadUsuario.frzrcmboclve == "1"));
                    $chkContratista.prop('checked', (entidadUsuario.fcntrtsta == "1"));


                    if (entidadUsuario.rscl) {
                        $personaN.hide();
                        $txtPNombre.val("");
                        $txtSNombre.val("");
                        $txtAPaterno.val("");
                        $txtAMaterno.val("");
                        $personaJ.show();
                        $txtRSocial.val(entidadUsuario.rscl);
                    } else {
                        $personaJ.hide();
                        $txtRSocial.val("");
                        $personaN.show();
                        $txtPNombre.val(entidadUsuario.pnmbre);
                        $txtSNombre.val(entidadUsuario.snmbre);
                        $txtAPaterno.val(entidadUsuario.aptrno);
                        $txtAMaterno.val(entidadUsuario.amtrno);
                    }

                    //INDICADOR QUE DEBE VENIR DE BD
                    if (entidadUsuario.fcntrtsta == "1") {
                        //MOSTRAR CONTRASEÑA, FECHA DE VENCIMIENTO, INDICADOR DE BLOQUEO, INDICADOR DE CAMBIO DE CONTRASEÑA,
                        $divContrasena.show();
                        $txtClave.attr("required", true);
                        $txtFVencimiento.parent().show();
                        $chkBloqueo.parent().show();
                        $chkForzar.parent().show();
                        $chkContratista.prop('checked', true);
                    } else {
                        //OCULTAR CONTRASEÑA, FECHA DE VENCIMIENTO, INDICADOR DE BLOQUEO, INDICADOR DE CAMBIO DE CONTRASEÑA,
                        //Y HACER NO REQUERIDOS/FALSOS los campos
                        $divContrasena.hide();
                        $txtClave.attr("required", false);
                        $txtFVencimiento.parent().hide();
                        $chkBloqueo.parent().hide();
                        $chkForzar.parent().hide();
                        $chkContratista.prop('checked', false);
                    }

                    $tipoDocumento.val(entidadUsuario.gddcmnto);
                    $txtNDocumento.val(entidadUsuario.ndcmnto);
                    $tipotelefono.val(entidadUsuario.gdttlfno);
                    $txtNTelefono.val(entidadUsuario.ntlfno);
                    $formUsuario.AgregarCamposAuditoria(entidadUsuario);
                }).fail(function (e) {
                }).always(function () {
                    $formUsuario.find("[name='FVCLVE']").attr("disabled", true);
                    $formUsuario.find(":input").attr("disabled", false);
                    $formUsuario.find("[name='GDDCMNTO']").attr("disabled", true);
                    $formUsuario.find("[name='GDTTLFNO']").attr("disabled", true);
                    $formUsuario.find("[name='CUSRO']").prop("readonly", true);
                    $ControlesPersona.prop("readonly", true);
                    $formUsuario.DeshabilitarCamposAuditoria();

                    $tab_configuracion.removeClass("disabled");
                    $tab_sucursales.removeClass("disabled");
                });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            tablaUsuario.objecto = $tablaUsuario.DataTable(tablaUsuario.opciones);
            tablaUsuario.eventos();
        }
    };

    var configVentanaUsuario = {
        form: {
            objeto: $formUsuario.validate({
                rules: {
                    CLVE: {
                        required: true,
                        minlength: 8
                    }
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    if ($hfaction.val() == "N" && !$txtUsuario.val()) {
                        Swal.fire({
                            icon: "info",
                            title: "Información.",
                            text: "Por favor primero seleccione una persona.",
                            confirmButtonText: "Aceptar"
                        });

                        return;
                    }

                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formUsuario.find(":input").attr("disabled", true);
                    var url = "";
                    if ($hfaction.val() == "N") {
                        url = `/usuario/insertarusuario`;
                    } else {
                        url = `/usuario/actualizarusuario`;
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaUsuario.reload();
                                //configVentanaUsuario.eventos.reset();

                                //$tab_usuarios.click();
                                //$tab_sucursal.addClass("disabled");                                
                                if ($hfaction.val() == "N") {
                                    $formUsuario.find("[name='ID']").val(e.codEstado); // e : retorno codigo del usuario insertado
                                } else {
                                    $formUsuario.find("[name='ID']").val(e);
                                }

                                if ($hfaction.val() == "N") {
                                    $hfaction.val('E');
                                }
                                $tab_configuracion.removeClass("disabled");
                                $tab_sucursales.removeClass("disabled");
                                $tab_configuracion.click();
                            });
                        })
                        .fail(function (e) {
                            //$formUsuario.find("[name='ID']").val("");

                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formUsuario.find(":input").attr("disabled", false);

                            $formUsuario.find("[name='GDDCMNTO']").attr("disabled", true);
                            $formUsuario.find("[name='GDTTLFNO']").attr("disabled", true);
                            $formUsuario.find("[name='CUSRO']").prop("readonly", false);
                            $ControlesPersona.prop("readonly", true);
                            $formUsuario.DeshabilitarCamposAuditoria();
                        });

                }
            }),
            eventos: {
                reset: function () {
                    configVentanaUsuario.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            reset: function () {
                configVentanaUsuario.form.eventos.reset();
                $formUsuario.trigger("reset");
                $formUsuario.find(":input").attr("disabled", false);
            }
        },
        init: function () {
        }
    }

    var tablaPersona = {
        objecto: null,
        opciones: {
            filter: false,
            ajax: {
                dataType: "JSON",
                url: "/usuario/listarPersonas",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.gdtprsna = $cboTPersonaNJ.val();
                    data.tipoDocumento = $cbo_TipoDocumento.val();
                    data.NumDocumento = $txtNumDocumento.val();
                    data.buscarNombreApellido = $txtNombreRazon.val();
                    data.estado = 'A';
                },
                error: function (xhr, error, thrown) {

                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Tipo Persona",
                    data: "tprsna",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Nombres y apellidos /Razón",
                    data: "datos",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Tipo Documento",
                    data: "gddcmnto",
                    width: '2%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Nro. Documento",
                    data: "ndcmnto",
                    width: '2%',
                    orderable: false,
                    className: "text-center"
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaPersona.objecto.on("click", "tr", function () {
                var data = tablaPersona.objecto.row(this).data();

                $formUsuario.find("[name='IDPRSNA']").val(data.id);
                if (data.gdtprsna == 2) {
                    $personaN.hide();
                    $txtPNombre.val("");
                    $txtSNombre.val("");
                    $txtAPaterno.val("");
                    $txtAMaterno.val("");
                    $personaJ.show();
                    $txtRSocial.val(data.datos).show();
                    $formUsuario.find("[name='CUSRO']").prop("readonly", false);
                } else {
                    $personaJ.hide();
                    $txtRSocial.val("");
                    $personaN.show();
                    $txtPNombre.val(data.pnmbre).show();
                    $txtSNombre.val(data.snmbre).show();
                    $txtAPaterno.val(data.aptrno).show();
                    $txtAMaterno.val(data.amtrno).show();
                    $formUsuario.find("[name='CUSRO']").prop("readonly", false);
                }
                //$txtUsuario.val(data.pusuario); //TIENEN QUE INGRESARLO ELLOS

                $txtFVencimiento.val(data.fvncmnto).trigger("change");
                $chkForzar.attr("checked", true);
                if (data.mpepE02 != '') {
                    var telefono = data.mpepE02.split("*");
                    $tipotelefono.val(telefono[0]);
                    $txtNTelefono.val(telefono[1]);

                }
                if (data.mpepE04 != '') {
                    var documento = data.mpepE04.split("*");
                    $tipoDocumento.val(documento[0]);
                    $txtNDocumento.val(documento[1]);
                    $formUsuario.find("[name='TDCMNTO']").val(documento[0]);
                }
                //INDICADOR QUE DEBE VENIR DE BD
                if (data.fcntrtsta) {
                    //MOSTRAR CONTRASEÑA, FECHA DE VENCIMIENTO, INDICADOR DE BLOQUEO, INDICADOR DE CAMBIO DE CONTRASEÑA,
                    $divContrasena.show();
                    $txtClave.attr("required", true);
                    $txtFVencimiento.parent().show();
                    $chkBloqueo.parent().show();
                    $chkForzar.parent().show();
                    $chkContratista.prop('checked', true);
                } else {
                    //OCULTAR CONTRASEÑA, FECHA DE VENCIMIENTO, INDICADOR DE BLOQUEO, INDICADOR DE CAMBIO DE CONTRASEÑA,
                    //Y HACER NO REQUERIDOS/FALSOS los campos
                    $divContrasena.hide();
                    $txtClave.attr("required", false);
                    $txtFVencimiento.parent().hide();
                    $chkBloqueo.parent().hide();
                    $chkForzar.parent().hide();
                    $chkContratista.prop('checked', false);
                }
                $chkBloqueo.prop('checked', false);
                $chkForzar.prop('checked', false);

                $btnCerrarPopup.click();
            });

        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            if (tablaPersona.objecto == undefined || tablaPersona.objecto == null) {
                tablaPersona.objecto = $tabla_Persona.DataTable(tablaPersona.opciones);
                tablaPersona.eventos();
            } else {
                tablaPersona.reload();
            }
        }
    };

    //Configuración
    var tablaConfiguracion = {
        asignados: [],
        noAsignados: [],
        objecto: null,
        opciones: {
            ajax: {
                gatatype: "JSON",
                url: "/perfil-usuario/obtener-perfiles",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idUsuario = $formUsuario.find("[name='ID']").val();

                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Asignado",
                    data: null,
                    orderable: false,
                    width: '11%',
                    class: "text-center",
                    render: function (data) {
                        var checkeado = false;
                        //verificar que no exista ni en una ni en la otra para agregarlo
                        if ($.inArray(data.id, tablaConfiguracion.asignados) == -1 && $.inArray(data.id, tablaConfiguracion.noAsignados) == -1) {
                            if (data.checkeado) {
                                checkeado = true;
                                tablaConfiguracion.asignados.push(data.id);
                            } else {
                                checkeado = false;
                                tablaConfiguracion.noAsignados.push(data.id);
                            }
                        } else {//si existe, buscarlo
                            if ($.inArray(data.id, tablaConfiguracion.asignados) == -1) {
                                checkeado = false;
                            }
                            else {
                                checkeado = true;
                            }
                        }
                        //
                        var tpm = `<label class="switch">
                                  <input type="checkbox" ${checkeado ? "checked" : ""} ${$accesoPerfilesUsuario.val() == "False" ? "disabled" : ""} class="check-asignado" data-id="${data.id}">
                                  <span class="slider"></span>
                                </label>`;
                        return tpm;
                    }
                },
                //{
                //    title: "ID",
                //    data: "id", width: '5%',
                //    orderable: true
                //},
                {
                    title: "Nombres",
                    data: "dprfl",
                    width: '20%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Fec. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            asignar: function () {
                tablaConfiguracion.objecto.on("click", ".check-asignado", function () {
                    var idperfil = $(this).data("id");
                    var checkeando = $(this).is(':checked');

                    if ($.inArray(idperfil, tablaConfiguracion.asignados) == -1 && $.inArray(idperfil, tablaConfiguracion.noAsignados) == -1) {
                        if (checkeando) {
                            tablaConfiguracion.asignados.push(idperfil);
                        } else {
                            tablaConfiguracion.noAsignados.push(idperfil);
                        }
                    } else {//si existe, buscarlo
                        if ($.inArray(idperfil, tablaConfiguracion.asignados) == -1 && checkeando) {//si no esta asignado y esta chequeado
                            //buscarlo en no asignados y eliminarlo
                            tablaConfiguracion.noAsignados.splice($.inArray(idperfil, tablaConfiguracion.noAsignados), 1);
                            //agregarlo al asignados
                            tablaConfiguracion.asignados.push(idperfil);
                        }
                        else {
                            //buscarlo en asignados y eliminarlo
                            tablaConfiguracion.asignados.splice($.inArray(idperfil, tablaConfiguracion.asignados), 1);
                            //agregarlo al NO asignados
                            tablaConfiguracion.noAsignados.push(idperfil);
                        }
                    }
                });
            },
            init: function () {
                this.asignar();
            }
        },
        reload: function () {
            if (tablaConfiguracion.objecto != null && tablaConfiguracion.objecto != undefined) {
                tablaConfiguracion.objecto.ajax.reload();
            } else {
                tablaConfiguracion.init();
            }
        },
        init: function () {
            tablaConfiguracion.objecto = $tablaConfiguracion.DataTable(tablaConfiguracion.opciones);
            this.eventos.init();
        }
    };

    var eventoGuardarPerfilPorUsuario = {
        init: function () {
            $btnGuardarPerfilPorUsuario.on("click", function () {
                $btn = $(this);
                if ($formUsuario.find("[name='ID']").val() != null && $formUsuario.find("[name='ID']").val() != 0 && $formUsuario.find("[name='ID']").val() != "") {
                    $btn.addLoader();
                    $.ajax({
                        type: "POST",
                        url: '/perfil-usuario/guardarporusuario',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            asignados: tablaConfiguracion.asignados,
                            noAsignados: tablaConfiguracion.noAsignados,
                            idUsuario: $formUsuario.find("[name='ID']").val()
                        })
                    })
                        .done(function (e) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registros actualizados satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaConfiguracion.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al actualizar registros.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                        });
                } else {
                    Swal.fire({
                        icon: "info",
                        title: "Información.",
                        text: "Por favor seleccione un Usuario",
                        confirmButtonText: "Aceptar"
                    });
                }
            });
        }
    }
    //Sucursales

    var tablaSucursales = {
        asignadas: [],
        noAsignadas: [],
        objecto: null,
        opciones: {
            ajax: {
                gatatype: "JSON",
                url: "/sucursal-usuario/obtener-sucursales",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idUsuario = $formUsuario.find("[name='ID']").val();
                    data.idEmpresa = $selectEmpresas.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Asignado",
                    data: null,
                    orderable: false,
                    width: '11%',
                    class: "text-center",
                    render: function (data) {
                        var checkeado = false;
                        //verificar que no exista ni en una ni en la otra para agregarlo
                        if ($.inArray(data.id, tablaSucursales.asignadas) == -1 && $.inArray(data.id, tablaSucursales.noAsignadas) == -1) {
                            if (data.checkeado) {
                                checkeado = true;
                                tablaSucursales.asignadas.push(data.id);
                            } else {
                                checkeado = false;
                                tablaSucursales.noAsignadas.push(data.id);
                            }
                        } else {//si existe, buscarlo
                            if ($.inArray(data.id, tablaSucursales.asignadas) == -1) {
                                checkeado = false;
                            }
                            else {
                                checkeado = true;
                            }
                        }
                        //${$accesoPerfilesUsuario.val() == "False" ? "disabled" : ""}
                        var tpm = `<label class="switch">
                                  <input type="checkbox" ${checkeado ? "checked" : ""}  class="check-asignado" data-id="${data.id}">
                                  <span class="slider"></span>
                                </label>`;
                        return tpm;
                    }
                },
                {
                    title: "Nombre",
                    data: "nscrsl",
                    width: '20%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Fec. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            asignar: function () {
                tablaSucursales.objecto.on("click", ".check-asignado", function () {
                    var idSucursal = $(this).data("id");
                    var checkeando = $(this).is(':checked');

                    if ($.inArray(idSucursal, tablaSucursales.asignadas) == -1 && $.inArray(idSucursal, tablaSucursales.noAsignadas) == -1) {
                        if (checkeando) {
                            tablaSucursales.asignadas.push(idSucursal);
                        } else {
                            tablaSucursales.noAsignadas.push(idSucursal);
                        }
                    } else {//si existe, buscarlo
                        if ($.inArray(idSucursal, tablaSucursales.asignadas) == -1 && checkeando) {//si no esta asignado y esta chequeado
                            //buscarlo en no asignadas y eliminarlo
                            tablaSucursales.noAsignadas.splice($.inArray(idSucursal, tablaSucursales.noAsignadas), 1);
                            //agregarlo al asignadas
                            tablaSucursales.asignadas.push(idSucursal);
                        }
                        else {
                            //buscarlo en asignadas y eliminarlo
                            tablaSucursales.asignadas.splice($.inArray(idSucursal, tablaSucursales.asignadas), 1);
                            //agregarlo al NO asignadas
                            tablaSucursales.noAsignadas.push(idSucursal);
                        }
                    }
                });
            },
            init: function () {
                this.asignar();
            }
        },
        reload: function () {
            if (tablaSucursales.objecto != null && tablaSucursales.objecto != undefined) {
                tablaSucursales.objecto.ajax.reload();
            } else {
                tablaSucursales.init();
            }
        },
        init: function () {
            tablaSucursales.objecto = $tablaSucursales.DataTable(tablaSucursales.opciones);
            this.eventos.init();
        }
    };

    var eventoGuardarSucursalesPorUsuario = {
        init: function () {
            $btnGuardarSucursalesPorUsuario.on("click", function () {
                $btn = $(this);
                if ($formUsuario.find("[name='ID']").val() != null && $formUsuario.find("[name='ID']").val() != 0 && $formUsuario.find("[name='ID']").val() != "") {
                    $btn.addLoader();
                    $.ajax({
                        type: "POST",
                        url: '/sucursal-usuario/guardar-sucursales-usuario',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            asignadas: tablaSucursales.asignadas,
                            noAsignadas: tablaSucursales.noAsignadas,
                            idUsuario: $formUsuario.find("[name='ID']").val()
                        })
                    })
                        .done(function (e) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registros actualizados satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaSucursales.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al actualizar registros.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                        });
                } else {
                    Swal.fire({
                        icon: "info",
                        title: "Información.",
                        text: "Por favor seleccione un Usuario",
                        confirmButtonText: "Aceptar"
                    });
                }
            });
        }
    };

    var eventosIncrustados = {
        tabs: function () {
            document.querySelector('.showPass i').addEventListener('click', e => {
                const passwordInput = document.querySelector('#txtClave');
                if (e.target.classList.contains('fa-eye')) {
                    e.target.classList.remove('fa-eye');
                    e.target.classList.add('fa-times');
                    passwordInput.type = 'text';
                } else {
                    e.target.classList.remove('fa-times');
                    e.target.classList.add('fa-eye');
                    passwordInput.type = 'password';
                }
            });
            $tab_usuarios.on("click", function () {
                $hfaction.val("");
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("rastrousuario");
                }
                $tab_datousuarios.addClass("disabled");
                $tab_configuracion.addClass("disabled");
                $tab_sucursales.addClass("disabled");
                configVentanaUsuario.eventos.reset();
            });
            $tab_datousuarios.on("click", function () {
                $txtUsuario.attr("required", true);
                $personaJ.hide();
                $personaN.show();
            });

            $tab_configuracion.on("click", function () {
                //recargar tabla
                tablaConfiguracion.reload();

                //limpiar arrays 
                tablaConfiguracion.asignados = [];
                tablaConfiguracion.noAsignados = [];
            });
            $tab_sucursales.on("click", function () {
                //recargar tabla
                $selectEmpresas.val("");
                tablaSucursales.reload();

                //limpiar arrays 
                tablaSucursales.asignados = [];
                tablaSucursales.noAsignadas = [];
            });
        },
        NuevoUsuario: function () {
            $btnNuevo.click(function () {
                $hfaction.val("N");
                $txtClave.prop("required", true);
                $ControlesPersona.prop("readonly", true);
                $formUsuario.find("[name='GDDCMNTO']").attr("disabled", true);
                $formUsuario.find("[name='GDTTLFNO']").attr("disabled", true);
                $formUsuario.find("[name='CUSRO']").prop("readonly", true);

                $btnBuscarPersona.show();
                $formUsuario.AgregarCamposDefectoAuditoria();
                $formUsuario.DeshabilitarCamposAuditoria();
                $tab_datousuarios.removeClass("disabled");
                $tab_datousuarios.click();
            })
        },
        AtrasUsuario: function () {
            $btnAtras.click(function () {

                configVentanaUsuario.eventos.reset();
                $tab_usuarios.click();
            })
        },
        BuscarPersona: function () {
            $btnBuscarPersona.click(function () {
                tablaPersona.inicializador();
            })
        },
        BuscarPopup: function () {
            $btnBuscarPersonaPopup.click(function () {
                tablaPersona.reload();
            })
        },
        CambioFechaVencimiento: function () {
            $txtClave.on("change", function () {
                $chkForzar.prop('checked', true);
            });
            $txtFVencimiento.on("change", function () {
                var valF = false;
                var $this = $(this);
                var fIngreso = $this.val();

                var valmenor = eventosIncrustados.menorFechaActual(valF, fIngreso);
                if (!valmenor) {
                    $this.addClass("error");
                    $('#mssg0').text("La Fecha ingresada es menor a la fecha actual");
                } else {
                    $this.removeClass("error");
                    $('#mssg0').text("");
                }

                $chkForzar.prop('checked', true);
            });
            $chkBloqueo.on("change", function () {
                if ($chkBloqueo.is(':checked')) {
                    $chkForzar.prop('checked', false);
                }
                else {
                    $chkForzar.prop('checked', true);
                }
            });
        },
        menorFechaActual: function (valFecha, fecAdd, factual) {
            fechaCon = fecAdd;
            var valFecha;
            var fCon = fechaCon.split("/");
            var fConAnnio = fCon[2];
            var fConMes = fCon[1];
            var fConDia = fCon[0];

            var annioActual;
            var mesActual;
            var diaActual;
            if (factual) {
                var actualSplit = factual.split("/");
                annioActual = actualSplit[2];
                mesActual = actualSplit[1];
                diaActual = actualSplit[0];
            } else {
                factual = new Date();
                annioActual = parseInt(factual.getFullYear());
                mesActual = parseInt(factual.getMonth()) + 1;
                diaActual = parseInt(factual.getDate());
            }

            var annioServ = fConAnnio - annioActual;
            var mesServ = fConMes - mesActual;

            if (mesServ > 0 || (mesServ === 0 && fConDia > diaActual)) {
                annioServ++;
            }
            if (annioServ > 0) {
                valFecha = true;
            }

            return valFecha;
        },
        init: function () {
            eventosIncrustados.tabs();
            eventosIncrustados.NuevoUsuario();
            eventosIncrustados.AtrasUsuario();
            eventosIncrustados.BuscarPersona();
            eventosIncrustados.BuscarPopup();
            eventosIncrustados.CambioFechaVencimiento();
        }
    }

    var selects = {
        empresas: function () {
            $.get(`/empresa/obtener-activas`)
                .done(function (data) {
                    $selectEmpresas.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectEmpresas.append($("<option />").val(item["id"]).text(item["nemprsa"]));
                    });
                });
            $selectEmpresas.on("change", function () {
                tablaSucursales.reload();
            });
        },
        init: function () {
            selects.empresas();
            $selectEstados.LlenarSelectEstados();
            $tipoDocumento.LlenarSelectTipoDocumento();
            $cbo_TipoDocumento.LlenarSelectTipoDocumento();
            $cboTPersonaNJ.LlenarSelectTipoPersona();
            $tipotelefono.LlenarSelectTipoTelefono();
        }
    };

    const configCboTipoDocumentos = {
        optDocumento: {
            obj: $txtNumDocumento[0].id,
            param: [
                { val: "1", mlength: "8", minLength: "8" },
                { val: "8", mlength: "11", minLength: "11" },
                { val: "2", mlength: "15", minLength: null },
                { val: "3", mlength: "15", minLength: null },
                { val: "6", mlength: "6", minLength: null },
                { val: "9", mlength: "15", minLength: null }]
        },
        init: function () {
            $cbo_TipoDocumento.maxlengthDocumento(configCboTipoDocumentos.optDocumento);
        }
    };

    return {
        init: function () {
            tablaUsuario.inicializador();
            configVentanaUsuario.init();
            eventosIncrustados.init();
            selects.init();
            eventoGuardarPerfilPorUsuario.init();
            eventoGuardarSucursalesPorUsuario.init();
            validacionControles.init();
            configCboTipoDocumentos.init();
        }
    };

}();



$(function () {
    InicializarUsuario.init();
})