﻿var InicializarFamilia = function () {
    var $selectEstados = $(".select-estados");
    var $selectFamilias = $(".select-familias");
    var $filtroFamilias = $(".filtro-select-familias");
    //variables JQuery    //variables JQuery
    var $tablaFamilia = $("#tabla_familia");
    var $formularioFamilia = $("#familia_form");
    var $modalFamilia = $("#modal_familia");
    //
    var $accesoAgregarFamilia = $("#accesoAgregarFamilia");
    var $accesoEditarFamilia = $("#accesoEditarFamilia");
    var $accesoEliminarFamilia = $("#accesoEliminarFamilia");

    var $btnNuevaFamilia = $("#btnNuevaFamilia");

    var entidadFamilia = {
        id: "",
        dscrpcn: "",
        idfmlapdre: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
    }
    var validacionControles = {
        init: function () {
            if ($accesoAgregarFamilia.val() == "False") {
                $btnNuevaFamilia.remove();
            }
        }
    };

    var tablaFamilia = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/familia/get",
                type: "GET",
                data: function (data) {
                    data.idFamilia = 0;
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    width: '40%',
                    orderable: false
                },
                {
                    title: "U.Edición",
                    className: "text-center",
                    data: "uedcn",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarFamilia.val() == "True") {
                            
                            tpm += `<a data-toggle="modal" href="#modal_familia" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarFamilia.val() == "True") {
                            
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaFamilia.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/familia/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaFamilia.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaFamilia.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioFamilia.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/familia/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadFamilia = result;
                        $formularioFamilia.find("[name='DSCRPCN']").val(entidadFamilia.dscrpcn);


                        $formularioFamilia.AgregarCamposAuditoria(entidadFamilia);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioFamilia.find(":input").attr("disabled", false);
                        $formularioFamilia.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaFamilia.objeto = $tablaFamilia.DataTable(tablaFamilia.opciones);
            tablaFamilia.eventos();
        }
    };
    var modalFamilia = {
        form: {
            objeto: $formularioFamilia.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    formData.append("IDFMLAPDRE", null);
                    $formularioFamilia.find(":input").attr("disabled", true);

                    if (!$formularioFamilia.find("[name='ID']").val()) {
                        url = `/familia/insertar`
                    } else {
                        url = `/familia/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaFamilia.reload();
                                modalFamilia.eventos.reset();
                                $modalFamilia.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioFamilia.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalFamilia.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalFamilia.on('hidden.bs.modal', function () {
                    modalFamilia.eventos.reset();
                    $formularioFamilia.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalFamilia.on('shown.bs.modal', function () {
                    if (!$formularioFamilia.find("[name='ID']").val()) {
                        $formularioFamilia.AgregarCamposDefectoAuditoria();
                        $formularioFamilia.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalFamilia.form.eventos.reset();
                $formularioFamilia.trigger("reset");
                $formularioFamilia.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var eventosIncrustados = {
        filtroFamilias: function () {
            $filtroFamilias.on("change", function () {
                tablaFamilia.reload();
            });
        },
        init: function () {
            this.filtroFamilias();
        }
    }

    var selects = {
        familias: function () {
            $.get("/familia/obtener-activas")
                .done(function (data) {
                    $selectFamilias.append($("<option />").val(-1).text("TODOS"));
                    $selectFamilias.append($("<option />").val('').text("NINGUNO"));
                    $.each(data, function (key, item) {
                        $selectFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.familias();
        }
    };
    return {
        init: function () {
            selects.init();
            tablaFamilia.inicializador();
            modalFamilia.init();

            //validacionControles.init();
            eventosIncrustados.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarFamilia.init();
})

