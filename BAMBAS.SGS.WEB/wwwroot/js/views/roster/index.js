﻿var InicializarRoster = function () {
    var $selectEstados = $(".select-estados");
    //variables JQuery    //variables JQuery
    var $tablaRoster = $("#tabla_roster");
    var $formularioRoster = $("#roster_form");
    var $modalRoster = $("#modal_roster");
    //
    var $accesoAgregarRoster = $("#accesoAgregarRoster");
    var $accesoEliminarRoster = $("#accesoEliminarRoster");
    //
    var $checkHabilitar = $(".check-habilitar");
    var $btnRoster = $("#btnRoster");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarRoster.val() == "False") {
                $btnRoster.remove();
            }
        }
    };

    var entidadRoster = {
        id: "",
        dscrpcn: "",
        dtrbjds: "",
        ddscnso: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }
    var tablaRoster = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/roster/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Días Trabajados",
                    className: "text-center",
                    width: '11%',
                    data: "dtrbjds",
                    orderable: false
                },
                {
                    title: "Días Descansados",
                    className: "text-center",
                    width: '11%',
                    data: "ddscnso",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Ver",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '2%',
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a class="btn  btn-xs btn-ver" data-toggle="modal" data-target="#modal_roster" data-id="${data.id}" title="Ver"><i class="la la-eye"></i></a>`;

                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '14%',
                    render: function (data) {
                        var tpm = "";

                        if ($accesoEliminarRoster.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaRoster.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/roster/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaRoster.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaRoster.objeto.on("click", ".btn-ver", function () {
                var id = $(this).data("id");
                $formularioRoster.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/roster/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadRoster = result;
                        $formularioRoster.find("[name='DSCRPCN']").val(entidadRoster.dscrpcn);
                        $formularioRoster.find("[name='DTRBJDS']").val(entidadRoster.dtrbjds);
                        $formularioRoster.find("[name='DDSCNSO']").val(entidadRoster.ddscnso);
                        $formularioRoster.AgregarCamposAuditoria(entidadRoster);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioRoster.find(":input").attr("disabled", false);
                        $formularioRoster.find("[name='DSCRPCN']").attr("disabled", true);
                        $formularioRoster.find("[name='DTRBJDS']").attr("disabled", true);
                        $formularioRoster.find("[name='DDSCNSO']").attr("disabled", true);
                        $checkHabilitar.attr("disabled", true);
                        $formularioRoster.find("button[type='submit']").hide();
                        $formularioRoster.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaRoster.objeto = $tablaRoster.DataTable(tablaRoster.opciones);
            tablaRoster.eventos();
        }
    };
    var modalRoster = {
        form: {
            objeto: $formularioRoster.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioRoster.find(":input").attr("disabled", true);
                    
                    if (!$formularioRoster.find("[name='ID']").val()) {
                        url = `/roster/insertar`
                    } else {
                        url = `/roster/actualizar`
                    }
                   
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaRoster.reload();
                                modalRoster.eventos.reset();
                                $modalRoster.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioRoster.find(":input").attr("disabled", false);
                            $formularioRoster.find("[name='DSCRPCN']").attr("disabled", true);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalRoster.form.objeto.resetForm();
                }
            }
        },
        
        eventos: {
            onHide: function () {
                $modalRoster.on('hidden.bs.modal', function () {
                    modalRoster.eventos.reset();
                    $formularioRoster.find("[name='ID']").val("");
                    $formularioRoster.find("button[type='submit']").show();
                })
            },
            onShow: function () {
                $modalRoster.on('shown.bs.modal', function () {
                    $formularioRoster.find("[name='DSCRPCN']").attr("disabled", true);
                    if (!$formularioRoster.find("[name='ID']").val()) {
                        $formularioRoster.AgregarCamposDefectoAuditoria();
                        $formularioRoster.DeshabilitarCamposAuditoria();
                    }

                })
            },
            reset: function () {
                modalRoster.form.eventos.reset();
                $formularioRoster.trigger("reset");
                $formularioRoster.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    };
    var eventosIncrustados = {
        init: function () {
            $checkHabilitar.on("change", function () {
                var checkeado = $(this).is(":checked");

                $formularioRoster.find("[name='DSCRPCN']").attr("disabled", !checkeado);
            });
        }
    };
    return {
        init: function () {
            selects.init();
            tablaRoster.inicializador();
            modalRoster.init();
            eventosIncrustados.init();
            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarRoster.init();
})

