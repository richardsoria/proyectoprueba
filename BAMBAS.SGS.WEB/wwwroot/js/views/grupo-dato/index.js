﻿//var bandejaConvenio = (function ($, win, doc) {
var InicializarGrupoDato = function () {
    //variables JQuery
    var $tablaGrupoDato = $("#tabla_grupodato");
    var $formularioAgregarGrupoDato = $("#grupodato_agregar_form");
    var $formularioEditarGrupoDato = $("#grupodato_editar_form");
    var $modalAgregarGrupoDato = $("#modal_agregar_grupodato");
    var $modalEditarGrupoDato = $("#modal_editar_grupodato");
    //
    var $formularioAgregarGrupoDatoDetalle = $("#grupodatodetalle_agregar_form");
    var $formularioEditarGrupoDatoDetalle = $("#grupodatodetalle_editar_form");
    var $modalAgregarGrupoDatoDetalle = $("#modal_agregar_grupodatodetalle");
    var $modalEditarGrupoDatoDetalle = $("#modal_editar_grupodatodetalle");
    var $tablaGrupoDatoDetalle = $("#tabla_grupodatodetalle");
    //
    var $codigoGrupoDato = $("#codigoGrupoDato");
    var $tab_grupodato = $("#grupodato-tab");
    var $tab_grupodatoDetalle = $("#grupodatodetalle-tab");
    var $selectEstados = $(".select-estados");
    var $lblgrupodato = $("#lblgrupodato");

    //CONTROLES
    var $btnAgregarGrupoDato = $("#btnAgregarGrupoDato");
    var $btnAgregarGrupoDatoDetalle = $("#btnAgregarGrupoDatoDetalle");
    var $accesoAgregarGrupoDato = $("#accesoAgregarGrupoDato");
    var $accesoEditarGrupoDato = $("#accesoEditarGrupoDato");
    var $accesoEliminarGrupoDato = $("#accesoEliminarGrupoDato");
    var $accesoAgregarGrupoDatoDetalle = $("#accesoAgregarGrupoDatoDetalle");
    var $accesoEditarGrupoDatoDetalle = $("#accesoEditarGrupoDatoDetalle");
    var $accesoEliminarGrupoDatoDetalle = $("#accesoEliminarGrupoDatoDetalle");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarGrupoDato.val() == "False") {
                $btnAgregarGrupoDato.remove();
            }
            if ($accesoAgregarGrupoDatoDetalle.val() == "False") {
                $btnAgregarGrupoDatoDetalle.remove();
            }
        }
    };

    var tablaGrupoDato = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/grupo-dato/obtener-grupodato",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
               
                {
                    title: "Código",
                    data: "cgdto",
                    orderable: false
                },
                {
                    title: "Descripción",
                    data: "dgdto",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Abreviación",
                    data: "agdto",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Ver",
                    data: null,
                    className: "text-center",
                    orderable: false,
                    width: '2%',
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a data-codigo="${data.cgdto}" class="btn btn-xs btn-ver"><i class="la la-eye"></i></a>`;
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarGrupoDato.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_editar_grupodato" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarGrupoDato.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaGrupoDato.objecto.on("click", ".btn-ver", function () {
                var data = tablaGrupoDato.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.dgdto, "rastrogrupodato");
                //$lblgrupodato.text(data.dgdto);
                $codigoGrupoDato.val($(this).data("codigo"));
                tablaGrupoDatoDetalle.inicializador();
                $tab_grupodatoDetalle.removeClass("disabled");
                $tab_grupodatoDetalle.click();
            });
            tablaGrupoDato.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/grupo-dato/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaGrupoDato.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaGrupoDato.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioEditarGrupoDato.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/grupo-dato/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        $formularioEditarGrupoDato.find("[name='CGDTO']").val(result.cgdto);
                        $formularioEditarGrupoDato.find("[name='DGDTO']").val(result.dgdto);
                        $formularioEditarGrupoDato.find("[name='AGDTO']").val(result.agdto);
                        $formularioEditarGrupoDato.AgregarCamposAuditoria(result);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioEditarGrupoDato.find(":input").attr("disabled", false);
                        $formularioEditarGrupoDato.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            tablaGrupoDato.objecto = $tablaGrupoDato.DataTable(tablaGrupoDato.opciones);
            tablaGrupoDato.eventos();
        }
    };
    var modalAgregar = {
        form: {
            objeto: $formularioAgregarGrupoDato.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    CGDTO: {
                        required: true,
                        maxlength: 15,
                        alfanumerico: true
                    },
                    DGDTO: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                    AGDTO: {
                        required: true,
                        maxlength: 50,
                        alfanumerico: true,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioAgregarGrupoDato.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGrupoDato.reload();
                                modalAgregar.eventos.reset();
                                $modalAgregarGrupoDato.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioAgregarGrupoDato.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalAgregar.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalAgregarGrupoDato.on('hidden.bs.modal', function () {
                    modalAgregar.eventos.reset();
                })
            },
            onShow: function () {
                $modalAgregarGrupoDato.on('shown.bs.modal', function () {
                    $formularioAgregarGrupoDato.AgregarCamposDefectoAuditoria();
                    $formularioAgregarGrupoDato.DeshabilitarCamposAuditoria();
                })
            },
            reset: function () {
                modalAgregar.form.eventos.reset();
                $formularioAgregarGrupoDato.trigger("reset");
                $formularioAgregarGrupoDato.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var modalEditar = {
        form: {
            objeto: $formularioEditarGrupoDato.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    CGDTO: {
                        required: true,
                        maxlength: 15,
                        alfanumerico: true,
                    },
                    DGDTO: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                    AGDTO: {
                        required: true,
                        maxlength: 50,
                        alfanumerico: true,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioEditarGrupoDato.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGrupoDato.reload();
                                modalEditar.form.objeto.reset();
                                $modalEditarGrupoDato.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioEditarGrupoDato.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalEditar.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalEditarGrupoDato.on('hidden.bs.modal', function () {
                    modalEditar.eventos.reset();
                })
            },
            onShow: function () {
                $modalEditarGrupoDato.on('shown.bs.modal', function () {

                })
            },
            reset: function () {
                modalEditar.form.eventos.reset();
                $formularioEditarGrupoDato.trigger("reset");
                $formularioEditarGrupoDato.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    /////DETALLE
    var tablaGrupoDatoDetalle = {
        codigoGrupoDato: null,
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: `/grupo-dato-detalle/obtener-grupodatodetalle`,
                type: "GET",
                data: function (data) {
                    data.codigoGrupoDato = $codigoGrupoDato.val();

                    delete data.colums;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código",
                    data: "cgdto",
                    orderable: false
                },
                {                           
                    title: "Descripción",
                    data: "dgddtlle",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Abreviación",
                    data: "agddtlle",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Valor 1",
                    class: "text-center",
                    data: "vlR1",
                    orderable: false
                },
                {
                    title: "Valor 2",
                    class: "text-center",
                    data: "vlR2",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    class: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarGrupoDatoDetalle.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_editar_grupodatodetalle" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarGrupoDatoDetalle.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaGrupoDatoDetalle.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/grupo-dato-detalle/eliminar-detalle",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaGrupoDatoDetalle.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaGrupoDatoDetalle.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioEditarGrupoDatoDetalle.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/grupo-dato-detalle/editar-detalle/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        $formularioEditarGrupoDatoDetalle.find("[name='CGDTO']").val(result.cgdto);
                        $formularioEditarGrupoDatoDetalle.find("[name='DGDDTLLE']").val(result.dgddtlle);
                        $formularioEditarGrupoDatoDetalle.find("[name='AGDDTLLE']").val(result.agddtlle);
                        $formularioEditarGrupoDatoDetalle.find("[name='VLR1']").val(result.vlR1);
                        $formularioEditarGrupoDatoDetalle.find("[name='VLR2']").val(result.vlR2);
                        $formularioEditarGrupoDatoDetalle.AgregarCamposAuditoria(result);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioEditarGrupoDatoDetalle.find(":input").attr("disabled", false);
                        $formularioEditarGrupoDatoDetalle.find("[name='CGDTO']").attr("disabled", true);
                        $formularioEditarGrupoDatoDetalle.DeshabilitarCamposAuditoria();
                    });
            });
        },
        //////
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            if (tablaGrupoDatoDetalle.objecto == undefined || tablaGrupoDatoDetalle.objecto == null) {
                tablaGrupoDatoDetalle.objecto = $tablaGrupoDatoDetalle.DataTable(tablaGrupoDatoDetalle.opciones);
                tablaGrupoDatoDetalle.eventos();
            } else {
                tablaGrupoDatoDetalle.reload();
            }
        }
    };
    var modalAgregarDetalle = {
        form: {
            objeto: $formularioAgregarGrupoDatoDetalle.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    CGDTO: {
                        required: true,
                        maxlength: 15,
                        alfanumerico: true
                    },
                    DGDDTLLE: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                    AGDDTLLE: {
                        required: true,
                        maxlength: 50,
                        alfanumerico: true,
                    },
                    VLR1: {
                        required: true,
                        maxlength: 250
                    },
                    VLR2: {
                        maxlength: 250
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    formData.append("CGDTO", $codigoGrupoDato.val());
                    $formularioAgregarGrupoDatoDetalle.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGrupoDatoDetalle.reload();
                                modalAgregarDetalle.eventos.reset();
                                $modalAgregarGrupoDatoDetalle.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioAgregarGrupoDatoDetalle.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalAgregarDetalle.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalAgregarGrupoDatoDetalle.on('hidden.bs.modal', function () {
                    modalAgregarDetalle.eventos.reset();
                })
            },
            onShow: function () {
                $modalAgregarGrupoDatoDetalle.on('shown.bs.modal', function () {
                    $formularioAgregarGrupoDatoDetalle.AgregarCamposDefectoAuditoria();
                    $formularioAgregarGrupoDatoDetalle.DeshabilitarCamposAuditoria();
                    $formularioAgregarGrupoDatoDetalle.find("[name='CGDTO']").val($codigoGrupoDato.val());
                })
            },
            reset: function () {
                modalAgregarDetalle.form.eventos.reset();
                $formularioAgregarGrupoDatoDetalle.trigger("reset");
                $formularioAgregarGrupoDatoDetalle.find(":input").attr("disabled", false);
                $formularioAgregarGrupoDatoDetalle.find("[name='CGDTO']").attr("disabled", true);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var modalEditarDetalle = {
        form: {
            objeto: $formularioEditarGrupoDatoDetalle.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    CGDTO: {
                        required: true,
                        maxlength: 15,
                        alfanumerico: true,
                    },
                    DGDDTLLE: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                    AGDDTLLE: {
                        required: true,
                        maxlength: 50,
                        alfanumerico: true,
                    },
                    VLR1: {
                        required: true,
                        maxlength: 250
                    },
                    VLR2: {
                        maxlength: 250
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    formData.append("CGDTO", $codigoGrupoDato.val());
                    $formularioEditarGrupoDatoDetalle.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGrupoDatoDetalle.reload();
                                $modalEditarGrupoDatoDetalle.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioEditarGrupoDatoDetalle.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalEditarDetalle.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalEditarGrupoDatoDetalle.on('hidden.bs.modal', function () {
                    modalEditarDetalle.eventos.reset();
                })
            },
            onShow: function () {
                $modalEditarGrupoDatoDetalle.on('shown.bs.modal', function () {
                    //$("#Nombre").focus();
                })
            },
            reset: function () {
                modalEditarDetalle.form.eventos.reset();
                $formularioEditarGrupoDatoDetalle.trigger("reset");
                $formularioEditarGrupoDatoDetalle.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var eventos = {
        init: function () {
            $tab_grupodato.on("click", function () {
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("rastrogrupodato");
                }
                $tab_grupodatoDetalle.addClass("disabled");
            });
        }
    }

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    }

    return {
        init: function () {
            tablaGrupoDato.inicializador();
            modalAgregar.init();
            modalEditar.init();
            modalAgregarDetalle.init();
            modalEditarDetalle.init();
            eventos.init();
            selects.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarGrupoDato.init();
})

