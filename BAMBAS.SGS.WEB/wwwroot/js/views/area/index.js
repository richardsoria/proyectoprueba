﻿var InicializarArea = function () {
    var $selectEstados = $(".select-estados");
    var $selectGerencias = $(".select-gerencias");
    var $filtroGerencias = $(".filtro-select-gerencias");
    var $formGerencias = $(".form-select-gerencias");
    var $selectSuperintendencias = $(".select-superintendencias");
    var $filtroSuperintendencias = $(".filtro-select-superintendencias");
    var $formSuperintendencias = $(".form-select-superintendencias");
    //variables JQuery    //variables JQuery
    var $tablaArea = $("#tabla_area");
    var $formularioArea = $("#area_form");
    var $modalArea = $("#modal_area");
    //
    var $accesoAgregarArea = $("#accesoAgregarArea");
    var $accesoEditarArea = $("#accesoEditarArea");
    var $accesoEliminarArea = $("#accesoEliminarArea");

    var $btnArea = $("#btnArea");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarArea.val() == "False") {
                $btnArea.remove()
            }
        }
    };

    var entidadArea = {
        id: "",
        dscrpcn: "",
        idgrnca: "",
        idsprintndnca: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }
    var tablaArea = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: `/area/get`,
                type: "GET",
                data: function (data) {
                    data.idGerencia = $selectGerencias.val();
                    data.idSuperintendencia = $selectSuperintendencias.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Gerencia",
                    className: "text-left",
                    data: "grnca",
                    orderable: false
                },
                {
                    title: "Superintendencia",
                    className: "text-left",
                    data: "sprintndnca",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarArea.val() == "True") {
                        tpm += `<a data-toggle="modal" href="#modal_area" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarArea.val() == "True") {
                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaArea.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/area/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaArea.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaArea.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioArea.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/area/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadArea = result;
                        selects.formGerencias(entidadArea.idgrnca, entidadArea.idsprintndnca)
                        //$formularioArea.find("[name='IDGRNCA']").val(entidadArea.idgrnca).trigger("change");
                        /*  $formularioArea.find("[name='IDSPRINTNDNCA']").val(entidadArea.idsprintndnca);*/
                        $formularioArea.find("[name='DSCRPCN']").val(entidadArea.dscrpcn);
                        $formularioArea.AgregarCamposAuditoria(entidadArea);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        setTimeout(function () {
                            $formularioArea.find(":input").attr("disabled", false);
                            $formularioArea.DeshabilitarCamposAuditoria();
                        }, 2000)
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArea.objeto = $tablaArea.DataTable(tablaArea.opciones);
            tablaArea.eventos();
        }
    };
    var modalArea = {
        form: {
            objeto: $formularioArea.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioArea.find(":input").attr("disabled", true);

                    if (!$formularioArea.find("[name='ID']").val()) {
                        url = `/area/insertar`;
                    } else {
                        url = `/area/actualizar`
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaArea.reload();
                                modalArea.eventos.reset();
                                $modalArea.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioArea.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalArea.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalArea.on('hidden.bs.modal', function () {
                    modalArea.eventos.reset();
                    $formularioArea.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalArea.on('shown.bs.modal', function () {
                    $formularioArea.find("[name='IDGRNCA']").val("").change();
                    if (!$formularioArea.find("[name='ID']").val()) {
                        $formularioArea.AgregarCamposDefectoAuditoria();
                        $formularioArea.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalArea.form.eventos.reset();
                $formularioArea.trigger("reset");
                $formularioArea.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        gerencias: function () {
            $.get("/gerencia/obtener-todas")
                .done(function (data) {
                    $selectGerencias.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectGerencias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
        },
        formGerencias: function (idgerencia, idsuper) {
            $formGerencias.unbind('change');
            $formGerencias.val(idgerencia);
            selects.formSuperintendencias(idsuper);
            eventosIncrustados.formGerencias();
        },
        filtroSuperintendencias: function () {
            $filtroSuperintendencias.empty();
            $filtroSuperintendencias.append($("<option />").val('').text("Seleccione"));
            if ($filtroGerencias.val()) {
                $.get("/superintendencia/obtener-todas", { idGerencia: $filtroGerencias.val() })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $filtroSuperintendencias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                        });
                        $filtroSuperintendencias.attr("disabled", false);
                    });
            } else {
                $filtroSuperintendencias.val("");
                $filtroSuperintendencias.attr("disabled",true);
            }
        },
        formSuperintendencias: function (selected) {
            $formSuperintendencias.attr("disabled", true);
            $formSuperintendencias.empty();
            $formSuperintendencias.append($("<option />").val('').text("Seleccione"));
            if ($formGerencias.val()) {
                $.get("/superintendencia/obtener-todas", { idGerencia: $formGerencias.val() })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $formSuperintendencias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                        });
                        if (selected) {
                            $formSuperintendencias.val(selected);
                        }
                    }).always(function () {
                        $formSuperintendencias.attr("disabled", false);
                    });
            }
        },
        init: function () {
            this.gerencias();
            this.filtroSuperintendencias();
            $selectEstados.LlenarSelectEstados();
        }
    };
    var eventosIncrustados = {
        filtroGerencias: function () {
            $filtroGerencias.on("change", function () {
                selects.filtroSuperintendencias();
                eventosIncrustados.filtroSuperintendenacias();
                tablaArea.reload();
            });
        },
        filtroSuperintendenacias: function () {
            $filtroSuperintendencias.unbind('change');
            $filtroSuperintendencias.on("change", function () {
                tablaArea.reload();
            });
        },
        formGerencias: function () {
            $formGerencias.on("change", function () {
                selects.formSuperintendencias();
                eventosIncrustados.formSuperintendenacias();
            });
        },
        formSuperintendenacias: function () {
            $formSuperintendencias.unbind('change');
            $formSuperintendencias.on("change", function () {
                //tablaArea.reload();
            });
        },
        init: function () {
            this.filtroGerencias();
            this.formGerencias();
        }
    }
    return {
        init: function () {
            selects.init();
            tablaArea.inicializador();
            modalArea.init();

            //validacionControles.init();
            eventosIncrustados.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarArea.init();
})

