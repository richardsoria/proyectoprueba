﻿var InicializarEmpresa = function () {
    //variables JQuery    //variables JQuery
    var $tablaEmpresa = $("#tabla_empresa");
    var $formularioAgregarEmpresa = $("#empresa_agregar_form");
    var $formularioEditarEmpresa = $("#empresa_editar_form");
    var $modalAgregarEmpresa = $("#modal_agregar_empresa");
    var $modalEditarEmpresa = $("#modal_editar_empresa");
    //
    var $formularioAgregarSucursal = $("#sucursal_agregar_form");
    var $formularioEditarSucursal = $("#sucursal_editar_form");
    var $modalAgregarSucursal = $("#modal_agregar_sucursal");
    var $modalEditarSucursal = $("#modal_editar_sucursal");
    var $tablaSucursal = $("#tabla_sucursal");
    var $tablaSucursalUsuario = $("#tabla_sucursalUsuario");
    var $btnGuardarSucursalUsuario = $("#btn-guardarSucursalUsuario")
    //
    var $empresaId = $("#empresaId");
    var $sucursalId = $("#sucursalId");
    var $tab_empresa = $("#empresa-tab");
    var $tab_sucursal = $("#sucursal-tab");
    var $tab_sucursalUsuarios = $("#sucursalUsuarios-tab");
    var $selectEstados = $(".select-estados");
    var $lblempresa = $("#lblempresa");
    var $lblsucursal = $("#lblsucursal");


    //CONTROLES
    var $accesoAgregarEmpresa = $("#accesoAgregarEmpresa");
    var $accesoEditarEmpresa = $("#accesoEditarEmpresa");
    var $accesoEliminarEmpresa = $("#accesoEliminarEmpresa");
    var $accesoAgregarSucursal = $("#accesoAgregarSucursal");
    var $accesoEditarSucursal = $("#accesoEditarSucursal");
    var $accesoEliminarSucursal = $("#accesoEliminarSucursal");
    var $accesoAsignarUsuarios = $("#accesoAsignarUsuarios");

    var $btnAgregarEmpresa = $("#btnAgregarEmpresa");
    var $btnAgregarSucursal = $("#btnAgregarSucursal");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarEmpresa.val() == "False") {
                $btnAgregarEmpresa.remove();
            }
            if ($accesoAgregarSucursal.val() == "False") {
                $btnAgregarSucursal.remove();
            }
            if ($accesoAsignarUsuarios.val() == "False") {
                $btnGuardarSucursalUsuario.remove();
            }
        }
    };


    var tablaEmpresa = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/empresa/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                
                {
                    title: "Nombre",
                    className: "text-center",
                    data: "nemprsa",
                    orderable: false
                },
                {
                    title: "RUC",
                    className: "text-center",
                    data: "ruc",
                    width: '10%',
                    orderable: false
                },
                //{
                //    title: "Dirección",
                //    data: "drccn", 
                //    orderable: false
                //},
                {
                    title: "Ubigeo",
                    className: "text-center",
                    data: "ubgo",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Ver",
                    data: null,
                    className: "text-center",
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a data-id="${data.id}" class="btn btn-xs btn-ver"><i class="la la-eye"></i></a>`;
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '10%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarEmpresa.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_editar_empresa" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarEmpresa.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaEmpresa.objecto.on("click", ".btn-ver", function () {
                var data = tablaEmpresa.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.nemprsa, "nempresa");
                //$lblempresa.text(data.nemprsa);
                $empresaId.val($(this).data("id"));
                tablaSucursal.inicializador();
                $tab_sucursal.removeClass("disabled");
                $tab_sucursal.click();
            });
            tablaEmpresa.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/empresa/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaEmpresa.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaEmpresa.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioEditarEmpresa.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/empresa/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        $formularioEditarEmpresa.find("[name='NEMPRSA']").val(result.nemprsa);
                        $formularioEditarEmpresa.find("[name='DRCCN']").val(result.drccn);
                        $formularioEditarEmpresa.find("[name='RUC']").val(result.ruc);
                        var opt = {}
                        opt.codUbigeo = result.ubgo
                        cargarUbigeos(opt);
                        $formularioEditarEmpresa.AgregarCamposAuditoria(result);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioEditarEmpresa.find(":input").attr("disabled", false);
                        $formularioEditarEmpresa.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            tablaEmpresa.objecto = $tablaEmpresa.DataTable(tablaEmpresa.opciones);
            tablaEmpresa.eventos();
        }
    };
    var modalAgregarEmpresa = {
        form: {
            objecto: $formularioAgregarEmpresa.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NEMPRSA: {
                        required: true,
                        maxlength: 150,
                    },
                    RUC: {
                        required: true,
                        maxlength: 11,
                        solonumeros: true,
                        longitudexacta: 11,
                    },
                    DPRTMNTO: {
                        required: true,
                    },
                    PRVNCA: {
                        required: true,
                    },
                    UBGO: {
                        required: true,
                        maxlength: 6
                    },
                    DRCCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioAgregarEmpresa.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaEmpresa.reload();
                                modalAgregarEmpresa.eventos.reset();
                                $modalAgregarEmpresa.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioAgregarEmpresa.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalAgregarEmpresa.form.objecto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalAgregarEmpresa.on('hidden.bs.modal', function () {
                    modalAgregarEmpresa.eventos.reset();
                })
            },
            onShow: function () {
                $modalAgregarEmpresa.on('shown.bs.modal', function () {
                    $formularioAgregarEmpresa.find(".select-distrito").val("");
                    $formularioAgregarEmpresa.find(".select-provincia").val("");
                    $formularioAgregarEmpresa.find(".select-provincia").attr("required", true);
                    $formularioAgregarEmpresa.AgregarCamposDefectoAuditoria();
                    $formularioAgregarEmpresa.DeshabilitarCamposAuditoria();
                    $formularioAgregarEmpresa.find(".select-departamento").val("").change();
                    $formularioAgregarEmpresa.find(".select-distrito").val("").prop("disabled", true);
                    $formularioAgregarEmpresa.find(".select-provincia").val("").prop("disabled", true);
                })
            },
            reset: function () {
                modalAgregarEmpresa.form.eventos.reset();
                $formularioAgregarEmpresa.trigger("reset");
                $formularioAgregarEmpresa.find(":input").attr("disabled", false);
                $formularioAgregarEmpresa.find(".select-departamento").val("").change();
                $formularioAgregarEmpresa.find(".select-distrito").val("").prop("disabled", true);
                $formularioAgregarEmpresa.find(".select-provincia").val("").prop("disabled", true);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var modalEditarEmpresa = {
        form: {
            objeto: $formularioEditarEmpresa.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NEMPRSA: {
                        required: true,
                        maxlength: 150,
                    },
                    RUC: {
                        required: true,
                        maxlength: 11,
                        longitudexacta: 11,
                        solonumeros: true
                    },
                    DPRTMNTO: {
                        required: true,
                    },
                    PRVNCA: {
                        required: true,
                    },
                    UBGO: {
                        required: true,
                        maxlength: 6
                    },
                    DRCCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioEditarEmpresa.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaEmpresa.reload();
                                modalEditarEmpresa.form.objeto.reset();
                                $modalEditarEmpresa.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioEditarEmpresa.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalEditarEmpresa.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalEditarEmpresa.on('hidden.bs.modal', function () {
                    modalEditarEmpresa.eventos.reset();
                })
            },
            onShow: function () {
                $modalEditarEmpresa.on('shown.bs.modal', function () {

                })
            },
            reset: function () {
                $formularioEditarEmpresa.find(".select-departamento").val("");
                $formularioEditarEmpresa.find(".select-distrito").val("").prop("disabled", true);
                $formularioEditarEmpresa.find(".select-provincia").val("").prop("disabled", true);
                modalEditarEmpresa.form.eventos.reset();
                $formularioEditarEmpresa.trigger("reset");
                $formularioEditarEmpresa.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    //SUCURSAL
    var tablaSucursal = {
        empresaId: null,
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: `/sucursal/obtener`,
                type: "GET",
                data: function (data) {
                    data.empresaId = $empresaId.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Nombre",
                    className: "text-left",
                    data: "nscrsl",
                    orderable: false
                },
                {
                    title: "Dirección",
                    className: "text-left",
                    data: "dscrsl",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarSucursal.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_editar_sucursal" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarSucursal.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaSucursal.objecto.on("click", ".btn-ver", function () {
                var data = tablaSucursal.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.nscrsl, "nsucursal");
                $sucursalId.val($(this).data("id"));
                tablaSucursalUsuario.reload();
                $tab_sucursalUsuarios.removeClass("disabled");
                $tab_sucursalUsuarios.click();
            });
            tablaSucursal.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/sucursal/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaSucursal.reload();
                                tablaEmpresa.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaSucursal.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioEditarSucursal.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/sucursal/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        $formularioEditarSucursal.find("[name='NSCRSL']").val(result.nscrsl);
                        $formularioEditarSucursal.find("[name='DSCRSL']").val(result.dscrsl);
                        var opt = {}
                        opt.codUbigeo = result.ubgo
                        cargarUbigeos(opt);
                        $formularioEditarSucursal.AgregarCamposAuditoria(result);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioEditarSucursal.find(":input").attr("disabled", false);
                        $formularioEditarSucursal.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            if (tablaSucursal.objecto == undefined || tablaSucursal.objecto == null) {
                tablaSucursal.objecto = $tablaSucursal.DataTable(tablaSucursal.opciones);
                tablaSucursal.eventos();
            } else {
                tablaSucursal.reload();
            }
        }
    };
    var modalAgregarSucursal = {
        form: {
            objeto: $formularioAgregarSucursal.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NSCRSL: {
                        required: true,
                        maxlength: 150,
                    },
                    DSCRSL: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioAgregarSucursal.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaSucursal.reload();
                                modalAgregarSucursal.eventos.reset();
                                $modalAgregarSucursal.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioAgregarSucursal.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalAgregarSucursal.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalAgregarSucursal.on('hidden.bs.modal', function () {
                    modalAgregarSucursal.eventos.reset();
                })
            },
            onShow: function () {
                $modalAgregarSucursal.on('shown.bs.modal', function () {
                    $formularioAgregarSucursal.find("[name='IDEMPRSA']").val($empresaId.val());
                    $formularioAgregarSucursal.find(".select-departamento").val("");
                    $formularioAgregarSucursal.find(".select-distrito").val("");
                    $formularioAgregarSucursal.find(".select-provincia").val("");
                    $formularioAgregarSucursal.AgregarCamposDefectoAuditoria();
                    $formularioAgregarSucursal.DeshabilitarCamposAuditoria();
                    $formularioAgregarSucursal.find(".select-distrito").val("").prop("disabled", true);
                    $formularioAgregarSucursal.find(".select-provincia").val("").prop("disabled", true);
                })
            },
            reset: function () {
                modalAgregarSucursal.form.eventos.reset();
                $formularioAgregarSucursal.trigger("reset");
                $formularioAgregarSucursal.find(":input").attr("disabled", false);
                $formularioAgregarSucursal.find(".select-departamento").val("");
                $formularioAgregarSucursal.find(".select-distrito").val("").prop("disabled", true);
                $formularioAgregarSucursal.find(".select-provincia").val("").prop("disabled", true);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var modalEditarSucursal = {
        form: {
            objeto: $formularioEditarSucursal.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NSCRSL: {
                        required: true,
                        maxlength: 150,
                    },
                    DSCRSL: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioEditarSucursal.find(":input").attr("disabled", true);
                    $.ajax({
                        url: $(formElement).attr("action"),
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaSucursal.reload();
                                $modalEditarSucursal.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioEditarSucursal.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalEditarSucursal.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalEditarSucursal.on('hidden.bs.modal', function () {
                    modalEditarSucursal.eventos.reset();
                })
            },
            onShow: function () {
                $modalEditarSucursal.on('shown.bs.modal', function () {
                    //$("#Nombre").focus();
                })
            },
            reset: function () {
                modalEditarSucursal.form.eventos.reset();
                $formularioEditarSucursal.trigger("reset");
                $formularioEditarSucursal.find(":input").attr("disabled", false);
                $formularioEditarSucursal.find(".select-departamento").val("").change();
                $formularioEditarSucursal.find(".select-distrito").val("").prop("disabled", true);
                $formularioEditarSucursal.find(".select-provincia").val("").prop("disabled", true);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    //SUCURSAL USUARIOS
    var tablaSucursalUsuario = {
        asignados: [],
        noAsignados: [],
        objecto: null,
        opciones: {
            ajax: {
                gatatype: "JSON",
                url: "/sucursal-usuario/obtener-usuarios",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idSucursal = $sucursalId.val();
                }
            },
            columns: [
                {
                    title: "Asignado",
                    data: null,
                    orderable: false,
                    width: '11%',
                    class: "text-center",
                    render: function (data) {
                        var checkeado = false;
                        //verificar que no exista ni en una ni en la otra para agregarlo
                        if ($.inArray(data.id, tablaSucursalUsuario.asignados) == -1 && $.inArray(data.id, tablaSucursalUsuario.noAsignados) == -1) {
                            if (data.checkeado) {
                                checkeado = true;
                                tablaSucursalUsuario.asignados.push(data.id);
                            } else {
                                checkeado = false;
                                tablaSucursalUsuario.noAsignados.push(data.id);
                            }
                        } else {//si existe, buscarlo
                            if ($.inArray(data.id, tablaSucursalUsuario.asignados) == -1) {
                                checkeado = false;
                            }
                            else {
                                checkeado = true;
                            }
                        }
                        //
                        var tpm = `<label class="switch">
                                  <input type="checkbox" ${checkeado ? "checked" : ""} ${$accesoAsignarUsuarios.val() == "False" ? "disabled" : ""} class="check-asignado" data-id="${data.id}">
                                  <span class="slider"></span>
                                </label>`;
                        return tpm;
                    }
                },
                //{
                //    title: "ID",
                //    data: "id", width: '5%',
                //    orderable: true
                //},
                {
                    title: "Nombres",
                    data: "nyapllds",
                    width: '17%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Cod. Usuario",
                    data: "cusro",
                    width: '8%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Fec. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            asignar: function () {
                tablaSucursalUsuario.objecto.on("click", ".check-asignado", function () {
                    var idusuario = $(this).data("id");
                    var checkeando = $(this).is(':checked');

                    if ($.inArray(idusuario, tablaSucursalUsuario.asignados) == -1 && $.inArray(idusuario, tablaSucursalUsuario.noAsignados) == -1) {
                        if (checkeando) {
                            tablaSucursalUsuario.asignados.push(idusuario);
                        } else {
                            tablaSucursalUsuario.noAsignados.push(idusuario);
                        }
                    } else {//si existe, buscarlo
                        if ($.inArray(idusuario, tablaSucursalUsuario.asignados) == -1 && checkeando) {//si no esta asignado y esta chequeado
                            //buscarlo en no asignados y eliminarlo
                            tablaSucursalUsuario.noAsignados.splice($.inArray(idusuario, tablaSucursalUsuario.noAsignados), 1);
                            //agregarlo al asignados
                            tablaSucursalUsuario.asignados.push(idusuario);
                        }
                        else {
                            //buscarlo en asignados y eliminarlo
                            tablaSucursalUsuario.asignados.splice($.inArray(idusuario, tablaSucursalUsuario.asignados), 1);
                            //agregarlo al NO asignados
                            tablaSucursalUsuario.noAsignados.push(idusuario);
                        }
                    }
                });
            },
            init: function () {
                this.asignar();
            }
        },
        reload: function () {
            if (tablaSucursalUsuario.objecto != null && tablaSucursalUsuario.objecto != undefined) {
                tablaSucursalUsuario.objecto.ajax.reload();
            } else {
                tablaSucursalUsuario.init();
            }
        },
        init: function () {
            tablaSucursalUsuario.objecto = $tablaSucursalUsuario.DataTable(tablaSucursalUsuario.opciones);
            this.eventos.init();
        }
    };
    var eventoGuardarSucursalUsuario = {
        init: function () {
            $btnGuardarSucursalUsuario.on("click", function () {
                $btn = $(this);
                $btn.addLoader();
                $.ajax({
                    type: "POST",
                    url: '/sucursal-usuario/guardar',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        asignados: tablaSucursalUsuario.asignados,
                        noAsignados: tablaSucursalUsuario.noAsignados,
                        idSucursal: $sucursalId.val()
                    })
                })
                    .done(function (e) {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Registros actualizados satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            tablaSucursalUsuario.reload();
                        });
                    })
                    .fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al actualizar registros.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    })
                    .always(function () {
                        $btn.removeLoader();
                    });
            });
        }
    }

    //
    var eventos = {
        init: function () {
            $tab_empresa.on("click", function () {
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("nempresa");
                }
                $tab_sucursal.addClass("disabled");
                $tab_sucursalUsuarios.addClass("disabled");
                tablaSucursalUsuario.noAsignados = [];
                tablaSucursalUsuario.asignados = [];
            });
            $tab_sucursal.on("click", function () {
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("nsucursal");
                }
                $tab_sucursalUsuarios.addClass("disabled");
                tablaSucursalUsuario.noAsignados = [];
                tablaSucursalUsuario.asignados = [];
            });
        }
    };

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    };
    return {
        init: function () {
            tablaEmpresa.inicializador();
            modalAgregarEmpresa.init();
            modalEditarEmpresa.init();
            modalAgregarSucursal.init();
            modalEditarSucursal.init();

            eventos.init();
            eventoGuardarSucursalUsuario.init();
            selects.init();
            cargarUbigeo();

            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarEmpresa.init();
})

