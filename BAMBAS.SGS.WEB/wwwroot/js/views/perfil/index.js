﻿var iniPerfil = function () {
    var $btnNuevo = $(".btn-nuevo");
    var $tblPerfil = $("#tabla_perfil");
    var $mdlPerfil = $("#mdlPerfil");
    var $modalTitulo = $mdlPerfil.find(".modal-title");
    var $frm_Perfil = $("#frm_Perfil");
    var $txtCodigo = $("#txtCodigo");
    var $txtDescripcion = $("#txtDescripcion");

    var $cboEstado = $(".select-estados");
    var $txtUCreacion = $("#txtUCreacion");
    var $txtUEdicion = $("#txtUEdicion");
    var $txtFEstado = $("#txtFEstado");
    var $txtFCreacion = $("#txtFCreacion");
    var $txtFEdicion = $("#txtFEdicion");
    var $hfaction = $("#hfPerfil");

    var $accesoAgregarPerfil = $("#accesoAgregarPerfil");
    var $accesoEditarPerfil = $("#accesoEditarPerfil");
    var $accesoEliminarPerfil = $("#accesoEliminarPerfil");

    var entidadPerfil = {
        id: "",
        dprfl: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarPerfil.val() == "False") {
                $btnNuevo.remove();
            }
        }
    };

    var configDTPerfil = {
        objecto: null,
        opciones: {
            ajax: {
                gatatype: "JSON",
                url: "/Perfil/ListarPerfil",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    data: "dprfl",
                    width: '25%',
                    orderable: false, className: "text-left"
                },                
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '11%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarPerfil.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlPerfil" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarPerfil.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                         }
                        return tpm;
                    }
                }

            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTPerfil.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    fnPerfil.eliminar(id);
                })
            },

            editar: function () {
                configDTPerfil.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTitulo.text("Editar Perfil");
                    $hfaction.val("E");
                    fnPerfil.obtener(id);
                })
            },
            init: function () {
                configDTPerfil.eventos.eliminar();
                configDTPerfil.eventos.editar();
            }
        },
        reload: function () {
            configDTPerfil.objecto.ajax
                .reload();
        },
        init: function () {
            configDTPerfil.objecto = $tblPerfil.DataTable(configDTPerfil.opciones);
            configDTPerfil.eventos.init();
        }
    };
    var configModalPerfil = {
        form: {
            objeto: $frm_Perfil.validate({
                rules: {
                    dprfl: {
                        required: true,
                        maxlength: 150
                    },
                }
            }),
            eventos: {
                reset: function () {
                    configModalPerfil.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlPerfil.on("hidden.bs.modal", function () {
                    configModalPerfil.eventos.reset();
                })
            },
            onShow: function () {
                $mdlPerfil.on("shown.bs.modal", function () {
                    if ($hfaction.val() == "N") {
                        $frm_Perfil.AgregarCamposDefectoAuditoria();
                        $frm_Perfil.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                $frm_Perfil.trigger("reset");
                $frm_Perfil.find(":input").attr("disabled", false);
                configModalPerfil.form.eventos.reset();
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }
    var fnPerfil = {
        guardar: function () {
            if ($hfaction.val() == "N") {
                fnPerfil.insertar();
            }
            else {
                fnPerfil.actualizar();
            }

            $hfaction.val("");
        },
        insertar: function () {
            if ($frm_Perfil.valid()) {
                entidadPerfil.dprfl = $txtDescripcion.val();
                entidadPerfil.gdestdo = $cboEstado.val();
                $.post("/Perfil/InsertarPerfil", entidadPerfil)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            configDTPerfil.reload();
                            $mdlPerfil.modal("hide");
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al guardar los datos.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
            }
        },
        actualizar: function () {
            if ($frm_Perfil.valid()) {
                entidadPerfil.id = $txtCodigo.val();
                entidadPerfil.dprfl = $txtDescripcion.val();
                entidadPerfil.gdestdo = $cboEstado.val();
                $.post("/Perfil/ActualizarPerfil", entidadPerfil)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            configDTPerfil.reload();
                            $mdlPerfil.modal("hide");
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar los datos.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
            }
        },
        obtener: function (codigo) {
            var vperfil = { id: codigo };
            $.post("/Perfil/ObtenerPerfil", vperfil)
                .done(function (data) {
                    entidadPerfil = data;
                    $txtCodigo.val(entidadPerfil.id);
                    $txtDescripcion.val(entidadPerfil.dprfl);
                    $frm_Perfil.AgregarCamposAuditoria(entidadPerfil);

                })
                .fail().always(function () {
                    $frm_Perfil.find(":input").attr("disabled", false);
                    $frm_Perfil.DeshabilitarCamposAuditoria();
                });
        },
        eliminar: function (codigo) {
            var vperfil = { id: codigo };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Perfil/EliminarPerfil", vperfil)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTPerfil.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                }
            });
        },
    }
  
    var eventosIncrustados = {
        NuevoPerfil: function () {
            $btnNuevo.click(function () {
                $modalTitulo.text("Nuevo Perfil");
                $hfaction.val("N");
            })
        },

        GuardarPerfil: function () {
            $frm_Perfil.find(".btn-save").click(function () {
                fnPerfil.guardar();
            })
        },

        init: function () {
            eventosIncrustados.NuevoPerfil();
            eventosIncrustados.GuardarPerfil();
        }
    }

    var IniEstados = {
        init: function () {
            $cboEstado.LlenarSelectEstados();
        }
    };
    return {
        init: function () {
            configDTPerfil.init();
            configModalPerfil.init();
            eventosIncrustados.init();
            IniEstados.init();

            validacionControles.init();
        }
    };
}();
$(() => {
    iniPerfil.init();
})
