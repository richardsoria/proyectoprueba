﻿var InicializarGrupoCargo = function () {
    var $selectEstados = $(".select-estados");
    var $IdGrupoCargo = $("#IdGrupoCargo");
    var $DivBusqueda = $("#DivBusqueda");
    //variables JQuery    //variables JQuery
    var $tablaArticulo = $("#tabla_articulo");
    var $formularioEPPGrupoCargo = $("#EPP_GrupoCargo_form");
    var $modalEPPArticulo = $("#modal_kitArticulo");
    //
    var $txtBusqueda = $("#txtBusqueda");
    var $txtCantidad = $("#txtCantidad");
    var $dataecontrada = $("#dataecontrada");
    var $btnAgregarArticulos = $('#btnAgregarArticulos');
    var $btnKitBuscar = $("#btnKitBuscar");
    var $btnKitAgregar = $("#btnKitAgregar");
    var $btnKitLimpiar = $("#btnKitLimpiar");
    var $btnEPPGuardar = $("#btnEPPGuardar");

    var entidadGrupoCargo = {
        id: "",
        dscrpcn: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: ""
    }

    var tablaArticulo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                //url: "/kit/get-articulos",
                url: "/articulo/get-activas",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Unid. Med.",
                    className: "text-center",
                    data: "gdunddmdda",
                    orderable: false
                },
                {
                    title: "Moneda",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false
                },
                {
                    title: "Costo",
                    className: "text-right",
                    data: "csto",
                    orderable: false,
                    render: function (data) {
                        return data.toFixed(2)
                    }
                },
                {
                    title: "Cantidad",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        var cantarticulo = tablaArticulo.memory_art.find(x => x.CSAP == data.csap);
                        if (cantarticulo) {
                            tpm += `   <input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-csto="${data.csto}" data-drcn="${data.drcn}" data-idartclo="${data.id}" type="text" class="form-control text-uppercase form-control-sm cntdd text-center" maxlength="8" value="${cantarticulo.CANT}">`;
                        } else {
                            tpm += `   <input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-csto="${data.csto}" data-drcn="${data.drcn}" data-idartclo="${data.id}" type="text" class="form-control text-uppercase form-control-sm cntdd text-center" maxlength="8">`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        memory_art: [],
        refresh_cant: function () {
            $("#tabla_articulo tbody tr").each(function (index) {
                var csaptabla = $(this).children('td').eq(0).text();

                for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                    if (tablaArticulo.memory_art[i].CSAP === csaptabla) {
                        $(this).children('td').eq(3).val(tablaArticulo.memory_art[i].CANT);
                    }
                }

            })
        },
        eventos: function () {
            tablaArticulo.objeto.on("keypress", ".cntdd", function () {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
                return true;
            });
            tablaArticulo.objeto.on("keyup", ".cntdd", function () {

                var csap = $(this).data("csap");
                var descr = $(this).data("dscrpcn");
                var und = $(this).data("gdunddmdda");
                var cant = $(this).val();
                var idart = $(this).data("idartclo");
                var csto = $(this).data("csto");
                var drcn = $(this).data("drcn");

                var delete_articulo = false;
                if (cant == 0 || cant == '') {
                    delete_articulo = true;
                }
                var new_articulo = true;
                for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                    if (tablaArticulo.memory_art[i].CSAP === csap) {
                        new_articulo = false;
                        if (delete_articulo) {
                            tablaArticulo.memory_art.splice(i, 1);
                        } else {
                            tablaArticulo.memory_art[i].CANT = cant;
                        }
                    }
                }
                if (new_articulo) {
                    tablaArticulo.memory_art.push({
                        CSAP: csap,
                        DESCR: descr,
                        UND: und,
                        CANT: cant,
                        IDART: idart,
                        CSTO: csto,
                        DRCN: drcn,
                    });
                }
                //console.log(tablaArticulo.memory_art);
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
            tablaArticulo.eventos();
        }
    };

    var modalEPPArticulo = {
        eventos: {
            onHide: function () {
                $modalEPPArticulo.on('hidden.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                    modalEPPArticulo.eventos.reset();
                })
            },
            onShow: function () {
                $modalEPPArticulo.on('shown.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                })
            },
            reset: function () {
                tablaArticulo.memory_art = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    };

    var eventosIncrustados = {
        CalcularIndex: function () {
            var hayfilas = $("#tabla_articuloadd tbody tr").length > 0;
            if (!hayfilas) {
                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
            $("#tabla_articuloadd tbody tr").each(function (index) {
                var trs = index + 1;
                $(this).children('td').eq(0).text(trs);
            })
        },
        editarCantidadFila: function () {
            $(document).on('click', '.btn-editar-tabla', function (event) {
                //
                var id = $(this).data("id");
                var btn = $(this);
                var hasClass = $(this).hasClass("btn-info");
                var input = ".cntdd-" + id;
                if (hasClass) {
                    $("#tabla_articuloadd").find(input).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anterior = $("#tabla_articuloadd").find(input).data("anterior");
                    $("#tabla_articuloadd").find(input).val(anterior);
                    $("#tabla_articuloadd").find(input).attr("disabled", true);
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
                //
            });
        },
        EliminarFila: function () {
            $(document).on('click', '.BorrarFilaSubMenu', function (event) {
                event.preventDefault();
                var fila = $(this).closest('tr');
                var idkitdetalle = $(this).attr("id");
                let arrayid = idkitdetalle.split('_');
                var id = arrayid[1];

                if ($IdGrupoCargo.val()) {
                    if (id > 0) {
                        Swal.fire({
                            title: "¿Quiere modificar el estado del registro?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Aceptar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: "/grupocargo/eliminar-detalle",
                                    type: "POST",
                                    data: {
                                        id: id
                                    }
                                }).done(function () {
                                    Swal.fire({
                                        icon: "success",
                                        allowOutsideClick: false,
                                        title: "Éxito",
                                        text: "Registro modificado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then((result) => {
                                        fila.remove();
                                        eventosIncrustados.CalcularIndex();
                                        /*                   eventosIncrustados.TotalCosto();*/
                                    });
                                }).fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al modificar el registro.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                });
                            }
                        });
                    } else {
                        fila.remove();
                    }

                } else {
                    fila.remove();
                }
                /*   eventosIncrustados.TotalCosto();*/
                eventosIncrustados.CalcularIndex();
            });
        },
        botonAgregarArticulo: function () {
            $btnAgregarArticulos.on("click", function () {
                for (var i = 0; i < tablaArticulo.memory_art.length; i++) {
                    var art_in_table = false;
                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').find("[class='csap']").text();
                        if (tablaArticulo.memory_art[i].CSAP == csaptabla.trim()) {
                            art_in_table = true;
                            $(this).children('td').find("[class='cntdd']").val(tablaArticulo.memory_art[i].CANT);
                        }
                    })
                    if (!art_in_table) {
                        var trs = i + 1;
                        var htmlTags = eventosIncrustados.formatoFilaTabla(trs, 0, tablaArticulo.memory_art[i].IDART, tablaArticulo.memory_art[i].CSAP, tablaArticulo.memory_art[i].DESCR, tablaArticulo.memory_art[i].UND, tablaArticulo.memory_art[i].CANT, tablaArticulo.memory_art[i].CSTO, tablaArticulo.memory_art[i].DRCN, false, false);

                        $('#tabla_articuloadd tbody').append(htmlTags);
                    }
                }
                tablaArticulo.memory_art = [];
                $modalEPPArticulo.modal('hide');
                eventosIncrustados.CalcularIndex();
                //eventosIncrustados.TotalCosto();
            })

        },
        botonBuscar: function () {
            $btnKitBuscar.on("click", function () {
                tablaArticulo.memory_art = [];
                tablaArticulo.reload();
            })
        },
        botonGrabarKit: function () {
            $btnEPPGuardar.on("click", function () {

                var fila = $("#tabla_articuloadd tbody").find('.noData').text();

                if (fila) {
                    Swal.fire({
                        title: "No se puede crear el EPP Por Grupo Cargo sin artículos asociados.",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                if ($formularioEPPGrupoCargo.valid()) {
                    $btnEPPGuardar.attr("disabled", true);

                    $formularioEPPGrupoCargo.find("[name='CNTDD']").add('was-validated');
                    var articulos = [];
                    $("#tabla_articuloadd tbody tr").each(function (index) {

                        var cant = $(this).find(".cntdd").val();
                        var id = $(this).find("[class='idartclo']").val();
                        articulos.push(
                            {
                                IDARTCLO: id,
                                CNTDD: cant,
                            }
                        );
                    })
                    //console.log(JSON.stringify(articulos));
                    var formData = new FormData();
                    formData.append("ARTCLS", JSON.stringify(articulos));
                    formData.append("ID", $IdGrupoCargo.val());
                    //console.log(formData);

                    $.ajax({
                        url: "/grupocargo/actualizar-detalle",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center" >Cargando datos...</td></tr>');
                                obtenerDatosGrupoCargo.obtenerDetalle();

                                eventosIncrustados.CalcularIndex();
                                //RedirectWithSubfolder(`/grupocargo/agregar/${$IdGrupoCargo.val()}`);
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                        });


                    $btnEPPGuardar.attr("disabled", false);
                }
            })
        },
        botonAgregar: function () {
            var contadorCantidad = 0;
            var contadorDescripcion = 0;
            $btnKitAgregar.on("click", function () {
                var cant = $txtCantidad.val();
                var txtbusqueda = $txtBusqueda.val();

                if (cant == "") {
                    if (contadorCantidad == 0) {
                        $formularioEPPGrupoCargo.find("[name='CNTDD']").add('error');
                        $formularioEPPGrupoCargo.find("[name='ValCNTDD']").after('<label id="CNTDD-error" class="error VCNTDDA" for="CNTDD">Ingrese Cantidad </label>');
                        $txtCantidad.addClass('error');
                        contadorCantidad = 1;
                    }
                } else {
                    $formularioEPPGrupoCargo.find("[name='CNTDD']").removeClass('error');
                    $txtCantidad.removeClass('error');
                    $(".VCNTDDA").remove();
                    contadorCantidad = 0;
                }

                if (txtbusqueda == "") {
                    if (contadorDescripcion == 0) {
                        $formularioEPPGrupoCargo.find("[name='txtBusqueda']").add('error');
                        $formularioEPPGrupoCargo.find("[name='ValtxtBusqueda']").after('<label id="txtBusqueda-error" class="error VtxtBusqueda" for="CNTDD">Ingrese Descripción </label>');
                        $txtBusqueda.addClass('error');
                        contadorDescripcion = 1;
                    }
                } else {
                    $formularioEPPGrupoCargo.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    $txtBusqueda.remove('error');
                    contadorDescripcion = 0;
                }

                if (cant != "" && txtbusqueda != "") {
                    $formularioEPPGrupoCargo.find("[name='CNTDD']").removeClass('error');
                    $txtCantidad.removeClass('error');
                    $(".VCNTDDA").remove();
                    $formularioEPPGrupoCargo.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    $txtBusqueda.remove('error');
                    contadorDescripcion = 0;
                    contadorCantidad = 0;

                    //  VIENE CODIGO SAP - DESCRIPCION
                    var dataencontrada = $dataecontrada.val()           //  VIENE EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA

                    Arraycsapdesc = txtbusqueda.split('-'); // SEPARA  CODIGO SAP - DESCRIPCION
                    var csap = Arraycsapdesc[0].toString();
                    var desc = Arraycsapdesc[1].toString();

                    Arrayidgduni = dataencontrada.split('-');  //  SEPARA EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA
                    var id = Arrayidgduni[0].toString();
                    var und = Arrayidgduni[1].toString();
                    var csto = Arrayidgduni[2].toString();
                    var drcn = Arrayidgduni[3].toString();
                    var flag = false;

                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var csaptabla = $(this).children('td').find("[class='csap']").text()

                        if (csap.trim() == csaptabla.trim()) {
                            $(this).children('td').find(".cntdd").val(cant);
                            flag = true
                        }
                    })

                    if (flag) {
                        $txtBusqueda.val("");
                        $txtCantidad.val("");
                        $dataecontrada.val("");
                        $txtBusqueda.attr("disabled", false);
                        $txtBusqueda.focus();
                        eventosIncrustados.CalcularIndex();
                        return;
                    }

                    //$('#divtabla').css("visibility", "visible ");
                    var trs = $("#tabla_articuloadd tbody tr").length + 1;
                    var htmlTags = eventosIncrustados.formatoFilaTabla(trs, 0, id.trim(), csap.trim(), desc.trim(), und.trim(), cant.trim(), csto.trim(), drcn.trim(), false, false);


                    $('#tabla_articuloadd tbody').append(htmlTags);
                    $txtBusqueda.val("");
                    $txtCantidad.val("");
                    $dataecontrada.val("");
                    $txtBusqueda.attr("disabled", false);
                    $txtBusqueda.focus();
                }
                //eventosIncrustados.TotalCosto();
                eventosIncrustados.CalcularIndex();
            })

        },
        botonLimpiar: function () {
            $btnKitLimpiar.on("click", function () {
                $txtBusqueda.val("");
                $dataecontrada.val("");
                $txtBusqueda.attr("disabled", false);
                $txtBusqueda.focus();
            })
        },
        solonumero: function () {
            $txtCantidad.on("keypress", function () {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
                return true;
            });
            $txtCantidad.on("keyup", function () {
                var cant = $txtCantidad.val();
                if (cant != "") {
                    $formularioEPPGrupoCargo.find("[name='CNTDD']").removeClass('error');
                    $(".VCNTDDA").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
            $txtBusqueda.on("keyup", function () {
                var txtBusqueda = $txtBusqueda.val();
                if (txtBusqueda != "") {
                    $formularioEPPGrupoCargo.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
        },
        formatoFilaTabla: function (trs, id, idarticulo, csap, desc, unidadmedida, cant, costo, duracion, obteniendo = false, FlagEstado = false) {
            var $filanodatos = $("#tabla_articuloadd tbody").find('.noData').parent();
            if ($filanodatos) {
                $filanodatos.remove();
            }

            //FUSION DE LAS 3
            var htmlTags = "<tr>" +
                '<td class="text-center"><label>' + trs + '</label></td>' +
                '<td class="text-center"><label class="csap">' + csap + '</label>' +
                '<input class="idkitdetalle" type="hidden" value="' + id + '">' +
                '<input class="idartclo" type="hidden" value="' + idarticulo + '">' +

                '</td > ' +
                '<td class="text-left"><label>' + desc + '</label></td>' +
                '<td class="text-center"><label>' + unidadmedida + '</label></td>' +
                '<td class="text-right"><label>' + duracion + '</label></td>' +
                '<td class=""><input class="cntdd cntdd-' + idarticulo + ' text-center form-control form-control-sm" disabled="disabled" data-anterior="' + cant + '" type="number" min="0" value="' + cant + '"/><input class="csto" type="hidden" value="' + costo + '"></td>' +
                '<td class="text-center">';
            if (!FlagEstado) {
                htmlTags += '<button type="button" title="Editar" data-id="' + idarticulo + '" class="btn btn-info btn-xs btn-editar-tabla "><span><i class="la la-edit"></i></button> ';
                //
                if (obteniendo) {
                    htmlTags += ' <button id="delete_' + id + '" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                } else {
                    htmlTags += ' <button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                }
            } else {
                htmlTags += '';
            }
            htmlTags += '</td>' +
                '</tr>';
            return htmlTags;
        },
        init: function () {
            //eventosIncrustados.TotalCosto();
            eventosIncrustados.CalcularIndex();
            eventosIncrustados.EliminarFila();
            eventosIncrustados.botonBuscar();
            eventosIncrustados.botonAgregar();
            eventosIncrustados.editarCantidadFila();
            eventosIncrustados.botonAgregarArticulo();
            eventosIncrustados.botonLimpiar();
            eventosIncrustados.botonGrabarKit();
            eventosIncrustados.solonumero();
        }
    }

    var autocomplete = {
        init: function () {
            $txtBusqueda.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/articulo/search",
                        dataType: "json",
                        data: {
                            term: request.term,
                            flagKit: true,
                        },
                        success: function (data) {
                            //console.log(data);
                            if (data.length == 1) {
                                $txtBusqueda.val(data[0].label);
                                $dataecontrada.val(data[0].value2);
                                $txtBusqueda.attr("disabled", true);
                                $txtCantidad.focus();
                            } else {
                                response(data);
                            }
                        },
                        error: function (xhr, status, error) {
                            alert(xhr + " " + status + " " + error);
                        },
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    $txtBusqueda.val(ui.item.label);
                    $dataecontrada.val(ui.item.value2);
                    $txtBusqueda.attr("disabled", true);
                    $txtCantidad.focus();

                    return false;
                }
            });
        }
    }

    var obtenerDatosGrupoCargo = {
        obtenerDetalle: function (FlagEstado) {
            $.get("/grupocargo/listar-detalle", { id: $IdGrupoCargo.val() })
                .done(function (data) {
                    if (data.length) {
                        $('#tabla_articuloadd tbody').html('');
                        for (var i = 0; i < data.length; i++) {
                            var trs = i + 1;
                            var htmlTags = eventosIncrustados.formatoFilaTabla(trs, data[i].id, data[i].idartclo, data[i].csap, data[i].dscrpcn, data[i].gdunddmdda, data[i].cntdd, data[i].csto, data[i].drcn, true, FlagEstado);
                            $('#tabla_articuloadd tbody').append(htmlTags);
                        }
                    } else {
                        $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
                    }
                });
        },
        init: function () {
            var FlagEstado = false
            $.get("/grupocargo/editar/" + $IdGrupoCargo.val())
                .done(function (data) {
                    $formularioEPPGrupoCargo.find("[name='GRPOCRGO']").val(data.dscrpcn);
                    $formularioEPPGrupoCargo.AgregarCamposAuditoria(data);

                    if (data.gdestdo == 'I') {
                        $DivBusqueda.remove();
                        $btnEPPGuardar.remove();
                        FlagEstado = true;
                    }

                    obtenerDatosGrupoCargo.obtenerDetalle(FlagEstado);
                });
        }
    };

    return {
        init: function () {
            selects.init();
            tablaArticulo.inicializador();
            modalEPPArticulo.init();
            autocomplete.init();
            eventosIncrustados.init();
            $formularioEPPGrupoCargo.AgregarCamposDefectoAuditoria();
            $formularioEPPGrupoCargo.DeshabilitarCamposAuditoria();

            if ($IdGrupoCargo.val()) {
                obtenerDatosGrupoCargo.init();
            } else {
                $('#tabla_articuloadd tbody').html('');
                $('#tabla_articuloadd tbody').html('<tr><td></td><td colspan="6" class="text-center noData" >No hay datos disponibles</td></tr>');
            }
        }
    };
}();

$(() => {
    InicializarGrupoCargo.init();
})

