﻿var InicializarGrupoCargo = function () {
    var $selectEstados = $(".select-estados");
    //variables JQuery    //variables JQuery
    var $tablaGrupoCargo = $("#tabla_grupocargo");
    var $formularioGrupoCargo = $("#grupocargo_form");
    var $modalGrupoCargo = $("#modal_grupocargo");
    //

    var $accesoAgregarGrupoCargo = $("#accesoAgregarGrupoCargo");
    var $accesoEditarGrupoCargo = $("#accesoEditarGrupoCargo");
    var $accesoEliminarGrupoCargo = $("#accesoEliminarGrupoCargo");

    var $btnGrupoCargo = $("#btnGrupoCargo");

    var entidadGrupoCargo = {
        id: "",
        dscrpcn: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: ""
    }
    var validacionControles = {
        init: function () {
            if ($accesoAgregarGrupoCargo.val() == "False") {
                $btnGrupoCargo.remove();
            }
        }
    };

    var tablaGrupoCargo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/grupocargo/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "uedcn",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    width: "10%",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "EPP",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '2%',
                    render: function (data) {
                        var tpm = "";
                        tpm += `<button title="EPP" data-id="${data.id}" class="btn btn-secondary btn-xs btn-ver"><span><i class="la la-hard-hat"></i><span></span></span></button>`;

                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarGrupoCargo.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_grupocargo" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarGrupoCargo.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaGrupoCargo.objeto.on("click", ".btn-ver", function () {
                var id = $(this).data("id");
                RedirectWithSubfolder(`/grupocargo/agregar/${id}`);
            });
            tablaGrupoCargo.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/grupocargo/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaGrupoCargo.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaGrupoCargo.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioGrupoCargo.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/grupocargo/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadGrupoCargo = result;
                        $formularioGrupoCargo.find("[name='DSCRPCN']").val(entidadGrupoCargo.dscrpcn);

                        $formularioGrupoCargo.AgregarCamposAuditoria(entidadGrupoCargo);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioGrupoCargo.find(":input").attr("disabled", false);
                        $formularioGrupoCargo.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaGrupoCargo.objeto = $tablaGrupoCargo.DataTable(tablaGrupoCargo.opciones);
            tablaGrupoCargo.eventos();
        }
    };
    var modalGrupoCargo = {
        form: {
            objeto: $formularioGrupoCargo.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioGrupoCargo.find(":input").attr("disabled", true);

                    if (!$formularioGrupoCargo.find("[name='ID']").val()) {
                        url = `/grupocargo/insertar`
                    } else {
                        url = `/grupocargo/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGrupoCargo.reload();
                                modalGrupoCargo.eventos.reset();
                                $modalGrupoCargo.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioGrupoCargo.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalGrupoCargo.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalGrupoCargo.on('hidden.bs.modal', function () {
                    modalGrupoCargo.eventos.reset();
                    $formularioGrupoCargo.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalGrupoCargo.on('shown.bs.modal', function () {
                    if (!$formularioGrupoCargo.find("[name='ID']").val()) {
                        $formularioGrupoCargo.AgregarCamposDefectoAuditoria();
                        $formularioGrupoCargo.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalGrupoCargo.form.eventos.reset();
                $formularioGrupoCargo.trigger("reset");
                $formularioGrupoCargo.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        init: function () {
            $selectEstados.LlenarSelectEstados();
        }
    };
    return {
        init: function () {
            selects.init();
            tablaGrupoCargo.inicializador();
            modalGrupoCargo.init();

            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarGrupoCargo.init();
})

