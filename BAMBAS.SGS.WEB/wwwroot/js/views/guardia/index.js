﻿var InicializarGuardia = function () {
    var $selectEstados = $(".select-estados");
    var $selectTurno = $(".select-turno");
    var $selectRoster = $(".select-roster");
    //variables JQuery    //variables JQuery
    var $tablaGuardia = $("#tabla_guardia");
    var $formularioGuardia = $("#guardia_form");
    var $modalGuardia = $("#modal_guardia");
    //
    var $accesoAgregarGuardia = $("#accesoAgregarGuardia");
    var $accesoEliminarGuardia = $("#accesoEliminarGuardia");

    var $btnGuardia = $("#btnGuardia");

    var validacionControles = {
        init: function () {
            if ($accesoAgregarGuardia.val() == "False") {
                $btnGuardia.remove();
            }
        }
    };

    var entidadGuardia = {
        id: "",
        dscrpcn: "",
        gdtrno: "",
        idrstr: "",

        finco: "",
        dsgrda: "",
        hraincoda: "",
        hrafnda: "",
        nchsgrda: "",
        hrainconche: "",
        hrafnnche: "",
        ddscnso: "",

        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }
    var tablaGuardia = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/guardia/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    width: "15%",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Roster",
                    data: "rstr", width: '10%',
                    orderable: false
                },
                {
                    title: "Turno",
                    className: "text-left",
                    data: "gdtrno", width: '10%',
                    orderable: false
                },
                {
                    title: "Días Guardia",
                    className: "text-right",
                    data: "dsgrda", width: '10%',
                    orderable: false
                },
                {
                    title: "Noches Guardia",
                    className: "text-right",
                    data: "nchsgrda", width: '10%',
                    orderable: false
                },
                {
                    title: "Días Descanso",
                    className: "text-right",
                    data: "dsdscnso", width: '10%',
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },

                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Ver",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '2%',
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a class="btn  btn-xs btn-ver" data-toggle="modal" data-target="#modal_guardia" data-id="${data.id}" title="Ver"><i class="la la-eye"></i></a>`;

                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEliminarGuardia.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaGuardia.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/guardia/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaGuardia.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            //tablaGuardia.objeto.on("click", ".btn-editar", function () {
            //    var id = $(this).data("id");
            //    $formularioGuardia.find(":input").attr("disabled", true);
            //    $.ajax({
            //        url: `/guardia/editar/${id}`,
            //        type: "Get"
            //    })
            //        .done(function (result) {
            //            entidadGuardia = result;
            //            $formularioGuardia.find("[name='DSCRPCN']").val(entidadGuardia.dscrpcn);
            //            $formularioGuardia.AgregarCamposAuditoria(entidadGuardia);
            //        })
            //        .fail(function (e) {
            //        })
            //        .always(function () {
            //            $formularioGuardia.find(":input").attr("disabled", false);
            //            $formularioGuardia.DeshabilitarCamposAuditoria();
            //        });
            //});

            tablaGuardia.objeto.on("click", ".btn-ver", function () {
                var id = $(this).data("id");
                $formularioGuardia.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/guardia/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadGuardia = result;
                        $formularioGuardia.find("[name='DSCRPCN']").val(entidadGuardia.dscrpcn);
                        $formularioGuardia.find("[name='IDRSTR']").val(entidadGuardia.idrstr);
                        $formularioGuardia.find("[name='GDTRNO']").val(entidadGuardia.gdtrno);

                        $formularioGuardia.find("[name='FINCO']").val(entidadGuardia.finco);

                        $formularioGuardia.find("[name='DSGRDA']").val(entidadGuardia.dsgrda);
                        $formularioGuardia.find("[name='HRAINCODA']").val(entidadGuardia.hraincoda);
                        $formularioGuardia.find("[name='HRAFNDA']").val(entidadGuardia.hrafnda);

                        $formularioGuardia.find("[name='NCHSGRDA']").val(entidadGuardia.nchsgrda);
                        $formularioGuardia.find("[name='HRAINCONCHE']").val(entidadGuardia.hrainconche);
                        $formularioGuardia.find("[name='HRAFNNCHE']").val(entidadGuardia.hrafnnche);

                        

                        $formularioGuardia.AgregarCamposAuditoria(entidadGuardia);
                    })
                    .always(function () {
                        
                        $formularioGuardia.find(":input").attr("disabled", true);
                        $formularioGuardia.find("button[type='submit']").hide();
                        $formularioGuardia.find("button[type='button']").attr("disabled", false);
                        $formularioGuardia.DeshabilitarCamposAuditoria();
                        
                        //$formularioGuardia.find("button[type='submit']").show();
                    });
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaGuardia.objeto = $tablaGuardia.DataTable(tablaGuardia.opciones);
            tablaGuardia.eventos();
        }
    };
    var modalGuardia = {
        form: {
            objeto: $formularioGuardia.validate({
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {

                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioGuardia.find(":input").attr("disabled", true);

                    if (!$formularioGuardia.find("[name='ID']").val()) {
                        url = `/guardia/insertar`
                    } else {
                        url = `/guardia/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaGuardia.reload();
                                modalGuardia.eventos.reset();
                                $modalGuardia.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioGuardia.find(":input").attr("disabled", false);
                            $(".select-turno").change();
                            $formularioGuardia.find("[name='DSDSCNSO']").attr("disabled", true);
                            $formularioGuardia.find("button[type='submit']").show();
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalGuardia.form.objeto.resetForm();

                    //limpiar.clear();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalGuardia.on('hidden.bs.modal', function () {
                    modalGuardia.eventos.reset();
                })
            },
            onShow: function () {
                $modalGuardia.on('shown.bs.modal', function () {
                    if (!$formularioGuardia.find("[name='ID']").val()) {
                        $formularioGuardia.AgregarCamposDefectoAuditoria();
                        $formularioGuardia.DeshabilitarCamposAuditoria();
                    }
                })


            },
            reset: function () {
                modalGuardia.form.eventos.reset();
                $formularioGuardia.trigger("reset");
                $formularioGuardia.find(":input").attr("disabled", false);
                datepickers.horaInicio();
                limpiar.clear();

            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var datepickers = {
        opciones: {
            language: "es",
            assumeNearbyYear: true,
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            keyboardNavigation: true
        },
        opcionesTimepicker: {
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '0',
            maxTime: '23',
            defaultTime: '',
            startTime: '',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            zindex: 9999999,
            template: 'modal'
        },
        horaInicio: function () {
            datepickers.opcionesTimepicker.startTime = '9';
            datepickers.opcionesTimepicker.defaultTime = '9';
            $formularioGuardia.find("[name='HRAINCODA']").timepicker(datepickers.opcionesTimepicker);
            datepickers.opcionesTimepicker.startTime = '21';
            datepickers.opcionesTimepicker.defaultTime = '21';
            $formularioGuardia.find("[name='HRAFNDA']").timepicker(datepickers.opcionesTimepicker);
            datepickers.opcionesTimepicker.startTime = '21';
            datepickers.opcionesTimepicker.defaultTime = '21';
            $formularioGuardia.find("[name='HRAINCONCHE']").timepicker(datepickers.opcionesTimepicker);
            datepickers.opcionesTimepicker.startTime = '9';
            datepickers.opcionesTimepicker.defaultTime = '9';
            $formularioGuardia.find("[name='HRAFNNCHE']").timepicker(datepickers.opcionesTimepicker);
        },
        fechaInicio: function () {
            $formularioGuardia.find("[name='FINCO']").datepicker(datepickers.opciones);
            $formularioGuardia.find("[name='FINCO']").on("change", function () {

            });
        },
        init: function () {
            this.fechaInicio();
            this.horaInicio();
        }
    };

    var selects = {
        roster: function () {
            $selectRoster.append($("<option />").val('').text("Cargando..."));
            $.get("/roster/obtener-activos")
                .done(function (data) {
                    $.each(data, function (key, item) {
                        $selectRoster.append($(`<option data-trabajados='${item["dtrbjds"]}' data-descanso='${item["ddscnso"]}'/>`).val(item["id"]).text(item["dscrpcn"]));
                    });
                    $selectRoster.children('option[value=""]').text("Seleccione");
                });
            $selectRoster.on("change", function () {
                var $this = $(this);
                var id = $this.val();
                if (id) {
                    
                    $formularioGuardia.find("[name='DSDSCNSO']").attr("disabled", true);
                    var diasDescanso = $this.children(`option[value="${id}"]`).data("descanso");
                    $formularioGuardia.find("[name='DSDSCNSO']").val(diasDescanso);
                    $formularioGuardia.find("[name='DSDSCNSO']").attr("disabled", false);

                } else {

                    $formularioGuardia.find("[name='DSGRDA']").val("");
                    $formularioGuardia.find("[name='NCHSGRDA']").val("");
                    $formularioGuardia.find("[name='DSDSCNSO']").val("");

                }
                $selectTurno.val('');
                $selectTurno.change();

            });

        },
        turno: function () {
            $selectTurno.on("change", function () {
                var $this = $(this);
                var id = $this.val();
                var idroster = $selectRoster.val();

                if (id == 1) {

                    var diasTrabajados = $selectRoster.children(`option[value="${idroster}"]`).data("trabajados");
                    $formularioGuardia.find("[name='DSGRDA']").val(diasTrabajados);
                    $formularioGuardia.find("[name='NCHSGRDA']").val("");
                    $formularioGuardia.find("[name='HRAINCONCHE']").val("");
                    $formularioGuardia.find("[name='HRAFNNCHE']").val("");
                    var diasDescanso = $selectRoster.children(`option[value="${idroster}"]`).data("descanso");
                    $formularioGuardia.find("[name='DSDSCNSO']").val(diasDescanso);

                    $formularioGuardia.find("[name='DSGRDA']").attr("disabled", false);
                    $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", false);
                    $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", false);

                    $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", true);
                }

                else if (id == 2) {

                    var diasTrabajados = $selectRoster.children(`option[value="${idroster}"]`).data("trabajados");
                    $formularioGuardia.find("[name='DSGRDA']").val("");
                    $formularioGuardia.find("[name='HRAINCODA']").val("");
                    $formularioGuardia.find("[name='HRAFNDA']").val("");
                    $formularioGuardia.find("[name='NCHSGRDA']").val(diasTrabajados);
                    var diasDescanso = $selectRoster.children(`option[value="${idroster}"]`).data("descanso");
                    $formularioGuardia.find("[name='DSDSCNSO']").val(diasDescanso);

                    $formularioGuardia.find("[name='DSGRDA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", true);

                    $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", false);
                    $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", false);
                    $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", false);
                }

                else if (id == 3) {
                    debugger;
                    var diasTrabajados = $selectRoster.children(`option[value="${idroster}"]`).data("trabajados");
                    if (isNaN(diasTrabajados)) { diasTrabajados = 0 };
                    if (diasTrabajados == 0) {
                        $formularioGuardia.find("[name='DSGRDA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", false);

                        $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", false);
                    } else {
                        var dias = Math.ceil(diasTrabajados / 2);
                        var noches = Math.floor(diasTrabajados / 2);
                        $formularioGuardia.find("[name='DSGRDA']").val(dias);
                        $formularioGuardia.find("[name='NCHSGRDA']").val(noches);
                        var diasDescanso = $selectRoster.children(`option[value="${idroster}"]`).data("descanso");
                        $formularioGuardia.find("[name='DSDSCNSO']").val(diasDescanso);

                        $formularioGuardia.find("[name='DSGRDA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", false);

                        $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", false);
                        $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", false);
                    }
                }

                else {

                    $formularioGuardia.find("[name='DSGRDA']").val("");
                    $formularioGuardia.find("[name='NCHSGRDA']").val("");
                    $formularioGuardia.find("[name='DSDSCNSO']").val("");

                    $formularioGuardia.find("[name='DSGRDA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", true);

                    $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", true);
                    $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", true);
                }
            });
        },

        calcularroster: function () {

            $formularioGuardia.find("[name='DSGRDA']").on("keyup", function () {

                var id = $formularioGuardia.find("[name='IDRSTR']").val();
                var DiasRoster = $formularioGuardia.find("[name='IDRSTR']").children(`option[value="${id}"]`).data("trabajados");

                var DiasGuardia = $formularioGuardia.find("[name='DSGRDA']").val();
                if (isNaN(DiasGuardia)) { DiasGuardia = 0 };

                var NochesRoster = DiasRoster - DiasGuardia;
                if (isNaN(NochesRoster)) { NochesRoster = 0 };
                $formularioGuardia.find("[name='NCHSGRDA']").val(NochesRoster);
            });

            $formularioGuardia.find("[name='NCHSGRDA']").on("keyup", function () {
                var id = $formularioGuardia.find("[name='IDRSTR']").val();
                var DiasRoster = $formularioGuardia.find("[name='IDRSTR']").children(`option[value="${id}"]`).data("trabajados");

                var NochesRoster = $formularioGuardia.find("[name='NCHSGRDA']").val();
                if (isNaN(NochesRoster)) { NochesRoster = 0 };

                var DiasGuardia = DiasRoster - NochesRoster;
                if (isNaN(DiasGuardia)) { DiasGuardia = 0 };
                $formularioGuardia.find("[name='DSGRDA']").val(DiasGuardia);
            });
        },

        init: function () {
            $selectEstados.LlenarSelectEstados();
            $selectTurno.LlenarSelectGD("GDTGRDA");

            selects.roster();
            selects.turno();
            selects.calcularroster();
        }
    };

    var limpiar = {

        clear: function () {
            $formularioGuardia.find("[name='HRAINCODA']").attr("disabled", true);
            $formularioGuardia.find("[name='HRAFNDA']").attr("disabled", true);
            $formularioGuardia.find("[name='HRAINCONCHE']").attr("disabled", true);
            $formularioGuardia.find("[name='HRAFNNCHE']").attr("disabled", true);
            $formularioGuardia.find("[name='DSGRDA']").attr("disabled", true);
            $formularioGuardia.find("[name='NCHSGRDA']").attr("disabled", true);
            $formularioGuardia.find("[name='DSDSCNSO']").attr("disabled", true);
        }

    };

    return {
        init: function () {
            selects.init();
            tablaGuardia.inicializador();
            modalGuardia.init();
            datepickers.init();
            limpiar.clear();
            //validacionControles.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarGuardia.init();
})

