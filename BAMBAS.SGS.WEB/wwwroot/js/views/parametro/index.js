﻿var initParametros = function () {
    $btnNuevo = $(".btn-nuevo");
    $tblParametro = $("#tabla_parametro");
    $mdlParametro = $("#mdlParametro");
    $modalTitulo = $mdlParametro.find(".modal-title");
    $formParametro = $("#parametro_agregar_form");
    $txtCodigo = $("#txtCodigo");
    $txtDescripcion = $("#txtDescripcion");
    $txtAbreviatura = $("#txtAbreviatura");
    $txtValor1 = $("#txtValor1");
    $txtValor2 = $("#txtValor2");

    $cboEstado = $("#cboEstado");
    $txtUCreacion = $("#txtUCreacion");
    $txtUEdicion = $("#txtUEdicion");
    $txtFEstado = $("#txtFEstado");
    $txtFCreacion = $("#txtFCreacion");
    $txtFEdicion = $("#txtFEdicion");
    $hfaction = $("#hfaction");

    var $accesoAgregarParametro = $("#accesoAgregarParametro");
    var $accesoEditarParametro = $("#accesoEditarParametro");
    var $accesoEliminarParametro = $("#accesoEliminarParametro");

    var entidadParametro = {
        id: "",
        dprmtro: "",
        aprmtro: "",
        vlR1: "",
        vlR2: "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
        cFCRCN: "",
        cFESTDO: "",
        cFEDCN: ""
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarParametro.val() == "False") {
                $btnNuevo.remove();
            }
        }
    };
    //config : configuracion
    var configDTParametro = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/Parametros/listarparametros",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    //data.search["value"] = $tblParametro.find("input[type='search']").val()
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Descripción",
                    data: "dprmtro",
                    className: "text-left"
                },
                {
                    title: "Valor 1",
                    data: "vlR1", width: '10%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    title: "Valor 2",
                    data: "vlR2", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    orderable: false,
                    width: '11%',
                    class: "text-center",
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarParametro.val() == "True") {
                            tpm += `<button type="button" class="btn btn-info btn-xs btn-editar" data-toggle="modal" data-target="#mdlParametro" data-id="${data.id}" title="Editar"><span><i class="la la-edit"></i></span></button>`;
                        }
                        if ($accesoEliminarParametro.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            eliminar: function () {
                configDTParametro.objecto.on("click", ".btn-delete", function () {
                    var id = $(this).data("id");
                    fnParametros.eliminar(id);
                })
            },
            editar: function () {
                configDTParametro.objecto.on("click", ".btn-editar", function () {
                    var id = $(this).data("id");
                    $modalTitulo.text("Editar Parámetro");
                    $hfaction.val("E");
                    fnParametros.obtener(id);
                })
            },
            init: function () {
                configDTParametro.eventos.eliminar();
                configDTParametro.eventos.editar();
            }
        },
        reload: function () {
            configDTParametro.objecto.ajax.reload();
        },
        init: function () {
            configDTParametro.objecto = $tblParametro.DataTable(configDTParametro.opciones);
            configDTParametro.eventos.init();
        }
    };
    var configModalParametro = {
        form: {
            objeto: $formParametro.validate({
                rules: {
                    DPRMTRO: {
                        required: true,
                        maxlength: 150
                    },
                    VLR1: {
                        required: true,
                        maxlength: 250
                    }
                }
            }),
            eventos: {
                reset: function () {
                    configModalParametro.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $mdlParametro.on("hidden.bs.modal", function () {
                    configModalParametro.eventos.reset();
                })
            },
            onShow: function () {
                $mdlParametro.on("shown.bs.modal", function () {
                    if ($hfaction.val() == "N") {
                        $formParametro.AgregarCamposDefectoAuditoria();
                        $formParametro.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                configModalParametro.form.eventos.reset();
                $formParametro.trigger("reset");
                $formParametro.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    }
    var fnParametros = {
        guardar: function () {
            if ($hfaction.val() == "N") {
                fnParametros.insertar();
            }
            else {
                fnParametros.actualizar();
            }
        },
        insertar: function () {
            if ($formParametro.valid()) {
                entidadParametro.dprmtro = $txtDescripcion.val();
                entidadParametro.aprmtro = $txtAbreviatura.val();
                entidadParametro.vlR1 = $txtValor1.val();
                entidadParametro.vlR2 = $txtValor2.val();
                entidadParametro.gdestdo = $cboEstado.val();
                $accesoAgregarParametro.attr("disabled", true);
                $.post("/Parametros/insertarparametro", entidadParametro)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se registro satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            configDTParametro.reload();
                            $mdlParametro.modal("hide");
                            $hfaction.val("");
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al insertar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
            }
        },
        actualizar: function () {
            if ($formParametro.valid()) {
                entidadParametro.id = $txtCodigo.val();
                entidadParametro.dprmtro = $txtDescripcion.val();
                entidadParametro.aprmtro = $txtAbreviatura.val();
                entidadParametro.vlR1 = $txtValor1.val();
                entidadParametro.vlR2 = $txtValor2.val();
                entidadParametro.gdestdo = $cboEstado.val();
                $.post("/Parametros/actualizarparametro", entidadParametro)
                    .done(function () {
                        Swal.fire({
                            icon: "success",
                            allowOutsideClick: false,
                            title: "Éxito",
                            text: "Se actualizó satisfactoriamente.",
                            confirmButtonText: "Aceptar"
                        }).then((result) => {
                            configDTParametro.reload();
                            $mdlParametro.modal("hide");
                            $hfaction.val("");
                        });
                    }).fail(function (e) {
                        Swal.fire({
                            icon: "error",
                            title: "Error al modificar el registro.",
                            text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                            confirmButtonText: "Aceptar"
                        });
                    });
            }
        },
        obtener: function (codigo) {
            var parametro = { id: codigo };
            $.post("/Parametros/obtenerparametro", parametro)
                .done(function (data) {
                    entidadParametro = data;
                    $txtCodigo.val(entidadParametro.id);
                    $txtDescripcion.val(entidadParametro.dprmtro);
                    $txtAbreviatura.val(entidadParametro.aprmtro);
                    $txtValor1.val(entidadParametro.vlR1);
                    $txtValor2.val(entidadParametro.vlR2);
                    $formParametro.AgregarCamposAuditoria(entidadParametro);
                })
                .fail().always(function () {
                    $formParametro.find(":input").attr("disabled", false);
                    $formParametro.DeshabilitarCamposAuditoria();
                });
        },
        eliminar: function (codigo) {
            var parametro = { id: codigo };
            Swal.fire({
                title: "¿Quiere modificar el estado del registro?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Aceptar",
                confirmButtonClass: "btn btn-danger",
                cancelButtonText: "Cancelar"
            }).then(function (result) {
                if (result.value) {
                    $.post("/Parametros/eliminarparametro", parametro)
                        .done(function (data) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTParametro.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                }
            });
        },
    }

    var eventosIncrustados = {
        NuevoParametro: function () {
            $btnNuevo.click(function () {
                $modalTitulo.text("Nuevo Parámetro");
                $hfaction.val("N");
            })
        },
        GuardarParametro: function () {
            $formParametro.find(".btn-save").click(function () {
                $formParametro.find(".btn-save").attr("disabled", false);
                fnParametros.guardar();
                $formParametro.find(".btn-save").attr("disabled", true);
            })
        },
        init: function () {
            // ejecuta funciones creadas
            eventosIncrustados.NuevoParametro();
            eventosIncrustados.GuardarParametro();
        }
    }
    var IniEstados = {
        init: function () {
            $cboEstado.LlenarSelectEstados();
        }
    };
    return {
        init: function () {
            configDTParametro.init();
            configModalParametro.init();
            eventosIncrustados.init();
            IniEstados.init();

            validacionControles.init();
        }
    };
}();

$(() => {
    initParametros.init();
})