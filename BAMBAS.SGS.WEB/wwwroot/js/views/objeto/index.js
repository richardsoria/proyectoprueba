﻿//var bandejaConvenio = (function ($, win, doc) {
var InicializarObjeto = function () {
    //variables JQuery
    var $tablaObjetoPadre = $("#tabla_objeto_padre");
    var $formularioObjetoPadre = $("#objeto_padre_form");
    var $modalObjetoPadre = $("#modal_objeto_padre");
    var $btnNuevoObjetoPadre = $("#nuevo-objeto-padre");

    var $tablaObjeto = $("#tabla_objeto");
    var $formularioObjeto = $("#objeto_form");
    var $modalObjeto = $("#modal_objeto");
    var $btnNuevoObjeto = $("#nuevo-objeto");
    //
    var $formularioObjetoDetalle = $("#objetodetalle_form");
    var $modalObjetoDetalle = $("#modal_objetodetalle");
    var $tablaObjetoDetalle = $("#tabla_objetodetalle");
    var $btnNuevoObjetoDetalle = $("#nuevo-objeto-detalle");
    //
    var $codigoObjetoPadre = $("#codigoObjetoPadre");
    var $codigoObjeto = $("#codigoObjeto");
    var $codigoObjetoDetalle = $("#codigoObjetoDetalle");

    var $tab_objeto_padre = $("#objeto-padre-tab");
    var $tab_objeto = $("#objeto-tab");
    var $tab_objetoDetalle = $("#objetodetalle-tab");

    var $selectEstados = $(".select-estados");
    var $selectModulos = $(".select-modulos");

    var $selectObjeto = $(".select-objeto");

    var $lblobjeto = $("#lblobjeto");

    var $btnNuevoDetalle = $("#nuevo-detalle");
    //CONTROLES

    var $accesoAgregarObjeto = $("#accesoAgregarObjeto");
    var $accesoEditarObjeto = $("#accesoEditarObjeto");
    var $accesoEliminarObjeto = $("#accesoEliminarObjeto");
    var $accesoAgregarObjetoDetalle = $("#accesoAgregarObjetoDetalle");
    var $accesoEditarObjetoDetalle = $("#accesoEditarObjetoDetalle");
    var $accesoEliminarObjetoDetalle = $("#accesoEliminarObjetoDetalle");

    var tablapadre = true;

    var validacionControles = {
        init: function () {
            if ($accesoAgregarObjeto.val() == "False") {
                $btnNuevoObjeto.remove();
                $btnNuevoObjetoPadre.remove();
            }
            if ($accesoAgregarObjetoDetalle.val() == "False") {
                $btnNuevoObjetoDetalle.remove();
            }
        }
    };
    //OBJETO PADRE 
    var tablaObjetoPadre = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/objeto/obtener-tabla-padre",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                
                {
                    title: "Módulo",
                    data: "mdlo",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "Nombre",
                    data: "dobjto",
                    orderable: false,
                    className: "text-left"
                },
                //{
                //    title: "Url",
                //    data: "url",
                //    orderable: false
                //},
                {
                    title: "Orden",
                    data: "fordn",
                    class: "text-center",
                    orderable: false
                },                
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Ver",
                    width: '2%',
                    data: null,
                    className: "text-center",
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        if (data.tienedetalles) {
                            tpm += `<a data-codigo="${data.id}" class="btn btn-xs btn-ver-detalles"><i class="la la-eye"></i></a>`;
                        } else {
                            tpm += `<a data-codigo="${data.id}" data-hijos="${data.tienehijos}" class="btn btn-xs btn-ver"><i class="la la-eye"></i></a>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    class: "text-center",
                    orderable: false,
                    width: '5%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarObjeto.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_objeto_padre" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarObjeto.val() == "True" && data.idobjtopdre != null) {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaObjetoPadre.objecto.on("click", ".btn-ver-detalles", function () {
                var data = tablaObjetoPadre.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.nobjto, "rastroobjetoPadre");
                //$lblobjeto.text(data.nobjto);
                //$codigoObjetoPadre.val($(this).data("codigo"));
                $codigoObjeto.val($(this).data("codigo"));
                tablaObjetoDetalle.inicializador();
                $tab_objetoDetalle.removeClass("disabled");
                $tab_objetoDetalle.click();
            });
            tablaObjetoPadre.objecto.on("click", ".btn-ver", function () {
                var data = tablaObjetoPadre.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.nobjto, "rastroobjetoPadre");
                //$lblobjeto.text(data.nobjto);
                var tienehijos = $(this).data("hijos");
                var codigo = $(this).data("codigo");
                if (tienehijos) {
                    $btnNuevoDetalle.hide();
                } else {
                    $btnNuevoDetalle.show();
                }
                $codigoObjetoPadre.val(codigo);
                tablaObjeto.inicializador();
                $tab_objeto.removeClass("disabled");
                $tab_objeto.click();
            });
            tablaObjetoPadre.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/objeto/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaObjetoPadre.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaObjetoPadre.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioObjetoPadre.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/objeto/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        //selects.objeto.llenar(result.idmdlo);
                        $formularioObjetoPadre.find("[name='NOBJTO']").val(result.nobjto);
                        $formularioObjetoPadre.find("[name='FORDN']").val(result.fordn);
                        $formularioObjetoPadre.find("[name='DOBJTO']").val(result.dobjto);

                        $formularioObjetoPadre.find("[name='URL']").val(result.url);
                        $formularioObjetoPadre.find("[name='IDMDLO']").val(result.idmdlo);
                        //setTimeout(function () {
                        //$formularioObjetoPadre.find("[name='IDOBJTOPDRE']").val(result.idobjtopdre);
                        //}, 500)
                        $formularioObjetoPadre.AgregarCamposAuditoria(result);
                        $codigoObjetoPadre.val(result.id);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioObjetoPadre.find(":input").attr("disabled", false);
                        $formularioObjetoPadre.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            tablaObjetoPadre.objecto = $tablaObjetoPadre.DataTable(tablaObjetoPadre.opciones);
            tablaObjetoPadre.eventos();
        }
    };
    var modalObjetoPadre = {
        form: {
            objeto: $formularioObjetoPadre.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NOBJTO: {
                        required: true,
                        maxlength: 150,
                        sololetras: true,
                    },
                    DOBJTO: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioObjetoPadre.find(":input").attr("disabled", true);
                    var url = "";
                    var editando = 0;
                    editando = $codigoObjetoPadre.val();
                    if (editando == 0 || editando == "" || editando == undefined) {
                        url = "/objeto/insertar";
                    } else {
                        url = `/objeto/actualizar`;
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaObjetoPadre.reload();
                                modalObjetoPadre.eventos.reset();
                                $modalObjetoPadre.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioObjetoPadre.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalObjetoPadre.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalObjetoPadre.on('hidden.bs.modal', function () {
                    modalObjetoPadre.eventos.reset();
                })
            },
            onShow: function () {
                $modalObjetoPadre.on('shown.bs.modal', function () {
                    var editando = $codigoObjetoPadre.val();
                    if (editando == 0 || editando == "" || editando == undefined) {
                        $formularioObjetoPadre.AgregarCamposDefectoAuditoria();
                        $formularioObjetoPadre.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalObjetoPadre.form.eventos.reset();
                $formularioObjetoPadre.trigger("reset");
                $formularioObjetoPadre.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    //OBJETO HIJO 
    var tablaObjeto = {
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/objeto/obtener-tabla",
                type: "GET",
                data: function (data) {
                    data.idobjetopadre = $codigoObjetoPadre.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                
                {
                    title: "Nombre",
                    data: "dobjto",
                    orderable: false,
                    className: "text-left"
                },
                //{
                //    title: "Url",
                //    data: "url",
                //    orderable: false
                //},
                {
                    title: "Orden",
                    width: '5%',
                    data: "fordn",
                    class: "text-center",
                    orderable: false
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Ver",
                    width: '4%',
                    data: null,
                    className: "text-center",
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        tpm += `<a data-codigo="${data.id}" class="btn btn-xs btn-ver"><i class="la la-eye"></i></a>`;
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    data: null,
                    class: "text-center",
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarObjeto.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_objeto" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarObjeto.val() == "True" && data.idobjtopdre != null) {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaObjeto.objecto.on("click", ".btn-ver", function () {
                var data = tablaObjeto.objecto.row($(this).parents('tr')).data();
                AgregarRastro(data.nobjto, "rastroobjeto");
                //$lblobjeto.text(data.nobjto);
                $codigoObjeto.val($(this).data("codigo"));
                tablaObjetoDetalle.inicializador();
                $tab_objetoDetalle.removeClass("disabled");
                $tab_objetoDetalle.click();
            });
            tablaObjeto.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/objeto/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaObjeto.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaObjeto.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioObjeto.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/objeto/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        selects.objeto.llenar(result.idmdlo);
                        $formularioObjeto.find("[name='NOBJTO']").val(result.nobjto);
                        $formularioObjeto.find("[name='FORDN']").val(result.fordn);
                        $formularioObjeto.find("[name='DOBJTO']").val(result.dobjto);

                        $formularioObjeto.find("[name='URL']").val(result.url);
                        $formularioObjeto.find("[name='IDMDLO']").val(result.idmdlo);
                        $formularioObjeto.find("[name='IDOBJTOPDRE']").val(result.idobjtopdre);
                       
                        $formularioObjeto.AgregarCamposAuditoria(result);
                        $codigoObjeto.val(result.id);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioObjeto.find(":input").attr("disabled", false);
                        $formularioObjeto.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            if (tablaObjeto.objecto == undefined || tablaObjeto.objecto == null) {
                tablaObjeto.objecto = $tablaObjeto.DataTable(tablaObjeto.opciones);
                tablaObjeto.eventos();
            } else {
                tablaObjeto.reload();
            }
        }
    };
    var modalObjeto = {
        form: {
            objeto: $formularioObjeto.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NOBJTO: {
                        required: true,
                        maxlength: 150,
                        sololetras: true,
                    },
                    DOBJTO: {
                        required: true,
                        maxlength: 150,
                        alfanumerico: true,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioObjeto.find(":input").attr("disabled", true);
                    var url = "";
                    var editando = 0;
                    editando = $codigoObjeto.val();

                    if (editando == 0 || editando == "" || editando == undefined) {
                        url = "/objeto/insertar";
                    } else {
                        url = `/objeto/actualizar`;
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaObjeto.reload();

                                modalObjeto.eventos.reset();
                                $modalObjeto.modal("hide");
                                $btnNuevoDetalle.hide();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioObjeto.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalObjeto.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalObjeto.on('hidden.bs.modal', function () {
                    modalObjeto.eventos.reset();
                })
            },
            onShow: function () {
                $modalObjeto.on('shown.bs.modal', function () {
                    var editando = $codigoObjeto.val();
                    if (editando == 0 || editando == "" || editando == undefined) {
                        $formularioObjeto.AgregarCamposDefectoAuditoria();
                        $formularioObjeto.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalObjeto.form.eventos.reset();
                $formularioObjeto.trigger("reset");
                $formularioObjeto.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    /////DETALLE
    var tablaObjetoDetalle = {
        codigoObjeto: null,
        objecto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: `/objeto-detalle/obtener-tabla`,
                type: "GET",
                data: function (data) {
                    data.idObjeto = $codigoObjeto.val();

                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Nombre Descriptivo",
                    data: "dobjto",
                    orderable: false,
                    className: "text-left"
                },
                {
                    title: "U. Edición",
                    data: "uedcn", width: '10%',
                    orderable: false,
                    class: "text-center"
                },
                {
                    title: "F. Edición",
                    data: "cFEDCN", width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        var tpm;
                        if (data.gdestdo == "A") {
                            tpm = `<span><i class="fa fa-circle text-success" title="Activo"></i></span>`;
                        }
                        else {
                            tpm = `<span><i class="fa fa-circle text-danger" title="Inactivo"></i></span>`;
                        }
                        return tpm;
                    }
                },
                {
                    title: "Opciones",
                    class: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarObjetoDetalle.val() == "True") {
                            tpm += `<a data-toggle="modal" href="#modal_objetodetalle" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }
                        if ($accesoEliminarObjetoDetalle.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }
                        return tpm;
                    }
                }
            ],
        },
        eventos: function () {
            tablaObjetoDetalle.objecto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    //text: "Una vez modificado no podrá recuperarlo",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/objeto-detalle/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaObjetoDetalle.reload();
                                tablaObjeto.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaObjetoDetalle.objecto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioObjetoDetalle.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/objeto-detalle/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        $formularioObjetoDetalle.find("[name='NOBJTO']").val(result.nobjto);
                        $formularioObjetoDetalle.find("[name='DOBJTO']").val(result.dobjto);
                        $formularioObjetoDetalle.AgregarCamposAuditoria(result);
                        $codigoObjetoDetalle.val(id);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioObjetoDetalle.find(":input").attr("disabled", false);
                        $formularioObjetoDetalle.DeshabilitarCamposAuditoria();
                    });
            });
        },
        reload: function () {
            this.objecto.ajax.reload();
        },
        inicializador: function () {
            if (tablaObjetoDetalle.objecto == undefined || tablaObjetoDetalle.objecto == null) {
                tablaObjetoDetalle.objecto = $tablaObjetoDetalle.DataTable(tablaObjetoDetalle.opciones);
                tablaObjetoDetalle.eventos();
            } else {
                tablaObjetoDetalle.reload();
            }
        }
    };
    var modalObjetoDetalle = {
        form: {
            objeto: $formularioObjetoDetalle.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    NOBJTO: {
                        required: true,
                        maxlength: 150,
                    },
                    DOBJTO: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.addLoader();
                    var formData = new FormData(formElement);
                    $formularioObjetoDetalle.find(":input").attr("disabled", true);
                    var url = "";
                    var editando = $codigoObjetoDetalle.val();
                    if (editando == 0 || editando == "" || editando == undefined) {
                        url = "/objeto-detalle/insertar";
                    } else {
                        url = `/objeto-detalle/actualizar`;
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaObjetoDetalle.reload();
                                tablaObjetoPadre.reload();
                                modalObjetoDetalle.eventos.reset();
                                $modalObjetoDetalle.modal("hide");
                                $btnNuevoDetalle.hide();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioObjetoDetalle.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalObjetoDetalle.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalObjetoDetalle.on('hidden.bs.modal', function () {
                    modalObjetoDetalle.eventos.reset();
                })
            },
            onShow: function () {
                $modalObjetoDetalle.on('shown.bs.modal', function () {
                    var editando = $codigoObjetoDetalle.val();
                    if (editando == 0 || editando == "" || editando == undefined) {
                        $formularioObjetoDetalle.AgregarCamposDefectoAuditoria();
                        $formularioObjetoDetalle.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalObjetoDetalle.form.eventos.reset();
                $formularioObjetoDetalle.trigger("reset");
                $formularioObjetoDetalle.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var eventos = {
        init: function () {
            $tab_objeto_padre.on("click", function () {
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("rastroobjetoPadre");
                }
                $tab_objeto.addClass("disabled");
                $tab_objetoDetalle.addClass("disabled");
            });
            $tab_objeto.on("click", function () {
                if (!$(this).hasClass("active") && !$(this).hasClass("disabled")) {
                    QuitarRastro("rastroobjeto");
                }
                $tab_objetoDetalle.addClass("disabled");
            });
            $btnNuevoObjetoPadre.on("click", function () {
                $codigoObjetoPadre.val("");
                //$formularioObjetoPadre.find("[name='IDOBJTOPDRE']").val("").change();
                //$formularioObjetoPadre.find("[name='IDOBJTOPDRE']").attr("disabled", true);
                //$formularioObjetoPadre.find("[name='IDOBJTOPDRE']").attr("required", false);
            });
            $btnNuevoObjeto.on("click", function () {
                tablapadre = false;
                $codigoObjeto.val("");
                $formularioObjeto.find("[name='IDOBJTOPDRE']").val($codigoObjetoPadre.val());
            });
            $btnNuevoObjetoDetalle.on("click", function () {
                $codigoObjetoDetalle.val("");
                $formularioObjetoDetalle.find("[name='IDOBJTO']").val($codigoObjeto.val());
            });
            $btnNuevoDetalle.on("click", function () {
                $codigoObjeto.val($codigoObjetoPadre.val());
                tablaObjetoDetalle.inicializador();
                $tab_objetoDetalle.removeClass("disabled");
                $tab_objeto.addClass("disabled");
                $tab_objetoDetalle.click();
                $btnNuevoObjetoDetalle.click();
            });
        }
    }

    var selects = {
        modulos: {
            evento: function () {
                $selectModulos.on("change", function () {
                    var id = $(this).val();
                    selects.objeto.llenar(id);
                });
            },
            init: function () {
                $selectModulos.LlenarSelectGD("GDSSTMA");
                selects.modulos.evento();
            }
        },
        objeto: {
            vaciar: function () {
                $selectObjeto.find("option").each(function () {
                    $(this).remove();
                });
            },
            llenar: function (idModulo) {
                selects.objeto.vaciar();
                $selectObjeto.append($("<option />").val('').text("NINGUNO"));
                $.get(`/objeto/obtener-por-modulo`, { idModulo: idModulo })
                    .done(function (data) {
                        $.each(data, function (key, item) {
                            $selectObjeto.append($("<option />").val(item["id"]).text(item["dobjto"]));
                        });
                    });
            },
            init: function () {
                $selectObjeto.append($("<option />").val('').text("NINGUNO"));
            }
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.objeto.init();
            selects.modulos.init();
        }
    }

    return {
        init: function () {
            tablaObjetoPadre.inicializador();
            modalObjetoPadre.init();
            modalObjeto.init();
            modalObjetoDetalle.init();
            eventos.init();
            selects.init();

            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarObjeto.init();
})

