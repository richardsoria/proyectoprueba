﻿var InicializarSubFamilia = function () {
    var $selectEstados = $(".select-estados");
    var $selectFamilias = $(".select-familias");
    var $filtroFamilias = $(".filtro-select-familias");
    //variables JQuery    //variables JQuery
    var $formSelectFamilias = $(".form-select-familias");
    var $tablaSubFamilia = $("#tabla_subfamilia");
    var $formularioSubFamilia = $("#subfamilia_form");
    var $modalSubFamilia = $("#modal_subfamilia");
    //
    var $accesoAgregarSubFamilia = $("#accesoAgregarSubFamilia");
    var $accesoEditarSubFamilia = $("#accesoEditarSubFamilia");
    var $accesoEliminarSubFamilia = $("#accesoEliminarSubFamilia");

    var $btnSubFamilia = $("#btnSubFamilia");

    var entidadSubFamilia = {
        id: "",
        dscrpcn: "",
        idfmlapdre : "",
        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarSubFamilia.val() == "False") {
                $btnSubFamilia.remove();
            }
        }
    };

    var tablaSubFamilia = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/familia/get",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idFamilia = $filtroFamilias.val();
                    data.soloHijos = true;
                    return data;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Familia Padre",
                    className: "text-center",
                    data: "fmlapdre",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-center",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    data: "cFEDCN",
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '11%',
                    render: function (data) {
                        var tpm = "";
                        if ($accesoEditarSubFamilia.val() == "True") {
                        tpm += `<a data-toggle="modal" href="#modal_subfamilia" data-id="${data.id}" class="btn btn-info btn-xs btn-editar" title="Editar"><span><i class="la la-edit"></i></a>`;
                        }

                        if ($accesoEliminarSubFamilia.val() == "True") {
                        tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ]
        },
        eventos: function () {
            tablaSubFamilia.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/familia/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaSubFamilia.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaSubFamilia.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                $formularioSubFamilia.find(":input").attr("disabled", true);
                $.ajax({
                    url: `/familia/editar/${id}`,
                    type: "Get"
                })
                    .done(function (result) {
                        entidadSubFamilia = result;
                        $formularioSubFamilia.find("[name='DSCRPCN']").val(entidadSubFamilia.dscrpcn);
                        $formularioSubFamilia.find("[name='IDFMLAPDRE']").val(entidadSubFamilia.idfmlapdre).change();
                        
                       
                        $formularioSubFamilia.AgregarCamposAuditoria(entidadSubFamilia);
                    })
                    .fail(function (e) {
                    })
                    .always(function () {
                        $formularioSubFamilia.find(":input").attr("disabled", false);
                        $formularioSubFamilia.DeshabilitarCamposAuditoria();
                    });
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaSubFamilia.objeto = $tablaSubFamilia.DataTable(tablaSubFamilia.opciones);
            tablaSubFamilia.eventos();
        }
    };
    var modalSubFamilia = {
        form: {
            objeto: $formularioSubFamilia.validate({
                //VALIDACIONES DEL FORMULARIO
                rules: {
                    DSCRPCN: {
                        required: true,
                        maxlength: 150,
                    },
                },
                submitHandler: function (formElement, e) {
                    e.preventDefault();
                    var $btn = $(formElement).find("button[type='submit']");
                    $btn.attr("disabled", true);
                    var formData = new FormData(formElement);
                    $formularioSubFamilia.find(":input").attr("disabled", true);

                    if (!$formularioSubFamilia.find("[name='ID']").val()) {
                        url = `/familia/insertar`
                    } else {
                        url = `/familia/actualizar`
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                tablaSubFamilia.reload();
                                modalSubFamilia.eventos.reset();
                                $modalSubFamilia.modal("hide");
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                            $formularioSubFamilia.find(":input").attr("disabled", false);
                        });
                }
            }),
            eventos: {
                reset: function () {
                    modalSubFamilia.form.objeto.resetForm();
                }
            }
        },
        eventos: {
            onHide: function () {
                $modalSubFamilia.on('hidden.bs.modal', function () {
                    modalSubFamilia.eventos.reset();
                    $formularioSubFamilia.find("[name='ID']").val("");
                })
            },
            onShow: function () {
                $modalSubFamilia.on('shown.bs.modal', function () {
                    if (!$formularioSubFamilia.find("[name='ID']").val()) {
                        $formularioSubFamilia.AgregarCamposDefectoAuditoria();
                        $formularioSubFamilia.DeshabilitarCamposAuditoria();
                    }
                })
            },
            reset: function () {
                modalSubFamilia.form.eventos.reset();
                $formularioSubFamilia.trigger("reset");
                $formularioSubFamilia.find(":input").attr("disabled", false);
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };
    var selects = {
        familias: function () {
            $.get("/familia/obtener-todas")
                .done(function (data) {
                    $selectFamilias.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            selects.familias();
        }
    };


    var eventosIncrustados = {
        filtroFamilias: function () {
            $filtroFamilias.on("change", function () {
                tablaSubFamilia.reload();
            });
        },
        init: function () {
            this.filtroFamilias();
        }
    }
    return {
        init: function () {
            selects.init();
            tablaSubFamilia.inicializador();
            modalSubFamilia.init();

            //validacionControles.init();
            eventosIncrustados.init();
            validacionControles.init();
        }
    };
}();

$(() => {
    InicializarSubFamilia.init();
})

