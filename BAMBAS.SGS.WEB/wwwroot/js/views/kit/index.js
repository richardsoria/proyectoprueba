﻿var InicializarKit = function () {
    var $selectEstados = $(".select-estados");
    var $selectFamilias = $(".select-familias");
    var $selectMedida = $(".select-unidadmedida");
    var $btnkitnew = ("#btnkitnew");

    //variables JQuery    //variables JQuery
    var $IdKit = $("#IdKit");
    var $tablaKit = $("#tabla_kit");
    var $tablaArticulo = $("#tabla_articulo");
    var $formulariokitNew = $("#kit_new_form");
    var $modalKitArticulo = $("#modal_kitArticulo");
    var $DivBusqueda = $("#DivBusqueda");
    //
    var $accesoAgregarKit = $("#accesoAgregarKit");
    var $accesoEditarKit = $("#accesoEditarKit");
    var $accesoEliminarKit = $("#accesoEliminarKit");

    var $txtBusqueda = $("#txtBusqueda");
    var $txtCantidad = $("#txtCantidad");
    var $dataecontrada = $("#dataecontrada");
    var $btnAgregarArticulos = $('#btnAgregarArticulos');
    var $btnKitBuscar = $("#btnKitBuscar");
    var $btnKitAgregar = $("#btnKitAgregar");
    var $btnKitLimpiar = $("#btnKitLimpiar");
    var $btnKitGuardar = $("#btnKitGuardar");

    var entidadKit = {
        id: "",
        dscrpcn: "",
        csap: "",
        idfmla: "",
        gdunddmdda: "",
        kits: "",
        gdtmnda: "",

        cntdd: "",
        idartclo: "",
        csto: "",

        gdestdo: "",
        festdo: "",
        ucrcn: "",
        fcrcn: "",
        uedcn: "",
        fedcn: "",
    }

    var validacionControles = {
        init: function () {
            if ($accesoAgregarKit.val() == "False") {
                $btnkitnew.remove();
            }
        }
    };

    var tablaArticulo = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/kit/get-articulos",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Unid. Med.",
                    className: "text-center",
                    data: "gdunddmdda",
                    orderable: false
                },
                {
                    title: "Moneda",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false
                },
                {
                    title: "Costo",
                    className: "text-right",
                    data: "csto",
                    orderable: false,
                    render: function (data) {
                        return data.toFixed(2)
                    }
                },
                {
                    title: "Cantidad",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        var cantarticulo = tablaArticulo.memory_art.find(x => x.CSAP == data.csap);
                        if (cantarticulo) {
                            tpm += `   <input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-csto="${data.csto}" data-idartclo="${data.id}" type="text" class="form-control text-uppercase form-control-sm cntdd" maxlength="8" value="${cantarticulo.CANT}">`;
                        } else {
                            tpm += `   <input data-dscrpcn="${data.dscrpcn}" data-csap="${data.csap}" data-gdunddmdda="${data.gdunddmdda}" data-csto="${data.csto}" data-idartclo="${data.id}" type="text" class="form-control text-uppercase form-control-sm cntdd" maxlength="8">`;
                        }
                        return tpm;
                    }
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        memory_art: [],
        eventos: function () {
            tablaArticulo.objeto.on("keypress", ".cntdd", function () {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
                return true;
            });
            tablaArticulo.objeto.on("keyup", ".cntdd", function () {

                var csap = $(this).data("csap");
                var descr = $(this).data("dscrpcn");
                var und = $(this).data("gdunddmdda");
                var cant = $(this).val();
                var idart = $(this).data("idartclo");
                var csto = $(this).data("csto");

                var delete_articulo = false;
                if (cant == 0 || cant == '') {
                    delete_articulo = true;
                }
                var new_articulo = true;
                for (let i = 0; i < tablaArticulo.memory_art.length; i++) {
                    if (tablaArticulo.memory_art[i].CSAP === csap) {
                        new_articulo = false;
                        if (delete_articulo) {
                            tablaArticulo.memory_art.splice(i, 1);
                        } else {
                            tablaArticulo.memory_art[i].CANT = cant;
                        }
                    }
                }
                if (new_articulo) {
                    tablaArticulo.memory_art.push({
                        CSAP: csap,
                        DESCR: descr,
                        UND: und,
                        CANT: cant,
                        IDART: idart,
                        CSTO: csto,
                    });
                }
                //console.log(tablaArticulo.memory_art);
            });
        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaArticulo.objeto = $tablaArticulo.DataTable(tablaArticulo.opciones);
            tablaArticulo.eventos();
        }
    };

    var tablaKit = {
        objeto: null,
        opciones: {
            ajax: {
                dataType: "JSON",
                url: "/kit/get",
                type: "GET",
                data: function (data) {
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
                {
                    title: "Código SAP",
                    className: "text-center",
                    data: "csap",
                    orderable: false
                },
                {
                    title: "Descripción",
                    className: "text-left",
                    data: "dscrpcn",
                    orderable: false
                },
                {
                    title: "Moneda",
                    className: "text-center",
                    data: "gdtmnda",
                    orderable: false,
                },
                {
                    title: "Costo",
                    className: "text-right",
                    data: null,
                    orderable: false,
                    render: function (data) {
                        var tpm = "";
                        if (data.csto == 0) {
                            tpm += `<span style="color:red" >${data.csto.toFixed(2)}</span>`;
                        } else {
                            tpm += `<span>${data.csto.toFixed(2)}</span>`;
                        }
                        return tpm
                    }
                },
                {
                    title: "U. Edición",
                    className: "text-center",
                    data: "uedcn",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "F. Edición",
                    className: "text-center",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false
                },
                {
                    title: "Estado",
                    data: null,
                    width: '2%',
                    orderable: false,
                    class: "text-center",
                    render: function (data) {
                        return ObtenerEstado(data);
                    }
                },
                {
                    title: "Opciones",
                    className: "text-center",
                    data: null,
                    orderable: false,
                    width: '15%',
                    render: function (data) {
                        var tpm = "";

                        if ($accesoEditarKit.val() == "True") {
                            tpm += `<button title="Editar" data-id="${data.id}" class="btn btn-info btn-xs btn-editar"><span><i class="la la-edit"></i></button>`;
                        }

                        if ($accesoEliminarKit.val() == "True") {
                            tpm += ` <button title="Cambiar estado" data-id="${data.id}" class="btn btn-danger btn-xs btn-delete"><span><i class="la la-refresh"></i><span></span></span></button>`;
                        }

                        return tpm;
                    }
                }
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: function () {
            tablaKit.objeto.on("click", ".btn-delete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: "¿Quiere modificar el estado del registro?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonText: "Cancelar"
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "/articulo/eliminar",
                            type: "POST",
                            data: {
                                id: id
                            }
                        }).done(function () {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registro modificado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                tablaKit.reload();
                            });
                        }).fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al modificar el registro.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        });
                    }
                });
            });
            tablaKit.objeto.on("click", ".btn-editar", function () {
                var id = $(this).data("id");
                RedirectWithSubfolder(`/kit/editar/${id}`);
            });

        },
        reload: function () {
            this.objeto.ajax.reload();
        },
        inicializador: function () {
            tablaKit.objeto = $tablaKit.DataTable(tablaKit.opciones);
            tablaKit.eventos();
        }
    };

    var modalKitArticulo = {
        eventos: {
            onHide: function () {
                $modalKitArticulo.on('hidden.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                    modalKitArticulo.eventos.reset();
                })
            },
            onShow: function () {
                $modalKitArticulo.on('shown.bs.modal', function () {
                    tablaArticulo.memory_art = [];
                })
            },
            reset: function () {
                tablaArticulo.memory_art = [];
            }
        },
        init: function () {
            this.eventos.onHide();
            this.eventos.onShow();
        }
    };

    var selects = {
        familias: function () {
            $.get("/familia/obtener-activas")
                .done(function (data) {
                    $selectFamilias.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectFamilias.append($("<option />").val(item["id"]).text(item["dscrpcn"]));
                    });
                });
        },
        init: function () {
            $selectEstados.LlenarSelectEstados();
            $selectMedida.LlenarSelectGD("GDUNDDMDDA");
            selects.familias();
        }
    };

    var autocomplete = {
        init: function () {
            $txtBusqueda.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/articulo/search",
                        dataType: "json",
                        data: {
                            term: request.term,
                            flagKit: false,
                        },
                        success: function (data) {
                            /*           console.log(data);*/
                            if (data.length == 1) {
                                $txtBusqueda.val(data[0].label);
                                $dataecontrada.val(data[0].value2);
                                $txtBusqueda.attr("disabled", true);
                                $txtCantidad.focus();
                            } else {
                                response(data);
                            }
                        },
                        error: function (xhr, status, error) {
                            alert(xhr + " " + status + " " + error);
                        },
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    $txtBusqueda.val(ui.item.label);
                    $dataecontrada.val(ui.item.value2);
                    $txtBusqueda.attr("disabled", true);
                    $txtCantidad.focus();

                    return false;
                }
            });
        }
    }

    var eventosIncrustados = {
        CalcularIndex: function () {
            var hayfilas = $("#tabla_articuloadd tbody tr").length > 0;
            if (!hayfilas) {
                $('#tabla_articuloadd tbody').html('<tr class="noData"><td></td><td colspan="6" class="text-center" >No hay datos disponibles</td></tr>');
            }
            $("#tabla_articuloadd tbody tr").each(function (index) {
                var trs = index + 1;
                $(this).children('td').eq(0).text(trs);
            })
        },
        TotalCosto: function () {
            var costototal = 0
            $("#tabla_articuloadd tbody tr").each(function (index) {
                cantidad = $(this).find(".cntdd").val();
                costo = $(this).find(".csto").val();
                costototal = costototal + (cantidad * costo);
            })
            $formulariokitNew.find("[name='CSTO']").val(costototal.toFixed(2));
        },
        editarCantidadFila: function () {
            $(document).on('click', '.btn-editar-tabla', function (event) {
                //
                var id = $(this).data("id");
                var btn = $(this);
                var hasClass = $(this).hasClass("btn-info");
                var input = ".cntdd-" + id;
                if (hasClass) {
                    $("#tabla_articuloadd").find(input).attr("disabled", false);
                    btn.removeClass("btn-info");
                    btn.find("i").removeClass("la-edit");
                    btn.addClass("btn-warning");
                    btn.find("i").addClass("la-reply");
                } else {
                    var anterior = $("#tabla_articuloadd").find(input).data("anterior");
                    $("#tabla_articuloadd").find(input).val(anterior);
                    $("#tabla_articuloadd").find(input).attr("disabled", true);
                    btn.removeClass("btn-warning");
                    btn.find("i").removeClass("la-reply");
                    btn.addClass("btn-info");
                    btn.find("i").addClass("la-edit");
                }
                //
            });
        },
        EliminarFila: function () {
            $(document).on('click', '.BorrarFilaSubMenu', function (event) {
                event.preventDefault();
                var fila = $(this).closest('tr');
                var idkitdetalle = $(this).attr("id");
                let arrayid = idkitdetalle.split('_');
                var id = arrayid[1];
                if ($IdKit.val()) {
                    if (id > 0) {
                        Swal.fire({
                            title: "¿Quiere modificar el estado del registro?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Aceptar",
                            confirmButtonClass: "btn btn-danger",
                            cancelButtonText: "Cancelar"
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: "/kit/eliminar",
                                    type: "POST",
                                    data: {
                                        id: id
                                    }
                                }).done(function () {
                                    Swal.fire({
                                        icon: "success",
                                        allowOutsideClick: false,
                                        title: "Éxito",
                                        text: "Registro modificado satisfactoriamente.",
                                        confirmButtonText: "Aceptar"
                                    }).then((result) => {
                                        fila.remove();
                                        eventosIncrustados.TotalCosto();
                                    });
                                }).fail(function (e) {
                                    Swal.fire({
                                        icon: "error",
                                        title: "Error al modificar el registro.",
                                        text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                        confirmButtonText: "Aceptar"
                                    });
                                });
                            }
                        });
                    } else {
                        fila.remove();
                    }
                } else {
                    fila.remove();
                }
                eventosIncrustados.TotalCosto();
                eventosIncrustados.CalcularIndex();
            });
        },
        botonAgregarArticulo: function () {
            $btnAgregarArticulos.on("click", function () {
                for (var i = 0; i < tablaArticulo.memory_art.length; i++) {

                    var art_in_table = false;

                    $("#tabla_articuloadd tbody tr").each(function (index) {


                        var csaptabla = $(this).children('td').find("[class='csap']").text();

                        if (tablaArticulo.memory_art[i].CSAP == csaptabla.trim()) {
                            art_in_table = true;
                            $(this).children('td').find("[class='cntdd']").val(tablaArticulo.memory_art[i].CANT);
                        }
                    })
                    if (!art_in_table) {
                        var trs = i + 1
                        var htmlTags = eventosIncrustados.formatoFilaTabla(trs, 0, tablaArticulo.memory_art[i].IDART, tablaArticulo.memory_art[i].CSAP, tablaArticulo.memory_art[i].DESCR, tablaArticulo.memory_art[i].UND, tablaArticulo.memory_art[i].CANT, tablaArticulo.memory_art[i].CSTO, false, false);

                        $('#tabla_articuloadd tbody').append(htmlTags);
                    }
                }
                tablaArticulo.memory_art = [];
                $modalKitArticulo.modal('hide');
                eventosIncrustados.TotalCosto();
                eventosIncrustados.CalcularIndex();
            })

        },
        botonBuscar: function () {
            $btnKitBuscar.on("click", function () {
                tablaArticulo.memory_art = [];
                tablaArticulo.reload();
            })
        },
        botonGrabarKit: function () {
            $btnKitGuardar.on("click", function () {
                var fila = $("#tabla_articuloadd tbody").find('.noData');

                if (fila.length) {
                    Swal.fire({
                        title: "No se puede crear el KIT sin artículos asociados.",
                        icon: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        confirmButtonClass: "btn btn-danger",
                    });
                    return;
                }

                if ($formulariokitNew.valid()) {
                    $btnKitGuardar.attr("disabled", true);

                    $formulariokitNew.find("[name='CNTDD']").add('was-validated');
                    var articulos = [];
                    $("#tabla_articuloadd tbody tr").each(function (index) {
                        var cant = $(this).find(".cntdd").val();
                        var id = $(this).find("[class='idartclo']").val();
                        var csto = $(this).find("[class='csto']").val();
                        articulos.push(
                            {
                                IDARTCLO: id,
                                CNTDD: cant,
                                CSTO: csto,
                            }
                        );
                    })

                    //console.log(JSON.stringify(articulos));

                    var formData = new FormData();
                    formData.append("KITS", JSON.stringify(articulos));
                    formData.append("DSCRPCN", $formulariokitNew.find("[name='DSCRPCN']").val());
                    formData.append("IDFMLA", $formulariokitNew.find("[name='IDFMLA']").val());
                    formData.append("CSAP", $formulariokitNew.find("[name='CSAP']").val());
                    formData.append("GDUNDDMDDA", $formulariokitNew.find("[name='GDUNDDMDDA']").val());
                    formData.append("GDTMNDA", 2);
                    formData.append("CSTO", $formulariokitNew.find("[name='CSTO']").val());
                    formData.append("GDESTDO", $formulariokitNew.find("[name='GDESTDO']").val());
                    //console.log(formData);
                    if ($IdKit.val()) {
                        formData.append("ID", $IdKit.val());
                        url = "/kit/actualizar"
                    } else {
                        url = "/kit/insertar"
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false
                    })
                        .done(function (e) {
                            Swal.fire({
                                title: "Éxito",
                                icon: "success",
                                allowOutsideClick: false,
                                text: "Guardado satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                if ($IdKit.val()) {
                                    $('#tabla_articuloadd tbody').html('<tr><td colspan="6" class="text-center" >Cargando datos...</td></tr>');
                                    obtenerDatosKit.obtenerDetalle();

                                    eventosIncrustados.TotalCosto();
                                    eventosIncrustados.CalcularIndex();
                                } else {
                                    RedirectWithSubfolder(`/kit/editar/${e}`);
                                }
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al guardar los datos.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                        });


                    $btnKitGuardar.attr("disabled", false);
                }
            })
        },
        botonAgregar: function () {
            var contadorCantidad = 0;
            var contadorDescripcion = 0;
            $btnKitAgregar.on("click", function () {
                var cant = $txtCantidad.val();
                var txtbusqueda = $txtBusqueda.val();

                if (cant == "") {
                    if (contadorCantidad == 0) {
                        $formulariokitNew.find("[name='CNTDD']").add('error');
                        $formulariokitNew.find("[name='ValCNTDD']").after('<label id="CNTDD-error" class="error VCNTDDA" for="CNTDD">Ingrese Cantidad </label>');
                        $txtCantidad.addClass('error');
                        contadorCantidad = 1;
                    }
                } else {
                    $formulariokitNew.find("[name='CNTDD']").removeClass('error');
                    $txtCantidad.removeClass('error');
                    $(".VCNTDDA").remove();
                    contadorCantidad = 0;
                }

                if (txtbusqueda == "") {
                    if (contadorDescripcion == 0) {
                        $formulariokitNew.find("[name='txtBusqueda']").add('error');
                        $formulariokitNew.find("[name='ValtxtBusqueda']").after('<label id="txtBusqueda-error" class="error VtxtBusqueda" for="CNTDD">Ingrese Descripción </label>');
                        $txtBusqueda.addClass('error');
                        contadorDescripcion = 1;
                    }
                } else {
                    $formulariokitNew.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    $txtBusqueda.remove('error');
                    contadorDescripcion = 0;
                }

                if (cant != "" && txtbusqueda != "") {
                    $formulariokitNew.find("[name='CNTDD']").removeClass('error');
                    $txtCantidad.removeClass('error');
                    $(".VCNTDDA").remove();
                    $formulariokitNew.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    $txtBusqueda.remove('error');
                    contadorDescripcion = 0;
                    contadorCantidad = 0;

                    //  VIENE CODIGO SAP - DESCRIPCION
                    var dataencontrada = $dataecontrada.val()           //  VIENE EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA

                    Arraycsapdesc = txtbusqueda.split('-'); // SEPARA  CODIGO SAP - DESCRIPCION
                    var csap = Arraycsapdesc[0].toString();
                    var desc = Arraycsapdesc[1].toString();

                    Arrayidgduni = dataencontrada.split('-');  //  SEPARA EL ID DEL ARTICULO - GRUPO DATO UNIDAD DE MEDIDA
                    var id = Arrayidgduni[0].toString();
                    var und = Arrayidgduni[1].toString();
                    var csto = Arrayidgduni[2].toString();
                    var flag = false;

                    $("#tabla_articuloadd tbody tr").each(function (index) {

                        var csaptabla = $(this).children('td').find("[class='csap']").text()

                        if (csap.trim() == csaptabla.trim()) {
                            $(this).children('td').find(".cntdd").val(cant);
                            flag = true
                        }
                    })

                    if (flag) {
                        $txtBusqueda.val("");
                        $txtCantidad.val("");
                        $dataecontrada.val("");
                        $txtBusqueda.attr("disabled", false);
                        $txtBusqueda.focus();
                        eventosIncrustados.TotalCosto();
                        eventosIncrustados.CalcularIndex();
                        return;
                    }

                    //$('#divtabla').css("visibility", "visible ");
                    var trs = $("#tabla_articuloadd tbody tr").length + 1;
                    var htmlTags = eventosIncrustados.formatoFilaTabla(trs, 0, id.trim(), csap.trim(), desc.trim(), und.trim(), cant.trim(), csto.trim(), false, false);

                    $('#tabla_articuloadd tbody').append(htmlTags);
                    $txtBusqueda.val("");
                    $txtCantidad.val("");
                    $dataecontrada.val("");
                    $txtBusqueda.attr("disabled", false);
                    $txtBusqueda.focus();
                }
                eventosIncrustados.TotalCosto();
                eventosIncrustados.CalcularIndex();
            })

        },
        botonLimpiar: function () {
            $btnKitLimpiar.on("click", function () {
                $txtBusqueda.val("");
                $dataecontrada.val("");
                $txtBusqueda.attr("disabled", false);
                $txtBusqueda.focus();
            })
        },
        solonumero: function () {
            $txtCantidad.on("keyup", function () {
                var cant = $txtCantidad.val();
                if (cant != "") {
                    $formulariokitNew.find("[name='CNTDD']").removeClass('error');
                    $(".VCNTDDA").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
            $txtBusqueda.on("keyup", function () {
                var txtBusqueda = $txtBusqueda.val();
                if (txtBusqueda != "") {
                    $formulariokitNew.find("[name='txtBusqueda']").removeClass('error');
                    $(".VtxtBusqueda").remove();
                    event.preventDefault();
                    return false;
                }
                return true;
            })
        },
        formatoFilaTabla: function (trs, id, idarticulo, csap, desc, unidadmedida, cant, costo, obteniendo = false, FlagEstado = false) {
            var $filanodatos = $("#tabla_articuloadd tbody").find('.noData');
            if ($filanodatos) {
                $filanodatos.remove();
            }
            //FUSION DE LAS 3
            var htmlTags = "<tr>" +
                '<td class="text-center"><label>' + trs + '</label></td>' +
                '<td class="text-center"><label class="csap">' + csap + '</label>' +
                '<input class="idkitdetalle" type="hidden" value="' + id + '">' +
                '<input class="idartclo" type="hidden" value="' + idarticulo + '">' +

                '</td > ' +
                '<td class="text-left"><label>' + desc + '</label></td>' +
                '<td class="text-center"><label>' + unidadmedida + '</label></td>' +
                '<td class=""><input class="cntdd cntdd-' + idarticulo + ' text-center form-control form-control-sm" disabled="disabled" data-anterior="' + cant + '" type="number" min="0" value="' + cant + '"/><input class="csto" type="hidden" value="' + costo + '"></td>' +
                '<td class="text-center">';
            if (!FlagEstado) {
                htmlTags += '<button type="button" title="Editar" data-id="' + idarticulo + '" class="btn btn-info btn-xs btn-editar-tabla "><span><i class="la la-edit"></i></button> ';
                //
                if (obteniendo) {
                    htmlTags += ' <button id="delete_' + id + '" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                } else {
                    htmlTags += ' <button id="delete_0" title="Eliminar Submenú" class="btn btn-danger btn-xs BorrarFilaSubMenu"><span class="fa fa-trash"></span></button>';
                }
            } else {
                htmlTags += '';
            }
            htmlTags += '</td>' +
                '</tr>';
            return htmlTags;
        },
        init: function () {
            eventosIncrustados.CalcularIndex();
            eventosIncrustados.TotalCosto();
            eventosIncrustados.EliminarFila();
            eventosIncrustados.editarCantidadFila();
            eventosIncrustados.botonBuscar();
            eventosIncrustados.botonAgregar();
            eventosIncrustados.botonAgregarArticulo();
            eventosIncrustados.botonLimpiar();
            eventosIncrustados.botonGrabarKit();
            eventosIncrustados.solonumero();
        }
    }

    var obtenerDatosKit = {
        obtenerDetalle: function (FlagEstado) {
            $.get("/Kit/listar-detalle", { id: $IdKit.val() })
                .done(function (data) {
                    if (data.length) {
                        $('#tabla_articuloadd tbody').html('');
                        //console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var trs = i + 1;
                            var htmlTags = eventosIncrustados.formatoFilaTabla(trs, data[i].id, data[i].idartclo, data[i].csap, data[i].dscrpcn, data[i].gdunddmdda, data[i].cntdd, data[i].csto, true, FlagEstado);
                            $('#tabla_articuloadd tbody').append(htmlTags);
                        }
                    } else {
                        $('#tabla_articuloadd tbody').html('<tr class="noData"><td></td><td colspan="6" class="text-center" >No hay datos disponibles</td></tr>');
                    }
                });
        },
        init: function () {
            var FlagEstado = false;
            $.get("/Kit/obtener", { id: $IdKit.val() })
                .done(function (data) {
                    $formulariokitNew.find("[name='DSCRPCN']").val(data.dscrpcn);
                    $formulariokitNew.find("[name='GDUNDDMDDA']").val(data.gdunddmdda);
                    $formulariokitNew.find("[name='IDFMLA']").val(data.idfmla);
                    $formulariokitNew.find("[name='CSAP']").val(data.csap);
                    $formulariokitNew.find("[name='CSTO']").val(data.csto.toFixed(2));
                    $formulariokitNew.AgregarCamposAuditoria(data);

                    if (data.gdestdo == 'I') {
                        $DivBusqueda.remove();
                        $btnKitGuardar.remove();
                        FlagEstado = true;
                    }

                    obtenerDatosKit.obtenerDetalle(FlagEstado);
                });
        }
    };

    return {
        init: function () {
            //$('#divtabla').css("visibility", "hidden");
            selects.init();
            tablaKit.inicializador();
            tablaArticulo.inicializador();
            modalKitArticulo.init();
            autocomplete.init();
            validacionControles.init();
            eventosIncrustados.init();
            $formulariokitNew.AgregarCamposDefectoAuditoria();
            $formulariokitNew.DeshabilitarCamposAuditoria();

            if ($IdKit.val()) {
                obtenerDatosKit.init();
            } else {
                $('#tabla_articuloadd tbody').html('');
                $('#tabla_articuloadd tbody').html('<tr class="noData"><td></td><td colspan="6" class="text-center" >No hay datos disponibles</td></tr>');
            }
        }
    }; 
}();

$(() => {
    InicializarKit.init();
})