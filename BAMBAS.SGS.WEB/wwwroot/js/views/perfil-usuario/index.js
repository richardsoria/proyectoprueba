﻿var iniPerfil = function () {
    var $selectPerfil = $(".select-perfiles");
    var $selectPerfilUsuario = $(".select-perfil-usuario");
    var $selectPerfilObjeto = $(".select-perfil-objeto");
    var $tblPerfilUsuarios = $("#tablaPerfilUsuarios");
    var $btnGuardarPerfilUsuario = $("#guardar-perfilUsuario");
    var $btnGuardarPerfilObjeto = $("#guardar-perfilObjeto");
    var $arbolPerfilObjeto = $('#arbolPerfilObjeto');

    var $thPerfilUsuario = $('#thPerfilUsuario');
    var $accesoGuardarPerfilUsuario = $("#accesoGuardarPerfilUsuario");
    var $accesoGuardarPerfilObjeto = $("#accesoGuardarPerfilObjeto");
    var $esAdmin = $("#esAdmin");

    var validacionControles = {
        init: function () {
            if ($accesoGuardarPerfilUsuario.val() == "False") {
                $btnGuardarPerfilUsuario.remove();
            }
            if ($accesoGuardarPerfilObjeto.val() == "False") {
                $btnGuardarPerfilObjeto.remove();
            }
        }
    };

    var selectPerfil = {
        evento: function () {
            $selectPerfilUsuario.on("change", function () {
                $thPerfilUsuario.show();


                //recargar tabla
                configDTPerfil.reload();

                //limpiar arrays 
                configDTPerfil.asignados = [];
                configDTPerfil.noAsignados = [];
            });
            $selectPerfilObjeto.on("change", function () {
                arbolPerfilObjeto.obtener();
            });
        },
        init: function () {
            $.get(`/perfil/ListarPerfiles`)
                .done(function (data) {
                    $selectPerfil.append($("<option />").val('').text("Seleccione"));
                    $.each(data, function (key, item) {
                        $selectPerfil.append($("<option />").val(item["id"]).text(item["dprfl"]));
                    });
                });
            selectPerfil.evento();
            arbolPerfilObjeto.eventos.init();
        }
    }

    var configDTPerfil = {
        asignados: [],
        noAsignados: [],
        objecto: null,
        opciones: {
            ajax: {
                gatatype: "JSON",
                url: "/perfil-usuario/obtener-usuarios",
                type: "GET",
                data: function (data) {
                    delete data.columns;
                    data.idPerfil = $selectPerfilUsuario.val();
                }
            },
            columns: [
                {
                    title: "",
                    className: "text-center",
                    data: null,
                    width: '2%',
                    orderable: false,
                    render: function (data) {
                        return "";
                    }
                },
        
                {
                    //title: "Asignado",
                    data: null,
                    orderable: false,
                    width: '11%',
                    className: "text-center",
                    render: function (data) {

                        var checkeado = false;

                        //verificar que no exista ni en una ni en la otra para agregarlo
                        if ($.inArray(data.id, configDTPerfil.asignados) == -1 && $.inArray(data.id, configDTPerfil.noAsignados) == -1) {
                            if (data.checkeado) {
                                checkeado = true;
                                configDTPerfil.asignados.push(data.id);
                            } else {
                                checkeado = false;
                                configDTPerfil.noAsignados.push(data.id);
                            }
                        } else {//si existe, buscarlo

                            if ($.inArray(data.id, configDTPerfil.asignados) == -1) {
                                checkeado = false;
                            }
                            else {
                                checkeado = true;
                            }
                        }
                        // ${$accesoGuardarPerfilUsuario.val() == "False" ? "disabled" : ""}
                        var tpm = `<label class="switch">
                                  <input type="checkbox" ${checkeado ? "checked" : ""} class="check-asignado" data-id="${data.id}">
                                  <span class="slider"></span>
                                </label>`;
                        return tpm;
                    }
                },
                //{
                //    //title: "ID",
                //    data: "id", width: '5%',
                //    orderable: true
                //},
                {
                    //title: "Nombres",
                    data: "nyapllds",
                    width: '17%',
                    orderable: false,
                    className: "text-left"
                },
                {
                    //title: "Cod. Usuario",
                    data: "cusro",
                    width: '8%',
                    orderable: false,
                    className: "text-center"
                },
                {
                    //title: "Fec. Edición",
                    data: "cFEDCN",
                    width: '10%',
                    orderable: false,
                    className: "text-center"
                },
            ],
            columnDefs: [
                {
                    targets: "_all",
                    className: 'text-center'
                }
            ]
        },
        eventos: {
            asignar: function () {
                configDTPerfil.objecto.on("change", ".check-asignado", function () {
                    var idusuario = $(this).data("id");
                    var checkeando = $(this).is(':checked');

                    if ($.inArray(idusuario, configDTPerfil.asignados) == -1 && $.inArray(idusuario, configDTPerfil.noAsignados) == -1) {
                        if (checkeando) {
                            configDTPerfil.asignados.push(idusuario);
                        } else {
                            configDTPerfil.noAsignados.push(idusuario);


                        }
                    } else {//si existe, buscarlo
                        if ($.inArray(idusuario, configDTPerfil.asignados) == -1 && checkeando) {//si no esta asignado y esta chequeado
                            //buscarlo en no asignados y eliminarlo
                            configDTPerfil.noAsignados.splice($.inArray(idusuario, configDTPerfil.noAsignados), 1);
                            //agregarlo al asignados
                            configDTPerfil.asignados.push(idusuario);
                        }
                        else {
                            //quita el check 
                            if (!checkeando) {

                                //buscarlo en asignados y eliminarlo
                                configDTPerfil.asignados.splice($.inArray(idusuario, configDTPerfil.asignados), 1);
                                //agregarlo al NO asignados
                                configDTPerfil.noAsignados.push(idusuario);
                            }
                        }
                    }
                });
            },
            init: function () {
                this.asignar();
            }
        },
        reload: function () {
            if (configDTPerfil.objecto != null && configDTPerfil.objecto != undefined) {
                configDTPerfil.objecto.ajax.reload();
            } else {
                configDTPerfil.init();
            }
        },
        init: function () {
            configDTPerfil.objecto = $tblPerfilUsuarios.DataTable(configDTPerfil.opciones);
            this.eventos.init();
        }
    };
    var eventoGuardarPerfilUsuario = {
        init: function () {
            $btnGuardarPerfilUsuario.on("click", function () {
                $btn = $(this);
                if ($selectPerfilUsuario.val() != null && $selectPerfilUsuario.val() != 0 && $selectPerfilUsuario.val() != "") {


                    $btn.addLoader();
                    $.ajax({
                        type: "POST",
                        url: '/perfil-usuario/guardar',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            asignados: configDTPerfil.asignados,
                            noAsignados: configDTPerfil.noAsignados,
                            idPerfil: $selectPerfilUsuario.val(),
                            
                        })
                    })
                        .done(function (e) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registros actualizados satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                configDTPerfil.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al actualizar registros.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                        });
                } else {
                    Swal.fire({
                        icon: "info",
                        title: "Información.",
                        text: "Por favor seleccione un perfil",
                        confirmButtonText: "Aceptar"
                    });
                }
            });
        }
    }
    //PERFIL OBJETO
    var arbolPerfilObjeto = {
        modulosAsignados: [],
        modulosNoAsignados: [],
        objPadreAsignados: [],
        objPadreNoAsignados: [],
        objHijoAsignados: [],
        objHijoNoAsignados: [],
        detallesAsignados: [],
        detallesNoAsignados: [],
        obtener: function () {
            $arbolPerfilObjeto.agregarLoading();
            $.ajax({
                type: "GET",
                url: '/perfil-objeto/obtener-objetos',
                data: {
                    idPerfil: $selectPerfilObjeto.val()
                }
            })
                .done(function (data) {
                    $arbolPerfilObjeto.html(data.partial);
                    arbolPerfilObjeto.llenarArreglos();
                    if (data.esAdmin) {
                        //$btnGuardarPerfilObjeto.hide();
                        //$("[type=checkbox]").prop("disabled", true);
                    } else {
                        if ($accesoGuardarPerfilObjeto.val() == "True") {
                            $btnGuardarPerfilObjeto.show();
                        }
                    }
                });
        },
        llenarArreglos: function () {
            arbolPerfilObjeto.modulosAsignados = [];
            arbolPerfilObjeto.modulosNoAsignados = [];
            arbolPerfilObjeto.objPadreAsignados = [];
            arbolPerfilObjeto.objPadreNoAsignados = [];
            arbolPerfilObjeto.objHijoAsignados = [];
            arbolPerfilObjeto.objHijoNoAsignados = [];
            arbolPerfilObjeto.detallesAsignados = [];
            arbolPerfilObjeto.detallesNoAsignados = [];
            var datosArb = $.parseJSON($arbolPerfilObjeto.find(".datosArbol").val());
            //MODULOS
            //var modulosArb = datosArb.filter(items => items.tipo == 1);
            //$.each(modulosArb, function (index, item) {
            //    arbolPerfilObjeto.arregloModulos(item.Id, item.Checkeado);
            //});

            $.each(datosArb, function (index, item) {
                arbolPerfilObjeto.arregloModulos(item.Id, item.Checkeado);

                //OBJETOS
                $.each(item.Nodos, function (index, objetos) {
                    arbolPerfilObjeto.arregloPadres(objetos.Id, objetos.Checkeado);

                    var sonhijos = objetos.Nodos.filter(filtrados => filtrados.Tipo == 3);
                    if (sonhijos.length > 0) {
                        //HIJOS
                        $.each(objetos.Nodos, function (index, hijos) {
                            arbolPerfilObjeto.arregloHijos(hijos.Id, hijos.Checkeado);

                            //DETALLES
                            $.each(hijos.Nodos, function (index, detalles) {
                                arbolPerfilObjeto.arregloDetalles(detalles.Id, detalles.Checkeado);
                            });
                        });
                    } else {
                        //DETALLES
                        $.each(objetos.Nodos, function (index, detalles) {
                            arbolPerfilObjeto.arregloDetalles(detalles.Id, detalles.Checkeado);
                        });
                    }
                });
            });
        },
        arregloModulos: function (id, isChecked) {
            //no existe en ninguno de los 2 arreglos
            if ($.inArray(id, arbolPerfilObjeto.modulosAsignados) == -1 && $.inArray(id, arbolPerfilObjeto.modulosNoAsignados) == -1) {
                if (isChecked) {
                    arbolPerfilObjeto.modulosAsignados.push(id);
                } else {
                    arbolPerfilObjeto.modulosNoAsignados.push(id);
                }
            } else {
                if ($.inArray(id, arbolPerfilObjeto.modulosAsignados) == -1 && isChecked) {//si no esta asignado y esta chequeado
                    //buscarlo en no asignados y eliminarlo
                    arbolPerfilObjeto.modulosNoAsignados.splice($.inArray(id, arbolPerfilObjeto.modulosNoAsignados), 1);
                    //agregarlo al asignados
                    arbolPerfilObjeto.modulosAsignados.push(id);
                }
                else {
                    //buscarlo en asignados y eliminarlo
                    arbolPerfilObjeto.modulosAsignados.splice($.inArray(id, arbolPerfilObjeto.modulosAsignados), 1);
                    //agregarlo al NO asignados
                    arbolPerfilObjeto.modulosNoAsignados.push(id);
                }
            }
        },
        arregloPadres: function (id, isChecked) {
            if ($.inArray(id, arbolPerfilObjeto.objPadreAsignados) == -1 && $.inArray(id, arbolPerfilObjeto.objPadreNoAsignados) == -1) {
                if (isChecked) {
                    arbolPerfilObjeto.objPadreAsignados.push(id);
                } else {
                    arbolPerfilObjeto.objPadreNoAsignados.push(id);
                }
            } else {
                if ($.inArray(id, arbolPerfilObjeto.objPadreAsignados) == -1 && isChecked) {//si no esta asignado y esta chequeado
                    //buscarlo en no asignados y eliminarlo
                    arbolPerfilObjeto.objPadreNoAsignados.splice($.inArray(id, arbolPerfilObjeto.objPadreNoAsignados), 1);
                    //agregarlo al asignados
                    arbolPerfilObjeto.objPadreAsignados.push(id);
                }
                else {
                    //buscarlo en asignados y eliminarlo
                    arbolPerfilObjeto.objPadreAsignados.splice($.inArray(id, arbolPerfilObjeto.objPadreAsignados), 1);
                    //agregarlo al NO asignados
                    arbolPerfilObjeto.objPadreNoAsignados.push(id);
                }
            }
        },
        arregloHijos: function (id, isChecked) {
            if ($.inArray(id, arbolPerfilObjeto.objHijoAsignados) == -1 && $.inArray(id, arbolPerfilObjeto.objHijoNoAsignados) == -1) {
                if (isChecked) {
                    arbolPerfilObjeto.objHijoAsignados.push(id);
                } else {
                    arbolPerfilObjeto.objHijoNoAsignados.push(id);
                }
            } else {
                if ($.inArray(id, arbolPerfilObjeto.objHijoAsignados) == -1 && isChecked) {//si no esta asignado y esta chequeado
                    //buscarlo en no asignados y eliminarlo
                    arbolPerfilObjeto.objHijoNoAsignados.splice($.inArray(id, arbolPerfilObjeto.objHijoNoAsignados), 1);
                    //agregarlo al asignados
                    arbolPerfilObjeto.objHijoAsignados.push(id);
                }
                else {
                    //buscarlo en asignados y eliminarlo
                    arbolPerfilObjeto.objHijoAsignados.splice($.inArray(id, arbolPerfilObjeto.objHijoAsignados), 1);
                    //agregarlo al NO asignados
                    arbolPerfilObjeto.objHijoNoAsignados.push(id);
                }
            }
        },
        arregloDetalles: function (id, isChecked) {
            if ($.inArray(id, arbolPerfilObjeto.detallesAsignados) == -1 && $.inArray(id, arbolPerfilObjeto.detallesNoAsignados) == -1) {
                if (isChecked) {
                    arbolPerfilObjeto.detallesAsignados.push(id);
                } else {
                    arbolPerfilObjeto.detallesNoAsignados.push(id);
                }
            } else {
                if ($.inArray(id, arbolPerfilObjeto.detallesAsignados) == -1 && isChecked) {//si no esta asignado y esta chequeado
                    //buscarlo en no asignados y eliminarlo
                    arbolPerfilObjeto.detallesNoAsignados.splice($.inArray(id, arbolPerfilObjeto.detallesNoAsignados), 1);
                    //agregarlo al asignados
                    arbolPerfilObjeto.detallesAsignados.push(id);
                }
                else {
                    //buscarlo en asignados y eliminarlo
                    arbolPerfilObjeto.detallesAsignados.splice($.inArray(id, arbolPerfilObjeto.detallesAsignados), 1);
                    //agregarlo al NO asignados
                    arbolPerfilObjeto.detallesNoAsignados.push(id);
                }
            }
        },
        eventos: {
            checkModulo: function () {
                $arbolPerfilObjeto.on("change", ".check-modulo", function () {

                    var nodo = $(this).data("nodos");
                    var isChecked = $(this).is(":checked");
                    var id = $(this).data("id");
                    $.each($arbolPerfilObjeto.find("#" + nodo).find(".check-objPadre"), function (index, value) {
                        var $ch = $($arbolPerfilObjeto.find("#" + nodo).find(".check-objPadre")[index]);
                        if (isChecked != $ch.is(":checked")) {
                            $ch.prop("checked", isChecked).change();
                        }
                    });
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-objPadre").prop("disabled", !isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-objHijo").prop("disabled", !isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle").prop("disabled", !isChecked);
                    arbolPerfilObjeto.arregloModulos(id, isChecked);
                });
            },
            checkObjPadre: function () {
                $arbolPerfilObjeto.on("change", ".check-objPadre", function () {
                    var nodo = $(this).data("nodos");
                    var isChecked = $(this).is(":checked");
                    var id = $(this).data("id");


                    $.each($arbolPerfilObjeto.find("#" + nodo).find(".check-objHijo"), function (index, value) {
                        var $ch = $($arbolPerfilObjeto.find("#" + nodo).find(".check-objHijo")[index]);
                        if (isChecked != $ch.is(":checked")) {
                            $ch.prop("checked", isChecked).change();
                        }
                    });
                    $.each($arbolPerfilObjeto.find("#" + nodo).find(".check-detalle-hijo"), function (index, value) {
                        var $ch2 = $($arbolPerfilObjeto.find("#" + nodo).find(".check-detalle-hijo")[index]);
                        if (isChecked != $ch2.is(":checked")) {
                            $ch2.prop("checked", isChecked).change();
                        }
                    });
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-objHijo").prop("disabled", !isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle").prop("disabled", !isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle-hijo").prop("disabled", !isChecked);
                    arbolPerfilObjeto.arregloPadres(id, isChecked);
                });
            },
            checkObjHijo: function () {
                $arbolPerfilObjeto.on("change", ".check-objHijo", function () {
                    //if ($accesoGuardarPerfilObjeto.val() == "False") {
                    //    this.checked = !this.checked;
                    //    return;
                    //}
                    var nodo = $(this).data("nodos");
                    var isChecked = $(this).is(":checked");
                    var id = $(this).data("id");
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle").prop("checked", isChecked).change();
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle").prop("disabled", !isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle-hijo").prop("checked", isChecked);
                    $arbolPerfilObjeto.find("#" + nodo).find(".check-detalle-hijo").prop("disabled", !isChecked);
                    arbolPerfilObjeto.arregloHijos(id, isChecked);
                });
            },
            checkDetalle: function () {
                $arbolPerfilObjeto.on("change", ".check-detalle", function () {
                    //if ($accesoGuardarPerfilObjeto.val() == "False") {
                    //    this.checked = !this.checked;
                    //    return;
                    //}
                    var isChecked = $(this).is(":checked");
                    var id = $(this).data("id");

                    arbolPerfilObjeto.arregloDetalles(id, isChecked);
                });
            },
            checkDetalleHijo: function () {
                $arbolPerfilObjeto.on("change", ".check-detalle-hijo", function () {
                    //alert("entro3")
                    var isChecked = $(this).is(":checked");
                    var id = $(this).data("id");
                    var tipo = $(this).data("tipo");
                    if (tipo == 3) {
                        arbolPerfilObjeto.arregloHijos(id, isChecked);
                    } else {
                        arbolPerfilObjeto.arregloDetalles(id, isChecked)
                    }
                    //arbolPerfilObjeto.arregloHijos(id, isChecked);//arregloDetalles
                });
            },
            init: function () {
                this.checkModulo();
                this.checkObjPadre();
                this.checkObjHijo();
                this.checkDetalle();
                this.checkDetalleHijo();
            }
        },
        init: function () {
            arbolPerfilObjeto.obtener();
            //arbolPerfilObjeto.eventos.init();
        }
    };
    var eventoGuardarPerfilObjeto = {
        init: function () {
            $btnGuardarPerfilObjeto.on("click", function () {
                $btn = $(this);
                if ($selectPerfilObjeto.val() != null && $selectPerfilObjeto.val() != 0 && $selectPerfilObjeto.val() != "") {
                    $btn.addLoader();
                    $.ajax({
                        type: "POST",
                        url: '/perfil-objeto/guardar',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            idPerfil: $selectPerfilObjeto.val(),
                            modulosAsignados: arbolPerfilObjeto.modulosAsignados,
                            modulosNoAsignados: arbolPerfilObjeto.modulosNoAsignados,
                            objPadreAsignados: arbolPerfilObjeto.objPadreAsignados,
                            objPadreNoAsignados: arbolPerfilObjeto.objPadreNoAsignados,
                            objHijoAsignados: arbolPerfilObjeto.objHijoAsignados,
                            objHijoNoAsignados: arbolPerfilObjeto.objHijoNoAsignados,
                            detalleAsignados: arbolPerfilObjeto.detallesAsignados,
                            detalleNoAsignados: arbolPerfilObjeto.detallesNoAsignados,
                        })
                    })
                        .done(function (e) {
                            Swal.fire({
                                icon: "success",
                                allowOutsideClick: false,
                                title: "Éxito",
                                text: "Registros actualizados satisfactoriamente.",
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                //configDTPerfil.reload();
                            });
                        })
                        .fail(function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Error al actualizar registros.",
                                text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                                confirmButtonText: "Aceptar"
                            });
                        })
                        .always(function () {
                            $btn.removeLoader();
                        });
                } else {
                    Swal.fire({
                        icon: "info",
                        title: "Información.",
                        text: "Por favor seleccione un perfil",
                        confirmButtonText: "Aceptar"
                    });
                }
            });
        }
    }

    return {
        init: function () {
            selectPerfil.init();
            eventoGuardarPerfilUsuario.init();
            eventoGuardarPerfilObjeto.init();
            validacionControles.init();
            configDTPerfil.reload();
            arbolPerfilObjeto.init();
        }
    };
}();
$(() => {
    iniPerfil.init();
})
