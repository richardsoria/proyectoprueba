﻿var InicializarErrores = function () {
    var $tabla = $("#table_id");

    var datatable = {
        errors: {
            object: null,
            options: {
                ajax: {
                    dataType: "JSON",
                    url: "/error/get",
                    type: "GET",
                    data: function (data) {

                    }
                },
                columns: [
                    {
                        title: "",
                        className: "text-center",
                        data: null,
                        width: '2%',
                        orderable: false,
                        render: function (data) {
                            return "";
                        }
                    },
                    {
                        title: "Fecha",
                        data: "edtme",
                        orderable: false
                    },
                    {
                        title: "Error",
                        data: "emssge",
                        orderable: false
                    },
                    {
                        title: "Esquema",
                        data: "eesqma",
                        orderable: false
                    },
                    {
                        title: "Procedimiento",
                        data: "eprcdre",
                        orderable: false
                    },
                    {
                        title: "Usuario",
                        data: "ucrcn",
                        orderable: false
                    },
                    //{
                    //    title: "Opciones",
                    //    data: null,
                    //    orderable: false,
                    //    render: function (data) {
                    //        var tpm = "";
                    //        tpm += `<a href="/sucursal/editar/${data.id}" class="btn btn-primary btn-sm"><span><i class="la la-edit"></i><span>Editar</span></span></a>`;
                    //        tpm += ` <button data-id="${data.id}" class="btn btn-danger btn-sm btn-delete"><span><i class="la la-trash"></i><span></span></span></button>`;
                    //        return tpm;
                    //    }
                    //}
                ]
            },
            events: function () {
                //$("#table_id").on("click", ".btn-delete", function () {
                //    var id = $(this).data("id");
                //    Swal.fire({
                //        title: "¿Desea eliminar el registro?",
                //        text: "Una vez eliminado no podrá recuperarlo",
                //        icon: "warning",
                //        showCancelButton: true,
                //        confirmButtonText: "Si, eliminar",
                //        confirmButtonClass: "btn btn-danger",
                //        cancelButtonText: "Cancelar"
                //    }).then(function (result) {
                //        if (result.value) {
                //            $.ajax({
                //                url: "/sucursal/eliminar",
                //                type: "POST",
                //                data: {
                //                    id: id
                //                }
                //            }).done(function () {
                //                Swal.fire({
                //                    icon: "success",
                //                    allowOutsideClick: false,
                //                    title: "Éxito",
                //                    text: "Registro eliminado satisfactoriamente.",
                //                    confirmButtonText: "Aceptar"
                //                }).then((result) => {
                //                    datatable.branchOffice.reload();
                //                });
                //            }).fail(function (e) {
                //                Swal.fire({
                //                    icon: "error",
                //                    title: "Error al eliminar el registro.",
                //                    text: e.status === 502 ? "No hay respuesta del servidor" : e.responseText,
                //                    confirmButtonText: "Aceptar"
                //                });
                //            });

                //        }
                //    });
                //});
            },
            reload: function () {
                this.object.ajax.reload();
            },
            init: function () {
                datatable.errors.object = $tabla.DataTable(datatable.errors.options);
                datatable.errors.events();
            }
        },

        init: function () {
            this.errors.init();
        }
    };

    return {
        init: function () {
            datatable.init();
        }
    };
}();

$(() => {
    InicializarErrores.init();
})