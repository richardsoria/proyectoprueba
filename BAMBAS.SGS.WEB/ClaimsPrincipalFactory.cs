﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BAMBAS.SGS.WEB
{
    public class ClaimsPrincipalFactory : UserClaimsPrincipalFactory<IdentityUser, IdentityRole>
    {
        //private readonly CepheidContext _context;
        public ClaimsPrincipalFactory(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            //CepheidContext context,
            IOptions<IdentityOptions> optionsAccessor
            )
            : base(userManager, roleManager, optionsAccessor)
        {
            //_context = context;
        }

        public override async Task<ClaimsPrincipal> CreateAsync(IdentityUser user)
        {
            var principal = await base.CreateAsync(user);
            var identity = (ClaimsIdentity)principal.Identity;

            identity.AddClaims(new[] {
                new Claim(ClaimTypes.Email, $"{user.Email}"),
                //new Claim(ClaimTypes.UserData, $"{user.Name} {user.Surnames}"),
                //new Claim("MainName", $"{user.Name}"),
                //new Claim("PictureUrl", user.Picture ?? ""),
            });

            //if (principal.IsInRole(ConstantHelpers.ROLES.LAWYER))
            //{
            //    var lawyer = await _context.Lawyers.Where(x => x.User.UserName == user.UserName).FirstOrDefaultAsync();
            //    if (lawyer != null)
            //    {
            //        identity.AddClaim(new Claim("LawyerValidated", $"{lawyer.Status == ConstantHelpers.ENTITIES.LAWYER.STATUS.VALIDATED}"));
            //    }
            //}

            return principal;
        }
    }
}
