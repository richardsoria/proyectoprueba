﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.CONFIG
{
    public static class ConfiguracionProyecto
    {
        public const string PROYECTO = PROYECTOS.BAMBAS;
        public const string TEMA = TEMAS.BAMBAS;
        public const string ENTORNO_DESPLIEGUE = ENTORNOS.Desarrollo;
        public const bool CONSULTA_SUSALUD = true;
        public static class TEMAS
        {
            public const string Default = "default"; 
            public const string BAMBAS = "bambas";
        }
        public static class PROYECTOS
        {
            public const string Cepheid = "CEPHEID";
            public const string BAMBAS = "BAMBAS";
        }
        public static class ENTORNOS
        {
            public const string Desarrollo = "Development";
            public const string Pruebas = "Staging";
            public const string Produccion = "Production";
        }

        public static class MODULOS
        {
            public const string Seguridad = "1";
            public const string Persona = "2";
            public const string Tareo = "3";
            public const string Epp = "4";
        }
        public static class CARPETAS_DESPLIEGUE
        {
            public const string Login_Web = "login";
            public const string Seguridad_Web = "sgs-web";
            public const string Persona_Web = "per-web";
            public const string Tareo_Web = "tro-web";
            public const string Epp_Web = "epp-web";
        }
    }
}
