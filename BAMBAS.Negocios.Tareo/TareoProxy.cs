﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Config;
using BAMBAS.Negocios.Modelos.Tareo.Tareo;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Tareo
{
    public interface ITareoProxy
    {
        Task<DataTablesStructs.ReturnedData<TareoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin);

        //Task<DataTablesStructs.ReturnedData<TareoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int id);
        Task<List<TareoDto>> Listar(int id);
        Task<DatosTareo> Obtener(int id);
        Task<TareoDto> ObtenerUltimoRegistro();
        Task<RespuestaConsulta> Insertar(TareoDto Tareo);
        Task<RespuestaConsulta> InsertarDetalleTareo(DatosPersonalTareo Tareo);
        Task<RespuestaConsulta> Actualizar(TareoDto Tareo);
        Task<RespuestaConsulta> Eliminar(TareoDto command);                
    }
    public class TareoProxy : ITareoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;
     
        public TareoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<DataTablesStructs.ReturnedData<TareoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin)
        {
            var url = $"{_apiUrls.TareoUrl}tareo/obtener-tabla?idgrnc={idgrnc}&idsprntndnc={idsprntndnc}&idarea={idarea}&idcntrtsta={idcntrtsta}&idrster={idrster}&numgrdia={numgrdia}&finic={finic}&ffin={ffin}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<TareoDto>(-2);
        }
        //public async Task<DataTablesStructs.ReturnedData<ListarTareo>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int id)
        //{
        //    var url = $"{_apiUrls.TareoUrl}tareo/obtener-tabla?id={id}".AgregarParametrosDatatable(parameters);
        //    var request = await _httpClient.GetAsync(url);
        //    return request.RespuestaTabla<ListarTareo>(-2);
        //}
        public async Task<List<TareoDto>> Listar(int id)
        {
            var url = $"{_apiUrls.TareoUrl}tareo/listar?in={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TareoDto>>(await request.Content.ReadAsStringAsync(), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }
        public async Task<DatosTareo> Obtener (int id)
        {
            var url = $"{_apiUrls.TareoUrl}tareo/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<DatosTareo>(await request.Content.ReadAsStringAsync(), new JsonSerializerOptions { 
                PropertyNameCaseInsensitive = true
            });
        }
        public async Task<TareoDto> ObtenerUltimoRegistro()
        {
            var url = $"{_apiUrls.TareoUrl}tareo/obtenerUltimoRegistro";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<TareoDto>(await request.Content.ReadAsStringAsync(), new JsonSerializerOptions { 
                PropertyNameCaseInsensitive = true
            });
        }
        public async Task<RespuestaConsulta> Insertar(TareoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.TareoUrl}tareo/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> InsertarDetalleTareo(DatosPersonalTareo command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );
            var request = await _httpClient.PostAsync($"{_apiUrls.TareoUrl}tareo/insertardetalle", content);
            return request.Respuesta(-2, nameof(InsertarDetalleTareo));
        }
        public async Task<RespuestaConsulta> Actualizar(TareoDto command)
        {
            var content = new StringContent(JsonSerializer.Serialize(command), Encoding.UTF8, "application/json");
            var request = await _httpClient.PostAsync($"{_apiUrls.TareoUrl}tareo/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(TareoDto command)
        {
            var content = new StringContent(JsonSerializer.Serialize(command), Encoding.UTF8, "application/json");
            var request = await _httpClient.PostAsync($"{_apiUrls.TareoUrl}tareo/eliminar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
    }
}
