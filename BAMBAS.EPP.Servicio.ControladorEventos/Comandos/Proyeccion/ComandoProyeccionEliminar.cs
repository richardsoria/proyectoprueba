﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion
{
    public class ComandoProyeccionEliminar : IRequest<RespuestaConsulta>
    {
        public int ID { get; set; }
        public string UEDCN { get; set; }
    }
}
