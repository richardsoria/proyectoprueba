﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion
{
    public class ComandoProyeccionInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string FAMILIAS { get; set; }
        public string DSCRPCN { get; set; }
        public string ANIO { get; set; }
        public string GDTPRYCCN { get; set; }
        public string GDTMNDA { get; set; }
        public decimal IMPRTE { get; set; }
        public int PRSNLPRYCTDO { get; set; }
        public string APRBDOX { get; set; }
        public string MSINCO { get; set; }
        public string MSFN { get; set; }

    }
}
