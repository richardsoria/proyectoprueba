﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Comandos
{
    public class ComandoAuditoria
    {
        public int? ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; } = "A";
        public DateTime? FESTDO { get; set; }
    }
}
