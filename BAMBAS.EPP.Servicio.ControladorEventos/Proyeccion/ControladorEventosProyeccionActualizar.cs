﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Proyeccion
{
    public class ControladorEventosProyeccionActualizar : IRequestHandler<ComandoProyeccionActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosProyeccionActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoProyeccionActualizar proyeccion, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", proyeccion.ID);
            param.Add("@DSCRPCN", proyeccion.DSCRPCN);
            param.Add("@ANIO", proyeccion.ANIO);
            param.Add("@GDTPRYCCN", proyeccion.GDTPRYCCN);
            param.Add("@GDTMNDA", proyeccion.GDTMNDA);
            param.Add("@IMPRTE", proyeccion.IMPRTE);
            param.Add("@PRSNLPRYCTDO", proyeccion.PRSNLPRYCTDO);
            param.Add("@APRBDOX", proyeccion.APRBDOX);
            param.Add("@MSINCO", proyeccion.MSINCO);
            param.Add("@MSFN", proyeccion.MSFN);
            param.Add("@GDESTDO", proyeccion.GDESTDO);
            param.Add("@UEDCN", proyeccion.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Proyeccion.ActualizarProyeccion, "RETORNO", param);
        }
    }
}
