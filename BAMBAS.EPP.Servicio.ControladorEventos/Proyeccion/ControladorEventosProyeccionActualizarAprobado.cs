﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Proyeccion
{
    public class ControladorEventosProyeccionActualizarAprobado : IRequestHandler<ComandoProyeccionActualizarAprobado, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosProyeccionActualizarAprobado(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoProyeccionActualizarAprobado proyeccion, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", proyeccion.ID);
            param.Add("@APRBDOX", proyeccion.APRBDOX);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Proyeccion.ActualizarAprobadoProyeccion, "RETORNO", param);
        }
    }
}
