﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Proyeccion
{
    public class ControladorEventosProyeccionInsertar : IRequestHandler<ComandoProyeccionInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosProyeccionInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoProyeccionInsertar proyeccion, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@FAMILIAS", proyeccion.FAMILIAS);
            param.Add("@DSCRPCN", proyeccion.DSCRPCN);
            param.Add("@ANIO", proyeccion.ANIO);
            param.Add("@GDTPRYCCN", proyeccion.GDTPRYCCN);
            param.Add("@GDTMNDA", proyeccion.GDTMNDA);
            param.Add("@IMPRTE", proyeccion.IMPRTE);
            param.Add("@PRSNLPRYCTDO", proyeccion.PRSNLPRYCTDO);
            param.Add("@APRBDOX", proyeccion.APRBDOX);
            param.Add("@MSINCO", proyeccion.MSINCO);
            param.Add("@MSFN", proyeccion.MSFN);
            param.Add("@GDESTDO", proyeccion.GDESTDO);
            param.Add("@UCRCN", proyeccion.UCRCN);          
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Proyeccion.InsertarProyeccion, "ID", param);
        }
    }
}
