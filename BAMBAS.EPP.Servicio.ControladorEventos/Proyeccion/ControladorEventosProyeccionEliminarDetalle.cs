﻿using Dapper;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.ControladorEventos.Comandos.Proyeccion;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Servicio.ControladorEventos.Proyeccion
{
    public class ControladorEventosProyeccionEliminarDetalle : IRequestHandler<ComandoProyeccionEliminarDetalle, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosProyeccionEliminarDetalle(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoProyeccionEliminarDetalle entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Proyeccion.EliminarProyeccionDetalle, "RETORNO", parametros);
        }
    }
}
