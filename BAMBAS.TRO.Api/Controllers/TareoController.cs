﻿using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.TRO.Servicio.Consultas;
using BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Api.Controllers
{
    [ApiController]
    [Route("tareo")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TareoController : ControllerBase
    {
        private readonly IDataTableService _dataTableService;
        private readonly IConsultasTareo _consultasTareo;
        private readonly IMediator _mediator;

        public TareoController(IDataTableService dataTableService, IConsultasTareo consultasTareo, IMediator mediator)
        {
            _dataTableService = dataTableService;
            _mediator = mediator;
            _consultasTareo = consultasTareo;
        }
        [HttpGet("obtener-tabla")]
        public async Task<IActionResult> ObtenerDataTable(int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin)
        {
            var parameters = _dataTableService.GetSentParameters();
            var resp = await _consultasTareo.ObtenerDataTable(parameters, idgrnc, idsprntndnc, idarea, idcntrtsta, idrster, numgrdia, finic, ffin);
            return Ok(resp);
        }
        [HttpGet("obtener")]
        public async Task<IActionResult> Obtener(int id)
        {
            var parameters = await _consultasTareo.ObtenerDatosTareo(id);
            return Ok(parameters);
        }
        [HttpGet("obtenerUltimoRegistro")]
        public async Task<IActionResult> ObtenerUltimoRegistro()
        {
            var parameters = await _consultasTareo.ObtenerUltimoRegistro();
            return Ok(parameters);
        }
        [HttpGet("listar")]
        public async Task<IActionResult> Listar(int id)
        {
            var parameters = await _consultasTareo.ListarTareo(id);
            return Ok(parameters);
        }
        [HttpPost("insertar")]
        public async Task<IActionResult> Insertar(ComandoTareoInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("insertardetalle")]
        public async Task<IActionResult> InsertarDetalleTareo(ComandoTareoDetalleInsertar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
        [HttpPost("actualizar")]
        public async Task<IActionResult> Actualizar(ComandoTareoActualizar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result); 
        }
        [HttpPost("eliminar")]
        public async Task<IActionResult> Eliminar(ComandoTareoEliminar entidad)
        {
            var result = await _mediator.Send(entidad);
            return Ok(result);
        }
    }
}
