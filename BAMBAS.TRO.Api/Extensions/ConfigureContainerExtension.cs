﻿using BAMBAS.CORE.Services.Implementations;
using BAMBAS.CORE.Services.Interfaces;
using BAMBAS.PER.Servicio.Proxies.Seguridad;
using BAMBAS.TRO.Servicio.Consultas;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Api.Extensions
{
    public static class ConfigureContainerExtension
    {
        public static void AddTransientServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDataTableService, DataTableService>();
            serviceCollection.AddTransient<IConsultasTareo, ConsultasTareo>();
            serviceCollection.AddTransient<ISeguridadProxy, SeguridadProxy>();
        }
    }
}
