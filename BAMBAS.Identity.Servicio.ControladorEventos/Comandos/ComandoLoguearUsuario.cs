﻿using BAMBAS.Identity.Servicio.ControladorEventos.Repuestas;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace BAMBAS.Identity.Servicio.ControladorEventos.Comandos
{
    public class ComandoLoguearUsuario : IRequest<IdentityAccess>
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }


        public bool IsActiveDirectory { get; set; } = false;
    }
}
