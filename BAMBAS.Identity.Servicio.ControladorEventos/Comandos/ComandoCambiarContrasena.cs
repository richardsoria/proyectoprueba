﻿using BAMBAS.CORE.Structs;
using BAMBAS.Identity.Servicio.ControladorEventos.Repuestas;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BAMBAS.Identity.Servicio.ControladorEventos.Comandos
{
    public class ComandoCambiarContrasena : IRequest<RespuestaConsulta>
    {
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
