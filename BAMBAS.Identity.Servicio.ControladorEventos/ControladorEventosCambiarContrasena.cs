﻿using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE;
using BAMBAS.Identity.Servicio.ControladorEventos.Comandos;
using BAMBAS.Identity.Servicio.ControladorEventos.Repuestas;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using BAMBAS.DATABASE.Helpers;
using Dapper;
using BAMBAS.Identity.Modelos;
using System.Security.Cryptography;
using System.Text.Json;
using System.Linq;
using System.Data;
using BAMBAS.CORE.Structs;

namespace BAMBAS.Identity.Servicio.ControladorEventos
{
    public class ControladorEventosCambiarContrasena :
        IRequestHandler<ComandoCambiarContrasena, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;
        private readonly IConfiguration _configuration;

        public ControladorEventosCambiarContrasena(
            ConfiguracionConexionBD configuracionConexionSql,
            IConfiguration configuration)
        {
            _configuracionConexionSql = configuracionConexionSql;
            _configuration = configuration;
        }

        public async Task<RespuestaConsulta> Handle(ComandoCambiarContrasena notification, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@CUSRO", notification.Email);
            var usuario = await _configuracionConexionSql.ObtenerPrimerRegistro<Usuario>(ProcedimientosAlmacenados.Usuario.ObtenerUsuario, param);

            var response = LeerEncriptada(notification.OldPassword, usuario.Entidad.CLVE);
            if (response)//si la vieja coincide con la de bd
            {
                var paramt = new DynamicParameters();
                paramt.Add("@CUSRO", notification.Email);
                paramt.Add("@CLVE", notification.NewPassword);
                paramt.Add("@RETORNO", value: 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.EditarContrasena, "RETORNO", paramt);
            }
            
            return new RespuestaConsulta
            {
                CodEstado = -4,
                Nombre = "LA CONTRASEÑA INGRESADA NO COINCIDE CON LA ANTERIOR"
            };
        }

        private bool LeerEncriptada(string password, string savedPasswordHash)
        {
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }
    }
}
