﻿using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE;
using BAMBAS.Identity.Servicio.ControladorEventos.Comandos;
using BAMBAS.Identity.Servicio.ControladorEventos.Repuestas;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using BAMBAS.DATABASE.Helpers;
using Dapper;
using BAMBAS.Identity.Modelos;
using System.Security.Cryptography;
using System.Text.Json;
using System.Linq;
using System.Data;
using BAMBAS.CORE.Helpers;
using BAMBAS.CONFIG;
using Microsoft.Extensions.Options;

namespace BAMBAS.Identity.Servicio.ControladorEventos
{
    public class ControladorEventosLoguearUsuario :
        IRequestHandler<ComandoLoguearUsuario, IdentityAccess>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;
        private readonly IConfiguration _configuration;
        public ControladorEventosLoguearUsuario(
            ConfiguracionConexionBD configuracionConexionSql,
            IConfiguration configuration)
        {
            _configuracionConexionSql = configuracionConexionSql;
            _configuration = configuration;
        }

        public async Task<IdentityAccess> Handle(ComandoLoguearUsuario notification, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@CUSRO", notification.Email);
            //param.Add("@CLVE", pass);

            var usuario = await _configuracionConexionSql.ObtenerPrimerRegistro<Usuario>(ProcedimientosAlmacenados.Usuario.ObtenerUsuario, param);
            var result = new IdentityAccess();

            if (usuario.Entidad != null && usuario.EsSatisfactoria)
            {
                var response = false;
                if (notification.IsActiveDirectory)
                {
                    response = true;
                }
                else
                {
                    response = LeerEncriptada(notification.Password, usuario.Entidad.CLVE);
                }

                //response: si su contraseña es correcta
                //si no esta bloqueado
                //Y si esta activo

                if ((notification.IsActiveDirectory && !usuario.Entidad.FBLQUO && usuario.Entidad.GDESTDO == "A")
                    || (response && !notification.IsActiveDirectory && !usuario.Entidad.FBLQUO && usuario.Entidad.GDESTDO == "A"))
                {
                    result.Succeeded = true;
                    //SUCURSALES
                    var param2 = new DynamicParameters();
                    param2.Add("@IDUSRO", usuario.Entidad.ID);
                    var sucursales = await _configuracionConexionSql.EjecutarProcedimiento<SucursalUsuario>(ProcedimientosAlmacenados.SucursalUsuario.ListarSucursales, param2);
                    //si no tiene sucursales
                    if (sucursales.Count() > 0)
                    {
                        ///PERFILES
                        var paramet = new DynamicParameters();
                        paramet.Add("@IDUSRO", usuario.Entidad.ID);
                        var perfiles = await _configuracionConexionSql.EjecutarProcedimiento<UsuarioPerfilCustom>(ProcedimientosAlmacenados.PerfilUsuario.ListarPorUsuario, paramet);
                        perfiles = perfiles
                            .Where(x => x.CHECKEADO)
                            .ToList();
                        if (perfiles.Count > 0)
                        {
                            await GenerateToken(usuario.Entidad, result, sucursales, perfiles, notification.IsActiveDirectory);

                            //RESET INTENTOS DE LOGUEO
                            paramet.Add("@IDUSRO", usuario.Entidad.ID);
                            paramet.Add("@RST", true);
                            paramet.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            var ejecucion = await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.SumarIntentoBloqueo, "RETORNO", paramet);

                        }
                        else
                        {
                            result.Succeeded = false;
                            result.ErrorMessage = "ACCESO DENEGADO: El usuario no tiene perfil asignado.";
                        }
                    }
                    else
                    {
                        result.Succeeded = false;
                        result.ErrorMessage = "ACCESO DENEGADO: El usuario no tiene Sucursales asignadas.";
                    }
                }
                else//si la contraseña es incorrecta
                {
                    result.Succeeded = false;
                    result.ErrorMessage = "ACCESO DENEGADO";
                    var paramet = new DynamicParameters();
                    paramet.Add("@IDUSRO", usuario.Entidad.ID);
                    paramet.Add("@RST", false);
                    paramet.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    var ejecucion = await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Usuario.SumarIntentoBloqueo, "RETORNO", paramet);

                    if (ejecucion.EsSatisfactoria && ejecucion.CodEstado == 1)
                        result.ErrorMessage = "SU USUARIO HA SIDO BLOQUEADO";
                }
                if (ConvertHelpers.DatepickerToDate(usuario.Entidad .FVCLVE.Trim()) <= DateTime.Today && !notification.IsActiveDirectory)
                {
                    result.Succeeded = false;
                    result.CambiarContrasena = true;
                    result.ErrorMessage = usuario.Entidad.CUSRO;
                }
                if (usuario.Entidad?.FRZRCMBOCLVE ?? false && !notification.IsActiveDirectory)
                {
                    result.Succeeded = false;
                    result.CambiarContrasena = usuario.Entidad.FRZRCMBOCLVE;
                    result.ErrorMessage = usuario.Entidad.CUSRO;
                }
            }
            else
            {
                result.Succeeded = false;
                result.ErrorMessage = string.IsNullOrEmpty(usuario.Nombre) && !notification.IsActiveDirectory ? "ACCESO DENEGADO" : notification.IsActiveDirectory ? "SU USUARIO NO HA SIDO CREADO EN EL SISTEMA DE GESTION DE PERSONAL" : $"ERROR {usuario.Nombre}";
            }

            return result;
        }

        private bool LeerEncriptada(string password, string savedPasswordHash)
        {
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }
        private async Task GenerateToken(Usuario user, IdentityAccess identity, List<SucursalUsuario> sucursales, List<UsuarioPerfilCustom> perfiles, bool esActiveDirectory)
        {
            
            var secretKey = _configuration.GetValue<string>("SecretKey");
            var key = Encoding.ASCII.GetBytes(secretKey);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
                new Claim(ClaimTypes.Name, user.CUSRO),
                new Claim(ClaimTypes.Surname, user.NYAPLLDS),
                new Claim(ClaimTypes.Email, user.UHMLGDO ?? ""),
                new Claim("SCRSLES", JsonSerializer.Serialize(sucursales)),
                new Claim("PRFLS",string.Join(",",perfiles.Select(x=>x.ID))),
                new Claim("ActDirec",esActiveDirectory.ToString()),
                new Claim("ScrslSelec", sucursales.FirstOrDefault().IDSCRSL)
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                )
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            identity.AccessToken = tokenHandler.WriteToken(createdToken);
        }
    }
}
