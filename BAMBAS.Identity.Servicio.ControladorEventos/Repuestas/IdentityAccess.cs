﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Identity.Servicio.ControladorEventos.Repuestas
{
    public class IdentityAccess
    {
        public bool Succeeded { get; set; }
        public string AccessToken { get; set; }
        public string ErrorMessage { get; set; }
        public bool CambiarContrasena { get; set; }
    }
}
