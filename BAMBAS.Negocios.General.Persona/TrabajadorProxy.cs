﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Trabajador;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Persona
{
    public interface ITrabajadorProxy
    {
        Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param);
        Task<List<TrabajadorDto>> ObtenerContratista();
        Task<DataTablesStructs.ReturnedData<TrabajadorDto>> listadoPersonalTareo(DataTablesStructs.SentParameters parameters, string csap, string datos, int idcntrtsta, int bambas);
    }
    public class TrabajadorProxy : ITrabajadorProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public TrabajadorProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }


        public async Task<Select2Structs.ResponseParameters> SelectTrabajador(Select2Structs.RequestParameters param)
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/select-trabajador".AgregarParametrosSelect2(param);
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<Select2Structs.ResponseParameters>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<TrabajadorDto>> ObtenerContratista()
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/obtenerContratista";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<TrabajadorDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<TrabajadorDto>> listadoPersonalTareo(DataTablesStructs.SentParameters parameters, string csap, string datos, int idcntrtsta, int bambas)
        {
            var url = $"{_apiUrls.PersonaUrl}trabajador/listadoPersonalTareo?csap={csap}&datos={datos}&idcntrtsta={idcntrtsta}&bambas={bambas}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<TrabajadorDto>(-2);
        }
    }

}
