﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Persona.Persona;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.General.Persona
{
    public interface IPersonaProxy
    {
        Task<DataTablesStructs.ReturnedData<ListarPersona>> listadoPersonaSinUsuario(DataTablesStructs.SentParameters parameters, string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado);
    }
    public class PersonaProxy : IPersonaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PersonaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<ListarPersona>> listadoPersonaSinUsuario(DataTablesStructs.SentParameters parameters, string id, string gdtprsna, string tipoDocumento, string numeroDocumento, string datos, string fechaDesde, string fechaHasta, string estado)
        {
            var url = $"{_apiUrls.PersonaUrl}persona/listadoPersonaSinUsuario?id={id}&gdtprsna={gdtprsna}&tipoDocumento={tipoDocumento}&numeroDocumento={numeroDocumento}&datos={datos}&fechaDesde={fechaDesde}&fechaHasta={fechaHasta}&estado={estado}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);

            return request.RespuestaTabla<ListarPersona>(-2);
        }
    }
}
