﻿using System;
using System.Collections.Generic;
using System.Text;
using static BAMBAS.CORE.Structs.Select2Structs;

namespace BAMBAS.CORE.Services.Interfaces
{
    public interface ISelect2Service
    {
        int GetCurrentPage();
        string GetQuery();
        string GetRequestType();
        string GetSearchTerm();
        RequestParameters GetRequestParameters();
    }
}
