﻿using System;
using System.Collections.Generic;
using System.Text;
using static BAMBAS.CORE.Structs.PaginationStructs;

namespace BAMBAS.CORE.Services.Interfaces
{
    public interface IPaginationService
    {
        int GetRecordsPerDraw();
        int GetPage();
        string GetSearchValue();
        SentParameters GetSentParameters();
    }
}
