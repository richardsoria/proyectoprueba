﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.CORE.Services.Models
{
    public class EmailStandard
    {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
