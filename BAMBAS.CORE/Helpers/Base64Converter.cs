﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.CORE.Helpers
{
    public static class Base64Converter
    {
        public static byte[] GetBase64Content(string[] token)
        {
            var bytes = new List<byte>();
            try
            {
                bytes = (Convert.FromBase64String($"{token[1]}")).ToList();
                return bytes.ToArray();
            }
            catch
            {
                try
                {
                    bytes = (Convert.FromBase64String($"{token[1]}=")).ToList();
                    return bytes.ToArray();
                }
                catch
                {
                    try
                    {
                        bytes = (Convert.FromBase64String($"{token[1]}==")).ToList();
                        return bytes.ToArray();
                    }
                    catch { }
                }
            }
            return null;
        }
    }
}
