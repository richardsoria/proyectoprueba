﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.CORE.Extensions
{
    public static class ClaimsExtension
    {
        public static string GetFullName(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                return user?.Claims?.FirstOrDefault(v => v.Type == ClaimTypes.Surname)?.Value ?? "-";
            }

            return "-";
        }

        public static string GetUserCode(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                if (user.EstaAutenticadoPorActiveDirectory())
                {
                    return user?.Claims?.FirstOrDefault(v => v.Type == ClaimTypes.Email)?.Value ?? "-";
                }
                return user?.Claims?.FirstOrDefault(v => v.Type == ClaimTypes.Name)?.Value ?? "-";
            }

            return "-";
        }
        public static string GetUserAccessToken(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                return user?.Claims?.FirstOrDefault(v => v.Type == "access_token")?.Value ?? "";
            }
            return "";
        }
        public static string ObtenerSucursales(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                var sucursales = user?.Claims?.FirstOrDefault(v => v.Type == "SCRSLES")?.Value ?? "";
                return sucursales;
            }
            return null;
        }
        public static string ObtenerPerfiles(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                var sucursales = user?.Claims?.FirstOrDefault(v => v.Type == "PRFLS")?.Value ?? "";
                return sucursales;
            }
            throw new Exception("Usuario no logueado");
        }

        public static bool EstaAutenticadoPorActiveDirectory(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated)
            {
                var sucursales = user?.Claims?.FirstOrDefault(v => v.Type == "ActDirec")?.Value ?? "";

                return (!string.IsNullOrEmpty(sucursales) ? Convert.ToBoolean(sucursales) : false);
            }
            throw new Exception("Usuario no logueado");
        }
    }
}
