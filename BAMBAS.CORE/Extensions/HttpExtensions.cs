﻿using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.CORE.Extensions
{
    public static class HttpExtensions
    {
        private static List<Claim> GetClaims(string access_token, AccessTokenUserInformation user)
        {
            return new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.nameid),
                new Claim(ClaimTypes.Name, user.unique_name),
                new Claim(ClaimTypes.Surname, user.family_name),
                new Claim(ClaimTypes.Email, user.email),
                new Claim("access_token", access_token),
                new Claim("SCRSLES", user.SCRSLES??""),
                new Claim("PRFLS", user.PRFLS??""),
                new Claim("ScrslSelec", user.ScrslSelec??""),
                new Claim("ActDirec",user.ActDirec)
            };
        }

        public static async Task ConectarConAccessToken(this HttpContext httpContext, string access_token, IMemoryCache cache = null)
        {
            var token = access_token.Split('.');
            var base64Content = Base64Converter.GetBase64Content(token);
            var user = JsonSerializer.Deserialize<AccessTokenUserInformation>(base64Content);
            var claims = GetClaims(access_token, user);

            var claimsIdentity = new ClaimsIdentity(
                 claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IssuedUtc = DateTime.UtcNow.AddHours(30),
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(30)
            };

            await httpContext.SignInAsync(
                 CookieAuthenticationDefaults.AuthenticationScheme,
                 new ClaimsPrincipal(claimsIdentity),
                 authProperties);

            if (cache != null)
            {
                // Look for cache key.
                var cacheEntry = "";
                if (!cache.TryGetValue("ScrslSelec", out cacheEntry))
                {
                    // Key not in cache, so get data.
                    cacheEntry = user.ScrslSelec;

                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetSlidingExpiration(TimeSpan.FromDays(1));

                    // Save data in cache.
                    cache.Set("ScrslSelec", cacheEntry, cacheEntryOptions);
                }
            }
        }
    }
}
