﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BAMBAS.CORE.Models
{
    public class AccessTokenUserInformation
    {
        public string nameid { get; set; }
        public string email { get; set; }
        public string unique_name { get; set; }
        public int exp { get; set; }
        public string family_name { get; set; }
        public string SCRSLES { get; set; }
        public string ScrslSelec { get; set; }
        public string PRFLS { get; set; }
        public string ActDirec { get; set; }

    }  
}
