﻿using BAMBAS.CORE.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos
{
    public class AuditoriaDto
    {
        public int? ID { get; set; }
        public string UCRCN { get; set; }
        public DateTime? FCRCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDCN { get; set; }
        public string GDESTDO { get; set; } = "A";
        public DateTime? FESTDO { get; set; }
        public string cFCRCN => FCRCN.ToLocalDateTimeFormat();
        public string cFEDCN => FEDCN.ToLocalDateTimeFormat();
        public string cFESTDO => FESTDO.ToLocalDateTimeFormat();
    }
}
