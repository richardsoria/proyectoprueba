﻿
using Microsoft.AspNetCore.Http;

namespace BAMBAS.Negocios.Modelos.Persona.Archivo
{
    public class ArchivoDto : AuditoriaDto
    {
        public string PATHARCHV { get; set; }
        public string RUTA { get; set; }
        public string NMBRARCHV { get; set; }
        public string TPOARCHV { get; set; }
        public string FPRNCPL { get; set; }
        public int IDPRSNA { get; set; }
    }
    public class ArchivoDtoCustom: ArchivoDto
    {
        public IFormFile ARCHVO { set; get; }
    }
}
