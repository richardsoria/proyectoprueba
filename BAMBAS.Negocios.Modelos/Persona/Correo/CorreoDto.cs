﻿namespace BAMBAS.Negocios.Modelos.Persona.Correo
{
    public class CorreoDto : AuditoriaDto
    {
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
