﻿namespace BAMBAS.Negocios.Modelos.Persona.Telefono
{
    public class TelefonoDto : AuditoriaDto
    {
        public string GDTTLFNO { get; set; }
        public string CUBGEO { get; set; }
        public string NTLFNO { get; set; }
        public string OBSRVCN { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
