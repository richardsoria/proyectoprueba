﻿namespace BAMBAS.Negocios.Modelos.Persona.Documento
{
    public class ProyeccionDto : AuditoriaDto
    {
        public string GDDCMNTO { get; set; }
        public string IGDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string FINSCRPCN { get; set; }
        public string FVNCMNTO { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
