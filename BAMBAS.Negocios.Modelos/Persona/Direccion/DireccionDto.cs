﻿namespace BAMBAS.Negocios.Modelos.Persona.Direccion
{
    public class DireccionDto : AuditoriaDto
    {
        public string IGDTDRCCN { get; set; }
        public string GDTDRCCN { get; set; }
        public string GDTVIA { get; set; }
        public string VIA { get; set; }
        public string NVIA { get; set; }
        public string NINTRR { get; set; }
        public string GDTDZNA { get; set; }
        public string ZNA { get; set; }
        public string RFRNCIA { get; set; }
        public string FPRNCPL { get; set; }
        public string CUBGEO { get; set; }
        public string IDPRSNA { get; set; }
    }
}
