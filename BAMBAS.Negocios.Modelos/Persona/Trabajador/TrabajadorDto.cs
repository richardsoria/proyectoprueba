﻿using System.Collections.Generic;
using System.Linq;

namespace BAMBAS.Negocios.Modelos.Persona.Trabajador
{
    public class TrabajadorDto : AuditoriaDto
    {
        public string IDPRSNA { get; set; }
        public string NMBRS { get; set; }
        public string DATOS { get; set; }
        public string GDTPRSNA { get; set; }
        public string CNTRTSTA { get; set; }
        public string CSAP { get; set; }
        public string CRGO { get; set; }
        public string GDTTRBJDR { get; set; }
        public int? IDCNTRTSTA { get; set; }
        public string FINGRSO { get; set; }
        public string FBJA { get; set; }
        public string MBJA { get; set; }
        public string GDSDE { get; set; }
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public int IDARA { get; set; }
        public int IDGRPO { get; set; }
        public string GRPO { get; set; }
        public int IDCRGO { get; set; }
        public int IDPSCN { get; set; }
        public int IDCNTROCSTO { get; set; }
        public int IDUNDDORGNZCNL { get; set; }
        public string GDGRPOPS { get; set; }
        public string GDNVL { get; set; }
        public string GDDLGCNATRZDA { get; set; }
        public string GDPRFLTRBJDR => string.Join(",", GDPRFLSTRBJDR?.Select(x => x)?? new List<string>())??"";
        public string[] GDPRFLSTRBJDR { get; set; }
        public int FTRBJORMTO { get; set; }
        public string GDTGRDA { get; set; }
        public int FTITLR { get; set; }
        public string GDLGRALJMNTO { get; set; }
        public string GDUBCCNALJMNTO { get; set; }
        public string GDTALJMNTO { get; set; }
        public string MDLO { get; set; }
        public string PSO { get; set; }
        public string CMA { get; set; }
        public string FTO { get; set; }
        public int BAMBAS { get; set; }
    }
}
