﻿using BAMBAS.CORE.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Persona.Persona
{
    public class ListarPersona
    {
        //private string fcrcn;  
        public int ID { get; set; }
        public string RSCL { get; set; }
        public string GDTPRSNA { get; set; }
        public string TPRSNA { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string DATOS { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public bool FCNTRTSTA { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string PUSUARIO { get; set; }
        public string FVNCMNTO { get; set; }
        public string FCRCN { get; set; }
        public string FEDCN { get; set; }
        public string UEDCN { get; set; }
        public DateTime? FEDICION { get; set; }
        public string cFEDCN => FEDICION.ToLocalDateTimeFormat();
        public string GDESTDO { get; set; }
        public string MPEPE00 { get; set; }//INCLUIRLO PARA OBTENER TIPO PERSONA
        public string MPEPE01 { get; set; }
        public string MPEPE02 { get; set; }
        public string MPEPE03 { get; set; }
        public string MPEPE04 { get; set; }
    }
}
