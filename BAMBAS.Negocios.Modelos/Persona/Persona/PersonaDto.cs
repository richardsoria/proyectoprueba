﻿using Microsoft.AspNetCore.Http;
using System;

namespace BAMBAS.Negocios.Modelos.Persona.Persona
{
    public class PersonaDto : AuditoriaDto
    {
        public string GDTPRSNA { get; set; }
        public string GDDCMNTO { get; set; }
        public string RSCL { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public string NCNLDD { get; set; }
        public string NDCMNTO { get; set; }
        public string CPNCMNTO { get; set; }
        public string GDECVL { get; set; }
        public string GDSXO { get; set; }
        public string FNCMNTO { get; set; }
        public string FFLLCMNTO { get; set; }
        public string CUBGEO { get; set; }
        public string GDTESCDD { get; set; }
        public string GDTETMNO { get; set; }
        public string FTO { get; set; }
        public bool FCNTRTSTA { get; set; }

    }
    public class PersonaConImagenDto : PersonaDto
    {
        public IFormFile IMGNPRSNA { get; set; }
    }
}








