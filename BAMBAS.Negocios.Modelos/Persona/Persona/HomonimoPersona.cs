﻿using BAMBAS.CORE.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Persona.Persona
{
    public class HomonimoPersona
    {
        public int ID { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string DATOS { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public DateTime? FCRCN { get; set; }
        public string cFCRCN => FCRCN.ToLocalDateTimeFormat();
        public string GDESTDO { get; set; }

    }
}
