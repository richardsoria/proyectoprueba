﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Tareo.Tareo
{
    public class DatosPersonalTareo : AuditoriaDto
    {
        public int IDTRO { get; set; }
        public string CSAP { get; set; }
    }
}
