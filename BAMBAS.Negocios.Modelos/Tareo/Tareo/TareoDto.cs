﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Tareo.Tareo
{
    public class TareoDto : AuditoriaDto
    {
        public string GDTRNO { get; set; }
        public string TRNO { get; set; }
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public int IDARA { get; set; }

        public string GRNCA { get; set; }
        public string SPRINTNDNCA { get; set; }
        public string ARA { get; set; }
        public int IDGRDIA { get; set; }
        public string GRDIA { get; set; }
        public string RSTR { get; set; }
        public int IDSPRVSR { get; set; }
        public int IDGRPO { get; set; }
        public int IDPRSNA { get; set; }
        public string FFINTRO { get; set; }
        public string FINCTRO { get; set; }
        public bool INDTRO { get; set; }
        public string OBSRVCNS { get; set; }
        public int IDCMBGRDIA { get; set; }    //ID CAMBIO DE GUARDIA
        public int IDUAPRBCN { get; set; }  //ID USUARIO APROBACION
        public bool INDBMBS { get; set; }
        public int IDCNTRTSTA { get; set; }  //ID CONTRATISTA
    }
}
