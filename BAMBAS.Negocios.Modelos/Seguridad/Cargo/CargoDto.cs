﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Cargo
{
    public class CargoDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int IDGRPO { get; set; }
        public string GRPO { get; set; }
    }
}
