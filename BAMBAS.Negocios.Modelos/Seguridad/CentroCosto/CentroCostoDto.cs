﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.CentroCosto
{
    public class CentroCostoDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public string DSCRPCNINGLS { get; set; }
        public string CSAP { get; set; }
    }
}
