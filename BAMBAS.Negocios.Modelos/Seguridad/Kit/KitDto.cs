﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Kit
{
    public class KitDto : AuditoriaDto
    {
        public string KITS { get; set; }
        public string DSCRPCN { get; set; }
        public string IDFMLA { get; set; }
        public string CSAP { get; set; }
        public decimal CSTO { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string GDTMNDA { get; set; }
        public int CNTDD { get; set; }
        public string IDARTCLO { get; set; }
        public string IDKITDTLLE { get; set; }
    }
}
