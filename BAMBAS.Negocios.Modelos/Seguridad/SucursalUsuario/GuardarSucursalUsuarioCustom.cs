﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.SucursalUsuario
{
    public class GuardarSucursalUsuarioCustom
    {
        public string IdSucursal { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignados { get; set; }
        public List<int> NoAsignados { get; set; }
    }
    public class GuardarUsuarioSucursalCustom
    {
        public string IdUsuario { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignadas { get; set; }
        public List<int> NoAsignadas { get; set; }
    }
}
