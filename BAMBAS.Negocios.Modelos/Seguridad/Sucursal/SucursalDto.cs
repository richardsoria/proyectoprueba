﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Sucursal
{
    public class SucursalDto : AuditoriaDto
    {
        public string UBGO { get; set; }
        public string DSCRSL { get; set; }
        public string NSCRSL { get; set; }


        public int IDEMPRSA { get; set; }
    }
}
