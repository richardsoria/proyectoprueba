﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Almacen
{
    public class AlmacenDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public string GDTALMCN { get; set; }
        public string TALMCN { get; set; }
        public int? IDTRBJDR { get; set; }
        public string TRBJDR { get; set; }
    }
}
