﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.PerfilObjeto
{
    public class GuardarPerfilObjetoCustom
    {
        public string IdPerfil { get; set; }
        public string UCRCN { get; set; }
        public List<int> ModulosAsignados { get; set; }
        public List<int> ObjPadreAsignados { get; set; }
        public List<int> ObjHijoAsignados { get; set; }
        public List<int> DetalleAsignados { get; set; }
        public List<int> ModulosNoAsignados { get; set; }
        public List<int> ObjPadreNoAsignados { get; set; }
        public List<int> ObjHijoNoAsignados { get; set; }
        public List<int> DetalleNoAsignados { get; set; }
    }
    public class ArbolModel
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public List<ArbolModel> Nodos { get; set; }
        public bool Checkeado { get; set; }
        public int Tipo { get; set; }
    }
}
