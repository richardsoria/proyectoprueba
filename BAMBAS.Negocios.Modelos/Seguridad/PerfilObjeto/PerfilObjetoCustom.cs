﻿using BAMBAS.CORE.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.PerfilObjeto
{
    public class PerfilObjetoCustom
    {
        public int ID { get; set; }
        public string IDPDRE { get; set; }
        public string NMBRE { get; set; }
        public string URL { get; set; }
        public int TIPO { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
        public string cFEDCN => FEDCN.ToLocalDateTimeFormat();
    }
    public class PerfilObjetoDetalleCustom
    {
        public string IDOBJDET { get; set; }
        public bool ACCESO { get; set; }
    }
}
