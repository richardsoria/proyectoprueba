﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.TrabajadorRestriccion
{
    public class TrabajadorRestriccionDto : AuditoriaDto
    {
        public string CSAP { get; set; }
        public string DSCRPCNARTCLO { get; set; }
        public int IDARTCLO { get; set; }
        public int IDTRBJDR { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string OBSRVCN { get; set; }
    }
    public class ActualizarRestriccionDto
    {
        public int? ID { get; set; }
        public string TLLADTO { get; set; }
        public string UEDCN { get; set; }
        public int IDTRBJDR { get; set; }
    }
}
