﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Familia
{
    public class FamiliaDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int? IDFMLAPDRE { get; set; }
        public string FMLAPDRE { get; set; }
    }
}
