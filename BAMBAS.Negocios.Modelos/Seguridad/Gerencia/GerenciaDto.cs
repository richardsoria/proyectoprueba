﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Gerencia
{
    public class GerenciaDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
    }
}
