﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.ObjetoDetalle
{
    public class ObjetoDetalleDto:AuditoriaDto
    {
        public int IDOBJTO { get; set; }
        public string NOBJTO { get; set; }
        public string DOBJTO { get; set; }
    }
}
