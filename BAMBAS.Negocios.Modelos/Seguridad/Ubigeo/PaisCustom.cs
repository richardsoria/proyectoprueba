﻿namespace BAMBAS.Negocios.Modelos.Seguridad.Ubigeo
{
    public class PaisCustom
    {
        public string CPS { get; set; }
        public string DPRVNCA { get; set; }
    }
}
