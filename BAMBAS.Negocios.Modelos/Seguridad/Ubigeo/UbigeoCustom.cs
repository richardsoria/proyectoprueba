﻿namespace BAMBAS.Negocios.Modelos.Seguridad.Ubigeo
{
    public class UbigeoCustom
    {
        public string CDGO { get; set; }
        public string DSCRPCN { get; set; }
    }
}
