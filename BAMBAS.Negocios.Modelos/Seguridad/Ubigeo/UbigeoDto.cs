﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Ubigeo
{
    public class UbigeoDto: AuditoriaDto
    {
        public string CDINEI { get; set; }
        public string CPINEI { get; set; }
        public string CUINEI { get; set; }
        public string DDPRTMNTO { get; set; }
        public string DPRVNCA { get; set; }
        public string DDSTRTO { get; set; }
        public string CDSSLD { get; set; }
        public string CPSSLD { get; set; }
        public string CUSSLD { get; set; }
        public string CPS { get; set; }
    }
}
