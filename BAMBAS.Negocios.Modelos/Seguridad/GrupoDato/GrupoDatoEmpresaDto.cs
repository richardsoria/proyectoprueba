﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.GrupoDato
{
    public class GrupoDatoEmpresaDto:AuditoriaDto
    {
        public string CGDTO { get; set; }
        public string DGDTO { get; set; }
        public string AGDTO { get; set; }
    }
}
