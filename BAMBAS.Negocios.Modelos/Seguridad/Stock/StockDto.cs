﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Stock
{
    public class StockDto : AuditoriaDto
    {
        public int IDALMCN { get; set; }
        public int IDARTCLO { get; set; }
        public string TCDGO { get; set; }
        public string DSCRPCNARTCLO { get; set; }
        public string CSAP { get; set; }
        public int STCK { get; set; }
        public int STCKMNMO { get; set; }
        public decimal PRCO { get; set; }
        public string UNDDMDDA { get; set; }
    }
    public class ActualizacionMasivaStockDto
    {
        public List<StockDto> Stock { get; set; }
        public string UEDCN { get; set; }
        public int IDALMCN { get; set; }
    }
    public class ActualizarStockMinimoDto
    {
        public string STOCKS { get; set; }
        public string UEDCN { get; set; }
        public int IDALMCN { get; set; }
    }
    public class StockActualizadosDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public string CSAP { get; set; }
        public int STCKANTRR { get; set; }
        public int STCKNVO { get; set; }
    }
}
