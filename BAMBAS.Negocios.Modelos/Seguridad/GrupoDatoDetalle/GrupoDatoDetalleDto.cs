﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.GrupoDatoDetalle
{
    public class GrupoDatoDetalleDto : AuditoriaDto
    {
        public string CGDTO { get; set; }
        public string DGDDTLLE { get; set; }
        public string AGDDTLLE { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
        //public string ID { get; set; }
    }
}
