﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Articulo
{
    public class ArticuloDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int IDFMLA { get; set; }
        public int IDSBFMLA { get; set; }
        public string CBRRA { get; set; }
        public string CSAP { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string GDTMNDA { get; set; }
        public int DRCN { get; set; }
        public decimal CSTO { get; set; }
        public bool FKIT { get; set; }
        public bool FMNJOTLLS { get; set; }
        public bool FMNJORSTRCN { get; set; }
        public string MNSJE { get; set; }
    }
    public class ActualizacionMasivaPreciosDto
    {
        public List<ArticuloDto> Articulos { get; set; }
        public string UEDCN { get; set; }
    }
    public class ArticuloActualizadosDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public string CSAP { get; set; }
        public decimal CSTOANTRR { get; set; }
        public decimal CSTONVO { get; set; }
    }
}
