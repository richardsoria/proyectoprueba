﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Error
{
    public class ErrorDto
    {
        public string UNME { get; set; }
        public string EESQMA { get; set; }
        public string ENMBR { get; set; }
        public string ESVRT { get; set; }
        public string ESTTE { get; set; }
        public string EPRCDRE { get; set; }
        public string ERRORLNE { get; set; }
        public string EMSSGE { get; set; }
        public string EDTME { get; set; }
        public string UCRCN { get; set; }
    }
}
