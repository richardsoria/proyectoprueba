﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Usuario
{
    public class UsuarioDto : AuditoriaDto
    {
        public string IDPRSNA { get; set; }
        public string CUSRO{ get; set; }
        public string UHMLGDO { get; set; }
        public string NYAPLLDS { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NTLFNO { get; set; }
        public string CLVE  { get; set; }
        public string FVCLVE { get; set; }
        public bool FBLQUO { get; set; }
        public bool FRZRCMBOCLVE { get; set; }        
        public bool FCNTRTSTA { get; set; }        
        public int INTNTS { get; set; }
    }
}
