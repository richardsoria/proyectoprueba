﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Usuario
{
    public class UsuarioCustom : AuditoriaDto
    {
        //Usuario
        public string IDPRSNA { get; set; }
        public string CUSRO { get; set; }
        public string UHMLGDO { get; set; }
        public string NYAPLLDS { get; set; }
        public string CLVE { get; set; }
        public string FVCLVE { get; set; }
        public string FBLQUO { get; set; }
        public string FRZRCMBOCLVE { get; set; }
        public string FCNTRTSTA { get; set; }

        //PErsona
        public string RSCL { get; set; }
        public string APTRNO { get; set; }
        public string AMTRNO { get; set; }
        public string ACSDA { get; set; }
        public string PNMBRE { get; set; }
        public string SNMBRE { get; set; }

        //Telefono Principal
        public string GDTTLFNO { get; set; }
        public string NTLFNO { get; set; }

        //Documento Principal
        public string TDCMNTO { get; set; }
        public string GDDCMNTO { get; set; }
        public string NDCMNTO { get; set; }

        //Correo Principal
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }

        //Dirección Principal
        public string GDTDRCCN { get; set; }
        public string DRCCN { get; set; }
    }
}
