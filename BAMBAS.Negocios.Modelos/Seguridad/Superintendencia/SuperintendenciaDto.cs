﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Superintendencia
{
    public class SuperintendenciaDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int IDGRNCA { get; set; }
        public string GRNCA { get; set; }
    }
}
