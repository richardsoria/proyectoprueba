﻿using BAMBAS.CORE.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.PerfilUsuario
{
    public class UsuarioPerfilCustom
    {
        public int ID { get; set; }
        public string DPRFL { get; set; }
        public bool CHECKEADO { get; set; }
        public DateTime? FEDCN { get; set; }
        public string cFEDCN => FEDCN.ToLocalDateTimeFormat();
    }
}
