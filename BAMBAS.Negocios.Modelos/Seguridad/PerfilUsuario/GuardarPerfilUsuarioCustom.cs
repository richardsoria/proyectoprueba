﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.PerfilUsuario
{
    public class GuardarPerfilUsuarioCustom
    {
        public string IdPerfil { get; set; }
        public string UCRCN { get; set; }
        public List<int> Asignados { get; set; }
        public List<int> NoAsignados { get; set; }
    }
}
