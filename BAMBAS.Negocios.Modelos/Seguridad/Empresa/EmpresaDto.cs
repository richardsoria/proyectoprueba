﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Empresa
{
    public class EmpresaDto : AuditoriaDto
    {
        public string NEMPRSA { get; set; }
        public string RUC { get; set; }
        public string UBGO { get; set; }
        public string DRCCN { get; set; }
    }
}
