﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.ObjetoPadre
{
    public class ObjetoPadreDto:AuditoriaDto
    {
        public int IDMDLO { get; set; }
        public string MDLO { get; set; }
        public string OBJTOPDRE { get; set; }
        public string NOBJTO { get; set; }
        public int FORDN { get; set; }
        public string IDOBJTOPDRE { get; set; }
        public string DOBJTO { get; set; }
        public bool TIENEHIJOS { get; set; }
        public bool TIENEDETALLES { get; set; }
        public string URL { get; set; }
    }
}
