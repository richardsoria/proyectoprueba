﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Area
{
    public class AreaDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public string GRNCA { get; set; }
        public string SPRINTNDNCA { get; set; }
    }
}
