﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Roster
{
    public class RosterDto : AuditoriaDto
    {
        public string DSCRPCN { get; set; }
        public int DTRBJDS { get; set; }
        public int DDSCNSO { get; set; }
    }
}
