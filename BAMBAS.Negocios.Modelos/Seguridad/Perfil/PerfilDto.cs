﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Perfil
{
    public class PerfilDto:AuditoriaDto
    {
        public string DPRFL { get; set; }
    }
}
