﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.Negocios.Modelos.Seguridad.Parametro
{
    public class ParametroDto:AuditoriaDto
    {
        public string DPRMTRO { get; set; }
        public string APRMTRO { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
    }
}
