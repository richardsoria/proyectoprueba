﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.SucursalUsuario;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface ISucursalUsuarioProxy
    {
        Task<DataTablesStructs.ReturnedData<SucursalUsuarioCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idSucursal);
        Task<DataTablesStructs.ReturnedData<UsuarioSucursalCustom>> ObtenerSucursalesDataTable(DataTablesStructs.SentParameters parameters, string idEmpresa, string idUsuario);
        Task<RespuestaConsulta> GuardarSucursalesUsuario(GuardarSucursalUsuarioCustom command);
        Task<RespuestaConsulta> GuardarSucursalesUsuario(GuardarUsuarioSucursalCustom command);
    }
    public class SucursalUsuarioProxy : ISucursalUsuarioProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public SucursalUsuarioProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<SucursalUsuarioCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idSucursal)
        {
            var url = $"{_apiUrls.SeguridadUrl}sucursal-usuario/obtener-tabla?idSucursal={idSucursal}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<SucursalUsuarioCustom>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<UsuarioSucursalCustom>> ObtenerSucursalesDataTable(DataTablesStructs.SentParameters parameters, string idEmpresa, string idUsuario)
        {
            var url = $"{_apiUrls.SeguridadUrl}sucursal-usuario/obtener-sucursales?idEmpresa={idEmpresa}&idUsuario={idUsuario}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<UsuarioSucursalCustom>(-2);
        }
        public async Task<RespuestaConsulta> GuardarSucursalesUsuario(GuardarSucursalUsuarioCustom command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}sucursal-usuario/guardar", content);
           return request.Respuesta(-2, "Guardar");
        }
        public async Task<RespuestaConsulta> GuardarSucursalesUsuario(GuardarUsuarioSucursalCustom command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}sucursal-usuario/guardar-sucursales-usuario", content);
            return request.Respuesta(-2, "Guardar");
        }
    }
}
