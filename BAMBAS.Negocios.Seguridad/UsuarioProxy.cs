﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Usuario;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IUsuarioProxy
    {
        Task<DataTablesStructs.ReturnedData<UsuarioDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<RespuestaConsulta> Insertar(UsuarioDto command);
        Task<RespuestaConsulta> Actualizar(UsuarioDto command);
        Task<RespuestaConsulta> Eliminar(UsuarioDto command);
        Task<UsuarioDto> Obtener(string cusro);
        Task<UsuarioDto> ObtenerXid(int id);
        Task<UsuarioCustom> ObtenerPrincipal(int id);
    }
    public class UsuarioProxy : IUsuarioProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public UsuarioProxy(HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<RespuestaConsulta> Actualizar(UsuarioDto command)
        {
            var content = new StringContent(
                 JsonSerializer.Serialize(command),
                 Encoding.UTF8,
                 "application/json"
             );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}usuario/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }

        public async Task<RespuestaConsulta> Eliminar(UsuarioDto command)
        {
            var content = new StringContent(
               JsonSerializer.Serialize(command),
               Encoding.UTF8,
               "application/json"
           );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}usuario/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<RespuestaConsulta> Insertar(UsuarioDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}usuario/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }

        public async Task<UsuarioDto> Obtener(string cusro)
        {
            var url = $"{_apiUrls.SeguridadUrl}usuario/obtener?cusro={cusro}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<UsuarioDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<UsuarioDto> ObtenerXid(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}usuario/obtenerXid?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<UsuarioDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<UsuarioCustom> ObtenerPrincipal(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}usuario/obtenerPrincipal?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<UsuarioCustom>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<DataTablesStructs.ReturnedData<UsuarioDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}usuario/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<UsuarioDto>(-2);
        }
    }
}
