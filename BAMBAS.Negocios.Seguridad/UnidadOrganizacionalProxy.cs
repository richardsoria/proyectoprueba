﻿using BAMBAS.Negocios.Modelos.Seguridad.UnidadOrganizacional;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IUnidadOrganizacionalProxy
    {
        Task<List<UnidadOrganizacionalDto>> ObtenerActivos();
    }
    public class UnidadOrganizacionalProxy : IUnidadOrganizacionalProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public UnidadOrganizacionalProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<List<UnidadOrganizacionalDto>> ObtenerActivos()
        {
            var url = $"{_apiUrls.SeguridadUrl}unidad-organizacional/obtener-activos";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<UnidadOrganizacionalDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
