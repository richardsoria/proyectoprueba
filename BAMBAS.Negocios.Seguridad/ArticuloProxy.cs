﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{

    public interface IArticuloProxy 
    {
        Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters);
        Task<List<ArticuloDto>> ObtenerActivas();
        Task<List<ArticuloDto>> ObtenerAutocomplete(string term,bool flagKit);
        Task<List<ArticuloDto>> ObtenerAutocompleteTalla(string term);
        Task<List<ArticuloDto>> ObtenerAutocompleteRestricciones(string term);
        Task<RespuestaConsulta> Insertar(ArticuloDto articulo);
        Task<RespuestaConsulta> Actualizar(ArticuloDto articulo);
        Task<RespuestaConsulta> Eliminar(ArticuloDto command);
        Task<RespuestaConsulta> ActualizacionMasivaPrecios(ActualizacionMasivaPreciosDto command);
        Task<ArticuloDto> Obtener(int id);
        Task<DataTablesStructs.ReturnedData<ArticuloActualizadosDto>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters);
        Task<ArticuloDto> ObtenerUltimaActualizacion();
    }
    public class ArticuloProxy : IArticuloProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public ArticuloProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloDto>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableActivas(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-tabla-activas".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloDto>(-2);
        }
        public async Task<List<ArticuloDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(ArticuloDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}articulo/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(ArticuloDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}articulo/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(ArticuloDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}articulo/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<RespuestaConsulta> ActualizacionMasivaPrecios(ActualizacionMasivaPreciosDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}articulo/actualizacion-masiva-precios", content);
            return request.Respuesta(-2, "Actualización masiva de precios");
        }

        public async Task<ArticuloDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ArticuloDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<List<ArticuloDto>> ObtenerAutocomplete(string term,bool flagKit)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocomplete?term={term}&flagKit={flagKit}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<ArticuloDto>> ObtenerAutocompleteTalla(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocompleteTalla?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<ArticuloDto>> ObtenerAutocompleteRestricciones(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/ObtenerAutocompleteRestricciones?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ArticuloDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloActualizadosDto>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-ultimos-actualizados".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloActualizadosDto>(-2);
        }

        public async Task<ArticuloDto> ObtenerUltimaActualizacion()
        {
            var url = $"{_apiUrls.SeguridadUrl}articulo/obtener-ultima-act";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ArticuloDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
