﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.PerfilUsuario;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IPerfilUsuarioProxy
    {
        Task<DataTablesStructs.ReturnedData<PerfilUsuarioCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string idPerfil);
        Task<RespuestaConsulta> GuardarPerfilesUsuario(GuardarPerfilUsuarioCustom command);

        Task<DataTablesStructs.ReturnedData<UsuarioPerfilCustom>> ObtenerDataTablePorUsuario(DataTablesStructs.SentParameters parameters, string idUsuario);

        Task<RespuestaConsulta> GuardarPerfilesPorUsuario(GuardarPerfilPorUsuarioCustom command);
    }
    public class PerfilUsuarioProxy : IPerfilUsuarioProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PerfilUsuarioProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<PerfilUsuarioCustom>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string idPerfil)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil-usuario/obtener-tabla?idPerfil={idPerfil}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<PerfilUsuarioCustom>(-2);
        }
        public async Task<RespuestaConsulta> GuardarPerfilesUsuario(GuardarPerfilUsuarioCustom command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}perfil-usuario/guardar", content);
            return request.Respuesta(-2, "Guardar");
        }
        public async Task<DataTablesStructs.ReturnedData<UsuarioPerfilCustom>> ObtenerDataTablePorUsuario(DataTablesStructs.SentParameters parameters, string idUsuario)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil-usuario/obtener-tabla-porusuario?idUsuario={idUsuario}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<UsuarioPerfilCustom>(-2);
        }
        public async Task<RespuestaConsulta> GuardarPerfilesPorUsuario(GuardarPerfilPorUsuarioCustom command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}perfil-usuario/guardarporusuario", content);
           return request.Respuesta(-2, "Guardar");
        }
    }
}
