﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoDato;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IGrupoDatoProxy
    {
        Task<List<GrupoDatoEmpresaDto>> ObtenerTodos();
        Task<List<GrupoDatoEmpresaDto>> ObtenerActivos();
        Task<DataTablesStructs.ReturnedData<GrupoDatoEmpresaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<RespuestaConsulta> Insertar(GrupoDatoEmpresaDto command);
        Task<RespuestaConsulta> Actualizar(GrupoDatoEmpresaDto command);
        Task<RespuestaConsulta> Eliminar(GrupoDatoEmpresaDto command);
        Task<GrupoDatoEmpresaDto> Obtener(int id);
        Task<List<CatalogoGrupoDatoCustom>> ObtenerCatalogos();
    }
    public class GrupoDatoProxy : IGrupoDatoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GrupoDatoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<GrupoDatoEmpresaDto>> ObtenerTodos()
        {
            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}grupo-dato/obtener-todos");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GrupoDatoEmpresaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<CatalogoGrupoDatoCustom>> ObtenerCatalogos()
        {

            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}grupo-dato/obtener-catalogo");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<CatalogoGrupoDatoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<List<GrupoDatoEmpresaDto>> ObtenerActivos()
        {
            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}grupo-dato/obtener-activos");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GrupoDatoEmpresaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<DataTablesStructs.ReturnedData<GrupoDatoEmpresaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupo-dato/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<GrupoDatoEmpresaDto>(-2);
        }
        public async Task<RespuestaConsulta> Insertar(GrupoDatoEmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(GrupoDatoEmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(GrupoDatoEmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<GrupoDatoEmpresaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupo-dato/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<GrupoDatoEmpresaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
