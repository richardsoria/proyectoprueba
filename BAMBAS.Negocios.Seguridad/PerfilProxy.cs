﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Perfil;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IPerfilProxy
    {
        Task<DataTablesStructs.ReturnedData<PerfilDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<PerfilDto>> Listar();
        Task<RespuestaConsulta>  Insertar(PerfilDto empresa);
        Task<RespuestaConsulta>  Actualizar(PerfilDto empresa);
        Task<RespuestaConsulta> Eliminar(PerfilDto command);
        Task<PerfilDto> Obtener(int id);
    }
    public class PerfilProxy: IPerfilProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public PerfilProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<PerfilDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<PerfilDto>(-2);
        }
        public async Task<List<PerfilDto>> Listar()
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil/listar";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<PerfilDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(PerfilDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}perfil/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(PerfilDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}perfil/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(PerfilDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}Perfil/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<PerfilDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}perfil/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<PerfilDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
