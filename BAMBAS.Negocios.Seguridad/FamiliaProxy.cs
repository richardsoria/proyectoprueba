﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Familia;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IFamiliaProxy
    {
        Task<DataTablesStructs.ReturnedData<FamiliaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false);
        Task<List<FamiliaDto>> ObtenerActivas(int idFamilia);
        Task<List<FamiliaDto>> ObtenerTodas(int idFamilia);
        Task<List<FamiliaDto>> ObtenerActivasSubfamilia(int idFamilia, bool? soloHijos = false);
        Task<RespuestaConsulta> Insertar(FamiliaDto familia);
        Task<RespuestaConsulta> Actualizar(FamiliaDto familia);
        Task<RespuestaConsulta> Eliminar(FamiliaDto command);
        Task<FamiliaDto> Obtener(int id);
        Task<List<FamiliaDto>> ObtenerAutocomplete(string term);
    }
    public class FamiliaProxy : IFamiliaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public FamiliaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<FamiliaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idFamilia, bool? soloHijos = false)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-tabla?idFamilia={idFamilia}&soloHijos={soloHijos}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<FamiliaDto>(-2);
        }
        public async Task<List<FamiliaDto>> ObtenerActivas(int idFamilia)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-activas?idFamilia={idFamilia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<FamiliaDto>> ObtenerTodas(int idFamilia)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-todas?idFamilia={idFamilia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<FamiliaDto>> ObtenerActivasSubfamilia(int idFamilia, bool? soloHijos = false)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener-activas-Subfamilia?idFamilia={idFamilia}&soloHijos{soloHijos}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(FamiliaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}familia/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(FamiliaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}familia/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(FamiliaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}familia/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<FamiliaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<FamiliaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<List<FamiliaDto>> ObtenerAutocomplete(string term)
        {
            var url = $"{_apiUrls.SeguridadUrl}familia/ObtenerAutocomplete?term={term}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<FamiliaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }


    }
}
                                                                                                                                       