﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Stock;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IStockProxy
    {
        Task<DataTablesStructs.ReturnedData<StockDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idalmcn);
        Task<List<StockDto>> ObtenerActivas();
        Task<RespuestaConsulta> Insertar(ActualizarStockMinimoDto stock);
        Task<RespuestaConsulta> Eliminar(StockDto command);
        Task<RespuestaConsulta> ActualizacionMasivaStock(ActualizacionMasivaStockDto command);
        Task<StockDto> Obtener(int id);
        Task<DataTablesStructs.ReturnedData<StockActualizadosDto>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters,int idalmacen);
        Task<StockDto> ObtenerUltimaActualizacion(int idalmacen);
    }
    public class StockProxy : IStockProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public StockProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<StockDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idalmcn)
        {
            var url = $"{_apiUrls.SeguridadUrl}stock/obtener-tabla?idalmcn={idalmcn}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<StockDto>(-2);
        }
        public async Task<List<StockDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}stock/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<StockDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(ActualizarStockMinimoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}stock/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Eliminar(StockDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}stock/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<RespuestaConsulta> ActualizacionMasivaStock(ActualizacionMasivaStockDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}stock/actualizacion-stock", content);
            return request.Respuesta(-2, "Actualización de stock");
        }
        public async Task<StockDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}stock/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<StockDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<StockActualizadosDto>> ObtenerUltimosActualizadosDataTable(DataTablesStructs.SentParameters parameters,int idalmacen)
        {
            var url = $"{_apiUrls.SeguridadUrl}stock/obtener-ultimos-actualizados?idalmacen={idalmacen}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<StockActualizadosDto>(-2);
        }

        public async Task<StockDto> ObtenerUltimaActualizacion(int idalmacen)
        {
            var url = $"{_apiUrls.SeguridadUrl}stock/obtener-ultima-act?idalmacen={idalmacen}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<StockDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
