﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Area;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IAreaProxy
    {
        Task<DataTablesStructs.ReturnedData<AreaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia, int idSuperintendencia);
        Task<List<AreaDto>> ObtenerActivas(int idSuperintendencia);
        Task<List<AreaDto>> ObtenerTodas(int idSuperintendencia);
        Task<RespuestaConsulta> Insertar(AreaDto area);
        Task<RespuestaConsulta> Actualizar(AreaDto area);
        Task<RespuestaConsulta> Eliminar(AreaDto command);
        Task<AreaDto> Obtener(int id);
    }
    public class AreaProxy : IAreaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public AreaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<AreaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia, int idSuperintendencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}area/obtener-tabla?idGerencia={idGerencia}&idSuperintendencia={idSuperintendencia}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<AreaDto>(-2);
        }
        public async Task<List<AreaDto>> ObtenerActivas(int idSuperintendencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}area/obtener-activas?idSuperintendencia={idSuperintendencia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<AreaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<AreaDto>> ObtenerTodas(int idSuperintendencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}area/obtener-todas?idSuperintendencia={idSuperintendencia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<AreaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(AreaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}area/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(AreaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}area/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(AreaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}area/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<AreaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}area/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<AreaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
