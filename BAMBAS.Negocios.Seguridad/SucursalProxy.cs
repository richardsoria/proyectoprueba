﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Sucursal;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface ISucursalProxy
    {
        Task<DataTablesStructs.ReturnedData<SucursalDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int empresaId);
        Task<RespuestaConsulta> Insertar(SucursalDto command);
        Task<RespuestaConsulta> Actualizar(SucursalDto command);
        Task<SucursalDto> Obtener(int id);
        Task<RespuestaConsulta> Eliminar(SucursalDto command);
    }
    public class SucursalProxy:ISucursalProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public SucursalProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<SucursalDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,int empresaId)
        {
            var url = $"{_apiUrls.SeguridadUrl}sucursal/obtener-tabla?empresaId={empresaId}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<SucursalDto>(-2);
        }
        public async Task<RespuestaConsulta> Insertar(SucursalDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}sucursal/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(SucursalDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}sucursal/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(SucursalDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}sucursal/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<SucursalDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}sucursal/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<SucursalDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
