﻿using BAMBAS.Negocios.Modelos.Seguridad.Ubigeo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IUbigeoProxy
    {
        Task<List<UbigeoCustom>> ObtenerDepartamentos(string cps);
        Task<List<UbigeoCustom>> ObtenerProvincias(string codigoDpto, string cps);
        Task<List<UbigeoCustom>> ObtenerDistrito(string codigoProvincia, string cps);
        Task<List<PaisCustom>> ObtenerPaises();
    }
    public class UbigeoProxy : IUbigeoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public UbigeoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<List<UbigeoDto>> ObtenerTodo()
        {
            var url = $"{_apiUrls.SeguridadUrl}ubigeo/obtener-todo";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<UbigeoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<UbigeoCustom>> ObtenerDepartamentos(string cps)
        {
            var url = $"{_apiUrls.SeguridadUrl}ubigeo/obtener-departamentos?cps={cps}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<UbigeoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<UbigeoCustom>> ObtenerProvincias(string codigoDpto, string cps)
        {
            var url = $"{_apiUrls.SeguridadUrl}ubigeo/obtener-provincias?codigoDpto={codigoDpto}&cps={cps}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<UbigeoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<UbigeoCustom>> ObtenerDistrito(string codigoProvincia, string cps)
        {
            var url = $"{_apiUrls.SeguridadUrl}ubigeo/obtener-distritos?codigoProvincia={codigoProvincia}&cps={cps}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<UbigeoCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<PaisCustom>> ObtenerPaises()
        {
            var url = $"{_apiUrls.SeguridadUrl}ubigeo/obtener-paises";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<PaisCustom>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
