﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoCargo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IGrupoCargoProxy
    {
        Task<DataTablesStructs.ReturnedData<GrupoCargoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<GrupoCargoDto>> ObtenerActivos();
        Task<List<GrupoCargoDto>> ObtenerTodas();
        Task<RespuestaConsulta> Insertar(GrupoCargoDto grupocargo);
        Task<RespuestaConsulta> Actualizar(GrupoCargoDto grupocargo);
        Task<RespuestaConsulta> ActualizarDetalle(GrupoCargoDto grupocargo);
        Task<RespuestaConsulta> Eliminar(GrupoCargoDto command);
        Task<RespuestaConsulta> EliminarDetalle(GrupoCargoDto command);
        Task<GrupoCargoDto> Obtener(int id);
        Task<List<GrupoCargoDto>> ListarDetalle(int id);
    }
    public class GrupoCargoProxy : IGrupoCargoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GrupoCargoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<GrupoCargoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupocargo/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<GrupoCargoDto>(-2);
        }
        public async Task<List<GrupoCargoDto>> ObtenerActivos()
        {
            var url = $"{_apiUrls.SeguridadUrl}grupocargo/obtener-activos";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GrupoCargoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<GrupoCargoDto>> ObtenerTodas()
        {
            var url = $"{_apiUrls.SeguridadUrl}grupocargo/obtener-todas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GrupoCargoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(GrupoCargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupocargo/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(GrupoCargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupocargo/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> ActualizarDetalle(GrupoCargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupocargo/actualizar-detalle", content);
            return request.Respuesta(-2, nameof(ActualizarDetalle));
        }
        public async Task<RespuestaConsulta> Eliminar(GrupoCargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupocargo/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }
        public async Task<RespuestaConsulta> EliminarDetalle(GrupoCargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupocargo/eliminar-detalle", content);
            return request.Respuesta(-2, nameof(EliminarDetalle));
        }

        public async Task<GrupoCargoDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupocargo/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<GrupoCargoDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<GrupoCargoDto>> ListarDetalle(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupocargo/listar-detalle?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GrupoCargoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
