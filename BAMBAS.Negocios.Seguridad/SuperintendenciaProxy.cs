﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Superintendencia;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface ISuperintendenciaProxy
    {
        Task<DataTablesStructs.ReturnedData<SuperintendenciaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia);
        Task<List<SuperintendenciaDto>> ObtenerActivas(int idGerencia);
        Task<List<SuperintendenciaDto>> ObtenerTodas(int idGerencia);
        Task<RespuestaConsulta> Insertar(SuperintendenciaDto superintendencia);
        Task<RespuestaConsulta> Actualizar(SuperintendenciaDto superintendencia);
        Task<RespuestaConsulta> Eliminar(SuperintendenciaDto command);
        Task<SuperintendenciaDto> Obtener(int id);
    }
    public class SuperintendenciaProxy : ISuperintendenciaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public SuperintendenciaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<SuperintendenciaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGerencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}superintendencia/obtener-tabla?idGerencia={idGerencia}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<SuperintendenciaDto>(-2);
        }
        public async Task<List<SuperintendenciaDto>> ObtenerActivas(int idGerencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}superintendencia/obtener-activas?idGerencia={idGerencia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<SuperintendenciaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<SuperintendenciaDto>> ObtenerTodas(int idGerencia)
        {
            var url = $"{_apiUrls.SeguridadUrl}superintendencia/obtener-todas?idGerencia={idGerencia}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<SuperintendenciaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(SuperintendenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}superintendencia/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(SuperintendenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}superintendencia/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(SuperintendenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}superintendencia/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<SuperintendenciaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}superintendencia/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<SuperintendenciaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
