﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Almacen;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IAlmacenProxy
    {
        Task<DataTablesStructs.ReturnedData<AlmacenDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string gdtalmcn);
        Task<List<AlmacenDto>> ObtenerActivas();
        Task<RespuestaConsulta> Insertar(AlmacenDto almacen);
        Task<RespuestaConsulta> Actualizar(AlmacenDto almacen);
        Task<RespuestaConsulta> Eliminar(AlmacenDto command);
        Task<AlmacenDto> Obtener(int id);
    }
    public class AlmacenProxy : IAlmacenProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public AlmacenProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<AlmacenDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string gdtalmcn)
        {
            var url = $"{_apiUrls.SeguridadUrl}almacen/obtener-tabla?gdtalmcn={gdtalmcn}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<AlmacenDto>(-2);
        }
        public async Task<List<AlmacenDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}almacen/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<AlmacenDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(AlmacenDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}almacen/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(AlmacenDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}almacen/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(AlmacenDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}almacen/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<AlmacenDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}almacen/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<AlmacenDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
