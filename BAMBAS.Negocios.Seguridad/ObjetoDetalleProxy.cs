﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.ObjetoDetalle;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IObjetoDetalleProxy
    {
        Task<DataTablesStructs.ReturnedData<ObjetoDetalleDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idObjeto);
        Task<List<ObjetoDetalleDto>> ObtenerActivos(string idObjeto);
        Task<RespuestaConsulta> Insertar(ObjetoDetalleDto command);
        Task<RespuestaConsulta> Actualizar(ObjetoDetalleDto command);
        Task<ObjetoDetalleDto> Obtener(int id);
        Task<RespuestaConsulta> Eliminar(ObjetoDetalleDto command);
    }
    public class ObjetoDetalleProxy : IObjetoDetalleProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public ObjetoDetalleProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }
        public async Task<List<ObjetoDetalleDto>> ObtenerActivos(string idObjeto)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto-detalle/obtener-activos?idObjeto={idObjeto}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ObjetoDetalleDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<DataTablesStructs.ReturnedData<ObjetoDetalleDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string idObjeto)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto-detalle/obtener-tabla?idObjeto={idObjeto}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ObjetoDetalleDto>(-2);
        }
        public async Task<RespuestaConsulta> Insertar(ObjetoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto-detalle/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(ObjetoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto-detalle/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(ObjetoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto-detalle/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<ObjetoDetalleDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto-detalle/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ObjetoDetalleDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
