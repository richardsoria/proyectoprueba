﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Articulo;
using BAMBAS.Negocios.Modelos.Seguridad.Kit;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IKitProxy
    {
        Task<DataTablesStructs.ReturnedData<KitDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters);
        Task<List<KitDto>> ObtenerActivas();
        Task<RespuestaConsulta> Insertar(KitDto kit);
        Task<RespuestaConsulta> Actualizar(KitDto kit);
        Task<RespuestaConsulta> Eliminar(KitDto command);
        Task<KitDto> Obtener(int id); 
        Task<List<KitDto>> ListarDetalle(int id);
    }
    public class KitProxy : IKitProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public KitProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<KitDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}kit/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<KitDto>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ArticuloDto>> ObtenerDataTableArticulos(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}kit/obtener-tabla-articulos".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<ArticuloDto>(-2);
        }
        public async Task<List<KitDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}kit/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<KitDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(KitDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}kit/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(KitDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}kit/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(KitDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}kit/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<KitDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}kit/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<KitDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<KitDto>> ListarDetalle(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}kit/listar-detalle?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<KitDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
