﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.GrupoDatoDetalle;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IGrupoDatoDetalleProxy
    {
        Task<DataTablesStructs.ReturnedData<GrupoDatoDetalleDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string codigoGrupoDato);
        Task<RespuestaConsulta> Insertar(GrupoDatoDetalleDto command);
        Task<RespuestaConsulta> Actualizar(GrupoDatoDetalleDto command);
        Task<GrupoDatoDetalleDto> Obtener(int id);
        Task<RespuestaConsulta> Eliminar(GrupoDatoDetalleDto command);
    }
    public class GrupoDatoDetalleProxy : IGrupoDatoDetalleProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GrupoDatoDetalleProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<GrupoDatoDetalleDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string codigoGrupoDato)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupo-dato-detalle/obtener-tabla?codigoGrupoDato={codigoGrupoDato}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<GrupoDatoDetalleDto>(-2);
        }
        public async Task<RespuestaConsulta> Insertar(GrupoDatoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato-detalle/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(GrupoDatoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato-detalle/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(GrupoDatoDetalleDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}grupo-dato-detalle/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<GrupoDatoDetalleDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}grupo-dato-detalle/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<GrupoDatoDetalleDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
