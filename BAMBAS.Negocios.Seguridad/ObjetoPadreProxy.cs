﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.ObjetoPadre;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IObjetoPadreProxy
    {
        Task<List<ObjetoPadreDto>> ObtenerTodos();
        Task<List<ObjetoPadreDto>> ObtenerActivos();
        Task<DataTablesStructs.ReturnedData<ObjetoPadreDto>> ObtenerPadreDataTable(DataTablesStructs.SentParameters parameters);
        Task<DataTablesStructs.ReturnedData<ObjetoPadreDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string idobjetopadre);
        Task<RespuestaConsulta>  Insertar(ObjetoPadreDto command);
        Task<RespuestaConsulta>  Actualizar(ObjetoPadreDto command);
        Task<RespuestaConsulta> Eliminar(ObjetoPadreDto command);
        Task<ObjetoPadreDto> Obtener(int id);
    }
    public class ObjetoPadreProxy : IObjetoPadreProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public ObjetoPadreProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<List<ObjetoPadreDto>> ObtenerTodos()
        {
            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}objeto/obtener-todos");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ObjetoPadreDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<ObjetoPadreDto>> ObtenerActivos()
        {
            var request = await _httpClient.GetAsync($"{_apiUrls.SeguridadUrl}objeto/obtener-activos");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<ObjetoPadreDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }

        public async Task<DataTablesStructs.ReturnedData<ObjetoPadreDto>> ObtenerPadreDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto/obtener-tabla-padre".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ObjetoPadreDto>(-2);
        }
        public async Task<DataTablesStructs.ReturnedData<ObjetoPadreDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters,string idobjetopadre)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto/obtener-tabla?idobjetopadre={idobjetopadre}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            return request.RespuestaTabla<ObjetoPadreDto>(-2);
        }
        public async Task<RespuestaConsulta> Insertar(ObjetoPadreDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(ObjetoPadreDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(ObjetoPadreDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}objeto/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<ObjetoPadreDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}objeto/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ObjetoPadreDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
