﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos.Seguridad.Empresa;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IEmpresaProxy
    {
        Task<DataTablesStructs.ReturnedData<EmpresaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<EmpresaDto>> ObtenerActivos();
        Task<RespuestaConsulta> Insertar(EmpresaDto empresa);
        Task<RespuestaConsulta> Actualizar(EmpresaDto empresa);
        Task<RespuestaConsulta> Eliminar(EmpresaDto command);
        Task<EmpresaDto> Obtener(int id);
    }
    public class EmpresaProxy : IEmpresaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public EmpresaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<EmpresaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}empresa/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<EmpresaDto>(-2);
        }
        public async Task<List<EmpresaDto>> ObtenerActivos()
        {
            var url = $"{_apiUrls.SeguridadUrl}empresa/obtener-activos";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<EmpresaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(EmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}empresa/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(EmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}empresa/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(EmpresaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}empresa/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<EmpresaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}empresa/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<EmpresaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
