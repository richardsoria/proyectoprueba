﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Cargo;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface ICargoProxy
    {
        Task<DataTablesStructs.ReturnedData<CargoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGrupoCargo);
        Task<List<CargoDto>> ObtenerActivos(int idGrupoCargo);
        Task<List<CargoDto>> ObtenerTodas(int idGrupoCargo);
        Task<RespuestaConsulta> Insertar(CargoDto cargo);
        Task<RespuestaConsulta> Actualizar(CargoDto cargo);
        Task<RespuestaConsulta> Eliminar(CargoDto command);
        Task<CargoDto> Obtener(int id);
    }
    public class CargoProxy : ICargoProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public CargoProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<CargoDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idGrupoCargo)
        {
            var url = $"{_apiUrls.SeguridadUrl}cargo/obtener-tabla?idGrupoCargo={idGrupoCargo}".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<CargoDto>(-2);
        }                                                                          
        public async Task<List<CargoDto>> ObtenerActivos(int idGrupoCargo)
        {
            var url = $"{_apiUrls.SeguridadUrl}cargo/obtener-activos?idGrupoCargo={idGrupoCargo}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<CargoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<CargoDto>> ObtenerTodas(int idGrupoCargo)
        {
            var url = $"{_apiUrls.SeguridadUrl}cargo/obtener-todas?idGrupoCargo={idGrupoCargo}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<CargoDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(CargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}cargo/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(CargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}cargo/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(CargoDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}cargo/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<CargoDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}cargo/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<CargoDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
