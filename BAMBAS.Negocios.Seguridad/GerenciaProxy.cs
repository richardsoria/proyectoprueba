﻿using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.Negocios.Modelos;
using BAMBAS.Negocios.Modelos.Seguridad.Gerencia;
using BAMBAS.Negocios.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Seguridad
{
    public interface IGerenciaProxy
    {
        Task<DataTablesStructs.ReturnedData<GerenciaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters);
        Task<List<GerenciaDto>> ObtenerActivas();
        Task<List<GerenciaDto>> ObtenerTodas();
        Task<RespuestaConsulta> Insertar(GerenciaDto gerencia);
        Task<RespuestaConsulta> Actualizar(GerenciaDto gerencia);
        Task<RespuestaConsulta> Eliminar(GerenciaDto command);
        Task<GerenciaDto> Obtener(int id);
    }
    public class GerenciaProxy : IGerenciaProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public GerenciaProxy(
            HttpClient httpClient,
            IOptions<ApiUrls> apiUrls,
            IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }

        public async Task<DataTablesStructs.ReturnedData<GerenciaDto>> ObtenerDataTable(DataTablesStructs.SentParameters parameters)
        {
            var url = $"{_apiUrls.SeguridadUrl}gerencia/obtener-tabla".AgregarParametrosDatatable(parameters);
            var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();
            return request.RespuestaTabla<GerenciaDto>(-2);
        }
        public async Task<List<GerenciaDto>> ObtenerActivas()
        {
            var url = $"{_apiUrls.SeguridadUrl}gerencia/obtener-activas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GerenciaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<List<GerenciaDto>> ObtenerTodas()
        {
            var url = $"{_apiUrls.SeguridadUrl}gerencia/obtener-todas";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<List<GerenciaDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
        public async Task<RespuestaConsulta> Insertar(GerenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}gerencia/insertar", content);
            return request.Respuesta(-2, nameof(Insertar));
        }
        public async Task<RespuestaConsulta> Actualizar(GerenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}gerencia/actualizar", content);
            return request.Respuesta(-2, nameof(Actualizar));
        }
        public async Task<RespuestaConsulta> Eliminar(GerenciaDto command)
        {
            var content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            var request = await _httpClient.PostAsync($"{_apiUrls.SeguridadUrl}gerencia/eliminar", content);
            return request.Respuesta(-2, nameof(Eliminar));
        }

        public async Task<GerenciaDto> Obtener(int id)
        {
            var url = $"{_apiUrls.SeguridadUrl}gerencia/obtener?id={id}";
            var request = await _httpClient.GetAsync(url);
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<GerenciaDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }
    }
}
