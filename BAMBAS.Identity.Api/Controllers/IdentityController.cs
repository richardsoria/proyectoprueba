﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MediatR;
using BAMBAS.Identity.Servicio.ControladorEventos.Comandos;

namespace BAMBAS.Identity.Api.Controllers
{
    [ApiController]
    [Route("v1/identity")]
    public class IdentityController : ControllerBase
    {
        private readonly ILogger<IdentityController> _logger;
        private readonly IMediator _mediator;

        public IdentityController(
                    ILogger<IdentityController> logger,
                    IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }
        [HttpGet("test")]
        public IActionResult GetTest()
        {
            return Ok("test");
        }

        [HttpPost("authentication")]
        public async Task<IActionResult> Authentication(ComandoLoguearUsuario command)
        {
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(command);

                if (!result.Succeeded)
                {
                    if (result.CambiarContrasena)
                    {
                        return BadRequest(new { Nombre = result.ErrorMessage, CodEstado = -5 });
                    }
                    return BadRequest(new { Nombre = result.ErrorMessage, CodEstado = -4 });
                }
                return Ok(result);
            }

            return BadRequest();
        }
        [HttpPost("cambio-contrasena")]
        public async Task<IActionResult> CambiarContraseña(ComandoCambiarContrasena command)
        {
            var result = await _mediator.Send(command);
            if (!result.EsSatisfactoria)
                return BadRequest(new { Nombre = result.Nombre, CodEstado = result.CodEstado });
            return Ok(result);
        }
    }
}
