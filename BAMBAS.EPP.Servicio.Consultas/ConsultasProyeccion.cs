﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.Consultas.Objetos;
using BAMBAS.EPP.Modelos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Servicio.Consultas
{
    public interface IConsultasProyeccion
    {
        Task<DataTablesStructs.ReturnedData<ProyeccionModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string dscrpcn, string anio);
        Task<ProyeccionModel> Obtener(int id);
    }
    public class ConsultasProyeccion : IConsultasProyeccion
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasProyeccion(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;    
        }
        public async Task<DataTablesStructs.ReturnedData<ProyeccionModel>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string dscrpcn, string anio)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@DSCRPCN", dscrpcn);
            parametros.Add("@ANIO", anio);
            var lista = await _configuracionConexionSql.EjecutarProcedimiento<ProyeccionModel>(ProcedimientosAlmacenados.Proyeccion.ListarProyeccion, parametros);
            return lista.ConvertirTabla(parameters);
        }
        public async Task<ProyeccionModel> Obtener(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var ret = await _configuracionConexionSql.ObtenerPrimerRegistro<ProyeccionModel>(ProcedimientosAlmacenados.Proyeccion.ObtenerProyeccion, param);
            return ret.Entidad;
        }
    } 
}
