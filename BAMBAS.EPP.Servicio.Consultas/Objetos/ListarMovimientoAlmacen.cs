﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.EPP.Servicio.Consultas.Objetos
{
    public class ListarMovimientoAlmacen
    {
        public int ID { get; set; }
        public string GDTRNO { get; set; }
        public int IDGRDIA { get; set; }
        public int IDRSTER { get; set; }
        public int IDSPRVSR { get; set; }
        public int IDGRPO { get; set; }
        public int IDPRSNA { get; set; }
        public DateTime? FCHTRO { get; set; }
        public DateTime? FINCTRO { get; set; }
        public bool INDTRO { get; set; }
        public string OBSRVCNS { get; set; }
        public int IDCMBGRDIA { get; set; }    //ID CAMBIO DE GUARDIA
        public int IDUAPRBCN { get; set; }  //ID USUARIO APROBACION
        public bool INDBMBS { get; set; }
        public int IDCNTRTSTA { get; set; }  //ID CONTRATISTA
        public int IDGRNCIA { get; set; }
        public int IDSPRINTNDCIA { get; set; }
        public int IDAREA { get; set; }
        public string FCRCN { get; set; }
        public string FEDCN { get; set; }
        public string GDESTDO { get; set; }
    }
}
