﻿using Dapper;
using BAMBAS.CORE.Extensions;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.EPP.Servicio.Consultas.Objetos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAMBAS.EPP.Servicio.Consultas
{
    public interface IConsultasMovimientoAlmacen
    {
        Task<DatosMovimientoAlmacen> ObtenerDatosMovimientoAlmacen(int id);
        Task<List<ListarMovimientoAlmacen>> ListarMovimientoAlmacen(int id);
        Task<DataTablesStructs.ReturnedData<ListarMovimientoAlmacen>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int idgrdia, string finic, string ffin);
    }
    public class ConsultasMovimientoAlmacen : IConsultasMovimientoAlmacen
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ConsultasMovimientoAlmacen(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;    
        }

        public async Task<DatosMovimientoAlmacen> ObtenerDatosMovimientoAlmacen(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            var res = await _configuracionConexionSql.ObtenerPrimerRegistro<DatosMovimientoAlmacen>(ProcedimientosAlmacenados.MovimientoAlmacen.ObtenerDatosMovimientoAlmacen, param);
            return res.Entidad;
        }
        public async Task<List<ListarMovimientoAlmacen>> ListarMovimientoAlmacen(int id)
        {
            var param = new DynamicParameters();
            param.Add("@ID", id);
            return await _configuracionConexionSql.EjecutarProcedimiento<ListarMovimientoAlmacen>(ProcedimientosAlmacenados.MovimientoAlmacen.ListarDatosMovimientoAlmacen, param);
        }

        public async Task<DataTablesStructs.ReturnedData<ListarMovimientoAlmacen>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, int idgrnc, int idsprntndnc, int idarea, int idcntrtsta, int idrster, int numgrdia, string finic, string ffin)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@IDGRNCIA", idgrnc);
            parametros.Add("@IDSPRINTNDCIA", idsprntndnc);
            parametros.Add("@IDAREA", idarea);
            parametros.Add("@IDCNTRTSTA", idcntrtsta);
            parametros.Add("@IDRSTER", idrster);
            parametros.Add("@IDGRDIA", numgrdia);
            parametros.Add("@FINCTRO", idgrnc);
            parametros.Add("@DESDE", finic);
            parametros.Add("@HASTA", ffin);
            var lista = await _configuracionConexionSql.EjecutarProcedimiento<ListarMovimientoAlmacen>(ProcedimientosAlmacenados.MovimientoAlmacen.ListarDatosMovimientoAlmacen, parametros);
            return lista.ConvertirTabla(parameters);
        }
    } 
}
