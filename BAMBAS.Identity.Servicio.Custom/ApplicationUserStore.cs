﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FOSMAR.Identity.Servicio.Custom
{
    using FOSMAR.DATABASE.Configuraciones;
    using Microsoft.AspNet.Identity;
    using Newtonsoft.Json;
    using System.Security.Cryptography;
    using System.Text;

    public class ApplicationUserStore<TUser,TKey> :
        IUserStore<TUser, TKey> where TUser : class, IUser<TKey>
        //IUserPasswordStore<User, string>,
        //IUserLockoutStore<User, string>,
        //IUserTwoFactorStore<User, string>,
        //IUserRoleStore<User, string>,
        //IUserClaimStore<User, string>,
        //IUserLoginStore<User, string>
    {

        protected readonly ConfiguracionConexionSql _configuracion;

        public ApplicationUserStore(ConfiguracionConexionSql configuracion)
        {
            _configuracion = configuracion;
        }

        public async Task CreateAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<TUser> FindByIdAsync(TKey userId)
        {
            throw new NotImplementedException();
        }

        public Task<TUser> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        //#region USER STORE

        //public Task CreateAsync(User user)
        //{
        //    var userId = "";
        //    var users = this.UserDb.TryLoadData();//buscar usuarios 
        //    userId = users == null ? "" : (users.Count + 1).ToString();//asignar id
        //    user.Id = userId;

        //    this.UserDb.Add(user);//agregar
        //    return Task.FromResult(user);
        //}

        //public Task DeleteAsync(User user)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<User> FindByIdAsync(string userId)
        //{
        //    User user = null;
        //    IList<User> users = this.UserDb.TryLoadData();
        //    if (users == null || users.Count == 0)
        //    {
        //        return Task.FromResult(user);
        //    }

        //    user = users.SingleOrDefault(f => f.Id == userId);

        //    return Task.FromResult(user);
        //}

        //public Task<User> FindByNameAsync(string userName)
        //{
        //    User user = null;
        //    IList<User> users = this.UserDb.TryLoadData();
        //    if (users == null || users.Count == 0)
        //    {
        //        return Task.FromResult(user);
        //    }

        //    user = users.SingleOrDefault(f => f.UserName == userName);

        //    return Task.FromResult(user);
        //}

        //public Task UpdateAsync(User user)
        //{
        //    return Task.FromResult(this.UserDb.Update(user));
        //}

        //public void Dispose()
        //{
        //    this.UserDb.FlushToDisk();
        //}

        //#endregion

        //#region PASSWORD STORE

        //public System.Threading.Tasks.Task<string> GetPasswordHashAsync(User user)
        //{
        //    return Task.FromResult(user.PasswordHash);
        //}

        //public System.Threading.Tasks.Task<bool> HasPasswordAsync(User user)
        //{
        //    throw new NotImplementedException();
        //}

        //public System.Threading.Tasks.Task SetPasswordHashAsync(User user, string passwordHash)
        //{
        //    user.PasswordHash = passwordHash;
        //    return Task.FromResult(0);
        //}

        //#endregion

        //#region LOCKOUT STORE

        //public Task<int> GetAccessFailedCountAsync(User user)
        //{
        //    return Task.FromResult(0);
        //}

        //public Task<bool> GetLockoutEnabledAsync(User user)
        //{
        //    return Task.FromResult(user.LockoutEnabled);
        //}

        //public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }
        //    if (!user.LockoutEndDateUtc.HasValue)
        //    {
        //        throw new InvalidOperationException("LockoutEndDate has no value.");
        //    }

        //    return Task.FromResult(new DateTimeOffset(user.LockoutEndDateUtc.Value));
        //}

        //public Task<int> IncrementAccessFailedCountAsync(User user)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task ResetAccessFailedCountAsync(User user)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task SetLockoutEnabledAsync(User user, bool enabled)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion

        //#region TWO FACTOR

        //public Task<bool> GetTwoFactorEnabledAsync(User user)
        //{
        //    return Task.FromResult(user.TwoFactorEnabled);
        //}

        //public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion

        //#region USERS - ROLES STORE

        //public Task AddToRoleAsync(User user, string roleName)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    if (string.IsNullOrEmpty(roleName))
        //    {
        //        throw new ArgumentNullException("role");
        //    }

        //    var roles = RoleDb.TryLoadData();
        //    var role = roles.SingleOrDefault(f => f.Name == roleName);

        //    if (role == null)
        //    {
        //        throw new KeyNotFoundException("role");
        //    }

        //    if (role != null && user.Roles != null && !user.Roles.Contains(roleName, StringComparer.InvariantCultureIgnoreCase))
        //    {
        //        user.Roles.Add(roleName);
        //    }

        //    return Task.FromResult(0);
        //}

        //public Task<IList<string>> GetRolesAsync(User user)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    return Task.FromResult<IList<string>>(user.Roles);
        //}

        //public Task<bool> IsInRoleAsync(User user, string roleName)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    if (string.IsNullOrEmpty(roleName))
        //    {
        //        throw new ArgumentNullException("role");
        //    }

        //    return Task.FromResult(user.Roles.Contains(roleName, StringComparer.InvariantCultureIgnoreCase));
        //}

        //public Task RemoveFromRoleAsync(User user, string roleName)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    user.Roles.Remove(roleName);

        //    return Task.FromResult(0);
        //}

        //#endregion

        //#region USERS - CLAIM STORE

        //public Task AddClaimAsync(User user, System.Security.Claims.Claim claim)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    if (claim == null)
        //    {
        //        throw new ArgumentNullException("claim");
        //    }

        //    if (user.Claims != null && user.Claims.Any(f => f.Value == claim.Value))
        //    {
        //        user.Claims.Add(new UserClaim(claim));
        //    }

        //    return Task.FromResult(0);
        //}

        //public Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(User user)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    return Task.FromResult<IList<System.Security.Claims.Claim>>(user.Claims.Select(clm => new System.Security.Claims.Claim(clm.Type, clm.Value)).ToList());
        //}

        //public Task RemoveClaimAsync(User user, System.Security.Claims.Claim claim)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    if (claim == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    user.Claims.Remove(new UserClaim(claim));

        //    return Task.FromResult(0);
        //}

        //#endregion

        //#region USER - LOGINS

        //public Task AddLoginAsync(User user, UserLoginInfo login)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }
        //    if (login == null)
        //    {
        //        throw new ArgumentNullException("user");
        //    }

        //    if (!user.Logins.Any(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey))
        //    {
        //        user.Logins.Add(new UserLoginInfo(login.LoginProvider, login.ProviderKey));
        //        UserDb.Update(user);
        //    }

        //    return Task.FromResult(true);
        //}

        //public Task<User> FindAsync(UserLoginInfo login)
        //{
        //    var loginId = GetLoginId(login);
        //    var user = UserDb.TryLoadData().SingleOrDefault(f => f.Id == loginId);
        //    return Task.FromResult(user);
        //}

        //public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task RemoveLoginAsync(User user, UserLoginInfo login)
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion

        //private string GetLoginId(UserLoginInfo login)
        //{
        //    using (var sha = new SHA1CryptoServiceProvider())
        //    {
        //        var clearBytes = Encoding.UTF8.GetBytes(login.LoginProvider + "|" + login.ProviderKey);
        //        var hashBytes = sha.ComputeHash(clearBytes);
        //        return ToHex(hashBytes);
        //    }
        //}

        //private static string ToHex(byte[] bytes)
        //{
        //    var sb = new StringBuilder(bytes.Length * 2);
        //    for (int i = 0; i < bytes.Length; i++)
        //        sb.Append(bytes[i].ToString("x2"));
        //    return sb.ToString();
        //}
    }
}
