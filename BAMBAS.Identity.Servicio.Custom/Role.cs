﻿using System;

namespace FOSMAR.Identity.Servicio.Custom
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNetCore.Identity;

    public class Role: IRole<int>
    {
        public Role()
        {

        }

        public Role(string name)
        {
            this.Name = name;
        }

        public Role(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
