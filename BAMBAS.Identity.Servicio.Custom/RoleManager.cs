﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FOSMAR.Identity.Servicio.Custom
{
    using Microsoft.AspNet.Identity;

    public class RoleManager : RoleManager<Role,int>
    {
        public RoleManager(RoleStore store): base(store)
        {
            this.RoleValidator = new RoleValidator<Role,int>(this);
        }
    }
}
