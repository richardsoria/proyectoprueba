﻿using BAMBAS.CORE.Structs;
using Sunat.Servicios;
using SunatServ;
using System.Threading.Tasks;

namespace BAMBAS.Negocios.Sunat
{
    public interface ISunatProxy
    {
        Task<response> Consultar(string _nuDocumento);
        Task<DataTablesStructs.ReturnedData<consultarNumRucResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string numDocumento);

    }
    public class SunatProxy : ISunatProxy
    {
        private readonly ISunatSoap _sunatSoap;
        public SunatProxy(
           ISunatSoap sunatSoap)
        {
            _sunatSoap = sunatSoap;
        }
        public async Task<response> Consultar(string _nuDocumento)
        {
            return await _sunatSoap.Consultar(_nuDocumento);
        }

        public async Task<DataTablesStructs.ReturnedData<consultarNumRucResponse>> ObtenerDataTable(DataTablesStructs.SentParameters parameters, string _nuDocumento)
        {
            return await _sunatSoap.ObtenerDataTable(parameters, _nuDocumento);

            //var url = $"{_apiGatewayUrl}sunat/lista-sunat?numDocumento={_nuDocumento}";
            //var request = await _httpClient.GetAsync(url);
            //request.EnsureSuccessStatusCode();

            //return JsonSerializer.Deserialize<DataTablesStructs.ReturnedData<response>>(
            //    await request.Content.ReadAsStringAsync(),
            //    new JsonSerializerOptions
            //    {
            //        PropertyNameCaseInsensitive = true
            //    }
            //);
        }
    }
}
