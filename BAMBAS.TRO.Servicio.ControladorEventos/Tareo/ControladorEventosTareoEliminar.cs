﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo;
using MediatR;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Tareo
{
    public class ControladorEventosTareoEliminar : IRequestHandler<ComandoTareoEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTareoEliminar(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTareoEliminar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", entidad.ID);
            param.Add("@UEDCN", entidad.UEDCN);
            param.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Tareo.EliminarTareo, "RETORNO", param);
        }
    }
}
