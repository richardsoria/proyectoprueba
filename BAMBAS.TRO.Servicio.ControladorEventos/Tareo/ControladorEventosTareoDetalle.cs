﻿using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo;
using Dapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Tareo
{
    public class ControladorEventosTareoDetalle : IRequestHandler<ComandoTareoDetalleInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTareoDetalle(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTareoDetalleInsertar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@IDTRO", entidad.IDTRO);
            param.Add("@CSAP", entidad.CSAP);
            param.Add("@GDESTDO", entidad.GDESTDO);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Tareo.InsertarTareoDetalle, "ID", param);
        }
    }
}
