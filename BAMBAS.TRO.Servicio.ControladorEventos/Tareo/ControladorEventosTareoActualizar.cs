﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Tareo
{
    public class ControladorEventosTareoActualizar : IRequestHandler<ComandoTareoActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTareoActualizar(ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<RespuestaConsulta> Handle(ComandoTareoActualizar entidad, CancellationToken cancellationToken)
        {
            var param = new DynamicParameters();
            param.Add("@ID", entidad.ID);
            param.Add("@IDGRNCA", entidad.IDGRNCA);
            param.Add("@IDSPRINTNDNCA", entidad.IDSPRINTNDNCA);
            param.Add("@IDARA", entidad.IDARA);
            param.Add("@IDGRDIA", entidad.IDGRDIA);
            param.Add("@FINCTRO", ConvertHelpers.DatepickerToDatetime(entidad.FINCTRO)?.ToString("yyyy/MM/dd"));
            param.Add("@FFINTRO", ConvertHelpers.DatepickerToDatetime(entidad.FFINTRO)?.ToString("yyyy/MM/dd"));
            param.Add("@INDBMBS", entidad.INDBMBS);
            param.Add("@IDCNTRTSTA", entidad.IDCNTRTSTA);
            param.Add("@GDESTDO", entidad.GDESTDO);
            param.Add("@UCRCN", entidad.UCRCN);
            param.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Tareo.ActualizarTareo, "ID", param);
        }
    }
}
