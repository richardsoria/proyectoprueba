﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo
{
    public class ComandoTareoEliminar : IRequest<RespuestaConsulta>
    {
        public int ID { get; set; }
        public string UEDCN { get; set; }
    }
}
