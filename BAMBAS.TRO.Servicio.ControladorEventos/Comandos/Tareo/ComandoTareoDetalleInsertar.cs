﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo
{
    public class ComandoTareoDetalleInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDTRO { get; set; }
        public string CSAP { get; set; }
    }
}
