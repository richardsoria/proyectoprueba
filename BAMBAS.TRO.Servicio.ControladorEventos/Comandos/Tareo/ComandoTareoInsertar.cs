﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.TRO.Servicio.ControladorEventos.Comandos.Tareo
{
    public class ComandoTareoInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public int IDARA { get; set; }
        public int IDGRDIA { get; set; }
        public string FINCTRO { get; set; }
        public string FFINTRO { get; set; }
        public int IDSPRVSR { get; set; }
        public int IDUAPRBCN { get; set; }
        public bool INDBMBS { get; set; }
        public int? IDCNTRTSTA { get; set; }
    }
}
