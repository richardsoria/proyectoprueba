﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 04
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP04")]
    public class GrupoDatoEmpresaModel : EntidadAuditoria
    {
        public string CGDTO { get; set; }
        public string DGDTO { get; set; }
        public string AGDTO { get; set; }
    }
}
