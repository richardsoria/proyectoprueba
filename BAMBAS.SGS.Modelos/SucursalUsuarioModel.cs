﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 10
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP10")]
    public class SucursalUsuarioModel : EntidadAuditoria
    {
        public int IDUSRO { get; set; }
        public int IDSCRSL { get; set; }
        public DateTime FIVGNCA { get; set; }
        public DateTime FFVGNCA { get; set; }
    }
}
