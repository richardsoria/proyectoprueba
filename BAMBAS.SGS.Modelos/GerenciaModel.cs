﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP14")]
    public class GerenciaModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
    }
}
