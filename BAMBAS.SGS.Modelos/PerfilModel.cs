﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 09
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP09")]
    public class PerfilModel : EntidadAuditoria
    {
        public string DPRFL { get; set; }
    }
}
