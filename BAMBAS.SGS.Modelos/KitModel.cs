﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;
using System.Collections.Generic;
using System.Text;
namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP02")]
    public class KitModel : EntidadAuditoria
    {
        public string KITS { get; set; }
        public string DSCRPCN { get; set; }
        public string IDFMLA { get; set; }
        public string CSAP { get; set; }
        public decimal CSTO { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string GDTMNDA { get; set; }
        public int CNTDD { get; set; }
        public string IDARTCLO { get; set; }
        public string IDKITDTLLE { get; set; }
    }
}
