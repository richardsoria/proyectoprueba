﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 05
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP05")]
    public class GrupoDatoDetalleEmpresaModel : EntidadAuditoria
    {
        public string CGDTO { get; set; }
        public string DGDDTLLE { get; set; }
        public string AGDDTLLE { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
        public string VLR3 { get; set; }
    }
}
