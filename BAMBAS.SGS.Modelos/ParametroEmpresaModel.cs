﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 03
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP03")]
    public class ParametroEmpresaModel : EntidadAuditoria
    {
        public string DPRMTRO { get; set; }
        public string APRMTRO { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }
    }
}
