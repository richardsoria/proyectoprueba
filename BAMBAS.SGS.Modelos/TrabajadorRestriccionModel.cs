﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 05
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP09")]
    public class TrabajadorRestriccionModel : EntidadAuditoria
    {
        public int IDARTCLO { get; set; }
        public int IDTRBJDR { get; set; }
        public string OBSRVCN { get; set; }
        public string GDUNDDMDDA { get; set; }

    }
}
