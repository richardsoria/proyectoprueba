﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 16
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP16")]
    public class AreaModel : EntidadAuditoria
    {
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public string DSCRPCN { get; set; }
    }
}
