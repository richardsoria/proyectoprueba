﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 05
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP15")]
    public class SuperintendenciaModel : EntidadAuditoria
    {
        public int IDGRNCA { get; set; }
        public string DSCRPCN { get; set; }
    }
}
