﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP03")]
    public class AlmacenModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public string GDTALMCN { get; set; }
        public int? IDTRBJDR { get; set; }
    }
}
