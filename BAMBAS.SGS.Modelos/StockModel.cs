﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP05")]
    public class StockModel : EntidadAuditoria
    {
        public int STCK { get; set; }
        public int STCKMNMO { get; set; }
        public int IDALMCN { get; set; }
        public int IDARTCLO { get; set; }
    }
}
