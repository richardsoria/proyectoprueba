﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP17")]
    public class RosterModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public int DTRBJDS { get; set; }
        public int DDSCNSO { get; set; }
    }
}
