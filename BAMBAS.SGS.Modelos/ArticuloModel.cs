﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP01")]
    public class ArticuloModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public int IDFMLA { get; set; }
        public int IDSBFMLA { get; set; }        
        public string CBRRA { get; set; }
        public string CSAP { get; set; }
        public string GDUNDDMDDA { get; set; }
        public string GDTMNDA { get; set; }
        public int DRCN { get; set; }
        public decimal CSTO { get; set; }        
        public bool FKIT { get; set; }
        public bool FMNJOTLLS { get; set; }
        public bool FMNJORSTRCN { get; set; }
        public string MNSJE { get; set; }
    }
}
