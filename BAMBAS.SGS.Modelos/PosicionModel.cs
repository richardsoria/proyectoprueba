﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA TBR 00
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGTRB00")]
    public class PosicionModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public string DSCRPCNINGLS { get; set; }
        public string CSAP { get; set; }
    }
}
