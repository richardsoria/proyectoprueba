﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 20
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP20")]
    public class GuardiaModel : EntidadAuditoria
    {
        public int IDGRDA { get; set; }
        public string DSCRPCN { get; set; }
        public int IDRSTR { get; set; }
        public string RSTR { get; set; }
        public int? DTRBJDS { get; set; }
        public int? DDSCNSO { get; set; }
        public string FINCO { get; set; }
        public string FFN { get; set; }
        public string GDTRNO { get; set; }
        public string TRNO { get; set; }
        public string HRAINCODA { get; set; }
        public string HRAINCONCHE { get; set; }
        public string HRAFNDA { get; set; }
        public string HRAFNNCHE { get; set; }
        public int DSGRDA { get; set; }
        public int NCHSGRDA { get; set; }
        public int DSDSCNSO { get; set; }
    }
}
