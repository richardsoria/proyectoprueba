﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP18")]
    public class GrupoCargoModel : EntidadAuditoria
    {
        public int IDGRPOCRGO { get; set; }
        public string DSCRPCN { get; set; }
        public string ARTCLS { get; set; }
        public string CSAP { get; set; }
        public int CNTDD { get; set; }
        public string GDUNDDMDDA { get; set; }
        public decimal CSTO { get; set; }
        public int DRCN { get; set; }
        public string IDARTCLO { get; set; }
        public string IDEPP { get; set; }

    }
}
