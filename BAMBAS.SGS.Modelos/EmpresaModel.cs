﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 00
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP00")]
    public class EmpresaModel : EntidadAuditoria
    {
        public string NEMPRSA { get; set; }
        public string RUC { get; set; }
        public string UBGO { get; set; }
        public string DRCCN { get; set; }
    }
}
