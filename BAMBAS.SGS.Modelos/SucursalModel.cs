﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 01
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP01")]
    public class SucursalModel : EntidadAuditoria
    {
        public string UBGO { get; set; }
        public string DSCRSL { get; set; }
        public string NSCRSL { get; set; }

        public int IDEMPRSA { get; set; }
    }
}
