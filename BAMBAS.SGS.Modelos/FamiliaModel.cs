﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP00")]
    public class FamiliaModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public int? IDFMLAPDRE { get; set; }
    }
}
