﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 05
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEPP06")]
    public class TrabajadorTallaModel : EntidadAuditoria
    {
        public int IDARTCLO { get; set; }
        public int IDTRBJDR { get; set; }
        public string TLLA { get; set; }
        public string GDUNDDMDDA { get; set; }
    }
}
