﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 08
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP08")]
    public class UsuarioModel : EntidadAuditoria
    {
        public string IDPRSNA { get; set; }
        public string CUSRO { get; set; }
        public string NYAPLLDS { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NTLFNO { get; set; }
        public string CLVE { get; set; }
        public string FVCLVE { get; set; }
        public bool FBLQUO { get; set; }
        public int INTNTS { get; set; }
        public bool FRZRCMBOCLVE { get; set; }
    }
}
