﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 06
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP06")]
    public class ObjetoPadreModel : EntidadAuditoria
    {
        public int IDMDLO { get; set; }
        public string MDLO { get; set; }
        public string NOBJTO { get; set; }
        public int FORDN { get; set; }
        public string IDOBJTOPDRE { get; set; }
        public string DOBJTO { get; set; }
        public string URL { get; set; }
    }
}
