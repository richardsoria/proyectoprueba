﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 12
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP12")]
    public class PerfilObjetoModel : EntidadAuditoria
    {
        public int IDPRFL { get; set; }
        public int IDOBJTO { get; set; }
        public string PINSRT { get; set; }
        public string PUPDTE { get; set; }
        public string PSELCT { get; set; }
        public string PDELTE { get; set; }
    }
}
