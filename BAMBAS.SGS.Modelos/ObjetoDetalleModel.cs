﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP07")]
    public class ObjetoDetalleModel : EntidadAuditoria
    {
        public int IDOBJTO { get; set; }
        public string NOBJTO { get; set; }
        public string DOBJTO { get; set; }
    }
}
