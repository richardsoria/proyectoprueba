﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 11
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP11")]
    public class PerfilUsuarioModel : EntidadAuditoria
    {
        public int IDUSRO { get; set; }
        public int IDPRFL { get; set; }
    }
}
