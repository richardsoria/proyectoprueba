﻿using BAMBAS.ENTIDADES;
using BAMBAS.ENTIDADES.Modelos.Auditoria;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 14
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP19")]
    public class CargoModel : EntidadAuditoria
    {
        public string DSCRPCN { get; set; }
        public int IDGRPO { get; set; }
    }
}
