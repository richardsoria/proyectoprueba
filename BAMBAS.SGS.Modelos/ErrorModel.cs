﻿using BAMBAS.CORE.Extensions;
using BAMBAS.ENTIDADES;
using System;

namespace BAMBAS.SGS.Modelos
{
    /// <summary>
    /// SGA EMP 02
    /// </summary>
    [BaseDatos(esquema = "SEG", nombreTabla = "MSGEMP02")]
    public class ErrorModel
    {
        public int ID { get; set; }
        public string UNME { get; set; }
        public string EESQMA { get; set; }
        public string ENMBR { get; set; }
        public string ESVRT { get; set; }
        public string ESTTE { get; set; }
        public string EPRCDRE { get; set; }
        public string ERRORLNE { get; set; }
        public string EMSSGE { get; set; }
        public string EDTME { get; set; }
        public string UCRCN { get; set; }
    }
}
