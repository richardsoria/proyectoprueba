﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Persona;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Persona
{
    public class ControladorEventosPersonaActualizar : IRequestHandler<ComandoPersonaActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosPersonaActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoPersonaActualizar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@GDTPRSNA", entidad.GDTPRSNA);
            parametros.Add("@FCNTRTSTA", entidad.FCNTRTSTA);
            parametros.Add("@RSCL", entidad.RSCL);
            parametros.Add("@APTRNO", entidad.APTRNO);
            parametros.Add("@AMTRNO", entidad.AMTRNO);
            parametros.Add("@ACSDA", entidad.ACSDA);
            parametros.Add("@PNMBRE", entidad.PNMBRE);
            parametros.Add("@SNMBRE", entidad.SNMBRE);
            parametros.Add("@NCNLDD", entidad.NCNLDD);
            parametros.Add("@CPNCMNTO", entidad.CPNCMNTO);
            parametros.Add("@GDECVL", entidad.GDECVL);
            parametros.Add("@GDSXO", entidad.GDSXO);
            parametros.Add("@FNCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FNCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FFLLCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FFLLCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@CUBGEO", entidad.CUBGEO);
            parametros.Add("@FTO", entidad.FTO);
            parametros.Add("@GDTESCDD", entidad.GDTESCDD);
            parametros.Add("@GDTETMNO", entidad.GDTETMNO);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Persona.ActualizarPersona, "RETORNO", parametros);
        }
    }
}
