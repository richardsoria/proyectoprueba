﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.CORE.Structs;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Persona;
using MediatR;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Persona
{
    public class ControladorEventosPersonaInsertar : IRequestHandler<ComandoPersonaInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosPersonaInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoPersonaInsertar entidad, CancellationToken cancellationToken)
        {

            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parametros.Add("@GDTPRSNA", entidad.GDTPRSNA);
            parametros.Add("@FCNTRTSTA", entidad.FCNTRTSTA);
            parametros.Add("@GDDCMNTO", entidad.GDDCMNTO);
            parametros.Add("@NDCMNTO", entidad.NDCMNTO);
            parametros.Add("@RSCL", entidad.RSCL);
            parametros.Add("@APTRNO", entidad.APTRNO);
            parametros.Add("@AMTRNO", entidad.AMTRNO);
            parametros.Add("@ACSDA", entidad.ACSDA);
            parametros.Add("@PNMBRE", entidad.PNMBRE);
            parametros.Add("@SNMBRE", entidad.SNMBRE);
            parametros.Add("@NCNLDD", entidad.NCNLDD);
            parametros.Add("@CPNCMNTO", entidad.CPNCMNTO);
            parametros.Add("@GDECVL", entidad.GDECVL);
            parametros.Add("@GDSXO", entidad.GDSXO);
            parametros.Add("@FNCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FNCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FFLLCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FFLLCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@CUBGEO", entidad.CUBGEO);
            parametros.Add("@GDTESCDD", entidad.GDTESCDD);
            parametros.Add("@GDTETMNO", entidad.GDTETMNO);
            parametros.Add("@FTO", entidad.FTO);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            parametros.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Persona.InsertarPersona, "ID", parametros);
        }
    }
}
