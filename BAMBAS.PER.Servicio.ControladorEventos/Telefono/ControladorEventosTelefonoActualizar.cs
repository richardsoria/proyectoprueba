﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Telefono;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Telefono
{
    public class ControladorEventosTelefonoActualizar : IRequestHandler<ComandoTelefonoActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTelefonoActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTelefonoActualizar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@GDTTLFNO", entidad.GDTTLFNO);
            parametros.Add("@CUBGEO", entidad.CUBGEO);
            parametros.Add("@NTLFNO", entidad.NTLFNO);
            parametros.Add("@OBSRVCN", entidad.OBSRVCN);
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Telefono.ActualizarTelefono, "RETORNO", parametros);
        }
    }
}
