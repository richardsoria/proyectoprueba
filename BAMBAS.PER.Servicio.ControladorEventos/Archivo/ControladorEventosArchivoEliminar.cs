﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Archivo;
using MediatR;
using System.Data;
using System.Threading;
using BAMBAS.CORE.Structs;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Archivo
{
    public class ControladorEventosArchivoEliminar : IRequestHandler<ComandoArchivoEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosArchivoEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<RespuestaConsulta> Handle(ComandoArchivoEliminar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Archivo.EliminarArchivo, "RETORNO", parametros);
        }
    }
}
