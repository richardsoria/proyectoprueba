﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Archivo;
using MediatR;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;
using System;
using System.IO;

namespace BAMBAS.PER.Servicio.ControladorEventos.Archivo
{
    public class ControladorEventosArchivoInsertar : IRequestHandler<ComandoArchivoInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosArchivoInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }
        public async Task<RespuestaConsulta> Handle(ComandoArchivoInsertar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@NMBRARCHV", entidad.NMBRARCHV);
            parametros.Add("@TPOARCHV", entidad.TPOARCHV);
            parametros.Add("@PATHARCHV", entidad.PATHARCHV);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            parametros.Add("@UEDCN", entidad.UEDCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Archivo.InsertarArchivo, "ID", parametros);
        }
    }
}
