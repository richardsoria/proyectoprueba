﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Direccion;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Direccion
{
    public class ControladorEventosDireccionInsertar : IRequestHandler<ComandoDireccionInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosDireccionInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoDireccionInsertar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parametros.Add("@GDTDRCCN", entidad.GDTDRCCN);
            parametros.Add("@GDTVIA", entidad.GDTVIA);
            parametros.Add("@VIA", entidad.VIA);
            parametros.Add("@NVIA", entidad.NVIA);
            parametros.Add("@NINTRR", entidad.NINTRR);
            parametros.Add("@GDTDZNA", entidad.GDTDZNA);
            parametros.Add("@ZNA", entidad.ZNA);
            parametros.Add("@RFRNCIA", entidad.RFRNCIA);
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@CUBGEO", entidad.CUBGEO);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Direccion.InsertarDireccion, "ID", parametros);
        }
    }
}
