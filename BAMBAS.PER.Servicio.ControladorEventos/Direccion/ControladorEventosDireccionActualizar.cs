﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Direccion;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using BAMBAS.CORE.Structs;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Direccion
{
    public class ControladorEventosDireccionActualizar : IRequestHandler<ComandoDireccionActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosDireccionActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoDireccionActualizar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@GDTDRCCN", entidad.GDTDRCCN);
            parametros.Add("@GDTVIA", entidad.GDTVIA);
            parametros.Add("@VIA", entidad.VIA);
            parametros.Add("@NVIA", entidad.NVIA);
            parametros.Add("@NINTRR", entidad.NINTRR);
            parametros.Add("@GDTDZNA", entidad.GDTDZNA);
            parametros.Add("@ZNA", entidad.ZNA);
            parametros.Add("@RFRNCIA", entidad.RFRNCIA);
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@CUBGEO", entidad.CUBGEO);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Direccion.ActualizarDireccion, "RETORNO", parametros);
        }
    }
}
