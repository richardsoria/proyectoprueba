﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Correo;
using MediatR;
using System;
using BAMBAS.CORE.Structs;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Correo
{
    public class ControladorEventosCorreoEliminar : IRequestHandler<ComandoCorreoEliminar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosCorreoEliminar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoCorreoEliminar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Correo.EliminarCorreo, "RETORNO", parametros);       
        }
    }
}
