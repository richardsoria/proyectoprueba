﻿using Dapper;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Correo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using BAMBAS.CORE.Structs;
using System.Threading;
using System.Threading.Tasks;

namespace BAMBAS.PER.Servicio.ControladorEventos.Correo
{
    public class ControladorEventosCorreoInsertar : IRequestHandler<ComandoCorreoInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosCorreoInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoCorreoInsertar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parametros.Add("@GDTCRREO", entidad.GDTCRREO);
            parametros.Add("@CCRREO", entidad.CCRREO);
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Correo.InsertarCorreo, "ID", parametros);
        }
    }
}
