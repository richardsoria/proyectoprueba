﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Trabajador;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Trabajador
{
    public class ControladorEventosTrabajadorInsertar : IRequestHandler<ComandoTrabajadorInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosTrabajadorInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoTrabajadorInsertar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);

            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@CSAP", entidad.CSAP);
            parametros.Add("@GDTTRBJDR", entidad.GDTTRBJDR);
            parametros.Add("@IDCNTRTSTA", entidad.IDCNTRTSTA);
            parametros.Add("@FINGRSO", ConvertHelpers.DatepickerToDatetime(entidad.FINGRSO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FBJA", ConvertHelpers.DatepickerToDatetime(entidad.FBJA)?.ToString("yyyy/MM/dd"));
            parametros.Add("@MBJA", entidad.MBJA);
            parametros.Add("@GDSDE", entidad.GDSDE);
            parametros.Add("@IDGRNCA", entidad.IDGRNCA);
            parametros.Add("@IDSPRINTNDNCA", entidad.IDSPRINTNDNCA);
            parametros.Add("@GDDLGCNATRZDA", entidad.GDDLGCNATRZDA);
            parametros.Add("@IDARA", entidad.IDARA);
            parametros.Add("@IDGRPO", entidad.IDGRPO);
            parametros.Add("@IDCRGO", entidad.IDCRGO);
            parametros.Add("@IDPSCN", entidad.IDPSCN);
            parametros.Add("@IDCNTROCSTO", entidad.IDCNTROCSTO);
            parametros.Add("@IDUNDDORGNZCNL", entidad.IDUNDDORGNZCNL);
            parametros.Add("@GDGRPOPS", entidad.GDGRPOPS);
            parametros.Add("@GDNVL", entidad.GDNVL);
            parametros.Add("@GDPRFLTRBJDR", entidad.GDPRFLTRBJDR);
            parametros.Add("@FTRBJORMTO", entidad.FTRBJORMTO == 2 ? 0 : entidad.FTRBJORMTO);
            parametros.Add("@GDTGRDA", entidad.GDTGRDA);
            parametros.Add("@FTITLR", entidad.FTITLR == 2 ? 0 : entidad.FTITLR);
            parametros.Add("@GDLGRALJMNTO", entidad.GDLGRALJMNTO);
            parametros.Add("@GDUBCCNALJMNTO", entidad.GDUBCCNALJMNTO);
            parametros.Add("@GDTALJMNTO", entidad.GDTALJMNTO);
            parametros.Add("@MDLO", entidad.MDLO);
            parametros.Add("@PSO", entidad.PSO);
            parametros.Add("@CMA", entidad.CMA);
            //
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Trabajador.InsertarTrabajador, "ID", parametros);
        }
    }
}
