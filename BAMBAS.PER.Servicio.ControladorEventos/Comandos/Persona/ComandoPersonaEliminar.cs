﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Persona
{
    public class ComandoPersonaEliminar : IRequest<RespuestaConsulta>
    {
       public int ID { get; set; }
       public string UEDCN { get; set; }
    }
}
