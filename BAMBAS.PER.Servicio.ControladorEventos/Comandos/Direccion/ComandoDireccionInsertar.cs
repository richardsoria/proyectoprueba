﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Direccion
{
    public class ComandoDireccionInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string GDTDRCCN { get; set; }
        public string GDTVIA { get; set; }
        public string VIA { get; set; }
        public string NVIA { get; set; }
        public string NINTRR { get; set; }
        public string GDTDZNA { get; set; }
        public string ZNA { get; set; }
        public string RFRNCIA { get; set; }
        public string FPRNCPL { get; set; }
        public string CUBGEO { get; set; }
        public string IDPRSNA { get; set; }
    }
}
