﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Telefono
{
    public class ComandoTelefonoInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string GDTTLFNO { get; set; }
        public string CUBGEO { get; set; }
        public string NTLFNO { get; set; }
        public string OBSRVCN { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
