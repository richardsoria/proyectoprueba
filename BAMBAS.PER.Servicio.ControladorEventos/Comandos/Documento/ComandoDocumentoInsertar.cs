﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Documento
{
    public class ComandoDocumentoInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
		public string GDDCMNTO { get; set; }
		public string NDCMNTO { get; set; }
		public string FINSCRPCN { get; set; }
		public string FVNCMNTO { get; set; }
		public string FPRNCPL { get; set; }
		public string IDPRSNA { get; set; }
	}
}
