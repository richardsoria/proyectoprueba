﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Archivo
{
    public class ComandoArchivoEliminar : IRequest<RespuestaConsulta>
    {
        public int ID { get; set; }
        public string UEDCN { get; set; }
    }
}
