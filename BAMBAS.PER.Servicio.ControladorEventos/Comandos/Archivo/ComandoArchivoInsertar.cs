﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Archivo
{
    public class ComandoArchivoInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string PATHARCHV { get; set; }     
        public string NMBRARCHV { get; set; }
        public string TPOARCHV { get; set; }
        public string FPRNCPL { get; set; }
        public int IDPRSNA { get; set; }
    }
}
