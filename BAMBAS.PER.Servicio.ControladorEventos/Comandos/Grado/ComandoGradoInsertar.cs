﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Grado
{
    public class ComandoGradoInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string GDSMLTR { get; set; }
        public string GDTGRDO { get; set; }
        public string GDTRLCN { get; set; }
        public string FINGRESO { get; set; }
        public string FREINGRESO { get; set; }
        public string FDISPON { get; set; }
        public string FTDISPON { get; set; }
        public string FBAJA { get; set; }
        public string GDCBJA { get; set; }
        public string GDESPCLDD { get; set; }
        public string GDTAFCCN { get; set; }
        public string GDTPRSNL { get; set; }
        public string GDMTVDSP { get; set; }
        public string FECRTFCDO { get; set; }
        public string NCRTFCDO { get; set; }
        public string DIAGCIE { get; set; }
        public string GDINTRDCTO { get; set; }
        public string GDPNCNBLE { get; set; }
        public string GDTUPGO { get; set; }
        public bool AFLBLE { get; set; }
        public string IDPRSNA { get; set; }
    }
}
