﻿using BAMBAS.CORE.Structs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Correo
{
    public class ComandoCorreoInsertar :ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string GDTCRREO { get; set; }
        public string CCRREO { get; set; }
        public string FPRNCPL { get; set; }
        public string IDPRSNA { get; set; }
    }
}
