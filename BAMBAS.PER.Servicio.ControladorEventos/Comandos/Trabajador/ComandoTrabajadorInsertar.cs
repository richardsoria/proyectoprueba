﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using BAMBAS.CORE.Structs;
namespace BAMBAS.PER.Servicio.ControladorEventos.Comandos.Trabajador
{
    public class ComandoTrabajadorInsertar : ComandoAuditoria, IRequest<RespuestaConsulta>
    {
        public string IDPRSNA { get; set; }
        public string CSAP { get; set; }
        public string GDTTRBJDR { get; set; }
        public int? IDCNTRTSTA { get; set; }
        public string FINGRSO { get; set; }
        public string FBJA { get; set; }
        public string MBJA { get; set; }
        public string GDSDE { get; set; }
        public int IDGRNCA { get; set; }
        public int IDSPRINTNDNCA { get; set; }
        public int IDARA { get; set; }
        public int IDGRPO { get; set; }
        public int IDCRGO { get; set; }
        public int IDPSCN { get; set; }
        public int IDCNTROCSTO { get; set; }
        public int IDUNDDORGNZCNL { get; set; }
        public string GDGRPOPS { get; set; }
        public string GDNVL { get; set; }
        public string GDDLGCNATRZDA { get; set; }
        public string GDPRFLTRBJDR { get; set; }
        public string[] GDPRFLSTRBJDR { get; set; }
        public int FTRBJORMTO { get; set; }
        public string GDTGRDA { get; set; }
        public int FTITLR { get; set; }
        public string GDLGRALJMNTO { get; set; }
        public string GDUBCCNALJMNTO { get; set; }
        public string GDTALJMNTO { get; set; }
        public string MDLO { get; set; }
        public string PSO { get; set; }
        public string CMA { get; set; }

        //
        public string NMBRS { get; set; }
        public string GDTPRSNA { get; set; }
        public string CNTRTSTA { get; set; }

    }
}
