﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Documento;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Documento
{
    public class ControladorEventosDocumentoInsertar : IRequestHandler<ComandoDocumentoInsertar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosDocumentoInsertar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoDocumentoInsertar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parametros.Add("@GDDCMNTO", entidad.GDDCMNTO);
            parametros.Add("@NDCMNTO", entidad.NDCMNTO);
            parametros.Add("@FINSCRPCN", ConvertHelpers.DatepickerToDatetime(entidad.FINSCRPCN)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FVNCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FVNCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UCRCN", entidad.UCRCN);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Documento.InsertarDocumento, "ID", parametros);
        }
    }
}
