﻿using Dapper;
using BAMBAS.CORE.Helpers;
using BAMBAS.DATABASE;
using BAMBAS.DATABASE.Configuraciones;
using BAMBAS.DATABASE.Helpers;
using BAMBAS.PER.Servicio.ControladorEventos.Comandos.Documento;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BAMBAS.CORE.Structs;

namespace BAMBAS.PER.Servicio.ControladorEventos.Documento
{
    public class ControladorEventosDocumentoActualizar : IRequestHandler<ComandoDocumentoActualizar, RespuestaConsulta>
    {
        private readonly ConfiguracionConexionBD _configuracionConexionSql;

        public ControladorEventosDocumentoActualizar(
            ConfiguracionConexionBD configuracionConexionSql)
        {
            _configuracionConexionSql = configuracionConexionSql;
        }

        public async Task<RespuestaConsulta> Handle(ComandoDocumentoActualizar entidad, CancellationToken cancellationToken)
        {
            var parametros = new DynamicParameters();
            parametros.Add("@ID", entidad.ID);
            parametros.Add("@GDDCMNTO", entidad.GDDCMNTO);
            parametros.Add("@NDCMNTO", entidad.NDCMNTO);
            parametros.Add("@FINSCRPCN", ConvertHelpers.DatepickerToDatetime(entidad.FINSCRPCN)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FVNCMNTO", ConvertHelpers.DatepickerToDatetime(entidad.FVNCMNTO)?.ToString("yyyy/MM/dd"));
            parametros.Add("@FPRNCPL", entidad.FPRNCPL);
            parametros.Add("@GDESTDO", entidad.GDESTDO);
            parametros.Add("@UEDCN", entidad.UEDCN);
            parametros.Add("@IDPRSNA", entidad.IDPRSNA);
            parametros.Add("@RETORNO", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return await _configuracionConexionSql.EjecutarProcedimiento(ProcedimientosAlmacenados.Documento.ActualizarDocumento, "RETORNO", parametros);
        }
    }
}
