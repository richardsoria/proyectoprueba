﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.DATABASE.Helpers
{
    public static class ProcedimientosAlmacenados
    {
        public static class Usuario
        {
            public const string ObtenerUsuario = "SEG.USP_OMSGEMP08_ObtenerUsuario";
            public const string ObtenerUsuarioPorId = "SEG.USP_OMSGEMP08_ObtenerUsuarioXid";
            public const string InsertarUsuario = "SEG.USP_IMSGEMP08_InsertarUsuario";
            public const string ObtenerTodas = "SEG.USP_LMSGEMP08_ListarTodosUsuarios";
            public const string EditarUsuario = "SEG.USP_UMSGEMP08_EditarUsuario";
            public const string EliminarUsuario = "SEG.USP_DMSGEMP08_EliminarUsuario";
            public const string EditarContrasena = "SEG.USP_UMSGEMP08_EditarContrasena";
            public const string SumarIntentoBloqueo = "SEG.USP_UMSGEMP08_SumarIntentoBloqueo";
            public const string ActualizarPersonaUsuario = "SEG.USP_UMSGEMP08_ActualizarPersonaUsuario";
        }
        public static class GrupoDatoEmpresa
        {
            public const string ObtenerTodos = "SEG.USP_LMSGEMP04_ListarTodosGrupoDatoEmpresa";
            public const string ObtenerActivos = "SEG.USP_LMSGEMP04_ListarActivosGrupoDatoEmpresa";
            public const string InsertarGrupoDato = "SEG.USP_IMSGEMP04_InsertarGrupoDato";
            public const string EditarGrupoDato = "SEG.USP_UMSGEMP04_EditarGrupoDato";
            public const string ObtenerGrupoDato = "SEG.USP_OMSGEMP04_ObtenerGrupoDato";
            public const string EliminarGrupoDato = "SEG.USP_DMSGEMP04_EliminarGrupoDato";
        }
        public static class GrupoDatoDetalleEmpresa
        {
            public const string ObtenerTodos = "SEG.USP_LMSGEMP05_ListarTodosGrupoDatoDetalleEmpresa";
            public const string ObtenerActivos = "SEG.USP_LMSGEMP05_ListarActivosGrupoDatoDetalleEmpresa";
            public const string InsertarGrupoDatoDetalle = "SEG.USP_IMSGEMP05_InsertarGrupoDatoDetalle";
            public const string EditarGrupoDatoDetalle = "SEG.USP_UMSGEMP05_EditarGrupoDatoDetalle";
            public const string ObtenerGrupoDatoDetalle = "SEG.USP_OMSGEMP05_ObtenerGrupoDatoDetalle";
            public const string ListarGrupoDatosCatalogos = "SEG.USP_LMSGEMP05_ListarCatalogos";
            public const string EliminarGrupoDatoDetalle = "SEG.USP_DMSGEMP05_EliminarGrupoDatoDetalle";
        }

        public static class Empresa
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP00_ListarTodasEmpresas";
            public const string InsertarEmpresa = "SEG.USP_IMSGEMP00_InsertarEmpresa";
            public const string EditarEmpresa = "SEG.USP_UMSGEMP00_EditarEmpresa";
            public const string ObtenerEmpresa = "SEG.USP_OMSGEMP00_ObtenerEmpresa";
            public const string EliminarEmpresa = "SEG.USP_DMSGEMP00_EliminarEmpresa";
        }
        public static class Error
        {
            public const string ObtenerTodos = "SEG.USP_LMSGEMP02_ListarErrores";
        }

        public static class ObjetoDetalle
        {
            public const string Obtener = "SEG.USP_OMSGEMP07_ObtenerObjetoDetalle";
            public const string Insertar = "SEG.USP_IMSGEMP07_InsertarObjetoDetalle";
            public const string Editar = "SEG.USP_UMSGEMP07_EditarObjetoDetalle";
            public const string ObtenerTodos = "SEG.USP_LMSGEMP07_ListarTodosObjetoDetalle";
            public const string ObtenerActivos = "SEG.USP_LMSGEMP07_ListarActivosObjetoDetalle";
            public const string Eliminar = "SEG.USP_DMSGEMP07_EliminarObjetoDetalle";
        }
        public static class ObjetoPadre
        {
            public const string Obtener = "SEG.USP_OMSGEMP06_ObtenerObjetoPadre";
            public const string Insertar = "SEG.USP_IMSGEMP06_InsertarObjetoPadre";
            public const string Editar = "SEG.USP_UMSGEMP06_EditarObjetoPadre";
            public const string ObtenerTodos = "SEG.USP_LMSGEMP06_ListarTodosObjetoPadre";
            public const string ObtenerActivos = "SEG.USP_LMSGEMP06_ListarActivosObjetoPadre";
            public const string Eliminar = "SEG.USP_DMSGEMP06_EliminarObjetoPadre";
        }
        public static class ParametroEmpresa
        {
            public const string ListarParametros = "SEG.USP_LMSGEMP03_ListarParametros";
            public const string ObtenerParametros = "SEG.USP_OMSGEMP03_ObtenerParametros";
            public const string ObtenerParametroPorAbreviacion = "SEG.USP_OMSGEMP03_ObtenerParametroPorAbreviacion";
            public const string InsertarParametros = "SEG.USP_IMSGEMP03_InsertarParametros";
            public const string ActualizarParametros = "SEG.USP_UMSGEMP03_ActualizarParametros";
            public const string EliminarParametros = "SEG.USP_DMSGEMP03_EliminarParametros";
        }
        public static class Perfil
        {
            public const string ListarPerfil = "SEG.USP_LMSGEMP09_ListarPerfil";
            public const string ObtenerPerfil = "SEG.USP_OMSGEMP09_ObtenerPerfil";
            public const string InsertarPerfil = "SEG.USP_IMSGEMP09_InsertarPerfil";
            public const string ActualizarPerfil = "SEG.USP_UMSGEMP09_ActualizarPerfil";
            public const string EliminarPerfil = "SEG.USP_DMSGEMP09_EliminarPerfil";
        }
        public static class PerfilUsuario
        {
            public const string ListarPorUsuario = "SEG.USP_LMSGEMP11_ListarPorUsuario";
            public const string ListarPerfil = "SEG.USP_LMSGEMP11_ListarPerfilUsuario";
            public const string GuardarPerfilesUsuarios = "SEG.USP_IMSGEMP11_InsertarPerfilUsuario";
            public const string GuardarPerfilesPorUsuario = "SEG.USP_IMSGEMP11_InsertarPerfilPorUsuario";
        }
        public static class PerfilObjeto
        {
            public const string ListarPerfil = "SEG.USP_LMSGEMP12_ListarPerfilObjeto";
            public const string ListarPerfilMenuLateral = "SEG.USP_LMSGEMP12_ListarPerfilObjetoMenuLateral";
            public const string ListarPerfilModulos = "SEG.USP_LMSGEMP12_ListarPerfilObjetoModulos";
            public const string GuardarPerfilesObjeto = "SEG.USP_IMSGEMP12_InsertarPerfilObjeto";
            public const string ListarControlesPorPerfil = "SEG.USP_LMSGEMP12_ListarControlesPorPerfil";
            public const string ValidarObjetoPorPerfiles = "SEG.UF_MSGEMP12_ValidarObjetoPorPerfiles";
        }
        public static class Sucursal
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP01_ListarTodasSucursales";
            public const string InsertarSucursal = "SEG.USP_IMSGEMP01_InsertarSucursal";
            public const string EditarSucursal = "SEG.USP_UMSGEMP01_EditarSucursal";
            public const string ObtenerSucursal = "SEG.USP_OMSGEMP01_ObtenerSucursal";
            public const string EliminarSucuesal = "SEG.USP_DMSGEMP01_EliminarSucursal";
        }
        public static class SucursalUsuario
        {
            public const string ListarUsuarios = "SEG.USP_LMSGEMP10_ListarSucursalUsuario";
            public const string ListarSucursalesUsuarios = "SEG.USP_LMSGEMP10_ListarSucursalesUsuario";
            public const string ListarSucursales = "SEG.USP_LMSGEMP10_ListarSucursalesPorUsuario";
            public const string GuardarSucursalUsuario = "SEG.USP_IMSGEMP10_InsertarSucursalUsuario";
            public const string GuardarUsuarioSucursal = "SEG.USP_IMSGEMP10_InsertarUsuarioSucursal";
        }
        public static class Ubigeo
        {
            public const string ObtenerTodos = "SEG.USP_LMSGEMP13_ListarUbigeo";
            public const string Obtener = "SEG.USP_LMSGEMP13_ListarTipoUbigeo";
            public const string ObtenerPaises = "SEG.USP_LMSGEMP13_ListarPais";
        }

        public static class Gerencia
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP14_ListarTodasGerencias";
            public const string Insertar = "SEG.USP_IMSGEMP14_InsertarGerencia";
            public const string Editar = "SEG.USP_UMSGEMP14_ActualizarGerencia";
            public const string Obtener = "SEG.USP_OMSGEMP14_ObtenerGerencia";
            public const string Eliminar = "SEG.USP_DMSGEMP14_EliminarGerencia";
        }

        public static class Articulo
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEPP01_ListarTodosArticulo";
            public const string Insertar = "SEG.USP_IMSGEPP01_InsertarArticulo";
            public const string Editar = "SEG.USP_UMSGEPP01_ActualizarArticulo";
            public const string Obtener = "SEG.USP_OMSGEPP01_ObtenerArticulo";
            public const string Eliminar = "SEG.USP_DMSGEPP01_EliminarArticulo";
            public const string ActualizarPrecios = "SEG.USP_UMSGEPP01_ActualizarPreciosArticulos";

            public const string ObtenerUltimosActualizados = "SEG.USP_LMSGEPP04_ListarUltimosArticulosActualizados";
            public const string ObtenerUltimaActualizacion = "SEG.USP_OMSGEPP04_ObtenerUltimaActualizacion";
        }

        public static class Superintendencia
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP15_ListarTodasSuperintendencias";
            public const string Insertar = "SEG.USP_IMSGEMP15_InsertarSuperintendencia";
            public const string Editar = "SEG.USP_UMSGEMP15_ActualizarSuperintendencia";
            public const string Obtener = "SEG.USP_OMSGEMP15_ObtenerSuperintendencia";
            public const string Eliminar = "SEG.USP_DMSGEMP15_EliminarSuperintendencia";
        }
        public static class Area
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP16_ListarTodasAreas";
            public const string Insertar = "SEG.USP_IMSGEMP16_InsertarArea";
            public const string Editar = "SEG.USP_UMSGEMP16_ActualizarArea";
            public const string Obtener = "SEG.USP_OMSGEMP16_ObtenerArea";
            public const string Eliminar = "SEG.USP_DMSGEMP16_EliminarArea";
        }

        public static class Roster
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP17_ListarTodasRoster";
            public const string Insertar = "SEG.USP_IMSGEMP17_InsertarRoster";
            public const string Editar = "SEG.USP_UMSGEMP17_ActualizarRoster";
            public const string Obtener = "SEG.USP_OMSGEMP17_ObtenerRoster";
            public const string Eliminar = "SEG.USP_DMSGEMP17_EliminarRoster";
        }
        public static class Guardia
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP20_ListarTodasGuardias";
            public const string Insertar = "SEG.USP_IMSGEMP20_InsertarGuardia";
            public const string Editar = "SEG.USP_UMSGEMP20_ActualizarGuardia";
            public const string Obtener = "SEG.USP_OMSGEMP20_ObtenerGuardia";
            public const string ObtenerPorId = "SEG.USP_OMSGEMP20_ObtenerGuardiaXid";
            public const string Eliminar = "SEG.USP_DMSGEMP20_EliminarGuardia";
        }
        public static class GrupoCargo
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP18_ListarTodosGrupoCargo";
            public const string ListarDetalle = "SEG.USP_LMSGEMP18_ListarDetalleGrupoCargo";
            public const string Insertar = "SEG.USP_IMSGEMP18_InsertarGrupoCargo";
            public const string Editar = "SEG.USP_UMSGEMP18_ActualizarGrupoCargo";
            public const string EditarDetalle = "SEG.USP_UMSGEMP18_ActualizarDetalleGrupoCargo";
            public const string Obtener = "SEG.USP_OMSGEMP18_ObtenerGrupoCargo";
            public const string Eliminar = "SEG.USP_DMSGEMP18_EliminarGrupoCargo";
            public const string EliminarDetalle = "SEG.USP_DMSGEMP18_EliminarDetallleGrupoCargo";
        }
        public static class Cargo
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEMP19_ListarTodosCargo";
            public const string Insertar = "SEG.USP_IMSGEMP19_InsertarCargo";
            public const string Editar = "SEG.USP_UMSGEMP19_ActualizarCargo";
            public const string Obtener = "SEG.USP_OMSGEMP19_ObtenerCargo";
            public const string Eliminar = "SEG.USP_DMSGEMP19_EliminarCargo";
        }
        public static class Familia
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEPP00_ListarTodasFamilia";
            public const string Insertar = "SEG.USP_IMSGEPP00_InsertarFamilia";
            public const string Editar = "SEG.USP_UMSGEPP00_ActualizarFamilia";
            public const string Obtener = "SEG.USP_OMSGEPP00_ObtenerFamilia";
            public const string Eliminar = "SEG.USP_DMSGEPP00_EliminarFamilia";
            public const string ObtenerTodasPadre = "SEG.USP_LMSGEPP00_ListarTodasFamiliaPadre";
        }
        public static class Almacen
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEPP03_ListarTodosAlmacen";
            public const string Insertar = "SEG.USP_IMSGEPP03_InsertarAlmacen";
            public const string Editar = "SEG.USP_UMSGEPP03_ActualizarAlmacen";
            public const string Obtener = "SEG.USP_OMSGEPP03_ObtenerAlmacen";
            public const string Eliminar = "SEG.USP_DMSGEPP03_EliminarAlmacen";
        }
        public static class Stock
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEPP05_ListarTodosStock";
            public const string Insertar = "SEG.USP_IMSGEPP05_InsertarStock";
            public const string Editar = "SEG.USP_UMSGEPP05_ActualizarStock";
            public const string Obtener = "SEG.USP_OMSGEPP05_ObtenerStock";
            public const string Eliminar = "SEG.USP_DMSGEPP05_EliminarStock";
            public const string ActualizarStock = "SEG.USP_UMSGEPP05_ActualizarStockAlmacen";
            public const string ActualizarStockMinimo = "SEG.USP_UMSGEPP05_ActualizarStockMinimo";

            public const string ObtenerUltimosActualizados = "SEG.USP_LMSGEPP07_ListarStockActualizados";
            public const string ObtenerUltimaActualizacion = "SEG.USP_OMSGEPP07_ObtenerActualizacionStock";
        }

        public static class Kit
        {
            public const string ObtenerTodas = "SEG.USP_LMSGEPP02_ListarTodosKit";
            public const string Insertar = "SEG.USP_IMSGEPP02_InsertarKit";
            public const string Editar = "SEG.USP_UMSGEPP02_ActualizarKit";
            public const string Obtener = "SEG.USP_OMSGEPP02_ObtenerKit";
            public const string ListarDetalle = "SEG.USP_OMSGEPP02_ListarDetalleKit";
            public const string Eliminar = "SEG.USP_DMSGEPP02_EliminarKit";
        }
        public static class TrabajadorTalla
        {
            public const string ActualizarTrabajadorTalla = "SEG.USP_UMSGEPP06_ActualizarTrabajadorTalla";
            public const string ListarTalla = "SEG.USP_LMSGEPP06_ListarTodasTalla";
            public const string EliminarDetalle = "SEG.USP_DMSGEPP06_EliminarDetallleTalla";
        }
        public static class TrabajadorRestriccion
        {
            public const string ActualizarTrabajadorRestriccion = "SEG.USP_UMSGEPP09_ActualizarRestricciones";
            public const string ListarRestriccion = "SEG.USP_LMSGEPP09_ListarTodasRestricciones";
            public const string EliminarDetalle = "SEG.USP_DMSGEPP09_EliminarDetallleRestricciones";
        }
        /*PERSONA*/
        public static class Persona
        {
            public const string ListarDatosPersona = "PER.USP_LMPEPE00_ListaPersonas";
            public const string ListarColaboradores = "PER.USP_LMPEPE00_ListaColaboradores";
            public const string ObtenerDatosPersona = "PER.USP_OMPEPE00_ObtenerPersona";
            public const string InsertarPersona = "PER.USP_IMPEPE00_InsertarPersona";
            public const string ActualizarPersona = "PER.USP_UMPEPE00_ActualizarPersona";
            public const string EliminarPersona = "PER.USP_DMPEPE00_EliminarPersona";

            //
            public const string ObtenerDatosPersonaPrincipal = "PER.USP_LMPEPE00_ListaPersonaPrincipal";
            public const string listaHomonimos = "PER.USP_LMPEPE00_ListaPersonasHomonimas";
        }

        public static class Direccion
        {
            public const string InsertarDireccion = "PER.USP_IMPEPE01_InsertarDireccion";
            public const string ActualizarDireccion = "PER.USP_UMPEPE01_ActualizarDireccion";
            public const string EliminarDireccion = "PER.USP_DMPEPE01_EliminarDireccion";
            public const string ListarDireccion = "PER.USP_LMPEPE01_ListarDireccion";
            public const string ObtenerDireccion = "PER.USP_OMPEPE01_ObtenerDireccion";
        }
        public static class Telefono
        {
            public const string InsertarTelefono = "PER.USP_IMPEPE02_InsertarTelefono";
            public const string ActualizarTelefono = "PER.USP_UMPEPE02_ActualizarTelefono";
            public const string EliminarTelefono = "PER.USP_DMPEPE02_EliminarTelefono";
            public const string ListarTelefono = "PER.USP_LMPEPE02_ListarTelefono";
            public const string ObtenerTelefono = "PER.USP_OMPEPE02_ObtenerTelefono";
        }

        public static class Correo
        {
            public const string InsertarCorreo = "PER.USP_IMPEPE03_InsertarCorreo";
            public const string ActualizarCorreo = "PER.USP_UMPEPE03_ActualizarCorreo";
            public const string EliminarCorreo = "PER.USP_DMPEPE03_EliminarCorreo";
            public const string ListarCorreo = "PER.USP_LMPEPE03_ListarCorreo";
            public const string ObtenerCorreo = "PER.USP_OMPEPE03_ObtenerCorreo";
        }

        public static class Documento
        {
            public const string InsertarDocumento = "PER.USP_IMPEPE04_InsertarDocumento";
            public const string ActualizarDocumento = "PER.USP_UMPEPE04_ActualizarDocumento";
            public const string EliminarDocumento = "PER.USP_DMPEPE04_EliminarDocumento";
            public const string ListarDocumento = "PER.USP_LMPEPE04_ListarDocumento";
            public const string ObtenerDocumento = "PER.USP_OMPEPE04_ObtenerDocumento";
            public const string ObtenerUltimoCRN = "PER.UF_OBTENER_ULTIMO_CRN";
        }

        public static class Archivo
        {
            public const string InsertarArchivo = "PER.USP_IMPEPE05_InsertarArchivo";
            public const string EliminarArchivo = "PER.USP_DMPEPE05_EliminarArchivo";
            public const string ListarArchivo = "PER.USP_LMPEPE05_ListarArchivo";
            public const string ObtenerArchivo = "PER.USP_OMPEPE05_ObtenerArchivo";
        }

        public static class Trabajador
        {
            public const string InsertarTrabajador = "PER.USP_IMPEPE06_InsertarTrabajador";
            public const string ActualizarTrabajador = "PER.USP_UMPEPE06_ActualizarTrabajador";
            public const string EliminarTrabajador = "PER.USP_DMPEPE06_EliminarTrabajador";
            public const string ListarTrabajador = "PER.USP_LMPEPE06_ListarTrabajador";
            public const string ListarPersonalTrabajador = "PER.USP_LMPEPE06_ListarTrabajadorTareo";
            public const string ObtenerTrabajador = "PER.USP_OMPEPE06_ObtenerTrabajador";
            public const string ListaContratista = "PER.USP_LMPEPE06_ListarContratista";
        }
        public static class Posicion
        {
            public const string ListarTodosPosiciones = "SEG.USP_LMSGTRB00_ListarTodosPosiciones";
        }
        public static class CentroDeCostos
        {
            public const string ListarTodosCentroDeCostos = "SEG.USP_LMSGTRB01_ListarTodosCentroDeCostos";
        }
        public static class UnidadOrganizacional
        {
            public const string ListarTodosUnidadOrganizacional = "SEG.USP_LMSGTRB02_ListarTodosUnidadOrganizacional";
        }


        /*TAREO*/
        public static class Tareo
        {
            public const string ListarDatosTareo = "SEG.USP_LMSGTRO00_ListaTareo";
            public const string ObtenerDatosTareo = "SEG.USP_OMSGTRO00_ObtenerTareo";
            public const string ObtenerUltimoTareo = "SEG.USP_OMSGTRO00_ObtenerUltimoTareo";
            public const string InsertarTareo = "SEG.USP_IMSGTRO00_InsertarTareo";
            public const string InsertarTareoDetalle = "SEG.USP_IMSGTRO00_InsertarTareoDetalle";
            public const string ActualizarTareo = "SEG.USP_UMSGTRO00_ActualizarTareo";
            public const string EliminarTareo = "SEG.USP_DMSGTRO00_EliminarTareo";
        }
        /*EPP*/
        public static class Proyeccion
        {
            public const string ListarProyeccion = "SEG.USP_LMSGEPP10_ListarTodosProyeccion";
            public const string ObtenerProyeccion = "SEG.USP_OMSGEPP10_ObtenerProyeccion";
            public const string InsertarProyeccion = "SEG.USP_IMSGEPP10_InsertarProyeccion";
            public const string ActualizarProyeccion = "SEG.USP_UMSGEPP10_ActualizarProyeccion";
            public const string ActualizarAprobadoProyeccion = "SEG.USP_UMSGEPP10_ActualizarAprobadoProyeccion";
            public const string EliminarProyeccion = "SEG.USP_DMSGEPP10_EliminarProyeccion";
            public const string EliminarProyeccionDetalle = "SEG.USP_DMSGEPP11_EliminarProyeccionDetalle";
        }
        public static class MovimientoAlmacen
        {
            public const string ListarDatosMovimientoAlmacen = "SGS.USP_LMSGTRO00_ListaMovimientoAlmacen";
            public const string ObtenerDatosMovimientoAlmacen = "SGS.USP_OMSGTRO00_ObtenerMovimientoAlmacen";
            public const string InsertarMovimientoAlmacen = "PER.USP_IMSGTRO00_InsertarMovimientoAlmacen";
            public const string ActualizarMovimientoAlmacen = "PER.USP_UMSGTRO00_ActualizarMovimientoAlmacen";
            public const string EliminarMovimientoAlmacen = "PER.USP_DMSGTRO00_EliminarMovimientoAlmacen";
        }
    }
}
