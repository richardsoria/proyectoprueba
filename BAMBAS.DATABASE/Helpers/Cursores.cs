﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAMBAS.DATABASE.Helpers
{
    public class Cursores<T1, T2>
    {
        public List<T1> Cursor1 { get; set; }
        public List<T2> Cursor2 { get; set; }
    }
    public class Cursores<T1,T2,T3>
    {
        public List<T1> Cursor1 { get; set; }
        public List<T2> Cursor2 { get; set; }
        public List<T3> Cursor3 { get; set; }
    }
}
