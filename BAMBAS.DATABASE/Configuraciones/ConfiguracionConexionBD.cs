﻿using Dapper;
using BAMBAS.CORE.Structs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BAMBAS.DATABASE.Configuraciones
{
    public class ConfiguracionConexionBD
    {
        public ConfiguracionConexionBD(string value) => CadenaConexion = value;
        public string CadenaConexion { get; }
    }
}
