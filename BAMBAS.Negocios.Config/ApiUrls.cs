﻿namespace BAMBAS.Negocios
{
    public class ApiUrls
    {
        public string IdentityUrl { get; set; }
        public string SeguridadUrl { get; set; }
        public string PersonaUrl { get; set; }
        public string TareoUrl { get; set; }

        public string EPPUrl { get; set; }
    }
}
